(function($){
	var initLayout = function() {
        var default_themecolors=new Array();
        default_themecolors[''] = '';

		var hash = window.location.hash.replace('#', '');
		var currentTab = $('ul.navigationTabs a')
							.bind('click', showTab)
							.filter('a[rel=' + hash + ']');
		if (currentTab.size() == 0) {
			currentTab = $('ul.navigationTabs a:first');
		}
		showTab.apply(currentTab.get(0));
		$('#colorpickerHolder').ColorPicker({flat: true});
		$('#colorpickerHolder2').ColorPicker({
			flat: true,
			color: '#00ff00',
			onSubmit: function(hsb, hex, rgb) {
				$('#colorSelector2 div').css('backgroundColor', '#' + hex);
			}
		});
		$('#colorpickerHolder2>div').css('position', 'absolute');
		var widt = false;
		$('#colorSelector2').bind('click', function() {
			$('#colorpickerHolder2').stop().animate({height: widt ? 0 : 173}, 500);
			widt = !widt;
		});
		$('#colorpickerField1, #colorpickerField2, #colorpickerField3').ColorPicker({
			onSubmit: function(hsb, hex, rgb, el) {
				$(el).val(hex);
				$(el).ColorPickerHide();
			},
			onBeforeShow: function () {
				$(this).ColorPickerSetColor(this.value);
			}
		})
		.bind('keyup', function(){
			$(this).ColorPickerSetColor(this.value);
		});


        //*Theme Color Start*//
        if(typeof original_text_color == 'undefined')original_text_color = '000';
        $('.colorSelectorP1').ColorPicker({
            //color: '#0000ff',

            color: original_text_color,
            onSubmit: function (hsb, hex, rgb){
                original_text_color = new_color_colorSelectorP1;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                $('.colorSelectorP1 div.inner').css('background-color', original_text_color);
                $('#single-team-section .span9, #single-team-section h1, #single-team h2, #single-team h3, #single-team h4, #single-team h5, #single-team h6').css('color', '#' + original_text_color+'!important');
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color_colorSelectorP1 = original_text_color;
                window.new_color_colorSelectorP1 = hex;

                $('.colorSelectorP1 div.inner').css('background-color', '#' + hex+'!important');
                $('#single-team-section .span9, #single-team-section h1, #single-team h2, #single-team h3, #single-team h4, #single-team h5, #single-team h6').css('color', '#' + hex+'!important');
            }
        });

        if(typeof original_bg_color == 'undefined')original_bg_color = '000';
        $('.colorSelectorP2').ColorPicker({
            color: original_bg_color,
            onSubmit: function (hsb, hex, rgb){
                original_bg_color = new_colorSelectorP2;
            },

            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorP2 div.inner').css('background-color', original_bg_color);
                $(colpkr).fadeOut(500);
                //console.log(original_bg_color+'___original_bg_color');
                $('#single-team-section').css({backgroundColor:original_bg_color+' !important;'});

                //$('.arrow_border, .arrow').css({
                $('#left-arrow-pointer').css({
                    borderRightColor: original_bg_color
                });

                //rgba(0, 0, 0, 0) <?=$this->session->userdata['profile_theme']['background']?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);

                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_colorSelectorP2 = original_bg_color;
                window.new_colorSelectorP2 = hex;

                $('#single-team-section').css({backgroundColor:'#'+hex+' !important;'});
                //$('.arrow_border, .arrow').css({
                $('#left-arrow-pointer').css({
                     borderRightColor: '#'+hex
                 });


                /*$('.arrow_border').css({
                    borderRightColor: '#'+hex
                });
                $('.arrow').css({
                    borderRightColor: '#'+hex
                    //, borderBottomColor: '#'+hex
                }); */

                $('.colorSelectorP2 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        $('.colorSelectorP3').ColorPicker({
            //color: profile_theme_icons,
            color: '#0000ff',
            onSubmit: function (){
                profile_theme_icons=new_colorSelectorP3;

                $('#social-connected-profile ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', profile_theme_icons+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });
                
                $('#social-connected-profile-full ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', profile_theme_icons+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });


            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorP3 div.inner').css('background-color', profile_theme_icons);
                $(colpkr).fadeOut(500);
                //$('#social-connected-profile ul.public-social-list li a i').css('color', profile_theme_icons+'!important');

                var target_ele = $('#social-connected-profile ul.public-social-list li a i').css('color', profile_theme_icons+'!important');
                //console.log(profile_theme_icons);
                target_ele.css('color', profile_theme_icons+'!important');
                
                var target_ele = $('#social-connected-profile-full ul.public-social-list li a i').css('color', profile_theme_icons+'!important');
                //console.log(profile_theme_icons);
                target_ele.css('color', profile_theme_icons+'!important');


                //return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                var new_colorSelectorP3 = profile_theme_icons;
                window.new_colorSelectorP3 = new_color;
                //console.log(new_colorSelectorP3+'new_colorSelectorP3');
                //$('#single-team-section .team-social ul.pub-soc-li li a:hover i').css('color', '#' + hex+'!important');
                $('#social-connected-profile ul.public-social-list li a i').css('color', new_color+'!important');
                $('#social-connected-profile-full ul.public-social-list li a i').css('color', new_color+'!important');

                $('#social-connected-profile ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    //target_ele.css('color', new_colorSelectorP3+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+new_colorSelectorP3+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });
                
                $('#social-connected-profile-full ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    //target_ele.css('color', new_colorSelectorP3+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+new_colorSelectorP3+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });
                
                $('.colorSelectorP3 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        $('.colorSelectorP4').ColorPicker({
            ///color: profile_icons_rollover,
            color: '#0000ff',
            onSubmit: function (){
                profile_icons_rollover=new_colorSelectorP4;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                $('.colorSelectorP4 div.inner').css('background-color', profile_icons_rollover);
                $('#social-connected-profile ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', profile_theme_icons+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });
                
                $('#social-connected-profile-full ul.public-social-list li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', profile_theme_icons+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                    target_ele.attr("onmouseover","this.style.color='"+profile_icons_rollover+"'");
                });


                return false;
            },
            onChange: function (hsb, hex, rgb) {
            var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
            //var new_colorSelectorP4 = profile_icons_rollover;
            //window.profile_icons_rollover = new_color;
            $('#social-connected-profile ul.public-social-list li a i').each(function (){
                var target_ele = $(this);
                //target_ele.css('color', new_color+'!important');
                target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                target_ele.attr("onmouseover","this.style.color='"+new_color+"'");
            });
            
            $('#social-connected-profile-full ul.public-social-list li a i').each(function (){
                var target_ele = $(this);
                //target_ele.css('color', new_color+'!important');
                target_ele.attr("onmouseout","this.style.color='"+profile_theme_icons+"'");
                target_ele.attr("onmouseover","this.style.color='"+new_color+"'");
            });
            
            $('.colorSelectorP4 div.inner').css('background-color', '#' + hex+'!important');


                window.new_colorSelectorP4 = new_color;
                //target_ele.attr("onMouseOver","this.style.color='"+new_colorSelectorP4+"'");
                ///console.log("window.new_colorSelectorP4 = new_color;"+new_color);
                //$('#social-connected-profile ul.public-social-list li a:hover i').css('color', new_color+'!important');
            }
        });


        $('.colorSelectorF1').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                feature_text =new_colorSelectorF1;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorF1 div.inner').css('background-color', feature_text);
                $(colpkr).fadeOut(500);
                $('.featured-description-text').css('color',feature_text+' !important');
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                ///feature_text
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                var new_colorSelectorF1=null;
                window.new_colorSelectorF1 = new_color;
                $('.featured-description-text').css('color',new_color);
                $('.colorSelectorF1 div.inner').css('background-color', '#' + hex+'!important');
                //$('.featured-description-text').addImportantcss('color',new_color);



                //$('.colorSelectorF1 div.inner').css('background-color', '#' + hex+'!important');
            }
        });


        $('.colorSelectorF2').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                feature_BGA=new_colorSelectorF2;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorF2 div.inner').css('background-color', feature_BGA);
                $(colpkr).fadeOut(500);
                $('.featured-bg').css('background-color',feature_BGA);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                //$('.colorSelectorF2 div.inner').css('background-color', '#' + hex+'!important');
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                var new_colorSelectorF2=null;
                window.new_colorSelectorF2 = new_color;
                $('.featured-bg').css('background-color',new_color);
                $('.colorSelectorF2 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        $('.colorSelectorF3').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                feature_BGB =new_colorSelectorF3;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorF3 div.inner').css('background-color', feature_BGB);
                $(colpkr).fadeOut(500);
                $('#feature_BGB').css('background-color', feature_BGB);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                var new_colorSelectorF3=null;
                window.new_colorSelectorF3 = new_color;
                $('#feature_BGB').css('background-color', new_color);
                $('.colorSelectorF3 div.inner').css('background-color', '#' + hex+'!important');
                ///$('.colorSelectorF3 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        if(typeof service_box_text == 'undefined')service_box_text = '000';
        $('.colorSelectorG1').ColorPicker({
            color: service_box_text,
            onSubmit: function (){
                service_box_text = new_service_box_text;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorG1 div.inner').css('background-color', service_box_text);
                $(colpkr).fadeOut(500);
                $('#services-box h3, .latest-work-disp > h4').css({color:service_box_text});
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                window.new_service_box_text = new_color;
                $('#services-box h3, .latest-work-disp > h4').css({color:new_color});
                $('.colorSelectorG1 div.inner').css('background-color', '#' + hex+'!important');
                //$('.colorSelectorG1 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        if(typeof service_box_bg == 'undefined')service_box_bg = '000';
        $('.colorSelectorG2').ColorPicker({
            color: service_box_bg,
            onSubmit: function (){
                service_box_bg =new_color_service_box_bg;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },

            onHide: function (colpkr) {
                $('.colorSelectorG2 div.inner').css('background-color', service_box_bg);
                $(colpkr).fadeOut(500);
                $('#services-box').css('background-color',service_box_bg+'!important');
                return false;
            },

            onChange: function (hsb, hex, rgb) {
                var new_color_service_box_bg = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                window.new_color_service_box_bg = new_color_service_box_bg;
                $('#services-box').css('background-color',new_color_service_box_bg+'!important');
                //$('.colorSelectorG3 div.inner').css('background-color', '#' + hex+'!important');
                $('.colorSelectorG2 div.inner').css('background-color', '#' + hex+'!important');
            }
        });

        if(typeof gallery_theme_overlay == 'undefined')gallery_theme_overlay = '000';
        $('.colorSelectorG3').ColorPicker({
            color: gallery_theme_overlay,
            onSubmit: function (){
                gallery_theme_overlay=new_gallery_theme_overlay;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },

            onHide: function (colpkr) {
                var prev_color = gallery_theme_overlay;
                $('.colorSelectorG3 div.inner').css('background-color', gallery_theme_overlay);
                $(colpkr).fadeOut(500);
                $('.latest-work-disp').css({"background-color":gallery_theme_overlay});
                return false;
            },

            onChange: function (hsb, hex, rgb) {
                var new_gallery_theme_overlay = 'rgba('+rgb.r+', '+rgb.g+', '+rgb.b+', 0.5 )';
                window.new_gallery_theme_overlay=new_gallery_theme_overlay;
                $('.latest-work-disp').css({"background-color":new_gallery_theme_overlay});
                $('.colorSelectorG3 div.inner').css('background-color', '#' + hex+'!important');
            }
        });


        $('.colorSelectorR1').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                resume_theme_text = new_resume_theme_text;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorR1 div.inner').css('background-color', resume_theme_text);
                $(colpkr).fadeOut(500);
                $('#latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6, #latest-work').css('color',resume_theme_text+'!important');
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_resume_theme_text = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                window.new_resume_theme_text = new_resume_theme_text;
                 $('#latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6, #latest-work').css('color',new_resume_theme_text+'!important');
                $('.colorSelectorR1 div.inner').css('background-color', '#' + hex+'!important');
            }

        });

        $('.colorSelectorR2').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                resume_theme_background = new_resume_theme_background;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                $('#latest-work').css('background-color',resume_theme_background+'!important');
                $('.colorSelectorR2 div.inner').css('background-color', resume_theme_background);

                $('footer .arrow-down').css({
                    borderTopColor: '#'+resume_theme_background
                });


                return false;
            },
            onChange: function (hsb, hex, rgb) {
                ///$('.colorSelectorR2 div.inner').css('background-color', '#' + hex+'!important');
                var new_resume_theme_background = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                window.new_resume_theme_background = new_resume_theme_background;
                $('#latest-work').css('background-color',new_resume_theme_background+'!important');
                $('footer .arrow-down').css({borderTopColor:new_resume_theme_background});
                $('.colorSelectorR2 div.inner').css('background-color', '#' + hex+'!important');
            }
        });


        /*$('.colorSelectorF1').ColorPicker({
            color: '#0000ff',
            onSubmit: function () {

            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                return false;
            },
            onChange: function (hsb, hex, rgb) {
                $('.colorSelectorF1 div.inner').css('background-color', '#' + hex + '!important');
            }
        }); */


        $('.colorSelectorFoot1').ColorPicker({
            //color: profile_theme_icons,
            color: '#0000ff',
            onSubmit: function (){
                footer_theme_icon=new_colorSelectorF1;

                $('#social-footer ul li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', footer_theme_icon+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+footer_theme_icon+"'");
                    target_ele.attr("onmouseover","this.style.color='"+footer_theme_icons_rollover+"'");
                });


            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $('.colorSelectorFoot1 div.inner').css('background-color', footer_theme_icon);
                $(colpkr).fadeOut(500);
                //$('#social-connected-profile ul.public-social-list li a i').css('color', profile_theme_icons+'!important');

                var target_ele = $('#social-footer ul li a i').css('color', footer_theme_icon+'!important');
                //console.log(profile_theme_icons);
                target_ele.css('color', footer_theme_icon+'!important');


                //return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                var new_colorSelectorF1 = footer_theme_icon;
                window.new_colorSelectorF1 = new_color;
                //console.log(new_colorSelectorP3+'new_colorSelectorP3');
                //$('#single-team-section .team-social ul.pub-soc-li li a:hover i').css('color', '#' + hex+'!important');
                $('#social-footer ul li a i').css('color', new_color+'!important');

                $('#social-footer ul li a i').each(function (){
                    var target_ele = $(this);
                    //target_ele.css('color', new_colorSelectorP3+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+new_colorSelectorF1+"'");
                    target_ele.attr("onmouseover","this.style.color='"+footer_theme_icons_rollover+"'");
                });
                $('.colorSelectorFoot1 div.inner').css('background-color', '#' + hex+'!important');
            }
        });






        $('.colorSelectorFoot2').ColorPicker({
            ///color: profile_icons_rollover,
            color: '#0000ff',
            onSubmit: function (){
                footer_theme_icons_rollover=new_colorSelectorFoot2;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                $('.colorSelectorFoot2 div.inner').css('background-color', footer_theme_icons_rollover);
                $('#social-footer ul li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', footer_theme_icon+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+footer_theme_icon+"'");
                    target_ele.attr("onmouseover","this.style.color='"+footer_theme_icons_rollover+"'");
                });


                return false;
            },
            onChange: function (hsb, hex, rgb) {
                var new_color = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                //var new_colorSelectorP4 = profile_icons_rollover;
                //window.profile_icons_rollover = new_color;
                $('#social-footer ul li a i').each(function (){
                    var target_ele = $(this);
                    target_ele.css('color', footer_theme_icon+'!important');
                    target_ele.attr("onmouseout","this.style.color='"+footer_theme_icon+"'");
                    target_ele.attr("onmouseover","this.style.color='"+new_color+"'");
                });
                $('.colorSelectorFoot2 div.inner').css('background-color', '#' + hex+'!important');

                window.new_colorSelectorFoot2 = new_color;
            }
        });








        $('.colorSelectorFoot3').ColorPicker({
            color: '#0000ff',
            onSubmit: function (){
                footer_theme_backgroundA = new_footer_theme_backgroundA;
            },
            onShow: function (colpkr) {
                $(colpkr).fadeIn(500);
                return false;
            },
            onHide: function (colpkr) {
                $(colpkr).fadeOut(500);
                $('.colorSelectorFoot3 div.inner').css('background-color', footer_theme_backgroundA);
                $('footer').css('background-color',footer_theme_backgroundA+'!important');
                return false;
            },
            onChange: function (hsb, hex, rgb) {

                var new_footer_theme_backgroundA = 'rgb('+rgb.r+', '+rgb.g+', '+rgb.b+' )';
                window.new_footer_theme_backgroundA = new_footer_theme_backgroundA;
                $('footer').css('background-color',new_footer_theme_backgroundA+'!important');
                $('.colorSelectorFoot3 div.inner').css('background-color', '#' + hex+'!important');

                //$('.colorSelectorFoot3 div.inner').css('background-color', '#' + hex+'!important');
            }
        });
        //*Theme Color End*//

	};
	
	var showTab = function(e) {
		var tabIndex = $('ul.navigationTabs a')
							.removeClass('active')
							.index(this);
		$(this)
			.addClass('active')
			.blur();
		$('div.tab')
			.hide()
				.eq(tabIndex)
				.show();
	};
	
	EYE.register(initLayout, 'init');
})(jQuery)