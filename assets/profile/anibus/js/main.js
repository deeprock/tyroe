jQuery(function($){

var ANUBIS = window.ANUBIS || {};

/* ==================================================
 Drop Menu
 ================================================== */

ANUBIS.subMenu = function(){
    $('#menu-nav').supersubs({
        minWidth: 12,
        maxWidth: 27,
        extraWidth: 0 // set to 1 if lines turn over
    }).superfish({
            delay: 0,
            animation: {opacity:'show'},
            speed: 'fast',
            autoArrows: false,
            dropShadows: false
        });
}

/* ==================================================
 Mobile Navigation
 ================================================== */
/* Clone Menu for use later */
var mobileMenuClone = $('#menu').clone().attr('id', 'navigation-mobile');

ANUBIS.mobileNav = function(){
    var windowWidth = $(window).width();

    // Show Menu or Hide the Menu
    if( windowWidth <= 979 ) {
        if( $('#mobile-nav').length > 0 ) {
            mobileMenuClone.insertAfter('header');
            $('#navigation-mobile #menu-nav').attr('id', 'menu-nav-mobile').wrap('<div class="container"><div class="row"><div class="span12" />');
        }
    } else {
        $('#navigation-mobile').css('display', 'none');
        if ($('#mobile-nav').hasClass('open')) {
            $('#mobile-nav').removeClass('open');
        }
    }
}

// Call the Event for Menu
ANUBIS.listenerMenu = function(){
    $('#mobile-nav').on('click', function(e){
        $(this).toggleClass('open');

        $('#navigation-mobile').stop().slideToggle(350, 'easeOutExpo');

        e.preventDefault();
    });
}

/* ==================================================
   Accordion
================================================== */

ANUBIS.accordion = function(){
	var accordion_trigger = $('.accordion-heading.accordionize');

	accordion_trigger.delegate('.accordion-toggle','click', function(e){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		   	$(this).addClass('inactive');
		}
		else{
		  	accordion_trigger.find('.active').addClass('inactive');
		  	accordion_trigger.find('.active').removeClass('active');
		  	$(this).removeClass('inactive');
		  	$(this).addClass('active');
	 	}
		e.preventDefault();
	});
}

/* ==================================================
   Toggle
================================================== */

ANUBIS.toggle = function(){
	var accordion_trigger_toggle = $('.accordion-heading.togglize');

	accordion_trigger_toggle.delegate('.accordion-toggle','click', function(e){
		if($(this).hasClass('active')){
			$(this).removeClass('active');
		   	$(this).addClass('inactive');
		}
		else{
		  	$(this).removeClass('inactive');
		  	$(this).addClass('active');
	 	}
		e.preventDefault();
	});
}


/* ==================================================
	Init
================================================== */


$(document).ready(function(){
    ANUBIS.accordion();
	ANUBIS.toggle();
    ANUBIS.mobileNav();
    ANUBIS.listenerMenu();
    ANUBIS.subMenu();
});

});
