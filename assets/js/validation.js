//Registeration Page
$(document).ready(function () {
    /*$.fn.extend( {
    -----UNCOMPLETED FUNCTION-----
        addImportantcss: function (propertyName, propertyVal) {
            if(propertyVal==null || typeof propertyVal==='undefined'|| propertyName==null || typeof propertyName==='undefined' ){
                console.error('ERRROR');
                return false;
            }

            this_ele = $(this);

            propertyName = propertyName.toLowerCase();
            propertyName = $.trim(propertyName);
            this_ele.css({propertyName:null});



            setTimeout(function (){
                var style_attr = this_ele.attr('style');
                console.log(style_attr+'style_attr__TimeOUT');
            }, 100)
            var style_attr = this_ele.attr('style');
            console.log(style_attr+'style_attr');
            //console.log(propertyVal+'__propertyVal');

            style_attr = style_attr+propertyName+':'+propertyVal+' !important; ';

            //style_attr = style_attr.replace(propertyName+':'+propertyVal+';',propertyName+':'+propertyVal+'');

            console.log(style_attr+'___style_attr');
            ///return this_ele.attr('style',style_attr);
        }

    }) */

})

function is_JSON(json_obj){
    try{
        JSON.parse(json_obj);
    } catch(e){
        return false;
    }
    return true;
}

function isEven(value) {
    if (value%2 == 0)
        return true;
    else
        return false;
}



function collect_galleryimagesdata(){
    var lefT_img_len = $('#portfolio-image-column1').find('.portfolio_latestwork_id').length;;
    var righT_img_len = $('#portfolio-image-column2').find('.portfolio_latestwork_id').length;;
    var total_loop_len = lefT_img_len+righT_img_len;
    var html_arraydata = new Array();
    var left_eq = 0;
    var right_eq = 0;
    for(forarray =0; forarray<total_loop_len; forarray++){
        if(isEven(forarray) ==true){
            left_eq++;
            var html_target = $('#portfolio-image-column2').find('.portfolio_latestwork_id').eq(left_eq).html();
        } else {
            right_eq++;
            var html_target = $('#portfolio-image-column1').find('.portfolio_latestwork_id').eq(right_eq).html();
        }
        html_arraydata[forarray]=html_target;
    }
    return html_arraydata;
    //console.log(html_arraydata);
}

function reorder_galleryimages(imagesdata){
    /*if(typeof imagesdata ==='undefined' || imagesdata==null){
        return false;
    } */

    $('body').after('<p class="prevdatatext"></p>');

    total_loop_len = imagesdata.length;

    for(forarray =0; forarray<total_loop_len; forarray++){
        var attach_html =  imagesdata[forarray];
        if(isEven(forarray)==true){
            var target_ele =$('#portfolio-image-column2').find('.portfolio_latestwork_id');
        } else {
            var target_ele =$('#portfolio-image-column1').find('.portfolio_latestwork_id');
        }
        $('body').after(attach_html);
        target_ele.prepend(attach_html);

    }

}

function form_submit() {
    
    if ($('#user_type').val() == "company") {
        
        var is_company_invite = $('#is_company_invite').val();
        
        $('#company_locations').css({border: ''});
        $('#company_name').css({border: ''});
        $('#job_title').css({border: ''});
        $('#s2id_country').css({border: ''});
        $('#s2id_city').css({border: ''});

        $('#user_firstname').css({border: ''});
        $('#user_lastname').css({border: ''});
        $('#user_name').css({border: ''});
        $('#user_email').css({border: ''});
        $('#user_password').css({border: ''});
        $('#tearms_box').css({border: ''});
        
        if ($.trim($('#company_name').val()) == "") {
            $('#company_name').css({
                border: '1px solid red'
            });
            $('#company_name').focus();
            return false;
        }
        
        if(is_company_invite == 0){
            if($('#location_select').hasClass('active')){
                if ($.trim($('#country').val()) == "") {
                    $('#s2id_country').css({
                        border: '1px solid red'
                    });
                    $('#s2id_country').focus();
                    return false;
                }

                if ($.trim($('#city').val()) == "") {
                    $('#s2id_city').css({
                        border: '1px solid red'
                    });
                    $('#s2id_city').focus();
                    return false;
                }
            }

            if($('#company_locations').hasClass('active')){
                var i = 0;
                $('.select_location').each(function(){
                    if($(this).hasClass('active')){
                       i++; 
                    }
                })

                if(i == 0){
                    $('#company_locations').css({
                        border: '1px solid red'
                    });

                    return false;
                }
            }


            if($('.select_location_new').hasClass('active')){
                if ($.trim($('#country').val()) == "") {
                    $('#s2id_country').css({
                        border: '1px solid red'
                    });
                    $('#s2id_country').focus();
                    return false;
                }

                if ($.trim($('#city').val()) == "") {
                    $('#s2id_city').css({
                        border: '1px solid red'
                    });
                    $('#s2id_city').focus();
                    return false;
                }
            }
        }
        
        
        
        
        
        
        if ($.trim($('#job_title').val()) == "") {
            $('#job_title').css({
                border: '1px solid red'
            });
            $('#job_title').focus();
            return false;
        }
        
        
    }
    
    
    
    if ($('#user_firstname').val() == "") {
        $('#user_firstname').css({
            border: '1px solid red'
        });
        $('#user_firstname').focus();
        return false;
    }


    if ($('#user_lastname').val() == "") {
        $('#user_lastname').css({
            border: '1px solid red'
        });
        $('#user_lastname').focus();
        return false;
    }

//    if ($('#user_name').val() == "") {
//           $('#user_name').css({
//               border: '1px solid red'
//           });
//           $('#user_name').focus();
//           return false;
//       }

      

    if ($('#user_email').val() == "") {
        $('#user_email').css({
            border: '1px solid red'
        });
        $('#user_email').focus();
        return false;
    }

    if ($('#user_email').val() != "") {
        var email = $('#user_email').val();
        var pattern = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;;
        if (!pattern.test(email)) {
            alert("Please Provide Email Format Eg: test@test.com");
            $('#user_email').css({
                border: '1px solid red'
            });
            $('#user_email').focus();
            return false;
        }
        $('#user_email').css({
            border: ''
        });
    }

    if ($('#user_password').val() == "") {
        $('#user_password').css({
            border: '1px solid red'
        });
        $('#user_password').focus();
        return false;
    }

    if ($('#user_password').val() != "") {
        $('#user_password').css({
            border: ''
        });
    }
    
    if (!$('#tearms_checkbox').is(':checked')) {
        $('#tearms_box').css({
            border: '1px solid red'
        });
        $('#tearms_box').focus();
        return false;
    }

   /* if ($('#user_confirmpass').val() == "") {
        $('#user_confirmpass').css({
            border: '1px solid red'
        });
        $('#user_confirmpass').focus();
        return false;
    }

    if ($('#user_confirmpass').val() != "") {
        $('#user_confirmpass').css({
            border: ''
        });
    }

    if ($('#user_confirmpass').val() != $('#user_password').val())
    {
        alert("Password not matched");
        $('#user_confirmpass').css({
                    border: '1px solid red'
                });
                $('#user_confirmpass').focus();
                return false;
    }*/
    else {
        return true;
    }


    }




function form_acc_details() {

if ($('#first_name').val() == "") {
        $('#first_name').css({
            border: '1px solid red'
        });
        $('#first_name').focus();
        return false;
    }
    if ($('#first_name').val() != "") {
        $('#first_name').css({
            border: ''
        });
    }

    if ($('#last_name').val() == "") {
            $('#last_name').css({
                border: '1px solid red'
            });
            $('#last_name').focus();
            return false;
        }
        if ($('#last_name').val() != "") {
            $('#last_name').css({
                border: ''
            });
        }
    if ($('#email').val() == "") {
        $('#email').css({
            border: '1px solid red'
        });
        $('#email').focus();
        return false;
    }
    if ($('#email').val() != "") {
        $('#email').css({
            border: ''
        });
    }

   /* if ($('#exist_pw').val() == "") {
        $('#exist_pw').css({
            border: '1px solid red'
        });
        $('#exist_pw').focus();
        return false;
    }
    if ($('#exist_pw').val() != "") {
        $('#exist_pw').css({
            border: ''
        });
    }

    if ($('#new_pw').val() == "") {
        $('#new_pw').css({
            border: '1px solid red'
        });
        $('#new_pw').focus();
        return false;
    }
    if ($('#new_pw').val() != "") {
        $('#new_pw').css({
            border: ''
        });
    }*/

    if ($('#new_confirm_pw').val() == "") {
        $('#new_confirm_pw').css({
            border: '1px solid red'
        });
        $('#new_confirm_pw').focus();
        return false;
    }

    /*if ($('#new_confirm_pw').val() != "") {
        if( ($('#new_pw').val()) == ($('#new_confirm_pw').val()) ){
            $('#new_confirm_pw').css({
                border: ''
            });
        } else {
            $('#new_confirm_pw').css({
                border: '1px solid red'
            });
            $('#new_confirm_pw').focus();
            return false;
        }
    }*/

}

function form_profile() {



    if ($('#user_name').val() == "") {
        $('#user_name').css({
            border: '1px solid red'
        });
        $('#user_name').focus();
        return false;
    }

    if ($('#user_name').val() != "") {
        $('#user_name').css({
            border: ''
        });
    }
   if ($('#firstname').val() == "") {
        $('#firstname').css({
            border: '1px solid red'
        });
        $('#firstname').focus();
        return false;
    }

    if ($('#firstname').val() != "") {
        $('#firstname').css({
            border: ''
        });
    }

    if ($('#lastname').val() == "") {
        $('#lastname').css({
            border: '1px solid red'
        });
        $('#lastname').focus();
        return false;
    }

    if ($('#lastname').val() != "") {
        $('#lastname').css({
            border: ''
        });
    }
    /*if ($('#user_occupation').val() == "") {
        $('#user_occupation').css({
            border: '1px solid red'
        });
        $('#user_occupation').focus();
        return false;
    }

    if ($('#user_occupation').val() != "") {
        $('#user_occupation').css({
            border: ''
        });
    }

    if ($('#user_web').val() == "") {
        $('#user_web').css({
            border: '1px solid red'
        });
        $('#user_web').focus();
        return false;
    }

    if ($('#user_web').val() != "") {
        $('#user_web').css({
            border: ''
        });
    }

    if($('#user_confirmpass').val() != $('#user_password').val())
    {
        alert("Password not matched");
        $('#user_confirmpass').css({
                    border: '1px solid red'
                });
                $('#user_confirmpass').focus();
                return false;
    }
*/

   /* if ($('#user_fax').val() == "") {
        $('#user_fax').css({
            border: '1px solid red'
        });
        $('#user_fax').focus();
        return false;
    }

    if ($('#user_fax').val() != "") {
        $('#user_fax').css({
            border: ''
        });
    }*/

    if ($('#user_country').val() == "") {
        $('#user_country').css({
            border: '1px solid red'
        });
        $('#user_country').focus();
        return false;
    }

    if ($('#user_country').val() != "") {
        $('#user_country').css({
            border: ''
        });
    }

    if ($('#user_state').val() == "") {
        $('#user_state').css({
            border: '1px solid red'
        });
        $('#user_state').focus();
        return false;
    }

    if ($('#user_state').val() != "") {
        $('#user_state').css({
            border: ''
        });
    }

    if ($('#user_city').val() == "") {
        $('#user_city').css({
            border: '1px solid red'
        });
        $('#user_city').focus();
        return false;
    }

    if ($('#user_city').val() != "") {
        $('#user_city').css({
            border: ''
        });
    }

    /*if ($('#user_cellphone').val() == "") {
              $('#user_cellphone').css({
                  border: '1px solid red'
              });
              $('#user_cellphone').focus();
              return false;
          }

          if ($('#user_cellphone').val() != "") {
              $('#user_cellphone').css({
                  border: ''
              });
          }*/
    /*if ($('#user_zipcode').val() == "") {
        $('#user_zipcode').css({
            border: '1px solid red'
        });
        $('#user_zipcode').focus();
        return false;
    }

    if ($('#user_zipcode').val() != "") {
        $('#user_zipcode').css({
            border: ''
        });
    }

    if ($('#user_address').val() == "") {
        $('#user_address').css({
            border: '1px solid red'
        });
        $('#user_address').focus();
        return false;
    }

    if ($('#user_address').val() != "") {
        $('#user_address').css({
            border: ''
        });
    }

    if ($('#user_phone').val() == "") {
           $('#user_phone').css({
               border: '1px solid red'
           });
           $('#user_phone').focus();
           return false;
       }

       if ($('#user_phone').val() != "") {
           $('#user_phone').css({
               border: ''
           });
       }*/


    /*if ($('#usersuburb').val() == "") {
        $('#usersuburb').css({
            border: '1px solid red'
        });
        $('#usersuburb').focus();
        return false;
    }

    if ($('#usersuburb').val() != "") {
        $('#usersuburb').css({
            border: ''
        });
    }
    if ($('#user_role').val() == "") {
        $('#user_role').css({
            border: '1px solid red'
        });
        $('#user_role').focus();
        return false;
    }

    if ($('#user_role').val() != "") {
        $('#user_role').css({
            border: ''
        });
    }*/
    else {
        return true;
    }


}
//Login Page
function listItemTask(id,url,obj){
    $('input[name*="item_id"]').val(id);
    $(obj).closest('form').attr('action',url);
    if($(obj).hasClass('go-btn')){
        $('input[name*="page"]').val('0');
        $('input[name*="sort_by"]').val('');
        $('input[name*="flag"]').val('');
    }
    submitform(obj);
}
function pagination(obj){
    $('input[name*="page"]').val($(obj).attr('page_link'));
    submitform(obj);
}
function ordering(sort,flag,obj){
    $('input[name*="sort_by"]').val(sort);
    $('input[name*="flag"]').val(flag);
    submitform(obj);
}
function submitform(obj){
    $(obj).closest('form').submit();
}
function dateFormat(date,format){

}
//Experience Validation
function form_experience() {
    if ($('#job_title').val() == "") {
           $('#job_title').css({
               border: '1px solid red'
           });
           $('#job_title').focus();
           return false;
       }

       if ($('#job_title').val() != "") {
           $('#job_title').css({
               border: ''
           });
       }


    if ($('#company_name').val() == "") {
        $('#company_name').css({
            border: '1px solid red'
        });
        $('#company_name').focus();
        return false;
    }

    if ($('#company_name').val() != "") {
        $('#company_name').css({
            border: ''
        });
    }

    if ($('#job_description').val() == "") {
        $('#job_description').css({
            border: '1px solid red'
        });
        $('#job_description').focus();
        return false;
    }

    if ($('#job_description').val() != "") {
        $('#job_description').css({
            border: ''
        });
    }

    else {
        return true;
    }


    }

//Skills Form
function form_skill() {
    if ($('#skill').val() == "") {
           $('#skill').css({
               border: '1px solid red'
           });
           $('#skill').focus();
           return false;
       }

       if ($('#skill').val() != "") {
           $('#skill').css({
               border: ''
           });
       }
    else {
        return true;
    }


    }
//JOB OPENING  Form
function form_opening() {
    if ($('#job_title').val() == "") {
           $('#job_title').css({
               border: '1px solid red'
           });
           $('#job_title').focus();
           return false;
       }

       if ($('#job_title').val() != "") {
           $('#job_title').css({
               border: ''
           });
       }


    if ($('#job_location').val() == "") {
        $('#job_location').css({
            border: '1px solid red'
        });
        $('#job_location').focus();
        return false;
    }

    if ($('#job_location').val() != "") {
        $('#job_location').css({
            border: ''
        });
    }

    if ($('#skill_level').val() == "") {
        $('#skill_level').css({
            border: '1px solid red'
        });
        $('#skill_level').focus();
        return false;
    }

    if ($('#skill_level').val() != "") {
        $('#skill_level').css({
            border: ''
        });
    }
    if ($('#job_quantity').val() == "") {
            $('#job_quantity').css({
                border: '1px solid red'
            });
            $('#job_quantity').focus();
            return false;
        }

        if ($('#job_quantity').val() != "") {
            if($.isNumeric($('#job_quantity').val())==true)
            {
            $('#job_quantity').css({
                border: ''
            });
            }
            else{
                $('#job_quantity').css({
                    border: '1px solid red'
                });
                $('#job_quantity').val("Numbers Only");
                $('#job_quantity').focus();
                return false;
            }
        }
    if ($('#job_start').val() == "") {
            $('#job_start').css({
                border: '1px solid red'
            });
            $('#job_start').focus();
            return false;
        }

        if ($('#job_start').val() != "") {
            $('#job_start').css({
                border: ''
            });
        }
    if ($('#job_end').val() == "") {
            $('#job_end').css({
                border: '1px solid red'
            });
            $('#job_end').focus();
            return false;
        }

        if ($('#job_end').val() != "") {
            $('#job_end').css({
                border: ''
            });
        }
    if ($('#job_description').val() == "") {
                $('#job_description').css({
                    border: '1px solid red'
                });
                $('#job_description').focus();
                return false;
            }

            if ($('#job_description').val() != "") {
                $('#job_description').css({
                    border: ''
                });
            }
    else {
        return true;
    }


    }



function form_reviewer() {

    if ($('#user_firstname').val() == "") {
        $('#user_firstname').css({
            border: '1px solid red'
        });
        $('#user_firstname').focus();
        return false;
    }

    if ($('#user_firstname').val() != "") {
        $('#user_firstname').css({
            border: ''
        });
    }

    if ($('#user_lastname').val() == "") {
        $('#user_lastname').css({
            border: '1px solid red'
        });
        $('#user_lastname').focus();
        return false;
    }

    if ($('#user_lastname').val() != "") {
        $('#user_lastname').css({
            border: ''
        });
    }

    if ($('#user_email').val() == "") {
           $('#user_email').css({
               border: '1px solid red'
           });
           $('#user_email').focus();
           return false;
       }

    if ($('#user_email').val() != "") {
           var email = $('#user_email').val();
           var pattern = /^[A-z]+([A-z0-9]+[\.\_]?[A-z0-9]+)*@[A-z0-9]{2,}(\.[A-z0-9]{1,}){1,2}[A-z]$/;
           if (!pattern.test(email)) {
               alert("Please Provide Email Format Eg: test@test.com");
               $('#user_email').css({
                   border: '1px solid red'
               });
               $('#user_email').focus();
               return false;
           }
           $('#user_email').css({
               border: ''
           });
       }

    if ($('#user_name').val() == "") {
        $('#user_name').css({
            border: '1px solid red'
        });
        $('#user_name').focus();
        return false;
    }

    if ($('#user_name').val() != "") {
        $('#user_name').css({
            border: ''
        });
    }

    if ($('#user_password').val() == "") {
        $('#user_password').css({
            border: '1px solid red'
        });
        $('#user_password').focus();
        return false;
    }

    if ($('#user_password').val() != "") {
        $('#user_password').css({
            border: ''
        });
    }

    if ($('#user_confirmpass').val() == "") {
        $('#user_confirmpass').css({
            border: '1px solid red'
        });
        $('#user_confirmpass').focus();
        return false;
    }

    if ($('#user_confirmpass').val() != "") {
        $('#user_confirmpass').css({
            border: ''
        });
    }

    if($('#user_confirmpass').val() != $('#user_password').val())
    {
        alert("Password not matched");
        $('#user_confirmpass').css({
                    border: '1px solid red'
                });
                $('#user_confirmpass').focus();
                return false;
    }


   /* if ($('#user_fax').val() == "") {
        $('#user_fax').css({
            border: '1px solid red'
        });
        $('#user_fax').focus();
        return false;
    }

    if ($('#user_fax').val() != "") {
        $('#user_fax').css({
            border: ''
        });
    }*/

    if ($('#user_country').val() == "") {
        $('#user_country').css({
            border: '1px solid red'
        });
        $('#user_country').focus();
        return false;
    }

    if ($('#user_country').val() != "") {
        $('#user_country').css({
            border: ''
        });
    }

    if ($('#user_state').val() == "") {
        $('#user_state').css({
            border: '1px solid red'
        });
        $('#user_state').focus();
        return false;
    }

    if ($('#user_state').val() != "") {
        $('#user_state').css({
            border: ''
        });
    }
    if ($('#user_city').val() == "") {
        $('#user_city').css({
            border: '1px solid red'
        });
        $('#user_city').focus();
        return false;
    }

    if ($('#user_city').val() != "") {
        $('#user_city').css({
            border: ''
        });
    }

    if ($('#user_zipcode').val() == "") {
        $('#user_zipcode').css({
            border: '1px solid red'
        });
        $('#user_zipcode').focus();
        return false;
    }

    if ($('#user_zipcode').val() != "") {
        $('#user_zipcode').css({
            border: ''
        });
    }

    if ($('#user_address').val() == "") {
        $('#user_address').css({
            border: '1px solid red'
        });
        $('#user_address').focus();
        return false;
    }

    if ($('#user_address').val() != "") {
        $('#user_address').css({
            border: ''
        });
    }

    if ($('#user_phone').val() == "") {
           $('#user_phone').css({
               border: '1px solid red'
           });
           $('#user_phone').focus();
           return false;
       }

       if ($('#user_phone').val() != "") {
           $('#user_phone').css({
               border: ''
           });
       }
       if ($('#user_cellphone').val() == "") {
           $('#user_cellphone').css({
               border: '1px solid red'
           });
           $('#user_cellphone').focus();
           return false;
       }

       if ($('#user_cellphone').val() != "") {
           $('#user_cellphone').css({
               border: ''
           });
       }
    else {
        return true;
    }


    }

/*Recommendation Form validation start*/
function recommend_form(){

    if ($('#recom_name').val() == "") {
        $('#recom_name').css({
            border: '1px solid red'
        });
        $('#recom_name').focus();
        return false;
    }

    if ($('#recom_name').val() != "") {
        $('#recom_name').css({
            border: ''
        });
    }

    if ($('#recom_dept').val() == "") {
        $('#recom_dept').css({
            border: '1px solid red'
        });
        $('#recom_dept').focus();
        return false;
    }

    if ($('#recom_dept').val() != "") {
        $('#recom_dept').css({
            border: ''
        });
    }

    if ($('#recom_company').val() == "") {
        $('#recom_company').css({
            border: '1px solid red'
        });
        $('#recom_company').focus();
        return false;
    }

    if ($('#recom_company').val() != "") {
        $('#recom_company').css({
            border: ''
        });
    }

    if ($('#recom').val() == "") {
        $('#recom').css({
            border: '1px solid red'
        });
        $('#recom').focus();
        return false;
    }

    if ($('#recom').val() != "") {
        $('#recom').css({
            border: ''
        });
    }
    else {
        return true;
    }
}
/*Recommendation Form validation end*/