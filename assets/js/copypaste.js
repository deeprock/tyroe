function strip_tags(t, e) {
    e = (((e || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join("");
    var n = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, o = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return t.replace(o, "").replace(n, function (t, n) {
        return e.indexOf("<" + n.toLowerCase() + ">") > -1 ? t : ""
    })
}
function PhotoGroup(t) {
    return this.id = t, this.$el = $("#photo-group-" + this.id), this
}
function Photo(t, e, n) {
    return this.file = e, this.group = t, this.position = n, this.key = Utils.randomStr(36), this.pending = !0, this.uploading = !1, this.recordID = null, this.width = 1, this.height = 1, this
}
!function (t, e) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function (t) {
        if (!t.document)throw new Error("jQuery requires a window with a document");
        return e(t)
    } : e(t)
}("undefined" != typeof window ? window : this, function (t, e) {
    function n(t) {
        var e = t.length, n = ie.type(t);
        return"function" === n || ie.isWindow(t) ? !1 : 1 === t.nodeType && e ? !0 : "array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t
    }

    function o(t, e, n) {
        if (ie.isFunction(e))return ie.grep(t, function (t, o) {
            return!!e.call(t, o, t) !== n
        });
        if (e.nodeType)return ie.grep(t, function (t) {
            return t === e !== n
        });
        if ("string" == typeof e) {
            if (he.test(e))return ie.filter(e, t, n);
            e = ie.filter(e, t)
        }
        return ie.grep(t, function (t) {
            return ie.inArray(t, e) >= 0 !== n
        })
    }

    function r(t, e) {
        do t = t[e]; while (t && 1 !== t.nodeType);
        return t
    }

    function i(t) {
        var e = xe[t] = {};
        return ie.each(t.match(we) || [], function (t, n) {
            e[n] = !0
        }), e
    }

    function a() {
        ge.addEventListener ? (ge.removeEventListener("DOMContentLoaded", s, !1), t.removeEventListener("load", s, !1)) : (ge.detachEvent("onreadystatechange", s), t.detachEvent("onload", s))
    }

    function s() {
        (ge.addEventListener || "load" === event.type || "complete" === ge.readyState) && (a(), ie.ready())
    }

    function l(t, e, n) {
        if (void 0 === n && 1 === t.nodeType) {
            var o = "data-" + e.replace(ke, "-$1").toLowerCase();
            if (n = t.getAttribute(o), "string" == typeof n) {
                try {
                    n = "true" === n ? !0 : "false" === n ? !1 : "null" === n ? null : +n + "" === n ? +n : _e.test(n) ? ie.parseJSON(n) : n
                } catch (r) {
                }
                ie.data(t, e, n)
            } else n = void 0
        }
        return n
    }

    function c(t) {
        var e;
        for (e in t)if (("data" !== e || !ie.isEmptyObject(t[e])) && "toJSON" !== e)return!1;
        return!0
    }

    function u(t, e, n, o) {
        if (ie.acceptData(t)) {
            var r, i, a = ie.expando, s = t.nodeType, l = s ? ie.cache : t, c = s ? t[a] : t[a] && a;
            if (c && l[c] && (o || l[c].data) || void 0 !== n || "string" != typeof e)return c || (c = s ? t[a] = V.pop() || ie.guid++ : a), l[c] || (l[c] = s ? {} : {toJSON:ie.noop}), ("object" == typeof e || "function" == typeof e) && (o ? l[c] = ie.extend(l[c], e) : l[c].data = ie.extend(l[c].data, e)), i = l[c], o || (i.data || (i.data = {}), i = i.data), void 0 !== n && (i[ie.camelCase(e)] = n), "string" == typeof e ? (r = i[e], null == r && (r = i[ie.camelCase(e)])) : r = i, r
        }
    }

    function d(t, e, n) {
        if (ie.acceptData(t)) {
            var o, r, i = t.nodeType, a = i ? ie.cache : t, s = i ? t[ie.expando] : ie.expando;
            if (a[s]) {
                if (e && (o = n ? a[s] : a[s].data)) {
                    ie.isArray(e) ? e = e.concat(ie.map(e, ie.camelCase)) : e in o ? e = [e] : (e = ie.camelCase(e), e = e in o ? [e] : e.split(" ")), r = e.length;
                    for (; r--;)delete o[e[r]];
                    if (n ? !c(o) : !ie.isEmptyObject(o))return
                }
                (n || (delete a[s].data, c(a[s]))) && (i ? ie.cleanData([t], !0) : oe.deleteExpando || a != a.window ? delete a[s] : a[s] = null)
            }
        }
    }

    function f() {
        return!0
    }

    function h() {
        return!1
    }

    function p() {
        try {
            return ge.activeElement
        } catch (t) {
        }
    }

    function g(t) {
        var e = Me.split("|"), n = t.createDocumentFragment();
        if (n.createElement)for (; e.length;)n.createElement(e.pop());
        return n
    }

    function m(t, e) {
        var n, o, r = 0, i = typeof t.getElementsByTagName !== Se ? t.getElementsByTagName(e || "*") : typeof t.querySelectorAll !== Se ? t.querySelectorAll(e || "*") : void 0;
        if (!i)for (i = [], n = t.childNodes || t; null != (o = n[r]); r++)!e || ie.nodeName(o, e) ? i.push(o) : ie.merge(i, m(o, e));
        return void 0 === e || e && ie.nodeName(t, e) ? ie.merge([t], i) : i
    }

    function v(t) {
        Ae.test(t.type) && (t.defaultChecked = t.checked)
    }

    function y(t, e) {
        return ie.nodeName(t, "table") && ie.nodeName(11 !== e.nodeType ? e : e.firstChild, "tr") ? t.getElementsByTagName("tbody")[0] || t.appendChild(t.ownerDocument.createElement("tbody")) : t
    }

    function b(t) {
        return t.type = (null !== ie.find.attr(t, "type")) + "/" + t.type, t
    }

    function w(t) {
        var e = Ve.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"), t
    }

    function x(t, e) {
        for (var n, o = 0; null != (n = t[o]); o++)ie._data(n, "globalEval", !e || ie._data(e[o], "globalEval"))
    }

    function C(t, e) {
        if (1 === e.nodeType && ie.hasData(t)) {
            var n, o, r, i = ie._data(t), a = ie._data(e, i), s = i.events;
            if (s) {
                delete a.handle, a.events = {};
                for (n in s)for (o = 0, r = s[n].length; r > o; o++)ie.event.add(e, n, s[n][o])
            }
            a.data && (a.data = ie.extend({}, a.data))
        }
    }

    function E(t, e) {
        var n, o, r;
        if (1 === e.nodeType) {
            if (n = e.nodeName.toLowerCase(), !oe.noCloneEvent && e[ie.expando]) {
                r = ie._data(e);
                for (o in r.events)ie.removeEvent(e, o, r.handle);
                e.removeAttribute(ie.expando)
            }
            "script" === n && e.text !== t.text ? (b(e).text = t.text, w(e)) : "object" === n ? (e.parentNode && (e.outerHTML = t.outerHTML), oe.html5Clone && t.innerHTML && !ie.trim(e.innerHTML) && (e.innerHTML = t.innerHTML)) : "input" === n && Ae.test(t.type) ? (e.defaultChecked = e.checked = t.checked, e.value !== t.value && (e.value = t.value)) : "option" === n ? e.defaultSelected = e.selected = t.defaultSelected : ("input" === n || "textarea" === n) && (e.defaultValue = t.defaultValue)
        }
    }

    function S(e, n) {
        var o = ie(n.createElement(e)).appendTo(n.body), r = t.getDefaultComputedStyle ? t.getDefaultComputedStyle(o[0]).display : ie.css(o[0], "display");
        return o.detach(), r
    }

    function _(t) {
        var e = ge, n = tn[t];
        return n || (n = S(t, e), "none" !== n && n || (Ze = (Ze || ie("<iframe frameborder='0' width='0' height='0'/>")).appendTo(e.documentElement), e = (Ze[0].contentWindow || Ze[0].contentDocument).document, e.write(), e.close(), n = S(t, e), Ze.detach()), tn[t] = n), n
    }

    function k(t, e) {
        return{get:function () {
            var n = t();
            if (null != n)return n ? (delete this.get, void 0) : (this.get = e).apply(this, arguments)
        }}
    }

    function T(t, e) {
        if (e in t)return e;
        for (var n = e.charAt(0).toUpperCase() + e.slice(1), o = e, r = pn.length; r--;)if (e = pn[r] + n, e in t)return e;
        return o
    }

    function N(t, e) {
        for (var n, o, r, i = [], a = 0, s = t.length; s > a; a++)o = t[a], o.style && (i[a] = ie._data(o, "olddisplay"), n = o.style.display, e ? (i[a] || "none" !== n || (o.style.display = ""), "" === o.style.display && Pe(o) && (i[a] = ie._data(o, "olddisplay", _(o.nodeName)))) : i[a] || (r = Pe(o), (n && "none" !== n || !r) && ie._data(o, "olddisplay", r ? n : ie.css(o, "display"))));
        for (a = 0; s > a; a++)o = t[a], o.style && (e && "none" !== o.style.display && "" !== o.style.display || (o.style.display = e ? i[a] || "" : "none"));
        return t
    }

    function P(t, e, n) {
        var o = un.exec(e);
        return o ? Math.max(0, o[1] - (n || 0)) + (o[2] || "px") : e
    }

    function R(t, e, n, o, r) {
        for (var i = n === (o ? "border" : "content") ? 4 : "width" === e ? 1 : 0, a = 0; 4 > i; i += 2)"margin" === n && (a += ie.css(t, n + Ne[i], !0, r)), o ? ("content" === n && (a -= ie.css(t, "padding" + Ne[i], !0, r)), "margin" !== n && (a -= ie.css(t, "border" + Ne[i] + "Width", !0, r))) : (a += ie.css(t, "padding" + Ne[i], !0, r), "padding" !== n && (a += ie.css(t, "border" + Ne[i] + "Width", !0, r)));
        return a
    }

    function A(t, e, n) {
        var o = !0, r = "width" === e ? t.offsetWidth : t.offsetHeight, i = en(t), a = oe.boxSizing() && "border-box" === ie.css(t, "boxSizing", !1, i);
        if (0 >= r || null == r) {
            if (r = nn(t, e, i), (0 > r || null == r) && (r = t.style[e]), rn.test(r))return r;
            o = a && (oe.boxSizingReliable() || r === t.style[e]), r = parseFloat(r) || 0
        }
        return r + R(t, e, n || (a ? "border" : "content"), o, i) + "px"
    }

    function O(t, e, n, o, r) {
        return new O.prototype.init(t, e, n, o, r)
    }

    function D() {
        return setTimeout(function () {
            gn = void 0
        }), gn = ie.now()
    }

    function I(t, e) {
        var n, o = {height:t}, r = 0;
        for (e = e ? 1 : 0; 4 > r; r += 2 - e)n = Ne[r], o["margin" + n] = o["padding" + n] = t;
        return e && (o.opacity = o.width = t), o
    }

    function $(t, e, n) {
        for (var o, r = (xn[e] || []).concat(xn["*"]), i = 0, a = r.length; a > i; i++)if (o = r[i].call(n, e, t))return o
    }

    function L(t, e, n) {
        var o, r, i, a, s, l, c, u, d = this, f = {}, h = t.style, p = t.nodeType && Pe(t), g = ie._data(t, "fxshow");
        n.queue || (s = ie._queueHooks(t, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function () {
            s.unqueued || l()
        }), s.unqueued++, d.always(function () {
            d.always(function () {
                s.unqueued--, ie.queue(t, "fx").length || s.empty.fire()
            })
        })), 1 === t.nodeType && ("height"in e || "width"in e) && (n.overflow = [h.overflow, h.overflowX, h.overflowY], c = ie.css(t, "display"), u = _(t.nodeName), "none" === c && (c = u), "inline" === c && "none" === ie.css(t, "float") && (oe.inlineBlockNeedsLayout && "inline" !== u ? h.zoom = 1 : h.display = "inline-block")), n.overflow && (h.overflow = "hidden", oe.shrinkWrapBlocks() || d.always(function () {
            h.overflow = n.overflow[0], h.overflowX = n.overflow[1], h.overflowY = n.overflow[2]
        }));
        for (o in e)if (r = e[o], vn.exec(r)) {
            if (delete e[o], i = i || "toggle" === r, r === (p ? "hide" : "show")) {
                if ("show" !== r || !g || void 0 === g[o])continue;
                p = !0
            }
            f[o] = g && g[o] || ie.style(t, o)
        }
        if (!ie.isEmptyObject(f)) {
            g ? "hidden"in g && (p = g.hidden) : g = ie._data(t, "fxshow", {}), i && (g.hidden = !p), p ? ie(t).show() : d.done(function () {
                ie(t).hide()
            }), d.done(function () {
                var e;
                ie._removeData(t, "fxshow");
                for (e in f)ie.style(t, e, f[e])
            });
            for (o in f)a = $(p ? g[o] : 0, o, d), o in g || (g[o] = a.start, p && (a.end = a.start, a.start = "width" === o || "height" === o ? 1 : 0))
        }
    }

    function M(t, e) {
        var n, o, r, i, a;
        for (n in t)if (o = ie.camelCase(n), r = e[o], i = t[n], ie.isArray(i) && (r = i[1], i = t[n] = i[0]), n !== o && (t[o] = i, delete t[n]), a = ie.cssHooks[o], a && "expand"in a) {
            i = a.expand(i), delete t[o];
            for (n in i)n in t || (t[n] = i[n], e[n] = r)
        } else e[o] = r
    }

    function B(t, e, n) {
        var o, r, i = 0, a = wn.length, s = ie.Deferred().always(function () {
            delete l.elem
        }), l = function () {
            if (r)return!1;
            for (var e = gn || D(), n = Math.max(0, c.startTime + c.duration - e), o = n / c.duration || 0, i = 1 - o, a = 0, l = c.tweens.length; l > a; a++)c.tweens[a].run(i);
            return s.notifyWith(t, [c, i, n]), 1 > i && l ? n : (s.resolveWith(t, [c]), !1)
        }, c = s.promise({elem:t, props:ie.extend({}, e), opts:ie.extend(!0, {specialEasing:{}}, n), originalProperties:e, originalOptions:n, startTime:gn || D(), duration:n.duration, tweens:[], createTween:function (e, n) {
            var o = ie.Tween(t, c.opts, e, n, c.opts.specialEasing[e] || c.opts.easing);
            return c.tweens.push(o), o
        }, stop:function (e) {
            var n = 0, o = e ? c.tweens.length : 0;
            if (r)return this;
            for (r = !0; o > n; n++)c.tweens[n].run(1);
            return e ? s.resolveWith(t, [c, e]) : s.rejectWith(t, [c, e]), this
        }}), u = c.props;
        for (M(u, c.opts.specialEasing); a > i; i++)if (o = wn[i].call(c, t, u, c.opts))return o;
        return ie.map(u, $, c), ie.isFunction(c.opts.start) && c.opts.start.call(t, c), ie.fx.timer(ie.extend(l, {elem:t, anim:c, queue:c.opts.queue})), c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always)
    }

    function F(t) {
        return function (e, n) {
            "string" != typeof e && (n = e, e = "*");
            var o, r = 0, i = e.toLowerCase().match(we) || [];
            if (ie.isFunction(n))for (; o = i[r++];)"+" === o.charAt(0) ? (o = o.slice(1) || "*", (t[o] = t[o] || []).unshift(n)) : (t[o] = t[o] || []).push(n)
        }
    }

    function j(t, e, n, o) {
        function r(s) {
            var l;
            return i[s] = !0, ie.each(t[s] || [], function (t, s) {
                var c = s(e, n, o);
                return"string" != typeof c || a || i[c] ? a ? !(l = c) : void 0 : (e.dataTypes.unshift(c), r(c), !1)
            }), l
        }

        var i = {}, a = t === zn;
        return r(e.dataTypes[0]) || !i["*"] && r("*")
    }

    function H(t, e) {
        var n, o, r = ie.ajaxSettings.flatOptions || {};
        for (o in e)void 0 !== e[o] && ((r[o] ? t : n || (n = {}))[o] = e[o]);
        return n && ie.extend(!0, t, n), t
    }

    function W(t, e, n) {
        for (var o, r, i, a, s = t.contents, l = t.dataTypes; "*" === l[0];)l.shift(), void 0 === r && (r = t.mimeType || e.getResponseHeader("Content-Type"));
        if (r)for (a in s)if (s[a] && s[a].test(r)) {
            l.unshift(a);
            break
        }
        if (l[0]in n)i = l[0]; else {
            for (a in n) {
                if (!l[0] || t.converters[a + " " + l[0]]) {
                    i = a;
                    break
                }
                o || (o = a)
            }
            i = i || o
        }
        return i ? (i !== l[0] && l.unshift(i), n[i]) : void 0
    }

    function q(t, e, n, o) {
        var r, i, a, s, l, c = {}, u = t.dataTypes.slice();
        if (u[1])for (a in t.converters)c[a.toLowerCase()] = t.converters[a];
        for (i = u.shift(); i;)if (t.responseFields[i] && (n[t.responseFields[i]] = e), !l && o && t.dataFilter && (e = t.dataFilter(e, t.dataType)), l = i, i = u.shift())if ("*" === i)i = l; else if ("*" !== l && l !== i) {
            if (a = c[l + " " + i] || c["* " + i], !a)for (r in c)if (s = r.split(" "), s[1] === i && (a = c[l + " " + s[0]] || c["* " + s[0]])) {
                a === !0 ? a = c[r] : c[r] !== !0 && (i = s[0], u.unshift(s[1]));
                break
            }
            if (a !== !0)if (a && t["throws"])e = a(e); else try {
                e = a(e)
            } catch (d) {
                return{state:"parsererror", error:a ? d : "No conversion from " + l + " to " + i}
            }
        }
        return{state:"success", data:e}
    }

    function X(t, e, n, o) {
        var r;
        if (ie.isArray(e))ie.each(e, function (e, r) {
            n || Yn.test(t) ? o(t, r) : X(t + "[" + ("object" == typeof r ? e : "") + "]", r, n, o)
        }); else if (n || "object" !== ie.type(e))o(t, e); else for (r in e)X(t + "[" + r + "]", e[r], n, o)
    }

    function z() {
        try {
            return new t.XMLHttpRequest
        } catch (e) {
        }
    }

    function G() {
        try {
            return new t.ActiveXObject("Microsoft.XMLHTTP")
        } catch (e) {
        }
    }

    function U(t) {
        return ie.isWindow(t) ? t : 9 === t.nodeType ? t.defaultView || t.parentWindow : !1
    }

    var V = [], Y = V.slice, Q = V.concat, K = V.push, J = V.indexOf, Z = {}, te = Z.toString, ee = Z.hasOwnProperty, ne = "".trim, oe = {}, re = "1.11.0", ie = function (t, e) {
        return new ie.fn.init(t, e)
    }, ae = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, se = /^-ms-/, le = /-([\da-z])/gi, ce = function (t, e) {
        return e.toUpperCase()
    };
    ie.fn = ie.prototype = {jquery:re, constructor:ie, selector:"", length:0, toArray:function () {
        return Y.call(this)
    }, get:function (t) {
        return null != t ? 0 > t ? this[t + this.length] : this[t] : Y.call(this)
    }, pushStack:function (t) {
        var e = ie.merge(this.constructor(), t);
        return e.prevObject = this, e.context = this.context, e
    }, each:function (t, e) {
        return ie.each(this, t, e)
    }, map:function (t) {
        return this.pushStack(ie.map(this, function (e, n) {
            return t.call(e, n, e)
        }))
    }, slice:function () {
        return this.pushStack(Y.apply(this, arguments))
    }, first:function () {
        return this.eq(0)
    }, last:function () {
        return this.eq(-1)
    }, eq:function (t) {
        var e = this.length, n = +t + (0 > t ? e : 0);
        return this.pushStack(n >= 0 && e > n ? [this[n]] : [])
    }, end:function () {
        return this.prevObject || this.constructor(null)
    }, push:K, sort:V.sort, splice:V.splice}, ie.extend = ie.fn.extend = function () {
        var t, e, n, o, r, i, a = arguments[0] || {}, s = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof a && (c = a, a = arguments[s] || {}, s++), "object" == typeof a || ie.isFunction(a) || (a = {}), s === l && (a = this, s--); l > s; s++)if (null != (r = arguments[s]))for (o in r)t = a[o], n = r[o], a !== n && (c && n && (ie.isPlainObject(n) || (e = ie.isArray(n))) ? (e ? (e = !1, i = t && ie.isArray(t) ? t : []) : i = t && ie.isPlainObject(t) ? t : {}, a[o] = ie.extend(c, i, n)) : void 0 !== n && (a[o] = n));
        return a
    }, ie.extend({expando:"jQuery" + (re + Math.random()).replace(/\D/g, ""), isReady:!0, error:function (t) {
        throw new Error(t)
    }, noop:function () {
    }, isFunction:function (t) {
        return"function" === ie.type(t)
    }, isArray:Array.isArray || function (t) {
        return"array" === ie.type(t)
    }, isWindow:function (t) {
        return null != t && t == t.window
    }, isNumeric:function (t) {
        return t - parseFloat(t) >= 0
    }, isEmptyObject:function (t) {
        var e;
        for (e in t)return!1;
        return!0
    }, isPlainObject:function (t) {
        var e;
        if (!t || "object" !== ie.type(t) || t.nodeType || ie.isWindow(t))return!1;
        try {
            if (t.constructor && !ee.call(t, "constructor") && !ee.call(t.constructor.prototype, "isPrototypeOf"))return!1
        } catch (n) {
            return!1
        }
        if (oe.ownLast)for (e in t)return ee.call(t, e);
        for (e in t);
        return void 0 === e || ee.call(t, e)
    }, type:function (t) {
        return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? Z[te.call(t)] || "object" : typeof t
    }, globalEval:function (e) {
        e && ie.trim(e) && (t.execScript || function (e) {
            t.eval.call(t, e)
        })(e)
    }, camelCase:function (t) {
        return t.replace(se, "ms-").replace(le, ce)
    }, nodeName:function (t, e) {
        return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
    }, each:function (t, e, o) {
        var r, i = 0, a = t.length, s = n(t);
        if (o) {
            if (s)for (; a > i && (r = e.apply(t[i], o), r !== !1); i++); else for (i in t)if (r = e.apply(t[i], o), r === !1)break
        } else if (s)for (; a > i && (r = e.call(t[i], i, t[i]), r !== !1); i++); else for (i in t)if (r = e.call(t[i], i, t[i]), r === !1)break;
        return t
    }, trim:ne && !ne.call("ï»¿Â ") ? function (t) {
        return null == t ? "" : ne.call(t)
    } : function (t) {
        return null == t ? "" : (t + "").replace(ae, "")
    }, makeArray:function (t, e) {
        var o = e || [];
        return null != t && (n(Object(t)) ? ie.merge(o, "string" == typeof t ? [t] : t) : K.call(o, t)), o
    }, inArray:function (t, e, n) {
        var o;
        if (e) {
            if (J)return J.call(e, t, n);
            for (o = e.length, n = n ? 0 > n ? Math.max(0, o + n) : n : 0; o > n; n++)if (n in e && e[n] === t)return n
        }
        return-1
    }, merge:function (t, e) {
        for (var n = +e.length, o = 0, r = t.length; n > o;)t[r++] = e[o++];
        if (n !== n)for (; void 0 !== e[o];)t[r++] = e[o++];
        return t.length = r, t
    }, grep:function (t, e, n) {
        for (var o, r = [], i = 0, a = t.length, s = !n; a > i; i++)o = !e(t[i], i), o !== s && r.push(t[i]);
        return r
    }, map:function (t, e, o) {
        var r, i = 0, a = t.length, s = n(t), l = [];
        if (s)for (; a > i; i++)r = e(t[i], i, o), null != r && l.push(r); else for (i in t)r = e(t[i], i, o), null != r && l.push(r);
        return Q.apply([], l)
    }, guid:1, proxy:function (t, e) {
        var n, o, r;
        return"string" == typeof e && (r = t[e], e = t, t = r), ie.isFunction(t) ? (n = Y.call(arguments, 2), o = function () {
            return t.apply(e || this, n.concat(Y.call(arguments)))
        }, o.guid = t.guid = t.guid || ie.guid++, o) : void 0
    }, now:function () {
        return+new Date
    }, support:oe}), ie.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function (t, e) {
        Z["[object " + e + "]"] = e.toLowerCase()
    });
    var ue = function (t) {
        function e(t, e, n, o) {
            var r, i, a, s, l, c, d, p, g, m;
            if ((e ? e.ownerDocument || e : j) !== O && A(e), e = e || O, n = n || [], !t || "string" != typeof t)return n;
            if (1 !== (s = e.nodeType) && 9 !== s)return[];
            if (I && !o) {
                if (r = ye.exec(t))if (a = r[1]) {
                    if (9 === s) {
                        if (i = e.getElementById(a), !i || !i.parentNode)return n;
                        if (i.id === a)return n.push(i), n
                    } else if (e.ownerDocument && (i = e.ownerDocument.getElementById(a)) && B(e, i) && i.id === a)return n.push(i), n
                } else {
                    if (r[2])return Z.apply(n, e.getElementsByTagName(t)), n;
                    if ((a = r[3]) && E.getElementsByClassName && e.getElementsByClassName)return Z.apply(n, e.getElementsByClassName(a)), n
                }
                if (E.qsa && (!$ || !$.test(t))) {
                    if (p = d = F, g = e, m = 9 === s && t, 1 === s && "object" !== e.nodeName.toLowerCase()) {
                        for (c = f(t), (d = e.getAttribute("id")) ? p = d.replace(we, "\\$&") : e.setAttribute("id", p), p = "[id='" + p + "'] ", l = c.length; l--;)c[l] = p + h(c[l]);
                        g = be.test(t) && u(e.parentNode) || e, m = c.join(",")
                    }
                    if (m)try {
                        return Z.apply(n, g.querySelectorAll(m)), n
                    } catch (v) {
                    } finally {
                        d || e.removeAttribute("id")
                    }
                }
            }
            return x(t.replace(le, "$1"), e, n, o)
        }

        function n() {
            function t(n, o) {
                return e.push(n + " ") > S.cacheLength && delete t[e.shift()], t[n + " "] = o
            }

            var e = [];
            return t
        }

        function o(t) {
            return t[F] = !0, t
        }

        function r(t) {
            var e = O.createElement("div");
            try {
                return!!t(e)
            } catch (n) {
                return!1
            } finally {
                e.parentNode && e.parentNode.removeChild(e), e = null
            }
        }

        function i(t, e) {
            for (var n = t.split("|"), o = t.length; o--;)S.attrHandle[n[o]] = e
        }

        function a(t, e) {
            var n = e && t, o = n && 1 === t.nodeType && 1 === e.nodeType && (~e.sourceIndex || V) - (~t.sourceIndex || V);
            if (o)return o;
            if (n)for (; n = n.nextSibling;)if (n === e)return-1;
            return t ? 1 : -1
        }

        function s(t) {
            return function (e) {
                var n = e.nodeName.toLowerCase();
                return"input" === n && e.type === t
            }
        }

        function l(t) {
            return function (e) {
                var n = e.nodeName.toLowerCase();
                return("input" === n || "button" === n) && e.type === t
            }
        }

        function c(t) {
            return o(function (e) {
                return e = +e, o(function (n, o) {
                    for (var r, i = t([], n.length, e), a = i.length; a--;)n[r = i[a]] && (n[r] = !(o[r] = n[r]))
                })
            })
        }

        function u(t) {
            return t && typeof t.getElementsByTagName !== U && t
        }

        function d() {
        }

        function f(t, n) {
            var o, r, i, a, s, l, c, u = X[t + " "];
            if (u)return n ? 0 : u.slice(0);
            for (s = t, l = [], c = S.preFilter; s;) {
                (!o || (r = ce.exec(s))) && (r && (s = s.slice(r[0].length) || s), l.push(i = [])), o = !1, (r = ue.exec(s)) && (o = r.shift(), i.push({value:o, type:r[0].replace(le, " ")}), s = s.slice(o.length));
                for (a in S.filter)!(r = pe[a].exec(s)) || c[a] && !(r = c[a](r)) || (o = r.shift(), i.push({value:o, type:a, matches:r}), s = s.slice(o.length));
                if (!o)break
            }
            return n ? s.length : s ? e.error(t) : X(t, l).slice(0)
        }

        function h(t) {
            for (var e = 0, n = t.length, o = ""; n > e; e++)o += t[e].value;
            return o
        }

        function p(t, e, n) {
            var o = e.dir, r = n && "parentNode" === o, i = W++;
            return e.first ? function (e, n, i) {
                for (; e = e[o];)if (1 === e.nodeType || r)return t(e, n, i)
            } : function (e, n, a) {
                var s, l, c = [H, i];
                if (a) {
                    for (; e = e[o];)if ((1 === e.nodeType || r) && t(e, n, a))return!0
                } else for (; e = e[o];)if (1 === e.nodeType || r) {
                    if (l = e[F] || (e[F] = {}), (s = l[o]) && s[0] === H && s[1] === i)return c[2] = s[2];
                    if (l[o] = c, c[2] = t(e, n, a))return!0
                }
            }
        }

        function g(t) {
            return t.length > 1 ? function (e, n, o) {
                for (var r = t.length; r--;)if (!t[r](e, n, o))return!1;
                return!0
            } : t[0]
        }

        function m(t, e, n, o, r) {
            for (var i, a = [], s = 0, l = t.length, c = null != e; l > s; s++)(i = t[s]) && (!n || n(i, o, r)) && (a.push(i), c && e.push(s));
            return a
        }

        function v(t, e, n, r, i, a) {
            return r && !r[F] && (r = v(r)), i && !i[F] && (i = v(i, a)), o(function (o, a, s, l) {
                var c, u, d, f = [], h = [], p = a.length, g = o || w(e || "*", s.nodeType ? [s] : s, []), v = !t || !o && e ? g : m(g, f, t, s, l), y = n ? i || (o ? t : p || r) ? [] : a : v;
                if (n && n(v, y, s, l), r)for (c = m(y, h), r(c, [], s, l), u = c.length; u--;)(d = c[u]) && (y[h[u]] = !(v[h[u]] = d));
                if (o) {
                    if (i || t) {
                        if (i) {
                            for (c = [], u = y.length; u--;)(d = y[u]) && c.push(v[u] = d);
                            i(null, y = [], c, l)
                        }
                        for (u = y.length; u--;)(d = y[u]) && (c = i ? ee.call(o, d) : f[u]) > -1 && (o[c] = !(a[c] = d))
                    }
                } else y = m(y === a ? y.splice(p, y.length) : y), i ? i(null, a, y, l) : Z.apply(a, y)
            })
        }

        function y(t) {
            for (var e, n, o, r = t.length, i = S.relative[t[0].type], a = i || S.relative[" "], s = i ? 1 : 0, l = p(function (t) {
                return t === e
            }, a, !0), c = p(function (t) {
                return ee.call(e, t) > -1
            }, a, !0), u = [function (t, n, o) {
                return!i && (o || n !== N) || ((e = n).nodeType ? l(t, n, o) : c(t, n, o))
            }]; r > s; s++)if (n = S.relative[t[s].type])u = [p(g(u), n)]; else {
                if (n = S.filter[t[s].type].apply(null, t[s].matches), n[F]) {
                    for (o = ++s; r > o && !S.relative[t[o].type]; o++);
                    return v(s > 1 && g(u), s > 1 && h(t.slice(0, s - 1).concat({value:" " === t[s - 2].type ? "*" : ""})).replace(le, "$1"), n, o > s && y(t.slice(s, o)), r > o && y(t = t.slice(o)), r > o && h(t))
                }
                u.push(n)
            }
            return g(u)
        }

        function b(t, n) {
            var r = n.length > 0, i = t.length > 0, a = function (o, a, s, l, c) {
                var u, d, f, h = 0, p = "0", g = o && [], v = [], y = N, b = o || i && S.find.TAG("*", c), w = H += null == y ? 1 : Math.random() || .1, x = b.length;
                for (c && (N = a !== O && a); p !== x && null != (u = b[p]); p++) {
                    if (i && u) {
                        for (d = 0; f = t[d++];)if (f(u, a, s)) {
                            l.push(u);
                            break
                        }
                        c && (H = w)
                    }
                    r && ((u = !f && u) && h--, o && g.push(u))
                }
                if (h += p, r && p !== h) {
                    for (d = 0; f = n[d++];)f(g, v, a, s);
                    if (o) {
                        if (h > 0)for (; p--;)g[p] || v[p] || (v[p] = K.call(l));
                        v = m(v)
                    }
                    Z.apply(l, v), c && !o && v.length > 0 && h + n.length > 1 && e.uniqueSort(l)
                }
                return c && (H = w, N = y), g
            };
            return r ? o(a) : a
        }

        function w(t, n, o) {
            for (var r = 0, i = n.length; i > r; r++)e(t, n[r], o);
            return o
        }

        function x(t, e, n, o) {
            var r, i, a, s, l, c = f(t);
            if (!o && 1 === c.length) {
                if (i = c[0] = c[0].slice(0), i.length > 2 && "ID" === (a = i[0]).type && E.getById && 9 === e.nodeType && I && S.relative[i[1].type]) {
                    if (e = (S.find.ID(a.matches[0].replace(xe, Ce), e) || [])[0], !e)return n;
                    t = t.slice(i.shift().value.length)
                }
                for (r = pe.needsContext.test(t) ? 0 : i.length; r-- && (a = i[r], !S.relative[s = a.type]);)if ((l = S.find[s]) && (o = l(a.matches[0].replace(xe, Ce), be.test(i[0].type) && u(e.parentNode) || e))) {
                    if (i.splice(r, 1), t = o.length && h(i), !t)return Z.apply(n, o), n;
                    break
                }
            }
            return T(t, c)(o, e, !I, n, be.test(t) && u(e.parentNode) || e), n
        }

        var C, E, S, _, k, T, N, P, R, A, O, D, I, $, L, M, B, F = "sizzle" + -new Date, j = t.document, H = 0, W = 0, q = n(), X = n(), z = n(), G = function (t, e) {
            return t === e && (R = !0), 0
        }, U = "undefined", V = 1 << 31, Y = {}.hasOwnProperty, Q = [], K = Q.pop, J = Q.push, Z = Q.push, te = Q.slice, ee = Q.indexOf || function (t) {
            for (var e = 0, n = this.length; n > e; e++)if (this[e] === t)return e;
            return-1
        }, ne = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", oe = "[\\x20\\t\\r\\n\\f]", re = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+", ie = re.replace("w", "w#"), ae = "\\[" + oe + "*(" + re + ")" + oe + "*(?:([*^$|!~]?=)" + oe + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + ie + ")|)|)" + oe + "*\\]", se = ":(" + re + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ae.replace(3, 8) + ")*)|.*)\\)|)", le = new RegExp("^" + oe + "+|((?:^|[^\\\\])(?:\\\\.)*)" + oe + "+$", "g"), ce = new RegExp("^" + oe + "*," + oe + "*"), ue = new RegExp("^" + oe + "*([>+~]|" + oe + ")" + oe + "*"), de = new RegExp("=" + oe + "*([^\\]'\"]*?)" + oe + "*\\]", "g"), fe = new RegExp(se), he = new RegExp("^" + ie + "$"), pe = {ID:new RegExp("^#(" + re + ")"), CLASS:new RegExp("^\\.(" + re + ")"), TAG:new RegExp("^(" + re.replace("w", "w*") + ")"), ATTR:new RegExp("^" + ae), PSEUDO:new RegExp("^" + se), CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + oe + "*(even|odd|(([+-]|)(\\d*)n|)" + oe + "*(?:([+-]|)" + oe + "*(\\d+)|))" + oe + "*\\)|)", "i"), bool:new RegExp("^(?:" + ne + ")$", "i"), needsContext:new RegExp("^" + oe + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + oe + "*((?:-\\d)?\\d*)" + oe + "*\\)|)(?=[^-]|$)", "i")}, ge = /^(?:input|select|textarea|button)$/i, me = /^h\d$/i, ve = /^[^{]+\{\s*\[native \w/, ye = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, be = /[+~]/, we = /'|\\/g, xe = new RegExp("\\\\([\\da-f]{1,6}" + oe + "?|(" + oe + ")|.)", "ig"), Ce = function (t, e, n) {
            var o = "0x" + e - 65536;
            return o !== o || n ? e : 0 > o ? String.fromCharCode(o + 65536) : String.fromCharCode(55296 | o >> 10, 56320 | 1023 & o)
        };
        try {
            Z.apply(Q = te.call(j.childNodes), j.childNodes), Q[j.childNodes.length].nodeType
        } catch (Ee) {
            Z = {apply:Q.length ? function (t, e) {
                J.apply(t, te.call(e))
            } : function (t, e) {
                for (var n = t.length, o = 0; t[n++] = e[o++];);
                t.length = n - 1
            }}
        }
        E = e.support = {}, k = e.isXML = function (t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return e ? "HTML" !== e.nodeName : !1
        }, A = e.setDocument = function (t) {
            var e, n = t ? t.ownerDocument || t : j, o = n.defaultView;
            return n !== O && 9 === n.nodeType && n.documentElement ? (O = n, D = n.documentElement, I = !k(n), o && o !== o.top && (o.addEventListener ? o.addEventListener("unload", function () {
                A()
            }, !1) : o.attachEvent && o.attachEvent("onunload", function () {
                A()
            })), E.attributes = r(function (t) {
                return t.className = "i", !t.getAttribute("className")
            }), E.getElementsByTagName = r(function (t) {
                return t.appendChild(n.createComment("")), !t.getElementsByTagName("*").length
            }), E.getElementsByClassName = ve.test(n.getElementsByClassName) && r(function (t) {
                return t.innerHTML = "<div class='a'></div><div class='a i'></div>", t.firstChild.className = "i", 2 === t.getElementsByClassName("i").length
            }), E.getById = r(function (t) {
                return D.appendChild(t).id = F, !n.getElementsByName || !n.getElementsByName(F).length
            }), E.getById ? (S.find.ID = function (t, e) {
                if (typeof e.getElementById !== U && I) {
                    var n = e.getElementById(t);
                    return n && n.parentNode ? [n] : []
                }
            }, S.filter.ID = function (t) {
                var e = t.replace(xe, Ce);
                return function (t) {
                    return t.getAttribute("id") === e
                }
            }) : (delete S.find.ID, S.filter.ID = function (t) {
                var e = t.replace(xe, Ce);
                return function (t) {
                    var n = typeof t.getAttributeNode !== U && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }), S.find.TAG = E.getElementsByTagName ? function (t, e) {
                return typeof e.getElementsByTagName !== U ? e.getElementsByTagName(t) : void 0
            } : function (t, e) {
                var n, o = [], r = 0, i = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = i[r++];)1 === n.nodeType && o.push(n);
                    return o
                }
                return i
            }, S.find.CLASS = E.getElementsByClassName && function (t, e) {
                return typeof e.getElementsByClassName !== U && I ? e.getElementsByClassName(t) : void 0
            }, L = [], $ = [], (E.qsa = ve.test(n.querySelectorAll)) && (r(function (t) {
                t.innerHTML = "<select t=''><option selected=''></option></select>", t.querySelectorAll("[t^='']").length && $.push("[*^$]=" + oe + "*(?:''|\"\")"), t.querySelectorAll("[selected]").length || $.push("\\[" + oe + "*(?:value|" + ne + ")"), t.querySelectorAll(":checked").length || $.push(":checked")
            }), r(function (t) {
                var e = n.createElement("input");
                e.setAttribute("type", "hidden"), t.appendChild(e).setAttribute("name", "D"), t.querySelectorAll("[name=d]").length && $.push("name" + oe + "*[*^$|!~]?="), t.querySelectorAll(":enabled").length || $.push(":enabled", ":disabled"), t.querySelectorAll("*,:x"), $.push(",.*:")
            })), (E.matchesSelector = ve.test(M = D.webkitMatchesSelector || D.mozMatchesSelector || D.oMatchesSelector || D.msMatchesSelector)) && r(function (t) {
                E.disconnectedMatch = M.call(t, "div"), M.call(t, "[s!='']:x"), L.push("!=", se)
            }), $ = $.length && new RegExp($.join("|")), L = L.length && new RegExp(L.join("|")), e = ve.test(D.compareDocumentPosition), B = e || ve.test(D.contains) ? function (t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t, o = e && e.parentNode;
                return t === o || !(!o || 1 !== o.nodeType || !(n.contains ? n.contains(o) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(o)))
            } : function (t, e) {
                if (e)for (; e = e.parentNode;)if (e === t)return!0;
                return!1
            }, G = e ? function (t, e) {
                if (t === e)return R = !0, 0;
                var o = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return o ? o : (o = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1, 1 & o || !E.sortDetached && e.compareDocumentPosition(t) === o ? t === n || t.ownerDocument === j && B(j, t) ? -1 : e === n || e.ownerDocument === j && B(j, e) ? 1 : P ? ee.call(P, t) - ee.call(P, e) : 0 : 4 & o ? -1 : 1)
            } : function (t, e) {
                if (t === e)return R = !0, 0;
                var o, r = 0, i = t.parentNode, s = e.parentNode, l = [t], c = [e];
                if (!i || !s)return t === n ? -1 : e === n ? 1 : i ? -1 : s ? 1 : P ? ee.call(P, t) - ee.call(P, e) : 0;
                if (i === s)return a(t, e);
                for (o = t; o = o.parentNode;)l.unshift(o);
                for (o = e; o = o.parentNode;)c.unshift(o);
                for (; l[r] === c[r];)r++;
                return r ? a(l[r], c[r]) : l[r] === j ? -1 : c[r] === j ? 1 : 0
            }, n) : O
        }, e.matches = function (t, n) {
            return e(t, null, null, n)
        }, e.matchesSelector = function (t, n) {
            if ((t.ownerDocument || t) !== O && A(t), n = n.replace(de, "='$1']"), !(!E.matchesSelector || !I || L && L.test(n) || $ && $.test(n)))try {
                var o = M.call(t, n);
                if (o || E.disconnectedMatch || t.document && 11 !== t.document.nodeType)return o
            } catch (r) {
            }
            return e(n, O, null, [t]).length > 0
        }, e.contains = function (t, e) {
            return(t.ownerDocument || t) !== O && A(t), B(t, e)
        }, e.attr = function (t, e) {
            (t.ownerDocument || t) !== O && A(t);
            var n = S.attrHandle[e.toLowerCase()], o = n && Y.call(S.attrHandle, e.toLowerCase()) ? n(t, e, !I) : void 0;
            return void 0 !== o ? o : E.attributes || !I ? t.getAttribute(e) : (o = t.getAttributeNode(e)) && o.specified ? o.value : null
        }, e.error = function (t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }, e.uniqueSort = function (t) {
            var e, n = [], o = 0, r = 0;
            if (R = !E.detectDuplicates, P = !E.sortStable && t.slice(0), t.sort(G), R) {
                for (; e = t[r++];)e === t[r] && (o = n.push(r));
                for (; o--;)t.splice(n[o], 1)
            }
            return P = null, t
        }, _ = e.getText = function (t) {
            var e, n = "", o = 0, r = t.nodeType;
            if (r) {
                if (1 === r || 9 === r || 11 === r) {
                    if ("string" == typeof t.textContent)return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling)n += _(t)
                } else if (3 === r || 4 === r)return t.nodeValue
            } else for (; e = t[o++];)n += _(e);
            return n
        }, S = e.selectors = {cacheLength:50, createPseudo:o, match:pe, attrHandle:{}, find:{}, relative:{">":{dir:"parentNode", first:!0}, " ":{dir:"parentNode"}, "+":{dir:"previousSibling", first:!0}, "~":{dir:"previousSibling"}}, preFilter:{ATTR:function (t) {
            return t[1] = t[1].replace(xe, Ce), t[3] = (t[4] || t[5] || "").replace(xe, Ce), "~=" === t[2] && (t[3] = " " + t[3] + " "), t.slice(0, 4)
        }, CHILD:function (t) {
            return t[1] = t[1].toLowerCase(), "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]), t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])), t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]), t
        }, PSEUDO:function (t) {
            var e, n = !t[5] && t[2];
            return pe.CHILD.test(t[0]) ? null : (t[3] && void 0 !== t[4] ? t[2] = t[4] : n && fe.test(n) && (e = f(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e), t[2] = n.slice(0, e)), t.slice(0, 3))
        }}, filter:{TAG:function (t) {
            var e = t.replace(xe, Ce).toLowerCase();
            return"*" === t ? function () {
                return!0
            } : function (t) {
                return t.nodeName && t.nodeName.toLowerCase() === e
            }
        }, CLASS:function (t) {
            var e = q[t + " "];
            return e || (e = new RegExp("(^|" + oe + ")" + t + "(" + oe + "|$)")) && q(t, function (t) {
                return e.test("string" == typeof t.className && t.className || typeof t.getAttribute !== U && t.getAttribute("class") || "")
            })
        }, ATTR:function (t, n, o) {
            return function (r) {
                var i = e.attr(r, t);
                return null == i ? "!=" === n : n ? (i += "", "=" === n ? i === o : "!=" === n ? i !== o : "^=" === n ? o && 0 === i.indexOf(o) : "*=" === n ? o && i.indexOf(o) > -1 : "$=" === n ? o && i.slice(-o.length) === o : "~=" === n ? (" " + i + " ").indexOf(o) > -1 : "|=" === n ? i === o || i.slice(0, o.length + 1) === o + "-" : !1) : !0
            }
        }, CHILD:function (t, e, n, o, r) {
            var i = "nth" !== t.slice(0, 3), a = "last" !== t.slice(-4), s = "of-type" === e;
            return 1 === o && 0 === r ? function (t) {
                return!!t.parentNode
            } : function (e, n, l) {
                var c, u, d, f, h, p, g = i !== a ? "nextSibling" : "previousSibling", m = e.parentNode, v = s && e.nodeName.toLowerCase(), y = !l && !s;
                if (m) {
                    if (i) {
                        for (; g;) {
                            for (d = e; d = d[g];)if (s ? d.nodeName.toLowerCase() === v : 1 === d.nodeType)return!1;
                            p = g = "only" === t && !p && "nextSibling"
                        }
                        return!0
                    }
                    if (p = [a ? m.firstChild : m.lastChild], a && y) {
                        for (u = m[F] || (m[F] = {}), c = u[t] || [], h = c[0] === H && c[1], f = c[0] === H && c[2], d = h && m.childNodes[h]; d = ++h && d && d[g] || (f = h = 0) || p.pop();)if (1 === d.nodeType && ++f && d === e) {
                            u[t] = [H, h, f];
                            break
                        }
                    } else if (y && (c = (e[F] || (e[F] = {}))[t]) && c[0] === H)f = c[1]; else for (; (d = ++h && d && d[g] || (f = h = 0) || p.pop()) && ((s ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++f || (y && ((d[F] || (d[F] = {}))[t] = [H, f]), d !== e)););
                    return f -= r, f === o || 0 === f % o && f / o >= 0
                }
            }
        }, PSEUDO:function (t, n) {
            var r, i = S.pseudos[t] || S.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
            return i[F] ? i(n) : i.length > 1 ? (r = [t, t, "", n], S.setFilters.hasOwnProperty(t.toLowerCase()) ? o(function (t, e) {
                for (var o, r = i(t, n), a = r.length; a--;)o = ee.call(t, r[a]), t[o] = !(e[o] = r[a])
            }) : function (t) {
                return i(t, 0, r)
            }) : i
        }}, pseudos:{not:o(function (t) {
            var e = [], n = [], r = T(t.replace(le, "$1"));
            return r[F] ? o(function (t, e, n, o) {
                for (var i, a = r(t, null, o, []), s = t.length; s--;)(i = a[s]) && (t[s] = !(e[s] = i))
            }) : function (t, o, i) {
                return e[0] = t, r(e, null, i, n), !n.pop()
            }
        }), has:o(function (t) {
            return function (n) {
                return e(t, n).length > 0
            }
        }), contains:o(function (t) {
            return function (e) {
                return(e.textContent || e.innerText || _(e)).indexOf(t) > -1
            }
        }), lang:o(function (t) {
            return he.test(t || "") || e.error("unsupported lang: " + t), t = t.replace(xe, Ce).toLowerCase(), function (e) {
                var n;
                do if (n = I ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))return n = n.toLowerCase(), n === t || 0 === n.indexOf(t + "-"); while ((e = e.parentNode) && 1 === e.nodeType);
                return!1
            }
        }), target:function (e) {
            var n = t.location && t.location.hash;
            return n && n.slice(1) === e.id
        }, root:function (t) {
            return t === D
        }, focus:function (t) {
            return t === O.activeElement && (!O.hasFocus || O.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
        }, enabled:function (t) {
            return t.disabled === !1
        }, disabled:function (t) {
            return t.disabled === !0
        }, checked:function (t) {
            var e = t.nodeName.toLowerCase();
            return"input" === e && !!t.checked || "option" === e && !!t.selected
        }, selected:function (t) {
            return t.parentNode && t.parentNode.selectedIndex, t.selected === !0
        }, empty:function (t) {
            for (t = t.firstChild; t; t = t.nextSibling)if (t.nodeType < 6)return!1;
            return!0
        }, parent:function (t) {
            return!S.pseudos.empty(t)
        }, header:function (t) {
            return me.test(t.nodeName)
        }, input:function (t) {
            return ge.test(t.nodeName)
        }, button:function (t) {
            var e = t.nodeName.toLowerCase();
            return"input" === e && "button" === t.type || "button" === e
        }, text:function (t) {
            var e;
            return"input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
        }, first:c(function () {
            return[0]
        }), last:c(function (t, e) {
            return[e - 1]
        }), eq:c(function (t, e, n) {
            return[0 > n ? n + e : n]
        }), even:c(function (t, e) {
            for (var n = 0; e > n; n += 2)t.push(n);
            return t
        }), odd:c(function (t, e) {
            for (var n = 1; e > n; n += 2)t.push(n);
            return t
        }), lt:c(function (t, e, n) {
            for (var o = 0 > n ? n + e : n; --o >= 0;)t.push(o);
            return t
        }), gt:c(function (t, e, n) {
            for (var o = 0 > n ? n + e : n; ++o < e;)t.push(o);
            return t
        })}}, S.pseudos.nth = S.pseudos.eq;
        for (C in{radio:!0, checkbox:!0, file:!0, password:!0, image:!0})S.pseudos[C] = s(C);
        for (C in{submit:!0, reset:!0})S.pseudos[C] = l(C);
        return d.prototype = S.filters = S.pseudos, S.setFilters = new d, T = e.compile = function (t, e) {
            var n, o = [], r = [], i = z[t + " "];
            if (!i) {
                for (e || (e = f(t)), n = e.length; n--;)i = y(e[n]), i[F] ? o.push(i) : r.push(i);
                i = z(t, b(r, o))
            }
            return i
        }, E.sortStable = F.split("").sort(G).join("") === F, E.detectDuplicates = !!R, A(), E.sortDetached = r(function (t) {
            return 1 & t.compareDocumentPosition(O.createElement("div"))
        }), r(function (t) {
            return t.innerHTML = "<a href='#'></a>", "#" === t.firstChild.getAttribute("href")
        }) || i("type|href|height|width", function (t, e, n) {
            return n ? void 0 : t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }), E.attributes && r(function (t) {
            return t.innerHTML = "<input/>", t.firstChild.setAttribute("value", ""), "" === t.firstChild.getAttribute("value")
        }) || i("value", function (t, e, n) {
            return n || "input" !== t.nodeName.toLowerCase() ? void 0 : t.defaultValue
        }), r(function (t) {
            return null == t.getAttribute("disabled")
        }) || i(ne, function (t, e, n) {
            var o;
            return n ? void 0 : t[e] === !0 ? e.toLowerCase() : (o = t.getAttributeNode(e)) && o.specified ? o.value : null
        }), e
    }(t);
    ie.find = ue, ie.expr = ue.selectors, ie.expr[":"] = ie.expr.pseudos, ie.unique = ue.uniqueSort, ie.text = ue.getText, ie.isXMLDoc = ue.isXML, ie.contains = ue.contains;
    var de = ie.expr.match.needsContext, fe = /^<(\w+)\s*\/?>(?:<\/\1>|)$/, he = /^.[^:#\[\.,]*$/;
    ie.filter = function (t, e, n) {
        var o = e[0];
        return n && (t = ":not(" + t + ")"), 1 === e.length && 1 === o.nodeType ? ie.find.matchesSelector(o, t) ? [o] : [] : ie.find.matches(t, ie.grep(e, function (t) {
            return 1 === t.nodeType
        }))
    }, ie.fn.extend({find:function (t) {
        var e, n = [], o = this, r = o.length;
        if ("string" != typeof t)return this.pushStack(ie(t).filter(function () {
            for (e = 0; r > e; e++)if (ie.contains(o[e], this))return!0
        }));
        for (e = 0; r > e; e++)ie.find(t, o[e], n);
        return n = this.pushStack(r > 1 ? ie.unique(n) : n), n.selector = this.selector ? this.selector + " " + t : t, n
    }, filter:function (t) {
        return this.pushStack(o(this, t || [], !1))
    }, not:function (t) {
        return this.pushStack(o(this, t || [], !0))
    }, is:function (t) {
        return!!o(this, "string" == typeof t && de.test(t) ? ie(t) : t || [], !1).length
    }});
    var pe, ge = t.document, me = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/, ve = ie.fn.init = function (t, e) {
        var n, o;
        if (!t)return this;
        if ("string" == typeof t) {
            if (n = "<" === t.charAt(0) && ">" === t.charAt(t.length - 1) && t.length >= 3 ? [null, t, null] : me.exec(t), !n || !n[1] && e)return!e || e.jquery ? (e || pe).find(t) : this.constructor(e).find(t);
            if (n[1]) {
                if (e = e instanceof ie ? e[0] : e, ie.merge(this, ie.parseHTML(n[1], e && e.nodeType ? e.ownerDocument || e : ge, !0)), fe.test(n[1]) && ie.isPlainObject(e))for (n in e)ie.isFunction(this[n]) ? this[n](e[n]) : this.attr(n, e[n]);
                return this
            }
            if (o = ge.getElementById(n[2]), o && o.parentNode) {
                if (o.id !== n[2])return pe.find(t);
                this.length = 1, this[0] = o
            }
            return this.context = ge, this.selector = t, this
        }
        return t.nodeType ? (this.context = this[0] = t, this.length = 1, this) : ie.isFunction(t) ? "undefined" != typeof pe.ready ? pe.ready(t) : t(ie) : (void 0 !== t.selector && (this.selector = t.selector, this.context = t.context), ie.makeArray(t, this))
    };
    ve.prototype = ie.fn, pe = ie(ge);
    var ye = /^(?:parents|prev(?:Until|All))/, be = {children:!0, contents:!0, next:!0, prev:!0};
    ie.extend({dir:function (t, e, n) {
        for (var o = [], r = t[e]; r && 9 !== r.nodeType && (void 0 === n || 1 !== r.nodeType || !ie(r).is(n));)1 === r.nodeType && o.push(r), r = r[e];
        return o
    }, sibling:function (t, e) {
        for (var n = []; t; t = t.nextSibling)1 === t.nodeType && t !== e && n.push(t);
        return n
    }}), ie.fn.extend({has:function (t) {
        var e, n = ie(t, this), o = n.length;
        return this.filter(function () {
            for (e = 0; o > e; e++)if (ie.contains(this, n[e]))return!0
        })
    }, closest:function (t, e) {
        for (var n, o = 0, r = this.length, i = [], a = de.test(t) || "string" != typeof t ? ie(t, e || this.context) : 0; r > o; o++)for (n = this[o]; n && n !== e; n = n.parentNode)if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && ie.find.matchesSelector(n, t))) {
            i.push(n);
            break
        }
        return this.pushStack(i.length > 1 ? ie.unique(i) : i)
    }, index:function (t) {
        return t ? "string" == typeof t ? ie.inArray(this[0], ie(t)) : ie.inArray(t.jquery ? t[0] : t, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
    }, add:function (t, e) {
        return this.pushStack(ie.unique(ie.merge(this.get(), ie(t, e))))
    }, addBack:function (t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    }}), ie.each({parent:function (t) {
        var e = t.parentNode;
        return e && 11 !== e.nodeType ? e : null
    }, parents:function (t) {
        return ie.dir(t, "parentNode")
    }, parentsUntil:function (t, e, n) {
        return ie.dir(t, "parentNode", n)
    }, next:function (t) {
        return r(t, "nextSibling")
    }, prev:function (t) {
        return r(t, "previousSibling")
    }, nextAll:function (t) {
        return ie.dir(t, "nextSibling")
    }, prevAll:function (t) {
        return ie.dir(t, "previousSibling")
    }, nextUntil:function (t, e, n) {
        return ie.dir(t, "nextSibling", n)
    }, prevUntil:function (t, e, n) {
        return ie.dir(t, "previousSibling", n)
    }, siblings:function (t) {
        return ie.sibling((t.parentNode || {}).firstChild, t)
    }, children:function (t) {
        return ie.sibling(t.firstChild)
    }, contents:function (t) {
        return ie.nodeName(t, "iframe") ? t.contentDocument || t.contentWindow.document : ie.merge([], t.childNodes)
    }}, function (t, e) {
        ie.fn[t] = function (n, o) {
            var r = ie.map(this, e, n);
            return"Until" !== t.slice(-5) && (o = n), o && "string" == typeof o && (r = ie.filter(o, r)), this.length > 1 && (be[t] || (r = ie.unique(r)), ye.test(t) && (r = r.reverse())), this.pushStack(r)
        }
    });
    var we = /\S+/g, xe = {};
    ie.Callbacks = function (t) {
        t = "string" == typeof t ? xe[t] || i(t) : ie.extend({}, t);
        var e, n, o, r, a, s, l = [], c = !t.once && [], u = function (i) {
            for (n = t.memory && i, o = !0, a = s || 0, s = 0, r = l.length, e = !0; l && r > a; a++)if (l[a].apply(i[0], i[1]) === !1 && t.stopOnFalse) {
                n = !1;
                break
            }
            e = !1, l && (c ? c.length && u(c.shift()) : n ? l = [] : d.disable())
        }, d = {add:function () {
            if (l) {
                var o = l.length;
                !function i(e) {
                    ie.each(e, function (e, n) {
                        var o = ie.type(n);
                        "function" === o ? t.unique && d.has(n) || l.push(n) : n && n.length && "string" !== o && i(n)
                    })
                }(arguments), e ? r = l.length : n && (s = o, u(n))
            }
            return this
        }, remove:function () {
            return l && ie.each(arguments, function (t, n) {
                for (var o; (o = ie.inArray(n, l, o)) > -1;)l.splice(o, 1), e && (r >= o && r--, a >= o && a--)
            }), this
        }, has:function (t) {
            return t ? ie.inArray(t, l) > -1 : !(!l || !l.length)
        }, empty:function () {
            return l = [], r = 0, this
        }, disable:function () {
            return l = c = n = void 0, this
        }, disabled:function () {
            return!l
        }, lock:function () {
            return c = void 0, n || d.disable(), this
        }, locked:function () {
            return!c
        }, fireWith:function (t, n) {
            return!l || o && !c || (n = n || [], n = [t, n.slice ? n.slice() : n], e ? c.push(n) : u(n)), this
        }, fire:function () {
            return d.fireWith(this, arguments), this
        }, fired:function () {
            return!!o
        }};
        return d
    }, ie.extend({Deferred:function (t) {
        var e = [
            ["resolve", "done", ie.Callbacks("once memory"), "resolved"],
            ["reject", "fail", ie.Callbacks("once memory"), "rejected"],
            ["notify", "progress", ie.Callbacks("memory")]
        ], n = "pending", o = {state:function () {
            return n
        }, always:function () {
            return r.done(arguments).fail(arguments), this
        }, then:function () {
            var t = arguments;
            return ie.Deferred(function (n) {
                ie.each(e, function (e, i) {
                    var a = ie.isFunction(t[e]) && t[e];
                    r[i[1]](function () {
                        var t = a && a.apply(this, arguments);
                        t && ie.isFunction(t.promise) ? t.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[i[0] + "With"](this === o ? n.promise() : this, a ? [t] : arguments)
                    })
                }), t = null
            }).promise()
        }, promise:function (t) {
            return null != t ? ie.extend(t, o) : o
        }}, r = {};
        return o.pipe = o.then, ie.each(e, function (t, i) {
            var a = i[2], s = i[3];
            o[i[1]] = a.add, s && a.add(function () {
                n = s
            }, e[1 ^ t][2].disable, e[2][2].lock), r[i[0]] = function () {
                return r[i[0] + "With"](this === r ? o : this, arguments), this
            }, r[i[0] + "With"] = a.fireWith
        }), o.promise(r), t && t.call(r, r), r
    }, when:function (t) {
        var e, n, o, r = 0, i = Y.call(arguments), a = i.length, s = 1 !== a || t && ie.isFunction(t.promise) ? a : 0, l = 1 === s ? t : ie.Deferred(), c = function (t, n, o) {
            return function (r) {
                n[t] = this, o[t] = arguments.length > 1 ? Y.call(arguments) : r, o === e ? l.notifyWith(n, o) : --s || l.resolveWith(n, o)
            }
        };
        if (a > 1)for (e = new Array(a), n = new Array(a), o = new Array(a); a > r; r++)i[r] && ie.isFunction(i[r].promise) ? i[r].promise().done(c(r, o, i)).fail(l.reject).progress(c(r, n, e)) : --s;
        return s || l.resolveWith(o, i), l.promise()
    }});
    var Ce;
    ie.fn.ready = function (t) {
        return ie.ready.promise().done(t), this
    }, ie.extend({isReady:!1, readyWait:1, holdReady:function (t) {
        t ? ie.readyWait++ : ie.ready(!0)
    }, ready:function (t) {
        if (t === !0 ? !--ie.readyWait : !ie.isReady) {
            if (!ge.body)return setTimeout(ie.ready);
            ie.isReady = !0, t !== !0 && --ie.readyWait > 0 || (Ce.resolveWith(ge, [ie]), ie.fn.trigger && ie(ge).trigger("ready").off("ready"))
        }
    }}), ie.ready.promise = function (e) {
        if (!Ce)if (Ce = ie.Deferred(), "complete" === ge.readyState)setTimeout(ie.ready); else if (ge.addEventListener)ge.addEventListener("DOMContentLoaded", s, !1), t.addEventListener("load", s, !1); else {
            ge.attachEvent("onreadystatechange", s), t.attachEvent("onload", s);
            var n = !1;
            try {
                n = null == t.frameElement && ge.documentElement
            } catch (o) {
            }
            n && n.doScroll && function r() {
                if (!ie.isReady) {
                    try {
                        n.doScroll("left")
                    } catch (t) {
                        return setTimeout(r, 50)
                    }
                    a(), ie.ready()
                }
            }()
        }
        return Ce.promise(e)
    };
    var Ee, Se = "undefined";
    for (Ee in ie(oe))break;
    oe.ownLast = "0" !== Ee, oe.inlineBlockNeedsLayout = !1, ie(function () {
        var t, e, n = ge.getElementsByTagName("body")[0];
        n && (t = ge.createElement("div"), t.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", e = ge.createElement("div"), n.appendChild(t).appendChild(e), typeof e.style.zoom !== Se && (e.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1", (oe.inlineBlockNeedsLayout = 3 === e.offsetWidth) && (n.style.zoom = 1)), n.removeChild(t), t = e = null)
    }), function () {
        var t = ge.createElement("div");
        if (null == oe.deleteExpando) {
            oe.deleteExpando = !0;
            try {
                delete t.test
            } catch (e) {
                oe.deleteExpando = !1
            }
        }
        t = null
    }(), ie.acceptData = function (t) {
        var e = ie.noData[(t.nodeName + " ").toLowerCase()], n = +t.nodeType || 1;
        return 1 !== n && 9 !== n ? !1 : !e || e !== !0 && t.getAttribute("classid") === e
    };
    var _e = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/, ke = /([A-Z])/g;
    ie.extend({cache:{}, noData:{"applet ":!0, "embed ":!0, "object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"}, hasData:function (t) {
        return t = t.nodeType ? ie.cache[t[ie.expando]] : t[ie.expando], !!t && !c(t)
    }, data:function (t, e, n) {
        return u(t, e, n)
    }, removeData:function (t, e) {
        return d(t, e)
    }, _data:function (t, e, n) {
        return u(t, e, n, !0)
    }, _removeData:function (t, e) {
        return d(t, e, !0)
    }}), ie.fn.extend({data:function (t, e) {
        var n, o, r, i = this[0], a = i && i.attributes;
        if (void 0 === t) {
            if (this.length && (r = ie.data(i), 1 === i.nodeType && !ie._data(i, "parsedAttrs"))) {
                for (n = a.length; n--;)o = a[n].name, 0 === o.indexOf("data-") && (o = ie.camelCase(o.slice(5)), l(i, o, r[o]));
                ie._data(i, "parsedAttrs", !0)
            }
            return r
        }
        return"object" == typeof t ? this.each(function () {
            ie.data(this, t)
        }) : arguments.length > 1 ? this.each(function () {
            ie.data(this, t, e)
        }) : i ? l(i, t, ie.data(i, t)) : void 0
    }, removeData:function (t) {
        return this.each(function () {
            ie.removeData(this, t)
        })
    }}), ie.extend({queue:function (t, e, n) {
        var o;
        return t ? (e = (e || "fx") + "queue", o = ie._data(t, e), n && (!o || ie.isArray(n) ? o = ie._data(t, e, ie.makeArray(n)) : o.push(n)), o || []) : void 0
    }, dequeue:function (t, e) {
        e = e || "fx";
        var n = ie.queue(t, e), o = n.length, r = n.shift(), i = ie._queueHooks(t, e), a = function () {
            ie.dequeue(t, e)
        };
        "inprogress" === r && (r = n.shift(), o--), r && ("fx" === e && n.unshift("inprogress"), delete i.stop, r.call(t, a, i)), !o && i && i.empty.fire()
    }, _queueHooks:function (t, e) {
        var n = e + "queueHooks";
        return ie._data(t, n) || ie._data(t, n, {empty:ie.Callbacks("once memory").add(function () {
            ie._removeData(t, e + "queue"), ie._removeData(t, n)
        })})
    }}), ie.fn.extend({queue:function (t, e) {
        var n = 2;
        return"string" != typeof t && (e = t, t = "fx", n--), arguments.length < n ? ie.queue(this[0], t) : void 0 === e ? this : this.each(function () {
            var n = ie.queue(this, t, e);
            ie._queueHooks(this, t), "fx" === t && "inprogress" !== n[0] && ie.dequeue(this, t)
        })
    }, dequeue:function (t) {
        return this.each(function () {
            ie.dequeue(this, t)
        })
    }, clearQueue:function (t) {
        return this.queue(t || "fx", [])
    }, promise:function (t, e) {
        var n, o = 1, r = ie.Deferred(), i = this, a = this.length, s = function () {
            --o || r.resolveWith(i, [i])
        };
        for ("string" != typeof t && (e = t, t = void 0), t = t || "fx"; a--;)n = ie._data(i[a], t + "queueHooks"), n && n.empty && (o++, n.empty.add(s));
        return s(), r.promise(e)
    }});
    var Te = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source, Ne = ["Top", "Right", "Bottom", "Left"], Pe = function (t, e) {
        return t = e || t, "none" === ie.css(t, "display") || !ie.contains(t.ownerDocument, t)
    }, Re = ie.access = function (t, e, n, o, r, i, a) {
        var s = 0, l = t.length, c = null == n;
        if ("object" === ie.type(n)) {
            r = !0;
            for (s in n)ie.access(t, e, s, n[s], !0, i, a)
        } else if (void 0 !== o && (r = !0, ie.isFunction(o) || (a = !0), c && (a ? (e.call(t, o), e = null) : (c = e, e = function (t, e, n) {
            return c.call(ie(t), n)
        })), e))for (; l > s; s++)e(t[s], n, a ? o : o.call(t[s], s, e(t[s], n)));
        return r ? t : c ? e.call(t) : l ? e(t[0], n) : i
    }, Ae = /^(?:checkbox|radio)$/i;
    !function () {
        var t = ge.createDocumentFragment(), e = ge.createElement("div"), n = ge.createElement("input");
        if (e.setAttribute("className", "t"), e.innerHTML = "  <link/><table></table><a href='/a'>a</a>", oe.leadingWhitespace = 3 === e.firstChild.nodeType, oe.tbody = !e.getElementsByTagName("tbody").length, oe.htmlSerialize = !!e.getElementsByTagName("link").length, oe.html5Clone = "<:nav></:nav>" !== ge.createElement("nav").cloneNode(!0).outerHTML, n.type = "checkbox", n.checked = !0, t.appendChild(n), oe.appendChecked = n.checked, e.innerHTML = "<textarea>x</textarea>", oe.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue, t.appendChild(e), e.innerHTML = "<input type='radio' checked='checked' name='t'/>", oe.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked, oe.noCloneEvent = !0, e.attachEvent && (e.attachEvent("onclick", function () {
            oe.noCloneEvent = !1
        }), e.cloneNode(!0).click()), null == oe.deleteExpando) {
            oe.deleteExpando = !0;
            try {
                delete e.test
            } catch (o) {
                oe.deleteExpando = !1
            }
        }
        t = e = n = null
    }(), function () {
        var e, n, o = ge.createElement("div");
        for (e in{submit:!0, change:!0, focusin:!0})n = "on" + e, (oe[e + "Bubbles"] = n in t) || (o.setAttribute(n, "t"), oe[e + "Bubbles"] = o.attributes[n].expando === !1);
        o = null
    }();
    var Oe = /^(?:input|select|textarea)$/i, De = /^key/, Ie = /^(?:mouse|contextmenu)|click/, $e = /^(?:focusinfocus|focusoutblur)$/, Le = /^([^.]*)(?:\.(.+)|)$/;
    ie.event = {global:{}, add:function (t, e, n, o, r) {
        var i, a, s, l, c, u, d, f, h, p, g, m = ie._data(t);
        if (m) {
            for (n.handler && (l = n, n = l.handler, r = l.selector), n.guid || (n.guid = ie.guid++), (a = m.events) || (a = m.events = {}), (u = m.handle) || (u = m.handle = function (t) {
                return typeof ie === Se || t && ie.event.triggered === t.type ? void 0 : ie.event.dispatch.apply(u.elem, arguments)
            }, u.elem = t), e = (e || "").match(we) || [""], s = e.length; s--;)i = Le.exec(e[s]) || [], h = g = i[1], p = (i[2] || "").split(".").sort(), h && (c = ie.event.special[h] || {}, h = (r ? c.delegateType : c.bindType) || h, c = ie.event.special[h] || {}, d = ie.extend({type:h, origType:g, data:o, handler:n, guid:n.guid, selector:r, needsContext:r && ie.expr.match.needsContext.test(r), namespace:p.join(".")}, l), (f = a[h]) || (f = a[h] = [], f.delegateCount = 0, c.setup && c.setup.call(t, o, p, u) !== !1 || (t.addEventListener ? t.addEventListener(h, u, !1) : t.attachEvent && t.attachEvent("on" + h, u))), c.add && (c.add.call(t, d), d.handler.guid || (d.handler.guid = n.guid)), r ? f.splice(f.delegateCount++, 0, d) : f.push(d), ie.event.global[h] = !0);
            t = null
        }
    }, remove:function (t, e, n, o, r) {
        var i, a, s, l, c, u, d, f, h, p, g, m = ie.hasData(t) && ie._data(t);
        if (m && (u = m.events)) {
            for (e = (e || "").match(we) || [""], c = e.length; c--;)if (s = Le.exec(e[c]) || [], h = g = s[1], p = (s[2] || "").split(".").sort(), h) {
                for (d = ie.event.special[h] || {}, h = (o ? d.delegateType : d.bindType) || h, f = u[h] || [], s = s[2] && new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = i = f.length; i--;)a = f[i], !r && g !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || o && o !== a.selector && ("**" !== o || !a.selector) || (f.splice(i, 1), a.selector && f.delegateCount--, d.remove && d.remove.call(t, a));
                l && !f.length && (d.teardown && d.teardown.call(t, p, m.handle) !== !1 || ie.removeEvent(t, h, m.handle), delete u[h])
            } else for (h in u)ie.event.remove(t, h + e[c], n, o, !0);
            ie.isEmptyObject(u) && (delete m.handle, ie._removeData(t, "events"))
        }
    }, trigger:function (e, n, o, r) {
        var i, a, s, l, c, u, d, f = [o || ge], h = ee.call(e, "type") ? e.type : e, p = ee.call(e, "namespace") ? e.namespace.split(".") : [];
        if (s = u = o = o || ge, 3 !== o.nodeType && 8 !== o.nodeType && !$e.test(h + ie.event.triggered) && (h.indexOf(".") >= 0 && (p = h.split("."), h = p.shift(), p.sort()), a = h.indexOf(":") < 0 && "on" + h, e = e[ie.expando] ? e : new ie.Event(h, "object" == typeof e && e), e.isTrigger = r ? 2 : 3, e.namespace = p.join("."), e.namespace_re = e.namespace ? new RegExp("(^|\\.)" + p.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, e.result = void 0, e.target || (e.target = o), n = null == n ? [e] : ie.makeArray(n, [e]), c = ie.event.special[h] || {}, r || !c.trigger || c.trigger.apply(o, n) !== !1)) {
            if (!r && !c.noBubble && !ie.isWindow(o)) {
                for (l = c.delegateType || h, $e.test(l + h) || (s = s.parentNode); s; s = s.parentNode)f.push(s), u = s;
                u === (o.ownerDocument || ge) && f.push(u.defaultView || u.parentWindow || t)
            }
            for (d = 0; (s = f[d++]) && !e.isPropagationStopped();)e.type = d > 1 ? l : c.bindType || h, i = (ie._data(s, "events") || {})[e.type] && ie._data(s, "handle"), i && i.apply(s, n), i = a && s[a], i && i.apply && ie.acceptData(s) && (e.result = i.apply(s, n), e.result === !1 && e.preventDefault());
            if (e.type = h, !r && !e.isDefaultPrevented() && (!c._default || c._default.apply(f.pop(), n) === !1) && ie.acceptData(o) && a && o[h] && !ie.isWindow(o)) {
                u = o[a], u && (o[a] = null), ie.event.triggered = h;
                try {
                    o[h]()
                } catch (g) {
                }
                ie.event.triggered = void 0, u && (o[a] = u)
            }
            return e.result
        }
    }, dispatch:function (t) {
        t = ie.event.fix(t);
        var e, n, o, r, i, a = [], s = Y.call(arguments), l = (ie._data(this, "events") || {})[t.type] || [], c = ie.event.special[t.type] || {};
        if (s[0] = t, t.delegateTarget = this, !c.preDispatch || c.preDispatch.call(this, t) !== !1) {
            for (a = ie.event.handlers.call(this, t, l), e = 0; (r = a[e++]) && !t.isPropagationStopped();)for (t.currentTarget = r.elem, i = 0; (o = r.handlers[i++]) && !t.isImmediatePropagationStopped();)(!t.namespace_re || t.namespace_re.test(o.namespace)) && (t.handleObj = o, t.data = o.data, n = ((ie.event.special[o.origType] || {}).handle || o.handler).apply(r.elem, s), void 0 !== n && (t.result = n) === !1 && (t.preventDefault(), t.stopPropagation()));
            return c.postDispatch && c.postDispatch.call(this, t), t.result
        }
    }, handlers:function (t, e) {
        var n, o, r, i, a = [], s = e.delegateCount, l = t.target;
        if (s && l.nodeType && (!t.button || "click" !== t.type))for (; l != this; l = l.parentNode || this)if (1 === l.nodeType && (l.disabled !== !0 || "click" !== t.type)) {
            for (r = [], i = 0; s > i; i++)o = e[i], n = o.selector + " ", void 0 === r[n] && (r[n] = o.needsContext ? ie(n, this).index(l) >= 0 : ie.find(n, this, null, [l]).length), r[n] && r.push(o);
            r.length && a.push({elem:l, handlers:r})
        }
        return s < e.length && a.push({elem:this, handlers:e.slice(s)}), a
    }, fix:function (t) {
        if (t[ie.expando])return t;
        var e, n, o, r = t.type, i = t, a = this.fixHooks[r];
        for (a || (this.fixHooks[r] = a = Ie.test(r) ? this.mouseHooks : De.test(r) ? this.keyHooks : {}), o = a.props ? this.props.concat(a.props) : this.props, t = new ie.Event(i), e = o.length; e--;)n = o[e], t[n] = i[n];
        return t.target || (t.target = i.srcElement || ge), 3 === t.target.nodeType && (t.target = t.target.parentNode), t.metaKey = !!t.metaKey, a.filter ? a.filter(t, i) : t
    }, props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "), fixHooks:{}, keyHooks:{props:"char charCode key keyCode".split(" "), filter:function (t, e) {
        return null == t.which && (t.which = null != e.charCode ? e.charCode : e.keyCode), t
    }}, mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "), filter:function (t, e) {
        var n, o, r, i = e.button, a = e.fromElement;
        return null == t.pageX && null != e.clientX && (o = t.target.ownerDocument || ge, r = o.documentElement, n = o.body, t.pageX = e.clientX + (r && r.scrollLeft || n && n.scrollLeft || 0) - (r && r.clientLeft || n && n.clientLeft || 0), t.pageY = e.clientY + (r && r.scrollTop || n && n.scrollTop || 0) - (r && r.clientTop || n && n.clientTop || 0)), !t.relatedTarget && a && (t.relatedTarget = a === t.target ? e.toElement : a), t.which || void 0 === i || (t.which = 1 & i ? 1 : 2 & i ? 3 : 4 & i ? 2 : 0), t
    }}, special:{load:{noBubble:!0}, focus:{trigger:function () {
        if (this !== p() && this.focus)try {
            return this.focus(), !1
        } catch (t) {
        }
    }, delegateType:"focusin"}, blur:{trigger:function () {
        return this === p() && this.blur ? (this.blur(), !1) : void 0
    }, delegateType:"focusout"}, click:{trigger:function () {
        return ie.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
    }, _default:function (t) {
        return ie.nodeName(t.target, "a")
    }}, beforeunload:{postDispatch:function (t) {
        void 0 !== t.result && (t.originalEvent.returnValue = t.result)
    }}}, simulate:function (t, e, n, o) {
        var r = ie.extend(new ie.Event, n, {type:t, isSimulated:!0, originalEvent:{}});
        o ? ie.event.trigger(r, null, e) : ie.event.dispatch.call(e, r), r.isDefaultPrevented() && n.preventDefault()
    }}, ie.removeEvent = ge.removeEventListener ? function (t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n, !1)
    } : function (t, e, n) {
        var o = "on" + e;
        t.detachEvent && (typeof t[o] === Se && (t[o] = null), t.detachEvent(o, n))
    }, ie.Event = function (t, e) {
        return this instanceof ie.Event ? (t && t.type ? (this.originalEvent = t, this.type = t.type, this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && (t.returnValue === !1 || t.getPreventDefault && t.getPreventDefault()) ? f : h) : this.type = t, e && ie.extend(this, e), this.timeStamp = t && t.timeStamp || ie.now(), this[ie.expando] = !0, void 0) : new ie.Event(t, e)
    }, ie.Event.prototype = {isDefaultPrevented:h, isPropagationStopped:h, isImmediatePropagationStopped:h, preventDefault:function () {
        var t = this.originalEvent;
        this.isDefaultPrevented = f, t && (t.preventDefault ? t.preventDefault() : t.returnValue = !1)
    }, stopPropagation:function () {
        var t = this.originalEvent;
        this.isPropagationStopped = f, t && (t.stopPropagation && t.stopPropagation(), t.cancelBubble = !0)
    }, stopImmediatePropagation:function () {
        this.isImmediatePropagationStopped = f, this.stopPropagation()
    }}, ie.each({mouseenter:"mouseover", mouseleave:"mouseout"}, function (t, e) {
        ie.event.special[t] = {delegateType:e, bindType:e, handle:function (t) {
            var n, o = this, r = t.relatedTarget, i = t.handleObj;
            return(!r || r !== o && !ie.contains(o, r)) && (t.type = i.origType, n = i.handler.apply(this, arguments), t.type = e), n
        }}
    }), oe.submitBubbles || (ie.event.special.submit = {setup:function () {
        return ie.nodeName(this, "form") ? !1 : (ie.event.add(this, "click._submit keypress._submit", function (t) {
            var e = t.target, n = ie.nodeName(e, "input") || ie.nodeName(e, "button") ? e.form : void 0;
            n && !ie._data(n, "submitBubbles") && (ie.event.add(n, "submit._submit", function (t) {
                t._submit_bubble = !0
            }), ie._data(n, "submitBubbles", !0))
        }), void 0)
    }, postDispatch:function (t) {
        t._submit_bubble && (delete t._submit_bubble, this.parentNode && !t.isTrigger && ie.event.simulate("submit", this.parentNode, t, !0))
    }, teardown:function () {
        return ie.nodeName(this, "form") ? !1 : (ie.event.remove(this, "._submit"), void 0)
    }}), oe.changeBubbles || (ie.event.special.change = {setup:function () {
        return Oe.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (ie.event.add(this, "propertychange._change", function (t) {
            "checked" === t.originalEvent.propertyName && (this._just_changed = !0)
        }), ie.event.add(this, "click._change", function (t) {
            this._just_changed && !t.isTrigger && (this._just_changed = !1), ie.event.simulate("change", this, t, !0)
        })), !1) : (ie.event.add(this, "beforeactivate._change", function (t) {
            var e = t.target;
            Oe.test(e.nodeName) && !ie._data(e, "changeBubbles") && (ie.event.add(e, "change._change", function (t) {
                !this.parentNode || t.isSimulated || t.isTrigger || ie.event.simulate("change", this.parentNode, t, !0)
            }), ie._data(e, "changeBubbles", !0))
        }), void 0)
    }, handle:function (t) {
        var e = t.target;
        return this !== e || t.isSimulated || t.isTrigger || "radio" !== e.type && "checkbox" !== e.type ? t.handleObj.handler.apply(this, arguments) : void 0
    }, teardown:function () {
        return ie.event.remove(this, "._change"), !Oe.test(this.nodeName)
    }}), oe.focusinBubbles || ie.each({focus:"focusin", blur:"focusout"}, function (t, e) {
        var n = function (t) {
            ie.event.simulate(e, t.target, ie.event.fix(t), !0)
        };
        ie.event.special[e] = {setup:function () {
            var o = this.ownerDocument || this, r = ie._data(o, e);
            r || o.addEventListener(t, n, !0), ie._data(o, e, (r || 0) + 1)
        }, teardown:function () {
            var o = this.ownerDocument || this, r = ie._data(o, e) - 1;
            r ? ie._data(o, e, r) : (o.removeEventListener(t, n, !0), ie._removeData(o, e))
        }}
    }), ie.fn.extend({on:function (t, e, n, o, r) {
        var i, a;
        if ("object" == typeof t) {
            "string" != typeof e && (n = n || e, e = void 0);
            for (i in t)this.on(i, e, n, t[i], r);
            return this
        }
        if (null == n && null == o ? (o = e, n = e = void 0) : null == o && ("string" == typeof e ? (o = n, n = void 0) : (o = n, n = e, e = void 0)), o === !1)o = h; else if (!o)return this;
        return 1 === r && (a = o, o = function (t) {
            return ie().off(t), a.apply(this, arguments)
        }, o.guid = a.guid || (a.guid = ie.guid++)), this.each(function () {
            ie.event.add(this, t, o, n, e)
        })
    }, one:function (t, e, n, o) {
        return this.on(t, e, n, o, 1)
    }, off:function (t, e, n) {
        var o, r;
        if (t && t.preventDefault && t.handleObj)return o = t.handleObj, ie(t.delegateTarget).off(o.namespace ? o.origType + "." + o.namespace : o.origType, o.selector, o.handler), this;
        if ("object" == typeof t) {
            for (r in t)this.off(r, e, t[r]);
            return this
        }
        return(e === !1 || "function" == typeof e) && (n = e, e = void 0), n === !1 && (n = h), this.each(function () {
            ie.event.remove(this, t, n, e)
        })
    }, trigger:function (t, e) {
        return this.each(function () {
            ie.event.trigger(t, e, this)
        })
    }, triggerHandler:function (t, e) {
        var n = this[0];
        return n ? ie.event.trigger(t, e, n, !0) : void 0
    }});
    var Me = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video", Be = / jQuery\d+="(?:null|\d+)"/g, Fe = new RegExp("<(?:" + Me + ")[\\s/>]", "i"), je = /^\s+/, He = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi, We = /<([\w:]+)/, qe = /<tbody/i, Xe = /<|&#?\w+;/, ze = /<(?:script|style|link)/i, Ge = /checked\s*(?:[^=]|=\s*.checked.)/i, Ue = /^$|\/(?:java|ecma)script/i, Ve = /^true\/(.*)/, Ye = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, Qe = {option:[1, "<select multiple='multiple'>", "</select>"], legend:[1, "<fieldset>", "</fieldset>"], area:[1, "<map>", "</map>"], param:[1, "<object>", "</object>"], thead:[1, "<table>", "</table>"], tr:[2, "<table><tbody>", "</tbody></table>"], col:[2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"], td:[3, "<table><tbody><tr>", "</tr></tbody></table>"], _default:oe.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]}, Ke = g(ge), Je = Ke.appendChild(ge.createElement("div"));
    Qe.optgroup = Qe.option, Qe.tbody = Qe.tfoot = Qe.colgroup = Qe.caption = Qe.thead, Qe.th = Qe.td, ie.extend({clone:function (t, e, n) {
        var o, r, i, a, s, l = ie.contains(t.ownerDocument, t);
        if (oe.html5Clone || ie.isXMLDoc(t) || !Fe.test("<" + t.nodeName + ">") ? i = t.cloneNode(!0) : (Je.innerHTML = t.outerHTML, Je.removeChild(i = Je.firstChild)), !(oe.noCloneEvent && oe.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || ie.isXMLDoc(t)))for (o = m(i), s = m(t), a = 0; null != (r = s[a]); ++a)o[a] && E(r, o[a]);
        if (e)if (n)for (s = s || m(t), o = o || m(i), a = 0; null != (r = s[a]); a++)C(r, o[a]); else C(t, i);
        return o = m(i, "script"), o.length > 0 && x(o, !l && m(t, "script")), o = s = r = null, i
    }, buildFragment:function (t, e, n, o) {
        for (var r, i, a, s, l, c, u, d = t.length, f = g(e), h = [], p = 0; d > p; p++)if (i = t[p], i || 0 === i)if ("object" === ie.type(i))ie.merge(h, i.nodeType ? [i] : i); else if (Xe.test(i)) {
            for (s = s || f.appendChild(e.createElement("div")), l = (We.exec(i) || ["", ""])[1].toLowerCase(), u = Qe[l] || Qe._default, s.innerHTML = u[1] + i.replace(He, "<$1></$2>") + u[2], r = u[0]; r--;)s = s.lastChild;
            if (!oe.leadingWhitespace && je.test(i) && h.push(e.createTextNode(je.exec(i)[0])), !oe.tbody)for (i = "table" !== l || qe.test(i) ? "<table>" !== u[1] || qe.test(i) ? 0 : s : s.firstChild, r = i && i.childNodes.length; r--;)ie.nodeName(c = i.childNodes[r], "tbody") && !c.childNodes.length && i.removeChild(c);
            for (ie.merge(h, s.childNodes), s.textContent = ""; s.firstChild;)s.removeChild(s.firstChild);
            s = f.lastChild
        } else h.push(e.createTextNode(i));
        for (s && f.removeChild(s), oe.appendChecked || ie.grep(m(h, "input"), v), p = 0; i = h[p++];)if ((!o || -1 === ie.inArray(i, o)) && (a = ie.contains(i.ownerDocument, i), s = m(f.appendChild(i), "script"), a && x(s), n))for (r = 0; i = s[r++];)Ue.test(i.type || "") && n.push(i);
        return s = null, f
    }, cleanData:function (t, e) {
        for (var n, o, r, i, a = 0, s = ie.expando, l = ie.cache, c = oe.deleteExpando, u = ie.event.special; null != (n = t[a]); a++)if ((e || ie.acceptData(n)) && (r = n[s], i = r && l[r])) {
            if (i.events)for (o in i.events)u[o] ? ie.event.remove(n, o) : ie.removeEvent(n, o, i.handle);
            l[r] && (delete l[r], c ? delete n[s] : typeof n.removeAttribute !== Se ? n.removeAttribute(s) : n[s] = null, V.push(r))
        }
    }}), ie.fn.extend({text:function (t) {
        return Re(this, function (t) {
            return void 0 === t ? ie.text(this) : this.empty().append((this[0] && this[0].ownerDocument || ge).createTextNode(t))
        }, null, t, arguments.length)
    }, append:function () {
        return this.domManip(arguments, function (t) {
            if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                var e = y(this, t);
                e.appendChild(t)
            }
        })
    }, prepend:function () {
        return this.domManip(arguments, function (t) {
            if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                var e = y(this, t);
                e.insertBefore(t, e.firstChild)
            }
        })
    }, before:function () {
        return this.domManip(arguments, function (t) {
            this.parentNode && this.parentNode.insertBefore(t, this)
        })
    }, after:function () {
        return this.domManip(arguments, function (t) {
            this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
        })
    }, remove:function (t, e) {
        for (var n, o = t ? ie.filter(t, this) : this, r = 0; null != (n = o[r]); r++)e || 1 !== n.nodeType || ie.cleanData(m(n)), n.parentNode && (e && ie.contains(n.ownerDocument, n) && x(m(n, "script")), n.parentNode.removeChild(n));
        return this
    }, empty:function () {
        for (var t, e = 0; null != (t = this[e]); e++) {
            for (1 === t.nodeType && ie.cleanData(m(t, !1)); t.firstChild;)t.removeChild(t.firstChild);
            t.options && ie.nodeName(t, "select") && (t.options.length = 0)
        }
        return this
    }, clone:function (t, e) {
        return t = null == t ? !1 : t, e = null == e ? t : e, this.map(function () {
            return ie.clone(this, t, e)
        })
    }, html:function (t) {
        return Re(this, function (t) {
            var e = this[0] || {}, n = 0, o = this.length;
            if (void 0 === t)return 1 === e.nodeType ? e.innerHTML.replace(Be, "") : void 0;
            if (!("string" != typeof t || ze.test(t) || !oe.htmlSerialize && Fe.test(t) || !oe.leadingWhitespace && je.test(t) || Qe[(We.exec(t) || ["", ""])[1].toLowerCase()])) {
                t = t.replace(He, "<$1></$2>");
                try {
                    for (; o > n; n++)e = this[n] || {}, 1 === e.nodeType && (ie.cleanData(m(e, !1)), e.innerHTML = t);
                    e = 0
                } catch (r) {
                }
            }
            e && this.empty().append(t)
        }, null, t, arguments.length)
    }, replaceWith:function () {
        var t = arguments[0];
        return this.domManip(arguments, function (e) {
            t = this.parentNode, ie.cleanData(m(this)), t && t.replaceChild(e, this)
        }), t && (t.length || t.nodeType) ? this : this.remove()
    }, detach:function (t) {
        return this.remove(t, !0)
    }, domManip:function (t, e) {
        t = Q.apply([], t);
        var n, o, r, i, a, s, l = 0, c = this.length, u = this, d = c - 1, f = t[0], h = ie.isFunction(f);
        if (h || c > 1 && "string" == typeof f && !oe.checkClone && Ge.test(f))return this.each(function (n) {
            var o = u.eq(n);
            h && (t[0] = f.call(this, n, o.html())), o.domManip(t, e)
        });
        if (c && (s = ie.buildFragment(t, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) {
            for (i = ie.map(m(s, "script"), b), r = i.length; c > l; l++)o = s, l !== d && (o = ie.clone(o, !0, !0), r && ie.merge(i, m(o, "script"))), e.call(this[l], o, l);
            if (r)for (a = i[i.length - 1].ownerDocument, ie.map(i, w), l = 0; r > l; l++)o = i[l], Ue.test(o.type || "") && !ie._data(o, "globalEval") && ie.contains(a, o) && (o.src ? ie._evalUrl && ie._evalUrl(o.src) : ie.globalEval((o.text || o.textContent || o.innerHTML || "").replace(Ye, "")));
            s = n = null
        }
        return this
    }}), ie.each({appendTo:"append", prependTo:"prepend", insertBefore:"before", insertAfter:"after", replaceAll:"replaceWith"}, function (t, e) {
        ie.fn[t] = function (t) {
            for (var n, o = 0, r = [], i = ie(t), a = i.length - 1; a >= o; o++)n = o === a ? this : this.clone(!0), ie(i[o])[e](n), K.apply(r, n.get());
            return this.pushStack(r)
        }
    });
    var Ze, tn = {};
    !function () {
        var t, e, n = ge.createElement("div"), o = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";
        n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = n.getElementsByTagName("a")[0], t.style.cssText = "float:left;opacity:.5", oe.opacity = /^0.5/.test(t.style.opacity), oe.cssFloat = !!t.style.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", oe.clearCloneStyle = "content-box" === n.style.backgroundClip, t = n = null, oe.shrinkWrapBlocks = function () {
            var t, n, r, i;
            if (null == e) {
                if (t = ge.getElementsByTagName("body")[0], !t)return;
                i = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px", n = ge.createElement("div"), r = ge.createElement("div"), t.appendChild(n).appendChild(r), e = !1, typeof r.style.zoom !== Se && (r.style.cssText = o + ";width:1px;padding:1px;zoom:1", r.innerHTML = "<div></div>", r.firstChild.style.width = "5px", e = 3 !== r.offsetWidth), t.removeChild(n), t = n = r = null
            }
            return e
        }
    }();
    var en, nn, on = /^margin/, rn = new RegExp("^(" + Te + ")(?!px)[a-z%]+$", "i"), an = /^(top|right|bottom|left)$/;
    t.getComputedStyle ? (en = function (t) {
        return t.ownerDocument.defaultView.getComputedStyle(t, null)
    }, nn = function (t, e, n) {
        var o, r, i, a, s = t.style;
        return n = n || en(t), a = n ? n.getPropertyValue(e) || n[e] : void 0, n && ("" !== a || ie.contains(t.ownerDocument, t) || (a = ie.style(t, e)), rn.test(a) && on.test(e) && (o = s.width, r = s.minWidth, i = s.maxWidth, s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = o, s.minWidth = r, s.maxWidth = i)), void 0 === a ? a : a + ""
    }) : ge.documentElement.currentStyle && (en = function (t) {
        return t.currentStyle
    }, nn = function (t, e, n) {
        var o, r, i, a, s = t.style;
        return n = n || en(t), a = n ? n[e] : void 0, null == a && s && s[e] && (a = s[e]), rn.test(a) && !an.test(e) && (o = s.left, r = t.runtimeStyle, i = r && r.left, i && (r.left = t.currentStyle.left), s.left = "fontSize" === e ? "1em" : a, a = s.pixelLeft + "px", s.left = o, i && (r.left = i)), void 0 === a ? a : a + "" || "auto"
    }), function () {
        function e() {
            var e, n, o = ge.getElementsByTagName("body")[0];
            o && (e = ge.createElement("div"), n = ge.createElement("div"), e.style.cssText = c, o.appendChild(e).appendChild(n), n.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;display:block;padding:1px;border:1px;width:4px;margin-top:1%;top:1%", ie.swap(o, null != o.style.zoom ? {zoom:1} : {}, function () {
                r = 4 === n.offsetWidth
            }), i = !0, a = !1, s = !0, t.getComputedStyle && (a = "1%" !== (t.getComputedStyle(n, null) || {}).top, i = "4px" === (t.getComputedStyle(n, null) || {width:"4px"}).width), o.removeChild(e), n = o = null)
        }

        var n, o, r, i, a, s, l = ge.createElement("div"), c = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px", u = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";
        l.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = l.getElementsByTagName("a")[0], n.style.cssText = "float:left;opacity:.5", oe.opacity = /^0.5/.test(n.style.opacity), oe.cssFloat = !!n.style.cssFloat, l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", oe.clearCloneStyle = "content-box" === l.style.backgroundClip, n = l = null, ie.extend(oe, {reliableHiddenOffsets:function () {
            if (null != o)return o;
            var t, e, n, r = ge.createElement("div"), i = ge.getElementsByTagName("body")[0];
            if (i)return r.setAttribute("className", "t"), r.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = ge.createElement("div"), t.style.cssText = c, i.appendChild(t).appendChild(r), r.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", e = r.getElementsByTagName("td"), e[0].style.cssText = "padding:0;margin:0;border:0;display:none", n = 0 === e[0].offsetHeight, e[0].style.display = "", e[1].style.display = "none", o = n && 0 === e[0].offsetHeight, i.removeChild(t), r = i = null, o
        }, boxSizing:function () {
            return null == r && e(), r
        }, boxSizingReliable:function () {
            return null == i && e(), i
        }, pixelPosition:function () {
            return null == a && e(), a
        }, reliableMarginRight:function () {
            var e, n, o, r;
            if (null == s && t.getComputedStyle) {
                if (e = ge.getElementsByTagName("body")[0], !e)return;
                n = ge.createElement("div"), o = ge.createElement("div"), n.style.cssText = c, e.appendChild(n).appendChild(o), r = o.appendChild(ge.createElement("div")), r.style.cssText = o.style.cssText = u, r.style.marginRight = r.style.width = "0", o.style.width = "1px", s = !parseFloat((t.getComputedStyle(r, null) || {}).marginRight), e.removeChild(n)
            }
            return s
        }})
    }(), ie.swap = function (t, e, n, o) {
        var r, i, a = {};
        for (i in e)a[i] = t.style[i], t.style[i] = e[i];
        r = n.apply(t, o || []);
        for (i in e)t.style[i] = a[i];
        return r
    };
    var sn = /alpha\([^)]*\)/i, ln = /opacity\s*=\s*([^)]*)/, cn = /^(none|table(?!-c[ea]).+)/, un = new RegExp("^(" + Te + ")(.*)$", "i"), dn = new RegExp("^([+-])=(" + Te + ")", "i"), fn = {position:"absolute", visibility:"hidden", display:"block"}, hn = {letterSpacing:0, fontWeight:400}, pn = ["Webkit", "O", "Moz", "ms"];
    ie.extend({cssHooks:{opacity:{get:function (t, e) {
        if (e) {
            var n = nn(t, "opacity");
            return"" === n ? "1" : n
        }
    }}}, cssNumber:{columnCount:!0, fillOpacity:!0, fontWeight:!0, lineHeight:!0, opacity:!0, order:!0, orphans:!0, widows:!0, zIndex:!0, zoom:!0}, cssProps:{"float":oe.cssFloat ? "cssFloat" : "styleFloat"}, style:function (t, e, n, o) {
        if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
            var r, i, a, s = ie.camelCase(e), l = t.style;
            if (e = ie.cssProps[s] || (ie.cssProps[s] = T(l, s)), a = ie.cssHooks[e] || ie.cssHooks[s], void 0 === n)return a && "get"in a && void 0 !== (r = a.get(t, !1, o)) ? r : l[e];
            if (i = typeof n, "string" === i && (r = dn.exec(n)) && (n = (r[1] + 1) * r[2] + parseFloat(ie.css(t, e)), i = "number"), null != n && n === n && ("number" !== i || ie.cssNumber[s] || (n += "px"), oe.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (l[e] = "inherit"), !(a && "set"in a && void 0 === (n = a.set(t, n, o)))))try {
                l[e] = "", l[e] = n
            } catch (c) {
            }
        }
    }, css:function (t, e, n, o) {
        var r, i, a, s = ie.camelCase(e);
        return e = ie.cssProps[s] || (ie.cssProps[s] = T(t.style, s)), a = ie.cssHooks[e] || ie.cssHooks[s], a && "get"in a && (i = a.get(t, !0, n)), void 0 === i && (i = nn(t, e, o)), "normal" === i && e in hn && (i = hn[e]), "" === n || n ? (r = parseFloat(i), n === !0 || ie.isNumeric(r) ? r || 0 : i) : i
    }}), ie.each(["height", "width"], function (t, e) {
        ie.cssHooks[e] = {get:function (t, n, o) {
            return n ? 0 === t.offsetWidth && cn.test(ie.css(t, "display")) ? ie.swap(t, fn, function () {
                return A(t, e, o)
            }) : A(t, e, o) : void 0
        }, set:function (t, n, o) {
            var r = o && en(t);
            return P(t, n, o ? R(t, e, o, oe.boxSizing() && "border-box" === ie.css(t, "boxSizing", !1, r), r) : 0)
        }}
    }), oe.opacity || (ie.cssHooks.opacity = {get:function (t, e) {
        return ln.test((e && t.currentStyle ? t.currentStyle.filter : t.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : e ? "1" : ""
    }, set:function (t, e) {
        var n = t.style, o = t.currentStyle, r = ie.isNumeric(e) ? "alpha(opacity=" + 100 * e + ")" : "", i = o && o.filter || n.filter || "";
        n.zoom = 1, (e >= 1 || "" === e) && "" === ie.trim(i.replace(sn, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === e || o && !o.filter) || (n.filter = sn.test(i) ? i.replace(sn, r) : i + " " + r)
    }}), ie.cssHooks.marginRight = k(oe.reliableMarginRight, function (t, e) {
        return e ? ie.swap(t, {display:"inline-block"}, nn, [t, "marginRight"]) : void 0
    }), ie.each({margin:"", padding:"", border:"Width"}, function (t, e) {
        ie.cssHooks[t + e] = {expand:function (n) {
            for (var o = 0, r = {}, i = "string" == typeof n ? n.split(" ") : [n]; 4 > o; o++)r[t + Ne[o] + e] = i[o] || i[o - 2] || i[0];
            return r
        }}, on.test(t) || (ie.cssHooks[t + e].set = P)
    }), ie.fn.extend({css:function (t, e) {
        return Re(this, function (t, e, n) {
            var o, r, i = {}, a = 0;
            if (ie.isArray(e)) {
                for (o = en(t), r = e.length; r > a; a++)i[e[a]] = ie.css(t, e[a], !1, o);
                return i
            }
            return void 0 !== n ? ie.style(t, e, n) : ie.css(t, e)
        }, t, e, arguments.length > 1)
    }, show:function () {
        return N(this, !0)
    }, hide:function () {
        return N(this)
    }, toggle:function (t) {
        return"boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function () {
            Pe(this) ? ie(this).show() : ie(this).hide()
        })
    }}), ie.Tween = O, O.prototype = {constructor:O, init:function (t, e, n, o, r, i) {
        this.elem = t, this.prop = n, this.easing = r || "swing", this.options = e, this.start = this.now = this.cur(), this.end = o, this.unit = i || (ie.cssNumber[n] ? "" : "px")
    }, cur:function () {
        var t = O.propHooks[this.prop];
        return t && t.get ? t.get(this) : O.propHooks._default.get(this)
    }, run:function (t) {
        var e, n = O.propHooks[this.prop];
        return this.pos = e = this.options.duration ? ie.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : t, this.now = (this.end - this.start) * e + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : O.propHooks._default.set(this), this
    }}, O.prototype.init.prototype = O.prototype, O.propHooks = {_default:{get:function (t) {
        var e;
        return null == t.elem[t.prop] || t.elem.style && null != t.elem.style[t.prop] ? (e = ie.css(t.elem, t.prop, ""), e && "auto" !== e ? e : 0) : t.elem[t.prop]
    }, set:function (t) {
        ie.fx.step[t.prop] ? ie.fx.step[t.prop](t) : t.elem.style && (null != t.elem.style[ie.cssProps[t.prop]] || ie.cssHooks[t.prop]) ? ie.style(t.elem, t.prop, t.now + t.unit) : t.elem[t.prop] = t.now
    }}}, O.propHooks.scrollTop = O.propHooks.scrollLeft = {set:function (t) {
        t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
    }}, ie.easing = {linear:function (t) {
        return t
    }, swing:function (t) {
        return.5 - Math.cos(t * Math.PI) / 2
    }}, ie.fx = O.prototype.init, ie.fx.step = {};
    var gn, mn, vn = /^(?:toggle|show|hide)$/, yn = new RegExp("^(?:([+-])=|)(" + Te + ")([a-z%]*)$", "i"), bn = /queueHooks$/, wn = [L], xn = {"*":[function (t, e) {
        var n = this.createTween(t, e), o = n.cur(), r = yn.exec(e), i = r && r[3] || (ie.cssNumber[t] ? "" : "px"), a = (ie.cssNumber[t] || "px" !== i && +o) && yn.exec(ie.css(n.elem, t)), s = 1, l = 20;
        if (a && a[3] !== i) {
            i = i || a[3], r = r || [], a = +o || 1;
            do s = s || ".5", a /= s, ie.style(n.elem, t, a + i); while (s !== (s = n.cur() / o) && 1 !== s && --l)
        }
        return r && (a = n.start = +a || +o || 0, n.unit = i, n.end = r[1] ? a + (r[1] + 1) * r[2] : +r[2]), n
    }]};
    ie.Animation = ie.extend(B, {tweener:function (t, e) {
        ie.isFunction(t) ? (e = t, t = ["*"]) : t = t.split(" ");
        for (var n, o = 0, r = t.length; r > o; o++)n = t[o], xn[n] = xn[n] || [], xn[n].unshift(e)
    }, prefilter:function (t, e) {
        e ? wn.unshift(t) : wn.push(t)
    }}), ie.speed = function (t, e, n) {
        var o = t && "object" == typeof t ? ie.extend({}, t) : {complete:n || !n && e || ie.isFunction(t) && t, duration:t, easing:n && e || e && !ie.isFunction(e) && e};
        return o.duration = ie.fx.off ? 0 : "number" == typeof o.duration ? o.duration : o.duration in ie.fx.speeds ? ie.fx.speeds[o.duration] : ie.fx.speeds._default, (null == o.queue || o.queue === !0) && (o.queue = "fx"), o.old = o.complete, o.complete = function () {
            ie.isFunction(o.old) && o.old.call(this), o.queue && ie.dequeue(this, o.queue)
        }, o
    }, ie.fn.extend({fadeTo:function (t, e, n, o) {
        return this.filter(Pe).css("opacity", 0).show().end().animate({opacity:e}, t, n, o)
    }, animate:function (t, e, n, o) {
        var r = ie.isEmptyObject(t), i = ie.speed(e, n, o), a = function () {
            var e = B(this, ie.extend({}, t), i);
            (r || ie._data(this, "finish")) && e.stop(!0)
        };
        return a.finish = a, r || i.queue === !1 ? this.each(a) : this.queue(i.queue, a)
    }, stop:function (t, e, n) {
        var o = function (t) {
            var e = t.stop;
            delete t.stop, e(n)
        };
        return"string" != typeof t && (n = e, e = t, t = void 0), e && t !== !1 && this.queue(t || "fx", []), this.each(function () {
            var e = !0, r = null != t && t + "queueHooks", i = ie.timers, a = ie._data(this);
            if (r)a[r] && a[r].stop && o(a[r]); else for (r in a)a[r] && a[r].stop && bn.test(r) && o(a[r]);
            for (r = i.length; r--;)i[r].elem !== this || null != t && i[r].queue !== t || (i[r].anim.stop(n), e = !1, i.splice(r, 1));
            (e || !n) && ie.dequeue(this, t)
        })
    }, finish:function (t) {
        return t !== !1 && (t = t || "fx"), this.each(function () {
            var e, n = ie._data(this), o = n[t + "queue"], r = n[t + "queueHooks"], i = ie.timers, a = o ? o.length : 0;
            for (n.finish = !0, ie.queue(this, t, []), r && r.stop && r.stop.call(this, !0), e = i.length; e--;)i[e].elem === this && i[e].queue === t && (i[e].anim.stop(!0), i.splice(e, 1));
            for (e = 0; a > e; e++)o[e] && o[e].finish && o[e].finish.call(this);
            delete n.finish
        })
    }}), ie.each(["toggle", "show", "hide"], function (t, e) {
        var n = ie.fn[e];
        ie.fn[e] = function (t, o, r) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(I(e, !0), t, o, r)
        }
    }), ie.each({slideDown:I("show"), slideUp:I("hide"), slideToggle:I("toggle"), fadeIn:{opacity:"show"}, fadeOut:{opacity:"hide"}, fadeToggle:{opacity:"toggle"}}, function (t, e) {
        ie.fn[t] = function (t, n, o) {
            return this.animate(e, t, n, o)
        }
    }), ie.timers = [], ie.fx.tick = function () {
        var t, e = ie.timers, n = 0;
        for (gn = ie.now(); n < e.length; n++)t = e[n], t() || e[n] !== t || e.splice(n--, 1);
        e.length || ie.fx.stop(), gn = void 0
    }, ie.fx.timer = function (t) {
        ie.timers.push(t), t() ? ie.fx.start() : ie.timers.pop()
    }, ie.fx.interval = 13, ie.fx.start = function () {
        mn || (mn = setInterval(ie.fx.tick, ie.fx.interval))
    }, ie.fx.stop = function () {
        clearInterval(mn), mn = null
    }, ie.fx.speeds = {slow:600, fast:200, _default:400}, ie.fn.delay = function (t, e) {
        return t = ie.fx ? ie.fx.speeds[t] || t : t, e = e || "fx", this.queue(e, function (e, n) {
            var o = setTimeout(e, t);
            n.stop = function () {
                clearTimeout(o)
            }
        })
    }, function () {
        var t, e, n, o, r = ge.createElement("div");
        r.setAttribute("className", "t"), r.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", t = r.getElementsByTagName("a")[0], n = ge.createElement("select"), o = n.appendChild(ge.createElement("option")), e = r.getElementsByTagName("input")[0], t.style.cssText = "top:1px", oe.getSetAttribute = "t" !== r.className, oe.style = /top/.test(t.getAttribute("style")), oe.hrefNormalized = "/a" === t.getAttribute("href"), oe.checkOn = !!e.value, oe.optSelected = o.selected, oe.enctype = !!ge.createElement("form").enctype, n.disabled = !0, oe.optDisabled = !o.disabled, e = ge.createElement("input"), e.setAttribute("value", ""), oe.input = "" === e.getAttribute("value"), e.value = "t", e.setAttribute("type", "radio"), oe.radioValue = "t" === e.value, t = e = n = o = r = null
    }();
    var Cn = /\r/g;
    ie.fn.extend({val:function (t) {
        var e, n, o, r = this[0];
        {
            if (arguments.length)return o = ie.isFunction(t), this.each(function (n) {
                var r;
                1 === this.nodeType && (r = o ? t.call(this, n, ie(this).val()) : t, null == r ? r = "" : "number" == typeof r ? r += "" : ie.isArray(r) && (r = ie.map(r, function (t) {
                    return null == t ? "" : t + ""
                })), e = ie.valHooks[this.type] || ie.valHooks[this.nodeName.toLowerCase()], e && "set"in e && void 0 !== e.set(this, r, "value") || (this.value = r))
            });
            if (r)return e = ie.valHooks[r.type] || ie.valHooks[r.nodeName.toLowerCase()], e && "get"in e && void 0 !== (n = e.get(r, "value")) ? n : (n = r.value, "string" == typeof n ? n.replace(Cn, "") : null == n ? "" : n)
        }
    }}), ie.extend({valHooks:{option:{get:function (t) {
        var e = ie.find.attr(t, "value");
        return null != e ? e : ie.text(t)
    }}, select:{get:function (t) {
        for (var e, n, o = t.options, r = t.selectedIndex, i = "select-one" === t.type || 0 > r, a = i ? null : [], s = i ? r + 1 : o.length, l = 0 > r ? s : i ? r : 0; s > l; l++)if (n = o[l], !(!n.selected && l !== r || (oe.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && ie.nodeName(n.parentNode, "optgroup"))) {
            if (e = ie(n).val(), i)return e;
            a.push(e)
        }
        return a
    }, set:function (t, e) {
        for (var n, o, r = t.options, i = ie.makeArray(e), a = r.length; a--;)if (o = r[a], ie.inArray(ie.valHooks.option.get(o), i) >= 0)try {
            o.selected = n = !0
        } catch (s) {
            o.scrollHeight
        } else o.selected = !1;
        return n || (t.selectedIndex = -1), r
    }}}}), ie.each(["radio", "checkbox"], function () {
        ie.valHooks[this] = {set:function (t, e) {
            return ie.isArray(e) ? t.checked = ie.inArray(ie(t).val(), e) >= 0 : void 0
        }}, oe.checkOn || (ie.valHooks[this].get = function (t) {
            return null === t.getAttribute("value") ? "on" : t.value
        })
    });
    var En, Sn, _n = ie.expr.attrHandle, kn = /^(?:checked|selected)$/i, Tn = oe.getSetAttribute, Nn = oe.input;
    ie.fn.extend({attr:function (t, e) {
        return Re(this, ie.attr, t, e, arguments.length > 1)
    }, removeAttr:function (t) {
        return this.each(function () {
            ie.removeAttr(this, t)
        })
    }}), ie.extend({attr:function (t, e, n) {
        var o, r, i = t.nodeType;
        if (t && 3 !== i && 8 !== i && 2 !== i)return typeof t.getAttribute === Se ? ie.prop(t, e, n) : (1 === i && ie.isXMLDoc(t) || (e = e.toLowerCase(), o = ie.attrHooks[e] || (ie.expr.match.bool.test(e) ? Sn : En)), void 0 === n ? o && "get"in o && null !== (r = o.get(t, e)) ? r : (r = ie.find.attr(t, e), null == r ? void 0 : r) : null !== n ? o && "set"in o && void 0 !== (r = o.set(t, n, e)) ? r : (t.setAttribute(e, n + ""), n) : (ie.removeAttr(t, e), void 0))
    }, removeAttr:function (t, e) {
        var n, o, r = 0, i = e && e.match(we);
        if (i && 1 === t.nodeType)for (; n = i[r++];)o = ie.propFix[n] || n, ie.expr.match.bool.test(n) ? Nn && Tn || !kn.test(n) ? t[o] = !1 : t[ie.camelCase("default-" + n)] = t[o] = !1 : ie.attr(t, n, ""), t.removeAttribute(Tn ? n : o)
    }, attrHooks:{type:{set:function (t, e) {
        if (!oe.radioValue && "radio" === e && ie.nodeName(t, "input")) {
            var n = t.value;
            return t.setAttribute("type", e), n && (t.value = n), e
        }
    }}}}), Sn = {set:function (t, e, n) {
        return e === !1 ? ie.removeAttr(t, n) : Nn && Tn || !kn.test(n) ? t.setAttribute(!Tn && ie.propFix[n] || n, n) : t[ie.camelCase("default-" + n)] = t[n] = !0, n
    }}, ie.each(ie.expr.match.bool.source.match(/\w+/g), function (t, e) {
        var n = _n[e] || ie.find.attr;
        _n[e] = Nn && Tn || !kn.test(e) ? function (t, e, o) {
            var r, i;
            return o || (i = _n[e], _n[e] = r, r = null != n(t, e, o) ? e.toLowerCase() : null, _n[e] = i), r
        } : function (t, e, n) {
            return n ? void 0 : t[ie.camelCase("default-" + e)] ? e.toLowerCase() : null
        }
    }), Nn && Tn || (ie.attrHooks.value = {set:function (t, e, n) {
        return ie.nodeName(t, "input") ? (t.defaultValue = e, void 0) : En && En.set(t, e, n)
    }}), Tn || (En = {set:function (t, e, n) {
        var o = t.getAttributeNode(n);
        return o || t.setAttributeNode(o = t.ownerDocument.createAttribute(n)), o.value = e += "", "value" === n || e === t.getAttribute(n) ? e : void 0
    }}, _n.id = _n.name = _n.coords = function (t, e, n) {
        var o;
        return n ? void 0 : (o = t.getAttributeNode(e)) && "" !== o.value ? o.value : null
    }, ie.valHooks.button = {get:function (t, e) {
        var n = t.getAttributeNode(e);
        return n && n.specified ? n.value : void 0
    }, set:En.set}, ie.attrHooks.contenteditable = {set:function (t, e, n) {
        En.set(t, "" === e ? !1 : e, n)
    }}, ie.each(["width", "height"], function (t, e) {
        ie.attrHooks[e] = {set:function (t, n) {
            return"" === n ? (t.setAttribute(e, "auto"), n) : void 0
        }}
    })), oe.style || (ie.attrHooks.style = {get:function (t) {
        return t.style.cssText || void 0
    }, set:function (t, e) {
        return t.style.cssText = e + ""
    }});
    var Pn = /^(?:input|select|textarea|button|object)$/i, Rn = /^(?:a|area)$/i;
    ie.fn.extend({prop:function (t, e) {
        return Re(this, ie.prop, t, e, arguments.length > 1)
    }, removeProp:function (t) {
        return t = ie.propFix[t] || t, this.each(function () {
            try {
                this[t] = void 0, delete this[t]
            } catch (e) {
            }
        })
    }}), ie.extend({propFix:{"for":"htmlFor", "class":"className"}, prop:function (t, e, n) {
        var o, r, i, a = t.nodeType;
        if (t && 3 !== a && 8 !== a && 2 !== a)return i = 1 !== a || !ie.isXMLDoc(t), i && (e = ie.propFix[e] || e, r = ie.propHooks[e]), void 0 !== n ? r && "set"in r && void 0 !== (o = r.set(t, n, e)) ? o : t[e] = n : r && "get"in r && null !== (o = r.get(t, e)) ? o : t[e]
    }, propHooks:{tabIndex:{get:function (t) {
        var e = ie.find.attr(t, "tabindex");
        return e ? parseInt(e, 10) : Pn.test(t.nodeName) || Rn.test(t.nodeName) && t.href ? 0 : -1
    }}}}), oe.hrefNormalized || ie.each(["href", "src"], function (t, e) {
        ie.propHooks[e] = {get:function (t) {
            return t.getAttribute(e, 4)
        }}
    }), oe.optSelected || (ie.propHooks.selected = {get:function (t) {
        var e = t.parentNode;
        return e && (e.selectedIndex, e.parentNode && e.parentNode.selectedIndex), null
    }}), ie.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function () {
        ie.propFix[this.toLowerCase()] = this
    }), oe.enctype || (ie.propFix.enctype = "encoding");
    var An = /[\t\r\n\f]/g;
    ie.fn.extend({addClass:function (t) {
        var e, n, o, r, i, a, s = 0, l = this.length, c = "string" == typeof t && t;
        if (ie.isFunction(t))return this.each(function (e) {
            ie(this).addClass(t.call(this, e, this.className))
        });
        if (c)for (e = (t || "").match(we) || []; l > s; s++)if (n = this[s], o = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(An, " ") : " ")) {
            for (i = 0; r = e[i++];)o.indexOf(" " + r + " ") < 0 && (o += r + " ");
            a = ie.trim(o), n.className !== a && (n.className = a)
        }
        return this
    }, removeClass:function (t) {
        var e, n, o, r, i, a, s = 0, l = this.length, c = 0 === arguments.length || "string" == typeof t && t;
        if (ie.isFunction(t))return this.each(function (e) {
            ie(this).removeClass(t.call(this, e, this.className))
        });
        if (c)for (e = (t || "").match(we) || []; l > s; s++)if (n = this[s], o = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(An, " ") : "")) {
            for (i = 0; r = e[i++];)for (; o.indexOf(" " + r + " ") >= 0;)o = o.replace(" " + r + " ", " ");
            a = t ? ie.trim(o) : "", n.className !== a && (n.className = a)
        }
        return this
    }, toggleClass:function (t, e) {
        var n = typeof t;
        return"boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : ie.isFunction(t) ? this.each(function (n) {
            ie(this).toggleClass(t.call(this, n, this.className, e), e)
        }) : this.each(function () {
            if ("string" === n)for (var e, o = 0, r = ie(this), i = t.match(we) || []; e = i[o++];)r.hasClass(e) ? r.removeClass(e) : r.addClass(e); else(n === Se || "boolean" === n) && (this.className && ie._data(this, "__className__", this.className), this.className = this.className || t === !1 ? "" : ie._data(this, "__className__") || "")
        })
    }, hasClass:function (t) {
        for (var e = " " + t + " ", n = 0, o = this.length; o > n; n++)if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(An, " ").indexOf(e) >= 0)return!0;
        return!1
    }}), ie.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function (t, e) {
        ie.fn[e] = function (t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }), ie.fn.extend({hover:function (t, e) {
        return this.mouseenter(t).mouseleave(e || t)
    }, bind:function (t, e, n) {
        return this.on(t, null, e, n)
    }, unbind:function (t, e) {
        return this.off(t, null, e)
    }, delegate:function (t, e, n, o) {
        return this.on(e, t, n, o)
    }, undelegate:function (t, e, n) {
        return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
    }});
    var On = ie.now(), Dn = /\?/, In = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    ie.parseJSON = function (e) {
        if (t.JSON && t.JSON.parse)return t.JSON.parse(e + "");
        var n, o = null, r = ie.trim(e + "");
        return r && !ie.trim(r.replace(In, function (t, e, r, i) {
            return n && e && (o = 0), 0 === o ? t : (n = r || e, o += !i - !r, "")
        })) ? Function("return " + r)() : ie.error("Invalid JSON: " + e)
    }, ie.parseXML = function (e) {
        var n, o;
        if (!e || "string" != typeof e)return null;
        try {
            t.DOMParser ? (o = new DOMParser, n = o.parseFromString(e, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(e))
        } catch (r) {
            n = void 0
        }
        return n && n.documentElement && !n.getElementsByTagName("parsererror").length || ie.error("Invalid XML: " + e), n
    };
    var $n, Ln, Mn = /#.*$/, Bn = /([?&])_=[^&]*/, Fn = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm, jn = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/, Hn = /^(?:GET|HEAD)$/, Wn = /^\/\//, qn = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/, Xn = {}, zn = {}, Gn = "*/".concat("*");
    try {
        Ln = location.href
    } catch (Un) {
        Ln = ge.createElement("a"), Ln.href = "", Ln = Ln.href
    }
    $n = qn.exec(Ln.toLowerCase()) || [], ie.extend({active:0, lastModified:{}, etag:{}, ajaxSettings:{url:Ln, type:"GET", isLocal:jn.test($n[1]), global:!0, processData:!0, async:!0, contentType:"application/x-www-form-urlencoded; charset=UTF-8", accepts:{"*":Gn, text:"text/plain", html:"text/html", xml:"application/xml, text/xml", json:"application/json, text/javascript"}, contents:{xml:/xml/, html:/html/, json:/json/}, responseFields:{xml:"responseXML", text:"responseText", json:"responseJSON"}, converters:{"* text":String, "text html":!0, "text json":ie.parseJSON, "text xml":ie.parseXML}, flatOptions:{url:!0, context:!0}}, ajaxSetup:function (t, e) {
        return e ? H(H(t, ie.ajaxSettings), e) : H(ie.ajaxSettings, t)
    }, ajaxPrefilter:F(Xn), ajaxTransport:F(zn), ajax:function (t, e) {
        function n(t, e, n, o) {
            var r, u, v, y, w, C = e;
            2 !== b && (b = 2, s && clearTimeout(s), c = void 0, a = o || "", x.readyState = t > 0 ? 4 : 0, r = t >= 200 && 300 > t || 304 === t, n && (y = W(d, x, n)), y = q(d, y, x, r), r ? (d.ifModified && (w = x.getResponseHeader("Last-Modified"), w && (ie.lastModified[i] = w), w = x.getResponseHeader("etag"), w && (ie.etag[i] = w)), 204 === t || "HEAD" === d.type ? C = "nocontent" : 304 === t ? C = "notmodified" : (C = y.state, u = y.data, v = y.error, r = !v)) : (v = C, (t || !C) && (C = "error", 0 > t && (t = 0))), x.status = t, x.statusText = (e || C) + "", r ? p.resolveWith(f, [u, C, x]) : p.rejectWith(f, [x, C, v]), x.statusCode(m), m = void 0, l && h.trigger(r ? "ajaxSuccess" : "ajaxError", [x, d, r ? u : v]), g.fireWith(f, [x, C]), l && (h.trigger("ajaxComplete", [x, d]), --ie.active || ie.event.trigger("ajaxStop")))
        }

        "object" == typeof t && (e = t, t = void 0), e = e || {};
        var o, r, i, a, s, l, c, u, d = ie.ajaxSetup({}, e), f = d.context || d, h = d.context && (f.nodeType || f.jquery) ? ie(f) : ie.event, p = ie.Deferred(), g = ie.Callbacks("once memory"), m = d.statusCode || {}, v = {}, y = {}, b = 0, w = "canceled", x = {readyState:0, getResponseHeader:function (t) {
            var e;
            if (2 === b) {
                if (!u)for (u = {}; e = Fn.exec(a);)u[e[1].toLowerCase()] = e[2];
                e = u[t.toLowerCase()]
            }
            return null == e ? null : e
        }, getAllResponseHeaders:function () {
            return 2 === b ? a : null
        }, setRequestHeader:function (t, e) {
            var n = t.toLowerCase();
            return b || (t = y[n] = y[n] || t, v[t] = e), this
        }, overrideMimeType:function (t) {
            return b || (d.mimeType = t), this
        }, statusCode:function (t) {
            var e;
            if (t)if (2 > b)for (e in t)m[e] = [m[e], t[e]]; else x.always(t[x.status]);
            return this
        }, abort:function (t) {
            var e = t || w;
            return c && c.abort(e), n(0, e), this
        }};
        if (p.promise(x).complete = g.add, x.success = x.done, x.error = x.fail, d.url = ((t || d.url || Ln) + "").replace(Mn, "").replace(Wn, $n[1] + "//"), d.type = e.method || e.type || d.method || d.type, d.dataTypes = ie.trim(d.dataType || "*").toLowerCase().match(we) || [""], null == d.crossDomain && (o = qn.exec(d.url.toLowerCase()), d.crossDomain = !(!o || o[1] === $n[1] && o[2] === $n[2] && (o[3] || ("http:" === o[1] ? "80" : "443")) === ($n[3] || ("http:" === $n[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = ie.param(d.data, d.traditional)), j(Xn, d, e, x), 2 === b)return x;
        l = d.global, l && 0 === ie.active++ && ie.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Hn.test(d.type), i = d.url, d.hasContent || (d.data && (i = d.url += (Dn.test(i) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Bn.test(i) ? i.replace(Bn, "$1_=" + On++) : i + (Dn.test(i) ? "&" : "?") + "_=" + On++)), d.ifModified && (ie.lastModified[i] && x.setRequestHeader("If-Modified-Since", ie.lastModified[i]), ie.etag[i] && x.setRequestHeader("If-None-Match", ie.etag[i])), (d.data && d.hasContent && d.contentType !== !1 || e.contentType) && x.setRequestHeader("Content-Type", d.contentType), x.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Gn + "; q=0.01" : "") : d.accepts["*"]);
        for (r in d.headers)x.setRequestHeader(r, d.headers[r]);
        if (d.beforeSend && (d.beforeSend.call(f, x, d) === !1 || 2 === b))return x.abort();
        w = "abort";
        for (r in{success:1, error:1, complete:1})x[r](d[r]);
        if (c = j(zn, d, e, x)) {
            x.readyState = 1, l && h.trigger("ajaxSend", [x, d]), d.async && d.timeout > 0 && (s = setTimeout(function () {
                x.abort("timeout")
            }, d.timeout));
            try {
                b = 1, c.send(v, n)
            } catch (C) {
                if (!(2 > b))throw C;
                n(-1, C)
            }
        } else n(-1, "No Transport");
        return x
    }, getJSON:function (t, e, n) {
        return ie.get(t, e, n, "json")
    }, getScript:function (t, e) {
        return ie.get(t, void 0, e, "script")
    }}), ie.each(["get", "post"], function (t, e) {
        ie[e] = function (t, n, o, r) {
            return ie.isFunction(n) && (r = r || o, o = n, n = void 0), ie.ajax({url:t, type:e, dataType:r, data:n, success:o})
        }
    }), ie.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function (t, e) {
        ie.fn[e] = function (t) {
            return this.on(e, t)
        }
    }), ie._evalUrl = function (t) {
        return ie.ajax({url:t, type:"GET", dataType:"script", async:!1, global:!1, "throws":!0})
    }, ie.fn.extend({wrapAll:function (t) {
        if (ie.isFunction(t))return this.each(function (e) {
            ie(this).wrapAll(t.call(this, e))
        });
        if (this[0]) {
            var e = ie(t, this[0].ownerDocument).eq(0).clone(!0);
            this[0].parentNode && e.insertBefore(this[0]), e.map(function () {
                for (var t = this; t.firstChild && 1 === t.firstChild.nodeType;)t = t.firstChild;
                return t
            }).append(this)
        }
        return this
    }, wrapInner:function (t) {
        return ie.isFunction(t) ? this.each(function (e) {
            ie(this).wrapInner(t.call(this, e))
        }) : this.each(function () {
            var e = ie(this), n = e.contents();
            n.length ? n.wrapAll(t) : e.append(t)
        })
    }, wrap:function (t) {
        var e = ie.isFunction(t);
        return this.each(function (n) {
            ie(this).wrapAll(e ? t.call(this, n) : t)
        })
    }, unwrap:function () {
        return this.parent().each(function () {
            ie.nodeName(this, "body") || ie(this).replaceWith(this.childNodes)
        }).end()
    }}), ie.expr.filters.hidden = function (t) {
        return t.offsetWidth <= 0 && t.offsetHeight <= 0 || !oe.reliableHiddenOffsets() && "none" === (t.style && t.style.display || ie.css(t, "display"))
    }, ie.expr.filters.visible = function (t) {
        return!ie.expr.filters.hidden(t)
    };
    var Vn = /%20/g, Yn = /\[\]$/, Qn = /\r?\n/g, Kn = /^(?:submit|button|image|reset|file)$/i, Jn = /^(?:input|select|textarea|keygen)/i;
    ie.param = function (t, e) {
        var n, o = [], r = function (t, e) {
            e = ie.isFunction(e) ? e() : null == e ? "" : e, o[o.length] = encodeURIComponent(t) + "=" + encodeURIComponent(e)
        };
        if (void 0 === e && (e = ie.ajaxSettings && ie.ajaxSettings.traditional), ie.isArray(t) || t.jquery && !ie.isPlainObject(t))ie.each(t, function () {
            r(this.name, this.value)
        }); else for (n in t)X(n, t[n], e, r);
        return o.join("&").replace(Vn, "+")
    }, ie.fn.extend({serialize:function () {
        return ie.param(this.serializeArray())
    }, serializeArray:function () {
        return this.map(function () {
            var t = ie.prop(this, "elements");
            return t ? ie.makeArray(t) : this
        }).filter(function () {
            var t = this.type;
            return this.name && !ie(this).is(":disabled") && Jn.test(this.nodeName) && !Kn.test(t) && (this.checked || !Ae.test(t))
        }).map(function (t, e) {
            var n = ie(this).val();
            return null == n ? null : ie.isArray(n) ? ie.map(n, function (t) {
                return{name:e.name, value:t.replace(Qn, "\r\n")}
            }) : {name:e.name, value:n.replace(Qn, "\r\n")}
        }).get()
    }}), ie.ajaxSettings.xhr = void 0 !== t.ActiveXObject ? function () {
        return!this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && z() || G()
    } : z;
    var Zn = 0, to = {}, eo = ie.ajaxSettings.xhr();
    t.ActiveXObject && ie(t).on("unload", function () {
        for (var t in to)to[t](void 0, !0)
    }), oe.cors = !!eo && "withCredentials"in eo, eo = oe.ajax = !!eo, eo && ie.ajaxTransport(function (t) {
        if (!t.crossDomain || oe.cors) {
            var e;
            return{send:function (n, o) {
                var r, i = t.xhr(), a = ++Zn;
                if (i.open(t.type, t.url, t.async, t.username, t.password), t.xhrFields)for (r in t.xhrFields)i[r] = t.xhrFields[r];
                t.mimeType && i.overrideMimeType && i.overrideMimeType(t.mimeType), t.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                for (r in n)void 0 !== n[r] && i.setRequestHeader(r, n[r] + "");
                i.send(t.hasContent && t.data || null), e = function (n, r) {
                    var s, l, c;
                    if (e && (r || 4 === i.readyState))if (delete to[a], e = void 0, i.onreadystatechange = ie.noop, r)4 !== i.readyState && i.abort(); else {
                        c = {}, s = i.status, "string" == typeof i.responseText && (c.text = i.responseText);
                        try {
                            l = i.statusText
                        } catch (u) {
                            l = ""
                        }
                        s || !t.isLocal || t.crossDomain ? 1223 === s && (s = 204) : s = c.text ? 200 : 404
                    }
                    c && o(s, l, c, i.getAllResponseHeaders())
                }, t.async ? 4 === i.readyState ? setTimeout(e) : i.onreadystatechange = to[a] = e : e()
            }, abort:function () {
                e && e(void 0, !0)
            }}
        }
    }), ie.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"}, contents:{script:/(?:java|ecma)script/}, converters:{"text script":function (t) {
        return ie.globalEval(t), t
    }}}), ie.ajaxPrefilter("script", function (t) {
        void 0 === t.cache && (t.cache = !1), t.crossDomain && (t.type = "GET", t.global = !1)
    }), ie.ajaxTransport("script", function (t) {
        if (t.crossDomain) {
            var e, n = ge.head || ie("head")[0] || ge.documentElement;
            return{send:function (o, r) {
                e = ge.createElement("script"), e.async = !0, t.scriptCharset && (e.charset = t.scriptCharset), e.src = t.url, e.onload = e.onreadystatechange = function (t, n) {
                    (n || !e.readyState || /loaded|complete/.test(e.readyState)) && (e.onload = e.onreadystatechange = null, e.parentNode && e.parentNode.removeChild(e), e = null, n || r(200, "success"))
                }, n.insertBefore(e, n.firstChild)
            }, abort:function () {
                e && e.onload(void 0, !0)
            }}
        }
    });
    var no = [], oo = /(=)\?(?=&|$)|\?\?/;
    ie.ajaxSetup({jsonp:"callback", jsonpCallback:function () {
        var t = no.pop() || ie.expando + "_" + On++;
        return this[t] = !0, t
    }}), ie.ajaxPrefilter("json jsonp", function (e, n, o) {
        var r, i, a, s = e.jsonp !== !1 && (oo.test(e.url) ? "url" : "string" == typeof e.data && !(e.contentType || "").indexOf("application/x-www-form-urlencoded") && oo.test(e.data) && "data");
        return s || "jsonp" === e.dataTypes[0] ? (r = e.jsonpCallback = ie.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback, s ? e[s] = e[s].replace(oo, "$1" + r) : e.jsonp !== !1 && (e.url += (Dn.test(e.url) ? "&" : "?") + e.jsonp + "=" + r), e.converters["script json"] = function () {
            return a || ie.error(r + " was not called"), a[0]
        }, e.dataTypes[0] = "json", i = t[r], t[r] = function () {
            a = arguments
        }, o.always(function () {
            t[r] = i, e[r] && (e.jsonpCallback = n.jsonpCallback, no.push(r)), a && ie.isFunction(i) && i(a[0]), a = i = void 0
        }), "script") : void 0
    }), ie.parseHTML = function (t, e, n) {
        if (!t || "string" != typeof t)return null;
        "boolean" == typeof e && (n = e, e = !1), e = e || ge;
        var o = fe.exec(t), r = !n && [];
        return o ? [e.createElement(o[1])] : (o = ie.buildFragment([t], e, r), r && r.length && ie(r).remove(), ie.merge([], o.childNodes))
    };
    var ro = ie.fn.load;
    ie.fn.load = function (t, e, n) {
        if ("string" != typeof t && ro)return ro.apply(this, arguments);
        var o, r, i, a = this, s = t.indexOf(" ");
        return s >= 0 && (o = t.slice(s, t.length), t = t.slice(0, s)), ie.isFunction(e) ? (n = e, e = void 0) : e && "object" == typeof e && (i = "POST"), a.length > 0 && ie.ajax({url:t, type:i, dataType:"html", data:e}).done(function (t) {
            r = arguments, a.html(o ? ie("<div>").append(ie.parseHTML(t)).find(o) : t)
        }).complete(n && function (t, e) {
            a.each(n, r || [t.responseText, e, t])
        }), this
    }, ie.expr.filters.animated = function (t) {
        return ie.grep(ie.timers,function (e) {
            return t === e.elem
        }).length
    };
    var io = t.document.documentElement;
    ie.offset = {setOffset:function (t, e, n) {
        var o, r, i, a, s, l, c, u = ie.css(t, "position"), d = ie(t), f = {};
        "static" === u && (t.style.position = "relative"), s = d.offset(), i = ie.css(t, "top"), l = ie.css(t, "left"), c = ("absolute" === u || "fixed" === u) && ie.inArray("auto", [i, l]) > -1, c ? (o = d.position(), a = o.top, r = o.left) : (a = parseFloat(i) || 0, r = parseFloat(l) || 0), ie.isFunction(e) && (e = e.call(t, n, s)), null != e.top && (f.top = e.top - s.top + a), null != e.left && (f.left = e.left - s.left + r), "using"in e ? e.using.call(t, f) : d.css(f)
    }}, ie.fn.extend({offset:function (t) {
        if (arguments.length)return void 0 === t ? this : this.each(function (e) {
            ie.offset.setOffset(this, t, e)
        });
        var e, n, o = {top:0, left:0}, r = this[0], i = r && r.ownerDocument;
        if (i)return e = i.documentElement, ie.contains(e, r) ? (typeof r.getBoundingClientRect !== Se && (o = r.getBoundingClientRect()), n = U(i), {top:o.top + (n.pageYOffset || e.scrollTop) - (e.clientTop || 0), left:o.left + (n.pageXOffset || e.scrollLeft) - (e.clientLeft || 0)}) : o
    }, position:function () {
        if (this[0]) {
            var t, e, n = {top:0, left:0}, o = this[0];
            return"fixed" === ie.css(o, "position") ? e = o.getBoundingClientRect() : (t = this.offsetParent(), e = this.offset(), ie.nodeName(t[0], "html") || (n = t.offset()), n.top += ie.css(t[0], "borderTopWidth", !0), n.left += ie.css(t[0], "borderLeftWidth", !0)), {top:e.top - n.top - ie.css(o, "marginTop", !0), left:e.left - n.left - ie.css(o, "marginLeft", !0)}
        }
    }, offsetParent:function () {
        return this.map(function () {
            for (var t = this.offsetParent || io; t && !ie.nodeName(t, "html") && "static" === ie.css(t, "position");)t = t.offsetParent;
            return t || io
        })
    }}), ie.each({scrollLeft:"pageXOffset", scrollTop:"pageYOffset"}, function (t, e) {
        var n = /Y/.test(e);
        ie.fn[t] = function (o) {
            return Re(this, function (t, o, r) {
                var i = U(t);
                return void 0 === r ? i ? e in i ? i[e] : i.document.documentElement[o] : t[o] : (i ? i.scrollTo(n ? ie(i).scrollLeft() : r, n ? r : ie(i).scrollTop()) : t[o] = r, void 0)
            }, t, o, arguments.length, null)
        }
    }), ie.each(["top", "left"], function (t, e) {
        ie.cssHooks[e] = k(oe.pixelPosition, function (t, n) {
            return n ? (n = nn(t, e), rn.test(n) ? ie(t).position()[e] + "px" : n) : void 0
        })
    }), ie.each({Height:"height", Width:"width"}, function (t, e) {
        ie.each({padding:"inner" + t, content:e, "":"outer" + t}, function (n, o) {
            ie.fn[o] = function (o, r) {
                var i = arguments.length && (n || "boolean" != typeof o), a = n || (o === !0 || r === !0 ? "margin" : "border");
                return Re(this, function (e, n, o) {
                    var r;
                    return ie.isWindow(e) ? e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement, Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === o ? ie.css(e, n, a) : ie.style(e, n, o, a)
                }, e, i ? o : void 0, i, null)
            }
        })
    }), ie.fn.size = function () {
        return this.length
    }, ie.fn.andSelf = ie.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function () {
        return ie
    });
    var ao = t.jQuery, so = t.$;
    return ie.noConflict = function (e) {
        return t.$ === ie && (t.$ = so), e && t.jQuery === ie && (t.jQuery = ao), ie
    }, typeof e === Se && (t.jQuery = t.$ = ie), ie
}), function (t, e) {
    t.rails !== e && t.error("jquery-ujs has already been loaded!");
    var n, o = t(document);
    t.rails = n = {linkClickSelector:"a[data-confirm], a[data-method], a[data-remote], a[data-disable-with]", buttonClickSelector:"button[data-remote]", inputChangeSelector:"select[data-remote], input[data-remote], textarea[data-remote]", formSubmitSelector:"form", formInputClickSelector:"form input[type=submit], form input[type=image], form button[type=submit], form button:not([type])", disableSelector:"input[data-disable-with], button[data-disable-with], textarea[data-disable-with]", enableSelector:"input[data-disable-with]:disabled, button[data-disable-with]:disabled, textarea[data-disable-with]:disabled", requiredInputSelector:"input[name][required]:not([disabled]),textarea[name][required]:not([disabled])", fileInputSelector:"input[type=file]", linkDisableSelector:"a[data-disable-with]", CSRFProtection:function (e) {
        var n = t('meta[name="csrf-token"]').attr("content");
        n && e.setRequestHeader("X-CSRF-Token", n)
    }, fire:function (e, n, o) {
        var r = t.Event(n);
        return e.trigger(r, o), r.result !== !1
    }, confirm:function (t) {
        return confirm(t)
    }, ajax:function (e) {
        return t.ajax(e)
    }, href:function (t) {
        return t.attr("href")
    }, handleRemote:function (o) {
        var r, i, a, s, l, c, u, d;
        if (n.fire(o, "ajax:before")) {
            if (s = o.data("cross-domain"), l = s === e ? null : s, c = o.data("with-credentials") || null, u = o.data("type") || t.ajaxSettings && t.ajaxSettings.dataType, o.is("form")) {
                r = o.attr("method"), i = o.attr("action"), a = o.serializeArray();
                var f = o.data("ujs:submit-button");
                f && (a.push(f), o.data("ujs:submit-button", null))
            } else o.is(n.inputChangeSelector) ? (r = o.data("method"), i = o.data("url"), a = o.serialize(), o.data("params") && (a = a + "&" + o.data("params"))) : o.is(n.buttonClickSelector) ? (r = o.data("method") || "get", i = o.data("url"), a = o.serialize(), o.data("params") && (a = a + "&" + o.data("params"))) : (r = o.data("method"), i = n.href(o), a = o.data("params") || null);
            d = {type:r || "GET", data:a, dataType:u, beforeSend:function (t, r) {
                return r.dataType === e && t.setRequestHeader("accept", "*/*;q=0.5, " + r.accepts.script), n.fire(o, "ajax:beforeSend", [t, r])
            }, success:function (t, e, n) {
                o.trigger("ajax:success", [t, e, n])
            }, complete:function (t, e) {
                o.trigger("ajax:complete", [t, e])
            }, error:function (t, e, n) {
                o.trigger("ajax:error", [t, e, n])
            }, crossDomain:l}, c && (d.xhrFields = {withCredentials:c}), i && (d.url = i);
            var h = n.ajax(d);
            return o.trigger("ajax:send", h), h
        }
        return!1
    }, handleMethod:function (o) {
        var r = n.href(o), i = o.data("method"), a = o.attr("target"), s = t("meta[name=csrf-token]").attr("content"), l = t("meta[name=csrf-param]").attr("content"), c = t('<form method="post" action="' + r + '"></form>'), u = '<input name="_method" value="' + i + '" type="hidden" />';
        l !== e && s !== e && (u += '<input name="' + l + '" value="' + s + '" type="hidden" />'), a && c.attr("target", a), c.hide().append(u).appendTo("body"), c.submit()
    }, disableFormElements:function (e) {
        e.find(n.disableSelector).each(function () {
            var e = t(this), n = e.is("button") ? "html" : "val";
            e.data("ujs:enable-with", e[n]()), e[n](e.data("disable-with")), e.prop("disabled", !0)
        })
    }, enableFormElements:function (e) {
        e.find(n.enableSelector).each(function () {
            var e = t(this), n = e.is("button") ? "html" : "val";
            e.data("ujs:enable-with") && e[n](e.data("ujs:enable-with")), e.prop("disabled", !1)
        })
    }, allowAction:function (t) {
        var e, o = t.data("confirm"), r = !1;
        return o ? (n.fire(t, "confirm") && (r = n.confirm(o), e = n.fire(t, "confirm:complete", [r])), r && e) : !0
    }, blankInputs:function (e, n, o) {
        var r, i, a = t(), s = n || "input,textarea", l = e.find(s);
        return l.each(function () {
            if (r = t(this), i = r.is("input[type=checkbox],input[type=radio]") ? r.is(":checked") : r.val(), !i == !o) {
                if (r.is("input[type=radio]") && l.filter('input[type=radio]:checked[name="' + r.attr("name") + '"]').length)return!0;
                a = a.add(r)
            }
        }), a.length ? a : !1
    }, nonBlankInputs:function (t, e) {
        return n.blankInputs(t, e, !0)
    }, stopEverything:function (e) {
        return t(e.target).trigger("ujs:everythingStopped"), e.stopImmediatePropagation(), !1
    }, disableElement:function (t) {
        t.data("ujs:enable-with", t.html()), t.html(t.data("disable-with")), t.bind("click.railsDisable", function (t) {
            return n.stopEverything(t)
        })
    }, enableElement:function (t) {
        t.data("ujs:enable-with") !== e && (t.html(t.data("ujs:enable-with")), t.removeData("ujs:enable-with")), t.unbind("click.railsDisable")
    }}, n.fire(o, "rails:attachBindings") && (t.ajaxPrefilter(function (t, e, o) {
        t.crossDomain || n.CSRFProtection(o)
    }), o.delegate(n.linkDisableSelector, "ajax:complete", function () {
        n.enableElement(t(this))
    }), o.delegate(n.linkClickSelector, "click.rails", function (o) {
        var r = t(this), i = r.data("method"), a = r.data("params");
        if (!n.allowAction(r))return n.stopEverything(o);
        if (r.is(n.linkDisableSelector) && n.disableElement(r), r.data("remote") !== e) {
            if (!(!o.metaKey && !o.ctrlKey || i && "GET" !== i || a))return!0;
            var s = n.handleRemote(r);
            return s === !1 ? n.enableElement(r) : s.error(function () {
                n.enableElement(r)
            }), !1
        }
        return r.data("method") ? (n.handleMethod(r), !1) : void 0
    }), o.delegate(n.buttonClickSelector, "click.rails", function (e) {
        var o = t(this);
        return n.allowAction(o) ? (n.handleRemote(o), !1) : n.stopEverything(e)
    }), o.delegate(n.inputChangeSelector, "change.rails", function (e) {
        var o = t(this);
        return n.allowAction(o) ? (n.handleRemote(o), !1) : n.stopEverything(e)
    }), o.delegate(n.formSubmitSelector, "submit.rails", function (o) {
        var r = t(this), i = r.data("remote") !== e, a = n.blankInputs(r, n.requiredInputSelector), s = n.nonBlankInputs(r, n.fileInputSelector);
        if (!n.allowAction(r))return n.stopEverything(o);
        if (a && r.attr("novalidate") == e && n.fire(r, "ajax:aborted:required", [a]))return n.stopEverything(o);
        if (i) {
            if (s) {
                setTimeout(function () {
                    n.disableFormElements(r)
                }, 13);
                var l = n.fire(r, "ajax:aborted:file", [s]);
                return l || setTimeout(function () {
                    n.enableFormElements(r)
                }, 13), l
            }
            return n.handleRemote(r), !1
        }
        setTimeout(function () {
            n.disableFormElements(r)
        }, 13)
    }), o.delegate(n.formInputClickSelector, "click.rails", function (e) {
        var o = t(this);
        if (!n.allowAction(o))return n.stopEverything(e);
        var r = o.attr("name"), i = r ? {name:r, value:o.val()} : null;
        o.closest("form").data("ujs:submit-button", i)
    }), o.delegate(n.formSubmitSelector, "ajax:beforeSend.rails", function (e) {
        this == e.target && n.disableFormElements(t(this))
    }), o.delegate(n.formSubmitSelector, "ajax:complete.rails", function (e) {
        this == e.target && n.enableFormElements(t(this))
    }), t(function () {
        var e = t("meta[name=csrf-token]").attr("content"), n = t("meta[name=csrf-param]").attr("content");
        t('form input[name="' + n + '"]').val(e)
    }))
}(jQuery), function (t, e) {
    function n(e, n) {
        var r, i, a, s = e.nodeName.toLowerCase();
        return"area" === s ? (r = e.parentNode, i = r.name, e.href && i && "map" === r.nodeName.toLowerCase() ? (a = t("img[usemap=#" + i + "]")[0], !!a && o(a)) : !1) : (/input|select|textarea|button|object/.test(s) ? !e.disabled : "a" === s ? e.href || n : n) && o(e)
    }

    function o(e) {
        return t.expr.filters.visible(e) && !t(e).parents().addBack().filter(function () {
            return"hidden" === t.css(this, "visibility")
        }).length
    }

    var r = 0, i = /^ui-id-\d+$/;
    t.ui = t.ui || {}, t.extend(t.ui, {version:"1.10.3", keyCode:{BACKSPACE:8, COMMA:188, DELETE:46, DOWN:40, END:35, ENTER:13, ESCAPE:27, HOME:36, LEFT:37, NUMPAD_ADD:107, NUMPAD_DECIMAL:110, NUMPAD_DIVIDE:111, NUMPAD_ENTER:108, NUMPAD_MULTIPLY:106, NUMPAD_SUBTRACT:109, PAGE_DOWN:34, PAGE_UP:33, PERIOD:190, RIGHT:39, SPACE:32, TAB:9, UP:38}}), t.fn.extend({focus:function (e) {
        return function (n, o) {
            return"number" == typeof n ? this.each(function () {
                var e = this;
                setTimeout(function () {
                    t(e).focus(), o && o.call(e)
                }, n)
            }) : e.apply(this, arguments)
        }
    }(t.fn.focus), scrollParent:function () {
        var e;
        return e = t.ui.ie && /(static|relative)/.test(this.css("position")) || /absolute/.test(this.css("position")) ? this.parents().filter(function () {
            return/(relative|absolute|fixed)/.test(t.css(this, "position")) && /(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
        }).eq(0) : this.parents().filter(function () {
            return/(auto|scroll)/.test(t.css(this, "overflow") + t.css(this, "overflow-y") + t.css(this, "overflow-x"))
        }).eq(0), /fixed/.test(this.css("position")) || !e.length ? t(document) : e
    }, zIndex:function (n) {
        if (n !== e)return this.css("zIndex", n);
        if (this.length)for (var o, r, i = t(this[0]); i.length && i[0] !== document;) {
            if (o = i.css("position"), ("absolute" === o || "relative" === o || "fixed" === o) && (r = parseInt(i.css("zIndex"), 10), !isNaN(r) && 0 !== r))return r;
            i = i.parent()
        }
        return 0
    }, uniqueId:function () {
        return this.each(function () {
            this.id || (this.id = "ui-id-" + ++r)
        })
    }, removeUniqueId:function () {
        return this.each(function () {
            i.test(this.id) && t(this).removeAttr("id")
        })
    }}), t.extend(t.expr[":"], {data:t.expr.createPseudo ? t.expr.createPseudo(function (e) {
        return function (n) {
            return!!t.data(n, e)
        }
    }) : function (e, n, o) {
        return!!t.data(e, o[3])
    }, focusable:function (e) {
        return n(e, !isNaN(t.attr(e, "tabindex")))
    }, tabbable:function (e) {
        var o = t.attr(e, "tabindex"), r = isNaN(o);
        return(r || o >= 0) && n(e, !r)
    }}), t("<a>").outerWidth(1).jquery || t.each(["Width", "Height"], function (n, o) {
        function r(e, n, o, r) {
            return t.each(i, function () {
                n -= parseFloat(t.css(e, "padding" + this)) || 0, o && (n -= parseFloat(t.css(e, "border" + this + "Width")) || 0), r && (n -= parseFloat(t.css(e, "margin" + this)) || 0)
            }), n
        }

        var i = "Width" === o ? ["Left", "Right"] : ["Top", "Bottom"], a = o.toLowerCase(), s = {innerWidth:t.fn.innerWidth, innerHeight:t.fn.innerHeight, outerWidth:t.fn.outerWidth, outerHeight:t.fn.outerHeight};
        t.fn["inner" + o] = function (n) {
            return n === e ? s["inner" + o].call(this) : this.each(function () {
                t(this).css(a, r(this, n) + "px")
            })
        }, t.fn["outer" + o] = function (e, n) {
            return"number" != typeof e ? s["outer" + o].call(this, e) : this.each(function () {
                t(this).css(a, r(this, e, !0, n) + "px")
            })
        }
    }), t.fn.addBack || (t.fn.addBack = function (t) {
        return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
    }), t("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (t.fn.removeData = function (e) {
        return function (n) {
            return arguments.length ? e.call(this, t.camelCase(n)) : e.call(this)
        }
    }(t.fn.removeData)), t.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()), t.support.selectstart = "onselectstart"in document.createElement("div"), t.fn.extend({disableSelection:function () {
        return this.bind((t.support.selectstart ? "selectstart" : "mousedown") + ".ui-disableSelection", function (t) {
            t.preventDefault()
        })
    }, enableSelection:function () {
        return this.unbind(".ui-disableSelection")
    }}), t.extend(t.ui, {plugin:{add:function (e, n, o) {
        var r, i = t.ui[e].prototype;
        for (r in o)i.plugins[r] = i.plugins[r] || [], i.plugins[r].push([n, o[r]])
    }, call:function (t, e, n) {
        var o, r = t.plugins[e];
        if (r && t.element[0].parentNode && 11 !== t.element[0].parentNode.nodeType)for (o = 0; o < r.length; o++)t.options[r[o][0]] && r[o][1].apply(t.element, n)
    }}, hasScroll:function (e, n) {
        if ("hidden" === t(e).css("overflow"))return!1;
        var o = n && "left" === n ? "scrollLeft" : "scrollTop", r = !1;
        return e[o] > 0 ? !0 : (e[o] = 1, r = e[o] > 0, e[o] = 0, r)
    }})
}(jQuery), function (t, e) {
    var n = 0, o = Array.prototype.slice, r = t.cleanData;
    t.cleanData = function (e) {
        for (var n, o = 0; null != (n = e[o]); o++)try {
            t(n).triggerHandler("remove")
        } catch (i) {
        }
        r(e)
    }, t.widget = function (e, n, o) {
        var r, i, a, s, l = {}, c = e.split(".")[0];
        e = e.split(".")[1], r = c + "-" + e, o || (o = n, n = t.Widget), t.expr[":"][r.toLowerCase()] = function (e) {
            return!!t.data(e, r)
        }, t[c] = t[c] || {}, i = t[c][e], a = t[c][e] = function (t, e) {
            return this._createWidget ? (arguments.length && this._createWidget(t, e), void 0) : new a(t, e)
        }, t.extend(a, i, {version:o.version, _proto:t.extend({}, o), _childConstructors:[]}), s = new n, s.options = t.widget.extend({}, s.options), t.each(o, function (e, o) {
            return t.isFunction(o) ? (l[e] = function () {
                var t = function () {
                    return n.prototype[e].apply(this, arguments)
                }, r = function (t) {
                    return n.prototype[e].apply(this, t)
                };
                return function () {
                    var e, n = this._super, i = this._superApply;
                    return this._super = t, this._superApply = r, e = o.apply(this, arguments), this._super = n, this._superApply = i, e
                }
            }(), void 0) : (l[e] = o, void 0)
        }), a.prototype = t.widget.extend(s, {widgetEventPrefix:i ? s.widgetEventPrefix : e}, l, {constructor:a, namespace:c, widgetName:e, widgetFullName:r}), i ? (t.each(i._childConstructors, function (e, n) {
            var o = n.prototype;
            t.widget(o.namespace + "." + o.widgetName, a, n._proto)
        }), delete i._childConstructors) : n._childConstructors.push(a), t.widget.bridge(e, a)
    }, t.widget.extend = function (n) {
        for (var r, i, a = o.call(arguments, 1), s = 0, l = a.length; l > s; s++)for (r in a[s])i = a[s][r], a[s].hasOwnProperty(r) && i !== e && (n[r] = t.isPlainObject(i) ? t.isPlainObject(n[r]) ? t.widget.extend({}, n[r], i) : t.widget.extend({}, i) : i);
        return n
    }, t.widget.bridge = function (n, r) {
        var i = r.prototype.widgetFullName || n;
        t.fn[n] = function (a) {
            var s = "string" == typeof a, l = o.call(arguments, 1), c = this;
            return a = !s && l.length ? t.widget.extend.apply(null, [a].concat(l)) : a, s ? this.each(function () {
                var o, r = t.data(this, i);
                return r ? t.isFunction(r[a]) && "_" !== a.charAt(0) ? (o = r[a].apply(r, l), o !== r && o !== e ? (c = o && o.jquery ? c.pushStack(o.get()) : o, !1) : void 0) : t.error("no such method '" + a + "' for " + n + " widget instance") : t.error("cannot call methods on " + n + " prior to initialization; " + "attempted to call method '" + a + "'")
            }) : this.each(function () {
                var e = t.data(this, i);
                e ? e.option(a || {})._init() : t.data(this, i, new r(a, this))
            }), c
        }
    }, t.Widget = function () {
    }, t.Widget._childConstructors = [], t.Widget.prototype = {widgetName:"widget", widgetEventPrefix:"", defaultElement:"<div>", options:{disabled:!1, create:null}, _createWidget:function (e, o) {
        o = t(o || this.defaultElement || this)[0], this.element = t(o), this.uuid = n++, this.eventNamespace = "." + this.widgetName + this.uuid, this.options = t.widget.extend({}, this.options, this._getCreateOptions(), e), this.bindings = t(), this.hoverable = t(), this.focusable = t(), o !== this && (t.data(o, this.widgetFullName, this), this._on(!0, this.element, {remove:function (t) {
            t.target === o && this.destroy()
        }}), this.document = t(o.style ? o.ownerDocument : o.document || o), this.window = t(this.document[0].defaultView || this.document[0].parentWindow)), this._create(), this._trigger("create", null, this._getCreateEventData()), this._init()
    }, _getCreateOptions:t.noop, _getCreateEventData:t.noop, _create:t.noop, _init:t.noop, destroy:function () {
        this._destroy(), this.element.unbind(this.eventNamespace).removeData(this.widgetName).removeData(this.widgetFullName).removeData(t.camelCase(this.widgetFullName)), this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled " + "ui-state-disabled"), this.bindings.unbind(this.eventNamespace), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")
    }, _destroy:t.noop, widget:function () {
        return this.element
    }, option:function (n, o) {
        var r, i, a, s = n;
        if (0 === arguments.length)return t.widget.extend({}, this.options);
        if ("string" == typeof n)if (s = {}, r = n.split("."), n = r.shift(), r.length) {
            for (i = s[n] = t.widget.extend({}, this.options[n]), a = 0; a < r.length - 1; a++)i[r[a]] = i[r[a]] || {}, i = i[r[a]];
            if (n = r.pop(), o === e)return i[n] === e ? null : i[n];
            i[n] = o
        } else {
            if (o === e)return this.options[n] === e ? null : this.options[n];
            s[n] = o
        }
        return this._setOptions(s), this
    }, _setOptions:function (t) {
        var e;
        for (e in t)this._setOption(e, t[e]);
        return this
    }, _setOption:function (t, e) {
        return this.options[t] = e, "disabled" === t && (this.widget().toggleClass(this.widgetFullName + "-disabled ui-state-disabled", !!e).attr("aria-disabled", e), this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus")), this
    }, enable:function () {
        return this._setOption("disabled", !1)
    }, disable:function () {
        return this._setOption("disabled", !0)
    }, _on:function (e, n, o) {
        var r, i = this;
        "boolean" != typeof e && (o = n, n = e, e = !1), o ? (n = r = t(n), this.bindings = this.bindings.add(n)) : (o = n, n = this.element, r = this.widget()), t.each(o, function (o, a) {
            function s() {
                return e || i.options.disabled !== !0 && !t(this).hasClass("ui-state-disabled") ? ("string" == typeof a ? i[a] : a).apply(i, arguments) : void 0
            }

            "string" != typeof a && (s.guid = a.guid = a.guid || s.guid || t.guid++);
            var l = o.match(/^(\w+)\s*(.*)$/), c = l[1] + i.eventNamespace, u = l[2];
            u ? r.delegate(u, c, s) : n.bind(c, s)
        })
    }, _off:function (t, e) {
        e = (e || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace, t.unbind(e).undelegate(e)
    }, _delay:function (t, e) {
        function n() {
            return("string" == typeof t ? o[t] : t).apply(o, arguments)
        }

        var o = this;
        return setTimeout(n, e || 0)
    }, _hoverable:function (e) {
        this.hoverable = this.hoverable.add(e), this._on(e, {mouseenter:function (e) {
            t(e.currentTarget).addClass("ui-state-hover")
        }, mouseleave:function (e) {
            t(e.currentTarget).removeClass("ui-state-hover")
        }})
    }, _focusable:function (e) {
        this.focusable = this.focusable.add(e), this._on(e, {focusin:function (e) {
            t(e.currentTarget).addClass("ui-state-focus")
        }, focusout:function (e) {
            t(e.currentTarget).removeClass("ui-state-focus")
        }})
    }, _trigger:function (e, n, o) {
        var r, i, a = this.options[e];
        if (o = o || {}, n = t.Event(n), n.type = (e === this.widgetEventPrefix ? e : this.widgetEventPrefix + e).toLowerCase(), n.target = this.element[0], i = n.originalEvent)for (r in i)r in n || (n[r] = i[r]);
        return this.element.trigger(n, o), !(t.isFunction(a) && a.apply(this.element[0], [n].concat(o)) === !1 || n.isDefaultPrevented())
    }}, t.each({show:"fadeIn", hide:"fadeOut"}, function (e, n) {
        t.Widget.prototype["_" + e] = function (o, r, i) {
            "string" == typeof r && (r = {effect:r});
            var a, s = r ? r === !0 || "number" == typeof r ? n : r.effect || n : e;
            r = r || {}, "number" == typeof r && (r = {duration:r}), a = !t.isEmptyObject(r), r.complete = i, r.delay && o.delay(r.delay), a && t.effects && t.effects.effect[s] ? o[e](r) : s !== e && o[s] ? o[s](r.duration, r.easing, i) : o.queue(function (n) {
                t(this)[e](), i && i.call(o[0]), n()
            })
        }
    })
}(jQuery), function (t) {
    var e = !1;
    t(document).mouseup(function () {
        e = !1
    }), t.widget("ui.mouse", {version:"1.10.3", options:{cancel:"input,textarea,button,select,option", distance:1, delay:0}, _mouseInit:function () {
        var e = this;
        this.element.bind("mousedown." + this.widgetName,function (t) {
            return e._mouseDown(t)
        }).bind("click." + this.widgetName, function (n) {
            return!0 === t.data(n.target, e.widgetName + ".preventClickEvent") ? (t.removeData(n.target, e.widgetName + ".preventClickEvent"), n.stopImmediatePropagation(), !1) : void 0
        }), this.started = !1
    }, _mouseDestroy:function () {
        this.element.unbind("." + this.widgetName), this._mouseMoveDelegate && t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate)
    }, _mouseDown:function (n) {
        if (!e) {
            this._mouseStarted && this._mouseUp(n), this._mouseDownEvent = n;
            var o = this, r = 1 === n.which, i = "string" == typeof this.options.cancel && n.target.nodeName ? t(n.target).closest(this.options.cancel).length : !1;
            return r && !i && this._mouseCapture(n) ? (this.mouseDelayMet = !this.options.delay, this.mouseDelayMet || (this._mouseDelayTimer = setTimeout(function () {
                o.mouseDelayMet = !0
            }, this.options.delay)), this._mouseDistanceMet(n) && this._mouseDelayMet(n) && (this._mouseStarted = this._mouseStart(n) !== !1, !this._mouseStarted) ? (n.preventDefault(), !0) : (!0 === t.data(n.target, this.widgetName + ".preventClickEvent") && t.removeData(n.target, this.widgetName + ".preventClickEvent"), this._mouseMoveDelegate = function (t) {
                return o._mouseMove(t)
            }, this._mouseUpDelegate = function (t) {
                return o._mouseUp(t)
            }, t(document).bind("mousemove." + this.widgetName, this._mouseMoveDelegate).bind("mouseup." + this.widgetName, this._mouseUpDelegate), n.preventDefault(), e = !0, !0)) : !0
        }
    }, _mouseMove:function (e) {
        return t.ui.ie && (!document.documentMode || document.documentMode < 9) && !e.button ? this._mouseUp(e) : this._mouseStarted ? (this._mouseDrag(e), e.preventDefault()) : (this._mouseDistanceMet(e) && this._mouseDelayMet(e) && (this._mouseStarted = this._mouseStart(this._mouseDownEvent, e) !== !1, this._mouseStarted ? this._mouseDrag(e) : this._mouseUp(e)), !this._mouseStarted)
    }, _mouseUp:function (e) {
        return t(document).unbind("mousemove." + this.widgetName, this._mouseMoveDelegate).unbind("mouseup." + this.widgetName, this._mouseUpDelegate), this._mouseStarted && (this._mouseStarted = !1, e.target === this._mouseDownEvent.target && t.data(e.target, this.widgetName + ".preventClickEvent", !0), this._mouseStop(e)), !1
    }, _mouseDistanceMet:function (t) {
        return Math.max(Math.abs(this._mouseDownEvent.pageX - t.pageX), Math.abs(this._mouseDownEvent.pageY - t.pageY)) >= this.options.distance
    }, _mouseDelayMet:function () {
        return this.mouseDelayMet
    }, _mouseStart:function () {
    }, _mouseDrag:function () {
    }, _mouseStop:function () {
    }, _mouseCapture:function () {
        return!0
    }})
}(jQuery), function (t) {
    function e(t, e, n) {
        return t > e && e + n > t
    }

    function n(t) {
        return/left|right/.test(t.css("float")) || /inline|table-cell/.test(t.css("display"))
    }

    t.widget("ui.sortable", t.ui.mouse, {version:"1.10.3", widgetEventPrefix:"sort", ready:!1, options:{appendTo:"parent", axis:!1, connectWith:!1, containment:!1, cursor:"auto", cursorAt:!1, dropOnEmpty:!0, forcePlaceholderSize:!1, forceHelperSize:!1, grid:!1, handle:!1, helper:"original", items:"> *", opacity:!1, placeholder:!1, revert:!1, scroll:!0, scrollSensitivity:20, scrollSpeed:20, scope:"default", tolerance:"intersect", zIndex:1e3, activate:null, beforeStop:null, change:null, deactivate:null, out:null, over:null, receive:null, remove:null, sort:null, start:null, stop:null, update:null}, _create:function () {
        var t = this.options;
        this.containerCache = {}, this.element.addClass("ui-sortable"), this.refresh(), this.floating = this.items.length ? "x" === t.axis || n(this.items[0].item) : !1, this.offset = this.element.offset(), this._mouseInit(), this.ready = !0
    }, _destroy:function () {
        this.element.removeClass("ui-sortable ui-sortable-disabled"), this._mouseDestroy();
        for (var t = this.items.length - 1; t >= 0; t--)this.items[t].item.removeData(this.widgetName + "-item");
        return this
    }, _setOption:function (e, n) {
        "disabled" === e ? (this.options[e] = n, this.widget().toggleClass("ui-sortable-disabled", !!n)) : t.Widget.prototype._setOption.apply(this, arguments)
    }, _mouseCapture:function (e, n) {
        var o = null, r = !1, i = this;
        return this.reverting ? !1 : this.options.disabled || "static" === this.options.type ? !1 : (this._refreshItems(e), t(e.target).parents().each(function () {
            return t.data(this, i.widgetName + "-item") === i ? (o = t(this), !1) : void 0
        }), t.data(e.target, i.widgetName + "-item") === i && (o = t(e.target)), o ? !this.options.handle || n || (t(this.options.handle, o).find("*").addBack().each(function () {
            this === e.target && (r = !0)
        }), r) ? (this.currentItem = o, this._removeCurrentsFromItems(), !0) : !1 : !1)
    }, _mouseStart:function (e, n, o) {
        var r, i, a = this.options;
        if (this.currentContainer = this, this.refreshPositions(), this.helper = this._createHelper(e), this._cacheHelperProportions(), this._cacheMargins(), this.scrollParent = this.helper.scrollParent(), this.offset = this.currentItem.offset(), this.offset = {top:this.offset.top - this.margins.top, left:this.offset.left - this.margins.left}, t.extend(this.offset, {click:{left:e.pageX - this.offset.left, top:e.pageY - this.offset.top}, parent:this._getParentOffset(), relative:this._getRelativeOffset()}), this.helper.css("position", "absolute"), this.cssPosition = this.helper.css("position"), this.originalPosition = this._generatePosition(e), this.originalPageX = e.pageX, this.originalPageY = e.pageY, a.cursorAt && this._adjustOffsetFromHelper(a.cursorAt), this.domPosition = {prev:this.currentItem.prev()[0], parent:this.currentItem.parent()[0]}, this.helper[0] !== this.currentItem[0] && this.currentItem.hide(), this._createPlaceholder(), a.containment && this._setContainment(), a.cursor && "auto" !== a.cursor && (i = this.document.find("body"), this.storedCursor = i.css("cursor"), i.css("cursor", a.cursor), this.storedStylesheet = t("<style>*{ cursor: " + a.cursor + " !important; }</style>").appendTo(i)), a.opacity && (this.helper.css("opacity") && (this._storedOpacity = this.helper.css("opacity")), this.helper.css("opacity", a.opacity)), a.zIndex && (this.helper.css("zIndex") && (this._storedZIndex = this.helper.css("zIndex")), this.helper.css("zIndex", a.zIndex)), this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName && (this.overflowOffset = this.scrollParent.offset()), this._trigger("start", e, this._uiHash()), this._preserveHelperProportions || this._cacheHelperProportions(), !o)for (r = this.containers.length - 1; r >= 0; r--)this.containers[r]._trigger("activate", e, this._uiHash(this));
        return t.ui.ddmanager && (t.ui.ddmanager.current = this), t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e), this.dragging = !0, this.helper.addClass("ui-sortable-helper"), this._mouseDrag(e), !0
    }, _mouseDrag:function (e) {
        var n, o, r, i, a = this.options, s = !1;
        for (this.position = this._generatePosition(e), this.positionAbs = this._convertPositionTo("absolute"), this.lastPositionAbs || (this.lastPositionAbs = this.positionAbs), this.options.scroll && (this.scrollParent[0] !== document && "HTML" !== this.scrollParent[0].tagName ? (this.overflowOffset.top + this.scrollParent[0].offsetHeight - e.pageY < a.scrollSensitivity ? this.scrollParent[0].scrollTop = s = this.scrollParent[0].scrollTop + a.scrollSpeed : e.pageY - this.overflowOffset.top < a.scrollSensitivity && (this.scrollParent[0].scrollTop = s = this.scrollParent[0].scrollTop - a.scrollSpeed), this.overflowOffset.left + this.scrollParent[0].offsetWidth - e.pageX < a.scrollSensitivity ? this.scrollParent[0].scrollLeft = s = this.scrollParent[0].scrollLeft + a.scrollSpeed : e.pageX - this.overflowOffset.left < a.scrollSensitivity && (this.scrollParent[0].scrollLeft = s = this.scrollParent[0].scrollLeft - a.scrollSpeed)) : (e.pageY - t(document).scrollTop() < a.scrollSensitivity ? s = t(document).scrollTop(t(document).scrollTop() - a.scrollSpeed) : t(window).height() - (e.pageY - t(document).scrollTop()) < a.scrollSensitivity && (s = t(document).scrollTop(t(document).scrollTop() + a.scrollSpeed)), e.pageX - t(document).scrollLeft() < a.scrollSensitivity ? s = t(document).scrollLeft(t(document).scrollLeft() - a.scrollSpeed) : t(window).width() - (e.pageX - t(document).scrollLeft()) < a.scrollSensitivity && (s = t(document).scrollLeft(t(document).scrollLeft() + a.scrollSpeed))), s !== !1 && t.ui.ddmanager && !a.dropBehaviour && t.ui.ddmanager.prepareOffsets(this, e)), this.positionAbs = this._convertPositionTo("absolute"), this.options.axis && "y" === this.options.axis || (this.helper[0].style.left = this.position.left + "px"), this.options.axis && "x" === this.options.axis || (this.helper[0].style.top = this.position.top + "px"), n = this.items.length - 1; n >= 0; n--)if (o = this.items[n], r = o.item[0], i = this._intersectsWithPointer(o), i && o.instance === this.currentContainer && r !== this.currentItem[0] && this.placeholder[1 === i ? "next" : "prev"]()[0] !== r && !t.contains(this.placeholder[0], r) && ("semi-dynamic" === this.options.type ? !t.contains(this.element[0], r) : !0)) {
            if (this.direction = 1 === i ? "down" : "up", "pointer" !== this.options.tolerance && !this._intersectsWithSides(o))break;
            this._rearrange(e, o), this._trigger("change", e, this._uiHash());
            break
        }
        return this._contactContainers(e), t.ui.ddmanager && t.ui.ddmanager.drag(this, e), this._trigger("sort", e, this._uiHash()), this.lastPositionAbs = this.positionAbs, !1
    }, _mouseStop:function (e, n) {
        if (e) {
            if (t.ui.ddmanager && !this.options.dropBehaviour && t.ui.ddmanager.drop(this, e), this.options.revert) {
                var o = this, r = this.placeholder.offset(), i = this.options.axis, a = {};
                i && "x" !== i || (a.left = r.left - this.offset.parent.left - this.margins.left + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollLeft)), i && "y" !== i || (a.top = r.top - this.offset.parent.top - this.margins.top + (this.offsetParent[0] === document.body ? 0 : this.offsetParent[0].scrollTop)), this.reverting = !0, t(this.helper).animate(a, parseInt(this.options.revert, 10) || 500, function () {
                    o._clear(e)
                })
            } else this._clear(e, n);
            return!1
        }
    }, cancel:function () {
        if (this.dragging) {
            this._mouseUp({target:null}), "original" === this.options.helper ? this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper") : this.currentItem.show();
            for (var e = this.containers.length - 1; e >= 0; e--)this.containers[e]._trigger("deactivate", null, this._uiHash(this)), this.containers[e].containerCache.over && (this.containers[e]._trigger("out", null, this._uiHash(this)), this.containers[e].containerCache.over = 0)
        }
        return this.placeholder && (this.placeholder[0].parentNode && this.placeholder[0].parentNode.removeChild(this.placeholder[0]), "original" !== this.options.helper && this.helper && this.helper[0].parentNode && this.helper.remove(), t.extend(this, {helper:null, dragging:!1, reverting:!1, _noFinalSort:null}), this.domPosition.prev ? t(this.domPosition.prev).after(this.currentItem) : t(this.domPosition.parent).prepend(this.currentItem)), this
    }, serialize:function (e) {
        var n = this._getItemsAsjQuery(e && e.connected), o = [];
        return e = e || {}, t(n).each(function () {
            var n = (t(e.item || this).attr(e.attribute || "id") || "").match(e.expression || /(.+)[\-=_](.+)/);
            n && o.push((e.key || n[1] + "[]") + "=" + (e.key && e.expression ? n[1] : n[2]))
        }), !o.length && e.key && o.push(e.key + "="), o.join("&")
    }, toArray:function (e) {
        var n = this._getItemsAsjQuery(e && e.connected), o = [];
        return e = e || {}, n.each(function () {
            o.push(t(e.item || this).attr(e.attribute || "id") || "")
        }), o
    }, _intersectsWith:function (t) {
        var e = this.positionAbs.left, n = e + this.helperProportions.width, o = this.positionAbs.top, r = o + this.helperProportions.height, i = t.left, a = i + t.width, s = t.top, l = s + t.height, c = this.offset.click.top, u = this.offset.click.left, d = "x" === this.options.axis || o + c > s && l > o + c, f = "y" === this.options.axis || e + u > i && a > e + u, h = d && f;
        return"pointer" === this.options.tolerance || this.options.forcePointerForContainers || "pointer" !== this.options.tolerance && this.helperProportions[this.floating ? "width" : "height"] > t[this.floating ? "width" : "height"] ? h : i < e + this.helperProportions.width / 2 && n - this.helperProportions.width / 2 < a && s < o + this.helperProportions.height / 2 && r - this.helperProportions.height / 2 < l
    }, _intersectsWithPointer:function (t) {
        var n = "x" === this.options.axis || e(this.positionAbs.top + this.offset.click.top, t.top, t.height), o = "y" === this.options.axis || e(this.positionAbs.left + this.offset.click.left, t.left, t.width), r = n && o, i = this._getDragVerticalDirection(), a = this._getDragHorizontalDirection();
        return r ? this.floating ? a && "right" === a || "down" === i ? 2 : 1 : i && ("down" === i ? 2 : 1) : !1
    }, _intersectsWithSides:function (t) {
        var n = e(this.positionAbs.top + this.offset.click.top, t.top + t.height / 2, t.height), o = e(this.positionAbs.left + this.offset.click.left, t.left + t.width / 2, t.width), r = this._getDragVerticalDirection(), i = this._getDragHorizontalDirection();
        return this.floating && i ? "right" === i && o || "left" === i && !o : r && ("down" === r && n || "up" === r && !n)
    }, _getDragVerticalDirection:function () {
        var t = this.positionAbs.top - this.lastPositionAbs.top;
        return 0 !== t && (t > 0 ? "down" : "up")
    }, _getDragHorizontalDirection:function () {
        var t = this.positionAbs.left - this.lastPositionAbs.left;
        return 0 !== t && (t > 0 ? "right" : "left")
    }, refresh:function (t) {
        return this._refreshItems(t), this.refreshPositions(), this
    }, _connectWith:function () {
        var t = this.options;
        return t.connectWith.constructor === String ? [t.connectWith] : t.connectWith
    }, _getItemsAsjQuery:function (e) {
        var n, o, r, i, a = [], s = [], l = this._connectWith();
        if (l && e)for (n = l.length - 1; n >= 0; n--)for (r = t(l[n]), o = r.length - 1; o >= 0; o--)i = t.data(r[o], this.widgetFullName), i && i !== this && !i.options.disabled && s.push([t.isFunction(i.options.items) ? i.options.items.call(i.element) : t(i.options.items, i.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), i]);
        for (s.push([t.isFunction(this.options.items) ? this.options.items.call(this.element, null, {options:this.options, item:this.currentItem}) : t(this.options.items, this.element).not(".ui-sortable-helper").not(".ui-sortable-placeholder"), this]), n = s.length - 1; n >= 0; n--)s[n][0].each(function () {
            a.push(this)
        });
        return t(a)
    }, _removeCurrentsFromItems:function () {
        var e = this.currentItem.find(":data(" + this.widgetName + "-item)");
        this.items = t.grep(this.items, function (t) {
            for (var n = 0; n < e.length; n++)if (e[n] === t.item[0])return!1;
            return!0
        })
    }, _refreshItems:function (e) {
        this.items = [], this.containers = [this];
        var n, o, r, i, a, s, l, c, u = this.items, d = [
            [t.isFunction(this.options.items) ? this.options.items.call(this.element[0], e, {item:this.currentItem}) : t(this.options.items, this.element), this]
        ], f = this._connectWith();
        if (f && this.ready)for (n = f.length - 1; n >= 0; n--)for (r = t(f[n]), o = r.length - 1; o >= 0; o--)i = t.data(r[o], this.widgetFullName), i && i !== this && !i.options.disabled && (d.push([t.isFunction(i.options.items) ? i.options.items.call(i.element[0], e, {item:this.currentItem}) : t(i.options.items, i.element), i]), this.containers.push(i));
        for (n = d.length - 1; n >= 0; n--)for (a = d[n][1], s = d[n][0], o = 0, c = s.length; c > o; o++)l = t(s[o]), l.data(this.widgetName + "-item", a), u.push({item:l, instance:a, width:0, height:0, left:0, top:0})
    }, refreshPositions:function (e) {
        this.offsetParent && this.helper && (this.offset.parent = this._getParentOffset());
        var n, o, r, i;
        for (n = this.items.length - 1; n >= 0; n--)o = this.items[n], o.instance !== this.currentContainer && this.currentContainer && o.item[0] !== this.currentItem[0] || (r = this.options.toleranceElement ? t(this.options.toleranceElement, o.item) : o.item, e || (o.width = r.outerWidth(), o.height = r.outerHeight()), i = r.offset(), o.left = i.left, o.top = i.top);
        if (this.options.custom && this.options.custom.refreshContainers)this.options.custom.refreshContainers.call(this); else for (n = this.containers.length - 1; n >= 0; n--)i = this.containers[n].element.offset(), this.containers[n].containerCache.left = i.left, this.containers[n].containerCache.top = i.top, this.containers[n].containerCache.width = this.containers[n].element.outerWidth(), this.containers[n].containerCache.height = this.containers[n].element.outerHeight();
        return this
    }, _createPlaceholder:function (e) {
        e = e || this;
        var n, o = e.options;
        o.placeholder && o.placeholder.constructor !== String || (n = o.placeholder, o.placeholder = {element:function () {
            var o = e.currentItem[0].nodeName.toLowerCase(), r = t("<" + o + ">", e.document[0]).addClass(n || e.currentItem[0].className + " ui-sortable-placeholder").removeClass("ui-sortable-helper");
            return"tr" === o ? e.currentItem.children().each(function () {
                t("<td>&#160;</td>", e.document[0]).attr("colspan", t(this).attr("colspan") || 1).appendTo(r)
            }) : "img" === o && r.attr("src", e.currentItem.attr("src")), n || r.css("visibility", "hidden"), r
        }, update:function (t, r) {
            (!n || o.forcePlaceholderSize) && (r.height() || r.height(e.currentItem.innerHeight() - parseInt(e.currentItem.css("paddingTop") || 0, 10) - parseInt(e.currentItem.css("paddingBottom") || 0, 10)), r.width() || r.width(e.currentItem.innerWidth() - parseInt(e.currentItem.css("paddingLeft") || 0, 10) - parseInt(e.currentItem.css("paddingRight") || 0, 10)))
        }}), e.placeholder = t(o.placeholder.element.call(e.element, e.currentItem)), e.currentItem.after(e.placeholder), o.placeholder.update(e, e.placeholder)
    }, _contactContainers:function (o) {
        var r, i, a, s, l, c, u, d, f, h, p = null, g = null;
        for (r = this.containers.length - 1; r >= 0; r--)if (!t.contains(this.currentItem[0], this.containers[r].element[0]))if (this._intersectsWith(this.containers[r].containerCache)) {
            if (p && t.contains(this.containers[r].element[0], p.element[0]))continue;
            p = this.containers[r], g = r
        } else this.containers[r].containerCache.over && (this.containers[r]._trigger("out", o, this._uiHash(this)), this.containers[r].containerCache.over = 0);
        if (p)if (1 === this.containers.length)this.containers[g].containerCache.over || (this.containers[g]._trigger("over", o, this._uiHash(this)), this.containers[g].containerCache.over = 1);
        else {
            for (a = 1e4, s = null, h = p.floating || n(this.currentItem), l = h ? "left" : "top", c = h ? "width" : "height", u = this.positionAbs[l] + this.offset.click[l], i = this.items.length - 1; i >= 0; i--)t.contains(this.containers[g].element[0], this.items[i].item[0]) && this.items[i].item[0] !== this.currentItem[0] && (!h || e(this.positionAbs.top + this.offset.click.top, this.items[i].top, this.items[i].height)) && (d = this.items[i].item.offset()[l], f = !1, Math.abs(d - u) > Math.abs(d + this.items[i][c] - u) && (f = !0, d += this.items[i][c]), Math.abs(d - u) < a && (a = Math.abs(d - u), s = this.items[i], this.direction = f ? "up" : "down"));
            if (!s && !this.options.dropOnEmpty)return;
            if (this.currentContainer === this.containers[g])return;
            s ? this._rearrange(o, s, null, !0) : this._rearrange(o, null, this.containers[g].element, !0), this._trigger("change", o, this._uiHash()), this.containers[g]._trigger("change", o, this._uiHash(this)), this.currentContainer = this.containers[g], this.options.placeholder.update(this.currentContainer, this.placeholder), this.containers[g]._trigger("over", o, this._uiHash(this)), this.containers[g].containerCache.over = 1
        }
    }, _createHelper:function (e) {
        var n = this.options, o = t.isFunction(n.helper) ? t(n.helper.apply(this.element[0], [e, this.currentItem])) : "clone" === n.helper ? this.currentItem.clone() : this.currentItem;
        return o.parents("body").length || t("parent" !== n.appendTo ? n.appendTo : this.currentItem[0].parentNode)[0].appendChild(o[0]), o[0] === this.currentItem[0] && (this._storedCSS = {width:this.currentItem[0].style.width, height:this.currentItem[0].style.height, position:this.currentItem.css("position"), top:this.currentItem.css("top"), left:this.currentItem.css("left")}), (!o[0].style.width || n.forceHelperSize) && o.width(this.currentItem.width()), (!o[0].style.height || n.forceHelperSize) && o.height(this.currentItem.height()), o
    }, _adjustOffsetFromHelper:function (e) {
        "string" == typeof e && (e = e.split(" ")), t.isArray(e) && (e = {left:+e[0], top:+e[1] || 0}), "left"in e && (this.offset.click.left = e.left + this.margins.left), "right"in e && (this.offset.click.left = this.helperProportions.width - e.right + this.margins.left), "top"in e && (this.offset.click.top = e.top + this.margins.top), "bottom"in e && (this.offset.click.top = this.helperProportions.height - e.bottom + this.margins.top)
    }, _getParentOffset:function () {
        this.offsetParent = this.helper.offsetParent();
        var e = this.offsetParent.offset();
        return"absolute" === this.cssPosition && this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) && (e.left += this.scrollParent.scrollLeft(), e.top += this.scrollParent.scrollTop()), (this.offsetParent[0] === document.body || this.offsetParent[0].tagName && "html" === this.offsetParent[0].tagName.toLowerCase() && t.ui.ie) && (e = {top:0, left:0}), {top:e.top + (parseInt(this.offsetParent.css("borderTopWidth"), 10) || 0), left:e.left + (parseInt(this.offsetParent.css("borderLeftWidth"), 10) || 0)}
    }, _getRelativeOffset:function () {
        if ("relative" === this.cssPosition) {
            var t = this.currentItem.position();
            return{top:t.top - (parseInt(this.helper.css("top"), 10) || 0) + this.scrollParent.scrollTop(), left:t.left - (parseInt(this.helper.css("left"), 10) || 0) + this.scrollParent.scrollLeft()}
        }
        return{top:0, left:0}
    }, _cacheMargins:function () {
        this.margins = {left:parseInt(this.currentItem.css("marginLeft"), 10) || 0, top:parseInt(this.currentItem.css("marginTop"), 10) || 0}
    }, _cacheHelperProportions:function () {
        this.helperProportions = {width:this.helper.outerWidth(), height:this.helper.outerHeight()}
    }, _setContainment:function () {
        var e, n, o, r = this.options;
        "parent" === r.containment && (r.containment = this.helper[0].parentNode), ("document" === r.containment || "window" === r.containment) && (this.containment = [0 - this.offset.relative.left - this.offset.parent.left, 0 - this.offset.relative.top - this.offset.parent.top, t("document" === r.containment ? document : window).width() - this.helperProportions.width - this.margins.left, (t("document" === r.containment ? document : window).height() || document.body.parentNode.scrollHeight) - this.helperProportions.height - this.margins.top]), /^(document|window|parent)$/.test(r.containment) || (e = t(r.containment)[0], n = t(r.containment).offset(), o = "hidden" !== t(e).css("overflow"), this.containment = [n.left + (parseInt(t(e).css("borderLeftWidth"), 10) || 0) + (parseInt(t(e).css("paddingLeft"), 10) || 0) - this.margins.left, n.top + (parseInt(t(e).css("borderTopWidth"), 10) || 0) + (parseInt(t(e).css("paddingTop"), 10) || 0) - this.margins.top, n.left + (o ? Math.max(e.scrollWidth, e.offsetWidth) : e.offsetWidth) - (parseInt(t(e).css("borderLeftWidth"), 10) || 0) - (parseInt(t(e).css("paddingRight"), 10) || 0) - this.helperProportions.width - this.margins.left, n.top + (o ? Math.max(e.scrollHeight, e.offsetHeight) : e.offsetHeight) - (parseInt(t(e).css("borderTopWidth"), 10) || 0) - (parseInt(t(e).css("paddingBottom"), 10) || 0) - this.helperProportions.height - this.margins.top])
    }, _convertPositionTo:function (e, n) {
        n || (n = this.position);
        var o = "absolute" === e ? 1 : -1, r = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, i = /(html|body)/i.test(r[0].tagName);
        return{top:n.top + this.offset.relative.top * o + this.offset.parent.top * o - ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : i ? 0 : r.scrollTop()) * o, left:n.left + this.offset.relative.left * o + this.offset.parent.left * o - ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : i ? 0 : r.scrollLeft()) * o}
    }, _generatePosition:function (e) {
        var n, o, r = this.options, i = e.pageX, a = e.pageY, s = "absolute" !== this.cssPosition || this.scrollParent[0] !== document && t.contains(this.scrollParent[0], this.offsetParent[0]) ? this.scrollParent : this.offsetParent, l = /(html|body)/i.test(s[0].tagName);
        return"relative" !== this.cssPosition || this.scrollParent[0] !== document && this.scrollParent[0] !== this.offsetParent[0] || (this.offset.relative = this._getRelativeOffset()), this.originalPosition && (this.containment && (e.pageX - this.offset.click.left < this.containment[0] && (i = this.containment[0] + this.offset.click.left), e.pageY - this.offset.click.top < this.containment[1] && (a = this.containment[1] + this.offset.click.top), e.pageX - this.offset.click.left > this.containment[2] && (i = this.containment[2] + this.offset.click.left), e.pageY - this.offset.click.top > this.containment[3] && (a = this.containment[3] + this.offset.click.top)), r.grid && (n = this.originalPageY + Math.round((a - this.originalPageY) / r.grid[1]) * r.grid[1], a = this.containment ? n - this.offset.click.top >= this.containment[1] && n - this.offset.click.top <= this.containment[3] ? n : n - this.offset.click.top >= this.containment[1] ? n - r.grid[1] : n + r.grid[1] : n, o = this.originalPageX + Math.round((i - this.originalPageX) / r.grid[0]) * r.grid[0], i = this.containment ? o - this.offset.click.left >= this.containment[0] && o - this.offset.click.left <= this.containment[2] ? o : o - this.offset.click.left >= this.containment[0] ? o - r.grid[0] : o + r.grid[0] : o)), {top:a - this.offset.click.top - this.offset.relative.top - this.offset.parent.top + ("fixed" === this.cssPosition ? -this.scrollParent.scrollTop() : l ? 0 : s.scrollTop()), left:i - this.offset.click.left - this.offset.relative.left - this.offset.parent.left + ("fixed" === this.cssPosition ? -this.scrollParent.scrollLeft() : l ? 0 : s.scrollLeft())}
    }, _rearrange:function (t, e, n, o) {
        n ? n[0].appendChild(this.placeholder[0]) : e.item[0].parentNode.insertBefore(this.placeholder[0], "down" === this.direction ? e.item[0] : e.item[0].nextSibling), this.counter = this.counter ? ++this.counter : 1;
        var r = this.counter;
        this._delay(function () {
            r === this.counter && this.refreshPositions(!o)
        })
    }, _clear:function (t, e) {
        this.reverting = !1;
        var n, o = [];
        if (!this._noFinalSort && this.currentItem.parent().length && this.placeholder.before(this.currentItem), this._noFinalSort = null, this.helper[0] === this.currentItem[0]) {
            for (n in this._storedCSS)("auto" === this._storedCSS[n] || "static" === this._storedCSS[n]) && (this._storedCSS[n] = "");
            this.currentItem.css(this._storedCSS).removeClass("ui-sortable-helper")
        } else this.currentItem.show();
        for (this.fromOutside && !e && o.push(function (t) {
            this._trigger("receive", t, this._uiHash(this.fromOutside))
        }), !this.fromOutside && this.domPosition.prev === this.currentItem.prev().not(".ui-sortable-helper")[0] && this.domPosition.parent === this.currentItem.parent()[0] || e || o.push(function (t) {
            this._trigger("update", t, this._uiHash())
        }), this !== this.currentContainer && (e || (o.push(function (t) {
            this._trigger("remove", t, this._uiHash())
        }), o.push(function (t) {
            return function (e) {
                t._trigger("receive", e, this._uiHash(this))
            }
        }.call(this, this.currentContainer)), o.push(function (t) {
            return function (e) {
                t._trigger("update", e, this._uiHash(this))
            }
        }.call(this, this.currentContainer)))), n = this.containers.length - 1; n >= 0; n--)e || o.push(function (t) {
            return function (e) {
                t._trigger("deactivate", e, this._uiHash(this))
            }
        }.call(this, this.containers[n])), this.containers[n].containerCache.over && (o.push(function (t) {
            return function (e) {
                t._trigger("out", e, this._uiHash(this))
            }
        }.call(this, this.containers[n])), this.containers[n].containerCache.over = 0);
        if (this.storedCursor && (this.document.find("body").css("cursor", this.storedCursor), this.storedStylesheet.remove()), this._storedOpacity && this.helper.css("opacity", this._storedOpacity), this._storedZIndex && this.helper.css("zIndex", "auto" === this._storedZIndex ? "" : this._storedZIndex), this.dragging = !1, this.cancelHelperRemoval) {
            if (!e) {
                for (this._trigger("beforeStop", t, this._uiHash()), n = 0; n < o.length; n++)o[n].call(this, t);
                this._trigger("stop", t, this._uiHash())
            }
            return this.fromOutside = !1, !1
        }
        if (e || this._trigger("beforeStop", t, this._uiHash()), this.placeholder[0].parentNode.removeChild(this.placeholder[0]), this.helper[0] !== this.currentItem[0] && this.helper.remove(), this.helper = null, !e) {
            for (n = 0; n < o.length; n++)o[n].call(this, t);
            this._trigger("stop", t, this._uiHash())
        }
        return this.fromOutside = !1, !0
    }, _trigger:function () {
        t.Widget.prototype._trigger.apply(this, arguments) === !1 && this.cancel()
    }, _uiHash:function (e) {
        var n = e || this;
        return{helper:n.helper, placeholder:n.placeholder || t([]), position:n.position, originalPosition:n.originalPosition, offset:n.positionAbs, item:n.currentItem, sender:e ? e.element : null}
    }})
}(jQuery), !function (t, e) {
    "undefined" != typeof module && module.exports ? module.exports.browser = e() : "function" == typeof define ? define(e) : this[t] = e()
}("bowser", function () {
    function t() {
        return r ? {name:"Internet Explorer", msie:o, version:n.match(/(msie |rv:)(\d+(\.\d+)?)/i)[2]} : h ? {name:"Opera", opera:o, version:n.match(v) ? n.match(v)[1] : n.match(/opr\/(\d+(\.\d+)?)/i)[1]} : i ? {name:"Chrome", webkit:o, chrome:o, version:n.match(/(?:chrome|crios)\/(\d+(\.\d+)?)/i)[1]} : a ? {name:"PhantomJS", webkit:o, phantom:o, version:n.match(/phantomjs\/(\d+(\.\d+)+)/i)[1]} : c ? {name:"TouchPad", webkit:o, touchpad:o, version:n.match(/touchpad\/(\d+(\.\d+)?)/i)[1]} : u ? {name:"Amazon Silk", webkit:o, android:o, mobile:o, version:n.match(/silk\/(\d+(\.\d+)?)/i)[1]} : s || l ? (e = {name:s ? "iPhone" : "iPad", webkit:o, mobile:o, ios:o, iphone:s, ipad:l}, v.test(n) && (e.version = n.match(v)[1]), e) : f ? {name:"Android", webkit:o, android:o, mobile:o, version:(n.match(v) || n.match(y))[1]} : d ? {name:"Safari", webkit:o, safari:o, version:n.match(v)[1]} : g ? (e = {name:"Gecko", gecko:o, mozilla:o, version:n.match(y)[1]}, p && (e.name = "Firefox", e.firefox = o), e) : m ? {name:"SeaMonkey", seamonkey:o, version:n.match(/seamonkey\/(\d+(\.\d+)?)/i)[1]} : {}
    }

    var e, n = navigator.userAgent, o = !0, r = /(msie|trident)/i.test(n), i = /chrome|crios/i.test(n), a = /phantom/i.test(n), s = /iphone/i.test(n), l = /ipad/i.test(n), c = /touchpad/i.test(n), u = /silk/i.test(n), d = /safari/i.test(n) && !i && !a && !u, f = /android/i.test(n), h = /opera/i.test(n) || /opr/i.test(n), p = /firefox/i.test(n), g = /gecko\//i.test(n), m = /seamonkey\//i.test(n), v = /version\/(\d+(\.\d+)?)/i, y = /firefox\/(\d+(\.\d+)?)/i, b = t();
    return b.msie && b.version >= 8 || b.chrome && b.version >= 10 || b.firefox && b.version >= 4 || b.safari && b.version >= 5 || b.opera && b.version >= 10 ? b.a = o : b.msie && b.version < 8 || b.chrome && b.version < 10 || b.firefox && b.version < 4 || b.safari && b.version < 5 || b.opera && b.version < 10 ? b.c = o : b.x = o, b
}), function () {
    for (var t = 0, e = ["ms", "moz", "webkit", "o"], n = 0; n < e.length && !window.requestAnimationFrame; ++n)window.requestAnimationFrame = window[e[n] + "RequestAnimationFrame"], window.cancelAnimationFrame = window[e[n] + "CancelAnimationFrame"] || window[e[n] + "CancelRequestAnimationFrame"];
    window.requestAnimationFrame || (window.requestAnimationFrame = function (e) {
        var n = (new Date).getTime(), o = Math.max(0, 16 - (n - t)), r = window.setTimeout(function () {
            e(n + o)
        }, o);
        return t = n + o, r
    }), window.cancelAnimationFrame || (window.cancelAnimationFrame = function (t) {
        clearTimeout(t)
    })
}(), function (t) {
    function e(t, e) {
        var n = typeof t[e];
        return n == w || n == b && !!t[e] || "unknown" == n
    }

    function n(t, e) {
        return typeof t[e] == b && !!t[e]
    }

    function o(t, e) {
        return typeof t[e] != x
    }

    function r(t) {
        return function (e, n) {
            for (var o = n.length; o--;)if (!t(e, n[o]))return!1;
            return!0
        }
    }

    function i(t) {
        return t && k(t, _) && N(t, S)
    }

    function a(t) {
        return n(t, "body") ? t.body : t.getElementsByTagName("body")[0]
    }

    function s(t) {
        n(window, "console") && e(window.console, "log") && window.console.log(t)
    }

    function l(t, e) {
        e ? window.alert(t) : s(t)
    }

    function c(t) {
        R.initialized = !0, R.supported = !1, l("Rangy is not supported on this page in your browser. Reason: " + t, R.config.alertOnFail)
    }

    function u(t) {
        l("Rangy warning: " + t, R.config.alertOnWarn)
    }

    function d(t) {
        return t.message || t.description || String(t)
    }

    function f() {
        if (!R.initialized) {
            var t, n = !1, o = !1;
            e(document, "createRange") && (t = document.createRange(), k(t, E) && N(t, C) && (n = !0), t.detach());
            var r = a(document);
            if (!r || "body" != r.nodeName.toLowerCase())return c("No body element found"), void 0;
            if (r && e(r, "createTextRange") && (t = r.createTextRange(), i(t) && (o = !0)), !n && !o)return c("Neither Range nor TextRange are available"), void 0;
            R.initialized = !0, R.features = {implementsDomRange:n, implementsTextRange:o};
            var l, u;
            for (var f in P)(l = P[f])instanceof p && l.init(l, R);
            for (var h = 0, g = O.length; g > h; ++h)try {
                O[h](R)
            } catch (m) {
                u = "Rangy init listener threw an exception. Continuing. Detail: " + d(m), s(u)
            }
        }
    }

    function h(t) {
        t = t || window, f();
        for (var e = 0, n = D.length; n > e; ++e)D[e](t)
    }

    function p(t, e, n) {
        this.name = t, this.dependencies = e, this.initialized = !1, this.supported = !1, this.initializer = n
    }

    function g(t, e, n, o) {
        var r = new p(e, n, function (t) {
            if (!t.initialized) {
                t.initialized = !0;
                try {
                    o(R, t), t.supported = !0
                } catch (n) {
                    var r = "Module '" + e + "' failed to load: " + d(n);
                    s(r)
                }
            }
        });
        P[e] = r
    }

    function m() {
    }

    function v() {
    }

    var y = "function" == typeof t.define && t.define.amd, b = "object", w = "function", x = "undefined", C = ["startContainer", "startOffset", "endContainer", "endOffset", "collapsed", "commonAncestorContainer"], E = ["setStart", "setStartBefore", "setStartAfter", "setEnd", "setEndBefore", "setEndAfter", "collapse", "selectNode", "selectNodeContents", "compareBoundaryPoints", "deleteContents", "extractContents", "cloneContents", "insertNode", "surroundContents", "cloneRange", "toString", "detach"], S = ["boundingHeight", "boundingLeft", "boundingTop", "boundingWidth", "htmlText", "text"], _ = ["collapse", "compareEndPoints", "duplicate", "moveToElementText", "parentElement", "select", "setEndPoint", "getBoundingClientRect"], k = r(e), T = r(n), N = r(o), P = {}, R = {version:"1.3alpha.804", initialized:!1, supported:!0, util:{isHostMethod:e, isHostObject:n, isHostProperty:o, areHostMethods:k, areHostObjects:T, areHostProperties:N, isTextRange:i, getBody:a}, features:{}, modules:P, config:{alertOnFail:!0, alertOnWarn:!1, preferTextRange:!1}};
    R.fail = c, R.warn = u, {}.hasOwnProperty ? R.util.extend = function (t, e, n) {
        var o, r;
        for (var i in e)e.hasOwnProperty(i) && (o = t[i], r = e[i], n && null !== o && "object" == typeof o && null !== r && "object" == typeof r && R.util.extend(o, r, !0), t[i] = r);
        return t
    } : c("hasOwnProperty not supported"), function () {
        var t = document.createElement("div");
        t.appendChild(document.createElement("span"));
        var e, n = [].slice;
        try {
            1 == n.call(t.childNodes, 0)[0].nodeType && (e = function (t) {
                return n.call(t, 0)
            })
        } catch (o) {
        }
        e || (e = function (t) {
            for (var e = [], n = 0, o = t.length; o > n; ++n)e[n] = t[n];
            return e
        }), R.util.toArray = e
    }();
    var A;
    e(document, "addEventListener") ? A = function (t, e, n) {
        t.addEventListener(e, n, !1)
    } : e(document, "attachEvent") ? A = function (t, e, n) {
        t.attachEvent("on" + e, n)
    } : c("Document does not have required addEventListener or attachEvent method"), R.util.addListener = A;
    var O = [];
    R.init = f, R.addInitListener = function (t) {
        R.initialized ? t(R) : O.push(t)
    };
    var D = [];
    R.addCreateMissingNativeApiListener = function (t) {
        D.push(t)
    }, R.createMissingNativeApi = h, p.prototype = {init:function () {
        for (var t, e, n = this.dependencies || [], o = 0, r = n.length; r > o; ++o) {
            if (e = n[o], t = P[e], !(t && t instanceof p))throw new Error("required module '" + e + "' not found");
            if (t.init(), !t.supported)throw new Error("required module '" + e + "' not supported")
        }
        this.initializer(this)
    }, fail:function (t) {
        throw this.initialized = !0, this.supported = !1, new Error("Module '" + this.name + "' failed to load: " + t)
    }, warn:function (t) {
        R.warn("Module " + this.name + ": " + t)
    }, deprecationNotice:function (t, e) {
        R.warn("DEPRECATED: " + t + " in module " + this.name + "is deprecated. Please use " + e + " instead")
    }, createError:function (t) {
        return new Error("Error in Rangy " + this.name + " module: " + t)
    }}, R.createModule = function (t) {
        var e, n;
        2 == arguments.length ? (e = arguments[1], n = []) : (e = arguments[2], n = arguments[1]), g(!1, t, n, e)
    }, R.createCoreModule = function (t, e, n) {
        g(!0, t, e, n)
    }, R.RangePrototype = m, R.rangePrototype = new m, R.selectionPrototype = new v;
    var I = !1, $ = function () {
        I || (I = !0, R.initialized || f())
    };
    return typeof window == x ? (c("No window found"), void 0) : typeof document == x ? (c("No document found"), void 0) : (e(document, "addEventListener") && document.addEventListener("DOMContentLoaded", $, !1), A(window, "load", $), y && t.define(function () {
        return R.amd = !0, R
    }), t.rangy = R, void 0)
}(this), rangy.createCoreModule("DomUtil", [], function (t, e) {
    function n(t) {
        var e;
        return typeof t.namespaceURI == P || null === (e = t.namespaceURI) || "http://www.w3.org/1999/xhtml" == e
    }

    function o(t) {
        var e = t.parentNode;
        return 1 == e.nodeType ? e : null
    }

    function r(t) {
        for (var e = 0; t = t.previousSibling;)++e;
        return e
    }

    function i(t) {
        switch (t.nodeType) {
            case 7:
            case 10:
                return 0;
            case 3:
            case 8:
                return t.length;
            default:
                return t.childNodes.length
        }
    }

    function a(t, e) {
        var n, o = [];
        for (n = t; n; n = n.parentNode)o.push(n);
        for (n = e; n; n = n.parentNode)if (D(o, n))return n;
        return null
    }

    function s(t, e, n) {
        for (var o = n ? e : e.parentNode; o;) {
            if (o === t)return!0;
            o = o.parentNode
        }
        return!1
    }

    function l(t, e) {
        return s(t, e, !0)
    }

    function c(t, e, n) {
        for (var o, r = n ? t : t.parentNode; r;) {
            if (o = r.parentNode, o === e)return r;
            r = o
        }
        return null
    }

    function u(t) {
        var e = t.nodeType;
        return 3 == e || 4 == e || 8 == e
    }

    function d(t) {
        if (!t)return!1;
        var e = t.nodeType;
        return 3 == e || 8 == e
    }

    function f(t, e) {
        var n = e.nextSibling, o = e.parentNode;
        return n ? o.insertBefore(t, n) : o.appendChild(t), t
    }

    function h(t, e, n) {
        var o = t.cloneNode(!1);
        if (o.deleteData(0, e), t.deleteData(e, t.length - e), f(o, t), n)for (var i, a = 0; i = n[a++];)i.node == t && i.offset > e ? (i.node = o, i.offset -= e) : i.node == t.parentNode && i.offset > r(t) && ++i.offset;
        return o
    }

    function p(t) {
        if (9 == t.nodeType)return t;
        if (typeof t.ownerDocument != P)return t.ownerDocument;
        if (typeof t.document != P)return t.document;
        if (t.parentNode)return p(t.parentNode);
        throw e.createError("getDocument: no document found for node")
    }

    function g(t) {
        var n = p(t);
        if (typeof n.defaultView != P)return n.defaultView;
        if (typeof n.parentWindow != P)return n.parentWindow;
        throw e.createError("Cannot get a window object for node")
    }

    function m(t) {
        if (typeof t.contentDocument != P)return t.contentDocument;
        if (typeof t.contentWindow != P)return t.contentWindow.document;
        throw e.createError("getIframeDocument: No Document object found for iframe element")
    }

    function v(t) {
        if (typeof t.contentWindow != P)return t.contentWindow;
        if (typeof t.contentDocument != P)return t.contentDocument.defaultView;
        throw e.createError("getIframeWindow: No Window object found for iframe element")
    }

    function y(t) {
        return t && R.isHostMethod(t, "setTimeout") && R.isHostObject(t, "document")
    }

    function b(t, e, n) {
        var o;
        if (t ? R.isHostProperty(t, "nodeType") ? o = 1 == t.nodeType && "iframe" == t.tagName.toLowerCase() ? m(t) : p(t) : y(t) && (o = t.document) : o = document, !o)throw e.createError(n + "(): Parameter must be a Window object or DOM node");
        return o
    }

    function w(t) {
        for (var e; e = t.parentNode;)t = e;
        return t
    }

    function x(t, n, o, i) {
        var s, l, u, d, f;
        if (t == o)return n === i ? 0 : i > n ? -1 : 1;
        if (s = c(o, t, !0))return n <= r(s) ? -1 : 1;
        if (s = c(t, o, !0))return r(s) < i ? -1 : 1;
        if (l = a(t, o), !l)throw new Error("comparePoints error: nodes have no common ancestor");
        if (u = t === l ? l : c(t, l, !0), d = o === l ? l : c(o, l, !0), u === d)throw e.createError("comparePoints got to case 4 and childA and childB are the same!");
        for (f = l.firstChild; f;) {
            if (f === u)return-1;
            if (f === d)return 1;
            f = f.nextSibling
        }
    }

    function C(t) {
        try {
            return t.parentNode, !1
        } catch (e) {
            return!0
        }
    }

    function E(t) {
        if (!t)return"[No node]";
        if (I && C(t))return"[Broken node]";
        if (u(t))return'"' + t.data + '"';
        if (1 == t.nodeType) {
            var e = t.id ? ' id="' + t.id + '"' : "";
            return"<" + t.nodeName + e + ">[" + r(t) + "][" + t.childNodes.length + "][" + (t.innerHTML || "[innerHTML not supported]").slice(0, 25) + "]"
        }
        return t.nodeName
    }

    function S(t) {
        for (var e, n = p(t).createDocumentFragment(); e = t.firstChild;)n.appendChild(e);
        return n
    }

    function _(t) {
        this.root = t, this._next = t
    }

    function k(t) {
        return new _(t)
    }

    function T(t, e) {
        this.node = t, this.offset = e
    }

    function N(t) {
        this.code = this[t], this.codeName = t, this.message = "DOMException: " + this.codeName
    }

    var P = "undefined", R = t.util;
    R.areHostMethods(document, ["createDocumentFragment", "createElement", "createTextNode"]) || e.fail("document missing a Node creation method"), R.isHostMethod(document, "getElementsByTagName") || e.fail("document missing getElementsByTagName method");
    var A = document.createElement("div");
    R.areHostMethods(A, ["insertBefore", "appendChild", "cloneNode"] || !R.areHostObjects(A, ["previousSibling", "nextSibling", "childNodes", "parentNode"])) || e.fail("Incomplete Element implementation"), R.isHostProperty(A, "innerHTML") || e.fail("Element is missing innerHTML property");
    var O = document.createTextNode("test");
    R.areHostMethods(O, ["splitText", "deleteData", "insertData", "appendData", "cloneNode"] || !R.areHostObjects(A, ["previousSibling", "nextSibling", "childNodes", "parentNode"]) || !R.areHostProperties(O, ["data"])) || e.fail("Incomplete Text Node implementation");
    var D = function (t, e) {
        for (var n = t.length; n--;)if (t[n] === e)return!0;
        return!1
    }, I = !1;
    !function () {
        var e = document.createElement("b");
        e.innerHTML = "1";
        var n = e.firstChild;
        e.innerHTML = "<br>", I = C(n), t.features.crashyTextNodes = I
    }();
    var $;
    typeof window.getComputedStyle != P ? $ = function (t, e) {
        return g(t).getComputedStyle(t, null)[e]
    } : typeof document.documentElement.currentStyle != P ? $ = function (t, e) {
        return t.currentStyle[e]
    } : e.fail("No means of obtaining computed style properties found"), _.prototype = {_current:null, hasNext:function () {
        return!!this._next
    }, next:function () {
        var t, e, n = this._current = this._next;
        if (this._current)if (t = n.firstChild)this._next = t; else {
            for (e = null; n !== this.root && !(e = n.nextSibling);)n = n.parentNode;
            this._next = e
        }
        return this._current
    }, detach:function () {
        this._current = this._next = this.root = null
    }}, T.prototype = {equals:function (t) {
        return!!t && this.node === t.node && this.offset == t.offset
    }, inspect:function () {
        return"[DomPosition(" + E(this.node) + ":" + this.offset + ")]"
    }, toString:function () {
        return this.inspect()
    }}, N.prototype = {INDEX_SIZE_ERR:1, HIERARCHY_REQUEST_ERR:3, WRONG_DOCUMENT_ERR:4, NO_MODIFICATION_ALLOWED_ERR:7, NOT_FOUND_ERR:8, NOT_SUPPORTED_ERR:9, INVALID_STATE_ERR:11}, N.prototype.toString = function () {
        return this.message
    }, t.dom = {arrayContains:D, isHtmlNamespace:n, parentElement:o, getNodeIndex:r, getNodeLength:i, getCommonAncestor:a, isAncestorOf:s, isOrIsAncestorOf:l, getClosestAncestorIn:c, isCharacterDataNode:u, isTextOrCommentNode:d, insertAfter:f, splitDataNode:h, getDocument:p, getWindow:g, getIframeWindow:v, getIframeDocument:m, getBody:R.getBody, isWindow:y, getContentDocument:b, getRootContainer:w, comparePoints:x, isBrokenNode:C, inspectNode:E, getComputedStyleProperty:$, fragmentFromNodeChildren:S, createIterator:k, DomPosition:T}, t.DOMException = N
}), rangy.createCoreModule("DomRange", ["DomUtil"], function (t) {
    function e(t, e) {
        return 3 != t.nodeType && (W(t, e.startContainer) || W(t, e.endContainer))
    }

    function n(t) {
        return t.document || q(t.startContainer)
    }

    function o(t) {
        return new B(t.parentNode, H(t))
    }

    function r(t) {
        return new B(t.parentNode, H(t) + 1)
    }

    function i(t, e, n) {
        var o = 11 == t.nodeType ? t.firstChild : t;
        return j(e) ? n == e.length ? L.insertAfter(t, e) : e.parentNode.insertBefore(t, 0 == n ? e : z(e, n)) : n >= e.childNodes.length ? e.appendChild(t) : e.insertBefore(t, e.childNodes[n]), o
    }

    function a(t, e, o) {
        if (k(t), k(e), n(e) != n(t))throw new F("WRONG_DOCUMENT_ERR");
        var r = X(t.startContainer, t.startOffset, e.endContainer, e.endOffset), i = X(t.endContainer, t.endOffset, e.startContainer, e.startOffset);
        return o ? 0 >= r && i >= 0 : 0 > r && i > 0
    }

    function s(t) {
        for (var e, o, r, i = n(t.range).createDocumentFragment(); o = t.next();) {
            if (e = t.isPartiallySelectedSubtree(), o = o.cloneNode(!e), e && (r = t.getSubtreeIterator(), o.appendChild(s(r)), r.detach(!0)), 10 == o.nodeType)throw new F("HIERARCHY_REQUEST_ERR");
            i.appendChild(o)
        }
        return i
    }

    function l(t, e, n) {
        var o, r;
        n = n || {stop:!1};
        for (var i, a; i = t.next();)if (t.isPartiallySelectedSubtree()) {
            if (e(i) === !1)return n.stop = !0, void 0;
            if (a = t.getSubtreeIterator(), l(a, e, n), a.detach(!0), n.stop)return
        } else for (o = L.createIterator(i); r = o.next();)if (e(r) === !1)return n.stop = !0, void 0
    }

    function c(t) {
        for (var e; t.next();)t.isPartiallySelectedSubtree() ? (e = t.getSubtreeIterator(), c(e), e.detach(!0)) : t.remove()
    }

    function u(t) {
        for (var e, o, r = n(t.range).createDocumentFragment(); e = t.next();) {
            if (t.isPartiallySelectedSubtree() ? (e = e.cloneNode(!1), o = t.getSubtreeIterator(), e.appendChild(u(o)), o.detach(!0)) : t.remove(), 10 == e.nodeType)throw new F("HIERARCHY_REQUEST_ERR");
            r.appendChild(e)
        }
        return r
    }

    function d(t, e, n) {
        var o, r = !!e && !!e.length, i = !!n;
        r && (o = new RegExp("^(" + e.join("|") + ")$"));
        var a = [];
        return l(new h(t, !1), function (e) {
            if (!(r && !o.test(e.nodeType) || i && !n(e))) {
                var s = t.startContainer;
                if (e != s || !j(s) || t.startOffset != s.length) {
                    var l = t.endContainer;
                    e == l && j(l) && 0 == t.endOffset || a.push(e)
                }
            }
        }), a
    }

    function f(t) {
        var e = "undefined" == typeof t.getName ? "Range" : t.getName();
        return"[" + e + "(" + L.inspectNode(t.startContainer) + ":" + t.startOffset + ", " + L.inspectNode(t.endContainer) + ":" + t.endOffset + ")]"
    }

    function h(t, e) {
        if (this.range = t, this.clonePartiallySelectedTextNodes = e, !t.collapsed) {
            this.sc = t.startContainer, this.so = t.startOffset, this.ec = t.endContainer, this.eo = t.endOffset;
            var n = t.commonAncestorContainer;
            this.sc === this.ec && j(this.sc) ? (this.isSingleCharacterDataNode = !0, this._first = this._last = this._next = this.sc) : (this._first = this._next = this.sc !== n || j(this.sc) ? G(this.sc, n, !0) : this.sc.childNodes[this.so], this._last = this.ec !== n || j(this.ec) ? G(this.ec, n, !0) : this.ec.childNodes[this.eo - 1])
        }
    }

    function p(t) {
        this.code = this[t], this.codeName = t, this.message = "RangeException: " + this.codeName
    }

    function g(t) {
        return function (e, n) {
            for (var o, r = n ? e : e.parentNode; r;) {
                if (o = r.nodeType, V(t, o))return r;
                r = r.parentNode
            }
            return null
        }
    }

    function m(t, e) {
        if (re(t, e))throw new p("INVALID_NODE_TYPE_ERR")
    }

    function v(t) {
        if (!t.startContainer)throw new F("INVALID_STATE_ERR")
    }

    function y(t, e) {
        if (!V(e, t.nodeType))throw new p("INVALID_NODE_TYPE_ERR")
    }

    function b(t, e) {
        if (0 > e || e > (j(t) ? t.length : t.childNodes.length))throw new F("INDEX_SIZE_ERR")
    }

    function w(t, e) {
        if (ne(t, !0) !== ne(e, !0))throw new F("WRONG_DOCUMENT_ERR")
    }

    function x(t) {
        if (oe(t, !0))throw new F("NO_MODIFICATION_ALLOWED_ERR")
    }

    function C(t, e) {
        if (!t)throw new F(e)
    }

    function E(t) {
        return Q && L.isBrokenNode(t) || !V(J, t.nodeType) && !ne(t, !0)
    }

    function S(t, e) {
        return e <= (j(t) ? t.length : t.childNodes.length)
    }

    function _(t) {
        return!!t.startContainer && !!t.endContainer && !E(t.startContainer) && !E(t.endContainer) && S(t.startContainer, t.startOffset) && S(t.endContainer, t.endOffset)
    }

    function k(t) {
        if (v(t), !_(t))throw new Error("Range error: Range is no longer valid after DOM mutation (" + t.inspect() + ")")
    }

    function T(t, e) {
        k(t);
        var n = t.startContainer, o = t.startOffset, r = t.endContainer, i = t.endOffset, a = n === r;
        j(r) && i > 0 && i < r.length && z(r, i, e), j(n) && o > 0 && o < n.length && (n = z(n, o, e), a ? (i -= o, r = n) : r == n.parentNode && i >= H(n) && i++, o = 0), t.setStartAndEnd(n, o, r, i)
    }

    function N(t) {
        t.START_TO_START = ue, t.START_TO_END = de, t.END_TO_END = fe, t.END_TO_START = he, t.NODE_BEFORE = pe, t.NODE_AFTER = ge, t.NODE_BEFORE_AND_AFTER = me, t.NODE_INSIDE = ve
    }

    function P(t) {
        N(t), N(t.prototype)
    }

    function R(t, e) {
        return function () {
            k(this);
            var n, o, i = this.startContainer, a = this.startOffset, s = this.commonAncestorContainer, c = new h(this, !0);
            i !== s && (n = G(i, s, !0), o = r(n), i = o.node, a = o.offset), l(c, x), c.reset();
            var u = t(c);
            return c.detach(), e(this, i, a, i, a), u
        }
    }

    function A(n, i, a) {
        function s(t, e) {
            return function (n) {
                v(this), y(n, K), y(Y(n), J);
                var i = (t ? o : r)(n);
                (e ? l : d)(this, i.node, i.offset)
            }
        }

        function l(t, e, n) {
            var o = t.endContainer, r = t.endOffset;
            (e !== t.startContainer || n !== t.startOffset) && ((Y(e) != Y(o) || 1 == X(e, n, o, r)) && (o = e, r = n), i(t, e, n, o, r))
        }

        function d(t, e, n) {
            var o = t.startContainer, r = t.startOffset;
            (e !== t.endContainer || n !== t.endOffset) && ((Y(e) != Y(o) || -1 == X(e, n, o, r)) && (o = e, r = n), i(t, o, r, e, n))
        }

        var f = function () {
        };
        f.prototype = t.rangePrototype, n.prototype = new f, M.extend(n.prototype, {setStart:function (t, e) {
            v(this), m(t, !0), b(t, e), l(this, t, e)
        }, setEnd:function (t, e) {
            v(this), m(t, !0), b(t, e), d(this, t, e)
        }, setStartAndEnd:function () {
            v(this);
            var t = arguments, e = t[0], n = t[1], o = e, r = n;
            switch (t.length) {
                case 3:
                    r = t[2];
                    break;
                case 4:
                    o = t[2], r = t[3]
            }
            i(this, e, n, o, r)
        }, setBoundary:function (t, e, n) {
            this["set" + (n ? "Start" : "End")](t, e)
        }, setStartBefore:s(!0, !0), setStartAfter:s(!1, !0), setEndBefore:s(!0, !1), setEndAfter:s(!1, !1), collapse:function (t) {
            k(this), t ? i(this, this.startContainer, this.startOffset, this.startContainer, this.startOffset) : i(this, this.endContainer, this.endOffset, this.endContainer, this.endOffset)
        }, selectNodeContents:function (t) {
            v(this), m(t, !0), i(this, t, 0, t, U(t))
        }, selectNode:function (t) {
            v(this), m(t, !1), y(t, K);
            var e = o(t), n = r(t);
            i(this, e.node, e.offset, n.node, n.offset)
        }, extractContents:R(u, i), deleteContents:R(c, i), canSurroundContents:function () {
            k(this), x(this.startContainer), x(this.endContainer);
            var t = new h(this, !0), n = t._first && e(t._first, this) || t._last && e(t._last, this);
            return t.detach(), !n
        }, detach:function () {
            a(this)
        }, splitBoundaries:function () {
            T(this)
        }, splitBoundariesPreservingPositions:function (t) {
            T(this, t)
        }, normalizeBoundaries:function () {
            k(this);
            var t = this.startContainer, e = this.startOffset, n = this.endContainer, o = this.endOffset, r = function (t) {
                var e = t.nextSibling;
                e && e.nodeType == t.nodeType && (n = t, o = t.length, t.appendData(e.data), e.parentNode.removeChild(e))
            }, a = function (r) {
                var i = r.previousSibling;
                if (i && i.nodeType == r.nodeType) {
                    t = r;
                    var a = r.length;
                    if (e = i.length, r.insertData(0, i.data), i.parentNode.removeChild(i), t == n)o += e, n = t; else if (n == r.parentNode) {
                        var s = H(r);
                        o == s ? (n = r, o = a) : o > s && o--
                    }
                }
            }, s = !0;
            if (j(n))n.length == o && r(n); else {
                if (o > 0) {
                    var l = n.childNodes[o - 1];
                    l && j(l) && r(l)
                }
                s = !this.collapsed
            }
            if (s) {
                if (j(t))0 == e && a(t); else if (e < t.childNodes.length) {
                    var c = t.childNodes[e];
                    c && j(c) && a(c)
                }
            } else t = n, e = o;
            i(this, t, e, n, o)
        }, collapseToPoint:function (t, e) {
            v(this), m(t, !0), b(t, e), this.setStartAndEnd(t, e)
        }}), P(n)
    }

    function O(t) {
        t.collapsed = t.startContainer === t.endContainer && t.startOffset === t.endOffset, t.commonAncestorContainer = t.collapsed ? t.startContainer : L.getCommonAncestor(t.startContainer, t.endContainer)
    }

    function D(t, e, n, o, r) {
        t.startContainer = e, t.startOffset = n, t.endContainer = o, t.endOffset = r, t.document = L.getDocument(e), O(t)
    }

    function I(t) {
        v(t), t.startContainer = t.startOffset = t.endContainer = t.endOffset = t.document = null, t.collapsed = t.commonAncestorContainer = null
    }

    function $(t) {
        this.startContainer = t, this.startOffset = 0, this.endContainer = t, this.endOffset = 0, this.document = t, O(this)
    }

    var L = t.dom, M = t.util, B = L.DomPosition, F = t.DOMException, j = L.isCharacterDataNode, H = L.getNodeIndex, W = L.isOrIsAncestorOf, q = L.getDocument, X = L.comparePoints, z = L.splitDataNode, G = L.getClosestAncestorIn, U = L.getNodeLength, V = L.arrayContains, Y = L.getRootContainer, Q = t.features.crashyTextNodes;
    h.prototype = {_current:null, _next:null, _first:null, _last:null, isSingleCharacterDataNode:!1, reset:function () {
        this._current = null, this._next = this._first
    }, hasNext:function () {
        return!!this._next
    }, next:function () {
        var t = this._current = this._next;
        return t && (this._next = t !== this._last ? t.nextSibling : null, j(t) && this.clonePartiallySelectedTextNodes && (t === this.ec && (t = t.cloneNode(!0)).deleteData(this.eo, t.length - this.eo), this._current === this.sc && (t = t.cloneNode(!0)).deleteData(0, this.so))), t
    }, remove:function () {
        var t, e, n = this._current;
        !j(n) || n !== this.sc && n !== this.ec ? n.parentNode && n.parentNode.removeChild(n) : (t = n === this.sc ? this.so : 0, e = n === this.ec ? this.eo : n.length, t != e && n.deleteData(t, e - t))
    }, isPartiallySelectedSubtree:function () {
        var t = this._current;
        return e(t, this.range)
    }, getSubtreeIterator:function () {
        var t;
        if (this.isSingleCharacterDataNode)t = this.range.cloneRange(), t.collapse(!1); else {
            t = new $(n(this.range));
            var e = this._current, o = e, r = 0, i = e, a = U(e);
            W(e, this.sc) && (o = this.sc, r = this.so), W(e, this.ec) && (i = this.ec, a = this.eo), D(t, o, r, i, a)
        }
        return new h(t, this.clonePartiallySelectedTextNodes)
    }, detach:function (t) {
        t && this.range.detach(), this.range = this._current = this._next = this._first = this._last = this.sc = this.so = this.ec = this.eo = null
    }}, p.prototype = {BAD_BOUNDARYPOINTS_ERR:1, INVALID_NODE_TYPE_ERR:2}, p.prototype.toString = function () {
        return this.message
    };
    var K = [1, 3, 4, 5, 7, 8, 10], J = [2, 9, 11], Z = [5, 6, 10, 12], te = [1, 3, 4, 5, 7, 8, 10, 11], ee = [1, 3, 4, 5, 7, 8], ne = g([9, 11]), oe = g(Z), re = g([6, 10, 12]), ie = document.createElement("style"), ae = !1;
    try {
        ie.innerHTML = "<b>x</b>", ae = 3 == ie.firstChild.nodeType
    } catch (se) {
    }
    t.features.htmlParsingConforms = ae;
    var le = ae ? function (t) {
        var e = this.startContainer, n = q(e);
        if (!e)throw new F("INVALID_STATE_ERR");
        var o = null;
        return 1 == e.nodeType ? o = e : j(e) && (o = L.parentElement(e)), o = null === o || "HTML" == o.nodeName && L.isHtmlNamespace(q(o).documentElement) && L.isHtmlNamespace(o) ? n.createElement("body") : o.cloneNode(!1), o.innerHTML = t, L.fragmentFromNodeChildren(o)
    } : function (t) {
        v(this);
        var e = n(this), o = e.createElement("body");
        return o.innerHTML = t, L.fragmentFromNodeChildren(o)
    }, ce = ["startContainer", "startOffset", "endContainer", "endOffset", "collapsed", "commonAncestorContainer"], ue = 0, de = 1, fe = 2, he = 3, pe = 0, ge = 1, me = 2, ve = 3;
    M.extend(t.rangePrototype, {compareBoundaryPoints:function (t, e) {
        k(this), w(this.startContainer, e.startContainer);
        var n, o, r, i, a = t == he || t == ue ? "start" : "end", s = t == de || t == ue ? "start" : "end";
        return n = this[a + "Container"], o = this[a + "Offset"], r = e[s + "Container"], i = e[s + "Offset"], X(n, o, r, i)
    }, insertNode:function (t) {
        if (k(this), y(t, te), x(this.startContainer), W(t, this.startContainer))throw new F("HIERARCHY_REQUEST_ERR");
        var e = i(t, this.startContainer, this.startOffset);
        this.setStartBefore(e)
    }, cloneContents:function () {
        k(this);
        var t, e;
        if (this.collapsed)return n(this).createDocumentFragment();
        if (this.startContainer === this.endContainer && j(this.startContainer))return t = this.startContainer.cloneNode(!0), t.data = t.data.slice(this.startOffset, this.endOffset), e = n(this).createDocumentFragment(), e.appendChild(t), e;
        var o = new h(this, !0);
        return t = s(o), o.detach(), t
    }, canSurroundContents:function () {
        k(this), x(this.startContainer), x(this.endContainer);
        var t = new h(this, !0), n = t._first && e(t._first, this) || t._last && e(t._last, this);
        return t.detach(), !n
    }, surroundContents:function (t) {
        if (y(t, ee), !this.canSurroundContents())throw new p("BAD_BOUNDARYPOINTS_ERR");
        var e = this.extractContents();
        if (t.hasChildNodes())for (; t.lastChild;)t.removeChild(t.lastChild);
        i(t, this.startContainer, this.startOffset), t.appendChild(e), this.selectNode(t)
    }, cloneRange:function () {
        k(this);
        for (var t, e = new $(n(this)), o = ce.length; o--;)t = ce[o], e[t] = this[t];
        return e
    }, toString:function () {
        k(this);
        var t = this.startContainer;
        if (t === this.endContainer && j(t))return 3 == t.nodeType || 4 == t.nodeType ? t.data.slice(this.startOffset, this.endOffset) : "";
        var e = [], n = new h(this, !0);
        return l(n, function (t) {
            (3 == t.nodeType || 4 == t.nodeType) && e.push(t.data)
        }), n.detach(), e.join("")
    }, compareNode:function (t) {
        k(this);
        var e = t.parentNode, n = H(t);
        if (!e)throw new F("NOT_FOUND_ERR");
        var o = this.comparePoint(e, n), r = this.comparePoint(e, n + 1);
        return 0 > o ? r > 0 ? me : pe : r > 0 ? ge : ve
    }, comparePoint:function (t, e) {
        return k(this), C(t, "HIERARCHY_REQUEST_ERR"), w(t, this.startContainer), X(t, e, this.startContainer, this.startOffset) < 0 ? -1 : X(t, e, this.endContainer, this.endOffset) > 0 ? 1 : 0
    }, createContextualFragment:le, toHtml:function () {
        k(this);
        var t = this.commonAncestorContainer.parentNode.cloneNode(!1);
        return t.appendChild(this.cloneContents()), t.innerHTML
    }, intersectsNode:function (t, e) {
        if (k(this), C(t, "NOT_FOUND_ERR"), q(t) !== n(this))return!1;
        var o = t.parentNode, r = H(t);
        C(o, "NOT_FOUND_ERR");
        var i = X(o, r, this.endContainer, this.endOffset), a = X(o, r + 1, this.startContainer, this.startOffset);
        return e ? 0 >= i && a >= 0 : 0 > i && a > 0
    }, isPointInRange:function (t, e) {
        return k(this), C(t, "HIERARCHY_REQUEST_ERR"), w(t, this.startContainer), X(t, e, this.startContainer, this.startOffset) >= 0 && X(t, e, this.endContainer, this.endOffset) <= 0
    }, intersectsRange:function (t) {
        return a(this, t, !1)
    }, intersectsOrTouchesRange:function (t) {
        return a(this, t, !0)
    }, intersection:function (t) {
        if (this.intersectsRange(t)) {
            var e = X(this.startContainer, this.startOffset, t.startContainer, t.startOffset), n = X(this.endContainer, this.endOffset, t.endContainer, t.endOffset), o = this.cloneRange();
            return-1 == e && o.setStart(t.startContainer, t.startOffset), 1 == n && o.setEnd(t.endContainer, t.endOffset), o
        }
        return null
    }, union:function (t) {
        if (this.intersectsOrTouchesRange(t)) {
            var e = this.cloneRange();
            return-1 == X(t.startContainer, t.startOffset, this.startContainer, this.startOffset) && e.setStart(t.startContainer, t.startOffset), 1 == X(t.endContainer, t.endOffset, this.endContainer, this.endOffset) && e.setEnd(t.endContainer, t.endOffset), e
        }
        throw new p("Ranges do not intersect")
    }, containsNode:function (t, e) {
        return e ? this.intersectsNode(t, !1) : this.compareNode(t) == ve
    }, containsNodeContents:function (t) {
        return this.comparePoint(t, 0) >= 0 && this.comparePoint(t, U(t)) <= 0
    }, containsRange:function (t) {
        var e = this.intersection(t);
        return null !== e && t.equals(e)
    }, containsNodeText:function (t) {
        var e = this.cloneRange();
        e.selectNode(t);
        var n = e.getNodes([3]);
        if (n.length > 0) {
            e.setStart(n[0], 0);
            var o = n.pop();
            e.setEnd(o, o.length);
            var r = this.containsRange(e);
            return e.detach(), r
        }
        return this.containsNodeContents(t)
    }, getNodes:function (t, e) {
        return k(this), d(this, t, e)
    }, getDocument:function () {
        return n(this)
    }, collapseBefore:function (t) {
        v(this), this.setEndBefore(t), this.collapse(!1)
    }, collapseAfter:function (t) {
        v(this), this.setStartAfter(t), this.collapse(!0)
    }, getBookmark:function (e) {
        var o = n(this), r = t.createRange(o);
        e = e || L.getBody(o), r.selectNodeContents(e);
        var i = this.intersection(r), a = 0, s = 0;
        return i && (r.setEnd(i.startContainer, i.startOffset), a = r.toString().length, s = a + i.toString().length, r.detach()), {start:a, end:s, containerNode:e}
    }, moveToBookmark:function (t) {
        var e = t.containerNode, n = 0;
        this.setStart(e, 0), this.collapse(!0);
        for (var o, r, i, a, s = [e], l = !1, c = !1; !c && (o = s.pop());)if (3 == o.nodeType)r = n + o.length, !l && t.start >= n && t.start <= r && (this.setStart(o, t.start - n), l = !0), l && t.end >= n && t.end <= r && (this.setEnd(o, t.end - n), c = !0), n = r; else for (a = o.childNodes, i = a.length; i--;)s.push(a[i])
    }, getName:function () {
        return"DomRange"
    }, equals:function (t) {
        return $.rangesEqual(this, t)
    }, isValid:function () {
        return _(this)
    }, inspect:function () {
        return f(this)
    }}), A($, D, I), M.extend($, {rangeProperties:ce, RangeIterator:h, copyComparisonConstants:P, createPrototypeRange:A, inspect:f, getRangeDocument:n, rangesEqual:function (t, e) {
        return t.startContainer === e.startContainer && t.startOffset === e.startOffset && t.endContainer === e.endContainer && t.endOffset === e.endOffset
    }}), t.DomRange = $, t.RangeException = p
}), rangy.createCoreModule("WrappedRange", ["DomRange"], function (t, e) {
    var n, o, r = t.dom, i = t.util, a = r.DomPosition, s = t.DomRange, l = r.getBody, c = r.getContentDocument, u = r.isCharacterDataNode;
    if (t.features.implementsDomRange && function () {
        function o(t) {
            for (var e, n = h.length; n--;)e = h[n], t[e] = t.nativeRange[e];
            t.collapsed = t.startContainer === t.endContainer && t.startOffset === t.endOffset
        }

        function a(t, e, n, o, r) {
            var i = t.startContainer !== e || t.startOffset != n, a = t.endContainer !== o || t.endOffset != r, s = !t.equals(t.nativeRange);
            (i || a || s) && (t.setEnd(o, r), t.setStart(e, n))
        }

        function u(t) {
            t.nativeRange.detach(), t.detached = !0;
            for (var e = h.length; e--;)t[h[e]] = null
        }

        var d, f, h = s.rangeProperties;
        n = function (t) {
            if (!t)throw e.createError("WrappedRange: Range must be specified");
            this.nativeRange = t, o(this)
        }, s.createPrototypeRange(n, a, u), d = n.prototype, d.selectNode = function (t) {
            this.nativeRange.selectNode(t), o(this)
        }, d.cloneContents = function () {
            return this.nativeRange.cloneContents()
        }, d.surroundContents = function (t) {
            this.nativeRange.surroundContents(t), o(this)
        }, d.collapse = function (t) {
            this.nativeRange.collapse(t), o(this)
        }, d.cloneRange = function () {
            return new n(this.nativeRange.cloneRange())
        }, d.refresh = function () {
            o(this)
        }, d.toString = function () {
            return this.nativeRange.toString()
        };
        var p = document.createTextNode("test");
        l(document).appendChild(p);
        var g = document.createRange();
        g.setStart(p, 0), g.setEnd(p, 0);
        try {
            g.setStart(p, 1), d.setStart = function (t, e) {
                this.nativeRange.setStart(t, e), o(this)
            }, d.setEnd = function (t, e) {
                this.nativeRange.setEnd(t, e), o(this)
            }, f = function (t) {
                return function (e) {
                    this.nativeRange[t](e), o(this)
                }
            }
        } catch (m) {
            d.setStart = function (t, e) {
                try {
                    this.nativeRange.setStart(t, e)
                } catch (n) {
                    this.nativeRange.setEnd(t, e), this.nativeRange.setStart(t, e)
                }
                o(this)
            }, d.setEnd = function (t, e) {
                try {
                    this.nativeRange.setEnd(t, e)
                } catch (n) {
                    this.nativeRange.setStart(t, e), this.nativeRange.setEnd(t, e)
                }
                o(this)
            }, f = function (t, e) {
                return function (n) {
                    try {
                        this.nativeRange[t](n)
                    } catch (r) {
                        this.nativeRange[e](n), this.nativeRange[t](n)
                    }
                    o(this)
                }
            }
        }
        d.setStartBefore = f("setStartBefore", "setEndBefore"), d.setStartAfter = f("setStartAfter", "setEndAfter"), d.setEndBefore = f("setEndBefore", "setStartBefore"), d.setEndAfter = f("setEndAfter", "setStartAfter"), d.selectNodeContents = function (t) {
            this.setStartAndEnd(t, 0, r.getNodeLength(t))
        }, g.selectNodeContents(p), g.setEnd(p, 3);
        var v = document.createRange();
        v.selectNodeContents(p), v.setEnd(p, 4), v.setStart(p, 2), d.compareBoundaryPoints = -1 == g.compareBoundaryPoints(g.START_TO_END, v) && 1 == g.compareBoundaryPoints(g.END_TO_START, v) ? function (t, e) {
            return e = e.nativeRange || e, t == e.START_TO_END ? t = e.END_TO_START : t == e.END_TO_START && (t = e.START_TO_END), this.nativeRange.compareBoundaryPoints(t, e)
        } : function (t, e) {
            return this.nativeRange.compareBoundaryPoints(t, e.nativeRange || e)
        };
        var y = document.createElement("div");
        y.innerHTML = "123";
        var b = y.firstChild, w = l(document);
        w.appendChild(y), g.setStart(b, 1), g.setEnd(b, 2), g.deleteContents(), "13" == b.data && (d.deleteContents = function () {
            this.nativeRange.deleteContents(), o(this)
        }, d.extractContents = function () {
            var t = this.nativeRange.extractContents();
            return o(this), t
        }), w.removeChild(y), w = null, i.isHostMethod(g, "createContextualFragment") && (d.createContextualFragment = function (t) {
            return this.nativeRange.createContextualFragment(t)
        }), l(document).removeChild(p), g.detach(), v.detach(), d.getName = function () {
            return"WrappedRange"
        }, t.WrappedRange = n, t.createNativeRange = function (t) {
            return t = c(t, e, "createNativeRange"), t.createRange()
        }
    }(), t.features.implementsTextRange) {
        var d = function (t) {
            var e = t.parentElement(), n = t.duplicate();
            n.collapse(!0);
            var o = n.parentElement();
            n = t.duplicate(), n.collapse(!1);
            var i = n.parentElement(), a = o == i ? o : r.getCommonAncestor(o, i);
            return a == e ? a : r.getCommonAncestor(e, a)
        }, f = function (t) {
            return 0 == t.compareEndPoints("StartToEnd", t)
        }, h = function (t, e, n, o, i) {
            var s = t.duplicate();
            s.collapse(n);
            var l = s.parentElement();
            if (r.isOrIsAncestorOf(e, l) || (l = e), !l.canHaveHTML) {
                var c = new a(l.parentNode, r.getNodeIndex(l));
                return{boundaryPosition:c, nodeInfo:{nodeIndex:c.offset, containerElement:c.node}}
            }
            var d = r.getDocument(l).createElement("span");
            d.parentNode && d.parentNode.removeChild(d);
            for (var f, h, p, g, m, v = n ? "StartToStart" : "StartToEnd", y = i && i.containerElement == l ? i.nodeIndex : 0, b = l.childNodes.length, w = b, x = w; x == b ? l.appendChild(d) : l.insertBefore(d, l.childNodes[x]), s.moveToElementText(d), f = s.compareEndPoints(v, t), 0 != f && y != w;) {
                if (-1 == f) {
                    if (w == y + 1)break;
                    y = x
                } else w = w == y + 1 ? y : x;
                x = Math.floor((y + w) / 2), l.removeChild(d)
            }
            if (m = d.nextSibling, -1 == f && m && u(m)) {
                s.setEndPoint(n ? "EndToStart" : "EndToEnd", t);
                var C;
                if (/[\r\n]/.test(m.data)) {
                    var E = s.duplicate(), S = E.text.replace(/\r\n/g, "\r").length;
                    for (C = E.moveStart("character", S); -1 == (f = E.compareEndPoints("StartToEnd", E));)C++, E.moveStart("character", 1)
                } else C = s.text.length;
                g = new a(m, C)
            } else h = (o || !n) && d.previousSibling, p = (o || n) && d.nextSibling, g = p && u(p) ? new a(p, 0) : h && u(h) ? new a(h, h.data.length) : new a(l, r.getNodeIndex(d));
            return d.parentNode.removeChild(d), {boundaryPosition:g, nodeInfo:{nodeIndex:x, containerElement:l}}
        }, p = function (t, e) {
            var n, o, i, a, s = t.offset, c = r.getDocument(t.node), d = l(c).createTextRange(), f = u(t.node);
            return f ? (n = t.node, o = n.parentNode) : (a = t.node.childNodes, n = s < a.length ? a[s] : null, o = t.node), i = c.createElement("span"), i.innerHTML = "&#feff;", n ? o.insertBefore(i, n) : o.appendChild(i), d.moveToElementText(i), d.collapse(!e), o.removeChild(i), f && d[e ? "moveStart" : "moveEnd"]("character", s), d
        };
        if (o = function (t) {
            this.textRange = t, this.refresh()
        }, o.prototype = new s(document), o.prototype.refresh = function () {
            var t, e, n, o = d(this.textRange);
            f(this.textRange) ? e = t = h(this.textRange, o, !0, !0).boundaryPosition : (n = h(this.textRange, o, !0, !1), t = n.boundaryPosition, e = h(this.textRange, o, !1, !1, n.nodeInfo).boundaryPosition), this.setStart(t.node, t.offset), this.setEnd(e.node, e.offset)
        }, o.prototype.getName = function () {
            return"WrappedTextRange"
        }, s.copyComparisonConstants(o), o.rangeToTextRange = function (t) {
            if (t.collapsed)return p(new a(t.startContainer, t.startOffset), !0);
            var e = p(new a(t.startContainer, t.startOffset), !0), n = p(new a(t.endContainer, t.endOffset), !1), o = l(s.getRangeDocument(t)).createTextRange();
            return o.setEndPoint("StartToStart", e), o.setEndPoint("EndToEnd", n), o
        }, t.WrappedTextRange = o, !t.features.implementsDomRange || t.config.preferTextRange) {
            var g = function () {
                return this
            }();
            "undefined" == typeof g.Range && (g.Range = o), t.createNativeRange = function (t) {
                return t = c(t, e, "createNativeRange"), l(t).createTextRange()
            }, t.WrappedRange = o
        }
    }
    t.createRange = function (n) {
        return n = c(n, e, "createRange"), new t.WrappedRange(t.createNativeRange(n))
    }, t.createRangyRange = function (t) {
        return t = c(t, e, "createRangyRange"), new s(t)
    }, t.createIframeRange = function (n) {
        return e.deprecationNotice("createIframeRange()", "createRange(iframeEl)"), t.createRange(n)
    }, t.createIframeRangyRange = function (n) {
        return e.deprecationNotice("createIframeRangyRange()", "createRangyRange(iframeEl)"), t.createRangyRange(n)
    }, t.addCreateMissingNativeApiListener(function (e) {
        var n = e.document;
        "undefined" == typeof n.createRange && (n.createRange = function () {
            return t.createRange(n)
        }), n = e = null
    })
}), rangy.createCoreModule("WrappedSelection", ["DomRange", "WrappedRange"], function (t, e) {
    function n(t) {
        return"string" == typeof t ? /^backward(s)?$/i.test(t) : !!t
    }

    function o(t, n) {
        if (!t)return window;
        if (N.isWindow(t))return t;
        if (t instanceof v)return t.win;
        var o = N.getContentDocument(t, e, n);
        return N.getWindow(o)
    }

    function r(t) {
        return o(t, "getWinSelection").getSelection()
    }

    function i(t) {
        return o(t, "getDocSelection").document.selection
    }

    function a(t) {
        var e = !1;
        return t.anchorNode && (e = 1 == N.comparePoints(t.anchorNode, t.anchorOffset, t.focusNode, t.focusOffset)), e
    }

    function s(t, e, n) {
        var o = n ? "end" : "start", r = n ? "start" : "end";
        t.anchorNode = e[o + "Container"], t.anchorOffset = e[o + "Offset"], t.focusNode = e[r + "Container"], t.focusOffset = e[r + "Offset"]
    }

    function l(t) {
        var e = t.nativeSelection;
        t.anchorNode = e.anchorNode, t.anchorOffset = e.anchorOffset, t.focusNode = e.focusNode, t.focusOffset = e.focusOffset
    }

    function c(t) {
        t.anchorNode = t.focusNode = null, t.anchorOffset = t.focusOffset = 0, t.rangeCount = 0, t.isCollapsed = !0, t._ranges.length = 0
    }

    function u(e) {
        var n;
        return e instanceof A ? (n = t.createNativeRange(e.getDocument()), n.setEnd(e.endContainer, e.endOffset), n.setStart(e.startContainer, e.startOffset)) : e instanceof O ? n = e.nativeRange : $.implementsDomRange && e instanceof N.getWindow(e.startContainer).Range && (n = e), n
    }

    function d(t) {
        if (!t.length || 1 != t[0].nodeType)return!1;
        for (var e = 1, n = t.length; n > e; ++e)if (!N.isAncestorOf(t[0], t[e]))return!1;
        return!0
    }

    function f(t) {
        var n = t.getNodes();
        if (!d(n))throw e.createError("getSingleElementFromRange: range " + t.inspect() + " did not consist of a single element");
        return n[0]
    }

    function h(t) {
        return!!t && "undefined" != typeof t.text
    }

    function p(t, e) {
        var n = new O(e);
        t._ranges = [n], s(t, n, !1), t.rangeCount = 1, t.isCollapsed = n.collapsed
    }

    function g(e) {
        if (e._ranges.length = 0, "None" == e.docSelection.type)c(e); else {
            var n = e.docSelection.createRange();
            if (h(n))p(e, n); else {
                e.rangeCount = n.length;
                for (var o, r = M(n.item(0)), i = 0; i < e.rangeCount; ++i)o = t.createRange(r), o.selectNode(n.item(i)), e._ranges.push(o);
                e.isCollapsed = 1 == e.rangeCount && e._ranges[0].collapsed, s(e, e._ranges[e.rangeCount - 1], !1)
            }
        }
    }

    function m(t, n) {
        for (var o = t.docSelection.createRange(), r = f(n), i = M(o.item(0)), a = B(i).createControlRange(), s = 0, l = o.length; l > s; ++s)a.add(o.item(s));
        try {
            a.add(r)
        } catch (c) {
            throw e.createError("addRange(): Element within the specified Range could not be added to control selection (does it have layout?)")
        }
        a.select(), g(t)
    }

    function v(t, e, n) {
        this.nativeSelection = t, this.docSelection = e, this._ranges = [], this.win = n, this.refresh()
    }

    function y(t) {
        t.win = t.anchorNode = t.focusNode = t._ranges = null, t.rangeCount = t.anchorOffset = t.focusOffset = 0, t.detached = !0
    }

    function b(t, e) {
        for (var n, o, r = ee.length; r--;)if (n = ee[r], o = n.selection, "deleteAll" == e)y(o); else if (n.win == t)return"delete" == e ? (ee.splice(r, 1), !0) : o;
        return"deleteAll" == e && (ee.length = 0), null
    }

    function w(t, n) {
        for (var o, r = M(n[0].startContainer), i = B(r).createControlRange(), a = 0, s = n.length; s > a; ++a) {
            o = f(n[a]);
            try {
                i.add(o)
            } catch (l) {
                throw e.createError("setRanges(): Element within one of the specified Ranges could not be added to control selection (does it have layout?)")
            }
        }
        i.select(), g(t)
    }

    function x(t, e) {
        if (t.win.document != M(e))throw new D("WRONG_DOCUMENT_ERR")
    }

    function C(e) {
        return function (n, o) {
            var r;
            this.rangeCount ? (r = this.getRangeAt(0), r["set" + (e ? "Start" : "End")](n, o)) : (r = t.createRange(this.win.document), r.setStartAndEnd(n, o)), this.setSingleRange(r, this.isBackward())
        }
    }

    function E(t) {
        var e = [], n = new I(t.anchorNode, t.anchorOffset), o = new I(t.focusNode, t.focusOffset), r = "function" == typeof t.getName ? t.getName() : "Selection";
        if ("undefined" != typeof t.rangeCount)for (var i = 0, a = t.rangeCount; a > i; ++i)e[i] = A.inspect(t.getRangeAt(i));
        return"[" + r + "(Ranges: " + e.join(", ") + ")(anchor: " + n.inspect() + ", focus: " + o.inspect() + "]"
    }

    t.config.checkSelectionRanges = !0;
    var S, _, k = "boolean", T = "number", N = t.dom, P = t.util, R = P.isHostMethod, A = t.DomRange, O = t.WrappedRange, D = t.DOMException, I = N.DomPosition, $ = t.features, L = "Control", M = N.getDocument, B = N.getBody, F = A.rangesEqual, j = R(window, "getSelection"), H = P.isHostObject(document, "selection");
    $.implementsWinGetSelection = j, $.implementsDocSelection = H;
    var W = H && (!j || t.config.preferTextRange);
    W ? (S = i, t.isSelectionValid = function (t) {
        var e = o(t, "isSelectionValid").document, n = e.selection;
        return"None" != n.type || M(n.createRange().parentElement()) == e
    }) : j ? (S = r, t.isSelectionValid = function () {
        return!0
    }) : e.fail("Neither document.selection or window.getSelection() detected."), t.getNativeSelection = S;
    var q = S(), X = t.createNativeRange(document), z = B(document), G = P.areHostProperties(q, ["anchorNode", "focusNode", "anchorOffset", "focusOffset"]);
    $.selectionHasAnchorAndFocus = G;
    var U = R(q, "extend");
    $.selectionHasExtend = U;
    var V = typeof q.rangeCount == T;
    $.selectionHasRangeCount = V;
    var Y = !1, Q = !0, K = U ? function (e, n) {
        var o = A.getRangeDocument(n), r = t.createRange(o);
        r.collapseToPoint(n.endContainer, n.endOffset), e.addRange(u(r)), e.extend(n.startContainer, n.startOffset)
    } : null;
    P.areHostMethods(q, ["addRange", "getRangeAt", "removeAllRanges"]) && typeof q.rangeCount == T && $.implementsDomRange && function () {
        var e = window.getSelection();
        if (e) {
            for (var n = e.rangeCount, o = n > 1, r = [], i = a(e), s = 0; n > s; ++s)r[s] = e.getRangeAt(s);
            var l = B(document), c = l.appendChild(document.createElement("div"));
            c.contentEditable = "false";
            var u = c.appendChild(document.createTextNode("Â Â Â ")), d = document.createRange();
            if (d.setStart(u, 1), d.collapse(!0), e.addRange(d), Q = 1 == e.rangeCount, e.removeAllRanges(), !o) {
                var f = d.cloneRange();
                d.setStart(u, 0), f.setEnd(u, 3), f.setStart(u, 2), e.addRange(d), e.addRange(f), Y = 2 == e.rangeCount, f.detach()
            }
            for (l.removeChild(c), e.removeAllRanges(), d.detach(), s = 0; n > s; ++s)0 == s && i ? K ? K(e, r[s]) : (t.warn("Rangy initialization: original selection was backwards but selection has been restored forwards because browser does not support Selection.extend"), e.addRange(r[s])) : e.addRange(r[s])
        }
    }(), $.selectionSupportsMultipleRanges = Y, $.collapsedNonEditableSelectionsSupported = Q;
    var J, Z = !1;
    z && R(z, "createControlRange") && (J = z.createControlRange(), P.areHostProperties(J, ["item", "add"]) && (Z = !0)), $.implementsControlRange = Z, _ = G ? function (t) {
        return t.anchorNode === t.focusNode && t.anchorOffset === t.focusOffset
    } : function (t) {
        return t.rangeCount ? t.getRangeAt(t.rangeCount - 1).collapsed : !1
    };
    var te;
    R(q, "getRangeAt") ? te = function (t, e) {
        try {
            return t.getRangeAt(e)
        } catch (n) {
            return null
        }
    } : G && (te = function (e) {
        var n = M(e.anchorNode), o = t.createRange(n);
        return o.setStartAndEnd(e.anchorNode, e.anchorOffset, e.focusNode, e.focusOffset), o.collapsed !== this.isCollapsed && o.setStartAndEnd(e.focusNode, e.focusOffset, e.anchorNode, e.anchorOffset), o
    }), v.prototype = t.selectionPrototype;
    var ee = [], ne = function (t) {
        if (t && t instanceof v)return t.refresh(), t;
        t = o(t, "getNativeSelection");
        var e = b(t), n = S(t), r = H ? i(t) : null;
        return e ? (e.nativeSelection = n, e.docSelection = r, e.refresh()) : (e = new v(n, r, t), ee.push({win:t, selection:e})), e
    };
    t.getSelection = ne, t.getIframeSelection = function (n) {
        return e.deprecationNotice("getIframeSelection()", "getSelection(iframeEl)"), t.getSelection(N.getIframeWindow(n))
    };
    var oe = v.prototype;
    if (!W && G && P.areHostMethods(q, ["removeAllRanges", "addRange"])) {
        oe.removeAllRanges = function () {
            this.nativeSelection.removeAllRanges(), c(this)
        };
        var re = function (t, e) {
            K(t.nativeSelection, e), t.refresh()
        };
        oe.addRange = V ? function (e, o) {
            if (Z && H && this.docSelection.type == L)m(this, e); else if (n(o) && U)re(this, e); else {
                var r;
                if (Y ? r = this.rangeCount : (this.removeAllRanges(), r = 0), this.nativeSelection.addRange(u(e).cloneRange()), this.rangeCount = this.nativeSelection.rangeCount, this.rangeCount == r + 1) {
                    if (t.config.checkSelectionRanges) {
                        var i = te(this.nativeSelection, this.rangeCount - 1);
                        i && !F(i, e) && (e = new O(i))
                    }
                    this._ranges[this.rangeCount - 1] = e, s(this, e, se(this.nativeSelection)), this.isCollapsed = _(this)
                } else this.refresh()
            }
        } : function (t, e) {
            n(e) && U ? re(this, t) : (this.nativeSelection.addRange(u(t)), this.refresh())
        }, oe.setRanges = function (t) {
            if (Z && t.length > 1)w(this, t); else {
                this.removeAllRanges();
                for (var e = 0, n = t.length; n > e; ++e)this.addRange(t[e])
            }
        }
    } else {
        if (!(R(q, "empty") && R(X, "select") && Z && W))return e.fail("No means of selecting a Range or TextRange was found"), !1;
        oe.removeAllRanges = function () {
            try {
                if (this.docSelection.empty(), "None" != this.docSelection.type) {
                    var t;
                    if (this.anchorNode)t = M(this.anchorNode); else if (this.docSelection.type == L) {
                        var e = this.docSelection.createRange();
                        e.length && (t = M(e.item(0)))
                    }
                    if (t) {
                        var n = B(t).createTextRange();
                        n.select(), this.docSelection.empty()
                    }
                }
            } catch (o) {
            }
            c(this)
        }, oe.addRange = function (e) {
            this.docSelection.type == L ? m(this, e) : (t.WrappedTextRange.rangeToTextRange(e).select(), this._ranges[0] = e, this.rangeCount = 1, this.isCollapsed = this._ranges[0].collapsed, s(this, e, !1))
        }, oe.setRanges = function (t) {
            this.removeAllRanges();
            var e = t.length;
            e > 1 ? w(this, t) : e && this.addRange(t[0])
        }
    }
    oe.getRangeAt = function (t) {
        if (0 > t || t >= this.rangeCount)throw new D("INDEX_SIZE_ERR");
        return this._ranges[t].cloneRange()
    };
    var ie;
    if (W)ie = function (e) {
        var n;
        t.isSelectionValid(e.win) ? n = e.docSelection.createRange() : (n = B(e.win.document).createTextRange(), n.collapse(!0)), e.docSelection.type == L ? g(e) : h(n) ? p(e, n) : c(e)
    }; else if (R(q, "getRangeAt") && typeof q.rangeCount == T)ie = function (e) {
        if (Z && H && e.docSelection.type == L)g(e); else if (e._ranges.length = e.rangeCount = e.nativeSelection.rangeCount, e.rangeCount) {
            for (var n = 0, o = e.rangeCount; o > n; ++n)e._ranges[n] = new t.WrappedRange(e.nativeSelection.getRangeAt(n));
            s(e, e._ranges[e.rangeCount - 1], se(e.nativeSelection)), e.isCollapsed = _(e)
        } else c(e)
    }; else {
        if (!G || typeof q.isCollapsed != k || typeof X.collapsed != k || !$.implementsDomRange)return e.fail("No means of obtaining a Range or TextRange from the user's selection was found"), !1;
        ie = function (t) {
            var e, n = t.nativeSelection;
            n.anchorNode ? (e = te(n, 0), t._ranges = [e], t.rangeCount = 1, l(t), t.isCollapsed = _(t)) : c(t)
        }
    }
    oe.refresh = function (t) {
        var e = t ? this._ranges.slice(0) : null, n = this.anchorNode, o = this.anchorOffset;
        if (ie(this), t) {
            var r = e.length;
            if (r != this._ranges.length)return!0;
            if (this.anchorNode != n || this.anchorOffset != o)return!0;
            for (; r--;)if (!F(e[r], this._ranges[r]))return!0;
            return!1
        }
    };
    var ae = function (t, e) {
        var n = t.getAllRanges();
        t.removeAllRanges();
        for (var o = 0, r = n.length; r > o; ++o)F(e, n[o]) || t.addRange(n[o]);
        t.rangeCount || c(t)
    };
    oe.removeRange = Z ? function (t) {
        if (this.docSelection.type == L) {
            for (var e, n = this.docSelection.createRange(), o = f(t), r = M(n.item(0)), i = B(r).createControlRange(), a = !1, s = 0, l = n.length; l > s; ++s)e = n.item(s), e !== o || a ? i.add(n.item(s)) : a = !0;
            i.select(), g(this)
        } else ae(this, t)
    } : function (t) {
        ae(this, t)
    };
    var se;
    !W && G && $.implementsDomRange ? (se = a, oe.isBackward = function () {
        return se(this)
    }) : se = oe.isBackward = function () {
        return!1
    }, oe.isBackwards = oe.isBackward, oe.toString = function () {
        for (var t = [], e = 0, n = this.rangeCount; n > e; ++e)t[e] = "" + this._ranges[e];
        return t.join("")
    }, oe.collapse = function (e, n) {
        x(this, e);
        var o = t.createRange(e);
        o.collapseToPoint(e, n), this.setSingleRange(o), this.isCollapsed = !0
    }, oe.collapseToStart = function () {
        if (!this.rangeCount)throw new D("INVALID_STATE_ERR");
        var t = this._ranges[0];
        this.collapse(t.startContainer, t.startOffset)
    }, oe.collapseToEnd = function () {
        if (!this.rangeCount)throw new D("INVALID_STATE_ERR");
        var t = this._ranges[this.rangeCount - 1];
        this.collapse(t.endContainer, t.endOffset)
    }, oe.selectAllChildren = function (e) {
        x(this, e);
        var n = t.createRange(e);
        n.selectNodeContents(e), this.setSingleRange(n)
    }, oe.deleteFromDocument = function () {
        if (Z && H && this.docSelection.type == L) {
            for (var t, e = this.docSelection.createRange(); e.length;)t = e.item(0), e.remove(t), t.parentNode.removeChild(t);
            this.refresh()
        } else if (this.rangeCount) {
            var n = this.getAllRanges();
            if (n.length) {
                this.removeAllRanges();
                for (var o = 0, r = n.length; r > o; ++o)n[o].deleteContents();
                this.addRange(n[r - 1])
            }
        }
    }, oe.eachRange = function (t, e) {
        for (var n = 0, o = this._ranges.length; o > n; ++n)if (t(this.getRangeAt(n)))return e
    }, oe.getAllRanges = function () {
        var t = [];
        return this.eachRange(function (e) {
            t.push(e)
        }), t
    }, oe.setSingleRange = function (t, e) {
        this.removeAllRanges(), this.addRange(t, e)
    }, oe.callMethodOnEachRange = function (t, e) {
        var n = [];
        return this.eachRange(function (o) {
            n.push(o[t].apply(o, e))
        }), n
    }, oe.setStart = C(!0), oe.setEnd = C(!1), t.rangePrototype.select = function (t) {
        ne(this.getDocument()).setSingleRange(this, t)
    }, oe.changeEachRange = function (t) {
        var e = [], n = this.isBackward();
        this.eachRange(function (n) {
            t(n), e.push(n)
        }), this.removeAllRanges(), n && 1 == e.length ? this.addRange(e[0], "backward") : this.setRanges(e)
    }, oe.containsNode = function (t, e) {
        return this.eachRange(function (n) {
            return n.containsNode(t, e)
        }, !0)
    }, oe.getBookmark = function (t) {
        return{backward:this.isBackward(), rangeBookmarks:this.callMethodOnEachRange("getBookmark", [t])}
    }, oe.moveToBookmark = function (e) {
        for (var n, o, r = [], i = 0; n = e.rangeBookmarks[i++];)o = t.createRange(this.win), o.moveToBookmark(n), r.push(o);
        e.backward ? this.setSingleRange(r[0], "backward") : this.setRanges(r)
    }, oe.toHtml = function () {
        return this.callMethodOnEachRange("toHtml").join("")
    }, oe.getName = function () {
        return"WrappedSelection"
    }, oe.inspect = function () {
        return E(this)
    }, oe.detach = function () {
        b(this.win, "delete"), y(this)
    }, v.detachAll = function () {
        b(null, "deleteAll")
    }, v.inspect = E, v.isDirectionBackward = n, t.Selection = v, t.selectionPrototype = oe, t.addCreateMissingNativeApiListener(function (t) {
        "undefined" == typeof t.getSelection && (t.getSelection = function () {
            return ne(t)
        }), t = null
    })
}), rangy.createModule("TextRange", ["WrappedSelection"], function (t, e) {
    function n(t, e) {
        function n(e, n, o) {
            for (var r = t.slice(e, n), i = {isWord:o, chars:r, toString:function () {
                return r.join("")
            }}, a = 0, l = r.length; l > a; ++a)r[a].token = i;
            s.push(i)
        }

        for (var o, r, i, a = t.join(""), s = [], l = 0; o = e.wordRegex.exec(a);) {
            if (r = o.index, i = r + o[0].length, r > l && n(l, r, !1), e.includeTrailingSpace)for (; Y.test(t[i]);)++i;
            n(r, i, !0), l = i
        }
        return l < t.length && n(l, t.length, !1), s
    }

    function o(t, e) {
        if (!t)return e;
        var n = {};
        return X(n, e), X(n, t), n
    }

    function r(t) {
        var e, n;
        return t ? (e = t.language || Q, n = {}, X(n, ie[e] || ie[Q]), X(n, t), n) : ie[Q]
    }

    function i(t) {
        return o(t, oe)
    }

    function a(t) {
        return o(t, re)
    }

    function s(t, e) {
        var n = ue(t, "display", e), o = t.tagName.toLowerCase();
        return"block" == n && ne && de.hasOwnProperty(o) ? de[o] : n
    }

    function l(t) {
        for (var e = h(t), n = 0, o = e.length; o > n; ++n)if (1 == e[n].nodeType && "none" == s(e[n]))return!0;
        return!1
    }

    function c(t) {
        var e;
        return 3 == t.nodeType && (e = t.parentNode) && "hidden" == ue(e, "visibility")
    }

    function u(t) {
        return t && (1 == t.nodeType && !/^(inline(-block|-table)?|none)$/.test(s(t)) || 9 == t.nodeType || 11 == t.nodeType)
    }

    function d(t) {
        return W.isCharacterDataNode(t) || !/^(area|base|basefont|br|col|frame|hr|img|input|isindex|link|meta|param)$/i.test(t.nodeName)
    }

    function f(t) {
        for (var e = []; t.parentNode;)e.unshift(t.parentNode), t = t.parentNode;
        return e
    }

    function h(t) {
        return f(t).concat([t])
    }

    function p(t) {
        for (; t && !t.nextSibling;)t = t.parentNode;
        return t ? t.nextSibling : null
    }

    function g(t, e) {
        return!e && t.hasChildNodes() ? t.firstChild : p(t)
    }

    function m(t) {
        var e = t.previousSibling;
        if (e) {
            for (t = e; t.hasChildNodes();)t = t.lastChild;
            return t
        }
        var n = t.parentNode;
        return n && 1 == n.nodeType ? n : null
    }

    function v(t) {
        if (!t || 3 != t.nodeType)return!1;
        var e = t.data;
        if ("" === e)return!0;
        var n = t.parentNode;
        if (!n || 1 != n.nodeType)return!1;
        var o = ue(t.parentNode, "whiteSpace");
        return/^[\t\n\r ]+$/.test(e) && /^(normal|nowrap)$/.test(o) || /^[\t\r ]+$/.test(e) && "pre-line" == o
    }

    function y(t) {
        if ("" === t.data)return!0;
        if (!v(t))return!1;
        var e = t.parentNode;
        return e ? l(t) ? !0 : !1 : !0
    }

    function b(t) {
        var e = t.nodeType;
        return 7 == e || 8 == e || l(t) || /^(script|style)$/i.test(t.nodeName) || c(t) || y(t)
    }

    function w(t, e) {
        var n = t.nodeType;
        return 7 == n || 8 == n || 1 == n && "none" == s(t, e)
    }

    function x() {
        this.store = {}
    }

    function C(t, e, n) {
        return function (o) {
            var r = this.cache;
            if (r.hasOwnProperty(t))return fe++, r[t];
            he++;
            var i = e.call(this, n ? this[n] : this, o);
            return r[t] = i, i
        }
    }

    function E(t, e) {
        this.node = t, this.session = e, this.cache = new x, this.positions = new x
    }

    function S(t, e) {
        this.offset = e, this.nodeWrapper = t, this.node = t.node, this.session = t.session, this.cache = new x
    }

    function _() {
        return"[Position(" + W.inspectNode(this.node) + ":" + this.offset + ")]"
    }

    function k() {
        return N(), _e = new ke
    }

    function T() {
        return _e || k()
    }

    function N() {
        _e && _e.detach(), _e = null
    }

    function P(t, n, o, r) {
        function i() {
            var t = null;
            return n ? (t = s, l || (s = s.previousVisible(), l = !s || o && s.equals(o))) : l || (t = s = s.nextVisible(), l = !s || o && s.equals(o)), l && (s = null), t
        }

        o && (n ? b(o.node) && (o = t.previousVisible()) : b(o.node) && (o = o.nextVisible()));
        var a, s = t, l = !1, c = !1;
        return{next:function () {
            if (c)return c = !1, a;
            for (var t, e; t = i();)if (e = t.getCharacter(r))return a = t, t;
            return null
        }, rewind:function () {
            if (!a)throw e.createError("createCharacterIterator: cannot rewind. Only one position can be rewound.");
            c = !0
        }, dispose:function () {
            t = o = null
        }}
    }

    function R(t, e, n) {
        function o(t) {
            for (var e, n, o = [], a = t ? r : i, s = !1, l = !1; e = a.next();) {
                if (n = e.character, V.test(n))l && (l = !1, s = !0); else {
                    if (s) {
                        a.rewind();
                        break
                    }
                    l = !0
                }
                o.push(e)
            }
            return o
        }

        var r = P(t, !1, null, e), i = P(t, !0, null, e), a = n.tokenizer, s = o(!0), l = o(!1).reverse(), c = a(l.concat(s), n), u = s.length ? c.slice(Te(c, s[0].token)) : [], d = l.length ? c.slice(0, Te(c, l.pop().token) + 1) : [];
        return{nextEndToken:function () {
            for (var t, e; 1 == u.length && !(t = u[0]).isWord && (e = o(!0)).length > 0;)u = a(t.chars.concat(e), n);
            return u.shift()
        }, previousStartToken:function () {
            for (var t, e; 1 == d.length && !(t = d[0]).isWord && (e = o(!1)).length > 0;)d = a(e.reverse().concat(t.chars), n);
            return d.pop()
        }, dispose:function () {
            r.dispose(), i.dispose(), u = d = null
        }}
    }

    function A(t, e, n, o, r) {
        var i, a, s, l, c = 0, u = t, d = Math.abs(n);
        if (0 !== n) {
            var f = 0 > n;
            switch (e) {
                case j:
                    for (a = P(t, f, null, o); (i = a.next()) && d > c;)++c, u = i;
                    s = i, a.dispose();
                    break;
                case H:
                    for (var h = R(t, o, r), p = f ? h.previousStartToken : h.nextEndToken; (l = p()) && d > c;)l.isWord && (++c, u = f ? l.chars[0] : l.chars[l.chars.length - 1]);
                    break;
                default:
                    throw new Error("movePositionBy: unit '" + e + "' not implemented")
            }
            f ? (u = u.previousVisible(), c = -c) : u && u.isLeadingSpace && (e == H && (a = P(t, !1, null, o), s = a.next(), a.dispose()), s && (u = s.previousVisible()))
        }
        return{position:u, unitsMoved:c}
    }

    function O(t, e, n, o) {
        var r = t.getRangeBoundaryPosition(e, !0), i = t.getRangeBoundaryPosition(e, !1), a = o ? i : r, s = o ? r : i;
        return P(a, !!o, s, n)
    }

    function D(t, e, n) {
        for (var o, r = [], i = O(t, e, n); o = i.next();)r.push(o);
        return i.dispose(), r
    }

    function I(e, n, o) {
        var r = t.createRange(e.node);
        r.setStartAndEnd(e.node, e.offset, n.node, n.offset);
        var i = !r.expand("word", o);
        return r.detach(), i
    }

    function $(t, e, n, o, r) {
        function i(t, e) {
            var n = g[t].previousVisible(), o = g[e - 1], i = !r.wholeWordsOnly || I(n, o, r.wordOptions);
            return{startPos:n, endPos:o, valid:i}
        }

        for (var a, s, l, c, u, d, f = K(r.direction), h = P(t, f, t.session.getRangeBoundaryPosition(o, f), r), p = "", g = [], m = null; a = h.next();)if (s = a.character, !n && !r.caseSensitive && (s = s.toLowerCase()), f ? (g.unshift(a), p = s + p) : (g.push(a), p += s), n) {
            if (u = e.exec(p))if (d) {
                if (l = u.index, c = l + u[0].length, !f && c < p.length || f && l > 0) {
                    m = i(l, c);
                    break
                }
            } else d = !0
        } else if (-1 != (l = p.indexOf(e))) {
            m = i(l, l + e.length);
            break
        }
        return d && (m = i(l, c)), h.dispose(), m
    }

    function L(t) {
        return function () {
            var e = !!_e, n = T(), o = [n].concat(q.toArray(arguments)), r = t.apply(this, o);
            return e || N(), r
        }
    }

    function M(t, e) {
        return L(function (n, a, s, l) {
            "undefined" == typeof s && (s = a, a = j), l = o(l, se);
            var c = i(l.characterOptions), u = r(l.wordOptions), d = t;
            e && (d = s >= 0, this.collapse(!d));
            var f = A(n.getRangeBoundaryPosition(this, d), a, s, c, u), h = f.position;
            return this[d ? "setStart" : "setEnd"](h.node, h.offset), f.unitsMoved
        })
    }

    function B(t) {
        return L(function (e, n) {
            n = i(n);
            for (var o, r = O(e, this, n, !t), a = 0; (o = r.next()) && V.test(o.character);)++a;
            r.dispose();
            var s = a > 0;
            return s && this[t ? "moveStart" : "moveEnd"]("character", t ? a : -a, {characterOptions:n}), s
        })
    }

    function F(t) {
        return L(function (e, n) {
            var o = !1;
            return this.changeEachRange(function (e) {
                o = e[t](n) || o
            }), o
        })
    }

    var j = "character", H = "word", W = t.dom, q = t.util, X = q.extend, z = W.getBody, G = /^[ \t\f\r\n]+$/, U = /^[ \t\f\r]+$/, V = /^[\t-\r \u0085\u00A0\u1680\u180E\u2000-\u200B\u2028\u2029\u202F\u205F\u3000]+$/, Y = /^[\t \u00A0\u1680\u180E\u2000-\u200B\u202F\u205F\u3000]+$/, Q = "en", K = t.Selection.isDirectionBackward, J = !1, Z = !1, te = !1, ee = !0;
    !function () {
        var e = document.createElement("div");
        e.contentEditable = "true", e.innerHTML = "<p>1 </p><p></p>";
        var n = z(document), o = e.firstChild, r = t.getSelection();
        n.appendChild(e), r.collapse(o.lastChild, 2), r.setStart(o.firstChild, 0), J = 1 == ("" + r).length, e.innerHTML = "1 <br>", r.collapse(e, 2), r.setStart(e.firstChild, 0), Z = 1 == ("" + r).length, e.innerHTML = "1 <p>1</p>", r.collapse(e, 2), r.setStart(e.firstChild, 0), te = 1 == ("" + r).length, n.removeChild(e), r.removeAllRanges()
    }();
    var ne, oe = {includeBlockContentTrailingSpace:!0, includeSpaceBeforeBr:!0, includeSpaceBeforeBlock:!0, includePreLineTrailingSpace:!0}, re = {includeBlockContentTrailingSpace:!ee, includeSpaceBeforeBr:!Z, includeSpaceBeforeBlock:!te, includePreLineTrailingSpace:!0}, ie = {en:{wordRegex:/[a-z0-9]+('[a-z0-9]+)*/gi, includeTrailingSpace:!1, tokenizer:n}}, ae = {caseSensitive:!1, withinRange:null, wholeWordsOnly:!1, wrap:!1, direction:"forward", wordOptions:null, characterOptions:null}, se = {wordOptions:null, characterOptions:null}, le = {wordOptions:null, characterOptions:null, trim:!1, trimStart:!0, trimEnd:!0}, ce = {wordOptions:null, characterOptions:null, direction:"forward"}, ue = W.getComputedStyleProperty;
    !function () {
        var t = document.createElement("table"), e = z(document);
        e.appendChild(t), ne = "block" == ue(t, "display"), e.removeChild(t)
    }(), t.features.tableCssDisplayBlock = ne;
    var de = {table:"table", caption:"table-caption", colgroup:"table-column-group", col:"table-column", thead:"table-header-group", tbody:"table-row-group", tfoot:"table-footer-group", tr:"table-row", td:"table-cell", th:"table-cell"};
    x.prototype = {get:function (t) {
        return this.store.hasOwnProperty(t) ? this.store[t] : null
    }, set:function (t, e) {
        return this.store[t] = e
    }};
    var fe = 0, he = 0, pe = {getPosition:function (t) {
        var e = this.positions;
        return e.get(t) || e.set(t, new S(this, t))
    }, toString:function () {
        return"[NodeWrapper(" + W.inspectNode(this.node) + ")]"
    }};
    E.prototype = pe;
    var ge = "EMPTY", me = "NON_SPACE", ve = "UNCOLLAPSIBLE_SPACE", ye = "COLLAPSIBLE_SPACE", be = "TRAILING_SPACE_BEFORE_BLOCK", we = "TRAILING_SPACE_IN_BLOCK", xe = "TRAILING_SPACE_BEFORE_BR", Ce = "PRE_LINE_TRAILING_SPACE_BEFORE_LINE_BREAK", Ee = "TRAILING_LINE_BREAK_AFTER_BR";
    X(pe, {isCharacterDataNode:C("isCharacterDataNode", W.isCharacterDataNode, "node"), getNodeIndex:C("nodeIndex", W.getNodeIndex, "node"), getLength:C("nodeLength", W.getNodeLength, "node"), containsPositions:C("containsPositions", d, "node"), isWhitespace:C("isWhitespace", v, "node"), isCollapsedWhitespace:C("isCollapsedWhitespace", y, "node"), getComputedDisplay:C("computedDisplay", s, "node"), isCollapsed:C("collapsed", b, "node"), isIgnored:C("ignored", w, "node"), next:C("nextPos", g, "node"), previous:C("previous", m, "node"), getTextNodeInfo:C("textNodeInfo", function (t) {
        var e = null, n = !1, o = ue(t.parentNode, "whiteSpace"), r = "pre-line" == o;
        return r ? (e = U, n = !0) : ("normal" == o || "nowrap" == o) && (e = G, n = !0), {node:t, text:t.data, spaceRegex:e, collapseSpaces:n, preLine:r}
    }, "node"), hasInnerText:C("hasInnerText", function (t, e) {
        for (var n = this.session, o = n.getPosition(t.parentNode, this.getNodeIndex() + 1), r = n.getPosition(t, 0), i = e ? o : r, a = e ? r : o; i !== a;) {
            if (i.prepopulateChar(), i.isDefinitelyNonEmpty())return!0;
            i = e ? i.previousVisible() : i.nextVisible()
        }
        return!1
    }, "node"), isRenderedBlock:C("isRenderedBlock", function (t) {
        for (var e = t.getElementsByTagName("br"), n = 0, o = e.length; o > n; ++n)if (!b(e[n]))return!0;
        return this.hasInnerText()
    }, "node"), getTrailingSpace:C("trailingSpace", function (t) {
        if ("br" == t.tagName.toLowerCase())return"";
        switch (this.getComputedDisplay()) {
            case"inline":
                for (var e = t.lastChild; e;) {
                    if (!w(e))return 1 == e.nodeType ? this.session.getNodeWrapper(e).getTrailingSpace() : "";
                    e = e.previousSibling
                }
                break;
            case"inline-block":
            case"inline-table":
            case"none":
            case"table-column":
            case"table-column-group":
                break;
            case"table-cell":
                return"	";
            default:
                return this.isRenderedBlock(!0) ? "\n" : ""
        }
        return""
    }, "node"), getLeadingSpace:C("leadingSpace", function () {
        switch (this.getComputedDisplay()) {
            case"inline":
            case"inline-block":
            case"inline-table":
            case"none":
            case"table-column":
            case"table-column-group":
            case"table-cell":
                break;
            default:
                return this.isRenderedBlock(!1) ? "\n" : ""
        }
        return""
    }, "node")});
    var Se = {character:"", characterType:ge, isBr:!1, prepopulateChar:function () {
        var t = this;
        if (!t.prepopulatedChar) {
            var e = t.node, n = t.offset, o = "", r = ge, i = !1;
            if (n > 0)if (3 == e.nodeType) {
                var a = e.data, s = a.charAt(n - 1), l = t.nodeWrapper.getTextNodeInfo(), c = l.spaceRegex;
                l.collapseSpaces ? c.test(s) ? n > 1 && c.test(a.charAt(n - 2)) || (l.preLine && "\n" === a.charAt(n) ? (o = " ", r = Ce) : (o = " ", r = ye)) : (o = s, r = me, i = !0) : (o = s, r = ve, i = !0)
            } else {
                var u = e.childNodes[n - 1];
                if (u && 1 == u.nodeType && !b(u) && ("br" == u.tagName.toLowerCase() ? (o = "\n", t.isBr = !0, r = ye, i = !1) : t.checkForTrailingSpace = !0), !o) {
                    var d = e.childNodes[n];
                    d && 1 == d.nodeType && !b(d) && (t.checkForLeadingSpace = !0)
                }
            }
            t.prepopulatedChar = !0, t.character = o, t.characterType = r, t.isCharInvariant = i
        }
    }, isDefinitelyNonEmpty:function () {
        var t = this.characterType;
        return t == me || t == ve
    }, resolveLeadingAndTrailingSpaces:function () {
        if (this.prepopulatedChar || this.prepopulateChar(), this.checkForTrailingSpace) {
            var t = this.session.getNodeWrapper(this.node.childNodes[this.offset - 1]).getTrailingSpace();
            t && (this.isTrailingSpace = !0, this.character = t, this.characterType = ye), this.checkForTrailingSpace = !1
        }
        if (this.checkForLeadingSpace) {
            var e = this.session.getNodeWrapper(this.node.childNodes[this.offset]).getLeadingSpace();
            e && (this.isLeadingSpace = !0, this.character = e, this.characterType = ye), this.checkForLeadingSpace = !1
        }
    }, getPrecedingUncollapsedPosition:function (t) {
        for (var e, n = this; n = n.previousVisible();)if (e = n.getCharacter(t), "" !== e)return n;
        return null
    }, getCharacter:function (t) {
        function e() {
            return l || (i = c.getPrecedingUncollapsedPosition(t), l = !0), i
        }

        if (this.resolveLeadingAndTrailingSpaces(), this.isCharInvariant)return this.character;
        var n = ["character", t.includeSpaceBeforeBr, t.includeBlockContentTrailingSpace, t.includePreLineTrailingSpace].join("_"), o = this.cache.get(n);
        if (null !== o)return o;
        var r, i, a = "", s = this.characterType == ye, l = !1, c = this;
        return s ? (" " != this.character || e() && !i.isTrailingSpace && "\n" != i.character) && ("\n" == this.character && this.isLeadingSpace ? e() && "\n" != i.character && (a = "\n") : (r = this.nextUncollapsed(), r && (r.isBr ? this.type = xe : r.isTrailingSpace && "\n" == r.character ? this.type = we : r.isLeadingSpace && "\n" == r.character && (this.type = be), "\n" === r.character ? !(this.type == xe && !t.includeSpaceBeforeBr || this.type == be && !t.includeSpaceBeforeBlock || this.type == we && r.isTrailingSpace && !t.includeBlockContentTrailingSpace || this.type == Ce && r.type == me && !t.includePreLineTrailingSpace || !("\n" === this.character ? r.isTrailingSpace ? this.isTrailingSpace || this.isBr && (r.type = Ee, e() && i.isLeadingSpace && "\n" == i.character && (r.character = "")) : a = "\n" : " " === this.character && (a = " "))) : a = this.character))) : "\n" !== this.character || !!(r = this.nextUncollapsed()) && !r.isTrailingSpace, this.cache.set(n, a), a
    }, equals:function (t) {
        return!!t && this.node === t.node && this.offset === t.offset
    }, inspect:_, toString:function () {
        return this.character
    }};
    S.prototype = Se, X(Se, {next:C("nextPos", function (t) {
        var e = t.nodeWrapper, n = t.node, o = t.offset, r = e.session;
        if (!n)return null;
        var i, a, s;
        return o == e.getLength() ? (i = n.parentNode, a = i ? e.getNodeIndex() + 1 : 0) : e.isCharacterDataNode() ? (i = n, a = o + 1) : (s = n.childNodes[o], r.getNodeWrapper(s).containsPositions() ? (i = s, a = 0) : (i = n, a = o + 1)), i ? r.getPosition(i, a) : null
    }), previous:C("previous", function (t) {
        var e, n, o, r = t.nodeWrapper, i = t.node, a = t.offset, s = r.session;
        return 0 == a ? (e = i.parentNode, n = e ? r.getNodeIndex() : 0) : r.isCharacterDataNode() ? (e = i, n = a - 1) : (o = i.childNodes[a - 1], s.getNodeWrapper(o).containsPositions() ? (e = o, n = W.getNodeLength(o)) : (e = i, n = a - 1)), e ? s.getPosition(e, n) : null
    }), nextVisible:C("nextVisible", function (t) {
        var e = t.next();
        if (!e)return null;
        var n = e.nodeWrapper, o = e.node, r = e;
        return n.isCollapsed() && (r = n.session.getPosition(o.parentNode, n.getNodeIndex() + 1)), r
    }), nextUncollapsed:C("nextUncollapsed", function (t) {
        for (var e = t; e = e.nextVisible();)if (e.resolveLeadingAndTrailingSpaces(), "" !== e.character)return e;
        return null
    }), previousVisible:C("previousVisible", function (t) {
        var e = t.previous();
        if (!e)return null;
        var n = e.nodeWrapper, o = e.node, r = e;
        return n.isCollapsed() && (r = n.session.getPosition(o.parentNode, n.getNodeIndex())), r
    })});
    var _e = null, ke = function () {
        function t(t) {
            var e = new x;
            return{get:function (n) {
                var o = e.get(n[t]);
                if (o)for (var r, i = 0; r = o[i++];)if (r.node === n)return r;
                return null
            }, set:function (n) {
                var o = n.node[t], r = e.get(o) || e.set(o, []);
                r.push(n)
            }}
        }

        function e() {
            this.initCaches()
        }

        var n = q.isHostProperty(document.documentElement, "uniqueID");
        return e.prototype = {initCaches:function () {
            this.elementCache = n ? function () {
                var t = new x;
                return{get:function (e) {
                    return t.get(e.uniqueID)
                }, set:function (e) {
                    t.set(e.node.uniqueID, e)
                }}
            }() : t("tagName"), this.textNodeCache = t("data"), this.otherNodeCache = t("nodeName")
        }, getNodeWrapper:function (t) {
            var e;
            switch (t.nodeType) {
                case 1:
                    e = this.elementCache;
                    break;
                case 3:
                    e = this.textNodeCache;
                    break;
                default:
                    e = this.otherNodeCache
            }
            var n = e.get(t);
            return n || (n = new E(t, this), e.set(n)), n
        }, getPosition:function (t, e) {
            return this.getNodeWrapper(t).getPosition(e)
        }, getRangeBoundaryPosition:function (t, e) {
            var n = e ? "start" : "end";
            return this.getPosition(t[n + "Container"], t[n + "Offset"])
        }, detach:function () {
            this.elementCache = this.textNodeCache = this.otherNodeCache = null
        }}, e
    }();
    X(W, {nextNode:g, previousNode:m});
    var Te = Array.prototype.indexOf ? function (t, e) {
        return t.indexOf(e)
    } : function (t, e) {
        for (var n = 0, o = t.length; o > n; ++n)if (t[n] === e)return n;
        return-1
    };
    X(t.rangePrototype, {moveStart:M(!0, !1), moveEnd:M(!1, !1), move:M(!0, !0), trimStart:B(!0), trimEnd:B(!1), trim:L(function (t, e) {
        var n = this.trimStart(e), o = this.trimEnd(e);
        return n || o
    }), expand:L(function (t, e, n) {
        var a = !1;
        n = o(n, le);
        var s = i(n.characterOptions);
        if (e || (e = j), e == H) {
            var l, c, u = r(n.wordOptions), d = t.getRangeBoundaryPosition(this, !0), f = t.getRangeBoundaryPosition(this, !1), h = R(d, s, u), p = h.nextEndToken(), g = p.chars[0].previousVisible();
            if (this.collapsed)l = p; else {
                var m = R(f, s, u);
                l = m.previousStartToken()
            }
            return c = l.chars[l.chars.length - 1], g.equals(d) || (this.setStart(g.node, g.offset), a = !0), c && !c.equals(f) && (this.setEnd(c.node, c.offset), a = !0), n.trim && (n.trimStart && (a = this.trimStart(s) || a), n.trimEnd && (a = this.trimEnd(s) || a)), a
        }
        return this.moveEnd(j, 1, n)
    }), text:L(function (t, e) {
        return this.collapsed ? "" : D(t, this, i(e)).join("")
    }), selectCharacters:L(function (t, e, n, o, r) {
        var i = {characterOptions:r};
        e || (e = z(this.getDocument())), this.selectNodeContents(e), this.collapse(!0), this.moveStart("character", n, i), this.collapse(!0), this.moveEnd("character", o - n, i)
    }), toCharacterRange:L(function (t, e, n) {
        e || (e = z(this.getDocument()));
        var o, r, i = e.parentNode, a = W.getNodeIndex(e), s = -1 == W.comparePoints(this.startContainer, this.endContainer, i, a), l = this.cloneRange();
        return s ? (l.setStartAndEnd(this.startContainer, this.startOffset, i, a), o = -l.text(n).length) : (l.setStartAndEnd(i, a, this.startContainer, this.startOffset), o = l.text(n).length), r = o + this.text(n).length, {start:o, end:r}
    }), findText:L(function (e, n, i) {
        i = o(i, ae), i.wholeWordsOnly && (i.wordOptions = r(i.wordOptions), i.wordOptions.includeTrailingSpace = !1);
        var a = K(i.direction), s = i.withinRange;
        s || (s = t.createRange(), s.selectNodeContents(this.getDocument()));
        var l = n, c = !1;
        "string" == typeof l ? i.caseSensitive || (l = l.toLowerCase()) : c = !0;
        var u = e.getRangeBoundaryPosition(this, !a), d = s.comparePoint(u.node, u.offset);
        -1 === d ? u = e.getRangeBoundaryPosition(s, !0) : 1 === d && (u = e.getRangeBoundaryPosition(s, !1));
        for (var f, h = u, p = !1; ;)if (f = $(h, l, c, s, i)) {
            if (f.valid)return this.setStartAndEnd(f.startPos.node, f.startPos.offset, f.endPos.node, f.endPos.offset), !0;
            h = a ? f.startPos : f.endPos
        } else {
            if (!i.wrap || p)return!1;
            s = s.cloneRange(), h = e.getRangeBoundaryPosition(s, !a), s.setBoundary(u.node, u.offset, a), p = !0
        }
    }), pasteHtml:function (t) {
        if (this.deleteContents(), t) {
            var e = this.createContextualFragment(t), n = e.lastChild;
            this.insertNode(e), this.collapseAfter(n)
        }
    }}), X(t.selectionPrototype, {expand:L(function (t, e, n) {
        this.changeEachRange(function (t) {
            t.expand(e, n)
        })
    }), move:L(function (t, e, n, o) {
        var r = 0;
        if (this.focusNode) {
            this.collapse(this.focusNode, this.focusOffset);
            var i = this.getRangeAt(0);
            o || (o = {}), o.characterOptions = a(o.characterOptions), r = i.move(e, n, o), this.setSingleRange(i)
        }
        return r
    }), trimStart:F("trimStart"), trimEnd:F("trimEnd"), trim:F("trim"), selectCharacters:L(function (e, n, o, r, i, a) {
        var s = t.createRange(n);
        s.selectCharacters(n, o, r, a), this.setSingleRange(s, i)
    }), saveCharacterRanges:L(function (t, e, n) {
        for (var o = this.getAllRanges(), r = o.length, i = [], a = 1 == r && this.isBackward(), s = 0, l = o.length; l > s; ++s)i[s] = {characterRange:o[s].toCharacterRange(e, n), backward:a, characterOptions:n};
        return i
    }), restoreCharacterRanges:L(function (e, n, o) {
        this.removeAllRanges();
        for (var r, i, a, s = 0, l = o.length; l > s; ++s)i = o[s], a = i.characterRange, r = t.createRange(n), r.selectCharacters(n, a.start, a.end, i.characterOptions), this.addRange(r, i.backward)
    }), text:L(function (t, e) {
        for (var n = [], o = 0, r = this.rangeCount; r > o; ++o)n[o] = this.getRangeAt(o).text(e);
        return n.join("")
    })}), t.innerText = function (e, n) {
        var o = t.createRange(e);
        o.selectNodeContents(e);
        var r = o.text(n);
        return o.detach(), r
    }, t.createWordIterator = function (t, e, n) {
        var a = T();
        n = o(n, ce);
        var s = i(n.characterOptions), l = r(n.wordOptions), c = a.getPosition(t, e), u = R(c, s, l), d = K(n.direction);
        return{next:function () {
            return d ? u.previousStartToken() : u.nextEndToken()
        }, dispose:function () {
            u.dispose(), this.next = function () {
            }
        }}
    }, t.noMutation = function (t) {
        var e = T();
        t(e), N()
    }, t.noMutation.createEntryPointFunction = L, t.textRange = {isBlockNode:u, isCollapsedWhitespaceNode:y, createPosition:L(function (t, e, n) {
        return t.getPosition(e, n)
    })}
}), rangy.createModule("SaveRestore", ["WrappedRange"], function (t, e) {
    function n(t, e) {
        return(e || document).getElementById(t)
    }

    function o(t, e) {
        var n, o = "selectionBoundary_" + +new Date + "_" + ("" + Math.random()).slice(2), r = p.getDocument(t.startContainer), i = t.cloneRange();
        return i.collapse(e), n = r.createElement("span"), n.id = o, n.style.lineHeight = "0", n.style.display = "none", n.className = "rangySelectionBoundary", n.appendChild(r.createTextNode(g)), i.insertNode(n), i.detach(), n
    }

    function r(t, o, r, i) {
        var a = n(r, t);
        a ? (o[i ? "setStartBefore" : "setEndBefore"](a), a.parentNode.removeChild(a)) : e.warn("Marker element has been removed. Cannot restore selection.")
    }

    function i(t, e) {
        return e.compareBoundaryPoints(t.START_TO_START, t)
    }

    function a(e, n) {
        var r, i, a = t.DomRange.getRangeDocument(e), s = e.toString();
        return e.collapsed ? (i = o(e, !1), {document:a, markerId:i.id, collapsed:!0}) : (i = o(e, !1), r = o(e, !0), {document:a, startMarkerId:r.id, endMarkerId:i.id, collapsed:!1, backward:n, toString:function () {
            return"original text: '" + s + "', new text: '" + e.toString() + "'"
        }})
    }

    function s(o, i) {
        var a = o.document;
        "undefined" == typeof i && (i = !0);
        var s = t.createRange(a);
        if (o.collapsed) {
            var l = n(o.markerId, a);
            if (l) {
                l.style.display = "inline";
                var c = l.previousSibling;
                c && 3 == c.nodeType ? (l.parentNode.removeChild(l), s.collapseToPoint(c, c.length)) : (s.collapseBefore(l), l.parentNode.removeChild(l))
            } else e.warn("Marker element has been removed. Cannot restore selection.")
        } else r(a, s, o.startMarkerId, !0), r(a, s, o.endMarkerId, !1);
        return i && s.normalizeBoundaries(), s
    }

    function l(e, o) {
        var r, s, l = [];
        e = e.slice(0), e.sort(i);
        for (var c = 0, u = e.length; u > c; ++c)l[c] = a(e[c], o);
        for (c = u - 1; c >= 0; --c)r = e[c], s = t.DomRange.getRangeDocument(r), r.collapsed ? r.collapseAfter(n(l[c].markerId, s)) : (r.setEndBefore(n(l[c].endMarkerId, s)), r.setStartAfter(n(l[c].startMarkerId, s)));
        return l
    }

    function c(n) {
        if (!t.isSelectionValid(n))return e.warn("Cannot save selection. This usually happens when the selection is collapsed and the selection document has lost focus."), null;
        var o = t.getSelection(n), r = o.getAllRanges(), i = 1 == r.length && o.isBackward(), a = l(r, i);
        return i ? o.setSingleRange(r[0], "backward") : o.setRanges(r), {win:n, rangeInfos:a, restored:!1}
    }

    function u(t) {
        for (var e = [], n = t.length, o = n - 1; o >= 0; o--)e[o] = s(t[o], !0);
        return e
    }

    function d(e, n) {
        if (!e.restored) {
            var o = e.rangeInfos, r = t.getSelection(e.win), i = u(o), a = o.length;
            1 == a && n && t.features.selectionHasExtend && o[0].backward ? (r.removeAllRanges(), r.addRange(i[0], !0)) : r.setRanges(i), e.restored = !0
        }
    }

    function f(t, e) {
        var o = n(e, t);
        o && o.parentNode.removeChild(o)
    }

    function h(t) {
        for (var e, n = t.rangeInfos, o = 0, r = n.length; r > o; ++o)e = n[o], e.collapsed ? f(t.doc, e.markerId) : (f(t.doc, e.startMarkerId), f(t.doc, e.endMarkerId))
    }

    var p = t.dom, g = "ï»¿";
    t.util.extend(t, {saveRange:a, restoreRange:s, saveRanges:l, restoreRanges:u, saveSelection:c, restoreSelection:d, removeMarkerElement:f, removeMarkers:h})
}), function (t, e) {
    var n = "onprogress"in t.ajaxSettings.xhr();
    if (n) {
        var o = t.ajaxSettings.xhr;
        t.ajaxSettings.xhr = function () {
            var t = o();
            return t instanceof e.XMLHttpRequest && t.addEventListener("progress", this.progress, !1), t.upload && t.upload.addEventListener("progress", this.progress, !1), t
        }
    }
}(jQuery, window), function (t) {
    function e(t) {
        if (t in d.style)return t;
        var e = ["Moz", "Webkit", "O", "ms"], n = t.charAt(0).toUpperCase() + t.substr(1);
        if (t in d.style)return t;
        for (var o = 0; o < e.length; ++o) {
            var r = e[o] + n;
            if (r in d.style)return r
        }
    }

    function n() {
        return d.style[f.transform] = "", d.style[f.transform] = "rotateY(90deg)", "" !== d.style[f.transform]
    }

    function o(t) {
        return"string" == typeof t && this.parse(t), this
    }

    function r(t, e, n) {
        e === !0 ? t.queue(n) : e ? t.queue(e, n) : n()
    }

    function i(e) {
        var n = [];
        return t.each(e, function (e) {
            e = t.camelCase(e), e = t.transit.propertyMap[e] || t.cssProps[e] || e, e = l(e), f[e] && (e = l(f[e])), -1 === t.inArray(e, n) && n.push(e)
        }), n
    }

    function a(e, n, o, r) {
        var a = i(e);
        t.cssEase[o] && (o = t.cssEase[o]);
        var s = "" + u(n) + " " + o;
        parseInt(r, 10) > 0 && (s += " " + u(r));
        var l = [];
        return t.each(a, function (t, e) {
            l.push(e + " " + s)
        }), l.join(", ")
    }

    function s(e, n) {
        n || (t.cssNumber[e] = !0), t.transit.propertyMap[e] = f.transform, t.cssHooks[e] = {get:function (n) {
            var o = t(n).css("transit:transform");
            return o.get(e)
        }, set:function (n, o) {
            var r = t(n).css("transit:transform");
            r.setFromString(e, o), t(n).css({"transit:transform":r})
        }}
    }

    function l(t) {
        return t.replace(/([A-Z])/g, function (t) {
            return"-" + t.toLowerCase()
        })
    }

    function c(t, e) {
        return"string" != typeof t || t.match(/^[\-0-9\.]+$/) ? "" + t + e : t
    }

    function u(e) {
        var n = e;
        return"string" != typeof n || n.match(/^[\-0-9\.]+/) || (n = t.fx.speeds[n] || t.fx.speeds._default), c(n, "ms")
    }

    t.transit = {version:"0.9.9", propertyMap:{marginLeft:"margin", marginRight:"margin", marginBottom:"margin", marginTop:"margin", paddingLeft:"padding", paddingRight:"padding", paddingBottom:"padding", paddingTop:"padding"}, enabled:!0, useTransitionEnd:!1};
    var d = document.createElement("div"), f = {}, h = navigator.userAgent.toLowerCase().indexOf("chrome") > -1;
    f.transition = e("transition"), f.transitionDelay = e("transitionDelay"), f.transform = e("transform"), f.transformOrigin = e("transformOrigin"), f.filter = e("Filter"), f.transform3d = n();
    var p = {transition:"transitionEnd", MozTransition:"transitionend", OTransition:"oTransitionEnd", WebkitTransition:"webkitTransitionEnd", msTransition:"MSTransitionEnd"}, g = f.transitionEnd = p[f.transition] || null;
    for (var m in f)f.hasOwnProperty(m) && "undefined" == typeof t.support[m] && (t.support[m] = f[m]);
    d = null, t.cssEase = {_default:"ease", "in":"ease-in", out:"ease-out", "in-out":"ease-in-out", snap:"cubic-bezier(0,1,.5,1)", easeOutCubic:"cubic-bezier(.215,.61,.355,1)", easeInOutCubic:"cubic-bezier(.645,.045,.355,1)", easeInCirc:"cubic-bezier(.6,.04,.98,.335)", easeOutCirc:"cubic-bezier(.075,.82,.165,1)", easeInOutCirc:"cubic-bezier(.785,.135,.15,.86)", easeInExpo:"cubic-bezier(.95,.05,.795,.035)", easeOutExpo:"cubic-bezier(.19,1,.22,1)", easeInOutExpo:"cubic-bezier(1,0,0,1)", easeInQuad:"cubic-bezier(.55,.085,.68,.53)", easeOutQuad:"cubic-bezier(.25,.46,.45,.94)", easeInOutQuad:"cubic-bezier(.455,.03,.515,.955)", easeInQuart:"cubic-bezier(.895,.03,.685,.22)", easeOutQuart:"cubic-bezier(.165,.84,.44,1)", easeInOutQuart:"cubic-bezier(.77,0,.175,1)", easeInQuint:"cubic-bezier(.755,.05,.855,.06)", easeOutQuint:"cubic-bezier(.23,1,.32,1)", easeInOutQuint:"cubic-bezier(.86,0,.07,1)", easeInSine:"cubic-bezier(.47,0,.745,.715)", easeOutSine:"cubic-bezier(.39,.575,.565,1)", easeInOutSine:"cubic-bezier(.445,.05,.55,.95)", easeInBack:"cubic-bezier(.6,-.28,.735,.045)", easeOutBack:"cubic-bezier(.175, .885,.32,1.275)", easeInOutBack:"cubic-bezier(.68,-.55,.265,1.55)"}, t.cssHooks["transit:transform"] = {get:function (e) {
        return t(e).data("transform") || new o
    }, set:function (e, n) {
        var r = n;
        r instanceof o || (r = new o(r)), e.style[f.transform] = "WebkitTransform" !== f.transform || h ? r.toString() : r.toString(!0), t(e).data("transform", r)
    }}, t.cssHooks.transform = {set:t.cssHooks["transit:transform"].set}, t.cssHooks.filter = {get:function (t) {
        return t.style[f.filter]
    }, set:function (t, e) {
        t.style[f.filter] = e
    }}, t.fn.jquery < "1.8" && (t.cssHooks.transformOrigin = {get:function (t) {
        return t.style[f.transformOrigin]
    }, set:function (t, e) {
        t.style[f.transformOrigin] = e
    }}, t.cssHooks.transition = {get:function (t) {
        return t.style[f.transition]
    }, set:function (t, e) {
        t.style[f.transition] = e
    }}), s("scale"), s("translate"), s("rotate"), s("rotateX"), s("rotateY"), s("rotate3d"), s("perspective"), s("skewX"), s("skewY"), s("x", !0), s("y", !0), o.prototype = {setFromString:function (t, e) {
        var n = "string" == typeof e ? e.split(",") : e.constructor === Array ? e : [e];
        n.unshift(t), o.prototype.set.apply(this, n)
    }, set:function (t) {
        var e = Array.prototype.slice.apply(arguments, [1]);
        this.setter[t] ? this.setter[t].apply(this, e) : this[t] = e.join(",")
    }, get:function (t) {
        return this.getter[t] ? this.getter[t].apply(this) : this[t] || 0
    }, setter:{rotate:function (t) {
        this.rotate = c(t, "deg")
    }, rotateX:function (t) {
        this.rotateX = c(t, "deg")
    }, rotateY:function (t) {
        this.rotateY = c(t, "deg")
    }, scale:function (t, e) {
        void 0 === e && (e = t), this.scale = t + "," + e
    }, skewX:function (t) {
        this.skewX = c(t, "deg")
    }, skewY:function (t) {
        this.skewY = c(t, "deg")
    }, perspective:function (t) {
        this.perspective = c(t, "px")
    }, x:function (t) {
        this.set("translate", t, null)
    }, y:function (t) {
        this.set("translate", null, t)
    }, translate:function (t, e) {
        void 0 === this._translateX && (this._translateX = 0), void 0 === this._translateY && (this._translateY = 0), null !== t && void 0 !== t && (this._translateX = c(t, "px")), null !== e && void 0 !== e && (this._translateY = c(e, "px")), this.translate = this._translateX + "," + this._translateY
    }}, getter:{x:function () {
        return this._translateX || 0
    }, y:function () {
        return this._translateY || 0
    }, scale:function () {
        var t = (this.scale || "1,1").split(",");
        return t[0] && (t[0] = parseFloat(t[0])), t[1] && (t[1] = parseFloat(t[1])), t[0] === t[1] ? t[0] : t
    }, rotate3d:function () {
        for (var t = (this.rotate3d || "0,0,0,0deg").split(","), e = 0; 3 >= e; ++e)t[e] && (t[e] = parseFloat(t[e]));
        return t[3] && (t[3] = c(t[3], "deg")), t
    }}, parse:function (t) {
        var e = this;
        t.replace(/([a-zA-Z0-9]+)\((.*?)\)/g, function (t, n, o) {
            e.setFromString(n, o)
        })
    }, toString:function (t) {
        var e = [];
        for (var n in this)if (this.hasOwnProperty(n)) {
            if (!f.transform3d && ("rotateX" === n || "rotateY" === n || "perspective" === n || "transformOrigin" === n))continue;
            "_" !== n[0] && (t && "scale" === n ? e.push(n + "3d(" + this[n] + ",1)") : t && "translate" === n ? e.push(n + "3d(" + this[n] + ",0)") : e.push(n + "(" + this[n] + ")"))
        }
        return e.join(" ")
    }}, t.fn.transition = t.fn.transit = function (e, n, o, i) {
        var s = this, l = 0, c = !0, d = jQuery.extend(!0, {}, e);
        "function" == typeof n && (i = n, n = void 0), "object" == typeof n && (o = n.easing, l = n.delay || 0, c = n.queue || !0, i = n.complete, n = n.duration), "function" == typeof o && (i = o, o = void 0), "undefined" != typeof d.easing && (o = d.easing, delete d.easing), "undefined" != typeof d.duration && (n = d.duration, delete d.duration), "undefined" != typeof d.complete && (i = d.complete, delete d.complete), "undefined" != typeof d.queue && (c = d.queue, delete d.queue), "undefined" != typeof d.delay && (l = d.delay, delete d.delay), "undefined" == typeof n && (n = t.fx.speeds._default), "undefined" == typeof o && (o = t.cssEase._default), n = u(n);
        var h = a(d, n, o, l), p = t.transit.enabled && f.transition, m = p ? parseInt(n, 10) + parseInt(l, 10) : 0;
        if (0 === m) {
            var v = function (t) {
                s.css(d), i && i.apply(s), t && t()
            };
            return r(s, c, v), s
        }
        var y = {}, b = function (n) {
            var o = !1, r = function () {
                o && s.unbind(g, r), m > 0 && s.each(function () {
                    this.style[f.transition] = y[this] || null
                }), "function" == typeof i && i.apply(s), "function" == typeof n && n()
            };
            m > 0 && g && t.transit.useTransitionEnd ? (o = !0, s.bind(g, r)) : window.setTimeout(r, m), s.each(function () {
                m > 0 && (this.style[f.transition] = h), t(this).css(e)
            })
        }, w = function (t) {
            this.offsetWidth, b(t)
        };
        return r(s, c, w), this
    }, t.transit.getTransitionValue = a
}(jQuery), function (t) {
    t.fn.maxlength = function (e) {
        var n = t(this);
        return n.each(function () {
            e = t.extend({}, {counterContainer:!1, text:"%left characters left"}, e);
            var n = t(this), o = {options:e, field:n, counter:t('<div class="maxlength"></div>'), maxLength:parseInt(n.attr("maxlength"), 10), lastLength:null, updateCounter:function () {
                var e = this.field.val().length, n = this.options.text.replace(/\B%(length|maxlength|left)\b/g, t.proxy(function (t, n) {
                    return"length" == n ? e : "maxlength" == n ? this.maxLength : this.maxLength - e
                }, this));
                this.counter.html(n), e != this.lastLength && this.updateLength(e)
            }, updateLength:function (t) {
                this.field.trigger("update.maxlength", [this.field, this.lastLength, t, this.maxLength, this.maxLength - t]), this.lastLength = t
            }};
            o.maxLength && (o.field.data("maxlength", o).bind({"keyup change":function () {
                t(this).data("maxlength").updateCounter()
            }, "cut paste drop":function () {
                setTimeout(t.proxy(function () {
                    t(this).data("maxlength").updateCounter()
                }, this), 1)
            }}), e.counterContainer ? e.counterContainer.append(o.counter) : o.field.after(o.counter), o.updateCounter())
        }), n
    }
}(jQuery), function (t) {
    function e(e) {
        return t(e.target).addClass("dragOver"), e.stopPropagation(), e.preventDefault(), !1
    }

    function n(t) {
        return t.originalEvent.dataTransfer.dropEffect = "copy", t.stopPropagation(), t.preventDefault(), !1
    }

    function o(e) {
        return t(e.target).removeClass("dragOver"), e.stopPropagation(), e.preventDefault(), !1
    }

    t.fn.dropArea = function () {
        return this.bind("dragenter", e).bind("dragover", n).bind("dragleave", o), this
    }
}(jQuery), function (t, e) {
    function n(t, e) {
        return"function" == typeof t ? t.call(e) : t
    }

    function o(t) {
        for (; t = t.parentNode;)if (t == document)return!0;
        return!1
    }

    function r(t) {
        return"object" == typeof HTMLElement ? t instanceof HTMLElement : t && "object" == typeof t && 1 === t.nodeType && "string" == typeof t.nodeName
    }

    function i() {
        return"tipsyuid" + s++
    }

    function a(e, n) {
        this.$element = t(e), this.options = n, this.enabled = !0, this.fixTitle()
    }

    var s = 0;
    a.prototype = {show:function () {
        if (o(this.$element[0]) && (!r(this.$element) || this.$element.is(":visible"))) {
            var e;
            if (this.enabled && (e = this.getTitle())) {
                var a = this.tip();
                a.find(".tipsy-inner" + this.options.theme)[this.options.html ? "html" : "text"](e), a[0].className = "tipsy" + this.options.theme, this.options.className && a.addClass(n(this.options.className, this.$element[0])), a.remove().css({top:0, left:0, visibility:"hidden", display:"block"}).prependTo(document.body);
                var s = t.extend({}, this.$element.offset());
                s = this.$element.parents("svg").size() > 0 ? t.extend(s, this.$element[0].getBBox()) : t.extend(s, {width:this.$element[0].offsetWidth || 0, height:this.$element[0].offsetHeight || 0});
                var l, c = a[0].offsetWidth, u = a[0].offsetHeight, d = n(this.options.gravity, this.$element[0]);
                switch (d.charAt(0)) {
                    case"n":
                        l = {top:s.top + s.height + this.options.offset, left:s.left + s.width / 2 - c / 2};
                        break;
                    case"s":
                        l = {top:s.top - u - this.options.offset, left:s.left + s.width / 2 - c / 2};
                        break;
                    case"e":
                        l = {top:s.top + s.height / 2 - u / 2, left:s.left - c - this.options.offset};
                        break;
                    case"w":
                        l = {top:s.top + s.height / 2 - u / 2, left:s.left + s.width + this.options.offset}
                }
                if (2 == d.length && (l.left = "w" == d.charAt(1) ? s.left + s.width / 2 - 15 : s.left + s.width / 2 - c + 15), a.css(l).addClass("tipsy-" + d + this.options.theme), a.find(".tipsy-arrow" + this.options.theme)[0].className = "tipsy-arrow" + this.options.theme + " tipsy-arrow-" + d.charAt(0) + this.options.theme, a.css({width:c - 10 + "px"}), this.options.fade ? (this.options.shadow && t(".tipsy-inner").css({"box-shadow":"0px 0px " + this.options.shadowBlur + "px " + this.options.shadowSpread + "px rgba(0, 0, 0, " + this.options.shadowOpacity + ")", "-webkit-box-shadow":"0px 0px " + this.options.shadowBlur + "px " + this.options.shadowSpread + "px rgba(0, 0, 0, " + this.options.shadowOpacity + ")"}), a.stop().css({opacity:0, display:"block", visibility:"visible"}).animate({opacity:this.options.opacity}, this.options.fadeInTime)) : a.css({visibility:"visible", opacity:this.options.opacity}), this.options.aria) {
                    var f = i();
                    a.attr("id", f), this.$element.attr("aria-describedby", f)
                }
            }
        }
    }, hide:function () {
        this.options.fade ? this.tip().stop().fadeOut(this.options.fadeOutTime, function () {
            t(this).remove()
        }) : this.tip().remove(), this.options.aria && this.$element.removeAttr("aria-describedby")
    }, fixTitle:function () {
        var t = this.$element;
        (t.attr("title") || "string" != typeof t.attr("original-title")) && t.attr("original-title", t.attr("title") || "").removeAttr("title")
    }, getTitle:function () {
        var t, e = this.$element, n = this.options;
        return this.fixTitle(), "string" == typeof n.title ? t = e.attr("title" == n.title ? "original-title" : n.title) : "function" == typeof n.title && (t = n.title.call(e[0])), t = ("" + t).replace(/(^\s*|\s*$)/, ""), t || n.fallback
    }, tip:function () {
        return this.$tip || (this.$tip = t('<div class="tipsy' + this.options.theme + '"></div>').html('<div class="tipsy-arrow' + this.options.theme + '"></div><div class="tipsy-inner' + this.options.theme + '"></div>').attr("role", "tooltip"), this.$tip.data("tipsy-pointee", this.$element[0])), this.$tip
    }, validate:function () {
        this.$element[0].parentNode || (this.hide(), this.$element = null, this.options = null)
    }, enable:function () {
        this.enabled = !0
    }, disable:function () {
        this.enabled = !1
    }, toggleEnabled:function () {
        this.enabled = !this.enabled
    }}, t.fn.tipsy = function (e) {
        function n(n) {
            var o = t.data(n, "tipsy");
            return o || (o = new a(n, t.fn.tipsy.elementOptions(n, e)), t.data(n, "tipsy", o)), o
        }

        function r() {
            if (t.fn.tipsy.enabled === !0) {
                var r = n(this);
                r.hoverState = "in", 0 === e.delayIn ? r.show() : (r.fixTitle(), setTimeout(function () {
                    "in" == r.hoverState && o(r.$element) && r.show()
                }, e.delayIn))
            }
        }

        function i() {
            var t = n(this);
            t.hoverState = "out", 0 === e.delayOut ? t.hide() : setTimeout(function () {
                "out" != t.hoverState && t.$element && t.$element.is(":visible") || t.hide()
            }, e.delayOut)
        }

        if (t.fn.tipsy.enable(), e === !0)return this.data("tipsy");
        if ("string" == typeof e) {
            var s = this.data("tipsy");
            return s && s[e](), this
        }
        if (e = t.extend({}, t.fn.tipsy.defaults, e), e.theme = e.theme && "" !== e.theme ? "-" + e.theme : "", e.live || this.each(function () {
            n(this)
        }), "manual" != e.trigger) {
            var l = "hover" == e.trigger ? "mouseenter mouseover" : "focus", c = "hover" == e.trigger ? "mouseleave mouseout" : "blur";
            if (e.live && e.live !== !0)t(this).on(l, e.live, r), t(this).on(c, e.live, i); else {
                if (e.live && !t.live)throw"Since jQuery 1.9, pass selector as live argument. eg. $(document).tipsy({live: 'a.live'});";
                var u = e.live ? "live" : "bind";
                this[u](l, r)[u](c, i)
            }
        }
        return this
    }, t.fn.tipsy.defaults = {aria:!1, className:null, delayIn:0, delayOut:0, fade:!1, fadeInTime:400, fadeOutTime:400, shadow:!1, shadowBlur:8, shadowOpacity:1, shadowSpread:0, fallback:"", gravity:"n", html:!1, live:!1, offset:0, opacity:.8, title:"title", trigger:"hover", theme:""}, t.fn.tipsy.revalidate = function () {
        t(".tipsy").each(function () {
            var e = t.data(this, "tipsy-pointee");
            e && o(e) || t(this).remove()
        })
    }, t.fn.tipsy.enable = function () {
        t.fn.tipsy.enabled = !0
    }, t.fn.tipsy.disable = function () {
        t.fn.tipsy.enabled = !1
    }, t.fn.tipsy.elementOptions = function (e, n) {
        return t.metadata ? t.extend({}, n, t(e).metadata()) : n
    }, t.fn.tipsy.autoNS = function () {
        return t(this).offset().top > t(document).scrollTop() + t(e).height() / 2 ? "s" : "n"
    }, t.fn.tipsy.autoWE = function () {
        return t(this).offset().left > t(document).scrollLeft() + t(e).width() / 2 ? "e" : "w"
    }, t.fn.tipsy.autoNWNE = function () {
        return t(this).offset().left > t(document).scrollLeft() + t(e).width() / 2 ? "ne" : "nw"
    }, t.fn.tipsy.autoSWSE = function () {
        return t(this).offset().left > t(document).scrollLeft() + t(e).width() / 2 ? "se" : "sw"
    }, t.fn.tipsy.autoBounds = function (n, o, r) {
        return function () {
            var i = {ns:r[0], ew:r.length > 1 ? r[1] : !1}, a = t(document).scrollTop() + n, s = t(document).scrollLeft() + o, l = t(this);
            return l.offset().top < a && (i.ns = "n"), l.offset().left < s && (i.ew = "w"), t(e).width() + t(document).scrollLeft() - l.offset().left < o && (i.ew = "e"), t(e).height() + t(document).scrollTop() - l.offset().top < n && (i.ns = "s"), i.ns + (i.ew ? i.ew : "")
        }
    }, t.fn.tipsy.autoBounds2 = function (n, o) {
        return function () {
            var r = {}, i = t(document).scrollTop() + n, a = t(document).scrollLeft() + n, s = t(this);
            return o.length > 1 ? (r.ns = o[0], r.ew = o[1]) : "e" == o[0] || "w" == o[0] ? r.ew = o[0] : r.ns = o[0], s.offset().top < i && (r.ns = "n"), s.offset().left < a && (r.ew = "w"), t(e).width() + t(document).scrollLeft() - (s.offset().left + s.width()) < n && (r.ew = "e"), t(e).height() + t(document).scrollTop() - (s.offset().top + s.height()) < n && (r.ns = "s"), r.ns ? r.ns + (r.ew ? r.ew : "") : r.ew
        }
    }
}(jQuery, window), function (t) {
    "function" == typeof define && define.amd ? define(["jquery"], t) : t(jQuery)
}(function (t) {
    function e(e) {
        return t.isFunction(e) || "object" == typeof e ? e : {top:e, left:e}
    }

    var n = t.scrollTo = function (e, n, o) {
        return t(window).scrollTo(e, n, o)
    };
    return n.defaults = {axis:"xy", duration:parseFloat(t.fn.jquery) >= 1.3 ? 0 : 1, limit:!0}, n.window = function () {
        return t(window)._scrollable()
    }, t.fn._scrollable = function () {
        return this.map(function () {
            var e = this, n = !e.nodeName || -1 != t.inArray(e.nodeName.toLowerCase(), ["iframe", "#document", "html", "body"]);
            if (!n)return e;
            var o = (e.contentWindow || e).document || e.ownerDocument || e;
            return/webkit/i.test(navigator.userAgent) || "BackCompat" == o.compatMode ? o.body : o.documentElement
        })
    }, t.fn.scrollTo = function (o, r, i) {
        return"object" == typeof r && (i = r, r = 0), "function" == typeof i && (i = {onAfter:i}), "max" == o && (o = 9e9), i = t.extend({}, n.defaults, i), r = r || i.duration, i.queue = i.queue && i.axis.length > 1, i.queue && (r /= 2), i.offset = e(i.offset), i.over = e(i.over), this._scrollable().each(function () {
            function a(t) {
                c.animate(d, r, i.easing, t && function () {
                    t.call(this, u, i)
                })
            }

            if (null != o) {
                var s, l = this, c = t(l), u = o, d = {}, f = c.is("html,body");
                switch (typeof u) {
                    case"number":
                    case"string":
                        if (/^([+-]=?)?\d+(\.\d+)?(px|%)?$/.test(u)) {
                            u = e(u);
                            break
                        }
                        if (u = t(u, this), !u.length)return;
                    case"object":
                        (u.is || u.style) && (s = (u = t(u)).offset())
                }
                var h = t.isFunction(i.offset) && i.offset(l, u) || i.offset;
                t.each(i.axis.split(""), function (t, e) {
                    var o = "x" == e ? "Left" : "Top", r = o.toLowerCase(), p = "scroll" + o, g = l[p], m = n.max(l, e);
                    if (s)d[p] = s[r] + (f ? 0 : g - c.offset()[r]), i.margin && (d[p] -= parseInt(u.css("margin" + o)) || 0, d[p] -= parseInt(u.css("border" + o + "Width")) || 0), d[p] += h[r] || 0, i.over[r] && (d[p] += u["x" == e ? "width" : "height"]() * i.over[r]); else {
                        var v = u[r];
                        d[p] = v.slice && "%" == v.slice(-1) ? parseFloat(v) / 100 * m : v
                    }
                    i.limit && /^\d+$/.test(d[p]) && (d[p] = d[p] <= 0 ? 0 : Math.min(d[p], m)), !t && i.queue && (g != d[p] && a(i.onAfterFirst), delete d[p])
                }), a(i.onAfter)
            }
        }).end()
    }, n.max = function (e, n) {
        var o = "x" == n ? "Width" : "Height", r = "scroll" + o;
        if (!t(e).is("html,body"))return e[r] - t(e)[o.toLowerCase()]();
        var i = "client" + o, a = e.ownerDocument.documentElement, s = e.ownerDocument.body;
        return Math.max(a[r], s[r]) - Math.min(a[i], s[i])
    }, n
});
var Plugins;
!function (t) {
    var e = function () {
        function t(t, e) {
            var n = this;
            this._input = $(t), this._options = e, this._mirror = $('<span style="position:absolute; top:-999px; left:0; white-space:pre;"/>'), $.each(["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform", "wordSpacing", "textIndent"], function (t, e) {
                n._mirror[0].style[e] = n._input.css(e)
            }), $("body").append(this._mirror), this._input.on("keydown keyup input propertychange change", function () {
                n.update()
            }), function () {
                n.update()
            }()
        }

        return Object.defineProperty(t.prototype, "options", {get:function () {
            return this._options
        }, enumerable:!0, configurable:!0}), Object.defineProperty(t, "instanceKey", {get:function () {
            return"autosizeInputInstance"
        }, enumerable:!0, configurable:!0}), t.prototype.update = function () {
            var t = this._input.val();
            if (t || (t = this._input.attr("placeholder") || ""), t !== this._mirror.text()) {
                this._mirror.text(t);
                var e = this._mirror.width() + this._options.space;
                this._input.width(e)
            }
        }, t
    }();
    t.AutosizeInput = e;
    var n = function () {
        function t(t) {
            "undefined" == typeof t && (t = 30), this._space = t
        }

        return Object.defineProperty(t.prototype, "space", {get:function () {
            return this._space
        }, set:function (t) {
            this._space = t
        }, enumerable:!0, configurable:!0}), t
    }();
    t.AutosizeInputOptions = n, function (e) {
        var o = "autosize-input", r = ["text", "password", "search", "url", "tel", "email"];
        e.fn.autosizeInput = function () {
            return this.each(function () {
                if ("INPUT" == this.tagName && e.inArray(this.type, r) > -1) {
                    var i = e(this);
                    if (!i.data(t.AutosizeInput.instanceKey)) {
                        if (void 0 == a) {
                            var a = i.data(o);
                            a && "object" == typeof a || (a = new n)
                        }
                        i.data(t.AutosizeInput.instanceKey, new t.AutosizeInput(this, a))
                    }
                }
            })
        }, e(function () {
            e("input[data-" + o + "]").autosizeInput()
        })
    }(jQuery)
}(Plugins || (Plugins = {})), function (t) {
    var e = {selector:"> *", visible:{opacity:1, transform:"scale(1)"}, hidden:{opacity:0, transform:"scale(0.001)"}}, n = t.expando + "mm", o = function () {
        var e = [];
        return this.parent().css({position:"relative"}), this.each(function () {
            e.push(t(this).position())
        }), this.each(function (n) {
            t(this).css(t.extend({position:"absolute"}, e[n]))
        }), this
    }, r = function () {
        return this.css({position:"", top:"", left:""})
    }, i = function (e, n) {
        var o = {easing:"linear", duration:400};
        n = t.extend({}, o, n);
        var r = ["all", n.duration + "ms", n.easing].join(" ");
        e = t.extend({transition:r}, e);
        var i = function () {
            t(this).css("transition", ""), t(this).dequeue()
        };
        return this.queue(function () {
            setTimeout(t.proxy(i, this), n.duration), t(this).css(e)
        })
    }, a = function () {
        return this.each(function () {
            this.offsetTop
        })
    }, s = function (t, e) {
        return t.filter(function () {
            var t = this;
            return!e.filter(function () {
                return this[n] == t[n]
            })[0]
        })
    }, l = function (t, e) {
        return t.filter(function () {
            var t = this;
            return e.filter(function () {
                return this[n] == t[n]
            })[0]
        })
    }, c = function (c, u) {
        "function" == typeof c && (u = c, c = {}), c = t.extend({}, e, c);
        var d = [], f = t(this), h = f.find(c.selector), p = f.clone(!0, !0), g = p.find(c.selector);
        h.each(function (t) {
            this[n] = g[t][n] = t
        }), p.css({position:"absolute", left:"-1000px", top:"-1000px"}).appendTo("body"), u.call(p, p, c), $tNodes = p.find(c.selector);
        var m = s($tNodes, h), v = s(h, $tNodes), y = l($tNodes, h);
        o.call(h), y.each(function () {
            var e = t(h[this[n]]), o = t(this), r = o.position();
            o.is(":visible") && e.is(":hidden") ? t.extend(r, c.visible) : o.is(":hidden") && e.is(":visible") && (t.extend(r, c.hidden), e.promise().done(function () {
                e.hide()
            })), i.call(e, r, c), d.push(e.promise())
        }), m.each(function () {
            var e = t(this), n = h.eq(e.index()), r = o.call(e).clone();
            r.css(c.hidden), r.insertBefore(n), a.call(r), i.call(r, c.visible, c), d.push(r.promise())
        }), v.each(function () {
            var e = t(h[this[n]]);
            i.call(e, c.hidden, c), e.promise().done(function () {
                e.remove()
            }), d.push(e.promise())
        }), p.remove();
        var b = t.when.apply(t, d);
        return b.done(function () {
            r.call(f.find(c.selector))
        })
    };
    t.fn.magicMove = function (t, e) {
        var n = this;
        return n.queue(function () {
            var o = c.call(n, t, e);
            o.done(function () {
                n.dequeue()
            })
        })
    }
}(jQuery);
var BinaryFile = function (t, e, n) {
    var o = t, r = e || 0, i = 0;
    this.getRawData = function () {
        return o
    }, "string" == typeof t ? (i = n || o.length, this.getByteAt = function (t) {
        return 255 & o.charCodeAt(t + r)
    }, this.getBytesAt = function (t, e) {
        for (var n = [], i = 0; e > i; i++)n[i] = 255 & o.charCodeAt(t + i + r);
        return n
    }) : "unknown" == typeof t && (i = n || IEBinary_getLength(o), this.getByteAt = function (t) {
        return IEBinary_getByteAt(o, t + r)
    }, this.getBytesAt = function (t, e) {
        return new VBArray(IEBinary_getBytesAt(o, t + r, e)).toArray()
    }), this.getLength = function () {
        return i
    }, this.getSByteAt = function (t) {
        var e = this.getByteAt(t);
        return e > 127 ? e - 256 : e
    }, this.getShortAt = function (t, e) {
        var n = e ? (this.getByteAt(t) << 8) + this.getByteAt(t + 1) : (this.getByteAt(t + 1) << 8) + this.getByteAt(t);
        return 0 > n && (n += 65536), n
    }, this.getSShortAt = function (t, e) {
        var n = this.getShortAt(t, e);
        return n > 32767 ? n - 65536 : n
    }, this.getLongAt = function (t, e) {
        var n = this.getByteAt(t), o = this.getByteAt(t + 1), r = this.getByteAt(t + 2), i = this.getByteAt(t + 3), a = e ? (((n << 8) + o << 8) + r << 8) + i : (((i << 8) + r << 8) + o << 8) + n;
        return 0 > a && (a += 4294967296), a
    }, this.getSLongAt = function (t, e) {
        var n = this.getLongAt(t, e);
        return n > 2147483647 ? n - 4294967296 : n
    }, this.getStringAt = function (t, e) {
        for (var n = [], o = this.getBytesAt(t, e), r = 0; e > r; r++)n[r] = String.fromCharCode(o[r]);
        return n.join("")
    }, this.getCharAt = function (t) {
        return String.fromCharCode(this.getByteAt(t))
    }, this.toBase64 = function () {
        return window.btoa(o)
    }, this.fromBase64 = function (t) {
        o = window.atob(t)
    }
}, BinaryAjax = function () {
    function t() {
        var t = null;
        return window.ActiveXObject ? t = new ActiveXObject("Microsoft.XMLHTTP") : window.XMLHttpRequest && (t = new XMLHttpRequest), t
    }

    function e(e, n, o) {
        var r = t();
        r ? (n && ("undefined" != typeof r.onload ? r.onload = function () {
            "200" == r.status ? n(this) : o && o(), r = null
        } : r.onreadystatechange = function () {
            4 == r.readyState && ("200" == r.status ? n(this) : o && o(), r = null)
        }), r.open("HEAD", e, !0), r.send(null)) : o && o()
    }

    function n(e, n, o, r, i, a) {
        var s = t();
        if (s) {
            var l = 0;
            r && !i && (l = r[0]);
            var c = 0;
            r && (c = r[1] - r[0] + 1), n && ("undefined" != typeof s.onload ? s.onload = function () {
                "200" == s.status || "206" == s.status || "0" == s.status ? (s.binaryResponse = new BinaryFile(s.responseText, l, c), s.fileSize = a || s.getResponseHeader("Content-Length"), n(s)) : o && o(), s = null
            } : s.onreadystatechange = function () {
                if (4 == s.readyState) {
                    if ("200" == s.status || "206" == s.status || "0" == s.status) {
                        var t = {status:s.status, binaryResponse:new BinaryFile("unknown" == typeof s.responseBody ? s.responseBody : s.responseText, l, c), fileSize:a || s.getResponseHeader("Content-Length")};
                        n(t)
                    } else o && o();
                    s = null
                }
            }), s.open("GET", e, !0), s.overrideMimeType && s.overrideMimeType("text/plain; charset=x-user-defined"), r && i && s.setRequestHeader("Range", "bytes=" + r[0] + "-" + r[1]), s.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 1970 00:00:00 GMT"), s.send(null)
        } else o && o()
    }

    return function (t, o, r, i) {
        i ? e(t, function (e) {
            var a, s, l = parseInt(e.getResponseHeader("Content-Length"), 10), c = e.getResponseHeader("Accept-Ranges");
            a = i[0], i[0] < 0 && (a += l), s = a + i[1] - 1, n(t, o, r, [a, s], "bytes" == c, l)
        }) : n(t, o, r)
    }
}();
document.write("<script type='text/vbscript'>\r\nFunction IEBinary_getByteAt(strBinary, iOffset)\r\n	IEBinary_getByteAt = AscB(MidB(strBinary, iOffset + 1, 1))\r\nEnd Function\r\nFunction IEBinary_getBytesAt(strBinary, iOffset, iLength)\r\n  Dim aBytes()\r\n  ReDim aBytes(iLength - 1)\r\n  For i = 0 To iLength - 1\r\n   aBytes(i) = IEBinary_getByteAt(strBinary, iOffset + i)\r\n  Next\r\n  IEBinary_getBytesAt = aBytes\r\nEnd Function\r\nFunction IEBinary_getLength(strBinary)\r\n	IEBinary_getLength = LenB(strBinary)\r\nEnd Function\r\n</script>\r\n");
var EXIF = function () {
    function t(t) {
        return!!t.exifdata
    }

    function e(t, e) {
        function o(o) {
            var r = n(o);
            t.exifdata = r || {}, e && e.call(t)
        }

        if (t instanceof Image)BinaryAjax(t.src, function (t) {
            o(t.binaryResponse)
        }); else if (window.FileReader && t instanceof window.File) {
            var r = new FileReader;
            r.onload = function (t) {
                o(new BinaryFile(t.target.result))
            }, r.readAsBinaryString(t)
        }
    }

    function n(t) {
        if (255 != t.getByteAt(0) || 216 != t.getByteAt(1))return!1;
        for (var e, n = 2, o = t.getLength(); o > n;) {
            if (255 != t.getByteAt(n))return d && console.log("Not a valid marker at offset " + n + ", found: " + t.getByteAt(n)), !1;
            if (e = t.getByteAt(n + 1), 22400 == e)return d && console.log("Found 0xFFE1 marker"), i(t, n + 4, t.getShortAt(n + 2, !0) - 2);
            if (225 == e)return d && console.log("Found 0xFFE1 marker"), i(t, n + 4, t.getShortAt(n + 2, !0) - 2);
            n += 2 + t.getShortAt(n + 2, !0)
        }
    }

    function o(t, e, n, o, i) {
        var a, s, l, c = t.getShortAt(n, i), u = {};
        for (l = 0; c > l; l++)a = n + 12 * l + 2, s = o[t.getShortAt(a, i)], !s && d && console.log("Unknown tag: " + t.getShortAt(a, i)), u[s] = r(t, a, e, n, i);
        return u
    }

    function r(t, e, n, o, r) {
        var i, a, s, l, c, u, d = t.getShortAt(e + 2, r), f = t.getLongAt(e + 4, r), h = t.getLongAt(e + 8, r) + n;
        switch (d) {
            case 1:
            case 7:
                if (1 == f)return t.getByteAt(e + 8, r);
                for (i = f > 4 ? h : e + 8, a = [], l = 0; f > l; l++)a[l] = t.getByteAt(i + l);
                return a;
            case 2:
                return i = f > 4 ? h : e + 8, t.getStringAt(i, f - 1);
            case 3:
                if (1 == f)return t.getShortAt(e + 8, r);
                for (i = f > 2 ? h : e + 8, a = [], l = 0; f > l; l++)a[l] = t.getShortAt(i + 2 * l, r);
                return a;
            case 4:
                if (1 == f)return t.getLongAt(e + 8, r);
                a = [];
                for (var l = 0; f > l; l++)a[l] = t.getLongAt(h + 4 * l, r);
                return a;
            case 5:
                if (1 == f)return c = t.getLongAt(h, r), u = t.getLongAt(h + 4, r), s = new Number(c / u), s.numerator = c, s.denominator = u, s;
                for (a = [], l = 0; f > l; l++)c = t.getLongAt(h + 8 * l, r), u = t.getLongAt(h + 4 + 8 * l, r), a[l] = new Number(c / u), a[l].numerator = c, a[l].denominator = u;
                return a;
            case 9:
                if (1 == f)return t.getSLongAt(e + 8, r);
                for (a = [], l = 0; f > l; l++)a[l] = t.getSLongAt(h + 4 * l, r);
                return a;
            case 10:
                if (1 == f)return t.getSLongAt(h, r) / t.getSLongAt(h + 4, r);
                for (a = [], l = 0; f > l; l++)a[l] = t.getSLongAt(h + 8 * l, r) / t.getSLongAt(h + 4 + 8 * l, r);
                return a
        }
    }

    function i(t, e) {
        if ("Exif" != t.getStringAt(e, 4))return d && console.log("Not valid EXIF data! " + t.getStringAt(e, 4)), !1;
        var n, r, i, a, s, l = e + 6;
        if (18761 == t.getShortAt(l))n = !1; else {
            if (19789 != t.getShortAt(l))return d && console.log("Not valid TIFF data! (no 0x4949 or 0x4D4D)"), !1;
            n = !0
        }
        if (42 != t.getShortAt(l + 2, n))return d && console.log("Not valid TIFF data! (no 0x002A)"), !1;
        if (8 != t.getLongAt(l + 4, n))return d && console.log("Not valid TIFF data! (First offset not 8)", t.getShortAt(l + 4, n)), !1;
        if (r = o(t, l, l + 8, h, n), r.ExifIFDPointer) {
            a = o(t, l, l + r.ExifIFDPointer, f, n);
            for (i in a) {
                switch (i) {
                    case"LightSource":
                    case"Flash":
                    case"MeteringMode":
                    case"ExposureProgram":
                    case"SensingMethod":
                    case"SceneCaptureType":
                    case"SceneType":
                    case"CustomRendered":
                    case"WhiteBalance":
                    case"GainControl":
                    case"Contrast":
                    case"Saturation":
                    case"Sharpness":
                    case"SubjectDistanceRange":
                    case"FileSource":
                        a[i] = g[i][a[i]];
                        break;
                    case"ExifVersion":
                    case"FlashpixVersion":
                        a[i] = String.fromCharCode(a[i][0], a[i][1], a[i][2], a[i][3]);
                        break;
                    case"ComponentsConfiguration":
                        a[i] = g.Components[a[i][0]] + g.Components[a[i][1]] + g.Components[a[i][2]] + g.Components[a[i][3]]
                }
                r[i] = a[i]
            }
        }
        if (r.GPSInfoIFDPointer) {
            s = o(t, l, l + r.GPSInfoIFDPointer, p, n);
            for (i in s) {
                switch (i) {
                    case"GPSVersionID":
                        s[i] = s[i][0] + "." + s[i][1] + "." + s[i][2] + "." + s[i][3]
                }
                r[i] = s[i]
            }
        }
        return r
    }

    function a(n, o) {
        return n instanceof Image && !n.complete ? !1 : (t(n) ? o && o.call(n) : e(n, o), !0)
    }

    function s(e, n) {
        return t(e) ? e.exifdata[n] : void 0
    }

    function l(e) {
        if (!t(e))return{};
        var n, o = e.exifdata, r = {};
        for (n in o)o.hasOwnProperty(n) && (r[n] = o[n]);
        return r
    }

    function c(e) {
        if (!t(e))return"";
        var n, o = e.exifdata, r = "";
        for (n in o)o.hasOwnProperty(n) && (r += "object" == typeof o[n] ? o[n]instanceof Number ? n + " : " + o[n] + " [" + o[n].numerator + "/" + o[n].denominator + "]\r\n" : n + " : [" + o[n].length + " values]\r\n" : n + " : " + o[n] + "\r\n");
        return r
    }

    function u(t) {
        return n(t)
    }

    var d = !1, f = {36864:"ExifVersion", 40960:"FlashpixVersion", 40961:"ColorSpace", 40962:"PixelXDimension", 40963:"PixelYDimension", 37121:"ComponentsConfiguration", 37122:"CompressedBitsPerPixel", 37500:"MakerNote", 37510:"UserComment", 40964:"RelatedSoundFile", 36867:"DateTimeOriginal", 36868:"DateTimeDigitized", 37520:"SubsecTime", 37521:"SubsecTimeOriginal", 37522:"SubsecTimeDigitized", 33434:"ExposureTime", 33437:"FNumber", 34850:"ExposureProgram", 34852:"SpectralSensitivity", 34855:"ISOSpeedRatings", 34856:"OECF", 37377:"ShutterSpeedValue", 37378:"ApertureValue", 37379:"BrightnessValue", 37380:"ExposureBias", 37381:"MaxApertureValue", 37382:"SubjectDistance", 37383:"MeteringMode", 37384:"LightSource", 37385:"Flash", 37396:"SubjectArea", 37386:"FocalLength", 41483:"FlashEnergy", 41484:"SpatialFrequencyResponse", 41486:"FocalPlaneXResolution", 41487:"FocalPlaneYResolution", 41488:"FocalPlaneResolutionUnit", 41492:"SubjectLocation", 41493:"ExposureIndex", 41495:"SensingMethod", 41728:"FileSource", 41729:"SceneType", 41730:"CFAPattern", 41985:"CustomRendered", 41986:"ExposureMode", 41987:"WhiteBalance", 41988:"DigitalZoomRation", 41989:"FocalLengthIn35mmFilm", 41990:"SceneCaptureType", 41991:"GainControl", 41992:"Contrast", 41993:"Saturation", 41994:"Sharpness", 41995:"DeviceSettingDescription", 41996:"SubjectDistanceRange", 40965:"InteroperabilityIFDPointer", 42016:"ImageUniqueID"}, h = {256:"ImageWidth", 257:"ImageHeight", 34665:"ExifIFDPointer", 34853:"GPSInfoIFDPointer", 40965:"InteroperabilityIFDPointer", 258:"BitsPerSample", 259:"Compression", 262:"PhotometricInterpretation", 274:"Orientation", 277:"SamplesPerPixel", 284:"PlanarConfiguration", 530:"YCbCrSubSampling", 531:"YCbCrPositioning", 282:"XResolution", 283:"YResolution", 296:"ResolutionUnit", 273:"StripOffsets", 278:"RowsPerStrip", 279:"StripByteCounts", 513:"JPEGInterchangeFormat", 514:"JPEGInterchangeFormatLength", 301:"TransferFunction", 318:"WhitePoint", 319:"PrimaryChromaticities", 529:"YCbCrCoefficients", 532:"ReferenceBlackWhite", 306:"DateTime", 270:"ImageDescription", 271:"Make", 272:"Model", 305:"Software", 315:"Artist", 33432:"Copyright"}, p = {0:"GPSVersionID", 1:"GPSLatitudeRef", 2:"GPSLatitude", 3:"GPSLongitudeRef", 4:"GPSLongitude", 5:"GPSAltitudeRef", 6:"GPSAltitude", 7:"GPSTimeStamp", 8:"GPSSatellites", 9:"GPSStatus", 10:"GPSMeasureMode", 11:"GPSDOP", 12:"GPSSpeedRef", 13:"GPSSpeed", 14:"GPSTrackRef", 15:"GPSTrack", 16:"GPSImgDirectionRef", 17:"GPSImgDirection", 18:"GPSMapDatum", 19:"GPSDestLatitudeRef", 20:"GPSDestLatitude", 21:"GPSDestLongitudeRef", 22:"GPSDestLongitude", 23:"GPSDestBearingRef", 24:"GPSDestBearing", 25:"GPSDestDistanceRef", 26:"GPSDestDistance", 27:"GPSProcessingMethod", 28:"GPSAreaInformation", 29:"GPSDateStamp", 30:"GPSDifferential"}, g = {ExposureProgram:{0:"Not defined", 1:"Manual", 2:"Normal program", 3:"Aperture priority", 4:"Shutter priority", 5:"Creative program", 6:"Action program", 7:"Portrait mode", 8:"Landscape mode"}, MeteringMode:{0:"Unknown", 1:"Average", 2:"CenterWeightedAverage", 3:"Spot", 4:"MultiSpot", 5:"Pattern", 6:"Partial", 255:"Other"}, LightSource:{0:"Unknown", 1:"Daylight", 2:"Fluorescent", 3:"Tungsten (incandescent light)", 4:"Flash", 9:"Fine weather", 10:"Cloudy weather", 11:"Shade", 12:"Daylight fluorescent (D 5700 - 7100K)", 13:"Day white fluorescent (N 4600 - 5400K)", 14:"Cool white fluorescent (W 3900 - 4500K)", 15:"White fluorescent (WW 3200 - 3700K)", 17:"Standard light A", 18:"Standard light B", 19:"Standard light C", 20:"D55", 21:"D65", 22:"D75", 23:"D50", 24:"ISO studio tungsten", 255:"Other"}, Flash:{0:"Flash did not fire", 1:"Flash fired", 5:"Strobe return light not detected", 7:"Strobe return light detected", 9:"Flash fired, compulsory flash mode", 13:"Flash fired, compulsory flash mode, return light not detected", 15:"Flash fired, compulsory flash mode, return light detected", 16:"Flash did not fire, compulsory flash mode", 24:"Flash did not fire, auto mode", 25:"Flash fired, auto mode", 29:"Flash fired, auto mode, return light not detected", 31:"Flash fired, auto mode, return light detected", 32:"No flash function", 65:"Flash fired, red-eye reduction mode", 69:"Flash fired, red-eye reduction mode, return light not detected", 71:"Flash fired, red-eye reduction mode, return light detected", 73:"Flash fired, compulsory flash mode, red-eye reduction mode", 77:"Flash fired, compulsory flash mode, red-eye reduction mode, return light not detected", 79:"Flash fired, compulsory flash mode, red-eye reduction mode, return light detected", 89:"Flash fired, auto mode, red-eye reduction mode", 93:"Flash fired, auto mode, return light not detected, red-eye reduction mode", 95:"Flash fired, auto mode, return light detected, red-eye reduction mode"}, SensingMethod:{1:"Not defined", 2:"One-chip color area sensor", 3:"Two-chip color area sensor", 4:"Three-chip color area sensor", 5:"Color sequential area sensor", 7:"Trilinear sensor", 8:"Color sequential linear sensor"}, SceneCaptureType:{0:"Standard", 1:"Landscape", 2:"Portrait", 3:"Night scene"}, SceneType:{1:"Directly photographed"}, CustomRendered:{0:"Normal process", 1:"Custom process"}, WhiteBalance:{0:"Auto white balance", 1:"Manual white balance"}, GainControl:{0:"None", 1:"Low gain up", 2:"High gain up", 3:"Low gain down", 4:"High gain down"}, Contrast:{0:"Normal", 1:"Soft", 2:"Hard"}, Saturation:{0:"Normal", 1:"Low saturation", 2:"High saturation"}, Sharpness:{0:"Normal", 1:"Soft", 2:"Hard"}, SubjectDistanceRange:{0:"Unknown", 1:"Macro", 2:"Close view", 3:"Distant view"}, FileSource:{3:"DSC"}, Components:{0:"", 1:"Y", 2:"Cb", 3:"Cr", 4:"R", 5:"G", 6:"B"}};
    return{readFromBinaryFile:u, pretty:c, getTag:s, getAllTags:l, getData:a, Tags:f, TiffTags:h, GPSTags:p, StringValues:g}
}();
!function (t) {
    function e(t, e) {
        this.file = t, this.options = o.extend({}, r, e), this._defaults = r, this._name = n, this.init()
    }

    var n = "canvasResize", o = {newsize:function (t, e, n, o, r) {
        var i = r ? "h" : "";
        if (n && t > n || o && e > o) {
            var a = t / e;
            (a >= 1 || 0 === o) && n && !r ? (t = n, e = n / a >> 0) : r && n / o >= a ? (t = n, e = n / a >> 0, i = "w") : (t = o * a >> 0, e = o)
        }
        return{width:t, height:e, cropped:i}
    }, dataURLtoBlob:function (t) {
        for (var e = t.split(",")[0].split(":")[1].split(";")[0], n = atob(t.split(",")[1]), o = new ArrayBuffer(n.length), r = new Uint8Array(o), i = 0; i < n.length; i++)r[i] = n.charCodeAt(i);
        var a = window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder;
        return a ? (a = new (window.BlobBuilder || window.WebKitBlobBuilder || window.MozBlobBuilder), a.append(o), a.getBlob(e)) : a = new Blob([o], {type:e})
    }, detectSubsampling:function (t) {
        var e = t.width, n = t.height;
        if (e * n > 1048576) {
            var o = document.createElement("canvas");
            o.width = o.height = 1;
            var r = o.getContext("2d");
            return r.drawImage(t, -e + 1, 0), 0 === r.getImageData(0, 0, 1, 1).data[3]
        }
        return!1
    }, rotate:function (t, e) {
        var n = {1:{90:6, 180:3, 270:8}, 2:{90:7, 180:4, 270:5}, 3:{90:8, 180:1, 270:6}, 4:{90:5, 180:2, 270:7}, 5:{90:2, 180:7, 270:4}, 6:{90:3, 180:8, 270:1}, 7:{90:4, 180:5, 270:2}, 8:{90:1, 180:6, 270:3}};
        return n[t][e] ? n[t][e] : t
    }, transformCoordinate:function (t, e, n, o) {
        switch (o) {
            case 5:
            case 6:
            case 7:
            case 8:
                t.width = n, t.height = e;
                break;
            default:
                t.width = e, t.height = n
        }
        var r = t.getContext("2d");
        switch (o) {
            case 1:
                break;
            case 2:
                r.translate(e, 0), r.scale(-1, 1);
                break;
            case 3:
                r.translate(e, n), r.rotate(Math.PI);
                break;
            case 4:
                r.translate(0, n), r.scale(1, -1);
                break;
            case 5:
                r.rotate(.5 * Math.PI), r.scale(1, -1);
                break;
            case 6:
                r.rotate(.5 * Math.PI), r.translate(0, -n);
                break;
            case 7:
                r.rotate(.5 * Math.PI), r.translate(e, -n), r.scale(-1, 1);
                break;
            case 8:
                r.rotate(-.5 * Math.PI), r.translate(-e, 0)
        }
    }, detectVerticalSquash:function (t, e, n) {
        var o = document.createElement("canvas");
        o.width = 1, o.height = n;
        var r = o.getContext("2d");
        r.drawImage(t, 0, 0);
        for (var i = r.getImageData(0, 0, 1, n).data, a = 0, s = n, l = n; l > a;) {
            var c = i[4 * (l - 1) + 3];
            0 === c ? s = l : a = l, l = s + a >> 1
        }
        var u = l / n;
        return 0 === u ? 1 : u
    }, callback:function (t) {
        return t
    }, extend:function () {
        var t = arguments[0] || {}, e = 1, n = arguments.length, r = !1;
        t.constructor === Boolean && (r = t, t = arguments[1] || {}), 1 === n && (t = this, e = 0);
        for (var i; n > e; e++)if (null !== (i = arguments[e]))for (var a in i)t !== i[a] && (r && "object" == typeof i[a] && t[a] ? o.extend(t[a], i[a]) : void 0 !== i[a] && (t[a] = i[a]));
        return t
    }}, r = {width:300, height:0, crop:!1, quality:80, rotate:0, callback:o.callback};
    e.prototype = {init:function () {
        var t = this, e = this.file, n = new FileReader;
        n.onloadend = function (n) {
            var r = n.target.result, i = atob(r.split(",")[1]), a = new BinaryFile(i, 0, i.length), s = EXIF.readFromBinaryFile(a), l = new Image;
            l.onload = function () {
                var n = s.Orientation || 1;
                n = o.rotate(n, t.options.rotate);
                var r = n >= 5 && 8 >= n ? o.newsize(l.height, l.width, t.options.width, t.options.height, t.options.crop) : o.newsize(l.width, l.height, t.options.width, t.options.height, t.options.crop), i = l.width, a = l.height, c = r.width, u = r.height, d = document.createElement("canvas"), f = d.getContext("2d");
                f.save(), o.transformCoordinate(d, c, u, n), o.detectSubsampling(l) && (i /= 2, a /= 2);
                var h = 1024, p = document.createElement("canvas");
                p.width = p.height = h;
                for (var g = p.getContext("2d"), m = o.detectVerticalSquash(l, i, a), v = 0; a > v;) {
                    for (var y = v + h > a ? a - v : h, b = 0; i > b;) {
                        var w = b + h > i ? i - b : h;
                        g.clearRect(0, 0, h, h), g.drawImage(l, -b, -v);
                        var x = Math.floor(b * c / i), C = Math.ceil(w * c / i), E = Math.floor(v * u / a / m), S = Math.ceil(y * u / a / m);
                        f.drawImage(p, 0, 0, w, y, x, E, C, S), b += h
                    }
                    v += h
                }
                f.restore(), p = g = null;
                var _ = document.createElement("canvas");
                _.width = "h" === r.cropped ? u : c, _.height = "w" === r.cropped ? c : u;
                var k = "h" === r.cropped ? .5 * (u - c) : 0, T = "w" === r.cropped ? .5 * (c - u) : 0;
                if (newctx = _.getContext("2d"), newctx.drawImage(d, k, T, c, u), console.log(e, e.type), "image/png" === e.type)var N = _.toDataURL(e.type); else var N = _.toDataURL("image/jpeg", .01 * t.options.quality);
                t.options.callback(N, _.width, _.height)
            }, l.src = r
        }, n.readAsDataURL(e)
    }}, t[n] = function (t, n) {
        return"string" == typeof t ? o[t](n) : (new e(t, n), void 0)
    }
}(window), function (t) {
    var e = function (n, o) {
        var r = !1;
        try {
            if ("object" != typeof n || 1 !== n.nodeType)throw new Error("First argument should be a DOM element");
            var i, a, s, l = n.getAttribute("data-withinviewport-settings") && window.JSON ? JSON.parse(n.getAttribute("data-withinviewport-settings")) : {}, o = "string" == typeof o ? {sides:o} : o || {}, c = {sides:o.sides || l.sides || e.defaults.sides || "all", top:o.top || l.top || e.defaults.top || 0, right:o.right || l.right || e.defaults.right || 0, bottom:o.bottom || l.bottom || e.defaults.bottom || 0, left:o.left || l.left || e.defaults.left || 0}, u = {top:function () {
                return f[1] >= d[1] + c.top
            }, right:function () {
                var t = window.innerWidth || document.documentElement.clientWidth;
                return f[0] + n.offsetWidth <= t + d[0] - c.right
            }, bottom:function () {
                var t = window.innerHeight || document.documentElement.clientHeight;
                return f[1] + n.offsetHeight <= t + d[1] - c.bottom
            }, left:function () {
                return f[0] >= d[0] + c.left
            }, all:function () {
                return u.top() && u.right() && u.bottom() && u.left()
            }}, d = function () {
                var e = t.body.scrollLeft, n = t.body.scrollTop;
                return 0 == n && (n = window.pageYOffset ? window.pageYOffset : t.body.parentElement ? t.body.parentElement.scrollTop : 0), 0 == e && (e = window.pageXOffset ? window.pageXOffset : t.body.parentElement ? t.body.parentElement.scrollLeft : 0), [e, n]
            }(), f = function () {
                var t = n, e = 0, o = 0;
                if (t.offsetParent)for (e = t.offsetLeft, o = t.offsetTop; t = t.offsetParent;)e += t.offsetLeft, o += t.offsetTop;
                return[e, o]
            }();
            for (i = c.sides.split(" "), a = i.length; a--;)if (s = i[a].toLowerCase(), /top|right|bottom|left|all/.test(s)) {
                if (!u[s]())return!1;
                r = !0
            }
            return r
        } catch (h) {
        } finally {
            return r
        }
    };
    e.prototype.defaults = {sides:"all", top:0, right:0, bottom:0, left:0}, e.defaults = e.prototype.defaults, window.withinViewport = e;
    for (var n = ["top", "right", "bottom", "left"], o = n.length; o--;) {
        var r = n[o];
        e.prototype[r] = function (t) {
            return e(t, r)
        }, e[r] = e.prototype[r]
    }
}(document), function () {
    var t = this, e = t._, n = {}, o = Array.prototype, r = Object.prototype, i = Function.prototype, a = o.push, s = o.slice, l = o.concat, c = r.toString, u = r.hasOwnProperty, d = o.forEach, f = o.map, h = o.reduce, p = o.reduceRight, g = o.filter, m = o.every, v = o.some, y = o.indexOf, b = o.lastIndexOf, w = Array.isArray, x = Object.keys, C = i.bind, E = function (t) {
        return t instanceof E ? t : this instanceof E ? (this._wrapped = t, void 0) : new E(t)
    };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = E), exports._ = E) : t._ = E, E.VERSION = "1.5.1";
    var S = E.each = E.forEach = function (t, e, o) {
        if (null != t)if (d && t.forEach === d)t.forEach(e, o); else if (t.length === +t.length) {
            for (var r = 0, i = t.length; i > r; r++)if (e.call(o, t[r], r, t) === n)return
        } else for (var a in t)if (E.has(t, a) && e.call(o, t[a], a, t) === n)return
    };
    E.map = E.collect = function (t, e, n) {
        var o = [];
        return null == t ? o : f && t.map === f ? t.map(e, n) : (S(t, function (t, r, i) {
            o.push(e.call(n, t, r, i))
        }), o)
    };
    var _ = "Reduce of empty array with no initial value";
    E.reduce = E.foldl = E.inject = function (t, e, n, o) {
        var r = arguments.length > 2;
        if (null == t && (t = []), h && t.reduce === h)return o && (e = E.bind(e, o)), r ? t.reduce(e, n) : t.reduce(e);
        if (S(t, function (t, i, a) {
            r ? n = e.call(o, n, t, i, a) : (n = t, r = !0)
        }), !r)throw new TypeError(_);
        return n
    }, E.reduceRight = E.foldr = function (t, e, n, o) {
        var r = arguments.length > 2;
        if (null == t && (t = []), p && t.reduceRight === p)return o && (e = E.bind(e, o)), r ? t.reduceRight(e, n) : t.reduceRight(e);
        var i = t.length;
        if (i !== +i) {
            var a = E.keys(t);
            i = a.length
        }
        if (S(t, function (s, l, c) {
            l = a ? a[--i] : --i, r ? n = e.call(o, n, t[l], l, c) : (n = t[l], r = !0)
        }), !r)throw new TypeError(_);
        return n
    }, E.find = E.detect = function (t, e, n) {
        var o;
        return k(t, function (t, r, i) {
            return e.call(n, t, r, i) ? (o = t, !0) : void 0
        }), o
    }, E.filter = E.select = function (t, e, n) {
        var o = [];
        return null == t ? o : g && t.filter === g ? t.filter(e, n) : (S(t, function (t, r, i) {
            e.call(n, t, r, i) && o.push(t)
        }), o)
    }, E.reject = function (t, e, n) {
        return E.filter(t, function (t, o, r) {
            return!e.call(n, t, o, r)
        }, n)
    }, E.every = E.all = function (t, e, o) {
        e || (e = E.identity);
        var r = !0;
        return null == t ? r : m && t.every === m ? t.every(e, o) : (S(t, function (t, i, a) {
            return(r = r && e.call(o, t, i, a)) ? void 0 : n
        }), !!r)
    };
    var k = E.some = E.any = function (t, e, o) {
        e || (e = E.identity);
        var r = !1;
        return null == t ? r : v && t.some === v ? t.some(e, o) : (S(t, function (t, i, a) {
            return r || (r = e.call(o, t, i, a)) ? n : void 0
        }), !!r)
    };
    E.contains = E.include = function (t, e) {
        return null == t ? !1 : y && t.indexOf === y ? -1 != t.indexOf(e) : k(t, function (t) {
            return t === e
        })
    }, E.invoke = function (t, e) {
        var n = s.call(arguments, 2), o = E.isFunction(e);
        return E.map(t, function (t) {
            return(o ? e : t[e]).apply(t, n)
        })
    }, E.pluck = function (t, e) {
        return E.map(t, function (t) {
            return t[e]
        })
    }, E.where = function (t, e, n) {
        return E.isEmpty(e) ? n ? void 0 : [] : E[n ? "find" : "filter"](t, function (t) {
            for (var n in e)if (e[n] !== t[n])return!1;
            return!0
        })
    }, E.findWhere = function (t, e) {
        return E.where(t, e, !0)
    }, E.max = function (t, e, n) {
        if (!e && E.isArray(t) && t[0] === +t[0] && t.length < 65535)return Math.max.apply(Math, t);
        if (!e && E.isEmpty(t))return-1 / 0;
        var o = {computed:-1 / 0, value:-1 / 0};
        return S(t, function (t, r, i) {
            var a = e ? e.call(n, t, r, i) : t;
            a > o.computed && (o = {value:t, computed:a})
        }), o.value
    }, E.min = function (t, e, n) {
        if (!e && E.isArray(t) && t[0] === +t[0] && t.length < 65535)return Math.min.apply(Math, t);
        if (!e && E.isEmpty(t))return 1 / 0;
        var o = {computed:1 / 0, value:1 / 0};
        return S(t, function (t, r, i) {
            var a = e ? e.call(n, t, r, i) : t;
            a < o.computed && (o = {value:t, computed:a})
        }), o.value
    }, E.shuffle = function (t) {
        var e, n = 0, o = [];
        return S(t, function (t) {
            e = E.random(n++), o[n - 1] = o[e], o[e] = t
        }), o
    };
    var T = function (t) {
        return E.isFunction(t) ? t : function (e) {
            return e[t]
        }
    };
    E.sortBy = function (t, e, n) {
        var o = T(e);
        return E.pluck(E.map(t,function (t, e, r) {
            return{value:t, index:e, criteria:o.call(n, t, e, r)}
        }).sort(function (t, e) {
            var n = t.criteria, o = e.criteria;
            if (n !== o) {
                if (n > o || void 0 === n)return 1;
                if (o > n || void 0 === o)return-1
            }
            return t.index < e.index ? -1 : 1
        }), "value")
    };
    var N = function (t, e, n, o) {
        var r = {}, i = T(null == e ? E.identity : e);
        return S(t, function (e, a) {
            var s = i.call(n, e, a, t);
            o(r, s, e)
        }), r
    };
    E.groupBy = function (t, e, n) {
        return N(t, e, n, function (t, e, n) {
            (E.has(t, e) ? t[e] : t[e] = []).push(n)
        })
    }, E.countBy = function (t, e, n) {
        return N(t, e, n, function (t, e) {
            E.has(t, e) || (t[e] = 0), t[e]++
        })
    }, E.sortedIndex = function (t, e, n, o) {
        n = null == n ? E.identity : T(n);
        for (var r = n.call(o, e), i = 0, a = t.length; a > i;) {
            var s = i + a >>> 1;
            n.call(o, t[s]) < r ? i = s + 1 : a = s
        }
        return i
    }, E.toArray = function (t) {
        return t ? E.isArray(t) ? s.call(t) : t.length === +t.length ? E.map(t, E.identity) : E.values(t) : []
    }, E.size = function (t) {
        return null == t ? 0 : t.length === +t.length ? t.length : E.keys(t).length
    }, E.first = E.head = E.take = function (t, e, n) {
        return null == t ? void 0 : null == e || n ? t[0] : s.call(t, 0, e)
    }, E.initial = function (t, e, n) {
        return s.call(t, 0, t.length - (null == e || n ? 1 : e))
    }, E.last = function (t, e, n) {
        return null == t ? void 0 : null == e || n ? t[t.length - 1] : s.call(t, Math.max(t.length - e, 0))
    }, E.rest = E.tail = E.drop = function (t, e, n) {
        return s.call(t, null == e || n ? 1 : e)
    }, E.compact = function (t) {
        return E.filter(t, E.identity)
    };
    var P = function (t, e, n) {
        return e && E.every(t, E.isArray) ? l.apply(n, t) : (S(t, function (t) {
            E.isArray(t) || E.isArguments(t) ? e ? a.apply(n, t) : P(t, e, n) : n.push(t)
        }), n)
    };
    E.flatten = function (t, e) {
        return P(t, e, [])
    }, E.without = function (t) {
        return E.difference(t, s.call(arguments, 1))
    }, E.uniq = E.unique = function (t, e, n, o) {
        E.isFunction(e) && (o = n, n = e, e = !1);
        var r = n ? E.map(t, n, o) : t, i = [], a = [];
        return S(r, function (n, o) {
            (e ? o && a[a.length - 1] === n : E.contains(a, n)) || (a.push(n), i.push(t[o]))
        }), i
    }, E.union = function () {
        return E.uniq(E.flatten(arguments, !0))
    }, E.intersection = function (t) {
        var e = s.call(arguments, 1);
        return E.filter(E.uniq(t), function (t) {
            return E.every(e, function (e) {
                return E.indexOf(e, t) >= 0
            })
        })
    }, E.difference = function (t) {
        var e = l.apply(o, s.call(arguments, 1));
        return E.filter(t, function (t) {
            return!E.contains(e, t)
        })
    }, E.zip = function () {
        for (var t = E.max(E.pluck(arguments, "length").concat(0)), e = new Array(t), n = 0; t > n; n++)e[n] = E.pluck(arguments, "" + n);
        return e
    }, E.object = function (t, e) {
        if (null == t)return{};
        for (var n = {}, o = 0, r = t.length; r > o; o++)e ? n[t[o]] = e[o] : n[t[o][0]] = t[o][1];
        return n
    }, E.indexOf = function (t, e, n) {
        if (null == t)return-1;
        var o = 0, r = t.length;
        if (n) {
            if ("number" != typeof n)return o = E.sortedIndex(t, e), t[o] === e ? o : -1;
            o = 0 > n ? Math.max(0, r + n) : n
        }
        if (y && t.indexOf === y)return t.indexOf(e, n);
        for (; r > o; o++)if (t[o] === e)return o;
        return-1
    }, E.lastIndexOf = function (t, e, n) {
        if (null == t)return-1;
        var o = null != n;
        if (b && t.lastIndexOf === b)return o ? t.lastIndexOf(e, n) : t.lastIndexOf(e);
        for (var r = o ? n : t.length; r--;)if (t[r] === e)return r;
        return-1
    }, E.range = function (t, e, n) {
        arguments.length <= 1 && (e = t || 0, t = 0), n = arguments[2] || 1;
        for (var o = Math.max(Math.ceil((e - t) / n), 0), r = 0, i = new Array(o); o > r;)i[r++] = t, t += n;
        return i
    };
    var R = function () {
    };
    E.bind = function (t, e) {
        var n, o;
        if (C && t.bind === C)return C.apply(t, s.call(arguments, 1));
        if (!E.isFunction(t))throw new TypeError;
        return n = s.call(arguments, 2), o = function () {
            if (!(this instanceof o))return t.apply(e, n.concat(s.call(arguments)));
            R.prototype = t.prototype;
            var r = new R;
            R.prototype = null;
            var i = t.apply(r, n.concat(s.call(arguments)));
            return Object(i) === i ? i : r
        }
    }, E.partial = function (t) {
        var e = s.call(arguments, 1);
        return function () {
            return t.apply(this, e.concat(s.call(arguments)))
        }
    }, E.bindAll = function (t) {
        var e = s.call(arguments, 1);
        if (0 === e.length)throw new Error("bindAll must be passed function names");
        return S(e, function (e) {
            t[e] = E.bind(t[e], t)
        }), t
    }, E.memoize = function (t, e) {
        var n = {};
        return e || (e = E.identity), function () {
            var o = e.apply(this, arguments);
            return E.has(n, o) ? n[o] : n[o] = t.apply(this, arguments)
        }
    }, E.delay = function (t, e) {
        var n = s.call(arguments, 2);
        return setTimeout(function () {
            return t.apply(null, n)
        }, e)
    }, E.defer = function (t) {
        return E.delay.apply(E, [t, 1].concat(s.call(arguments, 1)))
    }, E.throttle = function (t, e, n) {
        var o, r, i, a = null, s = 0;
        n || (n = {});
        var l = function () {
            s = n.leading === !1 ? 0 : new Date, a = null, i = t.apply(o, r)
        };
        return function () {
            var c = new Date;
            s || n.leading !== !1 || (s = c);
            var u = e - (c - s);
            return o = this, r = arguments, 0 >= u ? (clearTimeout(a), a = null, s = c, i = t.apply(o, r)) : a || n.trailing === !1 || (a = setTimeout(l, u)), i
        }
    }, E.debounce = function (t, e, n) {
        var o, r = null;
        return function () {
            var i = this, a = arguments, s = function () {
                r = null, n || (o = t.apply(i, a))
            }, l = n && !r;
            return clearTimeout(r), r = setTimeout(s, e), l && (o = t.apply(i, a)), o
        }
    }, E.once = function (t) {
        var e, n = !1;
        return function () {
            return n ? e : (n = !0, e = t.apply(this, arguments), t = null, e)
        }
    }, E.wrap = function (t, e) {
        return function () {
            var n = [t];
            return a.apply(n, arguments), e.apply(this, n)
        }
    }, E.compose = function () {
        var t = arguments;
        return function () {
            for (var e = arguments, n = t.length - 1; n >= 0; n--)e = [t[n].apply(this, e)];
            return e[0]
        }
    }, E.after = function (t, e) {
        return function () {
            return--t < 1 ? e.apply(this, arguments) : void 0
        }
    }, E.keys = x || function (t) {
        if (t !== Object(t))throw new TypeError("Invalid object");
        var e = [];
        for (var n in t)E.has(t, n) && e.push(n);
        return e
    }, E.values = function (t) {
        var e = [];
        for (var n in t)E.has(t, n) && e.push(t[n]);
        return e
    }, E.pairs = function (t) {
        var e = [];
        for (var n in t)E.has(t, n) && e.push([n, t[n]]);
        return e
    }, E.invert = function (t) {
        var e = {};
        for (var n in t)E.has(t, n) && (e[t[n]] = n);
        return e
    }, E.functions = E.methods = function (t) {
        var e = [];
        for (var n in t)E.isFunction(t[n]) && e.push(n);
        return e.sort()
    }, E.extend = function (t) {
        return S(s.call(arguments, 1), function (e) {
            if (e)for (var n in e)t[n] = e[n]
        }), t
    }, E.pick = function (t) {
        var e = {}, n = l.apply(o, s.call(arguments, 1));
        return S(n, function (n) {
            n in t && (e[n] = t[n])
        }), e
    }, E.omit = function (t) {
        var e = {}, n = l.apply(o, s.call(arguments, 1));
        for (var r in t)E.contains(n, r) || (e[r] = t[r]);
        return e
    }, E.defaults = function (t) {
        return S(s.call(arguments, 1), function (e) {
            if (e)for (var n in e)void 0 === t[n] && (t[n] = e[n])
        }), t
    }, E.clone = function (t) {
        return E.isObject(t) ? E.isArray(t) ? t.slice() : E.extend({}, t) : t
    }, E.tap = function (t, e) {
        return e(t), t
    };
    var A = function (t, e, n, o) {
        if (t === e)return 0 !== t || 1 / t == 1 / e;
        if (null == t || null == e)return t === e;
        t instanceof E && (t = t._wrapped), e instanceof E && (e = e._wrapped);
        var r = c.call(t);
        if (r != c.call(e))return!1;
        switch (r) {
            case"[object String]":
                return t == String(e);
            case"[object Number]":
                return t != +t ? e != +e : 0 == t ? 1 / t == 1 / e : t == +e;
            case"[object Date]":
            case"[object Boolean]":
                return+t == +e;
            case"[object RegExp]":
                return t.source == e.source && t.global == e.global && t.multiline == e.multiline && t.ignoreCase == e.ignoreCase
        }
        if ("object" != typeof t || "object" != typeof e)return!1;
        for (var i = n.length; i--;)if (n[i] == t)return o[i] == e;
        var a = t.constructor, s = e.constructor;
        if (a !== s && !(E.isFunction(a) && a instanceof a && E.isFunction(s) && s instanceof s))return!1;
        n.push(t), o.push(e);
        var l = 0, u = !0;
        if ("[object Array]" == r) {
            if (l = t.length, u = l == e.length)for (; l-- && (u = A(t[l], e[l], n, o)););
        } else {
            for (var d in t)if (E.has(t, d) && (l++, !(u = E.has(e, d) && A(t[d], e[d], n, o))))break;
            if (u) {
                for (d in e)if (E.has(e, d) && !l--)break;
                u = !l
            }
        }
        return n.pop(), o.pop(), u
    };
    E.isEqual = function (t, e) {
        return A(t, e, [], [])
    }, E.isEmpty = function (t) {
        if (null == t)return!0;
        if (E.isArray(t) || E.isString(t))return 0 === t.length;
        for (var e in t)if (E.has(t, e))return!1;
        return!0
    }, E.isElement = function (t) {
        return!(!t || 1 !== t.nodeType)
    }, E.isArray = w || function (t) {
        return"[object Array]" == c.call(t)
    }, E.isObject = function (t) {
        return t === Object(t)
    }, S(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function (t) {
        E["is" + t] = function (e) {
            return c.call(e) == "[object " + t + "]"
        }
    }), E.isArguments(arguments) || (E.isArguments = function (t) {
        return!(!t || !E.has(t, "callee"))
    }), "function" != typeof/./ && (E.isFunction = function (t) {
        return"function" == typeof t
    }), E.isFinite = function (t) {
        return isFinite(t) && !isNaN(parseFloat(t))
    }, E.isNaN = function (t) {
        return E.isNumber(t) && t != +t
    }, E.isBoolean = function (t) {
        return t === !0 || t === !1 || "[object Boolean]" == c.call(t)
    }, E.isNull = function (t) {
        return null === t
    }, E.isUndefined = function (t) {
        return void 0 === t
    }, E.has = function (t, e) {
        return u.call(t, e)
    }, E.noConflict = function () {
        return t._ = e, this
    }, E.identity = function (t) {
        return t
    }, E.times = function (t, e, n) {
        for (var o = Array(Math.max(0, t)), r = 0; t > r; r++)o[r] = e.call(n, r);
        return o
    }, E.random = function (t, e) {
        return null == e && (e = t, t = 0), t + Math.floor(Math.random() * (e - t + 1))
    };
    var O = {escape:{"&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;", "'":"&#x27;", "/":"&#x2F;"}};
    O.unescape = E.invert(O.escape);
    var D = {escape:new RegExp("[" + E.keys(O.escape).join("") + "]", "g"), unescape:new RegExp("(" + E.keys(O.unescape).join("|") + ")", "g")};
    E.each(["escape", "unescape"], function (t) {
        E[t] = function (e) {
            return null == e ? "" : ("" + e).replace(D[t], function (e) {
                return O[t][e]
            })
        }
    }), E.result = function (t, e) {
        if (null == t)return void 0;
        var n = t[e];
        return E.isFunction(n) ? n.call(t) : n
    }, E.mixin = function (t) {
        S(E.functions(t), function (e) {
            var n = E[e] = t[e];
            E.prototype[e] = function () {
                var t = [this._wrapped];
                return a.apply(t, arguments), B.call(this, n.apply(E, t))
            }
        })
    };
    var I = 0;
    E.uniqueId = function (t) {
        var e = ++I + "";
        return t ? t + e : e
    }, E.templateSettings = {evaluate:/<%([\s\S]+?)%>/g, interpolate:/<%=([\s\S]+?)%>/g, escape:/<%-([\s\S]+?)%>/g};
    var $ = /(.)^/, L = {"'":"'", "\\":"\\", "\r":"r", "\n":"n", "	":"t", "\u2028":"u2028", "\u2029":"u2029"}, M = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    E.template = function (t, e, n) {
        var o;
        n = E.defaults({}, n, E.templateSettings);
        var r = new RegExp([(n.escape || $).source, (n.interpolate || $).source, (n.evaluate || $).source].join("|") + "|$", "g"), i = 0, a = "__p+='";
        t.replace(r, function (e, n, o, r, s) {
            return a += t.slice(i, s).replace(M, function (t) {
                return"\\" + L[t]
            }), n && (a += "'+\n((__t=(" + n + "))==null?'':_.escape(__t))+\n'"), o && (a += "'+\n((__t=(" + o + "))==null?'':__t)+\n'"), r && (a += "';\n" + r + "\n__p+='"), i = s + e.length, e
        }), a += "';\n", n.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join,print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
        try {
            o = new Function(n.variable || "obj", "_", a)
        } catch (s) {
            throw s.source = a, s
        }
        if (e)return o(e, E);
        var l = function (t) {
            return o.call(this, t, E)
        };
        return l.source = "function(" + (n.variable || "obj") + "){\n" + a + "}", l
    }, E.chain = function (t) {
        return E(t).chain()
    };
    var B = function (t) {
        return this._chain ? E(t).chain() : t
    };
    E.mixin(E), S(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function (t) {
        var e = o[t];
        E.prototype[t] = function () {
            var n = this._wrapped;
            return e.apply(n, arguments), "shift" != t && "splice" != t || 0 !== n.length || delete n[0], B.call(this, n)
        }
    }), S(["concat", "join", "slice"], function (t) {
        var e = o[t];
        E.prototype[t] = function () {
            return B.call(this, e.apply(this._wrapped, arguments))
        }
    }), E.extend(E.prototype, {chain:function () {
        return this._chain = !0, this
    }, value:function () {
        return this._wrapped
    }})
}.call(this);
var Handlebars = function () {
    var t = function () {
        "use strict";
        function t(t) {
            this.string = t
        }

        var e;
        return t.prototype.toString = function () {
            return"" + this.string
        }, e = t
    }(), e = function (t) {
        "use strict";
        function e(t) {
            return s[t] || "&amp;"
        }

        function n(t, e) {
            for (var n in e)e.hasOwnProperty(n) && (t[n] = e[n])
        }

        function o(t) {
            return t instanceof a ? t.toString() : t || 0 === t ? (t = "" + t, c.test(t) ? t.replace(l, e) : t) : ""
        }

        function r(t) {
            return t || 0 === t ? f(t) && 0 === t.length ? !0 : !1 : !0
        }

        var i = {}, a = t, s = {"&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;", "'":"&#x27;", "`":"&#x60;"}, l = /[&<>"'`]/g, c = /[&<>"'`]/;
        i.extend = n;
        var u = Object.prototype.toString;
        i.toString = u;
        var d = function (t) {
            return"function" == typeof t
        };
        d(/x/) && (d = function (t) {
            return"function" == typeof t && "[object Function]" === u.call(t)
        });
        var d;
        i.isFunction = d;
        var f = Array.isArray || function (t) {
            return t && "object" == typeof t ? "[object Array]" === u.call(t) : !1
        };
        return i.isArray = f, i.escapeExpression = o, i.isEmpty = r, i
    }(t), n = function () {
        "use strict";
        function t() {
            for (var t = Error.prototype.constructor.apply(this, arguments), e = 0; e < n.length; e++)this[n[e]] = t[n[e]]
        }

        var e, n = ["description", "fileName", "lineNumber", "message", "name", "number", "stack"];
        return t.prototype = new Error, e = t
    }(), o = function (t, e) {
        "use strict";
        function n(t, e) {
            this.helpers = t || {}, this.partials = e || {}, o(this)
        }

        function o(t) {
            t.registerHelper("helperMissing", function (t) {
                if (2 === arguments.length)return void 0;
                throw new Error("Missing helper: '" + t + "'")
            }), t.registerHelper("blockHelperMissing", function (e, n) {
                var o = n.inverse || function () {
                }, r = n.fn;
                return f(e) && (e = e.call(this)), e === !0 ? r(this) : e === !1 || null == e ? o(this) : d(e) ? e.length > 0 ? t.helpers.each(e, n) : o(this) : r(e)
            }), t.registerHelper("each", function (t, e) {
                var n, o = e.fn, r = e.inverse, i = 0, a = "";
                if (f(t) && (t = t.call(this)), e.data && (n = m(e.data)), t && "object" == typeof t)if (d(t))for (var s = t.length; s > i; i++)n && (n.index = i, n.first = 0 === i, n.last = i === t.length - 1), a += o(t[i], {data:n}); else for (var l in t)t.hasOwnProperty(l) && (n && (n.key = l), a += o(t[l], {data:n}), i++);
                return 0 === i && (a = r(this)), a
            }), t.registerHelper("if", function (t, e) {
                return f(t) && (t = t.call(this)), !e.hash.includeZero && !t || a.isEmpty(t) ? e.inverse(this) : e.fn(this)
            }), t.registerHelper("unless", function (e, n) {
                return t.helpers["if"].call(this, e, {fn:n.inverse, inverse:n.fn, hash:n.hash})
            }), t.registerHelper("with", function (t, e) {
                return f(t) && (t = t.call(this)), a.isEmpty(t) ? void 0 : e.fn(t)
            }), t.registerHelper("log", function (e, n) {
                var o = n.data && null != n.data.level ? parseInt(n.data.level, 10) : 1;
                t.log(o, e)
            })
        }

        function r(t, e) {
            g.log(t, e)
        }

        var i = {}, a = t, s = e, l = "1.1.2";
        i.VERSION = l;
        var c = 4;
        i.COMPILER_REVISION = c;
        var u = {1:"<= 1.0.rc.2", 2:"== 1.0.0-rc.3", 3:"== 1.0.0-rc.4", 4:">= 1.0.0"};
        i.REVISION_CHANGES = u;
        var d = a.isArray, f = a.isFunction, h = a.toString, p = "[object Object]";
        i.HandlebarsEnvironment = n, n.prototype = {constructor:n, logger:g, log:r, registerHelper:function (t, e, n) {
            if (h.call(t) === p) {
                if (n || e)throw new s("Arg not supported with multiple helpers");
                a.extend(this.helpers, t)
            } else n && (e.not = n), this.helpers[t] = e
        }, registerPartial:function (t, e) {
            h.call(t) === p ? a.extend(this.partials, t) : this.partials[t] = e
        }};
        var g = {methodMap:{0:"debug", 1:"info", 2:"warn", 3:"error"}, DEBUG:0, INFO:1, WARN:2, ERROR:3, level:3, log:function (t, e) {
            if (g.level <= t) {
                var n = g.methodMap[t];
                "undefined" != typeof console && console[n] && console[n].call(console, e)
            }
        }};
        i.logger = g, i.log = r;
        var m = function (t) {
            var e = {};
            return a.extend(e, t), e
        };
        return i.createFrame = m, i
    }(e, n), r = function (t, e, n) {
        "use strict";
        function o(t) {
            var e = t && t[0] || 1, n = f;
            if (e !== n) {
                if (n > e) {
                    var o = h[n], r = h[e];
                    throw new Error("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version (" + o + ") or downgrade your runtime to an older version (" + r + ").")
                }
                throw new Error("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version (" + t[1] + ").")
            }
        }

        function r(t, e) {
            if (!e)throw new Error("No environment passed to template");
            var n;
            n = e.compile ? function (t, n, o, r, i, a) {
                var l = s.apply(this, arguments);
                if (l)return l;
                var c = {helpers:r, partials:i, data:a};
                return i[n] = e.compile(t, {data:void 0 !== a}, e), i[n](o, c)
            } : function (t, e) {
                var n = s.apply(this, arguments);
                if (n)return n;
                throw new d("The partial " + e + " could not be compiled when running in runtime-only mode")
            };
            var r = {escapeExpression:u.escapeExpression, invokePartial:n, programs:[], program:function (t, e, n) {
                var o = this.programs[t];
                return n ? o = a(t, e, n) : o || (o = this.programs[t] = a(t, e)), o
            }, merge:function (t, e) {
                var n = t || e;
                return t && e && t !== e && (n = {}, u.extend(n, e), u.extend(n, t)), n
            }, programWithDepth:i, noop:l, compilerInfo:null};
            return function (n, i) {
                i = i || {};
                var a, s, l = i.partial ? i : e;
                i.partial || (a = i.helpers, s = i.partials);
                var c = t.call(r, l, n, a, s, i.data);
                return i.partial || o(r.compilerInfo), c
            }
        }

        function i(t, e, n) {
            var o = Array.prototype.slice.call(arguments, 3), r = function (t, r) {
                return r = r || {}, e.apply(this, [t, r.data || n].concat(o))
            };
            return r.program = t, r.depth = o.length, r
        }

        function a(t, e, n) {
            var o = function (t, o) {
                return o = o || {}, e(t, o.data || n)
            };
            return o.program = t, o.depth = 0, o
        }

        function s(t, e, n, o, r, i) {
            var a = {partial:!0, helpers:o, partials:r, data:i};
            if (void 0 === t)throw new d("The partial " + e + " could not be found");
            return t instanceof Function ? t(n, a) : void 0
        }

        function l() {
            return""
        }

        var c = {}, u = t, d = e, f = n.COMPILER_REVISION, h = n.REVISION_CHANGES;
        return c.template = r, c.programWithDepth = i, c.program = a, c.invokePartial = s, c.noop = l, c
    }(e, n, o), i = function (t, e, n, o, r) {
        "use strict";
        var i, a = t, s = e, l = n, c = o, u = r, d = function () {
            var t = new a.HandlebarsEnvironment;
            return c.extend(t, a), t.SafeString = s, t.Exception = l, t.Utils = c, t.VM = u, t.template = function (e) {
                return u.template(e, t)
            }, t
        }, f = d();
        return f.create = d, i = f
    }(o, t, n, e, r);
    return i
}();
!function () {
    var t = Handlebars.template, e = Handlebars.templates = Handlebars.templates || {};
    e.photo = t(function (t, e, n, o, r) {
        function i() {
            return"should-fill-row"
        }

        this.compilerInfo = [4, ">= 1.0.0"], n = this.merge(n, t.helpers), r = r || {};
        var a, s = "", l = this, c = "function", u = this.escapeExpression;
        return s += '<div class="photo ', a = n["if"].call(e, (a = e && e.photo, null == a || a === !1 ? a : a.fill_width), {hash:{}, inverse:l.noop, fn:l.program(1, i, r), data:r}), (a || 0 === a) && (s += a), s += '" id="photo-' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.photo_key, typeof a === c ? a.apply(e) : a)) + '" data-photo-key="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.photo_key, typeof a === c ? a.apply(e) : a)) + '" data-aspect-ratio="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.aspect_ratio, typeof a === c ? a.apply(e) : a)) + '" data-original-dimensions="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.dimensions, typeof a === c ? a.apply(e) : a)) + '" data-position="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.position, typeof a === c ? a.apply(e) : a)) + '">\n  \n    <div class="photo-upload-progress animated fadeIn" style="display:none;">\n        <hr class="progress-bar" style="width:0%">\n        <span class="label">Uploading Photo</span>\n    </div>\n  \n    <div class="photo-bouncer" data-deferred-src="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.asset_url, typeof a === c ? a.apply(e) : a)) + '" rel="group' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.photo_group_id, typeof a === c ? a.apply(e) : a)) + '"></div>\n  \n    <div class="photo-caption">\n        <h5>Photo caption example</h5>\n    </div>\n\n    <div class="photo-controls">\n    \n        <nav>\n            <ul>\n                <li><a href="#" class="delete-photo-hook">Remove</a></li>\n                <li><a href="#" class="fill-row-hook" rel="tipsy-s" title="This will make the photo bigger and rearrange the layout to make the best use of free space.">Fill Row</a></li>\n            </ul>\n        </nav>    \n    \n    </div>\n    \n    <img class="set-photo" data-deferred-src="' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.asset_url, typeof a === c ? a.apply(e) : a)) + '" rel="group' + u((a = e && e.photo, a = null == a || a === !1 ? a : a.photo_group_id, typeof a === c ? a.apply(e) : a)) + '">\n\n</div>'
    }), e.photo_group = t(function (t, e, n, o, r) {
        function i() {
            return"left-aligned"
        }

        function a() {
            return"full-width-group"
        }

        function s() {
            return"photo-set-group content"
        }

        function l() {
            return'\n    <div class="group-help-tip animated fadeIn">\n        <span href="#" class="group-help-tip-button tour-tip" rel="tipsy-w" title="Group titles and descriptions are optional">?</span>\n    </div>\n    '
        }

        function c() {
            return"content"
        }

        function u() {
            return"\n                <h4>Drop a new photo here to replace the current one</h4>\n                <h5>You can only add one photo</h5>\n                "
        }

        function d() {
            return"\n                <h4>Drop New Photos Here to add more to this set</h4>\n                <h5>You have <span></span> remaining in this set</h5>\n                "
        }

        function f() {
            return"full-width-controls"
        }

        function h() {
            return"photo-set-controls"
        }

        function p() {
            return'\n                <a href="" class="replace-photo-hook">Replace Photo</a>\n                <a href="" class="remove-photo-hook">Remove Photo</a>\n                '
        }

        function g(t) {
            var e, n = "";
            return n += '\n                <a href="" class="add-new-photos-hook">Add New Photos</a>\n                <a href="" class="reorder-photos-hook tour-tip" data-group-id="' + b((e = t && t.group, e = null == e || e === !1 ? e : e.id, typeof e === y ? e.apply(t) : e)) + '">Reorder Photos</a>\n                <a href="" class="remove-photos-hook">Remove Photoset</a>\n                '
        }

        this.compilerInfo = [4, ">= 1.0.0"], n = this.merge(n, t.helpers), r = r || {};
        var m, v = "", y = "function", b = this.escapeExpression, w = this;
        return v += '<div class="group ', m = n["if"].call(e, e && e["left-align"], {hash:{}, inverse:w.noop, fn:w.program(1, i, r), data:r}), (m || 0 === m) && (v += m), v += " ", m = n.unless.call(e, e && e["is-set"], {hash:{}, inverse:w.noop, fn:w.program(3, a, r), data:r}), (m || 0 === m) && (v += m), v += " ", m = n["if"].call(e, e && e["is-set"], {hash:{}, inverse:w.noop, fn:w.program(5, s, r), data:r}), (m || 0 === m) && (v += m), v += '" data-group-id="' + b((m = e && e.group, m = null == m || m === !1 ? m : m.id, typeof m === y ? m.apply(e) : m)) + '" id="photo-group-' + b((m = e && e.group, m = null == m || m === !1 ? m : m.id, typeof m === y ? m.apply(e) : m)) + '" data-group-type="' + b((m = e && e.group, m = null == m || m === !1 ? m : m.group_type, typeof m === y ? m.apply(e) : m)) + '">\n    \n  \n    <div class="alignment-controls animated fadeIn tour-tip" rel="tipsy-w" title="Change text alignment">\n        <a href="#" class="alignment-button"></a>\n    </div>\n    \n    ', m = n["if"].call(e, e && e["show-group-help"], {hash:{}, inverse:w.noop, fn:w.program(7, l, r), data:r}), (m || 0 === m) && (v += m), v += '\n      \n    <div class="position-controls animated fadeIn tour-tip" title="Move this photo group up or down your post" rel="tipsy-e">\n        <a href="#" class="move-group-up" data-direction="up">Move This Group Up</a>\n        <a href="#" class="move-group-down" data-direction="down">Move This Group Down</a>\n    </div>\n\n    <div class="group-details ', m = n.unless.call(e, e && e["is-set"], {hash:{}, inverse:w.noop, fn:w.program(9, c, r), data:r}), (m || 0 === m) && (v += m), v += ' animateHeight">    \n\n        <h2 data-placeholder="Photo Group Title" data-field-name="title">' + b((m = e && e.group, m = null == m || m === !1 ? m : m.title, typeof m === y ? m.apply(e) : m)) + '</h2>\n\n        <div class="group-description" data-placeholder="<p>What are these photos about?</p>" data-field-name="description" data-barley="group_content_' + b((m = e && e.group, m = null == m || m === !1 ? m : m.id, typeof m === y ? m.apply(e) : m)) + '" data-barley-editor="exposure">', m = e && e.group, m = null == m || m === !1 ? m : m.content, m = typeof m === y ? m.apply(e) : m, (m || 0 === m) && (v += m), v += '</div>\n\n    </div>\n\n    <div class="photo-set">\n              \n        <div class="add-new-photos">\n\n            <div class="headings">\n                ', m = n.unless.call(e, e && e["is-set"], {hash:{}, inverse:w.program(13, d, r), fn:w.program(11, u, r), data:r}), (m || 0 === m) && (v += m), v += '\n                \n                <div class="button off-black finish-adding-photos">I\'m Finished</div>\n                \n            </div>\n\n        </div>\n\n        <div class="group-controls ', m = n.unless.call(e, e && e["is-set"], {hash:{}, inverse:w.program(17, h, r), fn:w.program(15, f, r), data:r}), (m || 0 === m) && (v += m), v += '">\n\n            <div class="buttons">\n                ', m = n.unless.call(e, e && e["is-set"], {hash:{}, inverse:w.program(21, g, r), fn:w.program(19, p, r), data:r}), (m || 0 === m) && (v += m), v += '\n            </div>\n\n        </div>                \n\n        <div class="photos-container">\n        </div>\n\n    </div>\n\n</div>\n\n'
    })
}(), function () {
    var t, e, n, o, r, i, a, s, l, c, u, d, f, h, p, g, m, v, y, b, w, x, C, E, S, _, k, T, N, P, R, A, O, D, I, $, L, M, B, F, j, H, W, q, X, z, G, U, V, Y, Q, K, J = [].indexOf || function (t) {
        for (var e = 0, n = this.length; n > e; e++)if (e in this && this[e] === t)return e;
        return-1
    }, Z = {}.hasOwnProperty;
    X = [], U = [], V = null, D = [], n = [], j = !1, M = "ctrl", F = ["meta", "alt", "option", "ctrl", "shift", "cmd"], Q = [], u = {keys:[], count:0}, m = function (t, e) {
        var n;
        return t.filter ? t.filter(e) : function () {
            var o, r, i;
            for (i = [], o = 0, r = t.length; r > o; o++)n = t[o], e(n) && i.push(n);
            return i
        }()
    }, $ = function () {
        return console.log.apply(console, arguments)
    }, d = function (t, e) {
        var n, o, r;
        if (t.length !== e.length)return!1;
        for (o = 0, r = t.length; r > o; o++)if (n = t[o], !(J.call(e, n) >= 0))return!1;
        return!0
    }, f = function (t, e) {
        var n, o, r;
        if (t.length !== e.length)return!1;
        for (n = o = 0, r = t.length; r >= 0 ? r > o : o > r; n = r >= 0 ? ++o : --o)if (t[n] !== e[n])return!1;
        return!0
    }, k = function (t, e) {
        var n, o, r;
        for (o = 0, r = t.length; r > o; o++)if (n = t[o], J.call(e, n) < 0)return!1;
        return!0
    }, T = function (t, e) {
        var n, o, r, i, a;
        for (r = 0, i = 0, a = t.length; a > i; i++) {
            if (o = t[i], n = e.indexOf(o), !(n >= r))return!1;
            r = n
        }
        return!0
    }, H = function (t, e) {
        return(e || keypress.suppress_event_defaults) && !keypress.force_event_defaults && (t.preventDefault ? t.preventDefault() : t.returnValue = !1, t.stopPropagation) ? t.stopPropagation() : void 0
    }, i = function (t) {
        return t.prevent_repeat ? !1 : "function" == typeof t.on_keydown ? !0 : void 0
    }, I = function (t) {
        var e, n, o, r, i;
        for (i = t.keys, o = 0, r = i.length; r > o; o++)if (e = i[o], J.call(D, e) >= 0) {
            n = !0;
            break
        }
        return n
    }, v = function (t, e, n) {
        return"function" == typeof e["on_" + t] && H(n, e["on_" + t].call(e["this"], n, e.count) === !1), "release" === t && (e.count = 0), "keyup" === t ? e.keyup_fired = !0 : void 0
    }, L = function (t, e) {
        var n, o, r;
        for (o = 0, r = X.length; r > o; o++)n = X[o], (n.is_ordered && f(t, n.keys) || !n.is_ordered && d(t, n.keys)) && e(n)
    }, y = function (t, e) {
        var n, o, r;
        for (o = 0, r = X.length; r > o; o++)n = X[o], (n.is_ordered && T(n.keys, t) || !n.is_ordered && k(n.keys, t)) && e(n)
    }, c = function (t) {
        return J.call(D, "cmd") >= 0 && J.call(t, "cmd") < 0 ? !1 : !0
    }, b = function (t) {
        var e, n;
        return e = [], n = m(D, function (e) {
            return e !== t
        }), n.push(t), L(n, function (t) {
            return c(t.keys) ? e.push(t) : void 0
        }), y(n, function (t) {
            return J.call(e, t) >= 0 ? void 0 : !t.is_solitary && c(t.keys) ? e.push(t) : void 0
        }), e
    }, x = function (t) {
        var e, n, o, r;
        for (n = [], o = 0, r = X.length; r > o; o++)e = X[o], e.is_sequence || J.call(e.keys, t) >= 0 && c(e.keys) && n.push(e);
        return n
    }, r = function (t) {
        var e, o, r, i, a, s, l, c, u, d, f, h, p, g, m;
        if (c = !1, l = !0, i = !1, J.call(n, t) >= 0)return!0;
        if (n.length)for (s = u = 0, g = n.length; g >= 0 ? g > u : u > g; s = g >= 0 ? ++u : --u)if (e = n[s], e && e.is_exclusive && t.is_exclusive) {
            if (r = e.keys, !c)for (d = 0, h = r.length; h > d; d++)if (o = r[d], c = !0, J.call(t.keys, o) < 0) {
                c = !1;
                break
            }
            if (l && !c)for (m = t.keys, f = 0, p = m.length; p > f; f++)if (a = m[f], l = !1, J.call(r, a) < 0) {
                l = !0;
                break
            }
            c && (i ? G(n.splice(s, 1)) : (G(n.splice(s, 1, t)), i = !0), l = !1)
        }
        return l && n.unshift(t), c || l
    }, z = function (t) {
        var e, o, r, i;
        for (o = r = 0, i = n.length; i >= 0 ? i > r : r > i; o = i >= 0 ? ++r : --r)if (e = n[o], e === t) {
            G(n.splice(o, 1));
            break
        }
    }, G = function (t) {
        return t ? (t.count = null, t.keyup_fired = null) : void 0
    }, o = function (t, e) {
        var n, o, r, i;
        if (U.push(t), o = w(), o.length) {
            for (r = 0, i = o.length; i > r; r++)n = o[r], H(e, n.prevent_default);
            V && clearTimeout(V), keypress.sequence_delay > -1 && (V = setTimeout(function () {
                return U = []
            }, keypress.sequence_delay))
        } else U = []
    }, w = function () {
        var t, e, n, o, r, i, a, s, l, c, u, d;
        for (r = [], a = 0, c = X.length; c > a; a++)for (t = X[a], n = s = 1, u = U.length; u >= 1 ? u >= s : s >= u; n = u >= 1 ? ++s : --s)if (i = U.slice(-n), t.is_sequence && (!(J.call(t.keys, "shift") < 0) || (i = m(i, function (t) {
            return"shift" !== t
        }), i.length))) {
            for (e = l = 0, d = i.length; d >= 0 ? d > l : l > d; e = d >= 0 ? ++l : --l) {
                if (t.keys[e] !== i[e]) {
                    o = !1;
                    break
                }
                o = !0
            }
            o && r.push(t)
        }
        return r
    }, C = function (t) {
        var e, n, o, r, i, a, s, l, c, u, d, f;
        for (s = 0, u = X.length; u > s; s++)if (e = X[s], e.is_sequence) {
            for (o = l = 1, d = U.length; d >= 1 ? d >= l : l >= d; o = d >= 1 ? ++l : --l)if (a = m(U,function (t) {
                return J.call(e.keys, "shift") >= 0 ? !0 : "shift" !== t
            }).slice(-o), e.keys.length === a.length)for (n = c = 0, f = a.length; f >= 0 ? f > c : c > f; n = f >= 0 ? ++c : --c)if (i = a[n], !(J.call(e.keys, "shift") < 0 && "shift" === i || "shift" === t && J.call(e.keys, "shift") < 0)) {
                if (e.keys[n] !== i) {
                    r = !1;
                    break
                }
                r = !0
            }
            if (r)return e
        }
        return!1
    }, p = function (t, e) {
        var n;
        return e.shiftKey ? (n = O[t], null != n ? n : !1) : !1
    }, E = function (t, e, n) {
        var o;
        return J.call(t.keys, e) < 0 ? !1 : (H(n, t && t.prevent_default), J.call(D, e) >= 0 && !i(t) ? !1 : (o = r(t, e), t.keyup_fired = !1, t.is_counting && "function" == typeof t.on_keydown && (t.count += 1), o ? v("keydown", t, n) : void 0))
    }, N = function (t, e) {
        var n, r, i, a, s, l, c, u, d, f, h, g, m, y, w;
        d = p(t, e), d && (t = d), o(t, e), u = C(t), u && v("keydown", u, e);
        for (s in B)i = B[s], e[i] && (s === t || J.call(D, s) >= 0 || D.push(s));
        for (s in B)if (i = B[s], s !== t && J.call(D, s) >= 0 && !e[i])for (a = f = 0, w = D.length; w >= 0 ? w > f : f > w; a = w >= 0 ? ++f : --f)D[a] === s && D.splice(a, 1);
        for (r = b(t), h = 0, m = r.length; m > h; h++)n = r[h], E(n, t, e);
        if (c = x(t), c.length)for (g = 0, y = c.length; y > g; g++)l = c[g], H(e, l.prevent_default);
        J.call(D, t) < 0 && D.push(t)
    }, S = function (t, e, n) {
        var o, r;
        r = I(t), t.keyup_fired || (o = D.slice(), o.push(n), (!t.is_solitary || d(o, t.keys)) && (v("keyup", t, e), t.is_counting && "function" == typeof t.on_keyup && "function" != typeof t.on_keydown && (t.count += 1))), r || (v("release", t, e), z(t))
    }, P = function (t, e) {
        var o, r, i, a, s, l, c, u, d, f, h, g, m, y, b, w, x;
        if (u = t, c = p(t, e), c && (t = c), c = O[u], e.shiftKey ? c && J.call(D, c) >= 0 || (t = u) : u && J.call(D, u) >= 0 || (t = c), l = C(t), l && v("keyup", l, e), J.call(D, t) < 0)return!1;
        for (s = d = 0, w = D.length; w >= 0 ? w > d : d > w; s = w >= 0 ? ++d : --d)if ((x = D[s]) === t || x === c || x === u) {
            D.splice(s, 1);
            break
        }
        for (r = n.length, a = [], f = 0, m = n.length; m > f; f++)o = n[f], J.call(o.keys, t) >= 0 && a.push(o);
        for (h = 0, y = a.length; y > h; h++)i = a[h], S(i, e, t);
        if (r > 1)for (g = 0, b = n.length; b > g; g++)o = n[g], void 0 === o || J.call(a, o) >= 0 || I(o) || z(o)
    }, q = function (t, e) {
        var n;
        if (j)return D.length && (D = []), void 0;
        if ((e || D.length) && (n = h(t.keyCode)))return e ? N(n, t) : P(n, t)
    }, Y = function (t) {
        var e, n, o, r;
        for (r = [], e = n = 0, o = X.length; o >= 0 ? o > n : n > o; e = o >= 0 ? ++n : --n) {
            if (t === X[e]) {
                X.splice(e, 1);
                break
            }
            r.push(void 0)
        }
        return r
    }, K = function (t) {
        var e, n, o, r, i, a, s, l, c, u, d, f;
        for (t.keys.length || $("You're trying to bind a combo with no keys."), n = a = 0, d = t.keys.length; d >= 0 ? d > a : a > d; n = d >= 0 ? ++a : --a)o = t.keys[n], e = R[o], e && (o = t.keys[n] = e), "meta" === o && t.keys.splice(n, 1, M), "cmd" === o && $('Warning: use the "meta" key rather than "cmd" for Windows compatibility');
        for (f = t.keys, s = 0, c = f.length; c > s; s++)if (o = f[s], J.call(Q, o) < 0)return $('Do not recognize the key "' + o + '"'), !1;
        if (J.call(t.keys, "meta") >= 0 || J.call(t.keys, "cmd") >= 0) {
            for (i = t.keys.slice(), l = 0, u = F.length; u > l; l++)r = F[l], (n = i.indexOf(r)) > -1 && i.splice(n, 1);
            if (i.length > 1)return $("META and CMD key combos cannot have more than 1 non-modifier keys", t, i), !0
        }
        return!0
    }, g = function () {
        -1 !== navigator.userAgent.indexOf("Mac OS X") && (M = "cmd")
    }, s = function (t) {
        var e;
        return J.call(D, "cmd") >= 0 && "cmd" !== (e = h(t.keyCode)) && "shift" !== e && "alt" !== e && "caps" !== e && "tab" !== e ? q(t, !1) : void 0
    }, l = function () {
        -1 !== navigator.userAgent.indexOf("Opera") && (A["17"] = "cmd")
    }, a = function () {
        var t;
        return t = function (t, e, n) {
            return t.addEventListener ? t.addEventListener(e, n) : t.attachEvent ? t.attachEvent("on" + e, n) : void 0
        }, t(document.body, "keydown", function (t) {
            return t = t || window.event, q(t, !0), s(t)
        }), t(document.body, "keyup", function (t) {
            return t = t || window.event, q(t, !1)
        }), t(window, "blur", function () {
            var t, e, n, o;
            for (e = 0, n = D.length; n > e; e++)t = D[e], P(t, {});
            return D = [], o = []
        })
    }, _ = function () {
        return g(), l()
    }, window.keypress = {}, keypress.force_event_defaults = !1, keypress.suppress_event_defaults = !1, keypress.sequence_delay = 800, keypress.get_registered_combos = function () {
        return X
    }, keypress.reset = function () {
        X = []
    }, keypress.combo = function (t, e, n) {
        return null == n && (n = !1), keypress.register_combo({keys:t, on_keydown:e, prevent_default:n})
    }, keypress.counting_combo = function (t, e, n) {
        return null == n && (n = !1), keypress.register_combo({keys:t, is_counting:!0, is_ordered:!0, on_keydown:e, prevent_default:n})
    }, keypress.sequence_combo = function (t, e, n) {
        return null == n && (n = !1), keypress.register_combo({keys:t, on_keydown:e, is_sequence:!0, prevent_default:n})
    }, keypress.register_combo = function (t) {
        var e, n;
        "string" == typeof t.keys && (t.keys = t.keys.split(" "));
        for (e in u)Z.call(u, e) && (n = u[e], null == t[e] && (t[e] = n));
        return K(t) ? (X.push(t), !0) : void 0
    }, keypress.register_many = function (t) {
        var e, n, o, r;
        for (r = [], n = 0, o = t.length; o > n; n++)e = t[n], r.push(keypress.register_combo(e));
        return r
    }, keypress.unregister_combo = function (t) {
        var e, n, o, r;
        if (!t)return!1;
        if (t.keys)return Y(t);
        for (r = [], n = 0, o = X.length; o > n; n++)e = X[n], e && (d(keys, e.keys) ? r.push(Y(e)) : r.push(void 0));
        return r
    }, keypress.unregister_many = function (t) {
        var e, n, o, r;
        for (r = [], n = 0, o = t.length; o > n; n++)e = t[n], r.push(keypress.unregister_combo(e));
        return r
    }, keypress.listen = function () {
        return j = !1
    }, keypress.stop_listening = function () {
        return j = !0
    }, h = function (t) {
        return A[t]
    }, B = {cmd:"metaKey", ctrl:"ctrlKey", shift:"shiftKey", alt:"altKey"}, R = {escape:"esc", control:"ctrl", command:"cmd", "break":"pause", windows:"cmd", option:"alt", caps_lock:"caps", apostrophe:"'", semicolon:";", tilde:"~", accent:"`", scroll_lock:"scroll", num_lock:"num"}, O = {"/":"?", ".":">", ",":"<", "'":'"', ";":":", "[":"{", "]":"}", "\\":"|", "`":"~", "=":"+", "-":"_", 1:"!", 2:"@", 3:"#", 4:"$", 5:"%", 6:"^", 7:"&", 8:"*", 9:"(", 0:")"}, A = {0:"\\", 8:"backspace", 9:"tab", 12:"num", 13:"enter", 16:"shift", 17:"ctrl", 18:"alt", 19:"pause", 20:"caps", 27:"escape", 32:"space", 33:"pageup", 34:"pagedown", 35:"end", 36:"home", 37:"left", 38:"up", 39:"right", 40:"down", 44:"print", 45:"insert", 46:"delete", 48:"0", 49:"1", 50:"2", 51:"3", 52:"4", 53:"5", 54:"6", 55:"7", 56:"8", 57:"9", 65:"a", 66:"b", 67:"c", 68:"d", 69:"e", 70:"f", 71:"g", 72:"h", 73:"i", 74:"j", 75:"k", 76:"l", 77:"m", 78:"n", 79:"o", 80:"p", 81:"q", 82:"r", 83:"s", 84:"t", 85:"u", 86:"v", 87:"w", 88:"x", 89:"y", 90:"z", 91:"cmd", 92:"cmd", 93:"cmd", 96:"num_0", 97:"num_1", 98:"num_2", 99:"num_3", 100:"num_4", 101:"num_5", 102:"num_6", 103:"num_7", 104:"num_8", 105:"num_9", 106:"num_multiply", 107:"num_add", 108:"num_enter", 109:"num_subtract", 110:"num_decimal", 111:"num_divide", 124:"print", 144:"num", 145:"scroll", 186:";", 187:"=", 188:",", 189:"-", 190:".", 191:"/", 192:"`", 219:"[", 220:"\\", 221:"]", 222:"'", 223:"`", 224:"cmd", 57392:"ctrl", 63289:"num"};
    for (e in A)t = A[e], Q.push(t);
    for (e in O)t = O[e], Q.push(t);
    _(), W = function (t) {
        return(document.attachEvent ? "complete" === document.readyState : "loading" !== document.readyState) ? t() : setTimeout(function () {
            return W(t)
        }, 9)
    }, W(a)
}.call(this), function (t, e) {
    t.glisse = function (n, o) {
        var r, i, a = this, s = t(n), l = {dataName:"data-glisse-big", speed:300, changeSpeed:1e3, effect:"bounce", mobile:!1, fullscreen:!1, disablindRightClick:!1, parent:null}, c = !1, u = {}, d = [], f = function (t) {
            return(t.attr(a.settings.dataName) || t.attr("src")) + "&q=95&w=1400"
        };
        a.settings = {}, a.els = {}, a.init = function () {
            a.settings = t.extend({}, l, o), i = s.attr("rel") || null, a.settings.mobile = !!navigator.userAgent.match(/iPhone|iPod|iPad|Android/i), s.on("click", function () {
                r = f(s), "undefined" == typeof s.attr("data-pending") && (window.location.hash = s.closest(".photo").attr("id").replace(/photo-/, "view:"), keypress.stop_listening(), p(), y(), m(r), b(), w(), h(), t(e).keydown(function (t) {
                    "27" === t.keyCode.toString() && g(), "39" === t.keyCode.toString() && v("next"), "37" === t.keyCode.toString() && v("prev")
                }), a.settings.disablindRightClick && a.els.content.on("contextmenu", function () {
                    return!1
                }), a.settings.mobile && (u = {touching:!1, nx:0, oX:0, scrollX:null}, e.ontouchmove = e.ontouchstart = e.ontouchend = _))
            })
        };
        var h = function () {
            return
        }, p = function () {
            s.addClass("active");
            var n = C("transition") + "transition", o = "opacity " + a.settings.speed + "ms ease, " + C("transform") + "transform " + a.settings.speed + "ms ease";
            if (a.els.wrapper = t(e.createElement("div")).attr("id", "glisse-wrapper"), a.els.overlay = t(e.createElement("div")).attr("id", "glisse-overlay").css(n, o), a.els.spinner = t(e.createElement("div")).attr("id", "glisse-spinner"), a.els.close = t(e.createElement("span")).attr("id", "glisse-close").css(n, o), a.els.content = t(e.createElement("div")).attr("id", "glisse-overlay-content").css(n, o).css(C("transform") + "transform", "scale(0.9)"), a.els.controls = t(e.createElement("div")).attr("id", "glisse-controls").css(n, o), a.els.controlNext = t(e.createElement("span")).attr("class", "glisse-next").append(t(e.createElement("a")).html("&#62;").attr("href", "#")), a.els.controlLegend = t(e.createElement("span")).attr("class", "glisse-legend"), a.els.controlPrev = t(e.createElement("span")).attr("class", "glisse-prev").append(t(e.createElement("a")).html("&#60;").attr("href", "#")), a.els.overlay.append(a.els.spinner), a.els.controls.append(a.els.controlNext, a.els.controlLegend, a.els.controlPrev), a.els.wrapper.append(a.els.overlay, a.els.close, a.els.content, a.els.controls), t("body").append(a.els.wrapper), E.observe("glisse-overlay", function () {
                a.els.overlay.css("opacity", 1)
            }), E.observe("glisse-close", function () {
                a.els.close.css("opacity", 1)
            }), E.observe("glisse-controls", function () {
                a.els.controls.css("opacity", 1)
            }), a.els.controls.delegate("a", "click", function (e) {
                e.preventDefault();
                var n = t(this).parent().hasClass("glisse-next") ? "next" : "prev";
                v(n)
            }), a.els.overlay.on("click", function () {
                g()
            }), a.els.content.on("click", function () {
                g()
            }), a.els.close.on("click", function () {
                g()
            }), t("#post-controls a.edit-post").on("click", function () {
                g()
            }), a.settings.fullscreen) {
                var r = e.documentElement;
                r.requestFullscreen ? r.requestFullscreen() : r.mozRequestFullScreen ? r.mozRequestFullScreen() : r.webkitRequestFullScreen && r.webkitRequestFullScreen()
            }
        }, g = function () {
            window.location.hash = "_", t("#post.edit-mode").length || keypress.listen(), a.els.content.css({opacity:0}).css(C("transform") + "transform", "scale(1.02)"), a.els.overlay.css({opacity:0}), a.els.close.css({opacity:0}), a.els.controls.css({opacity:0}), setTimeout(function () {
                a.els.content.remove(), a.els.overlay.remove(), a.els.close.remove(), a.els.controls.remove(), a.els.wrapper.remove(), t("#glisse-transition-css").remove()
            }, a.settings.speed), s.removeClass("active"), e.ontouchmove = function () {
                return!0
            }, e.ontouchstart = function () {
                return!0
            }, e.ontouchend = function () {
                return!0
            }, t(e).unbind("keydown"), a.settings.fullscreen && (e.exitFullscreen ? e.exitFullscreen() : e.mozCancelFullScreen ? e.mozCancelFullScreen() : e.webkitCancelFullScreen && e.webkitCancelFullScreen())
        }, m = function (e) {
            x(!0);
            var n = t("<img/>", {src:e}).appendTo(a.els.content);
            a.els.content.css({backgroundImage:'url("' + e + '")'}), n.load(function () {
                n.remove(), x(!1), a.els.content.css({visibility:"visible", opacity:1}).css(C("transform") + "transform", "scale(1)")
            })
        }, v = function (e) {
            var n = t(".photo-bouncer.active[rel=" + i + "]"), o = t(".photo-bouncer[rel=" + i + "]").index(n), s = t(".photo-bouncer[rel=" + i + "]").length, l = !0;
            if ((0 === o && "prev" === e || o === s - 1 && "next" === e) && (l = !1), l && c === !1) {
                c = !0;
                var u = "next" === e ? t(".photo-bouncer[rel=" + i + "]").eq(o + 1) : t(".photo-bouncer[rel=" + i + "]").eq(o - 1);
                if (a.settings.mobile)"next" !== e ? a.els.content.css(C("transform") + "transform", "translateX(2000px)") : a.els.content.css(C("transform") + "transform", "translateX(-2000px)"); else {
                    a.els.content.addClass("glisse-transitionOut-" + e);
                    var h = C("transition") + "transition", p = "opacity " + a.settings.speed + "ms ease, " + C("transform") + "transform " + a.settings.speed + "ms ease";
                    a.els.content.css(h, "")
                }
                r = f(u), -1 === t.inArray(r, d) && x(!0), n.removeClass("active"), u.addClass("active"), b(), w(), setTimeout(function () {
                    a.settings.mobile && a.els.content.css(C("transform") + "transform", "translateX(0px)").css("display", "none");
                    var n = t("<img/>", {src:r}).appendTo(a.els.content);
                    a.els.content.css({backgroundImage:'url("' + r + '")'}), n.load(function () {
                        n.remove(), -1 === t.inArray(r, d) && x(!1), a.settings.mobile && a.els.content.css("display", "block"), a.els.content.removeClass("glisse-transitionOut-" + e).addClass("glisse-transitionIn-" + e), setTimeout(function () {
                            a.els.content.removeClass("glisse-transitionIn-" + e).css(h, p), c = !1
                        }, a.settings.changeSpeed)
                    })
                }, a.settings.changeSpeed)
            } else l === !1 && c === !1 && (a.settings.mobile && a.els.content.css(C("transform") + "transform", "translateX(0px)"), a.els.content.addClass("shake"), setTimeout(function () {
                a.els.content.removeClass("shake")
            }, 600))
        }, y = function () {
            var n = C("transform"), o = C("animation"), r = [];
            switch (S(a.settings.effect) || (a.settings.effect = "bounce"), a.settings.effect) {
                case"bounce":
                    r = ["@" + o + "keyframes outLeft {", "0% { " + n + "transform: translateX(0);}", "20% { opacity: 1;" + n + "transform: translateX(20px);}", "100% { opacity: 0;" + n + "transform: translateX(-2000px);}", "}", "@" + o + "keyframes inLeft {", "0% {opacity: 0;" + n + "transform: translateX(-2000px);}", "60% {opacity: 1;" + n + "transform: translateX(30px);}", "80% {" + n + "transform: translateX(-10px);}", "100% {" + n + "transform: translateX(0);}", "}", "@" + o + "keyframes outRight {", "0% {" + n + "transform: translateX(0);}", "20% {opacity: 1;" + n + "transform: translateX(-20px);}", "100% {opacity: 0;" + n + "transform: translateX(2000px);}", "}", "@" + o + "keyframes inRight {", "0% {opacity: 0;" + n + "transform: translateX(2000px);}", "60% {opacity: 1;" + n + "transform: translateX(-30px);}", "80% {" + n + "transform: translateX(10px);}", "100% {" + n + "transform: translateX(0);}", "}"].join("");
                    break;
                case"fadeBig":
                    r = ["@" + o + "keyframes outLeft {", "0% { opacity: 1;" + n + "transform: translateX(0);}", "100% {opacity: 0;" + n + "transform: translateX(-2000px);}", "}", "@" + o + "keyframes inLeft {", "0% { opacity: 0;" + n + "transform: translateX(-2000px);}", "100% {opacity: 1;" + n + "transform: translateX(0);}", "}", "@" + o + "keyframes outRight {", "0% { opacity: 1;" + n + "transform: translateX(0);}", "100% {opacity: 0;" + n + "transform: translateX(2000px);}", "}", "@" + o + "keyframes inRight {", "0% { opacity: 0;" + n + "transform: translateX(2000px);}", "100% {opacity: 1;" + n + "transform: translateX(0);}", "}"].join("");
                    break;
                case"fade":
                    r = ["@" + o + "keyframes outLeft {", "0% { opacity: 1;" + n + "transform: translateX(0);}", "100% {opacity: 0;" + n + "transform: translateX(-200px);}", "}", "@" + o + "keyframes inLeft {", "0% { opacity: 0;" + n + "transform: translateX(-200px);}", "100% {opacity: 1;" + n + "transform: translateX(0);}", "}", "@" + o + "keyframes outRight {", "0% { opacity: 1;" + n + "transform: translateX(0);}", "100% {opacity: 0;" + n + "transform: translateX(200px);}", "}", "@" + o + "keyframes inRight {", "0% { opacity: 0;" + n + "transform: translateX(200px);}", "100% {opacity: 1;" + n + "transform: translateX(0);}", "}"].join("");
                    break;
                case"roll":
                    r = ["@" + o + "keyframes outLeft {", "0% { opacity: 1;" + n + "transform: translateX(0px) rotate(0deg);}", "100% {opacity: 0;" + n + "transform: translateX(-100%) rotate(-120deg);}", "}", "@" + o + "keyframes inLeft {", "0% { opacity: 0;" + n + "transform: translateX(-100%) rotate(-120deg);}", "100% {opacity: 1;" + n + "transform:  translateX(0px) rotate(0deg);}", "}", "@" + o + "keyframes outRight {", "0% { opacity: 1;" + n + "transform:translateX(0px) rotate(0deg);}", "100% {opacity: 0;" + n + "transform:translateX(100%) rotate(120deg);}", "}", "@" + o + "keyframes inRight {", "0% { opacity: 0;" + n + "transform: translateX(100%) rotate(120deg);}", "100% {opacity: 1;" + n + "transform:  translateX(0px) rotate(0deg);}", "}"].join("");
                    break;
                case"rotate":
                    r = ["@" + o + "keyframes outRight {", "0% { opacity: 1;" + n + "transform: rotate(0deg);" + n + "transform-origin:left bottom;}", "100% {opacity: 0;" + n + "transform: rotate(-90deg);" + n + "transform-origin:left bottom;}", "}", "@" + o + "keyframes inLeft {", "0% { opacity: 0;" + n + "transform: rotate(90deg);" + n + "transform-origin:left bottom;}", "100% {opacity: 1;" + n + "transform: rotate(0deg);" + n + "transform-origin:left bottom;}", "}", "@" + o + "keyframes outLeft {", "0% { opacity: 1;" + n + "transform: rotate(0deg);" + n + "transform-origin:right bottom;}", "100% {opacity: 0;" + n + "transform: rotate(90deg);" + n + "transform-origin:right bottom;}", "}", "@" + o + "keyframes inRight {", "0% { opacity: 0;" + n + "transform: rotate(-90deg);" + n + "transform-origin:right bottom;}", "100% {opacity: 1;" + n + "transform: rotate(0deg);" + n + "transform-origin:right bottom;}", "}"].join("");
                    break;
                case"flipX":
                    r = ["@" + o + "keyframes outLeft {", "0% {" + n + "transform: perspective(400px) rotateX(0deg);opacity: 1;}", "100% {" + n + "transform: perspective(400px) rotateX(90deg);opacity: 0;}", "}", "@" + o + "keyframes inLeft {", "0% {" + n + "transform: perspective(400px) rotateX(90deg);opacity: 0;}", "40% {" + n + "transform: perspective(400px) rotateX(-10deg);}", "70% {" + n + "transform: perspective(400px) rotateX(10deg);}", "100% {" + n + "transform: perspective(400px) rotateX(0deg);opacity: 1;}", "}", "@" + o + "keyframes outRight {", "0% {" + n + "transform: perspective(400px) rotateX(0deg);opacity: 1;}", "100% {" + n + "transform: perspective(400px) rotateX(90deg);opacity: 0;}", "}", "@" + o + "keyframes inRight {", "0% {" + n + "transform: perspective(400px) rotateX(90deg);opacity: 0;}", "40% {" + n + "transform: perspective(400px) rotateX(-10deg);}", "70% {" + n + "transform: perspective(400px) rotateX(10deg);}", "100% {" + n + "transform: perspective(400px) rotateX(0deg);opacity: 1;}", "}"].join("");
                    break;
                case"flipY":
                    r = ["@" + o + "keyframes outLeft {", "0% {" + n + "transform: perspective(400px) rotateY(0deg);opacity: 1;}", "100% {" + n + "transform: perspective(400px) rotateY(90deg);opacity: 0;}", "}", "@" + o + "keyframes inLeft {", "0% {" + n + "transform: perspective(400px) rotateY(90deg);opacity: 0;}", "40% {" + n + "transform: perspective(400px) rotateY(-10deg);}", "70% {" + n + "transform: perspective(400px) rotateY(10deg);}", "100% {" + n + "transform: perspective(400px) rotateY(0deg);opacity: 1;}", "}", "@" + o + "keyframes outRight {", "0% {" + n + "transform: perspective(400px) rotateY(0deg);opacity: 1;}", "100% {" + n + "transform: perspective(400px) rotateY(-90deg);opacity: 0;}", "}", "@" + o + "keyframes inRight {", "0% {" + n + "transform: perspective(400px) rotateY(90deg);opacity: 0;}", "40% {" + n + "transform: perspective(400px) rotateY(-10deg);}", "70% {" + n + "transform: perspective(400px) rotateY(10deg);}", "100% {" + n + "transform: perspective(400px) rotateY(0deg);opacity: 1;}", "}"].join("")
            }
            var i = [".glisse-transitionOut-next {", o + "animation: " + a.settings.changeSpeed + "ms ease;", o + "animation-name: outLeft;", o + "animation-fill-mode: both;", "}", ".glisse-transitionIn-prev {", o + "animation: " + a.settings.changeSpeed + "ms ease;", o + "animation-name: inLeft;", o + "animation-fill-mode: both;", "}", ".glisse-transitionOut-prev {", o + "animation: " + a.settings.changeSpeed + "ms ease;", o + "animation-name: outRight;", o + "animation-fill-mode: both;", "}", ".glisse-transitionIn-next {", o + "animation: " + a.settings.changeSpeed + "ms ease;", o + "animation-name: inRight;", o + "animation-fill-mode: both;", "}"].join("");
            e.getElementById("glisse-css") ? t("#glisse-css").html(r + i) : t('<style type="text/css" id="glisse-css">' + r + i + "</style>").appendTo("head")
        }, b = function () {
            var e = t("img[" + a.settings.dataName + '="' + r + '"]'), n = a.settings.parent ? e.closest(a.settings.parent) : e.parent();
            n.next().find("img[rel=" + i + "]").length ? a.els.controls.find(".glisse-next").removeClass("ended") : a.els.controls.find(".glisse-next").addClass("ended"), n.prev().find("img[rel=" + i + "]").length ? a.els.controls.find(".glisse-prev").removeClass("ended") : a.els.controls.find(".glisse-prev").addClass("ended")
        }, w = function () {
            var e = a.els.controls.find(".glisse-legend"), n = t("img[" + a.settings.dataName + '="' + r + '"]'), o = n.attr("title");
            e.html(o ? o : "")
        }, x = function (t) {
            a.els.overlay.toggleClass("loading", t)
        }, C = function (t) {
            for (var n = ["Moz", "Khtml", "Webkit", "O", "ms"], o = e.createElement("div"), r = t.charAt(0).toUpperCase() + t.slice(1), i = n.length; i--;)if (n[i] + r in o.style)return"-" + n[i].toLowerCase() + "-";
            return!1
        }, E = function () {
            return{observe:function (t, n) {
                var o = setInterval(function () {
                    e.getElementById(t) && (n(e.getElementById(t)), clearInterval(o))
                }, 60)
            }}
        }(), S = function (e) {
            return!!~t.inArray(e, ["bounce", "fadeBig", "fade", "roll", "rotate", "flipX", "flipY"])
        }, _ = function (t) {
            if ("touchstart" == t.type) {
                if (u.touching = !0, 1 == t.touches.length) {
                    a.els.content.css(C("transition") + "transition", "");
                    var e = t.touches[0];
                    e.target.onclick && e.target.onclick(), u.oX = e.pageX, u.nX = 0, u.scrollX = 0
                }
            } else if ("touchmove" == t.type) {
                if (t.preventDefault(), u.scrollX = null, 1 == t.touches.length) {
                    var e = t.touches[0];
                    u.nX = e.pageX, u.oX > u.nX ? u.scrollX = -(u.oX - u.nX) : u.nX > u.oX && (u.scrollX = u.nX - u.oX), a.els.content.css(C("transform") + "transform", "translateX(" + u.scrollX + "px)")
                }
            } else if ("touchend" == t.type || "touchcancel" == t.type) {
                u.touching = !1;
                var n = C("transition") + "transition", o = "opacity " + a.settings.speed + "ms ease, " + C("transform") + "transform " + a.settings.speed + "ms ease";
                a.els.content.css(n, o), u.scrollX > 140 ? v("prev") : u.scrollX < -140 ? v("next") : a.els.content.css(C("transform") + "transform", "translateX(0px)")
            }
        };
        a.changeEffect = function (t) {
            S(t) && (a.settings.effect = t, y())
        }, a.init()
    }, t.fn.glisse = function (e) {
        return this.each(function () {
            if (void 0 === t(this).data("glisse")) {
                var n = new t.glisse(this, e);
                t(this).data("glisse", n)
            }
        })
    }
}(jQuery, window.document);
var indexOfValue = _.indexOf;
_.mixin({indexOf:function (t, e) {
    if (!_.isFunction(e))return indexOfValue(t, e);
    for (var n = 0; n < t.length; n++)if (e(t[n]))return n;
    return-1
}}), function () {
    this.Partition = function () {
        function t() {
        }

        function e(t) {
            return $(t).hasClass("should-fill-row")
        }

        return t.partition = function (n, o) {
            return _.some(n, e) ? t.fillWidths(n, o) : t.fillRows(n, o)
        }, t.fillRows = function (t, e) {
            for (var n, o = 0, r = t.length, i = []; r > o;)n = Math.ceil((r - o) / e--), i.push(t.slice(o, o += n));
            return i
        }, t.takeRow = function (t) {
            var n;
            return n = 0 == t.length ? 0 : e(_.first(t)) ? 1 : _.some(t, e) ? _.indexOf(t, e) : t.length, _.first(t, n)
        }, t.partitionFullWidths = function (e) {
            for (var n = []; e.length > 0;)n.push(t.takeRow(e)), e = _.rest(e, _.last(n).length);
            return n
        }, t.fillWidths = function (e, n) {
            var o = t.partitionFullWidths(e), r = _.filter(o,function (t) {
                return 1 == t.length
            }).length, i = _.indexOf(o, function (t) {
                return t.length > 5
            });
            if (1 == o.length)return t.fillRows(e, n);
            if (r == o.length)return o;
            if (i >= 0)return t.partitionSingleRow(o, i, n - o.length + 1);
            if (o.length >= n)return o;
            var a = n - o.length, s = _.sortBy(_.reject(_.map(o, function (t, e) {
                return t.length > 1 ? e : null
            }), function (t) {
                return _.isNull(t)
            }), function (t) {
                return-t
            });
            return 1 == s.length ? o = t.partitionSingleRow(o, _.first(s), a + 1) : _.each(s, function (e) {
                a > 0 && (o = t.partitionSingleRow(o, e), a--)
            }), o
        }, t.partitionSingleRow = function (e, n, o) {
            var r, i, a;
            return o = _.isNumber(o) && o > 2 ? o : 2, r = 0 >= n ? [] : _.first(e, n), i = t.fillRows(e[n], o), a = _.rest(e, n + 1), _.flatten([r, i, a], !0)
        }, t
    }.call(this)
}.call(this), document.addEventListener("touchstart", function () {
}, !1), Array.prototype.last || (Array.prototype.last = function () {
    return this[this.length - 1]
}), jQuery.fn.selectText = function () {
    var t = this;
    requestAnimationFrame(function () {
        var e = $(t)[0], n = document.createRange();
        n.selectNodeContents(e);
        var o = window.getSelection();
        o.removeAllRanges(), o.addRange(n)
    })
}, _.debounce = function (t, e, n) {
    var o;
    return function () {
        var r = this, i = arguments, a = function () {
            o = null, n || t.apply(r, i)
        }, s = n && !o;
        clearTimeout(o), o = setTimeout(a, e), s && t.apply(r, i)
    }
};
var Utils = {htmlEntityMap:{"&":"&amp;", "<":"&lt;", ">":"&gt;", '"':"&quot;"}};
Utils.randomStr = function (t) {
    for (var e = ""; e.length < t;)e += Math.random().toString(36).substr(2, t);
    return e.substr(0, t)
}, Utils.decodeHtmlEntities = function (t) {
    return t ? (_.each(Utils.htmlEntityMap, function (e, n) {
        var o = new RegExp(e, "g");
        t = t.replace(o, n)
    }), t) : t
}, Utils.inViewport = function (t) {
    return withinViewport(t[0], {top:-400, bottom:-400})
};
var Exposure = {isLoading:!1, isMobile:!1, onCustomDomain:!window.location.hostname.match(/(lvh\.me|exposure\.so|localhost|exposure-staging\.com)/), features:{staff:"staff" === $("meta[name=user-bucket]").attr("content"), editor:!1, mobileDeref:!1}};
if ((bowser.chrome || bowser.firefox || bowser.safari) && "undefined" != typeof FileReader && (Exposure.features.editor = !0), (bowser.ios || bowser.mobile) && (Exposure.features.editor = !1, Exposure.isMobile = !0), Exposure.features.editor || ($("#create-new-post-link").bind("click", function (t) {
    return t.preventDefault(), Exposure.showMessage("Sorry! Post creation is only supported in Chrome, Safari 6.0+, and Firefox right now."), !1
}), $("#post-controls").remove(), $(".post-controls .edit-post-hook").remove(), $(".edit-profile-hook").parent().remove()), Exposure.loading = function (t) {
    if (!(t == Exposure.isLoading || !t && "undefined" != typeof PhotoGroups && PhotoGroups.photosToUpload > 0)) {
        var e = $(".loading-indicator.bottom");
        t === !0 ? (e.show(), $("body").addClass("app-loading")) : (e.hide(), $("body").removeClass("app-loading")), Exposure.isLoading = t
    }
}, Exposure.showMessage = function (t) {
    var e = $("#app-message");
    e.removeClass("animated fadeOut").addClass("animated bounceIn").find(".cnt").html(t), e.show(), setTimeout(function () {
        $("#app-message").click()
    }, 3333)
}, $(window).scroll(function () {
    var t = $(this).scrollTop();
    Exposure.isMobile || $(".author-meta, .upload-change-cover-image,.scroll-hint").css("opacity", +(9 >= t)), $(".cover-hotspot").css("opacity", +(t >= 10))
}), $("#logo-menu .logo").bind("click", function (t) {
    return $(this).find("a").length ? void 0 : (t.preventDefault(), $.scrollTo(0, 400), !1)
}), $(window).bind("resize", function () {
    $(".user-details h1.user-fullname, .user-bio, .post-titles h1, .post-titles h2, .group-details h2, .group-description, p, h1, h2, h3, .post-footnotes *").css("z-index", 1)
}), $("body").on("click", "#app-message", function () {
    $(this).removeClass("animated bounceIn").addClass("animated fadeOut")
}), $(document).tipsy({fade:!0, offset:14, live:"[rel=tipsy-w], a[data-tipsy-w]", gravity:"w"}), $(document).tipsy({fade:!0, offset:14, live:"[rel=tipsy-e], a[data-tipsy-e]", gravity:"e"}), $(document).tipsy({fade:!0, offset:25, live:"[rel=tipsy-s], a[data-tipsy-s]", gravity:"s"}), $(document).tipsy({fade:!0, offset:23, live:"[rel=tipsy-n], a[data-tipsy-n]", gravity:"n"}), $(document).tipsy({fade:!0, offset:14, live:"[rel=tipsy], a[data-tipsy]"}), Exposure.isMobile) {
    bowser.android && $("body").addClass("is-android");
    var _body = document.body, _scrollTimer;
    document.addEventListener("touchmove", function () {
        clearTimeout(_scrollTimer), _body.classList.contains("scrolling") || _body.classList.add("scrolling"), _scrollTimer = setTimeout(function () {
            _body.classList.remove("scrolling")
        }, 200)
    }, !1)
}
var Homepage = {};
$("a[href=#learn-more]").bind("click", function (t) {
    return t.preventDefault(), $.scrollTo("#main-copy", 200), !1
}), function (t) {
    t.fn.insertAtCaret = function (t) {
        document.execCommand("insertHTML", !1, t)
    }, t.fn.smarten = function (t) {
        return t = t.replace(/(^|[-\u2014\s(\["])'/g, "$1â€˜"), t = t.replace(/'/g, "â€™"), t = t.replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1â€œ"), t = t.replace(/"/g, "â€"), t = t.replace(/--/g, "â€”")
    }, t.fn.makeEditable = function (e) {
        if (0 === this.length)return this;
        if (e === !0 || e === !1)return e === !0 ? this.attr("contenteditable", !0) : this.removeAttr("contenteditable"), this;
        var n = "getValue" === e, o = {placeholder:"", multiLine:!1, maxLength:0, alwaysShowPlaceholder:!1}, r = {16:"shift", 17:"ctrl", 18:"alt", 91:"cmd", 8:"delete"};
        if (n && (e = null), e = e || this.data("editor-options"), e = t.extend(o, e), n) {
            var i = t.trim(this.html()), a = e.placeholder;
            return e.multiLine === !1 && (i = strip_tags(i)), i = Utils.decodeHtmlEntities(i), i === a ? "" : i
        }
        var s = this;
        return this.data("editor-options", e), this.attr("contenteditable", !0), e.placeholder && "" === this.makeEditable("getValue") && this.html(e.placeholder), this.bind("focus", function () {
            s.data("enter", t.trim(s.html())), s.data("before", t.trim(s.html())), (t.trim(s.text()) === e.placeholder || t.trim(s.html()) === e.placeholder) && s.selectText()
        }), this.bind("keydown keyup blur", function (n) {
            if ("" === t.trim(s.text()) && s.html(e.placeholder).selectText(), "blur" !== n.type && e.maxLength > 0) {
                var o = s.makeEditable("getValue").length;
                if (o > e.maxLength && "undefined" == typeof r[n.which])return n.preventDefault(), !1
            }
            if ("keyup" == n.type && (222 == n.which || 189 == n.which), "keydown" == n.type && 13 === n.which) {
                if (e.multiLine === !1)return n.preventDefault(), !1;
                for (var i = s.find("p"), a = 0, l = i.length; l > a; a++)for (var c = i[a], u = 0, d = c.attributes.length; d > u; u++)t(c).removeAttr(c.attributes[u].name)
            }
        }), this.bind("paste", function (t) {
            if (t.originalEvent.clipboardData) {
                t.preventDefault(), t = t.originalEvent;
                var n = t.clipboardData.getData("text/plain").split(/[\r\n]/g);
                if (n)if (e.multiLine) {
                    for (var o = "", r = 0, i = n.length; i > r; r++)o += "<p>" + s.smarten(n[r]) + "</p>\n";
                    s.insertAtCaret(strip_tags(o, "<p><br><br/><br />"))
                } else {
                    var o = strip_tags(n.join(" "));
                    s.insertAtCaret(s.smarten(o))
                }
                return!1
            }
        }), this.bind("keyup paste", function () {
            var n = t.trim(s.html());
            s.data("before") !== n && (s.data("before", n), n == e.placeholder && (n = ""), s.trigger({type:"change", action:"update", changed:n}))
        }), this.bind("blur", function () {
            var n = t.trim(s.html());
            s.data("enter") !== n && (s.data("enter", n), n == e.placeholder && (n = ""), s.trigger({type:"change", action:"save", changed:n}))
        }), this
    }
}(jQuery), $("form#invite-form").bind("submit", function (t) {
    t.preventDefault();
    var e = $(this).find("input[type=email]");
    return e.removeClass("error animated pulse"), $.post($(this).attr("action"), {email:e.val(), source:$(this).find("input[name=source]").val()}).always(function () {
        $(".inputs").transition({scale:0}, "ease").slideUp("fast"), $(".thank-you").show()
    }), !1
}), $(".author-avatar.editable").dropArea().bind("drop", function (t) {
    t.preventDefault();
    var e = $(this), n = t.originalEvent.dataTransfer.files[0], o = new FileReader;
    if (/image/.test(n.type)) {
        var r = new FormData;
        r.append("user[avatar]", n), PhotoGroups.photosToUpload++, Exposure.loading(!0), $.ajax({url:"/change-avatar", data:r, cache:!1, contentType:!1, processData:!1, type:"PUT"}).done(function () {
            Exposure.showMessage("Your avatar has been updated!")
        }).fail(function () {
            Exposure.showMessage("Your avatar photo didnâ€™t upload. Please try again.")
        }).always(function () {
            PhotoGroups.photosToUpload--, Exposure.loading(!1)
        }), o.onload = function () {
            canvasResize(n, {width:300, height:300, crop:!0, quality:95, callback:function (t) {
                e.find("img").attr("src", t)
            }})
        }, o.readAsDataURL(n)
    }
}), keypress.combo("left", function () {
    var t = $(".post-next-prev .prev h4 a");
    t.length && (window.location.href = t.attr("href"))
}), keypress.combo("right", function () {
    var t = $(".post-next-prev .next h4 a");
    t.length && (window.location.href = t.attr("href"))
}), $("#new_user .author-avatar").bind("change", function (t) {
    var e = t.target.files[0], n = new FileReader, o = $(this).find("img");
    n.onload = function () {
        canvasResize(e, {width:300, height:300, crop:!0, quality:95, callback:function (t) {
            o.attr("src", t)
        }})
    }, n.readAsDataURL(e)
}), $("input[data-action=update-email-pref]").bind("change", function (t) {
    t.preventDefault();
    var e = $(this).attr("data-pref"), n = $(this).is(":checked");
    return Exposure.loading(!0), $.post("/settings/email/update-pref/" + e, {on:n}).always(function () {
        Exposure.loading(!1), Exposure.showMessage("Youâ€™re now " + (n ? "subscribed to " : "unsubscribed from ") + e.replace("_", " ") + ".")
    }), !1
}), $("input[data-action=update-pref]").on("change", function (t) {
    t.preventDefault();
    var e = $(this).attr("data-pref"), n = $(this).is(":checked"), o = {enable_rss:"RSS preferences updated."};
    return Exposure.loading(!0), $.post("/settings/update-pref/" + e, {on:n}).always(function () {
        Exposure.loading(!1), Exposure.showMessage(o[e])
    }), !1
}), $("#user_bio").maxlength({counterContainer:$("span.chr-limit"), text:"%length/%maxlength"}), "#new" == window.location.hash && ($("div.author-avatar.editable").tipsy({trigger:"manual"}).tipsy("show"), window.location.hash = "");
for (var socialLinks = $(".user-details #social-links a"), socialLink, inc, i = 0, c = socialLinks.length; c > i; i++)socialLink = $(socialLinks[i]), inc = .5 * (i + 1) + "s", socialLink.parent().css({"-webkit-animation-delay":inc, "-moz-animation-delay":inc}).addClass("animated fadeIn").show();
$("#user_custom_domain").on("keyup", function () {
    var t = $("#custom-domain-notice").find("a");
    $(this).val() ? ($("#custom-domain-notice").show().find("p b").html($(this).val()), t.attr("href", t.attr("data-base-href") + "?domain=" + $(this).val())) : ($("#custom-domain-notice").hide().find("p b").html("your-site.com"), t.attr("href", t.attr("data-base-href")))
}), $(".post-square:not(.empty) .post-meta").click(function () {
    return window.location = $(this).find(".post-titles a").attr("href"), !1
});
var stripeResponseHandler = function (t, e) {
    var n = $("#new-subscription");
    e.error ? ($(".card-number .error-description").find("span").html(e.error.message).parent().fadeIn(200), $(".button, button").attr("disabled", !1)) : (n.append($('<input type="hidden" name="stripeToken">').val(e.id)), n.get(0).submit()), Exposure.loading(!1)
};
$("#new-subscription").bind("submit", function (t) {
    t.preventDefault(), Exposure.loading(!0);
    var e = $(this), n = $(".billing-options input[type=radio]:checked").val();
    return $(".button, button").attr("disabled", !0), "free" !== n ? $("#card-num").length ? Stripe.card.createToken(e, stripeResponseHandler) : $("#new-subscription").get(0).submit() : $("#new-subscription").get(0).submit(), !1
}), $(".save-changes-header-button").bind("click", function (t) {
    return t.preventDefault(), $("form").get(0).submit(), !1
}), $(".billing-options input[type=radio]").bind("click", function () {
    "free" === $(this).val() ? $(".card-details").transition({opacity:0}, 300, function () {
        $(this).hide()
    }) : $(".card-details").show().transition({opacity:1}, 500)
}), "free" === $(".billing-options input[type=radio]:checked").val() && $(".card-details").transition({opacity:0}, 50, function () {
    $(this).hide()
});
var stripeUpdateCardResponseHandler = function (t, e) {
    var n = $("#update-card");
    e.error ? ($(".card-number .error-description").find("span").html(e.error.message).parent().fadeIn(200), $(".button, button").attr("disabled", !1)) : (n.append($('<input type="hidden" name="stripeToken">').val(e.id)), n.get(0).submit()), Exposure.loading(!1)
};
$("#update-card").bind("submit", function (t) {
    return t.preventDefault(), Exposure.loading(!0), $(".button, button").attr("disabled", !0), Stripe.card.createToken($(this), stripeUpdateCardResponseHandler), !1
});
var Posts = {locationAutocompleter:!1, dirty:!1, showingOptions:!1};
Posts.setup = function () {
    Posts.setupCoverImageUploading(), Posts.setupEditing(), Posts.handleResize(), $("body").bind("click", function (t) {
        "app-message" === t.target.id || $(t.target).closest("#post-settings-container").length || Posts.hideOptions()
    }), $(window).resize(function () {
        Posts.handleResize()
    }).bind("beforeunload",function () {
        return Posts.dirty === !0 ? "You have unsaved changes which will be lost if you leave now." : void 0
    }).scroll(function () {
        var t = $(this).scrollTop();
        0 >= t ? $(".nav-chrome").removeClass("not-at-the-top") : $(".nav-chrome").addClass("not-at-the-top")
    });
    var t = function () {
        if ($("#share-post-reveal").length) {
            var t = $("#share-post-reveal"), e = Exposure.isMobile && event.pageY ? event.pageY : t.offset().top, n = document.body, o = document.documentElement, r = Math.max(n.scrollHeight, n.offsetHeight, o.clientHeight, o.scrollHeight, o.offsetHeight);
            if (r - e <= 2 * t.height()) {
                if (t.css("z-index", 33), !Exposure.isMobile) {
                    var i = 1 - (r - e - t.height()) / t.height();
                    t.css("opacity", i)
                }
            } else t.css("z-index", -1), Exposure.isMobile || t.css("opacity", 0)
        }
    };
    Exposure.isMobile ? $("#share-post-reveal").addClass("mobile") : $(window).bind("scroll", t), $(".back-to-the-top").bind("click", function (t) {
        return t.preventDefault(), $.scrollTo(0, 150), !1
    }), $("input.location-lookup").length && (Posts.locationAutocompleter = new google.maps.places.Autocomplete($("input.location-lookup")[0], {}), google.maps.event.addDomListener(Posts.locationAutocompleter, "place_changed", function () {
        var t = Posts.locationAutocompleter.getPlace();
        $("input[name=location-lat]").val(t.geometry.location.d), $("input[name=location-lon]").val(t.geometry.location.e), Posts.populateLocation()
    })), $("#post").on("keyup", "input[type=text]:not(.post-option), textarea, [contenteditable]", function () {
        Posts.dirty = !0
    }), keypress.combo("left", function () {
        var t = $(".post-next-prev .prev h4 a");
        t.length && (window.location.href = t.attr("href"))
    }), keypress.combo("right", function () {
        var t = $(".post-next-prev .next h4 a");
        t.length && (window.location.href = t.attr("href"))
    }), $(".own-post .post-footnotes span").makeEditable({multiLine:!1, placeholder:"Type here to add optional details or credits to the end of your post. (Max. 200 chars)", maxLength:200}).makeEditable(!1), $(".footnotes-empty").hide(), Posts.populateLocation(), $("a.share-to-facebook").bind("click", function (t) {
        return t.preventDefault(), window.open($(this).attr("href"), "exposure-fb-share", "width=626,height=436"), !1
    }), $("#post-popout .change-permalink input").bind("blur", function () {
        var t = $(this);
        t.val(Posts.cleanSlug(t.val()))
    }), $(".post-settings").bind("click", function (t) {
        return t.preventDefault(), Posts.showOrHideOptions(), !1
    }), $(".options-row.checkbox input[type=checkbox]").on("click", function () {
        var t = $(this), e = t.attr("data-attr");
        Exposure.loading(!0), $.post("/update-post-pref/" + $("#post").attr("data-slug") + "/" + e, {value:t.is(":checked")}).always(function () {
            Exposure.loading(!1)
        }), "disable_enjoys" == e ? t.is(":checked") ? $(".enjoy.button").hide() : $(".enjoy.button").show() : "hide_date" == e && (t.is(":checked") ? $("span.post-date").slideUp().fadeOut() : $("span.post-date").fadeIn().slideDown())
    }), $(".options-row.password .button").bind("click", function (t) {
        t.preventDefault(), Exposure.loading(!0);
        var e = $(this).parent().find("input").val();
        return $.post("/update-post-pref/" + $("#post").attr("data-slug") + "/post_password", {value:e}).done(function () {
            Exposure.showMessage("Post password " + ("" == e ? "removed" : "changed") + ".")
        }).always(function () {
            Exposure.loading(!1)
        }), !1
    }), $(".options-row.post-date .button").bind("click", function (t) {
        t.preventDefault(), Exposure.loading(!0);
        var e = $(this).parent().find("input").val();
        return $.post("/update-post-pref/" + $("#post").attr("data-slug") + "/published_at", {value:e}).done(function (t) {
            $(".author-details .post-date").html(t.resp), Exposure.showMessage("Post date changed.")
        }).fail(function () {
            Exposure.showMessage("Oops. Please type a valid date in the format mm/dd/yyyy")
        }).always(function () {
            Exposure.loading(!1)
        }), !1
    }), $(".options-row.change-permalink .button").bind("click", function (t) {
        t.preventDefault();
        var e = $(this).parent().find("input"), n = $.trim(e.val()), o = $("#post").attr("data-slug");
        return n !== o && confirm("Are you sure you want to save changes and update this postâ€™s URL? If so, the old post URL will no longer work.") ? (e.removeClass("animated pulse"), n ? (Posts.savePost(function (t) {
            t && (Exposure.loading(!0), $.post("/update-slug/" + $("#post").attr("data-slug"), {slug:n}).done(function () {
                window.location.href = "/" + n + "?new&edit"
            }).fail(function () {
                e.addClass("animated pulse").focus(), $("#post-popout h4").html("That URLâ€™s taken.")
            }).always(function () {
                Exposure.loading(!1)
            }))
        }), !1) : (e.focus(), !1)) : !1
    })
}, Posts.cleanSlug = function (t) {
    return String(t.replace(/[^ \-\w]+/gi, "").replace(/ +/g, "-")).toLowerCase()
}, Posts.showOrHideOptions = function () {
    Posts.showingOptions ? Posts.hideOptions() : Posts.showOptions()
}, Posts.showOptions = function () {
    Posts.showingOptions || ($("#post-popout").attr("class", "animated fadeOutDown").attr("class", "animated fadeInUp").show(), Posts.showingOptions = !0)
}, Posts.hideOptions = function () {
    Posts.showingOptions && ($("#post-popout").attr("class", "animated fadeInUp").attr("class", "animated fadeOutDown"), setTimeout(function () {
        $("#post-popout").hide()
    }, 550), Posts.showingOptions = !1)
}, Posts.populateLocation = function () {
    var t = $(".location-hook-added"), e = t.attr("data-url-proto"), n = $(".location-lookup").val();
    n && (t.html($("<a>").attr("target", "_blank").attr("href", e.replace(":query", encodeURIComponent(n))).html(n)), $(".location-lookup").trigger("keydown"))
}, Posts.handleResize = function () {
    $("#post header, #landing-page, #feature-page-cover").css({height:$(window).height() + "px"})
}, Posts.setupCoverImageUploading = function () {
    $("#post header").dropArea().bind("drop", function (t) {
        if (t.preventDefault(), t.stopPropagation(), $("body").hasClass("signed-in")) {
            var e = t.originalEvent.dataTransfer.files[0];
            if (/image/.test(e.type)) {
                PhotoGroups.photosToUpload++, Exposure.loading(!0);
                var n = new FileReader;
                n.onload = function () {
                    canvasResize(e, {width:0, height:0, crop:!1, quality:92, callback:function (t) {
                        $("#cover-image").css("background-image", 'url("' + t + '")')
                    }});
                    var t = new FormData, n = e.name.split(".").reverse()[0], o = Math.round((new Date).getTime() / 1e3), r = [EXPOSURE_ENV, "posts", $("#post").attr("data-id"), "cover-photo", "cover-" + o + "." + n].join("/");
                    t.append("key", r), t.append("AWSAccessKeyId", EXPOSURE_S3_KEY), t.append("acl", "public-read"), t.append("policy", EXPOSURE_S3_POLICY), t.append("signature", EXPOSURE_S3_SIGNATURE), t.append("Content-Type", e.type), t.append("file", e), $.ajax({url:"https://" + EXPOSURE_S3_BUCKET_NAME + ".s3.amazonaws.com/", data:t, cache:!1, contentType:!1, processData:!1, type:"POST"}).done(function () {
                        var t = new FormData;
                        t.append("post[cover_photo_file_name]", e.name), t.append("post[cover_photo_content_type]", e.type), t.append("post[cover_photo_file_size]", e.size), t.append("post[cover_photo_updated_at]", o), $.ajax({url:$("#cover-image").attr("data-upload-url"), data:t, cache:!1, contentType:!1, processData:!1, type:"PUT"}).done(function () {
                        }).fail(function () {
                            Exposure.showMessage("We couldnâ€™t upload that cover photo. Please try again.")
                        }).always(function () {
                            PhotoGroups.photosToUpload--, Exposure.loading(!1)
                        })
                    }).fail(function () {
                        PhotoGroups.photosToUpload--, Exposure.loading(!1), Exposure.showMessage("We couldnâ€™t upload that cover photo. Please try again.")
                    })
                }, n.readAsDataURL(e)
            }
        }
    })
}, Posts.setupEditing = function () {
    $("#post-controls a.edit-post").click(function (t) {
        return t.preventDefault(), $("#post").addClass("edit-mode"), $("#post-controls").animate({right:"30px"}, "fast"), $(this).fadeOut("fast"), $(".upload-area").slideDown("fast").transition({scale:1}, "ease"), $(".post-next-prev").transition({scale:0}, "ease").slideUp("fast"), $(".post-location-tag").slideDown("fast").transition({scale:1}, "ease"), $(".upload-change-cover-image").fadeIn("fast"), $(".location-hook-added").slideUp("fast"), $(".post-footnotes").slideDown("fast").transition({scale:1}, "ease"), $("footer").removeClass("share-enabled"), Posts.startEditing(), !1
    }), $("#post-controls a.cancel-changes").click(function (t) {
        return t.preventDefault(), Posts.endEditing(), $("#post").removeClass("edit-mode"), $("#post-controls").animate({right:"-539px"}, "fast"), $("#post[data-is-published='true'] #post-controls").animate({right:"-566px"}, "fast"), $("body[data-upgraded='false'] #post-controls").animate({right:"-579px"}, "fast"), $("body[data-can-publish='true'] #post-controls").animate({right:"-536px"}, "fast"), $("body[data-can-publish='true'] #post[data-is-published='true'] #post-controls").animate({right:"-566px"}, "fast"), $("#post-controls a.edit-post").fadeIn("slow"), $(".upload-area").transition({scale:0}, "ease").slideUp("fast"), $(".post-next-prev").slideDown("fast").transition({scale:1}, "ease"), $(".post-location-tag").transition({scale:0}, "ease").slideUp("fast"), $(".upload-change-cover-image").fadeOut("fast"), $(".location-hook-added").slideDown("fast"), $(".footnotes-empty").transition({scale:0}, "ease").slideUp("fast"), $("footer").addClass("share-enabled"), !1
    }), $(".upgrade-account").bind("click", function (t) {
        t.preventDefault();
        var e = $(this);
        return"undefined" != typeof e.attr("disabled") ? !1 : (Posts.savePost(function (t) {
            t === !0 && (window.location.href = e.attr("href"))
        }), !1)
    }), $(".publish-post").bind("click", function (t) {
        t.preventDefault();
        var e = $(this);
        return"undefined" != typeof e.attr("disabled") ? !1 : "true" !== $("#post").attr("data-is-published") || confirm("Are you sure you want to unpublish your post?") ? (Posts.savePost(function (t) {
            t === !0 && $.rails.handleMethod(e)
        }), !1) : !1
    }), $("#post-controls .draft-post").bind("click", function (t) {
        return t.preventDefault(), Posts.savePost(), !1
    }), $("#post").on("click", "a.add-new-photos-hook,a.replace-photo-hook,.finish-adding-photos", function () {
        var t = $(this).closest(".photo-set");
        return t.find(".add-new-photos").fadeToggle("fast"), t.hasClass("adding-new-photos") ? t.removeClass("adding-new-photos") : t.addClass("adding-new-photos"), !1
    }), $("#post").on("click", "a.reorder-photos-hook", function (t) {
        return t.preventDefault(), "undefined" != typeof $(this).attr("disabled") ? !1 : (new PhotoGroup($(this).attr("data-group-id")).startPhotoReordering(), !1)
    }), $("#post").on("click", "a.remove-photo-hook", function () {
        if (!confirm("Are you sure? Thereâ€™s no undo."))return!1;
        var t = $(this);
        return t.closest(".full-width-group").transition({opacity:0}, "ease").slideUp("ease", function () {
            PhotoGroups.remove(t.closest(".group").attr("data-group-id"))
        }), !1
    }), $("#post").on("click", "a.remove-photos-hook", function () {
        if (!confirm("Are you sure? Thereâ€™s no undo."))return!1;
        var t = $(this);
        return t.closest(".photo-set-group").transition({opacity:0}, "ease").slideUp("ease", function () {
            PhotoGroups.remove(t.closest(".group").attr("data-group-id"))
        }), !1
    })
}, Posts.startEditing = function () {
    return Exposure.features.editor ? (keypress.stop_listening(), $("#share-post-reveal").hide(), $(".post-titles h1, .post-titles h2").makeEditable(!0), $(".post-titles h2").fadeIn("fast"), barley.bar.settings.lock_edits = !1, barley.editor.toggleEditable(), barley.editor.init(), $(".group-details h2").makeEditable(!0).fadeIn("fast"), $(".group-details .group-description").fadeIn("fast"), _.each($(".group-details .group-description"), function (t) {
        t = $(t), $.trim(t.text()) || t.html(t.attr("data-placeholder"))
    }), $(".own-post .post-footnotes span").makeEditable(!0), void 0) : !1
}, Posts.endEditing = function () {
    keypress.listen(), Posts.populateLocation(), $("#share-post-reveal").show(), Posts.hideOptions(), $(".post-titles h1, .post-titles h2").makeEditable(!1), "Type a subtitle" === $(".post-titles h2").text() && $(".post-titles h2").fadeOut("fast"), $(".group-details h2").makeEditable(!1), barley.bar.settings.lock_edits = !0, barley.editor.toggleEditable(), barley.editor.init(), _.each($(".group-details h2"), function (t) {
        t = $(t), "Photo Group Title" == t.text() && t.fadeOut("fast")
    }), _.each($(".group-description"), function (t) {
        t = $(t), "What are these photos about?" == $.trim(t.text()) && t.fadeOut("fast")
    }), $(".add-new-photos").fadeOut("fast").closest(".photo-set").removeClass("adding-new-photos");
    var t = $(".own-post .post-footnotes span"), e = $(".post-footnotes");
    t.makeEditable(!1), t.makeEditable("getValue") ? e.removeClass("footnotes-empty") : e.addClass("footnotes-empty")
}, Posts.savePost = function (t) {
    Posts.dirty = !1;
    var e = {}, n = $(".group-details h2, .group-details .group-description");
    n.length && _.each(n, function (t) {
        if (t = $(t), !!t.closest(".group").attr("data-removed") != !0) {
            var n = t.closest(".group").attr("data-group-id");
            if (e[n] || (e[n] = {}), t.hasClass("group-description"))var o = t.html(); else var o = t.makeEditable("getValue");
            e[n][t.attr("data-field-name")] = o
        }
    }), Exposure.loading(!0), $.post($("#post").attr("data-update-url"), {_method:"PUT", post:{title:$(".post-titles h1").makeEditable("getValue"), subtitle:$(".post-titles h2").makeEditable("getValue"), location:$("input.location-lookup").val(), lat:$("input[name=location-lat]").val(), lon:$("input[name=location-lon]").val(), footnotes:$(".post-footnotes span").makeEditable("getValue"), group_data:e}},function () {
    }).done(function (e) {
        $("title").html(e.title + " " + $("title").text().match(/(by (.+))/)[0]), "function" != typeof t ? Exposure.showMessage("Your post was saved!") : t(!0)
    }).fail(function (e) {
        var n = [];
        try {
            for (var o in e.responseJSON.errors)n.push(o + " " + e.responseJSON.errors[o].join(", ").replace(/can't be blank/, "is missing"))
        } catch (r) {
            n.push("Please try again")
        }
        Exposure.showMessage("Your post wasnâ€™t saved. " + n.join(", ") + "."), "function" == typeof t && t(!1)
    }).always(function () {
        Exposure.loading(!1)
    })
}, $(".cover-hotspot").click(function () {
    $(".post-titles,.author-meta").fadeToggle("fast", "linear"), $("#post header #cover-image").toggleClass("fade-overlay"), $(this).toggleClass("cover-hotspot-active")
}), $("#post .post-titles h1").makeEditable({placeholder:"Untitled Post", multiLine:!1, maxLength:200, alwaysShowPlaceholder:!0}).makeEditable(!1), $("#post .post-titles h2").makeEditable({placeholder:"Type a subtitle", multiLine:!1, maxLength:200}).makeEditable(!1), "Type a subtitle" == $("#post .post-titles h2").text() && $("#post .post-titles h2").hide(), $("#custom-domain-agree").length && ($("#custom-domain-agree a.button").on("click", function (t) {
    return t.preventDefault(), $("#custom-domain-agree").transition({opacity:0}, function () {
        $(this).hide(), $.post("/users/saw-domain-notice", {saw:!0})
    }), !1
}), $("#custom-domain-agree").show()), $(".scroll-hint").click(function () {
    return $.scrollTo($("#post-content"), 400), !1
}), $("#share-post-reveal .enjoy").on("click", function (t) {
    t.preventDefault();
    var e = $(this).find(".enjoy-count"), n = Math.floor(e.html()), o = "true" == $(this).attr("data-agent-likes"), r = $(this).attr("data-agent");
    return o ? ($(this).removeClass("animated pulse enjoyed").attr("data-agent-likes", !1), e.html(--n)) : ($(this).addClass("animated pulse enjoyed").attr("data-agent-likes", !0), e.html(++n)), $.post("/update-like-status", {id:$("#post").attr("data-id"), agent:r}), !1
}), $("#mature-content").on("click", ".actions .button:first-child", function (t) {
    return t.preventDefault(), $(this).closest("#mature-content").removeClass("animated fadeIn").addClass("animated fadeOut").hide(), !1
});
var hashMatch = window.location.hash.match(/view:(.+)/);
if (hashMatch) {
    var key = "#photo-" + hashMatch[1];
    setTimeout(function () {
        $.scrollTo(key, 400, {offset:-200}), $(key).find(".photo-bouncer").click()
    }, 100)
}
$("#help-modal-container").length && ($("#help-modal-container a.close-help").on("click", function (t) {
    return t.preventDefault(), $("#help-modal-container").fadeOut(function () {
        $("#help-modal-container .white-out").hide()
    }), !1
}), $("#help-modal-container .help-topics li a").on("click",function (t) {
    t.preventDefault();
    var e = $(this).attr("class"), n = $("#help-modal-container .help-topics li.active");
    return n.removeClass("active"), $(this).parent().addClass("active"), $(".help-sections div." + n.find("a").attr("class")).fadeOut(), $(".help-sections div." + e).fadeIn(), !1
}).get(0).click(), $("a.editor-help-hook").on("click", function (t) {
    return t.preventDefault(), $("#help-modal-container").show(), $("#help-modal-container .white-out").show(), !1
})), Posts.setup(), PhotoGroup.prototype.groupType = function () {
    return this.$el.attr("data-group-type")
}, PhotoGroup.prototype.photos = function () {
    return photos[this.id]
}, PhotoGroup.prototype.maxPhotoPosition = function () {
    var t = _.max(this.$el.find(".photo"), function (t) {
        return parseInt($(t).attr("data-position"))
    });
    return parseInt($(t).attr("data-position")) || 0
}, PhotoGroup.prototype.startPhotoReordering = function () {
    var t = "photo-reordering-container", e = this;
    $("#" + t).remove();
    var n = $("<div>").attr("id", t), o = this.photos();
    _.each(o, function (t) {
        n.append($("<img>").attr("data-photo-key", t.photo_key).attr("src", t.asset_url + PhotoGroups.scaleOpts(120, 120) + "&fit=crop").css({background:"#ccc", display:"block"}))
    }), n.sortable({axis:"y", placeholder:"sortable-placeholder", items:"> img"}).on("sortupdate", function () {
        e.updateAndPersistPhotoOrder()
    }), n.append($("<a>").addClass("button").attr("href", "#").html("Iâ€™m Finished").bind("click", function (t) {
        return t.preventDefault(), e.endPhotoReordering(), !1
    })), $("#snowstorm-overlay .white-out").append(n).parent().show(), $(".nav-chrome").fadeOut("slow")
}, PhotoGroup.prototype.endPhotoReordering = function () {
    $("#snowstorm-overlay").fadeOut(200), $(".nav-chrome").fadeIn("slow")
}, PhotoGroup.prototype.updateAndPersistPhotoOrder = function () {
    var t = this, e = 0, n = {}, o = {}, r = $("#photo-reordering-container img");
    _.each(t.photos(), function (t) {
        o[t.photo_key] = t
    }), photos[t.id] = [], _.each(r, function (r) {
        ++e;
        var i = $(r).attr("data-photo-key");
        n[i] = e, o[i].position = e, photos[t.id].push(o[i])
    }), $("#photo-group-" + t.id).find(".photo").remove(), _.each(t.photos(), function (e) {
        Photos.render(t.id, e)
    }), PhotoGroups.renderGrid(t.id), Exposure.loading(!0), $.post("/photo-groups/" + t.id + "/update-photo-positions", {positions:n}).always(function () {
        Exposure.loading(!1)
    })
}, PhotoGroup.prototype.renderGrid = function () {
    var t, e = Exposure.features.mobileDeref && Exposure.isMobile ? "data-deref-src" : "src";
    if ("single" === this.groupType()) {
        var n = this.$el.find("img");
        return String(n.attr("src")).match(/^data:/) || String(n.attr("data-deferred-src")).match(/^data:/) || (t = n.attr("data-deferred-src") + "&q=92&w=" + $(window).width() * PhotoGroups.scaleMultiplier(), n.attr(e, t)), void 0
    }
    var o, r, i, a, s, l, c, u, d, f, h, p, g, m = this.$el.closest(".photo-set-group").width(), v = Math.round($(window).height() / (1.618 * 1.618)), y = this.$el.find(".photo"), b = _.reduce(y, function (t, e) {
        return t += parseFloat($(e).attr("data-aspect-ratio")) * v
    }, 0), w = Math.round(b / m), x = Partition.partition(y, w), C = 1, E = {};
    _.each(x, function (n) {
        s = _.reduce(n, function (t, e) {
            return t += parseFloat($(e).attr("data-aspect-ratio"))
        }, 0), p = 0, g = 0, _.each(n, function (v) {
            v = $(v), v.attr("data-row-index", g), v.attr("data-row-number", C), v.attr("data-last-in-row", g == n.length - 1), g++, o = v.attr("data-photo-key"), r = v.attr("data-original-dimensions").split("x"), 1 == y.length ? v.find("a.fill-row-hook").parent("li").hide() : v.find("a.fill-row-hook").parent("li").show(), i = parseInt(r[0]), a = parseInt(r[1]), u = i / a, l = parseInt(m / s * u) - 1, c = parseInt(m / s), v.width(l).height(c), p += l, d = v.find("img"), f = String(d.attr("src")), h = String(d.attr("data-deferred-src")), f.match(/^data:/) || h.match(/^data:/) || (t = h + PhotoGroups.scaleOpts(l, c), d.attr(e, t))
        }), E[C] = p, C++
    });
    var S, k = Number.POSITIVE_INFINITY, T = !1;
    _.each(E, function (t) {
        k > t && (k = t, T = !0)
    }), T && (C = 1, _.each(x, function (t) {
        if (t = $(t), S = E[C] - k, S > 0) {
            var e = t.last().width(), n = e - S;
            t.last().width(n)
        }
        C++
    }))
};
var PhotoGroups = PhotoGroups || {};
PhotoGroups.uploadPhotos = function (t, e, n, o) {
    if (!(e.length < 1))if ("undefined" == typeof n) {
        Exposure.loading(!0);
        var r = t.attr("data-create-group-url");
        $.post(r, {},function (n) {
            return PhotoGroups.render(n), Posts.startEditing(), PhotoGroups.uploadPhotos(t, e, n.id, n.group_type)
        }).always(function () {
            Exposure.loading(!1)
        })
    } else {
        var i = PhotoGroups.countPhotos(n), a = new PhotoGroup(n);
        _.every(e, function (t) {
            if (i < PhotoGroups.maxPhotosForMultiGroups && /image/.test(t.type)) {
                if (++i, PhotoGroups.uploadPhoto(t, a.id, a.maxPhotoPosition() + 1), "single" == o && i > 0)return!1;
                if ("set" == o && i >= PhotoGroups.maxPhotosForMultiGroups)return!1
            }
            return!0
        })
    }
}, PhotoGroups.uploadPhoto = function (t, e, n) {
    var o = new Photo(e, t, n), r = new PhotoGroup(e);
    return r.$el.find("a.reorder-photos-hook").attr("disabled", !0), $(".button.publish-post, .upgrade-account").attr("disabled", !0), o.upload(function () {
    }, function () {
    }, function () {
        PhotoGroups.photosToUpload <= 1 && (r.$el.find("a.reorder-photos-hook").removeAttr("disabled"), $(".button.publish-post, .upgrade-account").removeAttr("disabled"))
    }), !0
}, PhotoGroups.countPhotos = function (t) {
    return photos[t] ? photos[t].length : 0
}, PhotoGroups.updateReorderControls = function () {
    for (var t, e, n, o = $(".group").not("[data-removed=true]"), r = 0, i = o.length; i > r; r++)t = $(o[r]), e = t.prevAll(".group").not("[data-removed=true]").first(), n = t.nextAll(".group").not("[data-removed=true]").first(), e.length ? t.find(".move-group-up").fadeIn() : t.find(".move-group-up").fadeOut(), n.length ? t.find(".move-group-down").fadeIn() : t.find(".move-group-down").fadeOut()
}, PhotoGroups.render = function (t) {
    var e = "set" == t.group_type, n = Handlebars.templates.photo_group, o = {"is-set":e, "left-align":"left" === t.align, group:t, "show-group-help":PhotoGroups.showGroupHelp}, r = $(n(o));
    $(".photo-groups-container").append(r), photos[t.id] && photos[t.id].length && (_.each(photos[t.id], function (e) {
        Photos.render(t.id, e)
    }), PhotoGroups.renderGrid(t.id)), r.find(".add-new-photos h5 span").html(PhotoGroups.maxPhotosForMultiGroups - PhotoGroups.countPhotos(r.attr("data-group-id"))), r.find(".add-new-photos").dropArea().bind("drop", function (e) {
        return e.preventDefault(), e.stopPropagation(), "single" == t.group_type ? (Photos.remove(r.find(".photo")), PhotoGroups.uploadPhotos(r, [e.originalEvent.dataTransfer.files[0]], r.attr("data-group-id"))) : PhotoGroups.uploadPhotos(r, e.originalEvent.dataTransfer.files, r.attr("data-group-id"))
    }), r.find(".group-details h2").makeEditable({placeholder:"Photo Group Title", multiLine:!1, maxLength:200}).makeEditable(!1), barley.editor.init(), r.find(".group-description").on("keyup blur", function () {
        var t = $(this);
        Posts.dirty = !0, $.trim(t.text()) || t.html(t.attr("data-placeholder"))
    }), "Photo Group Title" == r.find(".group-details h2").text() && r.find(".group-details h2").hide(), "What are these photos about?" == $.trim(r.find(".group-description").text()) && r.find(".group-description").hide(), r.find(".position-controls a[data-direction]").bind("click", function (e) {
        e.preventDefault(), Exposure.loading(!0), $(".position-controls a").fadeOut("fast");
        var n, o = $(this).attr("data-direction"), i = t.id;
        return"up" == o ? n = r.prev() : "down" == o && (n = r.next()), n.length ? ("up" == o ? n.before(r) : "down" == o && n.after(r), $.scrollTo(r, 300, {offset:{top:-100}}), $.post("/photo-groups/" + i + "/update-position", {dir:o, swap:n.attr("data-group-id")}).done(function () {
            Exposure.showMessage("Great! Your group was moved " + o + "."), PhotoGroups.updateReorderControls()
        }).always(function () {
            Exposure.loading(!1)
        }), !1) : !1
    }), r.find(".alignment-controls").bind("click", function (e) {
        e.preventDefault(), r.toggleClass("left-aligned");
        var n = r.hasClass("left-aligned") ? "left" : "center";
        return $.post("/photo-groups/" + t.id + "/update-alignment", {align:n}), !1
    }), PhotoGroups.updateReorderControls()
}, PhotoGroups.scaleMultiplier = function () {
    return $("html").hasClass("highresdisplay") ? 2 : 1
}, PhotoGroups.scaleOpts = function (t) {
    return t *= PhotoGroups.scaleMultiplier(), t = 100 * Math.ceil(t / 100), "embedly" == EXPOSURE_IMAGE_PROXY ? "&quality=95&width=" + t : "imgix" == EXPOSURE_IMAGE_PROXY ? "&q=95&fm=jpg&w=" + t : void 0
}, PhotoGroups.renderGrid = function (t) {
    new PhotoGroup(t).renderGrid()
}, PhotoGroups.remove = function (t) {
    Exposure.loading(!0), $("#photo-group-" + t).attr("data-removed", !0), PhotoGroups.updateReorderControls(), $.post("/photo-groups/" + t, {_method:"DELETE"}).done(function () {
        $("#photo-group-" + t).remove()
    }).always(function () {
        Exposure.loading(!1)
    })
}, PhotoGroups.init = function () {
    if ("undefined" != typeof photoGroups) {
        if ($("body").dropArea().bind("drop", function (t) {
            t.preventDefault(), $("#post").hasClass("edit-mode") ? (Exposure.showMessage("Oops! Drop your photo(s) above to add them to your post."), $.scrollTo(".upload-area", 400, {offset:{top:-100}})) : Exposure.showMessage("Oops! Click Edit Post before adding photos.")
        }), $("#post .upload-target").dropArea().bind("drop", function (t) {
            return t.preventDefault(), t.stopPropagation(), PhotoGroups.uploadPhotos($(this), t.originalEvent.dataTransfer.files)
        }), photoGroups.length)for (var t = 0, e = photoGroups.length; e > t; t++)PhotoGroups.render(photoGroups[t]); else $("#post-controls a.edit-post").click();
        Exposure.features.editor && window.location.search && window.location.search.match(/edit/) && $("#post-controls a.edit-post").click(), $("#post").on("click", "a.delete-photo-hook", function (t) {
            return t.preventDefault(), Photos.remove($(this).closest(".photo")), !1
        }), $("#post").on("click", "a.fill-row-hook", function (t) {
            return t.preventDefault(), Photos.toggleFillRow($(this).closest(".photo")), !1
        }), $(window).bind("beforeunload", function () {
            return PhotoGroups.photosToUpload > 0 ? "Your photos wonâ€™t finish uploading if you leave now." : void 0
        });
        var n = _.debounce(function () {
            _.each($(".group"), function (t) {
                PhotoGroups.renderGrid($(t).attr("data-group-id"))
            })
        }, 250);
        if ($(window).bind("resize", n), Exposure.features.mobileDeref && Exposure.isMobile) {
            var o, r, i, a = $(".group"), s = (EXPOSURE_ENV.match(/dev/) ? "http" : "https") + "://" + EXPOSURE_HOST + "/blank.gif", l = !1, c = _.debounce(function () {
                for (var t = 0, e = a.length; e > t; t++) {
                    o = $(a[t]), r = o.find("img.set-photo");
                    for (var n = 0, c = r.length; c > n; n++)i = $(r[n]), l = Utils.inViewport($(i)), i.attr("src", l ? i.attr("data-deref-src") : s), l && o.hasClass("full-width-group")
                }
            }, 250);
            $("body").bind("touchmove", c), setTimeout(c, 25)
        }
    }
}, $(document).ready(PhotoGroups.init), Photo.prototype.upload = function (t, e, n) {
    Exposure.loading(!0), PhotoGroups.photosToUpload++;
    var o = this, r = {photo_key:this.key, asset_url:"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7", aspect_ratio:1, position:this.position, dimensions:this.dimensions(), photo_group_id:this.group}, i = new FileReader;
    return Photos.render(this.group, r), $("#photo-" + this.key).find(".delete-photo-hook, .fill-row-hook").css("visibility", "hidden"), "single" === $("#photo-group-" + this.group).attr("data-group-type") && $("#photo-group-" + this.group + " .add-new-photos").fadeOut("fast").closest(".photo-set").removeClass("adding-new-photos"), i.onload = function () {
        var r = $("#photo-" + o.key + " img");
        canvasResize(o.file, {width:0, height:0, crop:!1, quality:95, callback:function (t) {
            r.attr("src", t)
        }}), r.bind("load", function () {
            var r = $(this), i = r.prop("naturalWidth") || r.width(), a = r.prop("naturalHeight") || r.height();
            o.width = i, o.height = a, r.unbind("load"), r.parent().attr("data-aspect-ratio", o.aspectRatio()).attr("data-original-dimensions", o.dimensions()), PhotoGroups.renderGrid(o.group), $("#photo-" + o.key + " .photo-bouncer").attr("data-pending", "true");
            var s = new FormData;
            s.append("key", o.filenameWithPath()), s.append("AWSAccessKeyId", EXPOSURE_S3_KEY), s.append("acl", "public-read"), s.append("policy", EXPOSURE_S3_POLICY), s.append("signature", EXPOSURE_S3_SIGNATURE), s.append("Content-Type", o.file.type), s.append("file", o.file), $.ajax({url:"https://" + EXPOSURE_S3_BUCKET_NAME + ".s3.amazonaws.com/", data:s, cache:!1, contentType:!1, processData:!1, type:"POST", progress:function (t) {
                if (t.lengthComputable) {
                    var e = 100 * (t.loaded / t.total), n = $("#photo-" + o.key + " .photo-upload-progress");
                    100 > e ? n.show().find(".progress-bar").css("width", Math.round(e) + "%") : n.fadeOut(200)
                }
            }}).done(function () {
                o.pending = !1, o.uploading = !0;
                var r = new FormData;
                r.append("photo[position]", o.position), r.append("photo[photo_key]", o.key), r.append("photo[dimensions]", o.dimensions()), r.append("photo[photo_file_name]", o.file.name), r.append("photo[photo_content_type]", o.file.type), r.append("photo[photo_file_size]", o.file.size), $.ajax({url:$("#post").attr("data-upload-photo-url").replace(":group_id", o.group), data:r, cache:!1, contentType:!1, processData:!1, type:"POST"}).done(function (e) {
                    o.recordID = e.id, $("#photo-" + o.key).find("img").attr("data-deferred-src", o.assetURL()).closest(".photo").find(".delete-photo-hook, .fill-row-hook").css("visibility", "visible").closest(".photo").find(".photo-bouncer").attr("data-deferred-src", o.assetURL()).removeAttr("data-pending"), photos[o.group] = photos[o.group] || [], photos[o.group].push(e), photos[o.group] = _.sortBy(photos[o.group], function (t) {
                        return parseInt(t.position)
                    }), $("#photo-group-" + o.group).find(".add-new-photos h5 span").html(PhotoGroups.maxPhotosForMultiGroups - PhotoGroups.countPhotos(o.group)), t(o, e)
                }).fail(function (t) {
                    e(o, t);
                    var n = $("#photo-" + o.key);
                    n.transition({scale:0}, function () {
                        n.remove(), PhotoGroups.renderGrid(o.group)
                    }), Exposure.showMessage("There was a problem uploading " + o.file.name + ". Please try again."), Raven.captureMessage("photo:upload:failed-to-persist", {extra:{key:o.key, failData:t, group:o.group, uploadURL:$("#post").attr("data-upload-photo-url").replace(":group_id", o.group), fileType:o.file.type, fileSize:o.file.size}})
                }).always(function () {
                    PhotoGroups.photosToUpload--, Exposure.loading(!1), o.uploading = !1, n(o)
                })
            }).fail(function (t) {
                var e = $("#photo-" + o.key);
                e.transition({scale:0}, function () {
                    e.remove(), PhotoGroups.renderGrid(o.group)
                }), PhotoGroups.photosToUpload--, Exposure.showMessage("There was a problem uploading " + o.file.name + ". Please try again."), Exposure.loading(!1), Raven.captureMessage("photo:upload:s3-upload-failed", {extra:{key:o.key, status:t.status, text:t.responseText, group:o.group, fileType:o.file.type, fileSize:o.file.size}})
            })
        })
    }, i.readAsDataURL(o.file), this
}, Photo.prototype.aspectRatio = function () {
    return 1 * this.width / this.height
}, Photo.prototype.dimensions = function () {
    return[this.width, this.height].join("x")
}, Photo.prototype.extension = function () {
    return this.file.name.split(".").reverse()[0]
}, Photo.prototype.filenameWithPath = function () {
    return[EXPOSURE_ENV, "photos", this.key, "original." + this.extension()].join("/")
}, Photo.prototype.assetURL = function () {
    if ("imgix" === EXPOSURE_IMAGE_PROXY) {
        var t = Math.floor(4 * Math.random() + 1);
        return"https://" + EXPOSURE_IMGIX_PREFIX + "-" + t + ".imgix.net/" + this.filenameWithPath() + "?&q=95&fm=jpg"
    }
};
var Photos = Photos || {};
Photos.remove = function (t) {
    if (confirm("Are you sure? There is no undo.")) {
        var e = t.attr("data-photo-key"), n = t.closest(".group").attr("data-group-id");
        Exposure.loading(!0), t.transition({scale:0}, function () {
            t.remove(), PhotoGroups.renderGrid(n), photos && photos[n] && _.each(photos[n], function (t, o) {
                return t.photo_key == e ? (photos[n].splice(o, 1), void 0) : void 0
            })
        }), $.post("/photos/" + e, {_method:"DELETE"}).done(function () {
            $("#photo-group-" + n + " .add-new-photos h5 span").html(PhotoGroups.maxPhotosForMultiGroups - PhotoGroups.countPhotos(n))
        }).always(function () {
            Exposure.loading(!1)
        })
    }
}, Photos.render = function (t, e) {
    var n = Handlebars.templates.photo, o = {photo:e}, r = n(o);
    $("#photo-group-" + t + " .photos-container").append(r).find(".photo-bouncer").glisse({effect:"fade", changeSpeed:300, dataName:"data-deferred-src", speed:300, fullscreen:!1, disablindRightClick:!0})
}, Photos.setValue = function (t, e, n, o) {
    var r = photos[t.toString()], i = _.find(r, function (t) {
        return t.photo_key == e
    });
    i[n] = o
}, Photos.toggleFillRow = function (t) {
    var e = t.attr("data-photo-key"), n = t.closest(".group").attr("data-group-id");
    Exposure.loading(!0), t.toggleClass("should-fill-row"), Photos.setValue(n, e, "fill_width", t.hasClass("should-fill-row")), $.post("/photos/" + e + "/update-fill-width", {fill_width:t.hasClass("should-fill-row")}), PhotoGroups.renderGrid(n), Exposure.loading(!1), Exposure.showMessage("Set layout saved.")
};