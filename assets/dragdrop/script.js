$(document).ready(function () {

    $(document).delegate('.fullrs_ftimg', 'click', function () {
        $(".hdr-full-image-input-file").trigger('click');

    })



    $("#dropbox-file-input, #hdr-full-image-input-file").on('change',function (){
        var this_ele = $(this);
        var this_id = this_ele.attr('id');
        if (typeof dropdown_tyr_ele === 'undefined') {
            dropdown_tyr_ele = null;
        }

        if (this_id == 'hdr-full-image-input-file' || this_id =='full-width-box') {
            window.dropdown_tyr_ele = 'intro-box-dropdownft'
        } else if (this_id == 'dropbox-file-input') {
            window.dropdown_tyr_ele = 'latest-on-images-dropbox'
        }
    });

    $('#intro-box-dropdownft, #latest-on-images-dropbox, #full-width-box, #latestwork_dropdown_notearea').on('dragenter', function (e) {
        var this_ele = $(this);
        var this_id = this_ele.attr('id');

        if (typeof dropdown_tyr_ele === 'undefined') {
            dropdown_tyr_ele = null;
        }
        if (this_id == 'intro-box-dropdownft' || this_id =='full-width-box') {
            window.dropdown_tyr_ele = 'intro-box-dropdownft'
        } else if (this_id == 'latest-on-images-dropbox' || this_id == 'latestwork_dropdown_notearea') {
            window.dropdown_tyr_ele = 'latest-on-images-dropbox'
        }

        e.originalEvent.stopPropagation();
        e.originalEvent.preventDefault();
        //console.log('dragenter'+Math.random());
    });

    $('#intro-box-dropdownft, #latest-on-images-dropbox').on('dragleave', function (e) {
        e.originalEvent.stopPropagation();
        e.originalEvent.preventDefault();
        if (typeof dropdown_tyr_ele !== 'undefined') {
            setTimeout(function () {
                // window.dropdown_tyr_ele =null;
            }, 400)

        }
    });


    var dropbox;
    var oprand = {
        dragClass: "active",
        on: {
            load: function (e, file) {
                if(typeof dropdown_tyr_ele!=='undefined'){
                    if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                        //var already_sent = null;
                        //window.already_sent = 0;
                    }
                }



                // check file type
                var imageType = /image.*/;
                if (!file.type.match(imageType)) {

                    alert("File \"" + file.name + "\" is not a valid image file");

                    if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                        if (typeof already_sent !== 'undefined') {
                            already_sent = null;
                            window.already_sent = null;
                        }

                    }


                    return false;
                }

                // check file size
                /*if (parseInt(file.size / 1024) > 2050) {
                 alert("File \""+file.name+"\" is too big.Max allowed size is 2 MB.");
                 return false;
                 } */

                if(typeof already_sent!=='undefined'){
                    if(already_sent ==1){
                        return;
                        ///RETURN USE TO PREVENT USER IF DRAG MORE THEN 1 IMAGE FILE.
                    }
                }

                create_box(e, file);

            }
        }
    };
    //intro-box
    //FileReaderJS.setupDrop(document.getElementsByClassName('intro-box_featured_bg'), oprand);
    //full-width-box

    FileReaderJS.setupDrop(document.getElementById('intro-box-dropdownft'), oprand);
    FileReaderJS.setupDrop(document.getElementById('full-width-box'), oprand);
    FileReaderJS.setupDrop(document.getElementById('latest-on-images-dropbox'), oprand);

    FileReaderJS.setupDrop(document.getElementById('latestwork_dropdown_notearea'), oprand);
    //

    //FileReaderJS.setupDrop(document.getElementById('dropbox'), oprand);

    FileReaderJS.setupInput(document.getElementById('hdr-full-image-input-file'), oprand);
    FileReaderJS.setupInput(document.getElementById('dropbox-file-input'), oprand);
});

create_box = function (e, file) {
    if(typeof dropdown_tyr_ele!='undefined'){
        if (dropdown_tyr_ele == 'intro-box-dropdownft') {
            if (typeof already_sent !== 'undefined') {
                if (already_sent == 1) {
                    //PREVENTING IF USER SUBMIT MULTIPLE IMAGES IN FEATURE SINGLE IMAGE BOX
                    return;
                }
            } else {
                var already_sent = null;
            }

            already_sent = 1;
            window.already_sent = 1;
        }
    }

    var rand = Math.floor((Math.random() * 100000) + 3);
    var imgName = file.name; // not used, Irand just in case if user wanrand to print it.
    var src = e.target.result;
    var image_src = src;
    if (dropdown_tyr_ele == 'intro-box-dropdownft') {
        var single_rand_class = rand+'single_rand_class';
        $(".video-container").removeClass('video-container');
        $(".video-profile-res").removeClass('video-profile-res');
        $(".video-plugin-location").find('.drag-img-container').show();
        $(".video_note_rowfluid").hide();
        $("video-description-area-accordion").parent('.row-fluid').hide();
        $(".embed-video-section-iframe").remove();
        $(".note-own-dont, .full-width-arrow, .full-width-image-section, .featured-description-bg").hide();
        $(".view-vid-img").show().addClass('no-backgound default_padd');
        $("#edit-video-fullimg").next('img').remove();
        var loading_html = '<div class="uploader-overlay"><div class="progress-holder"><div class="progress-main '+single_rand_class+'" style="width:0px;"><p>Loading</p></div></div></div>';
            $('#edit-video-fullimg').after('<img src="'+image_src+'" alt="video">');
            $('#edit-video-fullimg').next('img').after(loading_html);

    } else if (dropdown_tyr_ele == 'latest-on-images-dropbox') {
        var template = '<div class="eachImage" id="' + rand + '">';
        template += '<span class="preview" id="' + rand + '"><img src="' + src + '"><span class="overlay"><span class="updone"></span></span>';
        template += '</span>';
        template += '<div class="progress" id="' + rand + '"><span></span></div>';

        //var current_percentage = (event.loaded / event.total) * 100;
        var overlay_progress_class = rand + '_overlay_progress_class';
        var overlay_progress_class_ele = $('.' + overlay_progress_class);
        if (typeof overlay_progress_class_ele.prop('tagName') === 'undefined') {

            var html = '<div class="cell portfolio_latestwork_id ' + overlay_progress_class + '" style=""><div class="latest-work-holder"><img style="width:100%;height:100%;" src="' + image_src + '"><div class="latest-work-disp"><ul class="user-port-work-ops"><li><a href="javascript:void(0);" class="customize_order"><i class="icon-pencil"></i></a></li></ul></div>' +
                '<div class="uploader-overlay"><div class="progress-holder"><div class="progress-main" style="width:0px;"><p>Loading</p></div></div></div>' +
                '</div></div>';

            $('.latestwork_parent').show();
            //$('.latestworks_parentupload_controlls').hide();
            //latestworks_parentupload_controlls
//            $('#portfolio-image-column1, #portfolio-image-column2').show();
//
//            var lefT_img_len = $('#portfolio-image-column1').find('.portfolio_latestwork_id').length;
//            var righT_img_len = $('#portfolio-image-column2').find('.portfolio_latestwork_id').length;
//
//            if (isNaN(righT_img_len)) {
//                righT_img_len = 0;
//            }
//
//            if (isNaN(lefT_img_len)) {
//                lefT_img_len = 0;
//            }
//            var next_html_target = null;
//            if( (righT_img_len == lefT_img_len) || lefT_img_len==0 || lefT_img_len<righT_img_len){
//                var the_order = 1;
//                next_html_target = $('#portfolio-image-column1');
//            } else {
//                var the_order = 2;
//                next_html_target = $('#portfolio-image-column2');
//            }

            $("#portfolio-image-column").append(html);
            var masonary_container = document.querySelector('#portfolio-image-column');
            var msnry = new Masonry( masonary_container, {
                // options
                itemSelector: '.portfolio_latestwork_id'
            });
            portfolioImageRescale();
            ///

            /*
            var total_loop_len = lefT_img_len + righT_img_len;
            var html_arraydata = new Array();
            var left_eq = 0;
            var right_eq = 0;
            var left_htmldata = null;
            var right_htmldata = null;
            for (forarray = 0; forarray < total_loop_len; forarray++) {
                if (isEven(forarray) == true) {
                    var html_target = $('#portfolio-image-column1').find('.portfolio_latestwork_id').eq(left_eq).clone();
                    if (typeof html_target[0] !== 'undefined' || html_target[0] != null) {
                        var left_html = html_target[0].outerHTML;
                        //left_htmldata += left_html;
                        right_htmldata += left_html;
                    }
                    left_eq++;
                } else {
                    var html_target = $('#portfolio-image-column2').find('.portfolio_latestwork_id').eq(right_eq).clone();
                    if (typeof html_target !== 'undefined' && html_target != null) {
                        if (typeof html_target[0] !== 'undefined' || html_target[0] != null) {
                            var right_html = html_target[0].outerHTML;
                            //right_htmldata += right_html;
                            left_htmldata += right_html;
                        }
                    }
                    right_eq++;
                }
            }

            var collect_galleryimagesdata = html_arraydata;
            var total_loop_len = collect_galleryimagesdata.length;

            $('#portfolio-image-column1,#portfolio-image-column2').empty();
            $('#portfolio-image-column1').prepend(html);

            if (typeof left_htmldata !== 'undefined' && left_htmldata != null) {
                left_htmldata = left_htmldata.replace('null', '');
                $('#portfolio-image-column1').append(left_htmldata);
            }

            if (typeof right_htmldata !== 'undefined' && right_htmldata != null) {
                right_htmldata = right_htmldata.replace('null', '');
                $('#portfolio-image-column2').append(right_htmldata);
            }*/

        }


    }


    ///$("#dropbox").css({"display":"none"});
    //if ($("#dropbox .eachImage").html() == null)
    //$("#dropbox").html(template);
    //else
    //$("#dropbox").prepend(template);

    // upload image
    ///setTimeout(function (){
        $(".latestwork_upload_notes").hide();
        upload(file, rand,1);
    //}, 50)
    //console.log('reached!!!!'+Math.random());

}

upload = function (file, rand,the_order) {

    // now upload the file
    var xhr = new Array();
    xhr[rand] = new XMLHttpRequest();
    if (dropdown_tyr_ele == 'intro-box-dropdownft') {
        //xhr[rand].open("post", photoupload_ajaxurl, true);
        xhr[rand].open("post", photoupload_ajaxurl+'/single_feat_img', true);
    } else if (dropdown_tyr_ele == 'latest-on-images-dropbox') {
        if(typeof the_order !=='undefined'){
            xhr[rand].open("post", photoupload_ajaxurl+'/multi_latestwork/'+the_order, true);
        } else {
            xhr[rand].open("post", photoupload_ajaxurl+'/multi_latestwork/', true);
        }
    }



    //xhr[rand].open("post", "../assets/dragdrop/ajax_fileupload.php", true);



    xhr[rand].upload.addEventListener("progress", function (event) {
        //console.log(event);
        if (event.lengthComputable) {
            var current_percentage = (event.loaded / event.total) * 100;

            if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                var single_rand_class = rand+'single_rand_class';
                var percentage_ele = $('.'+single_rand_class);
                percentage_ele.css({"width":current_percentage+"%"});

                if (current_percentage == 100) {
                    if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                        if (typeof already_sent !== 'undefined') {
                            already_sent = null;
                            window.already_sent = null;
                        }

                    }
                    percentage_ele.closest('.uploader-overlay').remove();
                }

            } else if (dropdown_tyr_ele == 'latest-on-images-dropbox') {

                var overlay_progress_class = rand + '_overlay_progress_class';
                var overlay_progress_class_ele = $('.' + overlay_progress_class);

                if (current_percentage == 100) {
                    //console.log('reached IF 100%');

                    $(".select_media_img_tags").select2({
                        tags:[""],
                        tokenSeparators: [",", " "]
                    });
                    //portfolio_latestwork_id
                    //portfolioimage-
                    overlay_progress_class_ele.find('.uploader-overlay').remove();
                    

                }
                
                //var overlay_progress_class = rand + '_overlay_progress_class';
                //var overlay_progress_class_ele = $('.' + overlay_progress_class);

                $("." + overlay_progress_class).find('.progress-main').css({"width": current_percentage + '%'});
            }

        }
        else {
            alert("Failed to compute file upload length");
            if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                if (typeof already_sent !== 'undefined') {
                    already_sent = null;
                    window.already_sent = null;
                }

            }
        }
    }, false);

    xhr[rand].onreadystatechange = function (oEvent) {
        ////JSON RESPONSE DROPZONE IMAGE
        if (xhr[rand].readyState === 4) {
            if (xhr[rand].status === 200) {
                var responsetext = this.responseText;
                if (is_JSON(responsetext)) {
                    //console.log('reached EVENT 100%');
                    var json_data = JSON.parse(responsetext);
                    var success_responsestatus = json_data.success;
                    success_responsestatus = $.trim(success_responsestatus);
                    if (success_responsestatus == 'true') {
                        if(total_images > 0)total_images--;
                        if(total_images == 0){
                            var masonary_container = document.querySelector('#portfolio-image-column');
                            var msnry = new Masonry( masonary_container, {
                                // options
                                itemSelector: '.portfolio_latestwork_id'
                            });
                            customize_order_of_image();
                            portfolioImageRescale();
                        }
                        /*Portfolio scorer by shafqat*/
                        //console.log(json_data.scorer.profile_scorer+'__profile_scorer');
                        //console.log(json_data.scorer.resume_scorer+'__resume_scorer');
                        //console.log(json_data.scorer.portfolio_scorer+'__portfolio_scorer');
                        if(json_data.scorer.resume_scorer+json_data.scorer.portfolio_scorer+json_data.scorer.profile_scorer > "74")
                        {
                            $('.custom-p-color').html('Congratulations, you can publish your profile!');
                        }else
                        {
                            $('.custom-p-color').html('Reach 75% and get published!');
                        }
                        if(json_data.scorer.resume_scorer == "" || json_data.scorer.resume_scorer == "0")
                        {
                            var resume_color = "#F9785B";
                        }
                        else
                        {
                            var resume_color = "#76bdee";
                        }
                        if(json_data.scorer.portfolio_scorer=="" || json_data.scorer.portfolio_scorer == "0")
                        {
                            var portfolio_color ="#F9785B";
                        }
                        else
                        {
                            var portfolio_color = "#c4dafe";
                        }
//                        new Morris.Donut({
//                            element: 'hero-donut',
//                            width: 300,
//                            data: [
//                                {label: 'Profile', value: json_data.scorer.profile_scorer },
//                                {label: 'Resume', value: json_data.scorer.resume_scorer },
//                                {label: 'Portfolio', value: json_data.scorer.portfolio_scorer },
//                                {label: 'Incomplete', value: json_data.scorer.incomplete }
//                            ],
//                            colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                            formatter: function (y) {
//                                return y + "%"
//                            }
//                        })
//                        $("#score-chart-input-field").val(json_data.scorer.fullstatus);
//                        $(".knob").knob();
//                        $("#score-chart-input-field").val(json_data.scorer.fullstatus + "%");
                        var prof_comp = parseInt(json_data.scorer.profile_scorer) + parseInt(json_data.scorer.resume_scorer) + parseInt(json_data.scorer.portfolio_scorer);
                        update_profile_completness_top_chart(prof_comp);
                        publish_unpublish_btn(json_data.scorer.fullstatus);



                        /*Portfolio scorer by shafqat*/

                        image_url = json_data.media_detail.image_url;
                        media_id = json_data.media_id;
                        media_name = json_data.media_detail.media_name;

                        var overlay_progress_class = rand + '_overlay_progress_class';
                        var overlay_progress_class_ele = $('.' + overlay_progress_class);
                        var new_img_id = 'portfolioimage-' + media_id + '-1';
                        overlay_progress_class_ele.attr('id', new_img_id);

                        //portfolio_latestwork_id
                        //portfolioimage-
                        overlay_progress_class_ele.find('.uploader-overlay').remove();
                        //var total_latest_images = ($('#portfolio-image-column1').find('.cell').length) + ($('#portfolio-image-column2').find('.cell').length);
                        //if (total_latest_images < 12) {
                        $("#services-box").show();

                    }

                }

                /*$(".progress[id='" + rand + "'] span").css("width", "100%");
                $(".preview[id='" + rand + "']").find(".updone").html("100%");
                $(".preview[id='" + rand + "'] .overlay").css("display", "none"); */
                //$(".progress[id='"+rand+"']").css("display","none");
            } else {
                if (dropdown_tyr_ele == 'intro-box-dropdownft') {
                    if (typeof already_sent !== 'undefined') {
                        already_sent = null;
                        window.already_sent = null;
                    }
                }
                alert("Error : Unexpected error while uploading file");
            }
        }
    };

    // Set headers
    //xhr[rand].setRequestHeader("Content-Type", "multipart/form-data");
    xhr[rand].setRequestHeader("Content-Type", "multipart/form-data");
    xhr[rand].setRequestHeader("X-File-Name", file.fileName);
    xhr[rand].setRequestHeader("X-File-Size", file.fileSize);
    xhr[rand].setRequestHeader("X-File-Type", file.type);


    if(dropdown_tyr_ele == "latest-on-images-dropbox"){
        total_images++;
    }
    // Send the file (doh)


    xhr[rand].send(file);
}
