<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/signin.css" type="text/css" media="screen"/>
<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img class="logo" src="<?= ASSETS_PATH ?>img/logo-dark.png">
    </a>

    <div class="span4 box">
        <div class="content-wrap">
            <div style="font-size: 70px;margin-bottom: 30px;">404</div>
            Oops, you found a dead link
        </div>
    </div>
</div>