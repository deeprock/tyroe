<script type="text/javascript">
    $(document).on('click','.get_applicants',function(){
        $('.loader-save-holder').show();
        $('.fillter_applicaint').hide();
        $('.get_candidate').removeClass('jobopening_tyroes_tabs_active');
        $('.get_shortlist').removeClass('jobopening_tyroes_tabs_active');
        $(this).addClass('jobopening_tyroes_tabs_active');
        $.ajax({
          type : 'POST',
          url: '<?= $vObj->getURL("openingoverview/applicants/" . $get_jobs['job_id']) ?>',
          success: function(response){
              search_list_inner_html = response;
              $('.loader-save-holder').hide();
              $('.tyroes_content_result').html(response);
              $('.tyroes_content_result').hide();
              $('.tyroes_content_result').fadeIn();
              $('.applicant_area_filters').show();
              $('#user_page_postion').val('applicaint');
              $('.decline_users').removeClass('active_fillters');
              $('.fillters_controllers').removeClass('hidden');
              $('.pagination_controllers').addClass('hidden');
              $('.singleprofile_controllers').addClass('hidden');
              $('.listview_btn').removeClass('active_fillters');
              $('.candidate_no_image_section').hide();
              $('.job_backicon').hide();
              $('.tr-sp').removeClass('tr-tab-active');
              $('.tr-sp').attr('onclick',"window.location.href='<?= $vObj->getURL("openings/".strtolower($vObj->cleanString($get_jobs['job_title']).'-'.$vObj->generate_job_number($get_jobs['job_id']))) ?>'")
              //removing container fliud and add conatiner
              //$('.opening_container').removeClass('container-fluid');
              //$('.opening_container').addClass('container');
              $('#pad-wrapper').css({"padding":"0"});
              $('.right-column').css({"padding":"0"});
              if(check_view_type == 'profile_view'){
                  $('.filter_editor #editors_pick_info .name:eq(0)').trigger('click');
                  $('.singleprofile_controllers').addClass('hidden');
              }
              setTimeout(function(){
                  $('.get_applicants').find('.candidate-badge').fadeOut();
              },2000)

               user_invitation_notification_action();
          }
        });
    });
    $(document).on('click','.get_candidate',function(){
        $('.reviewer_reviews').hide();
        $('.loader-save-holder').show();
        $('.fillter_applicaint').hide();
        $('.get_applicants').removeClass('jobopening_tyroes_tabs_active');
        $('.get_shortlist').removeClass('jobopening_tyroes_tabs_active');
        $(this).addClass('jobopening_tyroes_tabs_active');
        $.ajax({
            type : 'POST',
            url: '<?= $vObj->getURL("openingoverview/Candidates/" . $get_jobs['job_id']) ?>',
            success: function(response){
                search_list_inner_html = response;
                $('.fillter_action').removeClass('active_fillters');
                $('.loader-save-holder').hide();
                $('.tyroes_content_result').html(response);
                $('.tyroes_content_result').hide();
                $('.tyroes_content_result').fadeIn();
                $('.fillter_action:eq(0)').addClass('active_fillters');
                $('.candidate_area_filters').show();
                $('#user_page_postion').val('candidate');
                $('.fillters_controllers').removeClass('hidden');
                $('.pagination_controllers').addClass('hidden');
                $('.opening_container .singleprofile_controllers').addClass('hidden');
                $('.listview_btn').removeClass('active_fillters');
                $('.candidate_no_image_section').show();
                $('.job_backicon').hide();
                $('.tr-sp').removeClass('tr-tab-active');
                $('.tr-sp').attr('onclick',"window.location.href='<?= $vObj->getURL("openings/".strtolower($vObj->cleanString($get_jobs['job_title']).'-'.$vObj->generate_job_number($get_jobs['job_id']))) ?>'")
                //removing container fliud and add conatiner
                /*$('.opening_container').removeClass('container-fluid');
                $('.opening_container').addClass('container');*/
                $('#pad-wrapper').css({"padding":"0"});
                $('.right-column').css({"padding":"0"});

                if(check_view_type == 'profile_view'){
                    $('.filter_editor #editors_pick_info .name:eq(0)').trigger('click');
                }
                var to_trigger=$('#totrigger').val();
                if(to_trigger =='decline'){
                    $(".fillter_action[data-value='Decline']").trigger('click');
                    $('#totrigger').val('');
                }else if(to_trigger=='review'){
                    $(".fillter_action[data-value='Review']").trigger('click');
                    $('#totrigger').val('admin_see_review');
                }
                setTimeout(function(){
                    $('.get_candidate').find('.candidate-badge').fadeOut();
                },2000)
                <?php if($vObj->session->userdata('role_id') != 3){ ?>
                    $('.candidate_area_filters select[name="fillter_drop"] option:eq(3)').remove();
                <?php } ?>
                 user_invitation_notification_action();   
            }
        });
    });
    $(document).on('click','.get_shortlist',function(){
        $('.loader-save-holder').show();
        $('.fillter_applicaint').hide();
        $('.get_applicants').removeClass('jobopening_tyroes_tabs_active');
        $('.get_candidate').removeClass('jobopening_tyroes_tabs_active');
        $(this).addClass('jobopening_tyroes_tabs_active');
        $.ajax({
            type : 'POST',
            url: '<?= $vObj->getURL("openingoverview/Shortlisted/" . $get_jobs['job_id']) ?>',
            success: function(response){
                search_list_inner_html = response;
                $('.loader-save-holder').hide();
                $('.tyroes_content_result').html(response);
                $('.tyroes_content_result').hide();
                $('.tyroes_content_result').fadeIn();
                $('.shortlist_area_filters').show();
                $('#user_page_postion').val('shortlist');
                $('.fillters_controllers').removeClass('hidden');
                $('.pagination_controllers').addClass('hidden');
                $('.listview_btn').removeClass('active_fillters');
                $('.candidate_no_image_section').hide();
                $('.opening_container .singleprofile_controllers').addClass('hidden');
                $('.job_backicon').hide();
                $('.tr-sp').removeClass('tr-tab-active');
                $('.tr-sp').attr('onclick',"window.location.href='<?= $vObj->getURL("openings/".strtolower($vObj->cleanString($get_jobs['job_title']).'-'.$vObj->generate_job_number($get_jobs['job_id']))) ?>'")
                //removing container fliud and add conatiner
                /*$('.opening_container').removeClass('container-fluid');
                $('.opening_container').addClass('container');*/
                $('#pad-wrapper').css({"padding":"0"});
                $('.right-column').css({"padding":"0"});
                if(check_view_type == 'profile_view'){
                    $('.filter_editor #editors_pick_info .name:eq(0)').trigger('click');
                    $('.singleprofile_controllers').addClass('hidden');
                }
                setTimeout(function(){
                    $('.get_shortlist').find('.candidate-badge').fadeOut();
                },2000)
                 user_invitation_notification_action();
            }
        });
    });
</script>
<!-- upper main stats -->
<div id="main-stats" class="border-default">
    <div class="row-fluid stats-row st-bg">
        <div class="span5 tr-sp tr-tab-active" onclick="">
            <div class="span12">
                <div class="opening-title">
                    <a href="<?= $vObj->getURL("openings") ?>" class="job_backicon"><i class="icon-circle-arrow-left icon-large"></i></a>
                    <a href="<?= $vObj->getURL("openings/".strtolower($vObj->cleanString($get_jobs['job_title']).'-'.$vObj->generate_job_number($get_jobs['job_id']))) ?>"><?php echo stripslashes($get_jobs['job_title']); ?></a>
                </div>
                <div class="opening-title-s">
                    <span><?php echo $get_jobs['job_skill']; ?>. <?php echo date('d/m/Y', $get_jobs['start_date']); ?>
                        - <?php echo date('d/m/Y', $get_jobs['end_date']); ?>.</span>
                    <!--<a href="<?php/*= $vObj->getURL("jobopening/EditForm/" . $get_jobs['job_id']) */?>"><i
                            class="icon-edit openings-icon"></i></a>-->
                </div>
            </div>
        </div>
        <div class="span6 margin-default rt-tab">
            <div class="span4 stat first get_applicants">
                <div class="data margin-bottom-2 text-center padding-default">
                    <span class="number applicaint_counter"><?=$applicant_count['total_applicants']?></span>
                </div>
                <div class="br-tab-content margin-top-2" style="text-align:center;"><a>APPLICANTS</a>
                </div>
                <?php if($newest_applicant_count['total_newest_applicants'] > 0){ ?>
                    <span class="candidate-badge"><p><?= $newest_applicant_count['total_newest_applicants'] ?></p></span>
                <?php }?>
            </div>
            <div class="span4 stat get_candidate margin-default">
                <div class="data margin-bottom-2 text-center padding-default">
                    <span class="number candidate_counter"><?=$candidate_count['total_candidates']?></span>
                </div>
                <div class="br-tab-content margin-top-2" style="text-align:center;"><a>CANDIDATES</a>
                </div>
                <?php if($newest_candidate_count['total_newest_candidates'] > 0){ ?>
                    <span class="candidate-badge"><p><?= $newest_candidate_count['total_newest_candidates'] ?></p></span>
                <?php }?>
            </div>
            <div class="span4 stat get_shortlist margin-default">
                <div class="data margin-bottom-2 text-center padding-default">
                    <span class="number green-text shortlist_counter"><?=$shortlist_count['total_shortlist']?></span>
                </div>
                <div class="br-tab-content margin-top-2" style="text-align:center;"><a>SHORTLISTED</a>
                </div>
                <?php if($newest_shortlist_count['total_newest_shortlist'] > 0){ ?>
                    <span class="candidate-badge"><p><?= $newest_shortlist_count['total_newest_shortlist'] ?></p></span>
                <?php }?>
            </div>
            <!--<div class="span3 stat last">
                <div class="data">
                    <span class="number red-text"><?php/*=$hidden_count['total_hidden']*/?></span>
                </div>
                <div style="text-align:center;"><a
                        href="<?php/*= $vObj->getURL("openingoverview/Hidden/" . $get_jobs['job_id']) */?>"
                        class="btn-flat white stat-top">HIDDEN</a></div>
            </div>-->
        </div>

    </div>
    <div class="br-sep"></div>
</div>

<!-- end upper main stats -->