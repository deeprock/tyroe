<!doctype html>
<!--[if IE 8 ]><html class="ie no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if IE 9]> <html class="ie9 no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html  class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Tyroe - Finding an epic creative job starts with a new approach.</title>
	<meta name="description" content="Tyroe - Finding an epic creative job starts with a new approach">
	<meta name="keywords" content="online portfolio, resume, recruitment, jobs, creative jobs, freelancers, vfx, animation, games, junior, entry level, internships, mid level">
	<meta name="author" content="Tyroe">

	<link rel="shortcut icon" href="assets/static-site/images/favicon.ico">
	<link rel="apple-touch-icon" href="assets/static-site/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/static-site/assets/static-site/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/static-site/assets/static-site/images/apple-touch-icon-114x114.png">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="assets/static-site/js/jquery.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat' rel='stylesheet' type='text/css'>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/framework.css">
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/style.css">

</head>
    <body>
	<!-- Begin Navigation -->
	<nav class="mobile">

		<!-- Menu -->
		<ul class="nav-content clearfix">
			<li id="magic-line"></li>
			<li class="current-page upper"><a href="<?= base_url() ?>">For Talent</a></li>
			<li class="upper"><a href="<?= base_url() ?>recruiters">For Recruiters</a></li>
		</ul><!-- END -->

	</nav>
	<header class="mobile">

		<!-- Logo -->
		<a href="<?= base_url() ?>"><img class="logo" src="assets/static-site/images/logo.png" alt="Form Logo" width="96" height="35" /></a>

		<!-- Menu Button -->
		<button type="button" class="nav-button">
		  <div class="button-bars"></div>
	    </button><!-- END -->

	</header>

	<div class="sticky-head"></div>
	<!-- End Navigation -->

	<!-- Begin Large Hero Block -->
	<section class="hero accent parallax" style="background-image: url(assets/static-site/images/mpc-new-york2.jpg);">

		<!-- Heading -->
		<div class="hero-content container">
			<h1>Yew! Invite accepted.</h1>
			<h2>We will be in touch with registration details very soon.</h2>
		</div>
		<!-- END -->
			
            <!-- Insert Column -->
            <div class="callout internal" style="border-bottom: none; margin:50px 100px 200px 100px;">
                    <h5>Think this is cool? Tell your friends.</h5>
                    
		        <div align="center">                    <!-- AddThis Button BEGIN -->
		        <!-- AddThis Button BEGIN -->
			        <span class='st_sharethis_large' displayText='ShareThis'></span>
					<span class='st_facebook_large' displayText='Facebook'></span>
					<span class='st_twitter_large' displayText='Tweet'></span>
					<span class='st_linkedin_large' displayText='LinkedIn'></span>
					<span class='st_pinterest_large' displayText='Pinterest'></span>
					<span class='st_email_large' displayText='Email'></span>
		        </div>
		        <script type="text/javascript">var switchTo5x=true;</script>
				<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
				<script type="text/javascript">stLight.options({publisher: "0a20083a-3ca3-4012-89a2-d58a5f3b399a", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
		        <!-- AddThis Button END -->
        	</div>


		<!-- Button -->
		

	</section>
	<!-- End Large Hero Block -->

	

	<!-- Begin Footer -->
	<footer>
		<div class="container">
			<div class="row">

				<!-- Contact List -->
				<ul class="contact-list col-md-8">
					<li><i class="fa fa-envelope-o"></i><a href="mailto:hello@form.com">hello@tyroe.com</a></li>
					<li><i class="fa fa-phone"></i><a href="#">+ 61 (2) 9882 1835</a></li>
					<li><i class="fa fa-map-marker"></i><a href="#">Sydney, Australia.</a></li>
				</ul><!-- END -->

				<!-- Social List -->
				<ul class="social-list col-md-4">
					<li><a href="http://www.twitter.com/tyroeapp"><i class="fa fa-twitter"></i></a></li>
					<li><a href="http://www.facebook.com/tyroeapp"><i class="fa fa-facebook"></i></a></li>
					<li class="copyright">© 2014 Tyroe Pty Ltd.</li>
				</ul><!-- END -->

			</div>
		</div>
	</footer>
	<!-- End Footer -->
<!-- Update Google Analytics -->
       
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53597013-1', 'auto');
  ga('send', 'pageview');

</script>
	<!-- Javascript -->
	<script src="assets/static-site/js/retina.js"></script>
	<script src="assets/static-site/js/backgroundcheck.js"></script>
	<script src="assets/static-site/js/plugins.js"></script>

	<script src="assets/static-site/js/main.js"></script>

	<!-- AP Registration form -->
	

</body>

</html>