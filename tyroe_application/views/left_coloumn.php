<?php
if ($this->data['controller_name'] == "openingoverview") {
    $job_id = "/" . $this->uri->segment(3);
} else {
    $job_id = "";
}
$method_name = $this->uri->segment(2);

if ($method_name != "" || $method_name != null) {
    $method_name = '/' . $this->uri->segment(2);
} else {
    $method_name = "";
}
$class="";

if($controller_name=="stream" || $controller_name=='exposure' || $controller_name=='feedback'){
    $class="padding-top-11";
}

?>
<div class="span4 avatar-profile <?=$class?>">
    <div class="row-fluid" id="side-list">
        <?php
        if (isset($system_modules['navigation_hotspot'])) {
        foreach ($system_modules['navigation_hotspot'] as $k => $v) {
        ?>

        <div class="row-fluid" <?php if ($v['link'] == $controller_name . $method_name) {
            echo "id='active'";
        } else ''; ?>>
        <div class="span12">
            <?php $v['link'] = ($v['link']=="stream")?"activity-stream":$v['link']; ?>
            <a href="<?= $vObj->getURL($v['link'] . $job_id) ?>"
               class="name"><?= $v['module_title'] ?></a>
            <?php
                if( ($v['link']=="account") && ($this->session->userdata('role_id') != 2) ) {
                    $v['sub_text'] = "Edit your account details and linked accounts";
                }
            ?>
            <span class="subtext"><?= $v['sub_text'] ?></span>

            <div class="pointer">
                <div class="arrow"></div>
                <div class="arrow_border"></div>
            </div>
        </div>
    </div>
    <?php
    }
    }
    ?>

</div>
</div>