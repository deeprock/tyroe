<!DOCTYPE html>
<html class="login-bg">
<head>
<title><?=$page_title?></title>

    <?php if (strtolower(ENVIRONMENT) == 'development') { ?>
    <meta http-equiv="cache-control" content="max-age=0"/>
    <meta http-equiv="cache-control" content="no-cache"/>
    <meta http-equiv="expires" content="0"/>
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>
    <?php } ?>

    <meta name="description" content="<?php echo $this->session->userdata("sv_metatag_desc")?>">
    <meta name="keywords" content="<?php echo $this->session->userdata("sv_metatag_keyword")?>">
    <meta name="author" content="<?php echo $this->session->userdata("sv_metatag_author")?>">
    <!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">-->

    <!-- Mobile Specifics -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>

    <!-- Mobile Internet Explorer ClearType Technology -->
    <!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

    <link rel="shortcut icon" href="assets/static-site/images/favicon.ico">
    <link rel="apple-touch-icon" href="assets/static-site/images/apple-touch-icon.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/static-site/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/static-site/images/apple-touch-icon-114x114.png">
    
<!-- bootstrap -->
<link href="<?=ASSETS_PATH?>css/bootstrap/bootstrap.css" rel="stylesheet">
<link href="<?=ASSETS_PATH?>css/bootstrap/bootstrap-responsive.css" rel="stylesheet">
<link href="<?=ASSETS_PATH?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet">

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/layout.css">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/elements.css">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/icons.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />


<!-- libraries -->
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/lib/font-awesome.css">
    <!-- this page specific styles -->
    <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/signin.css" type="text/css" media="screen"/>
<!-- open sans font -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>



<![endif]-->
</head>
<body>