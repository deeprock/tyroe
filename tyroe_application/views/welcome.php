<!doctype html>
<!--[if IE 8 ]><html class="ie no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if IE 9]> <html class="ie9 no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html  class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge"><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Tyroe - Finding an epic creative job starts with a new approach.</title>
	<meta name="description" content="Tyroe - Finding an epic creative job starts with a new approach">
	<meta name="keywords" content="online portfolio, resume, recruitment, jobs, creative jobs, freelancers, vfx, animation, games, junior, entry level, internships, mid level">
	<meta name="author" content="Tyroe">

	<link rel="shortcut icon" href="assets/static-site/images/favicon.ico">
	<link rel="apple-touch-icon" href="assets/static-site/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/static-site/assets/static-site/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/static-site/assets/static-site/images/apple-touch-icon-114x114.png">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="assets/static-site/js/jquery.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat' rel='stylesheet' type='text/css'>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/framework.css">
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/style.css">

</head>

<body>
	<!-- Begin Navigation -->
	<nav class="mobile">

		<!-- Menu -->
		<ul class="nav-content clearfix">
			<li id="magic-line"></li>
			<li class="current-page upper"><a href="#">For Talent</a></li>
			<li class="upper"><a href="<?= base_url() ?>recruiters">For Recruiters</a></li>
		</ul><!-- END -->

	</nav>
	<header class="mobile">

		<!-- Logo -->
		<a href="<?= base_url() ?>"><img class="logo" src="assets/static-site/images/logo.png" alt="Form Logo" width="96" height="35" /></a>

		<!-- Menu Button -->
		<button type="button" class="nav-button">
		  <div class="button-bars"></div>
	    </button><!-- END -->

	</header>

	<div class="sticky-head"></div>
	<!-- End Navigation -->

	<!-- Begin Large Hero Block -->
	<section class="hero accent parallax" style="background-image: url(assets/static-site/images/mpc-new-york2.jpg);">

		<!-- Heading -->
		<div class="hero-content container">
			<h1>Finding an epic creative job<br>starts with a new approach</h1>
		</div>
		<!-- END -->
		<div class="sub-hero container">
		
			<div class="center col-sm-12">
				<form action="http://register.tyroe.com/" method="post" name="requestFormTop">
					<input id="requestFieldTop" class="signup" onblur="blurField('Top');" onclick="focusField('Top');" value="Email address..." name="requestEmail">
					<a class="submit-btn input-width-b" onclick="validate('Top');" href="#">Request Invite</a>
				</form>
			</div>
		</div>

	</section>
	<!-- End Large Hero Block -->

	


	<section class="content container">
 
	<div class="row">
		<div class="title center col-sm-12">
			<h2>What is a Tyroe?</h2>
			<div class="blurb">An artist with less than five years experience in highly competitive fields such as Film, Television, Games, Animation, Graphic Design, Web Design, Industrial Design, Illustration, Photography... </p>
			</div>
		</div> 
	</div> 
	</section>

	<!-- Begin Absolute Feature Block -->
	<section class="feature-box light absolute clearfix">
		
		<div class="content container center-mobile">

			<!-- Title -->
			<div class="row">
				<div class="title col-sm-6 col-sm-push-6">
					<h2>Pixel-perfect portfolios</h2>
					<p>Upload your artwork and resume to create a responsive portfolio that looks amazing on your mobile and 30inch displays. Show off your work to recruiters the way they have asked us to present it.</p>
				</div>
			</div><!-- END-->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray target"></i><h3>Interactive Builder</h3>
					<p class="small">Effortlessly create a stunning profile using our "what you see is what you get" editor. Drag-n-drop images, embed videos, enter your details right into the page without using a form.</p>
				</div>
			</div><!-- END -->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray monitor"></i><h3>Responsive design</h3>
					<p class="small">Keep your profile ready to show off at all times, and on all mobile devices.</p>
				</div>
			</div><!-- END -->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray browser"></i><h3>Twitter style recommendations</h3>
					<p class="small">Invite your colleagues and associates to add "twitter style" recommendations to your profile. No need to ask for long references that people simply don't have the time to read.</p>
				</div>
			</div><!-- END -->

			<!-- Button -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-offset-6">
					<a href="#start" class="button gray">Sign up</a>
				</div>
			</div><!-- END -->
		
		</div>

		<div class="image-absolute"></div>
	</section>
	<!-- End Feature Block -->



<section class="feature-box absolute-white clearfix">
	<div class="content container center-mobile">
		<div class="row">
		<div class="title col-sm-6 col-sm-push-6">
		<h2>Show off your work. To the right people.</h2>
		<p>We don't waste time increasing your page likes and getting you five star comments from your competitors and friends. We focus on delivering your work directly to decision makers at the world's top studios looking to hire talent.
		</p>
		</div>
	</div> 
	 
	<div class="row">
		<div class="feature col-sm-6 col-sm-offset-6 col-md-3 clearfix">
		<i class="icon gray look"></i><h3>Instantly visible</h3>
		<p class="small">Once you publish your profile, studios will be notified and kept up-to-date with your schuedule and latest work.</div>
		<div class="feature col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-0 clearfix">
		<i class="icon gray light"></i><h3>Valuable feeback</h3>
		<p class="small">Recruiters and their teams collaborate to find the best job candidate by sharing comments and scores with you.</p>
		</div>
	</div> 
	 
	<div class="row">
		<div class="feature col-sm-6 col-sm-offset-6 col-md-3 clearfix">
		<i class="icon gray phone"></i><h3>Industry ranking</h3>
		<p class="small">Get yourself featured by the editor and improve your chances of getting hired based on your industry score.</p>
		</div>
		<div class="feature col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-0 clearfix">
		<i class="icon gray help2"></i><h3>Compare skills</h3>
		<p class="small">Discover how you stack up against the competition, and your own</p>
		</div>
	</div> 
	 
	<div class="row">
		<div class="padded col-sm-6 col-sm-offset-6">
		<a href="#start" class="button gray">Sign up</a>
		</div>
		</div> 
	</div>
	<div class="image-absolute-iphone"></div>
</section>


	<!-- Begin Absolute Feature Block -->
	<section class="feature-box light absolute macbook clearfix">
		
		<div class="content container center-mobile">

			<!-- Title -->
			<div class="row">
				<div class="title col-sm-6 col-sm-push-6">
					<h2>Take control of your career</h2>
					<p>Take control of your career by applying for jobs and tracking their progress all in one place. Receive instant notifications about who is viewing your work and which jobs your are being considered for.</p>
				</div>
			</div><!-- END-->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray target"></i><h3>Personal Dashboard</h3>
					<p class="small">Your own dashboard provides you with a quick overview of everything you need to know. Monitor how you are performing in comparison to other's in your field, review your latest score, and check all recent activity to your profile.
					</p>
				</div>
			</div><!-- END -->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray monitor"></i><h3>One-Click Apply</h3>
					<p class="small">Applying for jobs has never been easier. Simply visit the Job Openings page, search the recommended jobs and hit "Hire Me".</p>
				</div>
			</div><!-- END -->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-push-6 clearfix">
					<i class="icon gray browser"></i><h3>Real-time Alerts</h3>
					<p class="small">Know exactly what is happening at all times. You will be informed the instant something important happens in relation to job applications, profile views, feedback and other helpful insights.</p>
				</div>
			</div><!-- END -->

			<!-- Button -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-offset-6">
					<a href="#start" class="button gray">Sign up</a>
				</div>
			</div><!-- END -->
		
		</div>

		<div class="image-absolute-macbook"></div>
	</section>
	<!-- End Feature Block -->

	
	<section class="content grey">
	<div id="start" class="container">
		<!-- Title -->
		<div class="row">
			<div class="center title col-sm-12">
				<h1 class="white">Anticipated by 7,427 creatives who make cool stuff!</h1>
				<p style="color:white;font-size:20px">Make sure to secure your spot in our beta test starting soon.</p>
				</div>
		</div><!-- END-->

		<!-- Form -->
		<div class="center col-sm-12">
				<form action="https://register.tyroe.com/" method="post" name="requestFormBottom">
					<input id="requestFieldBottom" class="signup" onblur="blurField('Bottom');" onclick="focusField('Bottom');" value="Email address..." name="requestEmail">
					<a class="submit-btn input-width-b" onclick="validate('Bottom');" href="#">Request Invite</a>
				</form>
			</div>
					
		</div>
	</section>
	<!-- End Contact Form Block -->

	<!-- Begin Footer -->
	<footer>
		<div class="container">
			<div class="row">

				<!-- Contact List -->
				<ul class="contact-list col-md-8">
					<li><i class="fa fa-envelope-o"></i><a href="mailto:hello@tyroe.com">hello@tyroe.com</a></li>
					<li><i class="fa fa-phone"></i><a href="#">+ 61 (2) 9882 1835</a></li>
					<li><i class="fa fa-map-marker"></i><a href="#">Sydney, Australia.</a></li>
				</ul><!-- END -->

				<!-- Social List -->
				<ul class="social-list col-md-4">
					<li><a href="https://www.twitter.com/tyroeapp"><i class="fa fa-twitter"></i></a></li>
					<li><a href="https://www.facebook.com/tyroeapp"><i class="fa fa-facebook"></i></a></li>
					<li class="copyright">© 2014 Tyroe Pty Ltd.</li>
				</ul><!-- END -->

			</div>
		</div>
	</footer>
	<!-- End Footer -->

<!-- Update Google Analytics -->
       
  <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53597013-1', 'auto');
  ga('send', 'pageview');

</script>



	<!-- Javascript -->
	<script src="assets/static-site/js/retina.js"></script>
	<script src="assets/static-site/js/backgroundcheck.js"></script>
	<script src="assets/static-site/js/plugins.js"></script>

	<script src="assets/static-site/js/main.js"></script>

	<!-- AP Registration form -->
	<script>
		function validate(form){
		valid = true;
		validtxt = '';
		if(!/\S+@\S+\.\S+/.test(document.getElementById('requestField'+form).value)){
		valid = false;
		validtxt += 'Please provide a valid email address';
		}
		if(!valid){
		alert(validtxt);
		}else{
		document.forms['requestForm'+form].submit();
		}
		}
		function focusField(form){
		if(document.getElementById('requestField'+form).value=='Email address...'){
		document.getElementById('requestField'+form).value='';
		}
		}
		function blurField(form){
		if(document.getElementById('requestField'+form).value==''){
		document.getElementById('requestField'+form).value='Email address...';
		}
		}
	</script>

</body>

</html>