<!-- main container -->
<!-- main container -->
<link href="<?= ASSETS_PATH ?>css/bar/style.css" rel="stylesheet" />
<link href="<?= ASSETS_PATH ?>css/bar/metro.css" rel="stylesheet" />
<style type="text/css">

</style>
<div class="content">

<div class="container-fluid">

<!-- upper main stats -->
<div id="main-stats">
    <div class="row-fluid stats-row" id="stats-padding">
        <div class="span12">
            <div class="span3 stat">
                <div class="data">
                    <span class="number"><?php
                        $total_count = 0;
                        //echo '<pre>';print_r($visits_today);echo '</pre>';
                        foreach($visits_today as $visits_today_new) {
                            $pages_count = $visits_today_new['pages_count'];
                            $visit_by = $visits_today_new['visit_by'];
                            $total_count = $pages_count+$total_count;
                        }
                        echo $total_count;
                        //echo $total_users['cnt'];?></span>
                    VISITS
                </div>
                <span class="date">Today</span>
                <?php /*
                <div class="data">
                    <span class="number"><?=$total_users['cnt'];?></span>
                    TOTAL
                </div>
                <span class="date">Members</span>  */?>


            </div> <?php /* */ // $visits_today ; ?>
            <div class="span3 stat">
                <div class="data">
                    <span class="number"><?=$total_users['cnt']; ?></span>
                    USERS
                </div>
                <span class="date">Total</span>
            </div>
            <div class="span3 stat">
                <div class="data">
                    <span class="number"><?php echo $admin_companies['cnt']; ?></span>
                    STUDIOS
                </div>
                <span class="date">Total</span>
            </div>
            <div class="span3 stat">
                <?php /*
                <div class="data">
                    <span class="number"><?php echo $admin_reviews['cnt']; ?></span>
                    REVIEWS
                </div>
                <span class="date">Outstanding</span>
                */ ?>
                <div class="data">
                    <span class="number"><?php echo $jobs_in_progress['jobs_in_progress']; ?></span>
                    OPENINGS
                </div>
                <span class="date">In Progress</span>
            </div>
        </div>

    </div>
</div>
<!-- end upper main stats -->
<div id="pad-wrapper">
    <div class="row-fluid header">
        <?php /* <h5 class="resume-title"><?= LABEL_DASHBOARD ?></h5>*/?>
    </div>
    <div class="grid-wrapper">
        <div class="row-fluid show-grid">
            <!-- START RIGHT COLUMN -->
            <div class="span12 admin_dashboard_overview" id="opening-overview" style="padding-right: 30px;">
                <div>
                   <!-- statistics chart built with jQuery Flot -->
                    <div class="row-fluid chart">
                        <h4 class="tab-margin">
                            Sign-ups
                        </h4>
                        <div class="btn-group pull-right admin_dashboard_graph_btns">
                            <button class="glow left" id="day">DAY</button>
                            <button class="glow right active" id="month">MONTH</button>
                            <button class="glow right" id="year">YEAR</button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="span12">
                            <div id="statsChart"></div>
                        </div>
                    </div>
                    <!-- end statistics chart -->


                    <!-- START ACTIVITY STREAM -->

                </div>

            </div>
            <hr class="divider">
            <div class="tyroe_featured_approval">
                <div class="row-fluid">
                    <div class="span12">
                        <h4 class="tab-margin">Tyroe Featured Approval</h4>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th class="span1 sortable align-left">
                                    ID
                                </th>
                                <th class="span4 sortable align-left">
                                    <span class="line"></span>NAME
                                </th>
                                <th class="span1 sortable text-center">
                                    <span class="line"></span>FEATURED
                                </th>
                                <th class="span1 sortable align-left">
                                    <span class="line"></span>GROUP
                                </th>
                                <th class="span3 sortable align-left">
                                    <span class="line"></span>EMAIL
                                </th>
                                <th class="span2 sortable align-left">
                                    <span class="line"></span>SIGNED UP
                                </th>
                                <th class="span2 sortable align-left">
                                    <span class="line"></span>LAST VISIT
                                </th>
                                <th class="span1 sortable align-left">
                                    <span class="line"></span>STATUS
                                </th>
                                <th class="span2 sortable align-left">
                                    <span class="line"></span>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <!-- row -->
                            <?php
                            $tyroe_featured_approval_filtered = array_filter($tyroe_featured_approval);
                            if(!is_array($tyroe_featured_approval) || empty($tyroe_featured_approval_filtered)){
                            ?>
                            <tr>
                                <td colspan="12">Currently 0 tyroe pending featured approval</td>
                            </tr>
                            <?php
                            }
                            foreach($tyroe_featured_approval as $tyroefeatured){
                                $tyroefeatured_user_id=$tyroefeatured['user_id'];
                                $tyroefeatured_role_id=$tyroefeatured['role_id'];
                                $tyroefeatured_firstname=$tyroefeatured['firstname'];
                                $tyroefeatured_lastname=$tyroefeatured['lastname'];
                                $tyroefeatured_extra_title=$tyroefeatured['extra_title'];
                                $tyroefeatured_profile_url=$tyroefeatured['profile_url'];
                                $tyroefeatured_email=$tyroefeatured['email'];
                                $tyroefeatured_username=$tyroefeatured['username'];
                                $tyroefeatured_created_timestamp=$tyroefeatured['created_timestamp'];
                                $tyroefeatured_visit_time=$tyroefeatured['visit_time'];
                                $user_profile_url = USER_PROFILE_URL.$tyroefeatured_profile_url;
                            ?>
                            <tr class="tyr_tr_<?=$tyroefeatured_user_id;?>" data-tyr-id="<?php echo $tyroefeatured_user_id; ?>">
                                <td class="normal-txt"><?php echo $tyroefeatured_user_id; ?></td>
                                <td>
                                    <a href="<?=$vObj->getURL("featured")?>" class="name"><?php echo $tyroefeatured_firstname.' '.$tyroefeatured_lastname;?></a>
                                </td>
                                <td class="text-center">
                                    <i class="icon-star-empty" style="color: #f20;"></i>
                                </td>
                                <td class="normal-txt">Tyroe</td>
                                <td class="text-left">
                                    <a  class="normal-txt" href="<?=$user_profile_url;?>"><?=$tyroefeatured_email;?></a>
                                </td>
                                <td class="normal-txt"><?php if(date('Y',$tyroefeatured_created_timestamp)!='1970') { echo date('M j, Y',$tyroefeatured_created_timestamp);} else { echo 'Not available';} ?></td>
                                <td class="normal-txt"><?php if(date('Y',$tyroefeatured_visit_time)!='1970') { echo date('M j, Y',$tyroefeatured_visit_time);} else { echo 'Not available';} ?></td>
                                <td class="normal-txt status_td">
                                    <span class="label label-success changeColor">Active</span>
                                </td>
                                <td class="align-right e_d_controls">
                                    <a class="normal-txt open_tyr_feat_popup" href="javascript:void(0)" id="<?=$tyroefeatured_user_id;?>" class="btn_pop open_tyr_feat_popup<?php /* edit_homepage_user */?>">Edit</a>
                                    <a class="normal-txt" href="javascript:void(0)">Delete</a>
                                </td>
                            </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="9">
                                    <div class="span12">
                                        <span class="pull-right button-center">
                                            <a id="comment_featured" class="btn-flat gray radius_4" href="<?php echo $vObj->getURL("featured")?>">View More</a>
                                        </span>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <div class="studios_pending_approval">
                <div class="row-fluid">
                    <div class="span12">
<!--                        <h4 class="tab-margin">Studios Pending Approval</h4>-->
                        <h4 class="tab-margin">New Studio</h4>
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <!--<th class="span sortable align-left">
                                    ID
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>NAME
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>STATUS
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>COUNTRY
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>EMAIL
                                </th>
                                <th class="col-md-2 sortable align-left">
                                    <span class="line"></span>SIGNUP
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>LAST VISIT
                                </th>
                                <th class="col-md-3 sortable align-left">
                                    <span class="line"></span>
                                </th>-->
                                <th class="span1 sortable align-left">
                                    ID
                                </th>
                                <th class="span3 sortable align-left">
                                    <span class="line"></span>NAME
                                </th>
                                <th class="span1 sortable text-left">
                                    <span class="line"></span>COUNTRY
                                </th>
                                <th class="span1 sortable align-left">
                                    <span class="line"></span>CITY
                                </th>
                                <th class="span3 sortable align-left">
                                    <span class="line"></span>EMAIL
                                </th>
                                <th class="span2 sortable align-left">
                                    <span class="line"></span>SIGNED UP
                                </th>
                                <th class="span2 sortable align-left">
                                    <span class="line"></span>LAST VISIT
                                </th>
<!--                                <th class="span2 sortable align-left">
                                    <span class="line"></span>
                                </th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <!-- row -->
                            <?php
                            $studios_featured_approval_filtered = array_filter($studios_featured_approval);
                            if(!is_array($studios_featured_approval) || empty($studios_featured_approval_filtered)){
                                ?>
                                <tr>
                                    <td colspan="12">Currently 0 studio pending approval</td>
                                </tr>
                            <?php
                            }
                            //studios_featured_approval
                            //print_array($studios_featured_approval,false,'$studios_featured_approval');
                            foreach($studios_featured_approval as $studiosfeatured){
                                $studiosfeatured_user_id=$studiosfeatured['tu_user_id'];
                                $companyname=$studiosfeatured['company_name'];
                                $studiosfeatured_email=$studiosfeatured['email'];
                                $studiosfeatured_countryname=$studiosfeatured['country'];
                                $studiosfeatured_created_timestamp=$studiosfeatured['signed_up'];
                                $studiosfeatured_visit_time=$studiosfeatured['visit_time'];
                                $studiosfeatured_publish_account=$studiosfeatured['publish_account'];
                                $studio_city = $studiosfeatured['city'];
                                $user_profile_url = USER_PROFILE_URL.$studiosfeatured_profile_url;
                                ?>
                                <tr class="tr_studio_<?=$studiosfeatured_user_id;?>" data-studioid="<?= $studiosfeatured_user_id;?>">
                                    <td class="normal-txt"><?php echo $studiosfeatured_user_id; ?></td>
                                    <td>
                                        <a href="#<?php ///$user_profile_url;?>" class="name"><?php echo $companyname;?></a>
                                        <span class="subtext"><?php echo $studiosfeatured['industry']; ?></span>
                                    </td>
                                    <td class="normal-txt">
                                       <?=($studiosfeatured_countryname=="")?'Not available':$studiosfeatured_countryname;?>
                                    </td>
                                    <td class="normal-txt"><?=($studio_city=="")?'Not available':$studio_city;?></td>
                                    <td class="normal-txt">
                                        <a href="mailto:<?=$studiosfeatured_email?>"><?=$studiosfeatured_email;?></a>
                                    </td>
                                    <td class="normal-txt"><?php echo date('M j, Y',$studiosfeatured_created_timestamp); //if(date('Y',$studiosfeatured_created_timestamp)!='1970') { echo date('M j Y',$studiosfeatured_created_timestamp);} else { echo 'Not available';} ?></td>
                                    <td class="align-left normal-txt"><?php if(date('Y',$studiosfeatured_visit_time)!='1970') { echo date('M j, Y',$studiosfeatured_visit_time);} else { echo 'Not available';} ?></td>
<!--                                    <td class="align-right e_d_controls">
                                        <a class="open_popup_approval normal-txt" href="#" id="<?=$studiosfeatured_user_id;?>">Edit</a>
                                        <a class="normal-txt" href="#">Delete</a>
                                    </td>-->
                                </tr>
                            <?php
                            }
                            ?>
                            <tr>
                                <td colspan="8">
                                    <div class="span12">
                                        <span class="pull-right button-center">
                                            <a id="comment" class="btn-flat gray radius_4" href="<?php echo $vObj->getURL("studios/listing")?>">View More</a>
                                        </span>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


            <?php /* <div class="span6 sidebar graph-margin" id="opening-overview1">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <h4 class="tab-margin">New Openings by Month</h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_5" style="height:350px;"></div>
                    </div>
                </div>
            </div>

            <div class="span6 sidebar" id="opening-overview2">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <h4>Subscription Income by Month</h4>
                        <div class="tools">
                            <a href="javascript:;" class="collapse"></a>
                            <a href="#portlet-config" data-toggle="modal" class="config"></a>
                            <a href="javascript:;" class="reload"></a>
                            <a href="javascript:;" class="remove"></a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chart_7" style="height:350px;"></div>
                    </div>
                </div>
            </div>*/ ?>
        </div>
        <!-- END RIGHT COLUMN -->
        <div>
        </div>
    </div>
</div>
</div>
</div>
<!-- end main container -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="studio_approval_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="global-popup_header job_create_header">
                    <button type="button" class="close top30px" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="clearfix"></div>
                <div class="global-popup_body form-body padding0px padding-left-15px">
                </div>
                <div class="clearfix"></div>
        </div>
            <div class="clearfix"></div>
    </div>
</div>


<script>
    $(document).ready(function (){
        //
        $(document).delegate('a','click',function (eve){
            var href = $(this).attr('href');
            if(href =='#'){
                eve.preventDefault();
            }
        });

        $('body').delegate('.open_popup_approval, .open_tyr_feat_popup','click', function (e) {
            var this_ele = $(this);
            if(this_ele.hasClass('open_popup_approval')){
                var modal_title = 'Studio pending approval';
                var stuidio_id = this_ele.closest('tr').attr('data-studioid');
                var poopup_html = ' <a data-stuidio_id="'+stuidio_id+'" class="btn-glow inverse studio_admindecline pull-right">Decline</a>'+
                        '<a data-stuidio_id="'+stuidio_id+'" class="btn-glow success studio_adminapprove pull-right">Approve</a>';

            } else if(this_ele.hasClass('open_tyr_feat_popup')){
                var modal_title = 'Tyro featured approval';
                var tyr_id = this_ele.closest('tr').attr('data-tyr-id');
                var poopup_html = ' <a data-tyr_id="'+tyr_id+'" class="btn-glow inverse tyr_admindecline pull-right">Decline</a>'+
                        '<a data-tyr_id="'+tyr_id+'" class="btn-glow success tyr_adminapprove pull-right">Approve</a>';
            }
            //data-tyr-id


            var model_ele = $('#studio_approval_modal');
            $('#studio_approval_modal').modal('show');
            model_ele.find('#myModalLabel').text(modal_title);
            model_ele.find('.global-popup_body').html(poopup_html);
        });

        $('body').delegate('.tyr_adminapprove, .tyr_admindecline','click', function () {
            var this_ele = $(this);
            this_ele.after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
            $('.tyr_adminapprove').addClass('disabled');
            $('.tyr_admindecline').addClass('disabled');
            if(this_ele.hasClass('tyr_adminapprove')){
                var tyr_ajax_url = '<?=$vObj->getURL("featured/add_to_featured")?>';
            } else if(this_ele.hasClass('tyr_admindecline')){
                var tyr_ajax_url = '<?=$vObj->getURL("featured/decline_featured")?>';
            }
            //decline_featured
            var tyroe_id = this_ele.attr('data-tyr_id');



            if(this_ele.hasClass('tyr_adminapprove')){
                var action = 1;
            } else if(this_ele.hasClass('tyr_admindecline')){
                var action = 2;
            }
            $.ajax({
                type:'POST',
                url: tyr_ajax_url,
                data: ({data_id:tyroe_id}),
                success: function (success_data){
                    success_data = $.trim(success_data);
                    $('#studio_approval_modal').modal('hide');
                    if(success_data==1){
                        setTimeout(function (){
                            $(".tyr_tr_"+tyroe_id).fadeOut(1000,function (){
                                $(this).remove();
                            });
                        },1000);
                    }
                },
                error: function(){

                }
            })
        })


        $('body').delegate('.studio_adminapprove, .studio_admindecline','click', function () {
            var this_ele = $(this);
            this_ele.after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
            $('.studio_adminapprove').addClass('disabled');
            $('.studio_admindecline').addClass('disabled');
            var studio_id = this_ele.attr('data-stuidio_id');
            var studio_ajax_url = '<?=$vObj->getURL("studios/approval")?>';
            if(this_ele.hasClass('studio_adminapprove')){
                var action = 1;
            } else if(this_ele.hasClass('studio_admindecline')){
                var action = 2;
            }
            $.ajax({
                type:'GET',
                url: studio_ajax_url,
                data: ({stuidio_id:studio_id,action:action }),
                success: function (success_data){
                    success_data = $.trim(success_data);
                    $('#studio_approval_modal').modal('hide');
                    if(success_data==1){
                        setTimeout(function (){
                            $(".tr_studio_"+studio_id).fadeOut(1000,function (){
                                $(this).remove();
                            });
                        },1000);
                    }
                },
                error: function(){

                }
            })
        })

        $("body").delegate('.btn_pop.edit_homepage_user','click', function (e) {
            var item_id = this.id;
            var url = '<?=$vObj->getURL("user/form/")?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: "item_id="+item_id,
                success: function (data) {
                    if (data) {
                        var jqObj = jQuery(data);
                        $('.modal-title').html(jqObj.find(".headerdiv").html());
                        jqObj.find(".headerdiv").remove();
                        var getfunc = jqObj.find(".btn-glow").attr("onclick");
                        $('.btn-save').attr("onclick", getfunc);
                        jqObj.find(".btn-glow").remove();
                        $('.modal-body').html(jqObj);
                        $('#myModal').modal('show');
                    }
                }
            })
        });




    })



</script>