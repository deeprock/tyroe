<?php
//foreach ($job_detail as $jobs) {
if(is_array($job_detail)){
for ($ind = 0; $ind < count($job_detail); $ind++) {
    $logo_url =  $job_detail[$ind]['company_logo'];
    if($logo_url != ''){
        $logo_url = UPLOAD_PATH.'company_logo/'.$job_detail[$ind]['company_logo'];

    }else{
        $logo_url = LIVE_SITE_URL.'assets/img/default-avatar.png';
    }
    if ($this->session->userdata("role_id") == '2' || $this->session->userdata("role_id") == '5') {
        if (is_array($job_detail) && isset($job_detail[0]['job_id']) && count($job_detail[0]['job_id']) > 0) {
            ?>

            <div class="row-fluid no-filter job-opening<?php echo $job_detail[$ind]['job_id']; ?>">
            <?php
            #Invitaion job declined
            if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "-1") {
                ?>
                <div class="disabled-opening"><a class="btn-flat danger pull-right btn_process">Declined</a></div>
            <?php
            }
            ?>
            <div class="span12 group tab-margin" id="top">
                <!-- INFO -->
                <div class="row-fluid">
                    <div class="logo-col" style="float: left;width: 15%;margin-right: 3%;">
                        <img class="company-logo-image" src="<?=$logo_url?>" style="height:86px;">
                    </div>
                    <div class="openings-container" style="width: 80%;">
                        <input type="hidden" class="row_id" name="row_id"
                               value="<?=$job_detail[$ind]['job_id']; ?>" data-studio-reviewer="<?=$job_detail[$ind]['user_id']; ?>"/>

                        <div class="span8 basic-detail tab-margin">
                            <h2 class="opening-tittle"><?php echo $job_detail[$ind]['company_name']; ?></h2>

                            <h3 class="opening-post"><?php echo $job_detail[$ind]['job_title']; ?></h3>
                            <h4 class="opening-location"><?php echo $job_detail[$ind]['job_city'] . "," . $job_detail[$ind]['job_location']; ?></h4>
                        </div>
                        <?php
                        #Shortlisted job
                        if ($job_detail[$ind]['job_status_id'] == "") {
                            ?>
                            <div class="span4 tab-margin pull-right">
                                <a class="btn-flat success pull-right hire-me">Hire me</a>
                            </div>
                            <div class="pull-left span12 opening-review-container applicant-bar" style="display:none">
                                <a class="btn-flat applicant">you are an applicant</a>
                            </div>
                        <?php } else if($job_detail[$ind]['job_status_id'] == "3"){ ?>
                            <div class="pull-left span12 opening-review-container">
                                <a class="btn-flat success">you have been shortlisted</a>
                            </div>
                        <?php
                        } #tyroe applied for this job or invited job is accepted
                        else if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "1" ) {
                            ?>
                            <div class="pull-left span12 opening-review-container">
                                <a class="btn-flat white">In review</a>
                                <a class="btn-flat primary">you are a candidate</a>
                            </div>
                        <?php
                        } else if ($job_detail[$ind]['job_status_id'] == "2") {
                            ?>
                            <div class="pull-left span12 opening-review-container applicant-bar">
                                <a class="btn-flat applicant">you are an applicant</a>
                            </div>
                            <?php
                        }
                        ?>
                        <div style="margin-bottom: 10px">
                            <p class="opening-detail jQueryClassReadmoreParagraphFunc" style="overflow: hidden;float: none;line-height: 20px;"><span><?php echo $job_detail[$ind]['job_description']; ?></span></p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="span12 tab-margin pull-left">
                            <div class="span4 tab-margin pull-left"><span
                                    class="opening-date"><i
                                        class="icon-calendar"></i> <?php echo date("dS F, Y", $job_detail[$ind]['start_date']); ?> </span>
                            </div>
                            <div class="span4 tab-margin pull-left"><span
                                    class="opening-month"><i
                                        class="icon-time"></i> <?php echo $job_detail[$ind]['duration']; ?></span>
                            </div>
                            <div class="span4 tab-margin pull-left"><span
                                    class="opening-time"><i
                                        class="icon-legal"></i> <?php echo $job_detail[$ind]['job_type']; ?></span>
                            </div>
                        </div>
                        <div class="span12 tab-margin pull-left">
                            <ul class="opening-skils-requirements">
                                <?php
                                for ($a = 0; $a < count($job_detail[$ind]['skills']); $a++) {
                                    if ($job_detail[$ind]['skills'][$a]['creative_skill']) {
                                        ?>
                                        <li>
                                            <span><?php echo $job_detail[$ind]['skills'][$a]['creative_skill']; ?></span>
                                        </li>
                                    <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                        #Invitaion for job
                        if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "0") {
                            ?>
                            <div class="span12 pull-left tab-margin opening-alert">
                                <div class="alert alert-info">
                                    <i class="icon-exclamation-sign"></i>
                                    You are a candidate for this job. Are you interested?
                                    <div class="pull-right alert-ops">
                                        <a class="btn-flat danger btn_process">No</a>
                                        <a class="btn-flat success btn_process">Yes</a>
                                    </div>
                                </div>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
<?php         /*   <div class="row-fluid no-filter job-opening<?php echo $job_detail[$ind]['job_id']; ?>">
                <div class="span12 group" id="top">

                    <!-- TITLE -->
                    <div class="row-fluid">
                        <div class="span8">
                            <h4>
                                <?php
                                echo $job_detail[$ind]['job_title']; ?>
                            </h4>
                        </div>
                        <div class="span4">
                            <?php if ($this->session->userdata("role_id") == TYROE_FREE_ROLE) { ?>
                                <!-- <span class="btn-glow success"> +Express Your Interest</span>-->
                                <a href="javascript:void(0);" class="btn-glow success">+Express Your Interest</a>
                                <span class="small-text"><p>Pro's can do this! (<a
                                            href="<?= $vObj->getURL('account/subscription') ?>">upgrade
                                            now</a>)</p></span>
                            <?php
                            } else {
                                ?>
                                <a href="javascript:void(0);" class="btn-glow primary">Express Your Interest</a>
                            <?php
                            }
                            ?>
                        </div>

                    </div>

                    <div class="row-fluid" id="opening-stats">
                        <div class="row-fluid">
                            <div class="span8">
                                <div class="row-fluid">
                                    <div class="span12">
                                 <span style="width: 100%; float:left;"><p
                                         style="width: 120px; float:left;">
                                         Studio:</p><span
                                         style="width:290px; float:left;"> <strong></strong><?php
                                         if ($this->session->userdata("role_id") == TYROE_FREE_ROLE) {
                                             ?>Pro's can see this! (<a
                                                 href="<?= $vObj->getURL('account/subscription') ?>">upgrade
                                                 now</a>)<?php
                                         } else {
                                             echo $job_detail[$ind]['studio_name'];
                                         } ?></span></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid" style="margin-top: 5px;">
                            <div class="span8">
                                <div class="row-fluid">
                                    <div class="span12">
                 <span
                     style="width: 100%; float:left;"><p
                         style="width: 120px; float:left;">
                         Country:
                     </p><span
                         style="width:290px; float:left;"><?php echo $job_detail[$ind]['job_location']; ?></span></span>
                 <span
                     style="width: 100%; float:left;"><p
                         style="width: 120px; float:left;">
                         City:</p><span
                         style="width:290px; float:left;"><?php echo $job_detail[$ind]['job_city']; ?>
                        </span></span>



                         <span
                             style="width: 100%; float:left;"><p
                                 style="width: 120px; float:left;">
                                 Skills:</p><span
                                 style="width:290px; float:left;"><?php
                                 for ($a = 0; $a < count($job_detail[$ind]['skills']); $a++) {
                                     echo $job_detail[$ind]['skills'][$a]['creative_skill'] . ", ";

                                 }

                                 ?>

                             </span></span>
                         <span
                             style="width: 100%; float:left;"><p
                                 style="width: 120px; float:left;">
                                 Level:</p><span
                                 style="width:290px; float:left;"><?php echo $job_detail[$ind]['job_skill']; ?>
                                                                                          </span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">

                                 <span style="width: 100%; float:left;"><p
                                         style="width: 100px; float:left; margin-left: 0;">
                                         To Start:</p><span
                                         style="width:112px; float:left;"><?php echo date('d M Y', $job_detail[$ind]['start_date']); ?></span></span>

                          <span style="width: 100%; float:left;"><p
                                  style="width: 100px; float:left; margin-left: 0;">
                                  Duration:</p><span
                                  style="width:112px; float:left;"><?php echo $job_detail[$ind]['job_duration']; ?></span></span>
                                 <span style="width: 100%; float:left;"><p
                                         style="width: 100px; float:left; margin-left: 0;">
                                         Type:</p><span
                                         style="width:112px; float:left;"><?php echo $job_detail[$ind]['job_type']; ?></span></span>
                          <span style="width: 100%; float:left;"><p
                                  style="width: 100px; float:left; margin-left: 0;">
                                  Quantity:</p><span
                                  style="width:112px; float:left;"><?php echo $job_detail[$ind]['job_quantity']; ?></span></span>


                            </div>
                        </div>


                    </div>
                </div>
            </div> */?>

        <?php
        } else {
            ?>
            <div class="span12 alert alert-warning"><i class="icon-warning-sign"></i>No Match Found</div>
        <?php
        }
        ?>
        <input type="hidden" name="for_filter" id="for_filter" value="<?= $hidden_field ?>"/>
        <!-- <div class="row-fluid no-filter job-opening<?php /*echo $jobs['job_id']; */?>">
                          <div class="span12 group" id="top">


                              <div class="row-fluid">
                                  <div class="span8">
                                      <h4>
                                          <?php /*echo $jobs['job_title']; */?>
                                      </h4>
                                  </div>

                              </div>


                              <div class="row-fluid" id="opening-stats">
                                  <div class="span4">
                                      <div class="row-fluid">
                                          <div class="span12">
                                              <div class="label label-success">Reviews Completed</div>
                                              <p><strong>Location:</strong> <?php /*echo $jobs['job_location']; */?></p>
                                              <p>
                                                  <strong>Date:</strong> <?php /*echo date('d M', $jobs['start_date']) . " - " . date('d M', $jobs['end_date']); */?>
                                              </p>

                                              <p><strong>Level:</strong> <?php /*echo $jobs['job_skill']; */?></p>

                                              <p><strong>Quantity:</strong> <?php /*echo $jobs['job_quantity']; */?></p>
                                          </div>
                                      </div>
                                  </div>
                              </div>

                          </div>
                      </div>-->
    <?php
    } else if ($this->session->userdata("role_id") == '4' || $this->session->userdata("role_id") == '3') {
        ?>
      <!--  <div class="row-fluid job-opening<?php /*echo $jobs['job_id']; */?>">
            <div class="span12 group" id="top">


                <div class="row-fluid">
                    <div class="span8">
                        <h4>
                            <a href="<?php/*= $vObj->getURL("openingoverview/JobOverview/" . $jobs['job_id']) */?>"><?php /*echo $jobs['job_title']; */?></a>
                        </h4>
                    </div>
                    <div class="span4">
                        <ul class="actions pull-right">
                            <?php
/*                            if ($this->session->userdata('role_id') == "5") {
                                */?>
                                <li><a class="btn-flat primary" href="javascript:void(0);"
                                       onclick="JobApply('<?php /*echo $jobs['job_id']; */?>');">Apply Here</a>
                                </li>

                            <?php
/*                            } else {
                                */?>
                                <li><a href="<?php/*= $vObj->getURL("JobOpening/EditForm/" . $jobs['job_id']) */?>"><i
                                            class="table-edit"></i></a>
                                </li>
                                <li><a href="#"><i class="table-settings"></i></a></li>
                                <li class="last"><i class="table-delete"
                                                    onclick="DeleteOpening('<?php/*= $jobs['job_id'] */?>');"></i></li>
                            <?php
/*                            }
                            */?>
                        </ul>
                    </div>
                </div>


                <div class="row-fluid" id="opening-stats">
                    <div class="span4">
                        <div class="row-fluid">
                            <div class="span12">
                                <div class="label label-success">Reviews Completed</div>
                                <p><strong>Location:</strong> <?php /*echo $jobs['job_location']; */?></p>

                                <p>
                                    <strong>Date:</strong> <?php /*echo date('d M', $jobs['start_date']) . " - " . date('d M', $jobs['end_date']); */?>
                                </p>

                                <p><strong>Level:</strong> <?php /*echo $jobs['job_skill']; */?></p>

                                <p><strong>Quantity:</strong> <?php /*echo $jobs['job_quantity']; */?></p>
                            </div>
                        </div>
                    </div>
                    <div class="span8">
                        <div class="row-fluid">
                            <div class="span3">
                                <div style="text-align:center;">
                                    <a href="<?php/*= $vObj->getURL("openingoverview/ReviewTeam/" . $jobs['job_id']) */?>"
                                       class="btn-flat white">
                                        <div class="data">
                                            <span class="number"><?php/*= $jobs['total_reviewer'] */?></span>
                                        </div>
                                        REVIEWERS
                                    </a>
                                </div>
                            </div>
                            <div class="span3">
                                <div style="text-align:center;">
                                    <a href="<?php/*= $vObj->getURL("openingoverview/Candidates/" . $jobs['job_id']) */?>"
                                       class="btn-flat white">
                                        <div class="data">
                                                   <span
                                                       class="number"><?php/*= $jobs['total_applicants'] */?></span>
                                        </div>
                                        CANDIDATES
                                    </a>
                                </div>
                            </div>
                            <div class="span3">
                                <div style="text-align:center;">
                                    <a href="<?php/*= $vObj->getURL("openingoverview/Shortlisted/" . $jobs['job_id']) */?>"
                                       class="btn-flat white">
                                        <div class="data">
                                                   <span
                                                       class="number green-text"><?php/*= $jobs['total_shortlist'] */?></span>
                                        </div>
                                        SHORTLISTED
                                    </a>
                                </div>
                            </div>
                            <div class="span3">
                                <div style="text-align:center;">
                                    <a href="<?php/*= $vObj->getURL("openingoverview/Hidden/" . $jobs['job_id']) */?>"
                                       class="btn-flat white">
                                        <div class="data">
                                                   <span
                                                       class="number red-text"><?php/*= $jobs['total_hidden'] */?></span>
                                        </div>
                                        HIDDEN
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>-->

    <?php
    }
}
}else{
    echo '<div class="span12 alert alert-warning"><i class="icon-warning-sign"></i>No Match Found</div>';
}
?>

<!--Dont remove this sign-->    <!--|||-->
<form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                      method="post" action="<?= $vObj->getURL('jobopening') ?>">
    <div class="span12 pagination"><?= $pagination ?></div>
    <input type="hidden" value="<?= $page ?>" name="page">
    <input type="hidden" value="<?= $country_id ?>" name="country_id">
    <input type="hidden" value="<?= $skill_level_id ?>" name="skill_level_id">
    <input type="hidden" value="<?= $creative_skill ?>" name="creative_skill">
    <input type="hidden" value="<?= $job_city ?>" name="job_city">
    <input type="hidden" value="<?= $keyword ?>" name="keyword">
    <input type="hidden" value="<?= $start_date ?>" name="start_date">
    <input type="hidden" value="<?= $job_type ?>" name="job_type">
    <input type="hidden" value="<?= $job_filter ?>" name="job_filter">
    <input type="hidden" value="<?= $orderby ?>" name="orderby">
</form>
