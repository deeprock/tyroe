<?php
//foreach ($job_detail as $jobs) {
for ($ind = 0; $ind < count($job_detail); $ind++) {

    if ($this->session->userdata("role_id") == '2' || $this->session->userdata("role_id") == '5') {
        if (count($job_detail[0]['job_id']) > 0) {
            ?>
            <div class="row-fluid no-filter job-opening<?php echo $job_detail[$ind]['job_id']; ?>">
                <div class="span12 group" id="top">

                    <!-- TITLE -->
                    <div class="row-fluid">
                        <div class="span8">
                            <h5>

                                <?php echo $job_detail[$ind]['job_title']; ?>

                            </h5>
                        </div>
                        <?php
                        if ($hidden_field == "candidate_filter") {
                            if ($job_detail[$ind]['invitation_status'] == 0) {
                                ?>
                                <div
                                    class="span4 status<?php echo $job_detail[$ind]['job_id']; ?>">
                                    <ul class="actions pull-right">
                                        <li><a href="javascript:void(0);"
                                               onclick="acceptInvitaion('<?= $job_detail[$ind]['job_id'] ?>');">Available</a>
                                        </li>
                                        <li><a href="javascript:void(0);"
                                               onclick="rejectInvitaion('<?= $job_detail[$ind]['job_id'] ?>');">Not
                                                Available</a></li>
                                    </ul>
                                </div>
                                <div
                                    class=" span4 span_result result<?= $job_detail[$ind]['job_id']; ?>"
                                    style="display: none"></div>
                            <?php } else { ?>
                                <div class="span4 span_result">
                                    <?php

                                    if ($job_detail[$ind]['invitation_status'] == 1) {
                                        echo "Available";
                                    } elseif ($job_detail[$ind]['invitation_status'] == -1) {
                                        echo "Not Available";
                                    }?>

                                </div>
                            <?php
                            }
                        } else if ($hidden_field == "shortlist_filter") {

                            ?>
                        <div class="span4 span_result">
                            <a>Not Available Now?</a>
</div>
                        <?php
                        }
                        ?>


                    </div>

                    <!-- INFO -->
                    <div class="row-fluid" id="opening-stats">
                        <div class="row-fluid">
                            <div class="span8">
                                <div class="row-fluid">
                                    <div class="span12">
                                                                           <span style="width: 100%; float:left;"><p
                                                                                   style="width: 120px; float:left;">
                                                                                   Studio:</p><span
                                                                                   style="width:290px; float:left;"> <strong><?php echo $job_detail[$ind]['username']; ?></strong></span></span>

                                                                           <span style="width: 100%; float:left;"><p
                                                                                   style="width: 120px; float:left;">
                                                                                   Reviewer:</p><span
                                                                                   style="width:290px; float:left;"><strong><?php echo $job_detail[$ind]['username']; ?></strong></span></span>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row-fluid" style="margin-top: 20px;">
                            <div class="span8">
                                <div class="row-fluid">
                                    <div class="span12">
                                                                   <span
                                                                       style="width: 100%; float:left;"><p
                                                                           style="width: 120px; float:left;">
                                                                           Country:
                                                                       </p><span
                                                                           style="width:290px; float:left;"><?php echo $job_detail[$ind]['job_location']; ?></span></span>




                                                                           <span
                                                                               style="width: 100%; float:left;"><p
                                                                                   style="width: 120px; float:left;">
                                                                                   City:</p><span
                                                                                   style="width:290px; float:left;"><?php echo $job_detail[$ind]['job_location']; ?>
                                                                                  </span></span>



                                                                                   <span
                                                                                       style="width: 100%; float:left;"><p
                                                                                           style="width: 120px; float:left;">
                                                                                           Skills:</p><span
                                                                                           style="width:290px; float:left;"><?php
                                                                                           for ($a = 0; $a < count($job_detail[$ind]['skills']); $a++) {
                                                                                               echo $job_detail[$ind]['skills'][$a]['creative_skill'] . ", ";

                                                                                           }

                                                                                           ?>
                                                                                       </span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="span4">
                                                                   <span style="width: 100%; float:left;"><p
                                                                           style="width: 100px; float:left; margin-left: 0;">
                                                                           To start:</p><span
                                                                           style="width:112px; float:left;"><?php echo date('d M Y', $job_detail[$ind]['start_date']); ?></span></span>
                                                                   <span style="width: 100%; float:left;"><p
                                                                           style="width: 100px; float:left; margin-left: 0;">
                                                                           Duration:</p><span
                                                                           style="width:112px; float:left;"><?php echo $job_detail[$ind]['username']; ?></span></span>
                                                                   <span style="width: 100%; float:left;"><p
                                                                           style="width: 100px; float:left; margin-left: 0;">
                                                                           Quantity:</p><span
                                                                           style="width:112px; float:left;"><?php echo $job_detail[$ind]['job_quantity']; ?></span></span>


                            </div>
                        </div>


                    </div>
                </div>
            </div>
        <?php
        } else {
            ?>

            <div class="span12 alert alert-warning">No Match Found</div>

        <?php
        }
        ?>
        <input type="hidden" name="for_filter" id="for_filter" value="<?= $hidden_field ?>"/>
    <?php
    }
}
?>