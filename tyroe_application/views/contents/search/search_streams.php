<div class="notes">
    <?php
    if (is_array($activity_stream) == "") {
        ?>
        <div class="span12 alert alert-warning"><i class="icon-warning-sign"></i>No Match Found</div>
        <!--<a href="#" class="item">
           No match found

        </a>--><?php

    } else {
        if ($this->session->userdata('role_id') == '3' || $this->session->userdata('role_id') == '4') {
            foreach ($activity_stream as $job_activity_log) {
                $icon;
                $text;
                $link;
                $href = "javascript:void(0)";
                $onclick = "";
                if ($job_activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                    /*JOB APPLICANT SECTION*/
                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];
                    $onclick = $return_activity_stream['onclick'];

                } else if ($job_activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                    /*Profile Favourited Section*/
                    $icon = LABEL_ICON_STAR;
                    $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                    $text = "You added " . $activity_data_email['tyroe_name'] . " to your Favourites";
                    $href = $vObj->getURL('favourite');

                } else if ($job_activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                    /*JOB CANDIDATE Section*/
                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];
                    $onclick = $return_activity_stream['onclick'];

                } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                    /*JOB SHORTLIST Section*/
                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];
                    $onclick = $return_activity_stream['onclick'];

                } else if ($job_activity_log['activity_type_id'] == LABEL_NEW_JOB_LOG) {
                    /*OPENINGS Section*/
                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];

                } else if ($job_activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                    /*Feedback Section*/
                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                    if(count($return_activity_stream) == 0)continue;
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];
                    $onclick = $return_activity_stream['onclick'];

                }
                ?>
            <a href="<?=$href?>"  onclick="<?=$onclick?>" >
            <span class="item">
            <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                <?= $text ?>
                <span class="time"><i
                        class="icon-time"></i> <?php echo get_activity_time($job_activity_log['created_timestamp']); ?></span>
            </a></span><?php
            } ?>
            <?php } else {
            #Tyroe
            foreach ($activity_stream as $activity_log) {
                $icon;
                $text;
                $link;
                $href = "javascript:void(0)";
                $onclick = "";
                if ($activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                    /*JOB APPLICANT SECTION*/
                    $return_activity_stream = $vObj->activity_sub_type_text($activity_log); // Get the text...
                    $icon = $return_activity_stream['icon'];
                    $text = $return_activity_stream['text'];
                    $href = $return_activity_stream['href'];
                    $link = "";
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $activity_sub_type = $activity_data_email['activity_sub_type'];

                } else if ($activity_log['activity_type_id'] == LABEL_PROFILE_VIEW_JOB_LOG) {
                    /*Profile Views Section*/
                    $icon = LABEL_ICON_VIEW;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $text = LABEL_VIEW_PROFILE_TEXT_LOG . " " . $activity_data_email['job_title'].", ".$activity_data_email['company_name'];

                } else if ($activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                    /*Profile Favourited Section*/
                    $icon = LABEL_ICON_STAR;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $text = LABEL_FAVOURITED_PROFILE_TEXT . " by " . $activity_data_email['job_title']." from ".$activity_data_email['company_name'];

                } else if ($activity_log['activity_type_id'] == LABEL_RECOMMEND_COMPLETE) {
                    /*Recommedation Section*/
                    $icon = LABEL_ICON_MOVE;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $text = LABEL_RECOMMEND_COMPLETE_TEXT . "  " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                    $href = $vObj->getURL("profile/#acc-5");

                } else if ($activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                    /*Feedback Section*/
                    $icon = LABEL_ICON_ENVLOPE;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    //$text = LABEL_FEEDBACK_ACTIVITY_TEXT . "  " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                    $text = LABEL_FEEDBACK_ACTIVITY_TEXT . $activity_data_email['reviewer_company_name'];
                    $url ="feedback/".strtolower(str_replace(' ', '-', $activity_data_email['rev_name']) . "-" . $vObj->generate_job_number($activity_data_email['rev_id']));
                    $href = $vObj->getURL($url);

                } else if ($activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                    /*JOB SHORTLIST Section*/
                    $icon = LABEL_ICON_MOVE;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $text = LABEL_SHORTLIST_JOB_LOG_TEXT . " " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                    $url = "opening/".strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                    $href = $vObj->getURL($url);

                } else if ($activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                    /*JOB CANDIDATE Section*/
                    $icon = LABEL_ICON_MOVE;
                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                    $text = LABEL_JOB_CANDIDATE_ACTIVITY_TEXT . " " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                    $url = "opening/".strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                    $href = $vObj->getURL($url);

                }
                ?>
                <a href="<?=$href?>"  onclick="<?=$onclick?>" >
            <span class="item">
            <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                <?= $text ?>
                <span class="time"><i
                        class="icon-time"></i> <?php echo get_activity_time($activity_log['created_timestamp']); ?></span>
                </a></span><?php
            }
        }

    }
    ?>
</div>

<!-- Dont remove the following sign this is for pagination -->
<!--|||-->

<form class="inline-input stream_pagination" _lpchecked="1" enctype="multipart/form-data" name="stream"
                                      method="post" action="<?= $vObj->getURL('activity-stream') ?>">
    <div class="span12 pagination"><?= $pagination ?></div>
    <input type="hidden" value="<?= $page ?>" name="page">
    <input type="hidden" value="<?= $search_activity_type ?>" name="search_activity_type">
    <input type="hidden" value="<?= $search_stream_keyword ?>" name="search_stream_keyword">
    <input type="hidden" value="<?= $search_stream_duration ?>" name="search_stream_duration">
</form>