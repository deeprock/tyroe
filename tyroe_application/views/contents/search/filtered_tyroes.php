<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<?php
//foreach($get_tyroes as $tyroes){

for ($tyr = 0; $tyr < count($get_tyroes); $tyr++) {
    $main_user_id = $get_tyroes[$tyr]['user_id'];
    $image;

    if ($get_tyroes[$tyr]['media_name'] != "") {
        // $image=ASSETS_PATH_NO_IMAGE;
        $image = ASSETS_PATH_PROFILE_THUMB . $get_tyroes[$tyr]['media_name'];
    } else {
        $image = ASSETS_PATH_NO_IMAGE;
    }
    ?>
    <div class="row-fluid filter-tyroe detailed-tyroe<?= $get_tyroes[$tyr]['user_id']; ?>">
        <div class="span12 group" id="top">
            <div class="row-fluid" id="user_info">

                <div class="span8 push-down-10" id="search-align">
                    <div class="span12">
                        <div class="span3" id="avatar">
                            <img src="<?php echo $image; ?>" class="img-circle avatar_search_large">

                            <!-- settings button -->
                            <div class="btn-group settings span12" id="under-avatar">
                                <button class="btn glow"><i class="icon-cog"></i></button>
                                <button class="btn glow dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" style="text-align: left;">
                                    <li><a href="javascript:void(0);"
                                           onclick="ContactTyroe('<?= $get_tyroes[$tyr]['user_id']; ?>')">Contact</a>
                                    </li>
                                    <li><a href="javascript:void(0);"
                                           onclick="AddSelection('<?= $get_tyroes[$tyr]['user_id']; ?>')"
                                           id="add_selection">Add to Selection</a></li>
                                    <li><a href="javascript:void(0);"
                                           onclick="HideTyroe('<?= $get_tyroes[$tyr]['user_id']; ?>')">Hide</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- user information -->
                        <div class="span8">
                            <a href="javascript:void(0);" class="name"
                               style="margin:0px;"><?= $get_tyroes[$tyr]['username']; ?></a>
                                    <span class="location"
                                          style="display: block;"><?= $get_tyroes[$tyr]['country']; ?></span>
                        <span class="tags" style="display: block;">
                            <?php
                            for ($a = 0; $a < count($get_tyroes[$tyr]['skills']); $a++) {
                                echo $get_tyroes[$tyr]['skills'][$a]['creative_skill'] . ", ";

                            }

                            ?>
                            </span>
                            <span class="label label-info">Available Now</span>
                            <span class="label label-success">Featured</span>


                        </div>
                    </div>
                </div>

                <div class="span4 phone-offset-search">
                    <div class="row-fluid show-grid" style="margin-bottom: 4px;">

                        <?php
                        for ($a = 0;
                        $a < 3;
                        $a++) {
                        $portfolio = $get_tyroes[$tyr]['portfolio'][$a]['media_name'];

                        ?>
                        <div class="span6 img-grid">
                            <img src="<?php echo ASSETS_PATH_PORTFOLIO_THUMB . $portfolio ?>">
                        </div>
                        <?php
                        if ($a == 0) {
                        ?>
                        <div class="span6 img-grid">
                            <div class="img-grid-score">
                                <div class="thumb-score">
                                            <span><?php
                                                echo $vObj->get_profile_score($get_tyroes[$tyr]['user_id']);
                                                ?></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                        <?php
                        }

                        }
                        ?>

                    </div>

                </div>


            </div>
        </div>
    </div>

<?php
}
?>
