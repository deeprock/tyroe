<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!--<script src="<?/*= ASSETS_PATH */?>js/jquery-1.9.1.min.js"></script>
<script src="<?/*= ASSETS_PATH */?>js/popup/popup.js"></script>-->
<style>
    .select2-container{
        margin: 0px !important;
        width:100%;
    }
    .select2-container input{
        width: 100%;
    }
    
    #search_countries, #city_search{
        width:100%;
    }
</style>
<script>
    $(function () {/*
     $('#Search_filters input[type="checkbox"]').select2();*/
        //$('#Search_filters select[name="country"]').select2();
        //$('#Search_filters select[name="city"]').select2();
    })
</script>

<script type="text/javascript">
//Global variables
var msg_to = '';
var industry_value = [];
var experience = [];
var level = [];
var country_id = '';
var periorty_value = 'most_recent';
var show_hidden_tyroe_value = '';
var input_value = '';
var search_page_index = 0;
var search_page_number = 1;
var city_id = '';
var availability = 'available';
var availability_later = 'avaliable_later';
var not_availability = '';
var click_id = 0;
var search_list_inner_html = '';
var total_person = <?php echo count($get_rows_for_pagination); ?>;
<?php
if($page_number > 1){
        $pos_id = ($page_number-1) * 12;
    }else{
        $pos_id=0;
    }
?>
var page_position = <?php echo $pos_id; ?>;
//for home page searching area
$(document).ready(function () {
    <?php if($this->input->post('dashboard_search_keyword') != ''){?>
    input_value = '<?=$this->input->post('dashboard_search_keyword') ?>';
    $('#searchinput').val(input_value)
    search_filter_editor();
    $('.loader-save-holder').hide();
    <?php } ?>
    <?php if($this->input->post('home_featured') == 'featured'){ ?>
    $('.loader-save-holder').hide();
    //$('.advanced_search_area').slideToggle();
    $('#search_by_editor_pick').attr('checked', 'checked');
    <?php if($this->input->post('tyroe_id')){ ?>
    page_user_id = '<?php echo $this->input->post('tyroe_id'); ?>';
    page_position = '<?php echo $this->input->post('page_position'); ?>';
    getsingle_user(page_position, page_user_id);
    <?php } ?>
    <?php } ?>
});
//End for home page searching area====
$(document).ready(function () {
    $('#myModal').modal('hide');
    $(document).on("click", ".btn-msg", function () {
        $(".sugg-msg").html("");
        msg_to = $(this).attr("data-value");
        $("#hd_email_to" + msg_to).val(msg_to);
        $('#myModal').modal('show');
        rescale();
        $('.modal-body').css('height', 146);
    });

    //Suggestion meesage Sending
    $(document).on("click", ".btn-send-email", function () {
        $('.sugg-msg').hide();
        if ($('#email-subject').val() == "") {
            $('#email-subject').css({
                border: '1px solid red'
            });
            $('#email-subject').focus();
            return false;
        }

        if ($('#email-subject').val() != "") {
            $('#email-subject').css({
                border: ''
            });
        }
        if ($('.email-message').val() == "") {
            $('.email-message').css({
                border: '1px solid red'
            });
            $('.email-message').focus();
            return false;
        }

        if ($('.email-message').val() != "") {
            $('.email-message').css({
                border: ''
            });
        }
        //else{
        $(this).text('Sending..');
        $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
        $(this).removeClass('btn-send-email');
        $(this).addClass('add_email_dummyclass');

        var email_message = $(".email-message").val();
        var email_subject = $("#email-subject").val();
        var email_to = msg_to;
        var data = "callfrom=send_email&email_message=" + email_message + "&email_subject=" + email_subject + "&email_to=" + email_to;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("search/access_module");?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.success) {
                    $(".sugg-msg").show();
                    $(".sugg-msg").html("Your message send success.");
                    $('.add_email_dummyclass').text('Send');
                    $('.add_email_dummyclass').addClass('btn-send-email');
                    $('.btn-send-email').removeClass('add_email_dummyclass').hide();
                    $('.small_loader').remove();
                    $('.email_close').show();
                }
            }
        });
        //return true;

        //}
    });
    
    $("#skills").select2({
        tags:[
            <?php
            $str = "";
            foreach($skills as $skill){
                $str = $str. '"'.$skill['text'].'",';
            }
            echo rtrim(rtrim($str), ",");
            ?>
        ],
        tokenSeparators: [","],
        placeholder : 'Search by Skills'
//        ajax: {
//            url: '<?= $vObj->getURL("search/get_skills"); ?>',
//            dataType: 'json',
//            delay: 250,
//            data: function (params) {
//              return {
//                q: params
//              };
//            },
//            processResults: function (data) {
//                // parse the results into the format expected by Select2.
//                console.log(data);
//                return {
//                    results: data.items
//                };
//            },
//            success: function(data){
//                console.log(data);
//            },
//            cache: true,
//            results: function (data) {
//                 return {
//                    results:data.items
//                 };
//            }
//        },
//        escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
//        minimumInputLength: 1,
//        templateResult: formatRepo, // omitted for brevity, see the source of this page
//        templateSelection: formatRepoSelection
    }).on("change", function(e) {
        search_filter_editor();
    });
    
});

function formatRepo(repo) {
    if (repo.loading) return repo.text;

    var markup = repo.text;

    return markup;
  }
  
  function formatRepoSelection (repo) {
    return repo.text;
  }


function rescale() {
    var size = {width: $(window).width(), height: $(window).height() }
    var offset = 20;
    var offsetBody = 240;
    //$('#myModal').css('height', size.height - offset );
    $('.modal-body').css('height', size.height - (offset + offsetBody));
    $('#myModal').css('top', '3%');
}

$(window).bind("resize", rescale);

$(document).ready(function () {
    var first_message = "<?php
        $message = $this->session->flashdata('payment_success_message');
        if($message == false)echo "";
        else echo $message;
    ?>";
    
    if(first_message != ''){
        $(".notification-box-message").html(first_message);
        $(".notification-box").show(100);
        setTimeout(function () {
            $(".notification-box").hide();
        }, 5000);
    }
    
    
    $(".batch-selection").on("click", function () {
        if (this.text == "Batch Contact") {

        } else if (this.text == "Batch Add to Job") {
            /* var chk_count = $("input[type=selected_tyroe_chk]").length;
             if (chk_count <= 0) {
             alert("Haven't select any user ");

             }*/
        }
        else if (this.text == "Batch Remove") {
            var id_s = $('input[name="selected_tyroe_chk"]:checked');
            $.each(id_s, function (index, item) {
                var tyroes_val = item.value;
                $(".no-filter-tyroe.detailed-tyroe" + tyroes_val).show();
                $(".tyroe" + tyroes_val).hide();
            });


        }
    });

    /*Add in openings*/
    $(document).on("click", ".add_in_opening_jobs", function () {
        /*First Param =  JOB ID*/
        /*Second Param =  Tyroe ID*/
        $('.loader-save-holder').show();
        var job_tyroe_id = $(this).data('value');
        var job_tyroe_arr = (job_tyroe_id).split(",");
        var job_id = job_tyroe_arr[0];
        var tyroe_id = job_tyroe_arr[1];
        $(this).addClass('disable_opening');
        $(this).removeClass('add_in_opening_jobs');
        $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'add_by_search'},
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();

            },
            failure: function (errMsg) {
            }
        });
    });
    /*Add in openings*/

    /*Add profile in favourite*/
    $(document).on("click", ".fav_profile", function favouriteCandidate() {
        $('.loader-save-holder').show();
        var uid = $(this).data('value');
        var data = "uid=" + uid;

        var current_btn = this;
        var current_star = $(this).find("i");
        $('.tempHolder').html(search_list_inner_html);
        if ($(current_btn).hasClass('star_btn')) {
            $(current_btn).css("background", "#2388d6");
            $(current_star).css("color", "#ffffff");
            $(current_btn).removeClass('star_btn');
            $('.tempHolder').find('.row-id'+uid).find('.fav_profile').removeClass('star_btn').css({"background": "#2388d6"}).children('i').css({"color": "#ffffff"});
        } else {
            $(current_btn).css("background", "#ffffff");
            $(current_star).css("color", "#2388d6");
            $(current_btn).addClass('star_btn');
            $('.tempHolder').find('.row-id'+uid).find('.fav_profile').addClass('star_btn').css({"background": "#ffffff"}).children('i').css({"color": "#2388d6"});
            $(this).blur();
        }
        search_list_inner_html = $('.tempHolder').html();
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(current_btn).css("background", "#2388d6");
                    $(current_star).css("color", "#ffffff");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(current_btn).css("background", "");
                    $(current_star).css("color", "");
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Add profile in favourite*/

    /*Hide Search*/
    $(document).on("click", ".hide_search", function () {
        var tyroe_id = $(this).data('value');
        var hide_tip = $(this).data('tip');
        $.ajax({
            type: "POST",
            data: {tyroe_id: tyroe_id, hide_tip: hide_tip},
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".row-id" + tyroe_id).hide(500);
                    $(".editor-seperator-row" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                } else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Hide Search*/
});

/*GO TO PROFILE VIEWS - START */
/*$(document).on('click', '.name', function () {
 var curr_userid = $(this).attr('value');
 var data="profile=1 & current_userid="+curr_userid;
 $.ajax({
 type: 'POST',
 data: data,
 url: '<?=$vObj->getURL("search_publicprofile");?>',
 dataType: 'html',
 success: function (responseData) {*/
//$('#sidebar-nav').hide();
//$('.navbar-inverse').css("margin-top","-57px");
//////////////$('.content').hide();
/////////////$('.public-profile').show();
/*var iframeHead  = $(".public-profile-frame").contents ().find("head");
 var iframeBody  = $(".public-profile-frame").contents ().find("body");
 iframeHead.append('<meta name="viewport" content="width=device-width, initial-scale=1.0">'+
 '<meta name="HandheldFriendly" content="true"/>'+
 '<meta name="MobileOptimized" content="320"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/bootstrap.min.css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/settings.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/main.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/jquery.fancybox.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/fonts.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/shortcodes.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/responsive.css" type="text/css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/custom.css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/chechkbox.css" rel="stylesheet"/>'+
 '<link href="<?= ASSETS_PATH ?>profile/anibus/viewport.css" rel="stylesheet"/>');
 iframeBody.append (responseData);
 $(".public-profile-frame").css('width','100%');*/
/////////////$('.public-profile').html(responseData);
//////////////////$("#sidebar-nav").css({position: 'relative'});
/////////////$('#navbar-sticky').hide();
/*}
 });
 });*/
/*GO TO PROFILE VIEWS - END */

function AddToJob() {
    var job_id = $("#job_id").val();
    var tyreo_arr = [];
    var id_s = $('input[name="selected_tyroe_chk"]:checked');
    $.each(id_s, function (index, item) {
        var tyroes_val = item.value;
        tyreo_arr.push(tyroes_val);
    });
    var tyroe_id = tyreo_arr.join(",");
    if (tyroe_id == "") {

    }
    if (job_id == "") {
        alert("Job Selection Required");
    } else {
        var data = "tyroe_id=" + tyroe_id + "&job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".detailed-tyroe" + tyroe_id).hide(500);
                    $(".selected.tyroe" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
}

function AddSelection(tyro_id) {
    $(".detailed-tyroe" + tyro_id).hide();
    $(".tyroe" + tyro_id).show();
}

function HideTyroe(tyroe_id) {
    var data = "tyroe_id=" + tyroe_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                if (data.success == 'unhidden') {
                    $(".row-id" + tyroe_id).hide(500);
                } else {
                    $(".detailed-tyroe" + tyroe_id).hide(500);
                }
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);

            }
            else {
                $(".notification-box-message").css("color", "#b81900");
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });


}

function ContactTyroe(tyro_id) {

}

function filter_tyroe(orderby) {
    var country_id = $("#country_id").val();
    var creative_skill_id = $("#creative_skill").val();
    var data = "orderby=" + orderby + "&country_id=" + country_id + "&creative_skill_id=" + creative_skill_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("search/FilterTyroes")?>',
        /*dataType: "json",*/
        success: function (data) {
            $(".no-filter-tyroe").hide();
            $(".filter-result").show();
            $(".filter-result").html(data);
        }
    });
}


</script>

<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {

        // DOM Ready
        $(function () {

            // Binding a click event
            // From jQuery v.1.7.0 use .on() instead of .bind()
            $('#invite').bind('click', function (e) {

                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired
                $('#element_to_pop_up').bPopup();

            });

        });

    })(jQuery);
</script>
<script>
//search fillter advance
function getindustry() {
    industry_value = [];
    $('.industries:checked').each(function (i) {
        industry_value[i] = $(this).val();
    });
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
function getexperience() {
    experience = [];
    $('.experience:checked').each(function (i) {
        experience[i] = $(this).val();
    });
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
function getlevel() {
    level = [];
    $('.level:checked').each(function (i) {
        level[i] = $(this).val();
    });
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
function getCities(obj) {
    var countryID = $(obj).val();
    country_id = countryID;
    city_id = '';
    if (countryID == "") {
        countryID = 0;
    }
    result = $.ajax({
        type: 'POST',
        data: 'country_id=' + countryID + '&getcites=yes',
        url: '<?=$vObj->getURL("search/getcities");?>',
        async: false,
        success: function (data) {
            $('#cities_box').html(data);
        }
    });
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();

}
function citiesvalue(city_value) {
    city_id = city_value;
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
function getperiority(periorty_text) {
    periorty_value = periorty_text;
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
function getavailability(avaiabilty_value) {
    if ($('#availability:checked').size() == 1) {
        availability = $('#availability:checked').val();
    } else {
        availability = '';
    }
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}

function getavailabilityLater(avaiabilty_later_value) {
    if ($('#availability_later:checked').size() == 1) {
        availability_later = $('#availability_later:checked').val();
    } else {
        availability_later = '';
    }
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}

function getavailabilityNot(avaiabilty_not_value) {
    if ($('#not_availability:checked').size() == 1) {
        not_availability = $('#not_availability:checked').val();
    } else {
        not_availability = '';
    }
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
}
$(document).on('click', '#show_hidden_tyroe', function () {
    if ($('#show_hidden_tyroe:checked').size() == 1) {
        show_hidden_tyroe_value = 'show_hidden';
    } else {
        show_hidden_tyroe_value = '';
    }
    search_filter_editor();
});
$(document).on('click', '#search_by_editor_pick', function () {
    if ($('#search_by_editor_pick:checked').size() == 1) {
        periorty_value = 'featured';
    } else {
        periorty_value = '';
    }
    search_page_index = 0;
    search_page_number = 1;
    search_filter_editor();
})

jQuery(document).ready(function () {
    $('#searchinput').donetyping(function () {
        search_page_index = 0;
        search_page_number = 1;
        input_value = $('#searchinput').val();
        search_filter_editor();
    }, 200);
});

$(document).ready(function () {
    $('#advance_toggle').click(function () {
        $('.advanced_search_area').slideToggle();
    });
});

function search_filter_editor() {
    $('.pagination_controllers').addClass('hidden');
    click_id = 0;
    var data = 'view_request=studio_search&search_page_index=' + search_page_index + '&search_page_number=' + search_page_number + '&industry_value=' + industry_value + '&experience=' + experience + '&show_hidden_tyroe_value=' + show_hidden_tyroe_value + '&level=' + level + '&world_wide_value=' + country_id + '&city_id=' + city_id + '&periorty=' + periorty_value + '&search_input=' + input_value + '&availability=' + availability;
    data = data + '&availability_later='+availability_later+"&not_availability="+not_availability+"&skills="+$("#skills").val();
        $('.loader-save-holder').show();

    var result = $.ajax({
        type: 'post',
        data: data,
        url: '<?=$vObj->getURL("search/FilterTyroes")?>',
        success: function (html) {
            $('html,body').animate({ scrollTop: 0 }, 'slow');
            $('.filter_editor').html(html);
            $('.single_person_pagination').html(hidden_fields_ajax);
            if (show_hidden_tyroe_value != '') {
                /*Hidden Area*/
                $('.hide_search').show();
                $('.hide_search').attr('data-tip', 'unhide');
                $('.hide_search').css('background', '#2388d6');
                $('.hide_search .icon-trash').css('color', '#ffffff');
                $('.btn-msg').addClass('right');
                $('.btn-msg').removeClass('middle');
            } else {
                $('.hide_search').show();
                $('.hide_search').attr('data-tip', '');
                $('.btn-msg').addClass('middle');
                $('.btn-msg').removeClass('right');
            }
//            setTimeout(function () {
                $('.loader-save-holder').hide();
                if (click_id == 0) {
                    search_list_inner_html = $('.filter_editor').html();
                    click_id = 1;
                }

//            }, 3000);
        }
    });


}
//End search fillter advance
//selecting single user
var current_profile_user_id = 0;
var gallary_page_no = 1;
function getsingle_user(page_number, search_user_id) {
    var is_profile_access = <?= $has_profile_access ?>;
    page_position = page_number;
    $('.loader-save-holder').show();
    if(is_profile_access != 0){
        $('.pagination_controllers').removeClass('hidden');
        $('.content .profile_container').removeClass('container');
        $('.content .profile_container').addClass('container-fluid');
        $('.content .profile_container.container-fluid').css({"padding": "0"});
    }
    if (click_id == 0) {
        search_list_inner_html = $('.filter_editor').html();
        click_id = 1;
    }
    current_profile_user_id = search_user_id;
    gallary_page_no = 1;
    $.ajax({
        type: 'POST',
        data: ({'current_userid': search_user_id}),
        url: "<?= $vObj->getURL("searchpublicprofile/index"); ?>",
        success: function (html) {
           
            if(is_profile_access != 0){
                $('.single_num_of_records').text(page_position + ' of ' + total_person);
                setTimeout(function () {
                    $('#Search_filters').addClass('rt-profile');
                    $('.filter_editor').html(html);

                    $("#profile_iframe").height($("#profile_iframe").contents().find("body").outerHeight(true));
                    $('#profile_iframe').css({"border": "0"});
                    var iframe_email_controllers_html = $("#single-team").contents().find(".btn_mail_n_remove").html();
                    $('.email_sending_btns').html(iframe_email_controllers_html);
                    $('.loader-save-holder').hide();
                }, 3000);
            }else{
                $('.filter_editor').html(html);
                $('.loader-save-holder').hide();
            }

        }
    });
}
//creating single user pagination
$(document).on("click", ".listview_btn", function () {
    $('#Search_filters').removeClass('rt-profile');
    $('.pagination_controllers').removeClass('hidden');
    /*$('.content .profile_container').removeClass('container-fluid');
     $('.content .profile_container').addClass('container');*/

    $('.loader-save-holder').show();
    $('.filter_editor').html('');
    $('.filter_editor').html(search_list_inner_html);
    $('.pagination_controllers').addClass('hidden');
//    setTimeout(function () {
    $('.loader-save-holder').hide();
//    }, 3000);
    click_id = 0;
});
$(document).on('click', '.single_next', function () {
    page_position++;
    if (page_position > total_person) {
        page_position = 1;
        page_found = $('#user_unique_id' + page_position).attr('data-id');
        page_position = page_found;
        page_user_id = $('#user_unique_id' + page_position).val();
        getsingle_user(page_position, page_user_id);
    } else {
        page_found = $('#user_unique_id' + page_position).attr('data-id');
        page_position = page_found;
        page_user_id = $('#user_unique_id' + page_position).val();
        getsingle_user(page_position, page_user_id);
    }

});
$(document).on('click', '.single_back', function () {
    page_position--;
    if (page_position < 1) {
        page_position = total_person;
        page_found = $('#user_unique_id' + page_position).attr('data-id');
        page_position = page_found;
        page_user_id = $('#user_unique_id' + page_position).val();
        getsingle_user(page_position, page_user_id);
    } else {
        page_found = $('#user_unique_id' + page_position).attr('data-id');
        page_position = page_found;
        page_user_id = $('#user_unique_id' + page_position).val();
        getsingle_user(page_position, page_user_id);
    }
});
//End creating single user pagination
//End selecting single user
function global_search_pagintion(search_page_number_start, page_count) {
    search_page_index = search_page_number_start;
    search_page_number = page_count;
    search_filter_editor();
}
function global_search_pagintion_previous() {
    if (search_page_number > 1) {
        search_page_index -= 12;
        search_page_number--;
        search_filter_editor();
    }
}
function global_search_pagintion_next() {
    search_page_index += 12;
    search_page_number++;
    search_filter_editor();
}
</script>


<!--This div is temporory html holder. for detail ask from zain Do not delete-->
<div class="tempHolder" style="display: none"></div> <!--Always Hide-->
<!-- End This div is temporory html holder. for detail ask from zain Do not delete-->

<div
    style="display: <?php if ($this->input->post('home_featured') == 'featured' || $this->input->post('dashboard_search_keyword') != '') {
        echo 'block';
    } else {
        echo 'none';
    } ?>;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<!-- main container -->
<!-- Element to pop up -->
<div id="element_to_pop_up">
    <div class="job-selection">
        <div class="field-box">
            <label>Select Job:</label>

            <div class="ui-select find">
                <select name="job_id" id="job_id">
                    <option value="">Select Job</option>
                    <?php
                    foreach ($get_jobs as $jobs) {
                        ?>
                        <option value="<?php echo $jobs['job_id']; ?>"><?php echo $jobs['job_title']; ?></option>
                    <?php
                    }
                    ?>
                </select></div>
            <div class="span9">
                <a class="btn-flat user-search" onclick="AddToJob();">SUBMIT</a>
            </div>
        </div>
    </div>

    <div class="contact-form" style="display: none">
        <div class="field-box">
            <label>Contact Form</label>
        </div>
    </div>
</div>

<div class="public-profile" style="display: none">
    <iframe class="public-profile-frame">

    </iframe>
</div>
<div class="content padding-default">
<div class="rt-search-filter">
    <div class="container">
        <div id="Search_filters" class="span12 offset1 margin-left-12">
            <div class="row-fluid">
                <input type="text" id="searchinput" placeholder="SEARCH FOR TALENT USING KEYWORDS..."
                       class="search span9 search_tyroe_field">
                <a href="javascript:void(0)" id="advance_toggle" class="pull-left">Advanced Search <i class="caret"></i>
                </a>

                <div class="clearfix"></div>
                <div class="row-fluid advanced_search_area" style="display: none;">
                    <div class="row-fluid first_filter">
                        <label class="span2"><input type="checkbox" id="search_by_editor_pick">&nbsp;&nbsp;Editor's pick</label>
                        <label class="span2"><input type="checkbox" id="show_hidden_tyroe"
                                                    value="available">&nbsp;&nbsp;Show Hidden</label>
                        <label class="span2"><input type="checkbox" onclick="getavailability()" id="availability"
                                                    value="available" checked="checked">&nbsp;&nbsp;Available now</label>
                        <label class="span2"><input type="checkbox" onclick="getavailabilityLater()" id="availability_later"
                                                    value="avaliable_later" checked="checked">&nbsp;&nbsp;Available Soon</label>
                        <label class="span2"><input type="checkbox" onclick="getavailabilityNot()" id="not_availability"
                                                    value="not_available">&nbsp;&nbsp;Not Available for work</label>
                        
                    </div>
                    <div class="row-fluid first_filter">
                        <span class="span2" style="color:#8c8f98">Skills : </span>
                        <div class="span4">
                            <input type="text" name="skills" placeholder="Search by Skills" id="skills" value="" class="form-control">
                        </div>
                        <label class="span6"><span class="pull-left">Display Order : </span>

                            <div class="field-box span6 bottom-margin">
                                <div class="tab-margin">
                                    <div class="ui-select span12">
                                        <select class="span12 inline-input" onchange="getperiority(this.value)">
                                            <option value="most_recent">Most Recent</option>
                                            <option value="alphabet">Alphabetical</option>
                                            <option value="recent_updated">Recently Updated</option>
                                            <option value="highest_score">Highest Score</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </label>
                    </div>
                    <div class="editor-seperator sep-left-adjust"></div>
                    <div class="row-fluid second_filter">
                        <label class="span2">Location : </label>

                        <div class="field-box span2">
                            <?= $get_countries_dropdown ?>
                        </div>
                        <div class="field-box span2" id="cities_box">
                            <?= $get_cities_dropdown ?>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <span class="span2">Level : </span>
                        <?php $lv_count = 0;
                        foreach ($get_levels as $get_level){
                        if ($lv_count == 5){
                        $lv_count = 0;
                        ?></div>
                    <span class="span2"></span>

                    <div class="row-fluid"><?php } ?>
                        <label class="span2"><input type="checkbox" onclick="getlevel()" class="level"
                                                    value="<?php echo $get_level['id']; ?>"><?php echo " &nbsp" . $get_level['name']; ?>
                        </label>
                        <?php $lv_count++;
                        } ?>

                    </div>
                    <div class="row-fluid forth_filter">
                        <span class="span2">Experience : </span>
                        <?php $ex_count = 0;
                        foreach ($get_experience as $get_exp){
                        if ($ex_count == 5){
                        $ex_count = 0;
                        ?></div>
                    <span class="span2"></span>

                    <div class="row-fluid"><?php } ?>
                        <label class="span2"><input type="checkbox" onclick="getexperience()" class="experience"
                                                    value="<?php echo $get_exp['id']; ?>"><?php echo " &nbsp" . $get_exp['expyear']; ?>
                            year</label>
                        <?php $ex_count++;
                        } ?>
                    </div>

                    <div class="editor-seperator sep-left-adjust"></div>

                    <div class="row-fluid fifth_filter">
                        <span class="span2">Industry : </span>
                        <?php $in_count = 0;
                        foreach ($get_industry as $get_indust){
                        if ($in_count == 5){
                        $in_count = 0;
                        ?></div>
                    <span class="span2"></span>

                    <div class="row-fluid"><?php } ?>
                        <label class="span2"><input type="checkbox" onclick="getindustry()" class="industries"
                                                    value="<?php echo $get_indust['id']; ?>"><?php echo " &nbsp" . $get_indust['name']; ?>
                        </label>
                        <?php $in_count++;
                        } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="editor-seperator"></div>
<div class="pagination_controllers hidden">
    <div class="editor-seperator"></div>
    <div class="container-fluid margin-top-4">
        <div class="row-fluid">
            <div id="Search_filters" class="span12">
                <div class="row">
                    <div id="editors_pick" class="rt-edit-list">
                        <div id="editors_pick_info">
                            <div class="container rt-pagination-sec widthMax94percent">
                                <div class="span4 offset1 ">
                                    <button class="btn btn-primary listview_btn list-rt-btn">LIST VIEW</button>
                                    <div class="btn-group">
                                        <button class="btn btn-default single_back glow left" type="button"><i
                                                class="icon-angle-left"></i></button>
                                        <button class="btn btn-default single_next glow right" type="button"><i
                                                class="icon-angle-right"></i></button>
                                    </div>
                                    <span class="single_num_of_records"></span>
                                </div>
                                <div class="span4 pull-right">
                                    <div class="email_sending_btns pull-right btn-group large ">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="editor-seperator"></div>
</div>
<div class="clearfix"></div>

<div class="container-fluid profile_container padding-default">


<div id="pad-wrapper" class="no-padding">

<div class="grid-wrapper">

<div class="row-fluid show-grid">


<!-- START RIGHT COLUMN -->
<!--This area for single layout pagination purpose-->
<div class="single_person_pagination">
    <?php
    if ($page_number > 1) {
        $pos_id = 0;
    } else {
        $pos_id = 0;
    }
    
    if($get_rows_for_pagination != ''){
        for ($tyr = $pos_id; $tyr < count($get_rows_for_pagination); $tyr++) {
        ?>
        <input type="hidden" id="user_unique_id<?php echo $pos_id + 1; ?>" class="users_ids"
               data-id="<?php echo $pos_id + 1; ?>"
               value="<?= $get_rows_for_pagination[$tyr]['user_id'] ?>"/>
        <?php $pos_id++;
    }  }?>
</div>
<!--End single layout pagination purpose-->
<div class="">
    <div class="right-column filter_editor no-padding" style="position:relative;">

        <?php
        if ($page_number > 1) {
            $pos_id = (($page_number - 1) * 12) + 1;
        } else {
            $pos_id = 1;
        }
        
        if($get_tyroes != ''){
        
        for ($tyr = 0; $tyr < count($get_tyroes); $tyr++) {
            $main_user_id = $get_tyroes[$tyr]['user_id'];
            $image;
            if ($get_tyroes[$tyr]['media_name'] != "") {
                $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_tyroes[$tyr]['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            if(isset($has_profile_access) && $has_profile_access == 0){
                $image = ASSETS_PATH_NO_IMAGE;
            }
            ?>
            <div class="row-fluid rg-bg hiderow<?= $get_tyroes[$tyr]['user_id'] ?>">

                <div class="container">
                    <div class="span12 editor-pck row-id<?= $get_tyroes[$tyr]['user_id'] ?>">
                        <div class="row-fluid ">
                            <div class="span12" id="top">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="edt_pck_sec">
                                            <div class="user-dp-holder">
                                                <a href="javascript:void(0)"
                                                   onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)" style="position: relative;display: inline-block;">
                                                    <img
                                                        src="<?php echo $image; ?>"
                                                        class="img-circle avatar-80">
                                                
                                                
                                                <?php
                    if (intval($get_tyroes[$tyr]['availability']) == 1) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#96bf48" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE FOR WORK"></i>
                    <?php
                    } else if (intval($get_tyroes[$tyr]['availability']) == 2) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#5ba0a3" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE SOON"></i>
                    <?php
                    } else if (intval($get_tyroes[$tyr]['availability']) == 3) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i>
                    <?php
                    }else{
                    ?>
                       <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i> 
                    <?php    
                    }
                    ?>
                                                </a>
                                            </div>
                                            <div class="editors_picked" id="editors_pick">
                                                <div class="editors_pick-info" id="editors_pick_info">
                                                    <!--<a href="javascript:void(0);" class="name" value="<?/*=$main_user_id*/?>"><?php /*echo $get_tyroes[$tyr]['username']; */?></a>-->
                                                    <a href="javascript:void(0)"
                                                       onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)"
                                                       class="name user_triger_id<?php echo $get_tyroes[$tyr]['user_id']; ?>">
                                                           <?php if(isset($has_profile_access) && $has_profile_access == 0){
                                                                    echo "Name Hidden";
                                                                }else {
                                                                    echo $get_tyroes[$tyr]['firstname'] . ' ' . $get_tyroes[$tyr]['lastname'];
                                                                }?></a>
                                            <span class="location"><?php
                                                /*for ($a = 0; $a < count($get_tyroes[$tyr]['skills']); $a++) {
                                                    $temp = $get_tyroes[$tyr]['skills'][$a]['creative_skill'];
                                                    echo ($a > 0) ? ", " : "";
                                                    if (!empty($temp)) {
                                                        echo $temp;
                                                    }

                                                }*/
                                                echo rtrim($get_tyroes[$tyr]['industry_name'] . ', ' . $get_tyroes[$tyr]['extra_title'], ', ');
                                                ?></span>
                                                    <span class="tags"><?php echo $get_tyroes[$tyr]['city'] . ', ' . $get_tyroes[$tyr]['country']; ?></span>
                                                    <br clear="all">
                                                      <?php
                                                        $class = 'left';
                                                        
                                                        if(isset($has_profile_access) && $has_profile_access == 1) {
                                                            $class = 'middle'
                                                                    ?>
                                                    <div class="btn-group large">
                                                      
                                                        <button class="glow left" data-toggle="dropdown"><i
                                                                class="icon-plus-sign"></i></button>
                                                        <ul class="dropdown-menu">

                                                            <li><a href="javascript:void(0)" class=""
                                                                   style="color:#999999"><?= "Add to opening:"; ?></a>
                                                            </li>
                                                             <?php 
                                                                 if($get_openings_dropdown != ''){
                                                                 for ($op = 0; $op < count($get_openings_dropdown); $op++) { ?>
                                                                <?php $duplicate_job = 0;
                                                                    if($get_tyroes[$tyr]['tyroe_jobs'] != ''){
                                                                foreach ($get_tyroes[$tyr]['tyroe_jobs'] as $tyroe_job) {
                                                                    ?>
                                                                    <?php if ($tyroe_job['job_id'] == $get_openings_dropdown[$op]['job_id']) {
                                                                        $duplicate_job = 1;

                                                                        ?>
                                                                        <li><a href="javascript:void(0)"
                                                                               class="disable_opening"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                        </li>
                                                                    <?php } ?>
                                                                <?php } }?>
                                                                <?php if ($duplicate_job == 0) { ?>
                                                                    <li><a href="javascript:void(0)"
                                                                           class="add_in_opening_jobs"
                                                                           data-value="<?= $get_openings_dropdown[$op]['job_id'] . "," . $get_tyroes[$tyr]['user_id']; ?>"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } }?>
                                                        </ul>
                                                      
                                                        <?php if ($get_tyroes[$tyr]['is_favourite'] == '') { ?>
                                                            <button class="glow <?= $class ?> fav_profile star_btn"
                                                                    data-value="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                <i class="icon-star"></i></button>
                                                        <?php } else { ?>
                                                            <button class="glow <?= $class ?> fav_profile"
                                                                    data-value="<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                                    style="background: #2388d6;"><i
                                                                    class="icon-star" style="color:#ffffff"></i>
                                                            </button>
                                                        <?php } ?>
                                                        <button class="glow middle btn-msg"
                                                                data-value="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                            <i class="icon-envelope "></i></button>
                                                        
                                                        <button class="glow right hide_search"
                                                                data-value="<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                                data-tip="">
                                                            <i class="icon-trash"></i></button>
                                                        <input type="hidden" name="hd_email_to"
                                                               id="hd_email_to<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                               value=""/>
                                                        
                                                    </div>
                                                    <?php } ?>
                                                </div>
                                                <div class="editors_showcase-container" id="editors-showcase">
                                                    <div class="editors_showcase_holder">
                                                        <div
                                                            class="full-profile_overlay black_overlay_separate<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                            style=""
                                                            onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)">
                                                            <div class="full-profile_overlay_holder">
                                                                <p>View Full Profile</p>
                                                                <i class="icon-picture"></i>
                                                            </div>
                                                        </div>

                                                        <div class="row-fluid">
                                                            <?php
                                                            for ($a = 0; $a < 3; $a++) {
                                                                if($get_tyroes[$tyr]['portfolio'][$a]['media_name']){
                                                                   $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_tyroes[$tyr]['portfolio'][$a]['media_name'];
                                                                   if (!file_exists($portfolio)) {
                                                                       $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                   }else{
                                                                       $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_tyroes[$tyr]['portfolio'][$a]['media_name'];
                                                                   }
                                                                }else{
                                                                    $portfolio = ASSETS_PATH.'/img/image-icon.png';
                                                                }
                                                                ?>
                                                                <div class="showcase_thumb pull-right">
                                                                    <a href="javascript:void(0);"
                                                                       class="score_img_anchor<?= $a ?>"
                                                                       data-id="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                        <?= (($a == 2) && ($get_tyroes[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                        <?php if ($a == 0) { ?>
                                                                            <?php if ($get_tyroes[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                                <div
                                                                                    class="tyroe_score_over_img tyroeScoreHolder<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                                    <div
                                                                                        class="tyroe_score_holder"><?= $get_tyroes[$tyr]['get_tyroes_total_score'] ?></div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <img
                                                                            src="<?php echo $portfolio ?>"
                                                                            alt="editor-picks-<?= $get_tyroes[$tyr]['is_user_featured'] ?>">
                                                                    </a>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
                <div class="editor-seperator editor-seperator-row<?= $get_tyroes[$tyr]['user_id'] ?>"></div>
            </div>
            <?php
            $pos_id++;
        } }
        ?>

        <!-- START PAGINATION -->
        <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
              method="post" action="<?= $vObj->getURL('search') ?>">
            <div class="span11 pagination rt-pg-supporter"><?= $pagination ?></div>
            <input type="hidden" value="<?= $page ?>" name="page">
        </form>
    </div>
</div>
</div>
</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>

<!-- end main container -->

<div style="display: none;" class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>

                </div>
                <div class="span8 email_content_body">
                    <div class="global-popup_body ">
                        <div class="center prof-dtl">
                            <div class="tab-margin">
                                <div class="sugg-msg alert alert-info" style="display: none"></div>
                                <div class="clearfix"></div>
                                <div class="field-box">
                                    <input type="text" name="email-subject" id="email-subject" placeholder="Subject"/>
                                </div>
                                <div class="field-box">
                                    <textarea name="" id="" class="email-message" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="global-popup_footer">
                        <button type="button" class="btn btn-flat white email_close" style="display: none"
                                data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-flat btn-save btn-send-email" value="<?= LABEL_SUBMIT ?>">
                            <?= LABEL_SUBMIT ?>
                        </button>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>