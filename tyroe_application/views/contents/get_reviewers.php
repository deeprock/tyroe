<?php if($get_reviewers != ''){ ?>
    <?php for($a=0; $a<count($get_reviewers); $a++){
       if($get_reviewers[$a]['user_id'] == $this->session->userdata('user_id')){
           continue;
       }
        $image;
        if ($get_reviewers[$a]['media_name'] != "") {
            $image = ASSETS_PATH_PROFILE_THUMB . $get_reviewers[$a]['media_name'];
        } else {
            $image = ASSETS_PATH_NO_IMAGE;
        }
        ?>
        <div class="reviewers review-mg">
            <div class="span2">
                <!--<img src="<?php /*echo $image; */?>" class="img-circle avatar-50 pull-right">-->
            </div>
            <div class="span3 reviewer-text-sp">
                    <div class="clearfix"></div>
                <span class="reviewer_name"><?php echo $get_reviewers[$a]['firstname'].' '.$get_reviewers[$a]['lastname']; ?></span>
                <div class="clearfix"></div>
                <span class="designation"><?php echo $get_reviewers[$a]['job_title']; ?></span>
                <div class="clearfix"></div>
                <?php if($get_reviewers[$a]['action_interview'] != ''){ ?>
                    <span class="label label-info"><?php echo $get_reviewers[$a]['action_interview']; ?></span>
                <?php } ?>
                <?php if($get_reviewers[$a]['action_shortlist'] != ''){ ?>
                    <span class="label label-info"><?php echo $get_reviewers[$a]['action_shortlist']; ?></span>
                <?php } ?>
                <div class="clearfix"></div>
                <div id="editors_pick" class="edit-mg">
                <div id="editors_pick_info">
                <div class="btn-group large group_icons ft-btn-sp ft-ms">
                    <button class="glow btn-msg right" data-value="<?=$get_reviewers[$a]['user_id']?>"><i class="icon-envelope "></i></button>
                </div></div></div>


            </div>
            <div class="span4">
                <?php if($get_reviewers[$a]['job_id'] != ''){?>
                    <p><b><?php echo $level_result =($get_reviewers[$a]['level_title']=='Intern') ? 'Entry Level' : $get_reviewers[$a]['level_title']; ?>: </b><?php echo nl2br($get_reviewers[$a]['review_comment']); ?></p>
                <?php }else{ ?>
                    <span class="btn btn-danger rt-danger">Review pending</span>
                <?php } ?>
            </div>
            <?php if($get_reviewers[$a]['job_id'] != ''){ ?>
            <div class="span2 rt-reviewer-score"><span><?php echo round($get_reviewers[$a]['review_average']); ?></span></div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
<?php } ?>