<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>
<script type="text/javascript">
    var original_bg_color = null;
    var original_text_color = null;
    var profile_theme_icons = null;
    var profile_icons_rollover = null;
    var gallery_theme_text = null;
    var gallery_theme_background = null;
    var gallery_theme_overlay = null;
    var resume_theme_text = null;
    var resume_theme_background = null;
    var service_box_bg =null;
    var service_box_text = null;
    var feature_text = null;
    var feature_BGA = null;
    var feature_BGB = null;
    var footer_theme_backgroundA = null;
    var footer_theme_icon = null;
    var footer_theme_icons_rollover = null;


    service_box_text = '<?=$this->session->userdata['gallery_theme']['text']?>';
    service_box_overlay = '<?=$this->session->userdata['gallery_theme']['overlay']?>';
    service_box_bg = '<?=$this->session->userdata['gallery_theme']['background']?>';

    feature_text = '<?=$this->session->userdata['featured_theme']['text']?>';
    feature_BGA = '<?=$this->session->userdata['featured_theme']['backgroundA']?>';
    feature_BGB = '<?=$this->session->userdata['featured_theme']['backgroundB']?>';

    profile_theme_icons = '<?=$this->session->userdata['profile_theme']['icons']?>';
    original_bg_color = '<?=$this->session->userdata['profile_theme']['background']?>';
    original_text_color = '<?=$this->session->userdata['profile_theme']['text']?>';
    profile_icons_rollover = '<?=$this->session->userdata['profile_theme']['icons_rollover']?>';
    gallery_theme_background = '<?=$this->session->userdata['gallery_theme']['background']?>';
    gallery_theme_overlay = '<?=$this->session->userdata['gallery_theme']['overlay']?>';
    gallery_theme_text = '<?=$this->session->userdata['gallery_theme']['text']?>';
    resume_theme_text = '<?=$this->session->userdata['resume_theme']['text']?>';
    resume_theme_background = '<?=$this->session->userdata['resume_theme']['background']?>';
    footer_theme_backgroundA = '<?=$this->session->userdata['footer_theme']['backgroundA']?>';
    footer_theme_icon = '<?=$this->session->userdata['footer_theme']['icons']?>';
    footer_theme_icons_rollover = '<?=$this->session->userdata['footer_theme']['icons_rollover']?>';

    function GetStates(ddname) {
        var country_name = $("#user_country option:selected").val();
        var data = "country=" + country_name + "&ddname=" + ddname;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("profile/get_state_by_country")?>',
            success: function (data) {
                $(".all-states").remove();
                $(".user_state1").html("");
                $(".result-states").html(data);
            }
        });
    }

    function publish_unpublish_btn(data_fullstatus){
        $('#fullstatus').val(data_fullstatus);
        var publish_account = $('#publish_account').val();
        if(data_fullstatus >= 75 && publish_account == 0)
        {
            var show_profile_publish_box = $('#show_profile_publish_box').val();
            if(show_profile_publish_box == 1){
                $('#publish-profile-model').modal('show');
                $(".btn_publish").removeClass('btn-disabled');
                $('#show_profile_publish_box').attr('value',0);
            }
            
//            var show_profile_publish_box = $('#show_profile_publish_box').val();
//            var show_profile_share_box = $('#show_profile_share_box').val();
//            
//            $(".btn_publish").removeClass('btn-disabled');
//            var publish_account = $('#publish_account').val();
//            if(publish_account == 1){
//                if(show_profile_share_box == 1){
//                    $('#share-publish-profile-model').modal('show');
//                    $('#show_profile_share_box').attr('value',0);
//                }
//                $(".btn_publish").removeClass("new-nav-btn1-color");
//                $(".btn_publish").html("<span>UNPUBLISH</span>");
//            } else {
//                if(show_profile_publish_box == 1){
//                   $('#publish-profile-model').modal('show');
//                   $('#show_profile_publish_box').attr('value',0);
//                }
//                $(".btn_publish").addClass("new-nav-btn1-color");
//                $(".btn_publish").html("<span>PUBLISH</span>");
//            }
        }else if(data_fullstatus < 75 && publish_account == 0){
            $(".btn_publish").addClass('btn-disabled');
            $('#show_profile_publish_box').attr('value',1);
        } else {
//            $(".btn_publish").html("<span>PUBLISH</span>");
//            $('#show_profile_publish_box').attr('value',1);
//            $('#show_profile_share_box').attr('value',1);
//            if(!$(".btn_publish").hasClass('btn-disabled')){
//                $(".btn_publish").addClass('btn-disabled');
//            }
//            $(".btn_publish").html("<span>PUBLISH</span>");
//            var btn_publish = 0;
//            var data="publish_account="+btn_publish+"&callfrom=publish_account";
//            $.ajax({
//                type: 'POST',
//                data: data,
//                url: '<?=$vObj->getURL("profile/save_profile");?>',
//                dataType: 'json',
//                success: function (responseData) {
//                    if (responseData.status) {
//                        $('.loader-save-holder').hide();
//                        $('#publish_account').val(0);
//                    }
//                }
//            });
        }
    }

//    function update_knob(fullstatus_value){
//        $(".padding-default-center").html("");
//        $(".padding-default-center").html('<input id="score-chart-input-field" type="text" value="" class="knob" data-thickness=".3" data-inputColor="#333" data-readonly="true" data-fgColor="#76BDEE" data-bgColor="#F97E76" data-width="220" data-height="220">');
//        $("#score-chart-input-field").val(fullstatus_value);
//        $(".knob").knob();
//        $("#score-chart-input-field").val(fullstatus_value+"%");
//    }
    
    function update_profile_completness_top_chart(prof_comp){
        $("#profile_completness_top_chart").html('<input class="edit_profile_completness_knob" type="text" value="" data-thickness=".3" data-width="200" data-height="200" data-fontsize="22" data-inputColor="#333" data-readonly="true" data-fgColor="#30a1ec" data-bgColor="#d4ecfd">');
        $(".edit_profile_completness_knob").val(prof_comp);
        $('.edit_profile_completness_knob').css({
            'box-shadow' : 'none'
        });
        $(".edit_profile_completness_knob").knob();
    }
</script>
<script>
    $(document).ready(function(){
	 $('body').addClass('change');
        publish_unpublish_btn(<?=$fullstatus?>);
        $("#score-chart-input-field").val(<?=$fullstatus?>);
        $(".knob").knob();
        $("#score-chart-input-field").val(<?=$fullstatus?>+"%");
    })
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    $(document).on('click', '.remove-btn', function (e) {
        $(this).parent().remove()
    });
    jQuery(document).ready(function () {
        $(".warm-video").hide();
        $(".warm-two").hide();
        /*Cloning*/
        jQuery(".clone-btn").click(function () {
            var clone_html = jQuery(this).parent().html();
            var clone_htmls = clone_html.replace('<input name="site_btn[]" class="btn f-right cus-btn-mov clone-btn" value="+ADD SITE" type="button">', '<input name="remove_btn" class="btn f-right cus-btn-mov remove-btn" value="-REMOVE" type="button">');
            var clone_div = '<div class="span12 field-box c-magic custom-borders clone-div">' + clone_htmls + '</div>';
            jQuery('.actions').before(clone_div);
        });
        /**/
        jQuery(".next").click(function () {
            $(".warm-one").hide();
            $(".warm-video").hide();
            $(".warm-two").show();
        });
        jQuery(".btn-watch-video").click(function () {
            $(".warm-one").hide();
            $(".warm-video").show();
            $(".warm-two").hide();
        });

        jQuery(".btn-ready").click(function () {
            $(".warm-welcome-container").hide();
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#user_cellphone").keyup(function () {
            var cell = jQuery("#user_cellphone").val()
            if (!$.isNumeric(cell)) {
                $("#user_cellphone").val("");
                $("#user_cellphone").attr("placeholder", "Please enter numeric values");
            }
        })

        //ProfileCompleteness fix style
        var browser='<?php echo $this->agent->browser();?>';
        if(browser=="Chrome"){
            $(document).on('click','.ProfileCompleteness',function(e){
                $('svg text').attr("transform","matrix(1.7037,0,0,1.7037,-116.1111,-87.2593)"); //firefox new
                $('svg text').attr("stroke-width",'0.5652173913043479');
                $('svg tspan').attr("dy",'5');
            });
        }

        $("#save_profile_img").click(function () {
            var imgname = $("#imgname").val();
            var data = "imgname=" + imgname;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("profile/save_image")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        var image_value_id = $("#image_id").val(data.image_id);
                        if (data.image_id != "") {
                            $("#pro_image_avatar").css("display", "block");
                            $(".old_img").attr("src", "");
                            $('#hidden_image_id').val(data.image_id);
                            $("#uploading").css('display','none');
                            $('.model-cropImage-btn-save').css("display","");
                            //$('.model-cropImage-btn-save').removeAttr("disabled");
                        }
                        else {
                            $(".btn-danger").css("display", "none");
                            $(".img-circle").attr('src', '/assets/img/default-avatar.png')
                        }
                    }
                    else {
                        //$(".files").show();
                    }

                },
                failure: function (errMsg) {

                }
            });
        })

        $(".profile_image").click(function () {

            var imgname = $("#image_id").val();
            var data = "image_id=" + imgname;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("profile/delete_photo")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".profile_img").attr("src", '<?=ASSETS_PATH_NO_IMAGE?>');
                        $(".delete-img").hide();
                        $(".abc").addClass('alert alert-success');
                        $("#pro_image_avatar").removeAttr("style");
                        $("#new_image").css("display", "none");
                        $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                        setTimeout(function () {
                            $(".abc").removeClass("alert alert-success");
                            $(".abc").html("");
                            $(".abc").show();
                        }, 2000);
                    }
                    else {
                        $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                        $("#fileupload").attr("enabled", "enabled");
                        setTimeout(function () {
                            $(".abc").removeClass("alert alert-success");
                            $(".abc").html("");
                            $(".abc").show();
                        }, 2000);
                    }
                },
                failure: function (errMsg) {
                }
            });
        })

        /*APPLY THEMES START*/
        $('.btn-apply-changes').click(function(){
            var p1_text = $('.colorSelectorP1 div.inner').css('background-color');
            var p2_bg = $('.colorSelectorP2 div.inner').css('background-color');
            var p3_icons = $('.colorSelectorP3 div.inner').css('background-color');
            var p4_icon_rollover = $('.colorSelectorP4 div.inner').css('background-color');
            var f1_text = $('.colorSelectorF1 div.inner').css('background-color');
            var f2_bga = $('.colorSelectorF2 div.inner').css('background-color');
            var f3_bgb = $('.colorSelectorF3 div.inner').css('background-color');
            var g1_text = $('.colorSelectorG1 div.inner').css('background-color');
            var g2_bg = $('.colorSelectorG2 div.inner').css('background-color');
            var g3_overlay = $('.colorSelectorG3 div.inner').css('background-color');
            var r1_text = $('.colorSelectorR1 div.inner').css('background-color');
            var r2_bg =$('.colorSelectorR2 div.inner').css('background-color');
            var foot1_icons = $('.colorSelectorFoot1 div.inner').css('background-color');
            var foot2_icons_rollover = $('.colorSelectorFoot2 div.inner').css('background-color');
            var foot3_bga = $('.colorSelectorFoot3 div.inner').css('background-color');

            var data = "p1_text="+p1_text+"&p2_bg="+p2_bg+"&p3_icons="+p3_icons+"&p4_icon_rollover="+p4_icon_rollover+
                "&f1_text="+f1_text+"&f2_bga="+f2_bga+"&f3_bgb="+f3_bgb+
                "&g1_text="+g1_text+"&g2_bg="+g2_bg+"&g3_overlay="+g3_overlay+
                "&r1_text="+r1_text+"&r2_bg="+r2_bg+
                "&foot1_icons="+foot1_icons+"&foot2_icons_rollover="+foot2_icons_rollover+"&foot3_bga="+foot3_bga;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("profile/theme_set")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        original_bg_color =  p2_bg;
                        original_text_color = p1_text;
                        $(".btn-apply-changes").text("Saved");
                        setTimeout(function() {
                            $(".btn-apply-changes").text("Apply Changes");
                        }, 2000);
                    }
                },
                failure: function (errMsg) {
                }
            });
        });
        /*APPLY THEMES END*/
        $(".dt-show-text").click(function(){
            $(".warm-welcome-container").css('display','none');
            $.ajax({
                type: "POST",
                data: 'callfrom=warm_welcome',
                url: '<?=$vObj->getURL("profile/save_profile")?>',
                dataType: "json",
                success: function (data) {
                }
            });
        });
        
        
        $('.select-available-option').click(function() {
            var option_id = $(this).val();
            if (!$(this).hasClass('active')) {
                $('.select-available-option').prop('checked', false);
                $('.select-available-option').removeClass('active');
                $('.select-available-option').parent().removeClass('selected');
                $(this).addClass('active');
                $(this).parent().addClass('selected');
                $(this).prop('checked', true);
                
                var data = "availability=" + option_id + "&callfrom=availablity";
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: '<?=$vObj->getURL("profile/save_profile");?>',
                    dataType: 'json',
                    success: function (responseData) {
                        if (responseData.status) {
                            $('.loader-save-holder').hide();
                            if(responseData.total_rows > 0){
                            $(".single-team").find($(".complete-tick-mark")).css("color","#96BF48");
                            }else{
                                 $(".single-team").find($(".complete-tick-mark")).css("color","");
                            }
                            var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                            update_profile_completness_top_chart(prof_comp);
                            publish_unpublish_btn(responseData.scorer.fullstatus);
                            switch(parseInt(option_id)){
                                case 1:
                                    $("#edit_web_link_button").html('AVAILABLE FOR WORK');
                                    $("#edit_web_link_button").css('background-color','#96bf48');
                                    break;
                                case 2:
                                    $("#edit_web_link_button").html('AVAILABLE SOON');
                                    $("#edit_web_link_button").css('background-color','#5ba0a3');
                                    break;
                                case 3:
                                    $("#edit_web_link_button").html('NOT AVAILABLE');
                                    $("#edit_web_link_button").css('background-color','#b85e80');
                                    break;
                            }
                            
                        }
                    }
                });
                
                
                
            } else {
                $(this).parent().addClass('selected');
                $(this).prop('checked', true);
            }
            $('#availability_option').val(option_id);
            $('#availability_mark').removeClass('one');
            $('#availability_mark').removeClass('two');
            $('#availability_mark').removeClass('three');

            if(option_id == 1){
                $('#availability_mark').addClass('one');
            }

            if(option_id == 2){
                $('#availability_mark').addClass('two');
            }

            if(option_id == 3){
                $('#availability_mark').addClass('three');
            }
        });
    });
</script>
<!-- POP-UP -->


<link rel="stylesheet" href="<?= ASSETS_PATH ?>jQueryFileUploader/css/jquery.fileupload.css">
<script type="text/javascript" src="<?=ASSETS_PATH?>js/jquery.sticky.js"></script>
<style>
    .bar {
        height: 18px;
        background: green;
    }
</style>
<?php
if ($login_history_cnt == 1) {
    ?>
    <div class="warm-welcome-container">
        <div class="welcome-overlay"></div>
        <div class="welcome-wrapper warm-one ">
            <h4>Hey <?php echo $this->session->userdata('firstname'); ?>!</h4>

            <h1>Welcome to Tyroe</h1>

            <p>Our "what you see is what you get" editor lets you create a profile page exactly as others will see it.
                Drag and drop to add your best images, embed a cool video, and add some information about your skills,
                work experience, and other information that will impress recruiters. Once done, hit Publish and get
                yourself head hunted. </p>

            <div class="welcome-buttons-container">
                <a class="next btn btn-large btn-default" href="javascript:void(0);">Next</a>
                <a class="demo btn btn-watch-video btn-large btn-default" href="javascript:void(0);">Watch demo</a>
                <div class="clearfix"></div>
                <a class="dt-show-text" href="javascript:void(0);">Got it! Don't show me again.</a>
            </div>
        </div>

        <div class="warm-menu warm-two" style="display: none">
            <div class="container">
                <div class="row-fluid">
                    <div class="warm-menu-holder span12">
                        <h4>Interactive Editor Menu</h4>
                        <img src="assets/img/menu-welcome.png" alt="Interactive-menu">

                        <div class="span12 ready-con">
                            <a class="button-main button-large btn new-nav-btn1 btn-flat btn-ready primary btn-success" href="javascript:void(0);">I'm ready!</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="warm-video" style="display: none">
            <div class="container">
                <div class="row-fluid">
                    <div class="warm-video-holder span12">
                        <iframe class="span12"
                                src="https://player.vimeo.com/video/9865011?byline=0&amp;portrait=0&amp;badge=0&amp;color=FF6056"></iframe>

                        <div class="span12 ready-con">
                            <a class="button-main button-large btn btn-ready new-nav-btn1 btn-flat primary">I'M READY</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>




    </div>


<?php } ?>
<link rel="stylesheet" href="<?=ASSETS_PATH?>css/jquery.Jcrop.css" type="text/css" />

<script src="<?= ASSETS_PATH ?>js/jquery.Jcrop.min.js"></script>

<style>
    .sticky {
        position: fixed;
        width: 86.8%;
        right: 0;
        display: block;
        top: 0;
        z-index: 100;
        border-top: 0;
    }
</style>

<script type="text/javascript">
    function stick_set(){
        var content_left_margin = $(".content").css("margin-left");
        var res = $(".content").width();
        $(".sticky").css("width",res);
    }

    function mobile_stick_set(){
        var main_width = $(window).width();
        $(".sticky").css("width",main_width);
    }


    $(document).ready(function() {
        var stickyNavTop = $('#navbar-sticky').offset().top;
        var window_size = $(window).width();
        var stickyNav = function(){
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbar-sticky').addClass('sticky');
                if(window_size > 979){
                    stick_set();
                } else {
                    mobile_stick_set();
                }
            } else {
                $('#navbar-sticky').removeClass('sticky');
                var res = $(".content").width();
            }
        };

        stickyNav();

        $(window).scroll(function() {
            stickyNav();
        });

        /*on toggle set sticky setting*/
        $(window).resize(function(){
            var window_size = $(window).width();
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbar-sticky').addClass('sticky');
                if(window_size > 979){
                    stick_set();
                } else {
                    mobile_stick_set();
                }
            } else {
                $('#navbar-sticky').removeClass('sticky');
                var res = $("#single-team-section").width();
                $("#navbar-sticky").css("width",res);
                var res = $(".content").width();
                if(window_size <= 979){
                    var res = $(window).width();
                    setTimeout(
                            function()
                            {
                                $("#navbar-sticky").css("width",$(window).width());
                            }, 100);
                }
                else if(window_size >= 980){
                    var navbar = $("#navbar-sticky").width();
                    var res = $("#single-team-section").width();
                    var setting = navbar - res;
                    if( (setting >= 0) && (setting <= 178) ){
                        res = res - 178;
                    }
                    setTimeout(
                            function()
                            {
                                $("#navbar-sticky").css("width",$("#single-team-section").width());
                            }, 100);
                }
                $("#navbar-sticky").css("width",res);
            }
        });


        $(".btn-toggler").click(function(){
            var scrollTop = $(window).scrollTop();
            if (scrollTop > stickyNavTop) {
                $('#navbar-sticky').addClass('sticky');
                stick_set();
            } else {
                //console.log("onTop");
            }
        });
    });
</script>
<input type="hidden" id="show_profile_publish_box" value="1">
<input type="hidden" id="show_profile_share_box" value="1">
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="public-profile"></div>

<section class="content">

<div id="navbar-sticky" class="navbar navbar-inverse">
    <div class="navbar-inner nav-fixed-top">
        <div class="pull-left layer1">
            <button type="button" class="button-main button-large custom-popup-box ProfileCompleteness" >
                <span class="icon-align-left"></span>
            </button>
            <div style="display: none;" class="pop-dialog">
                <div class="pointer">
                    <div class="arrow"></div>
                    <div class="arrow_border"></div>
                </div>
                <div class="body">
                    <a class="close-icon" href="#"><i class="icon-remove-sign"></i></a>
                    <div class="notifications">
                        <h3>Profile Completeness</h3>
                        <a class="item padding-default-left" href="#">
                            <div class="row-fluid sections ui-elements chart-top-holder">
                                <div class="span8 knobs">
<!--                                    <div id="hero-donut" class="knob-wrapper round-chart-top">

                                    </div>-->
                                    <div id="profile_completness_top_chart" style="width: 100%;margin: 10% 24%;">
                                          <input class="edit_profile_completness_knob" type="text"
                                            value="" data-thickness=".3" data-width="200" data-height="200" data-fontsize="22" data-inputColor="#333"
                                            data-readonly="true" data-fgColor="#30a1ec" data-bgColor="#d4ecfd">
                                    </div>
                                </div>
                            </div>
                        </a>
                        <?php
                            $profile_com=$profile_completness['portfolio_scorer']+$profile_completness['resume_scorer']+$profile_completness['profile_scorer'];
                            if($get_user['publish_account'] != 1){
                        ?>
                        <div class="footer" id="profile_completeness_message">
                            <p class="custom-p-color"><?php 
                                if($profile_com < 75){
                                    echo "Reach 75% and get published";
                                }else  {
                                    echo "Congratulations, you can publish your profile!";
                                }
                                ?></p>
                        </div>
                                    <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="pull-left layer1"><button type="button" class="button-main button-large trigger new-nav-btn1 offset-custom-1 custom-popup-box previe-options" >
                <span class="icon-tint"></span>
            </button>
            <div style="display: none;" class="pop-dialog">
                <div class="pointer">
                    <div class="arrow"></div>
                    <div class="arrow_border"></div>
                </div>
                <div class="body">
                    <a class="close-icon" href="javascript:void(0);"><i class="icon-remove-sign"></i></a>
                    <div class="notifications">
                        <h3>Color Schema</h3>
                        <a class="item schematrigger color-link" href="javascript:void(0);">
                            <i class="icon-tint"></i> Profile
                            <span class="pull-right ico-adjust-right"><i class="caret"></i></span>
                            <div class="schemaholder">
                                <div class="color-swatch">
                                    <div class="colorSelectorP1"><div class="inner" style="background-color: <?=$this->session->userdata['profile_theme']['text']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Text</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorP2"><div class="inner" style="background-color: <?=$this->session->userdata['profile_theme']['background']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg1</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorP3"><div class="inner" style="background-color: <?=$this->session->userdata['profile_theme']['icons']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Icons</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorP4"><div class="inner" style="background-color: <?=$this->session->userdata['profile_theme']['icons_rollover']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p class="doubled">Icons Rollover</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <a class="item schematrigger color-link" href="javascript:void(0);">
                            <i class="icon-tint"></i> Featured
                            <span class="pull-right ico-adjust-right"><i class="caret"></i></span>
                            <div class="schemaholder">
                                <div class="color-swatch">
                                    <div class="colorSelectorF1"><div class="inner" style="background-color: <?=$this->session->userdata['featured_theme']['text']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Text</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorF2"><div class="inner" style="background-color: <?=$this->session->userdata['featured_theme']['backgroundA']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg 1</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorF3"><div class="inner" style="background-color: <?=$this->session->userdata['featured_theme']['backgroundB']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg 2</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <a class="item schematrigger color-link" href="javascript:void(0);">
                            <i class="icon-tint"></i> Gallery
                            <span class="pull-right ico-adjust-right"><i class="caret"></i></span>
                            <div class="schemaholder">
                                <div class="color-swatch">
                                    <div class="colorSelectorG1"><div class="inner" style="background-color: <?=$this->session->userdata['gallery_theme']['text']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Text</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorG2"><div class="inner" style="background-color: <?=$this->session->userdata['gallery_theme']['background']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorG3"><div class="inner" style="background-color: <?=$this->session->userdata['gallery_theme']['overlay']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Overlay</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <a class="item schematrigger color-link" href="javascript:void(0);">
                            <i class="icon-tint"></i> Resume
                            <span class="pull-right ico-adjust-right"><i class="caret"></i></span>
                            <div class="schemaholder">
                                <div class="color-swatch">
                                    <div class="colorSelectorR1"><div class="inner" style="background-color: <?=$this->session->userdata['resume_theme']['text']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Text</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorR2"><div class="inner" style="background-color: <?=$this->session->userdata['resume_theme']['background']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <a class="item schematrigger color-link" href="javascript:void(0);">
                            <i class="icon-tint"></i> Footer
                            <span class="pull-right ico-adjust-right"><i class="caret"></i></span>
                            <div class="schemaholder">
                                <div class="color-swatch">
                                    <div class="colorSelectorFoot1"><div class="inner" style="background-color: <?=$this->session->userdata['footer_theme']['icons']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Icon</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorFoot2"><div class="inner" style="background-color: <?=$this->session->userdata['footer_theme']['icons_rollover']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p class="doubled">Icon Rollover</p>
                                </div>
                                <div class="color-swatch">
                                    <div class="colorSelectorFoot3"><div class="inner" style="background-color: <?=$this->session->userdata['footer_theme']['backgroundA']?>"><button class="btn swatch-caret"><i class="caret"></i></button></div></div>
                                    <p>Bg</p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                        <div class="footer">
                            <a href="javascript:void(0);" class="btn-flat white btn-reset-color">Reset</a>
                            <a class="btn-flat success btn-apply-changes" href="javascript:void(0);">Apply Changes</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <ul class="nav pull-right">
            <li>
                <button type="button" class="button-main button-large new-nav-btn1  pull-left" id="profile_view">
                    <span>PREVIEW</span>
                </button>
                <?php
                if($get_user['publish_account'] == 1)
                {
                ?>
<!--                <button type="button" class="button-main button-large new-nav-btn1-color btn-disabled pull-left offset-custom-2 btn_publish ">Publish</button>-->
                <?php
                }else if($get_user['publish_account'] == 0 && $fullstatus >= 75){
                ?>
                <button type="button" class="button-main button-large new-nav-btn1-color pull-left offset-custom-2 btn_publish ">Publish</button>
                <?php
                }else if($get_user['publish_account'] == 0 && $fullstatus < 75){
                ?>
                <button type="button" class="button-main button-large new-nav-btn1-color btn-disabled pull-left offset-custom-2 btn_publish ">Publish</button>
                <?php
                }
                ?>
                <input type="hidden" value="<?=$fullstatus?>" name="fullstatus" id="fullstatus" />
                <input type="hidden" value="<?=$get_user['publish_account']?>" name="publish_account" id="publish_account" />
            </li>
        </ul>
    </div>
</div>
<div class="clearfix"></div>
<section  id="single-team-section">
    <section class="container" id="single-team">
        <div class="row-fluid team-first">
            <div class="span12">
            <div class="span3">
                <div class="team-img">
                    <div class="profile-main-img-editor">
                        <div class="upload-btn-profile-image" id="upload-btn-profile-image">
                            <i class="icon-cloud-upload"></i>
                        </div>
                        <div class="remove-profile-img" id="remove-profile-img">
                            <i class="icon-remove-sign"></i>
                        </div>
<!--                        <div class="pro-ops-holder span12">

                            <ul class="user-port-dp-ops">
                                <li><a id="pro_image_avatar_btn-edit" class="profile-dp-edit" href="javascript:void(0);"><i class="icon-upload-alt"></i></a></li>
                                <li><a id="pro_image_avatar_btn-del" class="profile-dp-delete" href="javascript:void(0);"><i class="icon-remove-circle"></i></a></li>
                            </ul>
                        </div>-->
                    </div>
                    <img class="img-circle profile_img cus-image fileinput-button" id="pro_image_avatar" src="<?php echo $get_user['image']; ?>">
                     <?php
                        $class = '';
                        if($availability == 1){
                            $class = 'one';
                        }else if($availability == 2){
                            $class = 'two';
                        }else if($availability == 3){
                            $class = 'three';
                        }
                        ?>
                    <div class="availability-mark <?=$class?>" id="availability_mark" style="display:none;"></div>
                    <span style="display: none" id="new_image"></span>
                    <div class="span6 delete-img f-right">
                        <?php
                        if ($get_user['image_id']) {
                            ?>
                            <input type="hidden" name="image_id" id="image_id" value="<?php echo $get_user['image_id']; ?>"/>
                        <?php } ?>
                    </div>
                <span class="btn-flat success fileinput-button left-margin upload-file-btn" style="display: none;">
                    <i class="glyphicon glyphicon-plus"></i>
                                       <span>Select File</span>
                                       <!-- The file input field used as target for the file upload widget -->
                                       <input id="fileupload" type="file" name="files[]">
                    </span>
                    <input type="hidden" id="imgname" name="imgname" value="">
                    <input type="hidden" id="hidden_image_id" name="hidden_image_id" value="">
                    <input type="hidden" id="is_image_uploaded" name="is_image_uploaded" value="">
                    <input type="hidden" id="is_save_click" name="is_save_click" value="">
                    <div style="margin-top: 10px;" class="files" id="files">
                        <span id="image" class="span3"></span>
                                <span id="btn_save_img" style="display: none;" class="span7">
                                    <a class="btn-glow primary" id="save_profile_img" href="javascript:void(0);">
                                        Save changes                </a>
                                </span>
                    </div>
                </div>
            </div>
            <?php

            if($user_biography!="" && $get_user['image']!="" && $get_social_link!="" && $availability!="0"){
                $param="color: #96BF48;";
            }
            ?>
            <div class="span9">
                <div class="clearfix"></div>
                <div class="team-name">
                    <h2><?=($firstname!=""?$firstname:"FirstName")." ". ($lastname!=""?$lastname:"LastName");?></h2>
                    <div class="clearfix"></div>
                    <h3 class="custom-h4 job-title-experiance-location job_title_label_setter custom_profile_editor"><?=($job_title!=""?$job_title:"Job Title")?> | <?=($industry_name!=""?$industry_name:"Select Industry")?></h3>
                    <div class="clearfix"></div>
                    <p class="profile-basic-info job-title-experiance-location job_title_exp_setter custom_profile_editor"><?=($level!=""?$level:"Set level ")?>, <?=($experienceyear!=""?" with ".$experienceyear." years experience":"& experience")?></p>
                    <div class="clearfix"></div>
                    <p class="profile-basic-info job-title-experiance-location job_title_area_setter custom_profile_editor"><?=($city!=""?$city:"city")?>, <?=($country!=""?$country:"country")?></p>
                </div>
                <div class="clearfix"></div>
                <div class="team-description">
                    <p contenteditable="false" class="job-title-experiance-location custom_profile_editor" id="profile-biography" id="profile_biography" data-placeholder="Type here to add a small biography about yourself. Use this chance to grab the attention of recruitment teams by outlining your skills, career aspirations and anything else of interest"><?=($user_biography!=""?nl2br($user_biography):"Type here to add a small biography about yourself. Use this chance to grab the attention of recruitment teams by outlining your skills, career aspirations and anything else of interest.")?></p>
                </div>
                <div class="team-social">
                    <!--<ul>
                        <li>--><?php include dirname(__FILE__)."/edit_profile_social.php"; ?><!--</li>
                    </ul>-->
                </div>
            </div>
            </div>
        </div>
    </section>
</section>
<?php include APPPATH."views/contents/edit_profile_portfolio.php"; ?>
<?php include APPPATH."views/contents/edit_profile_experience.php"; ?>
<?php include APPPATH."views/contents/edit_profile_skills_recommendations.php"; ?>
<div class="hide">
    <img src="<?= ASSETS_PATH ?>profile/edit-profile-icon.png" id="edit_icon"/>
</div>


<div style="display: none;" class="modal" id="publish-profile-model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="profile-publish-dialog">
                    <div class="check-sigh"><i class="fa fa-check-square-o"></i></div>
                    <div class="congo-label">CONGRATULATIONS!</div>
                    <p class="text-1">Your profile is ready for launch.</p>
                    <p class="text-2">You are one click away from showcasing your pixel-perfect<br>
                    profile to our studio partner's and world
                    </p>
                    <p class="text-3">
                        <button class="btn btn-success btn-dialog-publish-profile" id="btn_dialog_publish_profile">PUBLISH PROFILE</button>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal" id="share-publish-profile-model" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <div class="share-profile-dialog">
                    <div class="check-sigh-green"><i class="fa fa-check-square-o"></i></div>
                    <div class="congo-label">THAT WAS IMPRESSIVE!</div>
                    <p class="text-1">You should share your achievement</p>
                    <p class="text-2">
                        <span class='st_facebook_large' displayText='Facebook'></span>
                        <span class='st_twitter_large' displayText='Tweet'></span>
                    </p>
                    <p class="text-3">
                        <span style="background-color: #ccc;padding: 10px;border-radius: 5px;"  class="form-control public-profile-url"><?=LIVE_SITE_URL?><?=$username?></span>
                    </p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
<!--            <div class="modal-header">
                
                <h4 class="modal-title mota-title">Basic Information</h4>
            </div>-->
            <div class="modal-body">
<!--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>-->
                <h4 class="modal-title mota-title basic-info-model-title">Basic Information</h4>
                <div class="center prof-dtl mdl-prf" id="edit_prof">
                    <div class="tab-margin edit-profile-details-box">
                        <input type="hidden" class="profil_edit_fun" name="callfrom" value="profile_top">
                        <div class="field-box">
                            <input name="extra_stuff" id="extra_stuff" value="<?=$job_title?>" placeholder="Title: eg:Animator, Freelancer, Illustrator" type="text" class="form-control pop-title-field edit-job-title">
                        </div>
                        <div class="field-box">
                            <?=$get_industry?>
                        </div>
                        <div class="field-box">
                            <?=$get_job_level?>
                        </div>
                        <div class="field-box">
                            <?=$get_experience_years?>
                        </div>
                        <div class="field-box">
                            <?=$get_countries_dropdown?>
                        </div>
                        <div class="field-box">
                            <?=$get_cities_dropdown?>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="edit-profile-details-box-2">
                        <textarea class="form-control about-you-desc" id="about_you_desc" name="biography"
                                  onKeyDown="limitText(this);" 
onKeyUp="limitText(this);" placeholder="Tell us a little bit about yourself..."><?=$user_biography?></textarea><br/>
                        <labe class="max-limit-label" id="max_limit_label"><?=intval(600) - intval(strlen($user_biography))?></labe>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success btn-save model-btn-save" value="<?= LABEL_SAVE_CHANGES ?>">SAVE CHANGES
                </button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal" id="myModal_social_link" style="width: 830px;height: 423px;" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-body" style="height: 331px;">
               <section class="social_link_section">
                   <?php
                if ($get_social_link == "") {
                    ?>
                    <h4 class="bolded-head">Connect your Social Network</h4>
                    <p>Select networks from dropdown menu and add URL </p>
                    <div class="row-fluid">
                        <div class="public-social-network-highlight pull-left span12">
                            <nav id="social-connected-profile">
                                <ul class="public-social-list"></ul>
                            </nav>
                        </div>
                    </div>
                <?php
                } else {
                    ?>
                    <div class="row-fluid">
                        <div class="public-social-network-highlight pull-left span12">
                            <nav id="social-connected-profile">
                                <ul class="public-social-list">
                                    <?php
                                    foreach ($get_social_link as $key => $social) {
                                        ?>
                                        <li>
                                            <a target="_blank" href="<?= $social['social_link']; ?>">
                                                <i class="font-icon-social-<?= $social['social_type']; ?>"></i>
                                            </a>
                                            <span data-social-id="<?= $social['social_id']; ?>" class="icon-remove-circle btn-remove-social"></span>
                                        </li>
                                    <?php
                                    }
                                    ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                <?php
                }
                ?>
               </section>
                <div class="row-fluid">
                    <div class="network-url-container">
                        <div class="tab-margin span2 user-pro-url-adjust pull-left">
                            <div class="btn-group">
                                <button data-toggle="dropdown" class="btn btn-ani"><i id="icn-social" class="font-icon-social-facebook"></i></button>
                                <input type="hidden" name="social_value" id="social_value" value=""/>
                                <button data-toggle="dropdown" class="btn dropdown-toggle network-btn-support btn-anbs">
                                    <i class="caret"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="javascript:void(0)" class="social_label" data-value="linkedin">LinkedIn</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="twitter">Twitter</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="facebook">Facebook</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="dribbble">Dribbble</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="github">Github</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="smugmug">SmugMug</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="500px">500px</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="vimeo">Vimeo</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="flickr">Flickr</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="youtube">YouTube</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="pinterest">Pinterest</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="tumblr">Tumblr</a></li>
                                    <li><a href="javascript:void(0)" class="social_label" data-value="google-plus">Google+</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="span10 profile-url-tab default-margin-left">
                            <input type="text" class="form-control span12 pull-left" id="social_url" name="social_url"
                                   placeholder="Profile URL">
                            <button type="button" id="social_url_btn" class="button-main button-large btn-connect btn-theme-anb">CONNECT
                            </button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="social-link-add-loader" id="social-link-add-loader"><img src="<?=LIVE_SITE_URL?>assets/img/ajax-loader.gif"/></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success btn-save" data-dismiss="modal" value="<?= LABEL_SAVE_CHANGES ?>">SAVE CHANGES
                </button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal" id="myModal_availability" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="height: 333px;">
                <h4 class="basic-info-model-title">Availability</h4>
                <div class="edit-profile-details-box-3">
                    <?php
                    $one = '';
                    $two = '';
                    $three = '';
                    if($availability == 1){
                        $one = 'selected';
                    }else if($availability == 2){
                        $two = 'selected';
                    }else if($availability == 3){
                        $three = 'selected';
                    }
                    ?>
                    <input type="hidden" class="availability_option" name="availability_option" id="availability_option" value="<?=$availability?>">
                    <div class="option_box">
                        <div class="label-box">
                            <span class="option-label">AVAILABLE FOR WORK</span>
                        </div>
                        <div class="check-option">
                            <div class="check-option-wrap <?=$one?>">
                                <input type="checkbox" class="select-available-option" value="1"/>
                            </div>
                        </div>
                    </div>
                    <div class="option_box">
                        <div class="label-box">
                            <span class="option-label">AVAILABLE SOON</span>
                        </div>
                        <div class="check-option">
                            <div class="check-option-wrap <?=$two?>">
                                <input type="checkbox" class="select-available-option" value="2"/>
                            </div>
                        </div>
                    </div>
                    <div class="option_box">
                        <div class="label-box">
                            <span class="option-label">NOT AVAILABLE</span>
                        </div>
                        <div class="check-option">
                            <div class="check-option-wrap <?=$three?>">
                                <input type="checkbox" class="select-available-option" value="3"/>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>        
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success btn-save" data-dismiss="modal" value="<?= LABEL_SAVE_CHANGES ?>">SAVE CHANGES
                </button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal fade" id="CropModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title mota-title">Crop Image</h4>
            </div>
            <div class="modal-body">
                <div class="center prof-dtl mdl-prf">
                    <div class="tab-margin">
                        <!--div class="row-fluid">
                          <section id="single-team">
                              <div class="row">
                                  <div id="cropImagepreview-pane" class="span12">
                                      <div class="team-img cropImagepreview-container">
                                          <img src="http://192.168.0.200/tyroe/assets/uploads/profile/thumbnail/img_4000_artwork_02.jpg" id="" class="img-circle profile_img cus-image offset3 fileinput-button">
                                      </div>
                                  </div>
                              </div>
                              </section>
                        </div-->
                        <input type="hidden" id="cropImageUrl" name="cropImageUrl" />
                        <input type="hidden" id="cropImageX" name="cropImageX" />
                        <input type="hidden" id="cropImageY" name="cropImageY" />
                        <input type="hidden" id="cropImageH" name="cropImageH" />
                        <input type="hidden" id="cropImageW" name="cropImageW" />
                        <input type="hidden" id="cropImageX" name="cropImageX1" />
                        <input type="hidden" id="cropImageY" name="cropImageY1" />
                        <div class="row-fluid">
                            <div>
                                <div id="cropImageContainer">
                                    <!--<img id="cropImage"  src="http://192.168.0.200/tyroe/penguins.jpg" class="jcrop-preview" alt="Preview" />-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" data-close-crop="close">Close</button>
                <button type="button" class="btn btn-primary btn-save model-cropImage-btn-save" value="<?= LABEL_SAVE_CHANGES ?>">Save changes
                </button>
                <span style="display:none" id="uploading"> Uploading <img src='<?=ASSETS_PATH?>img/newloading.gif'> </span>
            </div>
        </div>
    </div>
</div>
<script src="<?=ASSETS_PATH ?>js/select2.min.js"></script>
<script language="javascript" type="text/javascript">
function limitText(obj) {
        var limitNum = 600;
        if(obj.value.length > limitNum) {
                obj.value = obj.value.substring(0, limitNum);
                $('#max_limit_label').attr('value',0);
	} else {
                $('#max_limit_label').html(parseInt(limitNum - obj.value.length));
	}
}
</script>
<style>
    .sticky-cls{
        width: 100%;
    }
</style>
<script>
    /*function showCoords(c)
    {
        $('#cropImageX').val(c.x);
        $('#cropImageY').val(c.y);
        $('#cropImageH').val(c.h);
        $('#cropImageW').val(c.w);
    }
    var jcrop_api='';
    function customCropImage(){

        $('#cropImage').Jcrop({
            onChange:   showCoords,
            onSelect:   showCoords,
            boxWidth: 500,
            boxHeight: 500,
            allowResize:false,
            allowSelect:false,
            *//*minSize:[0,0],
             maxSize:[0,0],*//*
            allowMove: true
        },function(){
            jcrop_api = this;
            *//*jcrop_api.setSelect[ 100, 100, 220, 220 ];*//*
            jcrop_api.animateTo([0,0,315,315]);
        });
        $('#CropModal').modal('show');
        $('.loader-save-holder').hide();
    }*/


    jQuery(document).ready(function () {
        /*$('#navbar-sticky').sticky({ topSpacing: 0,class_name:'sticky-cls'});*/
//        var edit_clone;
//        $(".custom_profile_editor").hover(function(){
//            var offset = $(this).offset();
//            edit_clone = $('<img src="<?= ASSETS_PATH ?>profile/edit-profile-icon.png" id="edit_icon"/>');
//            $(this).append(edit_clone);
//            $(this).css({
//                position:'relative'
//            });
//            edit_clone.css({
//               position:'absolute',
//               left:$(this).width(),
//               top:$(this).height()/2
//            });
//        },function(){
//            //edit_clone.remove();
//        });
        $('.model-cropImage-btn-save').click(function(){
            if($('#is_image_uploaded').val() == "yes"){
                $('#is_image_uploaded').val("");
                var cropImageX=$('#cropImageX').val();
                var cropImageY=$('#cropImageY').val();
                var cropImageH=$('#cropImageH').val();
                var cropImageW=$('#cropImageW').val();
                var cropImageUrl=$('#cropImageUrl').val();

                var image_id_new = $('#hidden_image_id').val();
                //$("#iframeloading").show();
                $('.loader-save-holder').show();
                $('.loader-save-holder').css('z-index','99999999');
                $.ajax({
                    type: 'POST',
                    data: {'cropImageX':cropImageX,'cropImageY':cropImageY,'cropImageH':cropImageH,
                        'cropImageW':cropImageW,'cropImageUrl':cropImageUrl,'callfrom':'cropimage',
                        'image_id':image_id_new
                    },
                    url: '<?=$vObj->getURL("profile/save_profile");?>',
                    dataType: 'json',
                    success: function (responseData) {
                        $("#pro_image_avatar").attr("src", "<?=ASSETS_PATH?>uploads/profile/crop/" + cropImageUrl);
                        $('#CropModal').modal('hide');
                        $('.loader-save-holder').hide();
                        $('.loader-save-holder').css('z-index','');
                        if(responseData.scorer.resume_scorer+responseData.scorer.portfolio_scorer+responseData.scorer.profile_scorer > "74")
                        {
                            $('.custom-p-color').html('Congratulations, you can publish your profile!');
                        }else
                        {
                            $('.custom-p-color').html('Reach 75% and get published!');
                        }
                        if(responseData.scorer.resume_scorer == "" || responseData.scorer.resume_scorer == "0")
                        {
                            var resume_color = "#F9785B";
                        }
                        else
                        {
                            var resume_color = "#76bdee";
                        }
                        if(responseData.scorer.portfolio_scorer=="" || responseData.scorer.portfolio_scorer == "0")
                        {
                            var portfolio_color ="#F9785B";
                        }
                        else
                        {
                            var portfolio_color = "#c4dafe";
                        }
//                        new Morris.Donut({
//                            element: 'hero-donut',
//                            width:300,
//                            data: [
//                                {label: 'Profile', value: responseData.scorer.profile_scorer },
//                                {label: 'Resume', value: responseData.scorer.resume_scorer },
//                                {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                                {label: 'Incomplete', value: responseData.scorer.incomplete }
//                            ],
//                            colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                            formatter: function (y) {
//                                return y + "%"
//                            }
//                        });

                        var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                        update_profile_completness_top_chart(prof_comp);
                        //update_knob(responseData.scorer.fullstatus);
                        publish_unpublish_btn(responseData.scorer.fullstatus);
                    }
                });
            } else {
                $('.loader-save-holder').show();
                $('.loader-save-holder').css('z-index','99999999');
            }

        });

        $('body').toggleClass("change");
        $('#upload-btn-profile-image').click(function(){
            $("#fileupload").trigger('click');
            $('#fileupload').on('click', function() {
                $.each(this.files, function() {
                    var image = new Image();
                    image.src = window.URL.createObjectURL(this);

                    image.onload = function() {
                        $("canvas").drawImage({
                            source: image,
                            x: 50, y: 50,
                            width: 100,
                            fromCenter: false
                        });
                    };
                });
            });
            //console.log("Dddddd");
        });

        $('#pro_image_avatar').click(function () {
            $("#fileupload").trigger('click');
        });
        $('#remove-profile-img').click(function(){
            $.ajax({
                type: 'POST',
                data: {'callfrom':'removeprofileimage'
                },
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json',
                success: function (responseData) {
                    $("#pro_image_avatar").attr("src", "<?=ASSETS_PATH?>img/default-avatar.png");
                }
            });
        });

        $('.level_experience').find('.field-box').hide();
        $('.level_experience').click(function () {
            $(this).find('h4').hide();
            $(this).find('.field-box').show();
        })
    })
</script>
<script type="text/javascript">
    function rescale(){
        var size = {width: $(window).width() , height: $(window).height() };
        var offset = 20;
        var offsetBody = 150;
        //$('#myModal').css('height', size.height - offset );
        //$('.modal-body').css('height', size.height - (offset + offsetBody));
        //$('#myModal').css('top', '3%');
//        $('#myModal').css('left', '40%');
//        $('#myModal').css('width', '860px');

        /*var width_sticky = $(".content").width();
         $('.sticky').css('width', width_sticky+'px');*/
    }

    $(window).bind("resize", rescale);
    $(function () {
        $(".select2").select2();
    });
</script>
<script>
function getCities(obj){
    var countryID=$(obj).val();
    if(countryID==""){
        countryID=0;
    }
    $.ajax({
        type: 'POST',
        data: {'country_id':countryID},
        url: '<?=$vObj->getURL("profile/getcities");?>',
        dataType: 'json',
        success: function (data) {
            //alert(data.dropdown);
            var nextFieldbox=$(obj).parent().next();
	     $(nextFieldbox).select2('destroy');
            $(nextFieldbox).html(data.dropdown);
            $(nextFieldbox).find('select').select2();
        }
    });
}

/*pop dialog and colorpicker start*/
$(document).on("click", "body", function(event){
    if ( $(event.target).closest('.pop-dialog').length > 0 || $(event.target).closest('.custom-popup-box').length > 0 ) {
        return false;
    }else{
        if($(event.target).closest('.colorpicker').length > 0 || $(event.target).closest('.custom-popup-box').length > 0){
            return false;
        }
        else{
            $('.pop-dialog').hide();
        }
    }
});
$(document).on("click", ".colorpicker_submit", function(event){
    $( '.colorpicker' ).hide();
});
/*pop dialog  and colorpicker end*/
$(document).ready(function(){
    $('.pop-dialog').hide();
    $('.pop-dialog').find('.close-icon').click(function(){
        $(this).closest('.pop-dialog').hide();
    });
    $('.custom-popup-box').click(function(){
        $('.pop-dialog').hide();
        $(this).next('.pop-dialog').show();
    });
    $( 'body' ).toggleClass( "change" );

    var resume_color = "";
    var portfolio_color = "";
   <?php  if($resume_scorer == "" || $resume_scorer == "0")
    {
        $resume_color = "#F9785B";
    }
    else
    {
        $resume_color = "#76bdee";
    }
    if($profile_scorer=="" || $portfolio_scorer == "0")
    {
        $portfolio_color ="#F9785B";
    }
    else
    {
        $portfolio_color = "#c4dafe";
    } 
    $profile_completed = intval($profile_scorer) + intval($resume_scorer) + intval($portfolio_scorer)
    ?>
            
    $('.edit_profile_completness_knob').attr('value',<?=$profile_completed?>);         
    $('.edit_profile_completness_knob').knob();
    $('.edit_profile_completness_knob').css({
        'box-shadow' : 'none'
    });
    
//    Morris.Donut({
//        element: 'hero-donut',
//        width:300,
//        data: [
//            {label: 'Profile', value: <?=$profile_scorer;?> },
//            {label: 'Resume', value: <?=$resume_scorer;?> },
//            {label: 'Portfolio', value: <?=$portfolio_scorer;?> },
//            {label: 'Incomplete', value: <?=$incomplete;?> }
//        ],
//        colors: ["#30a1ec", "<?= $resume_color ?>", "<?= $portfolio_color ?>", "#F9785B"],
//        formatter: function (y) { return y + "%" }
//    });
    publish_unpublish_btn(<?=$fullstatus;?>);


    var browser='<?php echo $this->agent->browser();?>';

    if(browser == "Firefox"){
        $('svg text').attr("transform",'matrix(1.7037,0,0,1.7037,-116.1111,-87.2593)');
        $('svg tspan').attr("dy",'5');
    }
    
    function nl2br (str, is_xhtml) {   
        var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
        return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
    }

    $('.model-btn-save').click(function(){
        $('.loader-save-holder').css("z-index","9999");
        $('.loader-save-holder').show();
        var data=$(this).closest('.modal-content').find('.prof-dtl .edit-job-title,.prof-dtl select,.prof-dtl .about-you-desc,.prof-dtl .profil_edit_fun');
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("profile/save_profile");?>',
            dataType: 'json',
            success: function (responseData) {
                $('.loader-save-holder').css("z-index","");
                $('.loader-save-holder').hide();
                var industry='';
                var extra_stuff='';
                var year='';
                var experience_level='';
                var country='';
                var city='';
                var city_name='';
                var c=((1==1)?2:3);

                $(data).each(function(){
                    if($(this).attr('name')!=undefined){
                        var attrname=$(this).attr('name');
                        switch(attrname){
                            case 'industry':
                                industry=$(this).children('option:selected').text();
                                break;

                            case 'extra_stuff':
                                extra_stuff=$(this).val().replace(/<(?:.|\n)*?>/gm, '');
                                break;

                            case 'year':
                                year=$(this).children('option:selected').text();
                                if(year=="Experience Level")
                                {
                                    year ="";
                                }
                                break;

                            case 'experience_level':
                                experience_level=$(this).children('option:selected').text();
                                if(experience_level == "Years")
                                experience_level = "";
                                break;

                            case 'country':
                                country=$(this).children('option:selected').text();
                                break;

                            case 'city':
                                city=$(this).children('option:selected').val();
                                city_name=$(this).children('option:selected').text();
                                break;
                        }
                    }
                });

                var title=(extra_stuff!=""?extra_stuff:"Job Title")+' | '+ (industry!=""?industry:"Select Industry");
                var experience=(year!=""?year:"Set level ")+', '+ (experience_level!=""?" with "+experience_level+" years experience":"& experience");
                var area=(city_name!=""?city_name:"city")+', '+ (country!=""?country:"country");
                $('.job_title_label_setter').html(title);
                $('.job_title_exp_setter').html(experience);
                $('.job_title_area_setter').html(area);
                var desc = $('#about_you_desc').val();
                $("#extra_stuff").val($("#extra_stuff").val().replace(/<(?:.|\n)*?>/gm, ''));
                $('#profile-biography').html(nl2br(desc.replace(/<(?:.|\n)*?>/gm, '')));
                $('#about_you_desc').val(desc.replace(/<(?:.|\n)*?>/gm, ''));
                $('#myModal').modal('hide');
                if(responseData.scorer.resume_scorer+responseData.scorer.portfolio_scorer+responseData.scorer.profile_scorer > "74")
                {
                  $('.custom-p-color').html('Congratulations, you can publish your profile!');
                }else
                {
                  $('.custom-p-color').html('Reach 75% and get published!');
                }
                if(responseData.scorer.resume_scorer == "" || responseData.scorer.resume_scorer == "0")
                {
                   var resume_color = "#F9785B";
                }
                else
                {
                   var resume_color = "#76bdee";
                }
                if(responseData.scorer.portfolio_scorer=="" || responseData.scorer.portfolio_scorerr == "0")
                {
                   var portfolio_color ="#F9785B";
                }
                else
                {
                   var portfolio_color = "#c4dafe";
                }
//                new Morris.Donut({
//                    element: 'hero-donut',
//                    width:300,
//                    data: [
//                        {label: 'Profile', value: responseData.scorer.profile_scorer },
//                        {label: 'Resume', value: responseData.scorer.resume_scorer },
//                        {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                        {label: 'Incomplete', value: responseData.scorer.incomplete }
//                    ],
//                    colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                    formatter: function (y) {
//                        return y + "%"
//                    }
//                });
                var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                update_profile_completness_top_chart(prof_comp);
                //update_knob(responseData.scorer.fullstatus);
                publish_unpublish_btn(responseData.scorer.fullstatus);
            }
        });
    });
    $(document).on("click",".job-title-experiance-location",function(){
        $('#myModal').modal('show');
        rescale();
    });
});
//Frist&last name and biograph
function strip_tags(t, e) {
    e = (((e || "") + "").toLowerCase().match(/<[a-z][a-z0-9]*>/g) || []).join("");
    var n = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi, o = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
    return t.replace(o, "").replace(n, function (t, n) {
        return e.indexOf("<" + n.toLowerCase() + ">") > -1 ? t : ""
    })
}
function smarten(){
    return t = t.replace(/(^|[-\u2014\s(\["])'/g, "$1â€˜"), t = t.replace(/'/g, "â€™"), t = t.replace(/(^|[-\u2014/\[(\u2018\s])"/g, "$1â€œ"), t = t.replace(/"/g, "â€"), t = t.replace(/--/g, "â€”")
}


/*paste content in conteneditable html5 start*/
$(document).ready(function(){

    $('*[contenteditable]').on('paste',function(e) {

        e.preventDefault();
        var text = (e.originalEvent || e).clipboardData.getData('text/plain');
        // console.log(text);
        if(typeof text!=='undefined'){
            document.execCommand('insertText', false, text);
        }
    });
    
    $('#edit_web_link_button').click(function(){
        $('#myModal_availability').modal('show');
    });
    
    $('#edit_available_link_button').click(function(){
        $('#myModal_social_link').modal('show');
    });
    
});
/*paste content in conteneditable html5 end*/

$(document).on('blur', '.biography-field-label', function () {
    return;
    var data = {};

    $('.loader-save-holder').show();
    $(this).closest('.biography-field-label').find('*[contenteditable]');
    var is_editable_element = $(this).attr("contenteditable");
    if (is_editable_element == "true") {
        var attr_name = $(this).attr('data-name');

        if (attr_name == "bio") {
            data['bio'] = $(this).text();
            data['callfrom'] = "biograph";
            data['user_biograph_hid'] = $(".user_biograph_hidden").val();
        } else if (attr_name == "first_name") {
            var firstname = $(this).text().split(" ");
            data['firstname'] = firstname[0];
            data['lastname'] = firstname[1];
            data['callfrom'] = "basic_detail";
        }
    }
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("profile/save_profile");?>',
        dataType: 'json',
        success: function (responseData) {
            if (responseData.status) {
                $('.loader-save-holder').hide();
                if(attr_name == "first_name")
                {
                    if(responseData.ln==false)
                    responseData.ln = "";
                    $("#fn_ln").html(responseData.fn+" "+responseData.ln);
                }
                if(responseData.scorer.resume_scorer+responseData.scorer.portfolio_scorer+responseData.scorer.profile_scorer > "74")
                {
                  $('.custom-p-color').html('Congratulations, you can publish your profile!');
                }else
                {
                  $('.custom-p-color').html('Reach 75% and get published!');
                }
               if(responseData.scorer.resume_scorer == "" || responseData.scorer.resume_scorer == "0")
               {
                  var resume_color = "#F9785B";
               }
               else
               {
                  var resume_color = "#76bdee";
               }
               if(responseData.scorer.portfolio_scorer=="" || responseData.scorer.portfolio_scorerr == "0")
               {
                  var portfolio_color ="#F9785B";
               }
               else
               {
                  var portfolio_color = "#c4dafe";
               }
//                new Morris.Donut({
//                    element: 'hero-donut',
//                    width:300,
//                    data: [
//                        {label: 'Profile', value: responseData.scorer.profile_scorer },
//                        {label: 'Resume', value: responseData.scorer.resume_scorer },
//                        {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                        {label: 'Incomplete', value: responseData.scorer.incomplete }
//                    ],
//                    colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                    formatter: function (y) {
//                        return y + "%"
//                    }
//                });
                var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                update_profile_completness_top_chart(prof_comp);
                //update_knob(responseData.scorer.fullstatus);
                publish_unpublish_btn(responseData.scorer.fullstatus);
            }
        }
    });
});

//function for checking visiblity section
$(document).on('click','.visibility_section_icon',function(){
    var obj=this;
    var section=$(this).attr('data-visibile-sec');
    var value=$(this).attr('data-visibile-val');
    var class1="";
    var data="vis="+section+"&callfrom=visibility"+"&val="+value;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("profile/save_profile");?>',
        dataType: 'json',
        success: function (responseData) {
            if (responseData.status) {
                if (responseData.data_val == "1") {
                    $(obj).attr('data-visibile-val', "0");
                    $(obj).css("color", "#F97E76");

                } else {
                    $(obj).attr('data-visibile-val', "1");
                    $(obj).css("color", "");
                }
            }
        }
    });
});

$(document).on('click','.btn_publish',function(){
    var profile_com=$('#fullstatus').val();
    if(profile_com > 75)
    {
        $('.loader-save-holder').show();
        var btn_publish =  $(".btn_publish").find("span").text().toLowerCase();
        btn_publish = 1;
//        if(btn_publish=="publish"){
//            btn_publish = "1";
//            $(".btn_publish").removeClass("new-nav-btn1-color");
//            $("btn_publish").removeClass("button-main");
//            $(".btn_publish").find("span").text("unpublish");
//            $('#share-publish-profile-model').modal('show');
//        }
//        else{
//            var co = confirm("Are you sure you would like to Unpublish profile?");
//            if(co){
//                btn_publish="0";
//                $(".btn_publish").addClass("new-nav-btn1-color");
//                $(".btn_publish").find("span").text("publish");
//            }
//
//        }
       
        var data="publish_account="+btn_publish+"&callfrom=publish_account";
        $('#publish_account').val(btn_publish);
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("profile/save_profile");?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    $('.loader-save-holder').hide();
                    $(".btn_publish").addClass('btn-disabled');
                    $('#share-publish-profile-model').modal('show');
                    $(".btn_publish").hide();
                    $("#profile_completeness_message").hide();
                }
            }
        });

    }
});

$(document).on('click','#btn_dialog_publish_profile',function(){
    var profile_com=$('#fullstatus').val();
    $('#publish-profile-model').modal('hide');
    if(profile_com>75)
    {
        $('.loader-save-holder').show();
        var btn_publish =  $(".btn_publish").find("span").text().toLowerCase();
        btn_publish = 1;
//        if(btn_publish=="publish"){
//            btn_publish="1";
//            $(".btn_publish").removeClass("new-nav-btn1-color");
//            $("btn_publish").removeClass("button-main");
//            $(".btn_publish").find("span").text("unpublish");
//            $('#share-publish-profile-model').modal('show');
//        }
//        else{
//            var co = confirm("Are you sure you would like to Unpublish profile?");
//            if(co){
//                btn_publish="0";
//                $(".btn_publish").addClass("new-nav-btn1-color");
//                $(".btn_publish").find("span").text("publish");
//            }
//
//        }
        var data="publish_account="+btn_publish+"&callfrom=publish_account";
        $('#publish_account').val(btn_publish);
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("profile/save_profile");?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    $('.loader-save-holder').hide();
                    $(".btn_publish").addClass('btn-disabled');
                    $('#share-publish-profile-model').modal('show');
                    $(".btn_publish").hide();
                    $("#profile_completeness_message").hide();
                }
            }
        });

    }
});

// View public profile
$(document).on('click', '#profile_view', function () {
    $(".loader-save-holder").show();
    var data="profile=1";
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("publicprofile");?>',
        dataType: 'html',
        success: function (responseData) {
            $(".loader-save-holder").hide();
            $('#sidebar-nav').hide();
            $('.navbar-inverse').css("margin-top","-57px");
            $('.last-spacer').hide();
            $('.content').hide();
            $('.public-profile').html(responseData);
        }
    });
});

$(document).on('click', '#edit_profile', function () {
    $('.content').show();
    $('#sidebar-nav').show();
    $('.navbar-inverse').css("margin-top","0px");
    $('.last-spacer').show();
    $('.public-profile').html("");
    total_images = 0;
    save_portfolio_order(false);
});

$(document).on('click', '.btn-reset-color', function () {
    var co = confirm("Are you sure you would like to reset the colours?");
    if (!co) {
    } else {
        $('.colorSelectorP1 div.inner').css('background-color', '#323A45');
        $('.colorSelectorP2 div.inner').css('background-color', '#ffffff');
        $('.colorSelectorP3 div.inner').css('background-color', '#D0DBE2');
        $('.colorSelectorP4 div.inner').css('background-color', '#F97F76');
        $('.colorSelectorF1 div.inner').css('background-color', '#323A45');
        $('.colorSelectorF2 div.inner').css('background-color', '#F97F76');
        $('.colorSelectorF3 div.inner').css('background-color', '#fff');
        $('.colorSelectorG1 div.inner').css('background-color', '#323A45');
        $('.colorSelectorG2 div.inner').css('background-color', '#fff');
        $('.colorSelectorG3 div.inner').css('background-color', 'rgba(255, 255 ,255, 0.5)');
        $('.colorSelectorR1 div.inner').css('background-color', '#323A45');
        $('.colorSelectorR2 div.inner').css('background-color', '#E9F0F4');
        $('.colorSelectorFoot1 div.inner').css('background-color', '#E9F0F4');
        $('.colorSelectorFoot2 div.inner').css('background-color', '#fff');
        $('.colorSelectorFoot3 div.inner').css('background-color', '#323A45');

        //profile theme reset
        $('#left-arrow-pointer').css('border-right-color', '#ffffff');
        $('#single-team-section').css('background-color','#ffffff');
        $('#single-team-section').find('.span9').css('color', '#323A45');
        $("#single-team-section h1,#single-team-section h2,#single-team-section h3,#single-team-section h4,#single-team-section h5").css('color','#323A45');
        $('#social-connected-profile').find('ul.public-social-list li a i').css('color','#D0DBE2');
        $('#social-connected-profile').find('ul.public-social-list li a i').on({
            mouseenter: function () {
                $(this).css('color','rgb(249, 127, 118)');
            },
            mouseleave: function () {
                $(this).css('color','#D0DBE2');
            }
        });
        
        $('#social-connected-profile-full').find('ul.public-social-list li a i').css('color','#D0DBE2');
        $('#social-connected-profile-full').find('ul.public-social-list li a i').on({
            mouseenter: function () {
                $(this).css('color','rgb(249, 127, 118)');
            },
            mouseleave: function () {
                $(this).css('color','#D0DBE2');
            }
        });
        //feature theme reset
        $('#feature_BGB').css('background-color', '#fff');
        $('.featured-description-text').css('color','#323A45');
        $('.featured-bg').css('background-color','#rgb(249, 127, 118)');

        //gallery theme reset
        $('#services-box').css('background-color','#fff');
        $('#services-box').find('h3').css('color','#323A45');
        $('#services-box').find('.latest-work-disp h4').css('color','#323A45');
        $('#services-box').find('.latest-work-disp').css('background-color','rgba(255,255,255,0.5)');

        //latest work theme reset
        $('#latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6').css('color','#323A45');
        $('#latest-work').css({'color':'#323A45','background-color':'#E9F0F4'});
        $('footer .arrow-down').css('border-top-color','#E9F0F4');

        //footer theme reset
        $('footer').css('background-color','#323A45');
        $('#social-footer').find('ul li a i').css('color', '#E9F0F4');
        $('#social-footer').find('ul li a i').on({
            mouseenter: function () {
                $(this).css('color','#fff');
            },
            mouseleave: function () {
                $(this).css('color','#E9F0F4');
            }
        });
       /* $("#single-team-section h1,#single-team-section h2,#single-team-section h3,#single-team-section h4,#single-team-section h5").css('color', "#323A45");
        $("#single-team-section p, #single-team-section span").css('color', "#323A45");
        $("#single-team-section").css('background-color', "#ffffff");

        //$("#single-team-section .team-social ul.pub-soc-li li a i").css('color', "#D0DBE2");
        $("#single-team-section .team-social ul.public-social-list li a i").css('color', "#D0DBE2");
        //$("#single-team-section .team-social ul.pub-soc-li li a:hover i").css('color', "#D0DBE2");
        $("#single-team-section .team-social ul.public-social-list li a:hover i").css('color', "#F97F76");

        $('#latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6, #latest-work').css('color', '#323A45');
        $('#latest-work').css('background-color', '#E9F0F4');
        $('footer .arrow-down').css({
            borderTopColor: '#E9F0F4'
        });
        $('.latest-work-disp').css('background-color', 'rgba(255,255,255,0.5)');
        $('#services-box h3, .latest-work-disp > h4').css({color: '#000'});
        $('#intro-box').css({'background-color': '#F97F76'});
        $('#services-box').css('background-color', '#fff');
        $('#feature_BGB').css('background-color', '#fff');
        $('footer').css('background-color', '#323A45 !mportant');
        $('#social-footer').find('ul li a i').css('color', '#E9F0F4');
        $('#social-footer').find('ul li a:hover i').css('color', '#fff');*/
    }

    });

$(document).ready(function(){
    if($(window).width()==360){
        $('.nav-fixed-top').find('button').removeClass("button-large").addClass("button-small");
        $('#hero-bar').attr('data-height','120');
    }
});

</script>




<style>
    #single-team-section .span9{
        color:<?=$this->session->userdata['profile_theme']['text']?>;
    }
    #single-team-section h1, #single-team h2, #single-team h3, #single-team h4, #single-team h5, #single-team h6{
        color:<?=$this->session->userdata['profile_theme']['text']?>;
    }

    #social-connected-profile ul.public-social-list li a i,  #social-connected-profile-full ul.public-social-list li a {
        color:<?=$this->session->userdata['profile_theme']['icons']?>;
    }

    #social-connected-profile ul.public-social-list li a:hover i,  #social-connected-profile-full ul.public-social-list li a:hover i{
        color:<?=$this->session->userdata['profile_theme']['icons_rollover']?>;
    }

    #single-team-section{
        background-color:<?=$this->session->userdata['profile_theme']['background']?>;
    }
    #dashboard-menu .pointer .arrow{
        border-right-color: <?=$this->session->userdata['profile_theme']['background']?>;
    }

    /*PROFILE THEME END*/

    /*FEATURED THEME START*/
    .featured-description-text{
        color:<?=$this->session->userdata['featured_theme']['text']?>;
    }

    #feature_BGB{
        background:<?=$this->session->userdata['featured_theme']['backgroundB']?>;
    }

    #intro-box {
        background:none repeat scroll 0 0 <?=$featured_theme_backgroundA = $this->session->userdata['featured_theme']['backgroundA'];
        echo $featured_theme_backgroundA!=null?$featured_theme_backgroundA:"rgb(249, 127, 118 )";?>;
    }
    #intro-box-dropdownft{
        background:none repeat scroll 0 0 <?=$featured_theme_backgroundA = $this->session->userdata['featured_theme']['backgroundA'];
        echo $featured_theme_backgroundA!=null?$featured_theme_backgroundA:"rgb(249, 127, 118 )";?>;
    }
    /*FEATURED THEME END*/

    /*GALLERY THEME START*/
    #services-box h3{
        color:<?=$this->session->userdata['gallery_theme']['text']?>;
    }
    #services-box{
        background:none repeat scroll 0 0 <?=$this->session->userdata['gallery_theme']['background']?>;
    }


        #services-box .latest-work-disp {
           background-color:<?=$this->session->userdata['gallery_theme']['overlay']?>;
       }
    #services-box .latest-work-disp h4{
        color:<?=$this->session->userdata['gallery_theme']['text']?>;
    }
    /*buttons and overlay color remain*/
    /*GALLERY THEME END*/

    /*RESUME THEME START*/
    #latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6{
        color:<?=$this->session->userdata['resume_theme']['text']?>;
    }
    #latest-work{
        color:<?=$this->session->userdata['resume_theme']['text']?>;
        background-color:<?=$this->session->userdata['resume_theme']['background']?>;
    }
    footer span .arrow-down{
        /*working for drop arrow*/
        border-left: 20px solid rgba(0, 0, 0, 0);
        border-right: 20px solid rgba(0, 0, 0, 0);
        border-top: 20px solid <?=$this->session->userdata['resume_theme']['background']?>;
    }
        /*RESUME THEME END*/


        /*FOOTER THEME START*/
    footer{
        background:none repeat scroll 0 0 <?php
        echo $footer_theme_backgroundA=$this->session->userdata['footer_theme']['backgroundA'];
        ?>;
    }

    #social-footer ul li a i{
        color:<?php
        $footer_theme_icons=$this->session->userdata['footer_theme']['icons'];
        echo $footer_theme_icons!=null?$footer_theme_icons:"#D0DBE2";
        ?>;
        opacity: 1;
    }

    #social-footer ul li a:hover i{
        color:<?php
        $footer_theme_icons_rollover=$this->session->userdata['footer_theme']['icons_rollover'];
        echo $footer_theme_icons_rollover!=null?$footer_theme_icons_rollover:"#fff";
        ?>;
    }
        /*FOOTER THEME END*/
</style>
