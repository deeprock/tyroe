<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<section class="skill-container">
    <div class="row-fluid">
        <?php
        $param = "";
        if (is_array($get_skills)) {
            $param = "style='color: #96BF48'";
        }
        if ($get_visibility[3]['visible_section'] == "skill" && $get_visibility[3]['visibility'] == "1") {
            $visibile_param = "style='color: #F97E76'";
        }
        ?>
        <div class="span4"></div>
        <!-- Keep this div empty to align -->
        <div class="span8">
            <a name="education" class="division"></a>

                <h2>Skills <!--<i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark" <?php /* = $param */ ?>></i>
                     <i class="btn-dis icon-minus-sign pull-right done-completed visibility_section_icon" data-visibile-sec="skill" data-visibile-val="<?php /* = $get_visibility[3]['visibility'] */ ?>" <?php /* = $visibile_param */ ?>></i>--></h2>
        </div>
        <!-- span8 -->
        <div class="row-fluid">
            <div class="span4"></div>
            <!-- Keep this div empty to align -->
            <div class="span8 key-details-section">
                <ul id="tags" class="key-skills">
                    <?php
                    foreach ($get_skills as $k => $v) {
                        $v['skill_name'] = stripslashes($v['skill_name']);
                        ?>
                        <li><a href="javascript:void(0);" class="button-main button-large"><?= $v['skill_name'] ?> <i
                                    data-item-id="<?= $v['skill_id'] ?>"
                                    class="icon-remove-circle btn-remove-skill"></i></a></li>
                            <?php
                        }
                        ?>
                </ul>

                <div class="clearfix"></div>
                <div class="row-fluid add-skill-adjust">
                    <div class="network-url-container span12">
                        <div class="span12 profile-url-tab default-margin-left">
                            <input type="text" id="skills" placeholder="Software, Tools, Languages, Talents..." value=""
                                   class="form-control video-link span12 pull-left">
                            <button class="button-main button-large btn-theme-anb btn-add-skills" type="button">Add Skill</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <!--<div class="key-content">
            <div class="tab-margin profile-url-tab skills-tab">
                <input type="text" id="skills" placeholder="Software, Tools, Languages, Talents..." value="" class="form-control pull-left video-link">
                <button class="btn btn-flat pull-left embed-button btn-add-skills" type="button">ADD SKILL</button>
                <div class="clearfix"></div>
            </div>
        </div>-->
    </div>
</section>
<section class="recommend-container">
    <div class="row-fluid">
        <?php
        $param = "";
        if (is_array($get_recommendation_sent_list) || is_array($get_recommendation_pending_approval_list) || is_array($get_recommendation_approved_list)) {
            $param = "style='color: #96BF48'";
        }
        if ($get_visibility[4]['visible_section'] == "recommendation" && $get_visibility[4]['visibility'] == "1") {
            $visibile_param = "style='color: #F97E76'";
        }
        ?>
        <div class="span4"></div>
        <!-- Keep this div empty to align -->
        <div class="span8">
            <a name="education" class="division"></a>

            <h2>Recommendations <!--<i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark" <?php /* = $param */ ?>></i><i
                    class="btn-dis icon-minus-sign pull-right done-completed visibility_section_icon" data-visibile-sec="recommendation" data-visibile-val="<?php /* = $get_visibility[4]['visibility'] */ ?>" <?php /* = $visibile_param */ ?>></i>--></h2>
        </div>
        <!-- span8 -->
        <div class="row-fluid">
            <div class="span4"></div>
            <!-- Keep this div empty to align -->
            <div class="span8 key-details-section">
                <p>Invite collegues and assosiates to add "Twitter Style" blurbs about you.</p>

                <div class="clearfix"></div>

                <div class="row-fluid add-skill-adjust">
                    <div class="network-url-container span12">
                        <div class="abc">
                            <i class="icon"></i>
                        </div>
                        <div class="span12 profile-url-tab default-margin-left">
                            <input type="text" placeholder="<?= LABEL_RECOMMENDATION_TEXTFIELD_PLACEHOLDER ?>"
                                   id="recommendation_email" class="form-control pull-left video-link span12 pull-left">
                            <button class="button-main button-large btn-theme-anb btn-recommend-request" type="button">SEND
                                REQUEST
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>


            </div>

        </div>
    </div>


    <!--<div class="resume-key no-border">
                                <div class="key-title">
                                <h2>Recommendations</h2>
                                <i class="icon-ok-sign pull-right ico-done ico-done-latest"></i>
                                <p>Invite collegues and assosiates to add "Twitter Style" blurbs about you.</p>
                                </div>

                                <div class="key-content">
                                    <div class="tab-margin profile-url-tab skills-tab">
                                        <input type="text" placeholder="Email Address" id="recommendation_email" class="form-control pull-left video-link">
                                        <button class="btn btn-flat pull-left embed-button btn-recommend-request" type="button">SEND REQUEST</button>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>-->


    <?php
    if ($get_recommendation_sent_list == "") {
        $css_sent = "style='display:none'";
    }
    ?>
    <div class="resume-key no-border custom-accordion recommendation_send_list" <?= $css_sent ?>>
        <div class="key-content">
            <!--<button class="btn white add-key-button">SENT REQUEST <i class="pull-right icon-chevron-down"></i></button>-->

            <div class="accordion accordion_recommendation" id="accordionArea ">
                <div class="accordion-group">
                    <div class="accordion-heading accordionize">
                        <a data-toggle="collapse" data-parent="#accordionArea" href="#acc-7"
                           class="accordion-toggle collapsed">SENT REQUEST<span
                                class="font-icon-arrow-simple-down"></span></a>
                    </div>
                    <div class="accordion-body collapse" id="acc-7" style="height: 0px;">
                        <div class="accordion-inner recommendation_send_list_inner">
                            <div class="row-fluid">
                                <div class="span12">
                                    <?php
                                    foreach ($get_recommendation_sent_list as $k => $v) {
                                        $arr_time = @explode(',', $v['created_timestamp']);
                                        if (is_array($arr_time) && count($arr_time) > 0) {
                                            $v['created_timestamp'] = $arr_time[0];
                                        }
                                        ?>
                                        <div class="request-sent-container">
                                            <div class="span9 request-left-area">
                                                <h2><?= $v['recommend_email'] ?></h2>
                                                <span><?= date("d F Y", $v['created_timestamp']) . " AT " . date("h:i A", $v['created_timestamp']); ?></span>
                                            </div>
                                            <div data-item-id="<?= $v['recommend_id'] ?>"
                                                 class="pull-left span3 request-right-area">
                                                <i class="icon-remove resend-reminder-remove-btn send-reminder pull-left"></i>
                                                <a class="our-btn reminder-btn resend-reminder-btn pull-left button-main inverted button-large">SEND
                                                    REMINDER</a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                        <div class="clearfix"></div>
    <?php }
?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    //}

    if ($get_recommendation_pending_approval_list == "") {
        $css_approval = "style='display:none'";
    }
    ?>
    <div class="resume-key no-border custom-accordion recommendation_approval_list" <?= $css_approval ?>>
        <div class="key-content">
            <!--<button class="btn white add-key-button">PENDING APPROVAL<i class="pull-right icon-chevron-down"></i></button>-->

            <div class="accordion accordion_recommendation" id="accordionArea">
                <div class="accordion-group">
                    <div class="accordion-heading accordionize">
                        <a data-toggle="collapse" data-parent="#accordionArea" href="#acc-5"
                           class="accordion-toggle collapsed">PENDING APPROVAL<span
                                class="font-icon-arrow-simple-down"></span></a>
                    </div>
                    <div class="accordion-body collapse " id="acc-5" style="height: 0px;">
                        <div class="accordion-inner recommendation_pending_list_inner">
                            <div class="row-fluid">
                                <div class="span12">
<?php
foreach ($get_recommendation_pending_approval_list as $k => $v) {
    ?>
                                        <div class="request-pending-container" data-item-id="<?= $v['recommend_id'] ?>">
                                            <div class="span9 pending-left-area">
                                                <h2><?= $v['recommend_name'] ?></h2>
                                                <span class="recomm-company"><?= $v['recommend_company'] ?></span>
                                                <br clear="all">
                                                <span class="recomm-dept"><?= $v['recommend_dept'] ?></span>
                                            </div>
                                            <div data-item-id="<?= $v['recommend_id'] ?>"
                                                 class="pull-left span3 pending-right-area">
                                                <i class="icon-remove recommendation_pending_approval_remove_btn pending-approval  pull-left"></i>
                                                <a class="our-btn approve-btn recommendation_pending_approval_approve_btn  pull-left button-main inverted button-large approve-btn">APPROVE</a>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <p class="recomm-dtl"><?= $v['recommendation'] ?></p>
                                                </div>
                                            </div>

                                        </div>

                                        <div class="clearfix"></div>
    <?php
}
?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    //}

    if ($get_recommendation_approved_list == "") {
        $css_approved = "style='display:none'";
    }
    ?>
    <div class="resume-key no-border custom-accordion recommendation_approved_list" <?= $css_approved ?>>
        <div class="key-content">
            <div class="accordion accordion_recommendation" id="accordionArea">
                <div class="accordion-group">
                    <div class="accordion-heading accordionize">
                        <a data-toggle="collapse" data-parent="#accordionArea" href="#acc-6"
                           class="accordion-toggle collapsed">APPROVED<span
                                class="font-icon-arrow-simple-down"></span></a>
                    </div>
                    <div class="accordion-body collapse" id="acc-6" style="height: 0px;">
                        <div class="accordion-inner recommendation_approved_list_inner">
                            <div class="row-fluid">
                                <div class="span12">
<?php
foreach ($get_recommendation_approved_list as $k => $v) {
    ?>
                                        <div class="request-pending-container request-pending-container<?= $v['recommend_id'] ?>" data-item-id="<?= $v['recommend_id'] ?>">
                                            <div data-item-id="<?= $v['recommend_id'] ?>"
                                                 class="span9 pending-left-area">
                                                <h2><?= $v['recommend_name'] ?></h2>
                                                <span class="recomm-company"><?= $v['recommend_company'] ?></span>
                                                <br clear="all">
                                                <span class="recomm-dept"><?= $v['recommend_dept'] ?></span>

                                                <p class="recomm-dtl"><?= $v['recommendation'] ?></p>
                                            </div>
                                            <div class="span3 pending-right-area" data-item-id="<?= $v['recommend_id'] ?>">
                                                <i data-item-id="<?= $v['recommend_id'] ?>" class="icon-remove recommendation_approved_remove_btn pull-right"></i>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
    <?php
}
?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    //}
    ?>
</div>
</section>
<footer>
    <span style="border-top: 20px solid <?php
    $resume_theme_background = $this->session->userdata['resume_theme']['background'];
    echo $resume_theme_background != null ? $resume_theme_background : "#E9F0F4";
    ?>;" class="arrow-down"></span>

    <div class="container">
        <div class="row">
            <nav id="social-footer">
                <ul>
<?php
foreach ($get_social_link as $key => $social) {
    ?>
                        <li data-id="<?= $social['social_id']; ?>">
                            <a target="_blank" class="<?php /* = $social['social_type']; */ ?>" href="<?= $social['social_link']; ?>" >
                                <i class="font-icon-social-<?= $social['social_type']; ?>"></i>
                            </a>
                        </li>
    <?php
}
?>
                </ul>
            </nav>
        </div>
    </div>
</footer>
<section id="footer-credits">
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <p class="credits"><a href="http://www.tyroe.com" target="_blank"> &copy; Built with TYROE.</p> </a>
            </div>
        </div>
    </div>
</section>

<!--</section>-->

<section class="last-spacer" ></section>
</section>
<div id="element_to_pop_up">
</div>

<!-- Recommendation Modal -->
<div style="display: none;" class="modal" id="recommendation_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="center prof-dtl mdl-prf" id="edit_prof">
                    <div class="tab-margin firstdiv">
                        <h5><?= LABEL_RECOMMENDATION_SENT_HEADER ?></h5>
                    </div>
                    <div class="tab-margin seconddiv">
<?= LABEL_RECOMMENDATION_SENT_CONTENT ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Recommendation Modal -->

<!-- Warning Modal -->
<div style="display: none;" class="modal" id="warning_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title mota-title">Recommendation Remove</h4>
            </div>
            <div class="modal-body">
                <div class="center prof-dtl mdl-prf" id="edit_prof1">
<?= LABEL_REMOVE_SELECTED ?>
                    <input type="hidden" name="current_item_id" id="current_item_id">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-yes" data-dismiss="modal">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Warning Modal -->



<script type="text/javascript">
    function RecommendationMarginBottom() {
        $('.request-sent-container:last').css('margin-bottom', '70px');
    }
    function RecommendationRemoveMarginBottom() {
        $('.request-sent-container:last').css('margin-bottom', '0px');
    }

    function formatRepo(repo) {
        if (repo.loading)
            return repo.text;

        return repo.text;
    }

    function formatRepoSelection(repo) {
        return repo.text;
    }


    $(document).ready(function () {

        $("body").delegate('.accordion_recommendation', 'click', function () {
            var this_ele = $(this);
            var target_ele = this_ele.find('.accordion-toggle');
            var css_height = this_ele.find('.accordion-body').css('height').replace('px', '');
            //console.log(css_height);
            if (css_height != 0) {
                target_ele.removeClass('active');
            }
        });


        $("#skills").select2({
            ajax: {
                url: '<?= $vObj->getURL("search/get_skills"); ?>',
                dataType: 'json',
                delay: 250,
                data: function (params) {
                    return {
                        q: params
                    };
                },
                processResults: function (data, page) {
                    // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to
                    // alter the remote JSON data
                    return {
                        results: data.items
                    };
                },
                results: function(data) { 

                    return {results: data.items};
                }
            },
            id: function(e) { return e.text; },
            escapeMarkup: function (markup) {
                return markup;
            }, // let our custom formatter work
            minimumInputLength: 1,
            templateResult: formatRepo, // omitted for brevity, see the source of this page
            templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });

        setTimeout(function () {
            //$(".accordion_recommendation").find('.accordion-toggle').addClass('active');
            $(".accordion_recommendation").find('.accordion-body').addClass('in').css({"height": "auto"});
            //accordion-body
        }, 150)
        RecommendationMarginBottom();
    })
//------------------------------Skill Section Start-----------------------------------------------------------
    $(document).on('click', '.btn-remove-skill', function () {
        var obj = this;
        var item_id = $(this).attr('data-item-id');
        $.ajax({
            type: 'POST',
            data: {
                'item_id': item_id,
                'callfrom': 'removeskill'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).closest('li').remove();
                if (data.total_rows == 0) {
                    $("." + data.clas).find($(".complete-tick-mark")).css("color", "");
                }
                if (data.scorer.resume_scorer + data.scorer.portfolio_scorer + data.scorer.profile_scorer > "74")
                {
                    $('.custom-p-color').html('Congratulations, you can publish your profile!');
                } else
                {
                    $('.custom-p-color').html('Reach 75% and get published!');
                }
                if (data.scorer.resume_scorer == "" || data.scorer.resume_scorer == "0")
                {
                    var resume_color = "#F9785B";
                }
                else
                {
                    var resume_color = "#76bdee";
                }
                if (data.scorer.portfolio_scorer == "" || data.scorer.portfolio_scorerr == "0")
                {
                    var portfolio_color = "#F9785B";
                }
                else
                {
                    var portfolio_color = "#c4dafe";
                }
//                new Morris.Donut({
//                    element: 'hero-donut',
//                    width:300,
//                    data: [
//                        {label: 'Profile', value: data.scorer.profile_scorer },
//                        {label: 'Resume', value: data.scorer.resume_scorer },
//                        {label: 'Portfolio', value: data.scorer.portfolio_scorer },
//                        {label: 'Incomplete', value: data.scorer.incomplete }
//                    ],
//                    colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                    formatter: function (y) {
//                        return y + "%"
//                    }
//                });
                //update_knob(data.scorer.fullstatus);
                var prof_comp = parseInt(data.scorer.profile_scorer) + parseInt(data.scorer.resume_scorer) + parseInt(data.scorer.portfolio_scorer);
                update_profile_completness_top_chart(prof_comp);
                publish_unpublish_btn(data.scorer.fullstatus);
            }
        });
    });



    $(document).on('click', '.btn-add-skills', function () {
        if ($('#skills').val() == "") {
            $('#skills').css({
                border: '1px solid red'
            });
            $('#skills').focus();
            return false;
        }
        if ($('#skills').val() != "") {
            $('#skills').css({
                border: ''
            });
        }
        var skill = $.trim($(this).prev().val());
        var obj = this;
        if (skill != "") {
            $.ajax({
                type: 'POST',
                data: {
                    'skill': skill,
                    'callfrom': 'skill'
                },
                url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                dataType: 'json',
                success: function (data) {
                    var alread_exist = false;
                    $(obj).closest('.key-details-section').find('.key-skills').find('.btn-remove-skill').each(function (i, v) {
                        var item_id = $(this).attr('data-item-id');
                        if (item_id == data.item_id) {
                            alread_exist = true;
                        }
                    });
                    if (!alread_exist) {
                        $(obj).closest('.key-details-section').find('.key-skills').append('<li><a class="button-main button-large" href="javascript:void(0);">' + skill + '<i data-item-id="' + data.item_id + '" class="icon-remove-circle btn-remove-skill"></i></a> </li>');
                        if (data.total_rows == 0) {
                            $("." + data.clas).find($(".complete-tick-mark")).css("color", "#96BF48");
                        }
                        if (data.scorer.resume_scorer + data.scorer.portfolio_scorer + data.scorer.profile_scorer > "74")
                        {
                            $('.custom-p-color').html('Congratulations, you can publish your profile!');
                        } else
                        {
                            $('.custom-p-color').html('Reach 75% and get published!');
                        }
                        if (data.scorer.resume_scorer == "" || data.scorer.resume_scorer == "0")
                        {
                            var resume_color = "#F9785B";
                        }
                        else
                        {
                            var resume_color = "#76bdee";
                        }
                        if (data.scorer.portfolio_scorer == "" || data.scorer.portfolio_scorerr == "0")
                        {
                            var portfolio_color = "#F9785B";
                        }
                        else
                        {
                            var portfolio_color = "#c4dafe";
                        }
                    }
                    $("#skills").parent().children('.select2-container').children('a').children('span').text('Software, Tools, Languages, Talents...');
//                    new Morris.Donut({
//                        element: 'hero-donut',
//                        width:300,
//                        data: [
//                            {label: 'Profile', value: data.scorer.profile_scorer },
//                            {label: 'Resume', value: data.scorer.resume_scorer },
//                            {label: 'Portfolio', value: data.scorer.portfolio_scorer },
//                            {label: 'Incomplete', value: data.scorer.incomplete }
//                        ],
//                        colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                        formatter: function (y) {
//                            return y + "%"
//                        }
//                    });
//                    update_knob(data.scorer.fullstatus);
                    var prof_comp = parseInt(data.scorer.profile_scorer) + parseInt(data.scorer.resume_scorer) + parseInt(data.scorer.portfolio_scorer);
                    update_profile_completness_top_chart(prof_comp);
                    publish_unpublish_btn(data.scorer.fullstatus);
                }
            });


        }
    });
//------------------------------Skill Section End-------------------------------------------------------------

//------------------------------Recommend Section Start-----------------------------------------------------------
    $(document).on('click', '.btn-recommend-request', function () {
        var textfield_placeholder = '<?= LABEL_RECOMMENDATION_TEXTFIELD_PLACEHOLDER ?>';
        var textfield_error_placeholder = '<?= LABEL_RECOMMENDATION_TEXTFIELD_ERROR_PLACEHOLDER ?>';

        if ($('#recommendation_email').val() == "") {
            $('#recommendation_email').css({
                border: '1px solid red'
            });
            $('#recommendation_email').focus();
            return false;
        }

        if ($('#recommendation_email').val() != "") {
            var email = $('#recommendation_email').val();
            var pattern = /^[A-z0-9]+([A-z0-9]+[\.\_]?[A-z0-9]+)*@[A-z0-9]{2,}(\.[A-z0-9]{1,}){1,2}[A-z]$/;
            if (!pattern.test(email)) {
                alert(textfield_error_placeholder);
                $('#user_email').css({
                    border: '1px solid red'
                });
                $('#recommendation_email').focus();
                return false;
            }
            $('#recommendation_email').css({
                border: ''
            });
        }

        var obj = this;
        var recommend_email = $.trim($('#recommendation_email').val());

        if (recommend_email != "") {
            $('.loader-save-holder').show();
            $.ajax({
                type: 'POST',
                data: {
                    'recommend_email': recommend_email,
                    'callfrom': 'sendrecommend'
                },
                url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                dataType: 'json',
                success: function (data) {
                    $(obj).prev().val('');
                    $('.loader-save-holder').hide();
                    if (data.status) {
                        //var html='<div class="request-sent-container"><div class="span8 request-left-area"><h2>'+data.email+'</h2><span>'+data.datesent+'</span></div><div data-item-id="'+data.item_id+'" class="pull-left span3 request-right-area"><a class="our-btn reminder-btn resend-reminder-btn">SEND REMINDER</a><i class="icon-remove resend-reminder-remove-btn"></i><div class="clearfix"></div></div></div><div class="clearfix"></div>';
                        /*var html='<div class="request-sent-container"><div class="span9 request-left-area"><h2>'+data.email+'</h2><span>'+data.datesent+'</span><div data-item-id="'+data.item_id+'" class="pull-left span3 request-right-area"></div><i class="icon-remove resend-reminder-remove-btn pull-left"></i><a class="our-btn reminder-btn resend-reminder-btn pull-left button-main inverted button-large">SEND REMINDER</a></div><div class="clearfix"></div></div>';*/
                        var html = '<div class="request-sent-container"><div class="span9 request-left-area"><h2>' + data.email + '</h2><span>' + data.datesent + '</span></div><div data-item-id="' + data.item_id + '" class="pull-left span3 request-right-area"><i class="icon-remove resend-reminder-remove-btn send-reminder pull-left"></i><a class="our-btn reminder-btn resend-reminder-btn pull-left button-main inverted button-large">SEND REMINDER</a></div><div class="clearfix"></div></div>';
                        var alread_exist = false;
                        $('.recommendation_send_list').find('.request-right-area').each(function (i, v) {
                            var item_id = $(this).attr('data-item-id');
                            if (item_id == data.item_id) {
                                alread_exist = true;
                            }
                        });
                        $('.recommendation_send_list').css('display', '');
                        if (!alread_exist) {
                            RecommendationRemoveMarginBottom();
                            $('.recommendation_send_list_inner').find('.span12').append(html);
                            RecommendationMarginBottom();
                            if (data.total_rows == 0) {
                                $("." + data.clas).find($(".complete-tick-mark")).css("color", "#96BF48");
                            }
                            $(".seconddiv").text(function () {
                                return $(this).text().replace("[EMAIL]", recommend_email);
                            });
                            $("#recommendation_modal").modal('show');
                        }
                    } else {
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }

                }
            });
        }
    });

    $(document).on('click', '.resend-reminder-remove-btn', function () {
        var item_id = $(this).parent().attr('data-item-id');
        var obj = $(this);
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: {
                'recommend_id': item_id,
                'callfrom': 'removerecommend'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).prev().val('');
                $('.loader-save-holder').hide();
                if (data.status) {
                    //$(obj).closest('.request-sent-container').next('div').remove();
                    //$(obj).parent('.request-sent-container').remove();
                    obj.parent().parent().remove();
                    /*if (data.total_rows == 0) {
                     $("." + data.clas).find($(".complete-tick-mark")).css("color", "");
                     $(".recommendation_send_list").css("display","none");
                     }*/
                }
                /*  if( $.trim($(".recommendation_send_list_inner").find('.span12').html()).length == 0){
                 $(".recommendation_send_list").css("display","none");
                 }*/

            }
        });
    });

    $(document).on('click', '.resend-reminder-btn', function () {
        var item_id = $(this).parent().attr('data-item-id');
        var obj = this;
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: {
                'recommend_id': item_id,
                'callfrom': 'resendrecommend'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).prev().val('');
                $('.loader-save-holder').hide();
                if (data.status) {
                    $(obj).parent().prev().find('span').html(data.datesent);
                }

            }
        });
    });

    $(document).on('click', '.recommendation_pending_approval_approve_btn', function () {
        var item_id = $(this).parent().attr('data-item-id');
        var obj = this;
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: {
                'recommend_id': item_id,
                'callfrom': 'approverecommend'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).prev().val('');
                $('.loader-save-holder').hide();
                if (data.status) {
                    var html = '<div class="request-pending-container request-pending-container' + data.recommend_id + '"><div class="span9 pending-left-area" data-item-id="' + data.recommend_id + '"><h2>' + data.recommend_name + '</h2><span class="recomm-company">' + data.recommend_company + '</span><br clear="all"><span class="recomm-dept">' + data.recommend_dept + '</span><p class="recomm-dtl">' + data.recommendation + '</p></div><div class="span3 pending-right-area"><i class="icon-remove recommendation_approved_remove_btn pull-right"></i></div></div><div class="clearfix"></div>';
                    if ($('.recommendation_approved_list').css('display') == 'none') {
                        $('.recommendation_approved_list').css('display', '');
                    }
                    $('.recommendation_approved_list_inner').find('.span12').append(html);
                    $(obj).closest('.request-pending-container').next('div').remove();
                    $(obj).closest('.request-pending-container').remove();
                    if ($.trim($(".recommendation_pending_list_inner").find('.span12').html()).length == 0) {
                        $(".recommendation_approval_list").css("display", "none");
                    }
                }

            }
        });
    });

    $(document).on('click', '.recommendation_pending_approval_remove_btn', function () {
        var item_id = $(this).parent().attr('data-item-id');
        var obj = this;
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: {
                'recommend_id': item_id,
                'callfrom': 'remove-pending-recommend'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).prev().val('');
                $('.loader-save-holder').hide();
                if (data.status) {
                    $(obj).closest('.request-pending-container').next('div').remove();
                    $(obj).closest('.request-pending-container').remove();
                    if (data.total_rows == 0) {
                        $("." + data.clas).find($(".complete-tick-mark")).css("color", "");
                    }
                    //      $(obj).parent().prev().find('span').html(data.datesent);
                }

            }
        });
    });

    /*Approved Recommendation Remove*/
    $(".btn-yes").click(function () {
        var item_id = $("#current_item_id").val();
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: {
                'recommend_id': item_id,
                'callfrom': 'remove-approved-recommend'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $('.loader-save-holder').hide();
                if (data.status) {
                    $(".request-pending-container" + item_id).next('div').remove();
                    $(".request-pending-container" + item_id).remove();
                }
                if ($.trim($(".recommendation_approved_list_inner").find('.span12').html()).length == 0) {
                    $(".recommendation_approved_list").css("display", "none");
                }
            }
        });
    });
    $(document).on('click', '.recommendation_approved_remove_btn', function () {
        $("#warning_modal").modal("show");
        var item_id = $(this).parent().attr('data-item-id');
        $("#current_item_id").val(item_id);
    });
    /*Approved Recommendation Remove*/

//------------------------------Recommend Section Start-----------------------------------------------------------
</script>


<script>
//saving skills on enter
    var save_skill = function (e) {
        if (e.keyCode == '13')
            $('.btn-add-skills').trigger("click");
    };
    $('#skills').keyup(save_skill);

//sending email request on enter
    var send_email_request = function (e) {
        if (e.keyCode == '13')
            $('.btn-recommend-request').trigger("click");
    };
    $('#recommendation_email').keyup(send_email_request);
//add social profile url on enter
    var add_social_profile = function (e) {
        if (e.keyCode == '13')
            $('#social_url_btn').trigger("click");
    };
    $('#social_url').keyup(add_social_profile);
</script>