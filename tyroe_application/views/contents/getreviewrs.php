<?php if($get_reviewers != ''){ ?>
    <?php for($a=0; $a<count($get_reviewers); $a++){
//       if($get_reviewers[$a]['user_id'] == $this->session->userdata('user_id')){
//           continue;
//       }
        if(intval($job_role['is_admin']) == 0 && intval($job_role['is_reviewer']) == 1  && $get_reviewers[$a]['user_id'] != $this->session->userdata('user_id'))continue;
        $image;
        if ($get_reviewers[$a]['media_name'] != "") {
            $image = ASSETS_PATH_PROFILE_THUMB . $get_reviewers[$a]['media_name'];
        } else {
            $image = ASSETS_PATH_NO_IMAGE;
        }
        ?>
        <div class="reviewers review-mg">
            <?php
            if($get_reviewers[$a]['user_id'] == $this->session->userdata('user_id') && $get_reviewers[$a]['job_id'] != '' && $job_detail['archived_status'] != 1){ ?>
            <div class="span2 reviewer_support_tools">
                <ul class="actions edit-ul-sec" style="float: right">
                    <li><a href="javascript:void(0)" class="edit_reviews"
                           data-id="<?php echo $get_reviewers[$a]['reviewer_review']['review_id'] ?>"
                           data-value="<?php echo $get_reviewers[$a]['reviewer_review']['candidate_id']; ?>,<?php echo $get_reviewers[$a]['firstname'] . ' ' . $get_reviewers[$a]['lastname'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['level_id'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['technical'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['creative'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['impression'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['review_comment'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['action_shortlist'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['action_interview'] ?>"
                            ><i class="table-edit"></i></a></li>
                    <li class="last"><a href="javascript:void(0)" class="delete_tyroe_review"
                                        data-id="<?php echo $get_reviewers[$a]['reviewer_review']['review_id'] ?>,<?php echo $get_reviewers[$a]['reviewer_review']['candidate_id']; ?>"><i
                                class="table-delete"></i></a></li>
                </ul>
            </div>
            <?php }else { ?>
            <div class="span2">
                <!--<img src="<?php /*echo $image; */?>" class="img-circle avatar-50 pull-right">-->
            </div>
            
            <?php } ?>
            <div class="span3 reviewer-text-sp">
                    <div class="clearfix"></div>
                <span class="reviewer_name"><?php 
                    if($get_reviewers[$a]['firstname'] != ''){
                        echo $get_reviewers[$a]['firstname'].' '.$get_reviewers[$a]['lastname']; 
                    }else{
                        echo 'Name not available'; 
                    }    
                    
                ?></span>
                <div class="clearfix"></div>
                <span class="designation"><?php 
                    if($get_reviewers[$a]['job_title'] != ''){
                        echo $get_reviewers[$a]['job_title']; 
                    }else{
                        echo '<label class="label label-info rt-pending" style="background: #5ea4d8 !important;">Invitation Pending</label>'; 
                    }  
                    
                ?></span>
                <div class="clearfix"></div>
                <?php if($get_reviewers[$a]['action_interview'] != ''){ ?>
                    <span class="label label-info"><?php echo $get_reviewers[$a]['action_interview']; ?></span>
                <?php } ?>
                <?php if($get_reviewers[$a]['action_shortlist'] != ''){ ?>
                    <span class="label label-info"><?php echo $get_reviewers[$a]['action_shortlist']; ?></span>
                <?php } ?>
                <div class="clearfix"></div>
                <div id="editors_pick" class="edit-mg">
                <div id="editors_pick_info">
                <?php
                 if($get_reviewers[$a]['job_title'] != '' && $get_reviewers[$a]['user_id'] != $this->session->userdata('user_id')){
                ?>
                <div class="btn-group large group_icons ft-btn-sp ft-ms">
                    <button class="glow btn-msg right" data-value="<?=$get_reviewers[$a]['user_id']?>"><i class="icon-envelope "></i></button>
                </div>
                <?php
                 }
                ?>
                </div></div>


            </div>
            <div class="span4">
                <?php if($get_reviewers[$a]['job_id'] != ''){ ?>
                <div class="jQueryClassReadmoreParagraphFunc1" style="overflow: hidden"><span>
                        <b><?php echo $level_result =($get_reviewers[$a]['level_title']=='Intern') ? 'Entry Level' : $get_reviewers[$a]['level_title']; ?>: </b>
                            <?php echo nl2br($get_reviewers[$a]['review_comment']); ?>
                    </span></div>
                <?php }else{ ?>
                    <span class="btn btn-danger rt-danger">Review pending</span>
                <?php } ?>
            </div>
            <?php if($get_reviewers[$a]['job_id'] != ''){ ?>
            <div class="span2 rt-reviewer-score"><span><?php echo round($get_reviewers[$a]['review_average']); ?></span></div>
            <?php } ?>
            <div class="clearfix"></div>
        </div>
    <?php } ?>
<?php } ?>