<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        $(".activity_val").on("click", function () {
            var id=$(this).attr("data-value");
             $('.id').val(id);
            if ($(this).attr("data-id") == '3') {
                $("#form_stream").attr("action","<?= $vObj->getURL('exposure/feedback_rating') ?>")
            } else if ($(this).attr("data-id") == '6') {
                $("#form_stream").attr("action", "<?= $vObj->getURL('jobopening') ?>")
            }
            $("#form_stream").submit();

        });

        $(".search_by").on("click", function () {

            if ($(this).attr("data-value") == "2") {
                $("#stream_duration").val("week");
            }
            else if ($(this).attr("data-value") == "3") {
                $("#stream_duration").val("month");
            }
            else if ($(this).attr("data-value") == "1") {
                $("#stream_duration").val("all");
            }
            FilterStreams();

        });

        $("select").on("change", function () {
            var str = "";
            $("select option:selected").each(function () {
                str += $(this).text() + " ";
            });
            if ($(this).val() != "") {
                $(".activity_type_label").text(str);
            }
        })
        $('#stream_keyword').donetyping(function (){
                    FilterStreams();
         },500);
//.change();

        /*Clear button */
        $("#clear_btn").click(function () {
            $(this).closest('form').find("input[type=text], textarea,select").val("");
            FilterStreams();
        });

    });

    function applicaint_call(job_id,job_title){
            $('input[name="applicaint_url_trigger"]').val('yes');
            var job_title = job_title.toLowerCase();
            job_title = job_title.replace(/ /gi,'-');
            var redirect_url = "<?=$vObj->getURL("openings")?>/"+job_title+'-'+job_id;
            $('#form_stream').attr('action',redirect_url)
            $('#form_stream').submit();
        }
        function shortlist_call(job_id,job_title){
            $('input[name="shortlist_url_trigger"]').val('yes');
            var job_title = job_title.toLowerCase();
            job_title = job_title.replace(/ /gi,'-');
            var redirect_url = "<?=$vObj->getURL("openings")?>/"+job_title+'-'+job_id;
            $('#form_stream').attr('action',redirect_url)
            $('#form_stream').submit();
        }
        function candidate_call(job_id,job_title){
            $('input[name="candidate_url_trigger"]').val('yes');
            var job_title = job_title.toLowerCase();
            job_title = job_title.replace(/ /gi,'-');
            var redirect_url = "<?=$vObj->getURL("openings")?>/"+job_title+'-'+job_id;
            $('#form_stream').attr('action',redirect_url)
            $('#form_stream').submit();
        }

    function FilterStreams() {
        $('.loader-save-holder').show();
        var activity_type = $("#activity_type").val();
        var stream_keyword = $("#stream_keyword").val();
        var stream_duration = $("#stream_duration").val();
        var data = "activity_type=" + activity_type + "&stream_keyword=" + stream_keyword + "&stream_duration=" + stream_duration;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("stream/FilterStream")?>',

            success: function (data) {
                $('.loader-save-holder').hide();
                $(".no-filter").hide();
                var data_arr = data.split('<!--|||-->');
                $(".filter-result").html(data_arr[0]);
                $('#footer_pagination').html(data_arr[1]);
                /*if (data.success) {

                 $(".no-filter").hide();
                 $(".filter-result").html(data.success_message);
                 }
                 else {
                 $(".notification-box-message").css("color", "#b81900")
                 $(".notification-box-message").html(data.success_message);
                 $(".notification-box").show(100);
                 setTimeout(function () {
                 $(".notification-box").hide();
                 }, 5000);
                 }*/
            }

        });
    }
</script>
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">

<div class="container-fluid">
<!-- upper main stats -->


<!-- upper main stats -->
   <?php
    if($this->session->userdata('role_id') == '2')
        $this->load->view('upper_stats');
   ?>
   <!-- end upper main stats -->
<!-- end upper main stats -->
<div id="pad-wrapper" class="margin-top-1">

    <div class="grid-wrapper">

        <div class="row-fluid show-grid">

            <!-- START LEFT COLUMN -->
            <?php
            //$this->load->view('left_coloum_stream');
            $this->load->view('left_coloumn');
            ?>
            <!-- END LEFT COLUMN -->

            <!-- START RIGHT COLUMN -->
            <!-- START JOB OPENING -->
            <div class="pointer">
                <div class="arrow"></div>
                <div class="arrow_border"></div>
            </div>
            <div class="span8 sidebar padding-top-11" id="opening-overview">

                    <div class="right-column" style="position:relative;">

                        <div class="span12">
                            <h4>Latest Activity Stream | <span class="activity_type_label">All</span></h4>
                            <!--Tooltip -->
                            <!--<span class="pull-right button-center"><a
                                        href="javascript:void(0)" class="someClass"
                                        title="<?php /*echo LABEL_ALL_STREAM_TOOLTIP_TEXT */?>">
                                        <img src="<?php /*echo ASSETS_PATH */?>img/bar-graph-icon.png" width="30px"
                                             height="30px"/>
                                    </a></span>-->

                            <div class="row-fluid margin-top-8">
                                <div class="container span4 pull-right activity-last">
                                    <div class="field-box">
                                        <div class="btn-group pull-left stream-view">
                                            <button class="glow left search_by" data-value="1">All</button>
                                            <button class="glow search_by" data-value="2">This Week</button>
                                            <button class="glow right search_by" data-value="3">This Month</button>
                                            <input type="hidden" name="stream_duration" id="stream_duration" value="">
                                        </div>
                                    </div>
                                </div>

                                <div class="field-box span4 pull-right activity-keyword tab-margin">
                                    <div class="span12 ctrls">
                                        <input type="text" name="stream_keyword" id="stream_keyword"
                                               value="<?= $search_stream_keyword ?>"
                                               placeholder="Keywords"
                                               class="search span12 padding-1">
                                    </div>
                                </div>

                                <div class="span4 pull-right activity-search tab-margin">
                                    <div class="container span12">

                                        <div class="field-box">
                                            <div class="ui-select find">
                                                <select class="span5 inline-input" name="activity_type"
                                                        id="activity_type" onchange="FilterStreams();">
                                                    <option value="">Select Type</option>
                                                    <?php
                                                    foreach ($get_activity_type as $key => $activity_type) {
                                                        if($search_activity_type == $activity_type['activity_type_id'])
                                                        {
                                                            $selected = 'selected="selected"';
                                                        }else{
                                                            $selected="";
                                                        }
                                                        ?>
                                                        <option <?=$selected?>
                                                            value="<?php echo $activity_type['activity_type_id'] ?>"><?php echo $activity_type['activity_title'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="clearfix"></div>

                            </div>

                        </div>
                        <!--<form enctype="multipart/form-data" id="form_stream" class="form_stream" name="stream"
                                                              method="post" action="<?php/*//= $vObj->getURL('exposure/feedback_rating') */?>">-->
                        <input type="hidden" name="applicaint_url_trigger">
                        <input type="hidden" name="candidate_url_trigger">
                        <input type="hidden" name="shortlist_url_trigger">
                        <div class="row-fluid">
                            <div class="span12 margin-top-8">
                                <div class="navbar navbar-inverse">
                                    <ul style="margin: 0 10px 10px 10px; list-style:none;">
                                        <li class="stream-container">
                                            <div class="stream-dialog">
                                                <div class="filter-result"></div>

                                                <div class="no-filter notes">

                                                    <?php
                                                    if (is_array($activity_stream) == "") {
                                                        ?>
                                                        <div class="alert alert-info">
                                                            <i class="icon-exclamation-sign"></i>
                                                            There is no activity

                                                        </div><?php

                                                    } else {
                                                        if ($this->session->userdata('role_id') == '3' || $this->session->userdata('role_id') == '4') {
                                                            foreach ($activity_stream as $job_activity_log) {
                                                                $icon;
                                                                $text;
                                                                $link = "";
                                                                $href = "javascript:void(0)";
                                                                $onclick = "";
                                                                if ($job_activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                                                                    /*JOB APPLICANT SECTION*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                                                                    /*Profile Favourited Section*/
                                                                    $icon = LABEL_ICON_STAR;
                                                                    $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                                    $text = "You added " . $activity_data_email['tyroe_name'] . " to your Favourites";
                                                                    $href = $vObj->getURL('favourite');

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                                                                    /*JOB CANDIDATE Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                                    /*JOB SHORTLIST Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_NEW_JOB_LOG) {
                                                                    /*OPENINGS Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                                    /*Feedback Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    if(count($return_activity_stream) == 0)continue;
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                }
                                                                ?>
                                                                <a href="<?=$href?>" class="activity_val" data-id="<?= $link ?>" data-value="<?= $id ?>" onclick="<?=$onclick?>">
                                                                <span class="item" >
                                                                <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                                                                    <?= $text ?>
                                                                    <span class="time"><i
                                                                            class="icon-time"></i> <?php echo get_activity_time($job_activity_log['created_timestamp']); ?></span>
                                                                </a></span>
                                                                <?php
                                                            }?>
                                                        <?php } else {
                                                            #Tyroe
                                                            foreach ($activity_stream as $activity_log) {
                                                                $icon;
                                                                $text;
                                                                $link = "";
                                                                $href = "javascript:void(0)";
                                                                $onclick = "";
                                                                if ($activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                                                                    /*JOB APPLICANT SECTION*/
                                                                    $return_activity_stream = $vObj->activity_sub_type_text($activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $activity_sub_type = $activity_data_email['activity_sub_type'];

                                                                }  else if ($activity_log['activity_type_id'] == LABEL_PROFILE_VIEW_JOB_LOG) {
                                                                    /*Profile Views Section*/
                                                                    $icon = LABEL_ICON_VIEW;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_VIEW_PROFILE_TEXT_LOG . " " . $activity_data_email['job_title'].", ".$activity_data_email['company_name'];

                                                                } else if ($activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                                                                    /*Profile Favourited Section*/
                                                                    $icon = LABEL_ICON_STAR;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_FAVOURITED_PROFILE_TEXT . " by " . $activity_data_email['job_title']." from ".$activity_data_email['company_name'];

                                                                } else if ($activity_log['activity_type_id'] == LABEL_RECOMMEND_COMPLETE) {
                                                                    /*Recommedation Section*/
                                                                    $icon = LABEL_ICON_MOVE;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_RECOMMEND_COMPLETE_TEXT . "  " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                                                                    $href = $vObj->getURL("profile/#acc-5");

                                                                } else if ($activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                                    /*Feedback Section*/
                                                                    $icon = LABEL_ICON_ENVLOPE;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    //$text = LABEL_FEEDBACK_ACTIVITY_TEXT . "  " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                                                                    $text = LABEL_FEEDBACK_ACTIVITY_TEXT . $activity_data_email['reviewer_company_name'];
                                                                    $url ="feedback/".strtolower(str_replace(' ', '-', $activity_data_email['rev_name']) . "-" . $vObj->generate_job_number($activity_data_email['rev_id']));
                                                                    $href = $vObj->getURL($url);

                                                                } else if ($activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                                    /*JOB SHORTLIST Section*/
                                                                    $icon = LABEL_ICON_MOVE;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_SHORTLIST_JOB_LOG_TEXT . " " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                                                                    $url = "opening/".strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                                                                    $href = $vObj->getURL($url);

                                                                } else if ($activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                                                                    /*JOB CANDIDATE Section*/
                                                                    $icon = LABEL_ICON_MOVE;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_JOB_CANDIDATE_ACTIVITY_TEXT . " " . $activity_data_email['job_title']." at ".$activity_data_email['company_name'];
                                                                    $url = "opening/".strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                                                                    $href = $vObj->getURL($url);

                                                                } else if ($activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                                    /*Feedback Section*/
                                                                    $icon = LABEL_ICON_ENVLOPE;
                                                                    $activity_data_email = unserialize(stripslashes($activity_log['activity_data']));
                                                                    $text = LABEL_FEEDBACK_ACTIVITY_TEXT  .$activity_data_email['reviewer_company_name'];
                                                                    $href = $vObj->getURL("feedback");

                                                                }
                                                                ?>
                                                            <a href="<?=$href?>" class="activity_val" data-id="<?= $link ?>" data-value="<?= $id ?>" onclick="<?=$onclick?>">
                                                                <span class="item" >
                                                                <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                                                                    <?= $text ?>
                                                                    <span class="time"><i
                                                                            class="icon-time"></i> <?php echo get_activity_time($activity_log['created_timestamp']); ?></span>
                                                            </a></span>

                                                                <?php
                                                            }
                                                        }

                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                            <input type="hidden" value="" name="id" class="id">
                            <!--<input type="hidden" value="" name="action_name">-->
                            <input type="hidden" name="applicaint_url_trigger">
                            <input type="hidden" name="candidate_url_trigger">
                            <input type="hidden" name="shortlist_url_trigger">
                        <!--</form>-->
                        <form method="post" action="" id="jumptoform">
                            <input type="hidden" name="job_id" id="job_id" value="">
                            <input type="hidden" name="job_title" id="job_title" value="">
                            <input type="hidden" name="action" id="action" value="">
                        </form>
                    </div>
             <!--pagination>-->
            <div id="footer_pagination">
                <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="stream"
                                      method="post" action="<?= $vObj->getURL('activity-stream') ?>">
                    <div class="span12 pagination"><?= $pagination ?></div>
                    <input type="hidden" value="<?= $page ?>" name="page">
                    <input type="hidden" value="<?= $search_activity_type ?>" name="search_activity_type">
                    <input type="hidden" value="<?= $search_stream_keyword ?>" name="search_stream_keyword">
                    <input type="hidden" value="<?= $search_stream_duration ?>" name="search_stream_duration">
                </form>
            </div>
            <!--pagination end-->
            </div>
            <!-- END JOB OPENING -->


            <!-- END RIGHT COLUMN -->

        </div>
    </div>
</div>
</div>
</div>
<!-- end main container -->
