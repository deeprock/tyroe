<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">

    function Add(table_name, coloum_name) {
        if ($('#refrence').val() == "") {
            $('#refrence').css({
                border: '1px solid red'
            });
            $('#refrence').focus();
            return false;
        }

        if ($('#refrence').val() != "") {
            $('#refrence').css({
                border: ''
            });
            var refrence = $("#refrence").val();
            var ref_id = $("#ref_id").val();
            var data = "data=" + refrence + "&table_name=" + table_name + "&coloum_name=" + coloum_name + "&id=" + ref_id;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("resume/save")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".field-box").hide();
                        $("#message").html(data.success_message).delay(5000);
                        window.location = "<?=$vObj->getURL("resume")?>";
                    }
                    else {
                        $("#error_msg").html(data.success_message);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }
</script>

<div class="row-fluid form-wrapper">
    <div class="span12">
        <div class="container-fluid">
            <div class="span4 default-header">
                <h4>
                    <?php
                    if ($flag == "edit") {
                        echo LABEL_EDIT_REFERENCES;
                    } else {
                        echo LABEL_ADD_REFERENCES;
                    }
                    ?>
                </h4>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1"
                  enctype="multipart/form-data" name="profile_form" method="post">
                <div class="field-box">
                    <label><?= LABEL_REFRENCE_TITLE ?>:</label>
                    <input class="span9" type="text"
                           name="refrence" id="refrence"
                           value="<?php echo $refrence_data['refrence_detail']; ?>">
                </div>
                <div class="span11 field-box actions">
                    <input type="hidden" name="ref_id" id="ref_id"
                           value="<?= $refrence_data['refrence_id']; ?>"/>
                    <input type="button" class="btn-glow primary"
                           onclick="Add('<?= TABLE_REFRENCES ?>','refrence_detail');"
                           value="<?= LABEL_SAVE_CHANGES ?>">
                    <span>OR</span>
                    <input type="reset" value="Cancel" class="reset"
                           onclick="window.history.back()">
                </div>
            </form>
        </div>
        <span id="message" class="success_message"></span>
    </div>
</div>

