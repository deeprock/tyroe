<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); echo "d";die;?>

<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/jquery.ui.css">
<script>

</script>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>

                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">
                            <div class="span12">
                                <div class="container-fluid">
                                    <div class="span4 default-header">
                                        <h4><?= LABEL_JOB_OPENING ?></h4>
                                    </div>

                                    <div class="field-box">
                                        <span id="error_msg"><?php echo validation_errors();
                                            ?></span>
                                    </div>
                                    <form class="new_user_form inline-input" _lpchecked="1"
                                          enctype="multipart/form-data" name="opening_form" id="opening_form"
                                          method="post" action="<?= $vObj->getURL('jobopening/SaveOpening') ?>"
                                          onsubmit="return form_opening();">
                                        <div class="field-box">
                                            <label><?= LABEL_JOB_TITLE ?>:</label>
                                            <input class="span9" type="text"
                                                   name="job_title" id="job_title"
                                                   value="<?php echo $edit_job['job_title']; ?>">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_JOB_LOCATION ?>:</label>

                                            <div class="ui-select span6">
                                                <select class="span5 inline-input" name="country_id"
                                                        id="country_id">
                                                    <option value="">Select Country</option>
                                                    <?php
                                                    foreach ($get_countries as $key => $countries) {
                                                        ?>
                                                        <option <?php if ($countries['id'] == $edit_job['name']) {
                                                            echo "selected='selected'";
                                                        } else {
                                                        } ?>
                                                            value="<?php echo $countries['id'] ?>"><?php echo $countries['name'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_JOB_SKILL_LEVEL ?>:</label>

                                            <div class="ui-select span6">
                                                <select class="span5 inline-input" name="skill_level" id="skill_level">
                                                    <option value="">Select Level</option>
                                                    <?php
                                                    foreach ($get_job_level as $key => $job_level) {
                                                        ?>
                                                        <option <?php if ($edit_job['job_level_id'] == $job_level['job_level_id']) {
                                                            echo "selected='delected'";
                                                        } else {
                                                        } ?>
                                                            value="<?php echo $job_level['job_level_id'] ?>"><?php echo $job_level['level_title'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_JOB_QUANTITY ?>:</label>
                                            <input class="span9" type="text"
                                                   name="job_quantity" id="job_quantity"
                                                   value="<?php echo $edit_job['job_quantity']; ?>"
                                                   placeholder="Numbers Only">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_START_DATE ?>:</label>
                                            <input class="span9 start_date" type="text" name="job_start" id="job_start"
                                                   value="<?php echo $edit_job['start_date']; ?>"/>
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_END_DATE ?>:</label>
                                            <input class="span9 inline-input" type="text"
                                                   name="job_end" id="job_end"
                                                   value="<?php echo $edit_job['end_date']; ?>">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_JOB_DESCRIPTION ?>:</label>
                                            <textarea class="span9 inline-input"
                                                      name="job_description"
                                                      id="job_description"><?php echo $edit_job['job_description']; ?></textarea>

                                        </div>

                                        <div class="field-box">
                                            <table style="border-collapse: separate;border-spacing: 20px;">
                                                <tr>
                                                    <?php foreach ($predefine_skills as $k => $v) {
                                                        $count++; ?>
                                                        <td><input class="user" type="checkbox"
                                                                   name="job_skill[]" id="job_skill"
                                                                   value="<?= $v['creative_skill_id']; ?>"
                                                                <?php
                                                                if ($skill_data[$k]['creative_skill_id'] == $v['creative_skill_id']) {
                                                                    echo "checked=''";
                                                                }
                                                                ?>
                                                                >
                                                            &nbsp;&nbsp;<?=
                                                            $v['creative_skill']; ?></td>
                                                        <?php if ($count == 4) {
                                                            echo "</tr><tr>";
                                                            $count = 0;
                                                        }
                                                    } ?>
                                                </tr>
                                            </table>
                                        </div>
                                        <input type="hidden" name="hidden_edit_id"
                                               value="<?php echo $edit_job['job_id']; ?>">
                                        <input type="hidden" name="created_timestamp"
                                               value="<?php echo $edit_job['created_timestamp']; ?>">

                                        <div class="span11 field-box actions">
                                            <input type="submit" class="btn-glow primary"
                                                   value="<?= LABEL_SAVE_CHANGES ?>">
                                            <span>OR</span>
                                            <input type="reset" value="Cancel" class="reset"
                                                   onclick="window.history.back()">
                                        </div>
                                    </form>
                                </div>
                                <span id="message"></span>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
