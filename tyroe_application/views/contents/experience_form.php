<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!--<script src="http://code.jquery.com/jquery-latest.min.js"></script>-->
<script type="text/javascript">
    function CurrentJob() {
        if ($('input[name=current_job]:checked').val()) {
            $("#job_end").attr('readonly', true);
            $("#job_end").css('background', "#d0dde9");
        }
        else {
            $("#job_end").attr('readonly', false);
            $("#job_end").css('background', "#fff");

        }

    }
    function AddExperience() {
        var company_name = $("#company_name").val();
        var job_title = $("#job_title").val();
        var current_job = $('input[name=current_job]:checked').val();
        var job_start = $("#job_start").val();
        var job_end = $("#job_end").val();
        var job_description = $("#job_description").val();
        var job_url = $("#job_url").val();
        var job_country = $("#job_country").val();
        var job_city = $("#job_city").val();
        var exp_id = $("#exp_id").val();

        if (job_title == '') {
            $('#job_title').css({ border:'1px solid red'});
            $('#job_title').focus();
            return false;
        } else {
            $('#job_title').removeAttr("style")
        }
        if (company_name == '') {
            $('#company_name').css({ border:'1px solid red'});
            $('#company_name').focus();
            return false;
        } else {
            $('#company_name').removeAttr("style")
        }
        if (job_start == '') {
            $('#job_start').css({ border:'1px solid red'});
            $('#job_start').focus();
            return false;
        } else {
            $('#job_start').removeAttr("style")
        }
        if (current_job != 'on') {
            if (job_end == '') {
                $('#job_end').css({ border:'1px solid red'});
                $('#job_end').focus();
                return false;
            } else {
                $('#job_end').removeAttr("style")
            }
        }


        if (current_job) {
            current_job = '1'
            job_end = ''
        }
        else {
            current_job = '0'
        }
        var data = "company_name=" + company_name + "&job_title=" + job_title + "&current_job=" + current_job
                + "&job_start=" + job_start + "&job_end=" + job_end + "&job_description=" + job_description
                + "&exp_id=" + exp_id + "&job_url=" + job_url + "&job_country=" + job_country + "&job_city=" + job_city;
        $.ajax({
            type:"POST",
            data:data,
            url:'<?=$vObj->getURL("resume/save_experience")?>',
            dataType:"json",
            success:function (data) {
                if (data.success) {
                    $(".field-box").hide();
                    //$("#message").html(data.success_message);
                    window.location = "<?=$vObj->getURL("resume")?>";
                }
                else {
                    $("#error_msg").html("Fail to save experience");
                }
            },
            failure:function (errMsg) {
            }
        });
    }

    $("#job_start").datepicker({dateFormat: 'dd-mm-yy' });
    $("#job_end").datepicker();

    function GetStates(ddname) {
        var country_name = $("#user_country option:selected").val();
        var data = "country=" + country_name + "&ddname=" + ddname;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("profile/get_state_by_country")?>',

            success: function (data) {
                //$(".all-states").hide();
                $(".all-states").remove();
                $(".result-states").html(data);
            }
        });
    }

</script>

<!-- START RIGHT COLUMN -->


    <div class="span5 tab-margin">
        <div class="container-fluid">
            <div class="span4 default-header tab-margin headerdiv">
                <h4><?php if ($flag == "edit") {
                        echo LABEL_EDIT_EXPERIENCE;
                    } else {
                        echo LABEL_ADD_EXPERIENCE;
                    }

                    ?>
                </h4>
            </div>

            <div class="field-box">
                <span id="error_msg"><?php //echo validation_errors();

                    //if($msg!=""){echo $msg;}
                    ?></span>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1"
                  enctype="multipart/form-data" id="profile_form" name="profile_form"
                  method="post">
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_JOB_TITLE ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text"
                           name="job_title" id="job_title"
                           value="<?php echo $exp_data['job_title']; ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_COMPANY_NAME ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text"
                           name="company_name" id="company_name"
                           value="<?php echo $exp_data['company_name']; ?>"></div>
                </div>
                <?php
                if($this->session->userdata("current_job")<=0 || $exp_data['current_job'] == 1){
                ?>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_CURRENT_JOB ?>:</label></div>
                        <div class="span3 margin-adjust tab-margin"> <input type="checkbox" class="user" <?php if ($exp_data['current_job'] == 1) {
                        echo "checked=''";
                    } ?> name="current_job" id="current_job" onclick="CurrentJob();"></div>
                </div><?php
                }
                ?>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_START_DATE ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span2" type="datetime" name="job_start" id="job_start"
                           value="<?php
                           if ($flag == "edit"){
                           echo date("m/d/Y",$exp_data['job_start']);} ?>"></div>

                    <div class="bfh-datepicker"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_END_DATE ?>:</label></div>
                        <div class="span3 tab-margin"><input class="span2 inline-input" type="text"
                           name="job_end" id="job_end"
                           value="<?php  if ($flag == "edit"){
                               echo date("m/d/Y",$exp_data['job_end']);} ?>"></div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_JOB_DESCRIPTION ?>:</label></div>
                        <div class="span3 tab-margin"><textarea class="span3 inline-input"
                              name="job_description"
                              id="job_description"><?= $exp_data['job_description'] ?></textarea></div>

                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_WEBSITE_URL ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span3" type="text"
                                                          name="job_url" id="job_url"
                                                          value="<?= $exp_data['job_url']; ?>"></div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin bottom-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_COUNTRY ?>:</label></div>
                    <div class="span3 tab-margin">
                        <div class="ui-select span6">
                        <select class="span3 inline-input" name="job_country" id="job_country">
                            <option value="">Select Country</option>
                            <?php
                            foreach ($get_countries as $key => $countries) {
                                ?>
                                <option <?php if ($countries['country_id'] == $exp_data['job_country']) {
                                    echo "selected";
                                } ?>
                                        value="<?php echo $countries['country_id'] ?>"><?php echo $countries['country'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span3" type="text"
                                                         name="job_city" id="job_city"
                                                         value="<?= $exp_data['job_city']; ?>"></div>
                </div>
                <div class="clearfix"></div>

                <div class="span1 pop-btn field-box actions tab-margin">
                    <div style="display: none" id="big_loader" ><img src="<?=ASSETS_PATH?>img/big_loader.gif" width="50px" /></div>
                    <input type="hidden" name="exp_id" id="exp_id"
                           value="<?= $exp_data['exp_id'] ?>"/>
                    <input type="button" class="btn-glow pull-right primary" onclick="AddExperience()"
                           value="<?= LABEL_SAVE_CHANGES ?>">
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        <span id="message"></span>
    </div>




