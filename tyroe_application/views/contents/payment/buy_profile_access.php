<div class="container-fluid payment_block">
    <?php
    if ($is_reviewer == 1) {
        ?>
        <div id="in_pricing">
            <div class="head">
                <h6>Please contact your Studio Administrator to get full access to Tyroe Profiles</h6>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div id="in_pricing">
            <div class="head">
                <h3>Unlock Talent Search for 30 days</h3>
                <h6>Find better talent, faster</h6>
            </div>
            <div class="row charts_wrapp">
                <!-- Plan Box -->
                <div class="plan-box pro">
                    <div class="plan">
                        <div class="wrapper">
                            <img class="ribbon" src="<?= ASSETS_PATH ?>img/badge.png">
                            <h3>Awesome</h3>
                            <div class="price">
                                <span class="dollar">$</span>
                                <span class="qty">180</span>
                                <span class="month">/ 30 day access</span>
                            </div>
                            <div class="features">
                                <p>
                                    <strong>Unlimited</strong>
                                    searches
                                </p>
                                <p>
                                    <strong>Full Access</strong>
                                    For Entire Team
                                </p>
                                <p>
                                    <strong>Unlimited</strong>
                                    Messaging
                                </p>
                            </div>
                            <a class="order" href="javascript:void(0)" id="order_now">ORDER NOW</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="faq">
            <div class="section_header">
                <h3>Frequently asked questions</h3>
            </div>
            <div>
                <div class="span6 block">
                    <div class="box">
                        <p class="title">Why use Tyroe Search to find talent?</p>
                        <p class="answ">
                            Glad you asked! Seriously though, if you urgently need to find local rockstar talent, this is perfect for you and your entire team.  Our database is full of amazing junior artists that have already been vetted and sorted for you.</p>
                    </div>
                    <div class="box">
                        <p class="title">Can I message talent when I find them?</p>
                        <p class="answ">
                            Absolutely. This is the beauty of the Tyroe Talent Search. Once unlocked, you will have access to all our members pixel-perfect profiles and be able to contact them directly. What are you waiting for, start a conversation.</p>
                    </div>
                </div>

                <div class="span6 block">
                    <div class="box">
                        <p class="title">What will I be able to view?</p>
                        <p class="answ">
                            Absolutely everything about the Tyroe. Unlocking the search allows you to view their complete profiles, up to date availability, and full contact details.</p>
                    </div>
                    <div class="box">
                        <p class="title">What happens when access finishes?</p>
                        <p class="answ">
                            All your favourited searches and talent will be waiting for you when you reactivate. The best part is that all messaging is handled through your own mail system so you can keep conversations alive and kicking.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <form action="<?= LIVE_SITE_URL ?>payment/profile_access" method="post" id="payment_form">
            <div class="payment-section" id="payment_section">
                <div class="span6 block">
                    <div class="header">
                        <h3>Credit Card Details</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box form-block">
                        <div class="form-row">
                            <input class="input-control cc_number" type="text" id="cc_number" onkeypress="return isNumberKey(event);" placeholder="Credit Card Number">
                            <img src="<?= ASSETS_PATH ?>img/creditcards/visa.png" id="cc_type_img"/>
                        </div>
                        <div class="form-row">
                            <input class="input-control c_month" type="text" id="c_month" onkeypress="return isNumberKey(event);" placeholder="MM" autocomplete="off">
                            <span class="sep">&nbsp;/&nbsp;</span>
                            <input class="input-control c_year" type="text" id="c_year" onkeypress="return isNumberKey(event);" placeholder="YYYY" autocomplete="off">
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-row">
                            <input class="input-control security_code" type="password" id="security_code" autocomplete="off" onkeypress="return isNumberKey(event);" placeholder="Security Code">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="vertical-sep"></div>
                <div class="span6 block">
                    <div class="header">
                        <h3>Billing Address</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="box form-block">
                        <div class="form-row">
                            <input class="input-control address" type="text" name="address" id="address" placeholder="Address">
                        </div>
                        <div class="form-row">
                            <select class="select-country" id="select_country" name="select_country">
                                <option value="">Country</option>
                                <?php
                                foreach ($countries as $val) {
                                    ?>
                                    <option value="<?= $val['id'] ?>"><?= $val['name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <div class="form-row">
                            <input class="input-control a_city" type="text" name="a_city" id="a_city" placeholder="City">
                            <input class="input-control a_state" type="text" name="a_state" id="a_state" placeholder="State">
                            <input class="input-control a_postcode" type="text" name="a_postcode" id="a_postcode" placeholder="Postcode">
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="alert-message alert alert-warning" id="service_tax_warning" style="display: none;padding: 10px;"><i class="icon-warning-sign"></i>GST (10%) applicable for studios located in Australia</div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="hor-sep"></div>
                <div class="clearfix"></div>
                <div class="amount-section">
                    <div class="span6">
                        <div class="form-block">
                            <div class="form-row">
                                <input class="input-control coupon_code" type="text" name="coupon_code" id="coupon_code" placeholder="Coupon Code">
                                <a href="javascript:void(0)" class="btn btn-apply" id="btn_apply">APPLY</a>
                                <div class="clearfix"></div>
                                <div class="alert alert-error" id="coupon_error_div" style="display: none;">
                                    <i class="icon-remove-sign"></i>
                                    <span id="coupon_error">10% service tax applicable for Studios located in Australia</span>
                                </div>
                                <div class="alert alert-success" id="coupon_success_div" style="display: none;">
                                    <i class="icon-ok-sign"></i>
                                    <span id="coupon_success">GST (10%) applicable for studios located in Australia</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="span6">
                        <div class="amount-details alert-info">
<!--                            <span class="amount">$180</span><span class="sub-text"> 1 x Awesome package</span>
                            <div class="clearfix"></div>
                            <div class="extra-condition" id="extra_condition" style="margin-bottom: 10px;">
                                Service Tax: <span id="service_tax_amt"></span>
                            </div>-->
                            <div class="label-row" id="pkg_text"><a id="select_package" href="#in_pricing" style="font-weight: normal">Select Package</a> : </div><div class="label-amount">$<span id="pkg_amount">0</span></div>
                            <div class="clearfix"></div>    
                            <div class="label-row discount_div" style="display: none;">Coupon Discount :  </div><div class="label-amount discount_div" style="display: none;">$<span id="discount_amount">0</span></div>
                            <div class="clearfix"></div>
                            <div id="gst-block" style="display: none;">
                                <div class="label-row">GST(10%) :  </div><div class="label-amount" >$<span id="tax_amount">0</span></div>
                                <div class="clearfix"></div>
                            </div>    
                            <div class="hor-sep"></div>
                            <div class="label-row total-label">Total :  </div><div class="label-amount total-amount">$<span id="total_amount">0</span></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="bottom-section">
                <a href="javascript:void(0)" class="btn btn-flat success btn-pay-now" id="btn_pay_now">PAY NOW</a>
                <a href="javascript:void(0)" class="btn btn-flat white btn-back" id="btn_back">BACK</a>
                <div class="clearfix"></div>
            </div>
            <div class="hidden">
                <input type="text" name="amt" id="amt" value="180"/>
                <input type="text" name="tax" id="tax" value="0"/>
                <input type="text" name="discount" id="discount" value="0"/>
                <input type="text" name="discount_type" id="discount_type" value="0"/>
                <input type="text" name="coupon_id" id="coupon_id" value=""/>
            </div>
        </form>


        <div class="hidden">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_payment" id="open_modal">
            </button>
        </div>

        <div class="modal fade" id="myModal_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;background-color: white">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Error</h4>
                    </div>
                    <div class="modal-body" id="modal_error">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    var assest_path = "<?= ASSETS_PATH ?>";
    var pub_key = "<?= STRIPE_PUBLIC_KEY ?>";
    var card_no = '';
    var package_selected = false;
$(document).ready(function () {
    
    $('#order_now').click(function () {
        package_selected = true;
        $('#btn_pay_now').removeClass('inactive');
        $('#cc_number').focus();
        $('#pkg_text').html('Awesome Package :  ');
        $('#amt').val(180.00);
        cal_total_amt();
    });

    $('#select_country').change(function () {
        var id = $(this).val();
        if (id == 2) {
            $('#tax').val(10);
            $("#service_tax_warning").fadeIn();
             $('#gst-block').show();
            cal_total_amt();
        } else {
            $('#gst-block').hide();
            $('#tax').val(0);
            cal_total_amt();
            $("#service_tax_warning").fadeOut();
        }
    });
    
    $(".cc_number").keyup(function(){
        //var cc_len = ($(this).val().length);
        if(true){
            var type = Stripe.card.cardType($(this).val());
            switch(type){
                case "Visa":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/visa.png');
                    $("#cc_type_img").show();
                    break;
                case "MasterCard":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/mastercard.png');
                    $("#cc_type_img").show();
                    break;
                case "American Express":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/amex.png');
                    $("#cc_type_img").show();
                    break;
                case "Discover":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/discover.png');
                    $("#cc_type_img").show();
                    break;
                case "Diners Club":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/diners.png');
                    $("#cc_type_img").show();
                    break;  
                case "JCB":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/jcb.png');
                    $("#cc_type_img").show();
                    break;
                case "Maestro":
                    $("#cc_type_img").attr('src',assest_path+'img/creditcards/maestro.png');
                    $("#cc_type_img").show();
                    break;
                default:
                        $("#cc_type_img").hide();
            }
        }
    });

    $('#btn_pay_now').click(function () {
        
        if(package_selected === false){
            alert('Please select package');
            return;
        }
        
        $('#cc_number').css({border: ''});
        $('#c_month').css({border: ''});
        $('#c_year').css({border: ''});
        $('#security_code').css({border: ''});
        $('#address').css({border: ''});
        $('#select_country').css({border: ''});
        $('#a_city').css({border: ''});
        $('#a_state').css({border: ''});
        $('#a_potscode').css({border: ''});

        var cc_number = $('#cc_number').val();
        var c_month = $('#c_month').val();
        var c_year = $('#c_year').val();
        var security_code = $('#security_code').val();
        var address = $('#address').val();
        var country = $('#select_country').val();
        var city = $('#a_city').val();
        var state = $('#a_state').val();
        var potscode = $('#a_postcode').val();

        if ($.trim(cc_number) == "") {
            $('#cc_number').css({
                border: '1px solid red'
            });
            $('#cc_number').focus();
            return false;
        }

        if ($.trim(c_month) == "") {
            $('#c_month').css({
                border: '1px solid red'
            });
            $('#c_month').focus();
            return false;
        }

        if ($.trim(c_year) == "") {
            $('#c_year').css({
                border: '1px solid red'
            });
            $('#c_year').focus();
            return false;
        }

        if ($.trim(security_code) == "") {
            $('#security_code').css({
                border: '1px solid red'
            });
            $('#security_code').focus();
            return false;
        }

        if ($.trim(address) == "") {
            $('#address').css({
                border: '1px solid red'
            });
            $('#address').focus();
            return false;
        }

        if ($.trim(country) == "") {
            $('#select_country').css({
                border: '1px solid red'
            });
            $('#select_country').focus();
            return false;
        }

        if ($.trim(city) == "") {
            $('#a_city').css({
                border: '1px solid red'
            });
            $('#a_city').focus();
            return false;
        }

        if ($.trim(state) == "") {
            $('#a_state').css({
                border: '1px solid red'
            });
            $('#a_state').focus();
            return false;
        }

        if ($.trim(potscode) == "") {
            $('#a_postcode').css({
                border: '1px solid red'
            });
            $('#a_postcode').focus();
            return false;
        }

        $('#btn_pay_now').attr("disabled", "disabled");

        var ccNum = $('.cc_number').val(), cvcNum = $('.security_code').val(), expMonth = $('.c_month').val(), expYear = $('.c_year').val();
        var error = false;
        if (!Stripe.validateCardNumber(ccNum)) {
            error = true;
            reportError('The credit card number appears to be invalid.');
            return;
        }

        // Validate the CVC:
        if (!Stripe.validateCVC(cvcNum)) {
            error = true;
            reportError('The CVC number appears to be invalid.');
            return;
        }

        // Validate the expiration:
        if (!Stripe.validateExpiry(expMonth, expYear)) {
            error = true;
            reportError('The expiration date appears to be invalid.');
            return;
        }        
        
        Stripe.setPublishableKey(pub_key);
        card_no = ccNum;
        $('.loader-save-holder').show();
        if (!error) {
			
			// Get the Stripe token:
			Stripe.createToken({
				number: ccNum,
				cvc: cvcNum,
				exp_month: expMonth,
				exp_year: expYear
                                
			}, stripeResponseHandler);

		}
    });
    
    $("#btn_apply").click(function(){
        if(package_selected === false){
            alert('Please select package');
            return;
        }
        $("#coupon_error_div").hide();
        $("#coupon_success_div").hide();
        $('.loader-save-holder').show();
        if($.trim($("#coupon_code").val()) == ''){
            return;
        }
        $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("payment/get_coupons_details")?>',
            dataType: "json",
            data: {coupon_code: $.trim($("#coupon_code").val())},
            success: function (data) {
                $('.loader-save-holder').hide();
                if(data.status === 'ERROR'){
                    $("#coupon_error").html(data.data);
                    $("#coupon_error_div").show();
                    $("#discount").val('0');
                    $("#discount_type").val('0');
                    $("#coupon_id").val('');
                    $(".discount_div").hide();
                    cal_total_amt();
                }else{
                    var type = parseInt(data.data.type);
                    var discount = parseInt(data.data.discount);
                    if(type == 1){
                        discount = discount/100;
                    }
                    $("#discount").val(discount);
                    $("#coupon_id").val(data.data.coupon_id);
                    $("#discount_type").val(type);
                    $("#coupon_success").html("Congratulations! Your coupon has been applied.");
                    $("#coupon_success_div").show();
                    $(".discount_div").show();
                    cal_total_amt();
                }
            }
        });
    });
});

function cal_total_amt(){
    
    $('#pkg_amount').text(parseFloat($("#amt").val()).toFixed(2));
    
    var orig_amt = parseInt($("#amt").val());
    var tax = parseInt($("#tax").val());
    var discount = parseInt($("#discount").val());
    var discount_type = parseInt($("#discount_type").val());

    var discount_amt = 0;
    var tax_amt = 0;
    if(discount_type === 1){
        discount_amt = discount;
        orig_amt = orig_amt-discount;
    }else if(discount_type === 2){
        discount_amt = (discount*orig_amt)/100;
        discount = (100 - discount)/100;
        orig_amt = discount*orig_amt;
    }
    
    if(tax != 0){
        tax_amt = ((tax*orig_amt)/100);
        orig_amt = orig_amt + tax_amt;
    }
    
    $(".amount").text("$"+parseFloat(orig_amt).toFixed(2));
    $('#discount_amount').text(parseFloat(discount_amt).toFixed(2));
    $('#tax_amount').text(parseFloat(tax_amt).toFixed(2));
    $('#total_amount').text(parseFloat(orig_amt).toFixed(2));
}

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function reportError(msg) {
    $("#modal_error").html(msg);
    $("#open_modal").click();
    $('#btn_pay_now').removeAttr("disabled");
}

function stripeResponseHandler(status, response) {
    // Check for an error:
    if (response.error) {
        $('.loader-save-holder').hide();
        reportError(response.error.message);

    } else { // No errors, submit the form:

        var f = $("#payment_form");

        // Token contains id, last4, and card type:
        var token = response['id'];

        // Insert the token into the form so it gets submitted to the server
        f.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
        f.append("<input type='hidden' name='number_c' value='" + card_no + "' />");

        // Submit the form:
        f.get(0).submit();

    }

}
</script>
