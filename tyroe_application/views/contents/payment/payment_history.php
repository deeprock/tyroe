<div style="display: none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="content">
    <div class="payment-history-container">
        <?php if(count($transactions) == 0){ ?>
        <div class="alert-message alert alert-warning"><i class="icon-warning-sign"></i>No Payment History Found</div>
        <?php } else { ?>
        <div class="header-label">
            Payment History
        </div>
        
        <ul class="payment-history-list">
            <?php foreach($transactions as $transaction) { ?>
            <li class="payment-history-row">
                <div class="icon"><i class="fa fa-credit-card"></i></div>
                <div class="title">
                     <span class="text-label">Payment For:</span> <?php echo $transaction['payment_for']; ?>
                     <div class="clearfix"></div>
                     <span class="text-label">Package:</span> <?php echo $transaction['package_name']; ?> <br/>
                     <div class="clearfix"></div>
                     <span class="text-label">Payment Date:</span> <?php
                        $epoch = $transaction['transaction_time'];
                        $dt = new DateTime("@$epoch"); 
                        echo $dt->format('m-d-Y H:i:s')." (GMT)";
                     ?>
                </div>
                <div class="coupon"><?php if($transaction['coupon_id'] != '') { ?><span class="text-label">Coupon:</span> <?= $transaction['coupon_id'] ?><?php } ?></div>
                <div class="total-amount"><span class="text-label">Total Amount:</span> $<?= $transaction['amt'] ?></div>
                <a href="javascript:void(0)" class="btn-invoice" data="<?= $transaction['transaction_id'] ?>"><i class="fa fa-print"></i>&nbsp;&nbsp;INVOICE</a>
                <div class="clearfix"></div>
            </li>
            <?php } ?>
        </ul>
        <div class="clearfix"></div>
        <div class="hidden">
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_payment" id="open_modal">
            </button>
            
            <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#invoice_modal" id="open_invoice">
            </button>
        </div>

        <div class="modal fade" id="myModal_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Error</h4>
                    </div>
                    <div class="modal-body" id="modal_error">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <div class="clearfix"></div>
    </div>
</div>
<script>
    $(document).ready(function(){
        var printWindow;
        $(".btn-invoice").click(function(){
            $('.loader-save-holder').show();
            $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("payment/get_transaction_details")?>',
            data: {transaction_id: $.trim($(this).attr('data'))},
            success: function (data) {
                $('.loader-save-holder').hide();
                if(typeof data.status !== 'undefined' && data.status === 'ERROR'){
                    $("#coupon_error").html(data.data);
                    $("#coupon_error_div").show();
                }else{
                    $("#invoice_block").html(data);
                    printWindow = window.open('', '', 'height='+$( window ).height()+',width='+$( window ).width());
                    printWindow.document.write('<html><head><title>Invoice</title><style type="text/css" media="print">@page { size: landscape; }</style>');
                    printWindow.document.write('</head><body>');
                    printWindow.document.write(data);
                    printWindow.document.write('</body></html>');
                    printWindow.document.close();
                    setTimeout(function(){
                        printWindow.print();
                    },3000);
                    
                }
            }
            });
        });
    });

</script>

