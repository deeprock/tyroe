<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script src="<?= ASSETS_PATH ?>js/jquery.validation.js" type="text/javascript"></script>
<script type="text/javascript">
    function AddEducation() {
        var edu_organization = $("#edu_organization").val();
        var edu_url = $("#edu_url").val();
        var edu_position = $("#edu_position").val();
        var edu_country = $("#edu_country").val();
        var edu_city = $("#edu_city").val();
        var start_year = $("#start_year").val();
        var end_year = $("#end_year").val();
        var education_description = $("#education_description").val();
        var edu_id = $("#edu_id").val();

        if (edu_organization == '') {
            $('#edu_organization').css({ border: '1px solid red'});
            $('#edu_organization').focus();
            return false;
        } else {
            $('#edu_organization').removeAttr("style")
        }
        if (edu_position == '') {
            $('#edu_position').css({ border: '1px solid red'});
            $('#edu_position').focus();
            return false;
        } else {
            $('#edu_position').removeAttr("style")
        }
        if (edu_country == '') {
            $('#edu_country').css({ border: '1px solid red'});
            $('#edu_country').focus();
            return false;
        } else {
            $('#edu_country').removeAttr("style")
        }
        if (start_year == '') {
            $('#start_year').css({ border: '1px solid red'});
            $('#start_year').focus();
            return false;
        } else {
            $('#start_year').removeAttr("style")
        }
        if (end_year == '') {
            $('#end_year').css({ border: '1px solid red'});
            $('#end_year').focus();
            return false;
        } else {
            $('#end_year').removeAttr("style")
        }


        var data = "edu_organization=" + edu_organization + "&edu_url=" + edu_url + "&edu_position=" + edu_position
                + "&edu_country=" + edu_country + "&edu_city=" + edu_city + "&start_year=" + start_year
                + "&end_year=" + end_year + "&education_description=" + education_description + "&edu_id=" + edu_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("resume/save_education")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".field-box").hide();
                    window.location = "<?=$vObj->getURL("resume")?>";
                }
                else {
                    $("#error_msg").html("Fail to save experience");
                }
            },
            failure: function (errMsg) {
            }
        });
    }

    $("#start_year").datepicker({dateFormat: 'dd-mm-yy' });
    $("#end_year").datepicker();
</script>

    <div class="span5 tab-margin">
        <div class="container-fluid">
            <div class="span4 default-header tab-margin headerdiv">
                <h4>
                    <?php
                    if ($flag == "edit") {
                        echo LABEL_EDIT_EDUCATION;
                    } else {
                        echo LABEL_ADD_EDUCATION;
                    }
                    ?>
                </h4>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1"
                  enctype="multipart/form-data" id="profile_form" name="profile_form"
                  method="post">

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_ORGANIZATION ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text"
                           name="edu_organization" id="edu_organization"
                           value="<?php echo $education_data['edu_organization']; ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_WEBSITE_URL ."(for linking)" ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text"
                           name="edu_url" id="edu_url"
                           value="<?php echo $education_data['edu_url']; ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_POSITION ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text" name="edu_position" id="edu_position"
                           value="<?php echo $education_data['edu_position']; ?>"></div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin bottom-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_COUNTRY ?>:</label></div>
                    <div class="span3 tab-margin">
                        <div class="ui-select span6">
                            <select class="span3 inline-input" name="edu_country" id="edu_country">
                                <option value="">Select Country</option>
                                <?php
                                foreach ($get_countries as $key => $countries) {
                                    ?>
                                    <option <?php if ($countries['country_id'] == $education_data['edu_country']) {
                                        echo "selected";
                                    } ?>
                                            value="<?php echo $countries['country_id'] ?>"><?php echo $countries['country'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span3" type="text"
                                                         name="edu_city" id="edu_city"
                                                         value="<?= $education_data['edu_city']; ?>"></div>
                </div>
                <div class="clearfix"></div>

                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_START_DATE ?>:</label></div>
                        <div class="span3 bottom-margin tab-margin"><input class="span2" type="datetime" name="start_year" id="start_year"
                                                                           value="<?php
                                                                           if ($flag == "edit"){
                                                                               echo date("m/d/Y",$education_data['start_year']);} ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"> <label><?= LABEL_END_DATE ?>:</label></div>
                        <div class="span3 bottom-margin tab-margin"><input class="span2 inline-input" type="text"
                                                                           name="end_year" id="end_year"
                                                                           value="<?php  if ($flag == "edit"){
                                                                               echo date("m/d/Y",$education_data['end_year']);} ?>"></div>

                </div>

                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_DETAILS ?>:</label></div>
                        <div class="span3 tab-margin"> <textarea class="span3 inline-input"
                              name="education_description"
                              id="education_description"><?= $education_data['education_description']; ?></textarea></div>

                </div>
                <div class="clearfix"></div>

                <div class="span1 pull-right field-box actions tab-margin">
                    <div style="display: none" id="big_loader" ><img src="<?=ASSETS_PATH?>img/big_loader.gif" width="50px" /></div>
                    <input type="hidden" value="<?= $education_data['education_id']; ?>"
                           name="edu_id" id="edu_id"/>
                    <input type="button" class="btn-glow pop-btn primary" onclick="AddEducation();"
                           value="<?= LABEL_SAVE_CHANGES ?>">
                    <!--<span>OR</span>
                    <input type="reset" value="Cancel" class="reset"
                           onclick="window.history.back()">-->
                </div>
            </form>
        </div>
        <span id="message" class="success_message"></span>
    </div>

