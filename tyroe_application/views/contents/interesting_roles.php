<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="content">

    <div class="container-fluid">


        <div id="pad-wrapper" id="new-user">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->

                    <!--Default-->
                    <div class="span8 sidebar view-resume">
                        <div class="right-column">

                            <div class="span4 default-header">
                                <h4><?= LABEL_INTERESTING_OPENINGS ?></h4>
                            </div>

                            <div class="row-fluid header element">

                                <div class="row-fluid ">
                                    <div class="panel">
                                        <?php
                                        if ($this->session->userdata("role_id") == TYROE_FREE_ROLE) {
                                            ?>
                                            <div class="row-fluid">
                                                <div class="span12 people" id="top">
                                                    <div class="row-fluid">
                                                        <p>This feature is only available to Tyore Pro account
                                                            holders.It allows you to browse hob openings and submit
                                                            expressions of interest. This will put your profile in front
                                                            of the review team at the studio who has listed the opening
                                                            and help you get noticed!
                                                        </p>

                                                        <p><a class="btn-glow primary"
                                                              href="<?= $vObj->getURL('account/subscription') ?>">Upgrade
                                                                to Pro Now!</a></p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        <?php
                                        } else {
                                            ?>
                                            <!--put pro data here -->
                                        <?php
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <!--Default-->


            </div>
        </div>
    </div>
</div>
<!-- end main container -->
