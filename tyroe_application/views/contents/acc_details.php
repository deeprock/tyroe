<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//if ($this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '4')
//{
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    /*function recalc(){

                   $('input:checkbox').click(function(){
                       if($(this).is(':checked')){
                           $('input:checkbox').not(this).prop('checked', false);
                       }
                   });


    }*/
    $(document).ready(function () {
        $('#myModal').modal('hide');
        $('.btn-del-acc').click(function () {
            $('#delete-studio-error-message').hide();
            if($('#role_id').val() == 3){
                var delete_id = $('#user_id').val();
                $('.loader-save-holder').show();
                $.ajax({
                    type: 'POST',
                    data: {
                        delete_id : delete_id
                    },
                    url: '<?=$vObj->getURL("studios/get_studio_team_member");?>',
                    dataType: 'json',
                    success: function (responseData) {
                        if(responseData.reviewer_count > 0){
                            $('#team_member_count').html(' '+responseData.reviewer_count+' team member(s) ');
                            $('#model_confirmation_note').show();
                        }else{
                            $('#model_confirmation_note').hide();
                        }
                        $('.loader-save-holder').hide();
                        $('#myModal_delete_my_account_1').modal('show');
                    }
                });
            
            }else{
                $('#myModal_delete_my_account_1').modal('show');
            }
            
            //rescale();
        });
        
        
        //DELETE ACCOUNT
        $("body").delegate('#btn_yes_delete', 'click', function (e) {
                $('#myModal_delete_my_account').modal('hide');
                $('#myModal_delete_my_account_1').modal('show');
        });
        
        $("body").delegate(".btn-cancel-delete-studio",'click',function(){
            $('#delete_confirm_text').val('');
        });
        
        $("body").delegate('#btn_delete_studio', 'click', function (e) {
            $('#delete-studio-error-message').hide();
            if($.trim($('#delete_confirm_text').val()) == ''){
                $('#delete-studio-error-message').html("Please enter DELETE text").show();
                return false;
            }

            if($.trim($('#delete_confirm_text').val()) != 'DELETE'){
                $('#delete-studio-error-message').html("Please enter correct text").show();
                return false;
            }
                
            $('#myModal_delete_my_account_1').modal('hide');
            e.preventDefault();
            
            if($('#role_id').val() == 3 || $('#role_id').val() == 4){
                $('.loader-save-holder').show();
                var url = '<?=$vObj->getURL("account/delete_studio_account")?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "json",
                    success: function (data) {
                        if (data.success) {
                            alert("Account delete successfully");
                            window.location.href = '<?=$vObj->getURL("logout")?>';
                        } else {
                            alert("Fail to delete account");
                        }
                        $('.loader-save-holder').hide();
                        $('#delete_confirm_text').val('');
                    }
                });
            
            }else{
                $('.loader-save-holder').show();
                var url = '<?=$vObj->getURL("account/delete_account")?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    dataType: "json",
                    success: function (data) {
                        if (data.success) {
                            alert("Account delete successfully");
                            window.location.href = '<?=$vObj->getURL("logout")?>';
                        } else {
                            alert("Fail to delete account");
                        }
                        $('.loader-save-holder').hide();
                    }
                });
            }
        });

        //Suggestion meesage Sending
        $('.btn-del-sugg').click(function () {
            var sugg = $(".leaving-message").val();
            if (sugg == "" || sugg == undefined) {
                $('.leaving-message').css({
                    border:'1px solid red'
                });
                $('.leaving-message').focus();
                return false;
            } else {
                $(".suggestion_loader").css('display','');
                $('.leaving-message').css({
                    border:''
                });
                var data = "sugg=" + sugg;
                $.ajax({
                    type:'POST',
                    data:data,
                    url:'<?=$vObj->getURL("account/delete_account_suggestion");?>',
                    dataType:'json',
                    success:function (responseData) {
                        if (responseData.success) {
                            $(".suggestion_loader").css('display','none');
                            $(".sugg-msg").html('<div class="alert alert-success"><i class="icon-ok-sign"></i> Thank you for suggestion.</div>')
                            $(".sugg-msg").css('display','');
                            $(".prof-dtl").find('.field-box').css('display','none');
                            $('.modal-body').animate({
                                height : 70
                            }, "normal");
                        } else {
                            $(".suggestion_loader").css('display','none');
                            $(".sugg-msg").html('<div class="alert alert-danger"><i class="icon-remove-sign"></i> Please try again.</div>')
                            $(".sugg-msg").css('display','');
                        }

                    }

                });
            }
        });


        $('.btn-delete-account').click(function () {
            $('.delete_acc_loader').css('display','');
            var data = true;
            $.ajax({
                type:'POST',
                data:data,
                url:'<?=$vObj->getURL("account/delete_account");?>',
                dataType:'json',
                success:function (responseData) {
                    var url = '<?=$vObj->getURL("logout")?>';
                    $(location).attr('href',url);
                }
            });
        });

    });
    function rescale() {
        var size = {width:$(window).width(), height:$(window).height() }
        var offset = 20;
        var offsetBody = 150;
        //$('#myModal').css('height', size.height - offset );
        //$('.modal-body').css('height', size.height - (offset + offsetBody));
        $('#myModal').css('top', '3%');
    }
    $(window).bind("resize", rescale);
    /*$(function () {
        $(".select2").select2();
    });*/
</script>
<!-- USERNAME VALIDATION -->
<script type="text/javascript">
    jQuery(function () {
        /*jQuery("#username").blur(function () {
           check_username();
        })*/
    });
    function check_username(is_return) {
        var return_to = 5;
        var user_name = jQuery("#username").val();
        if (user_name != '') {
            jQuery("#loader").css("display", "");
            jQuery("#save_loader").css("display", "");
            jQuery("#user_result").css("display", "none");
            var data = "username=" + user_name;
            jQuery.ajax({
                type:'POST',
                data:data,
                url:'<?=$vObj->getURL("profile/check_username");?>',
                dataType:'json',
                success:function (data) {
                    jQuery("#loader").css("display", "");
                    if (data.result == 1) {
                        jQuery("#loader").css("display", "none");
                        jQuery("#user_result").attr("src", "<?=ASSETS_PATH?>img/not_ok.png");
                        jQuery("#username").val("");
                        return_to = 0;
                        if (typeof is_return !== 'undefined') {
                            window.return_to = 0;
                        }
                    } else {
                        jQuery("#loader").css("display", "none");
                        jQuery("#user_result").attr("src", "<?=ASSETS_PATH?>img/yes_check.png");
                        var first_name = $("#first_name").val();
                        var last_name = $("#last_name").val();
                        var email = $("#email").val();
                        var username = $("#username").val();
                        var cp = $("#new_confirm_pw").val();
                        if ( (username == "" || username == undefined) && (data.role_id == 2) ) {
                            $('#username').css({
                                border:'1px solid red'
                            });
                            $('#username').focus();
                            return false;
                        } else {
                            $('#username').css({
                                border:''
                            });

                            var data = "first_name=" + first_name + "&last_name=" + last_name + "&email=" + email + "&username=" + username + "&cp=" + cp;
                            $.ajax({
                                type:"POST",
                                data:data,
                                url:'<?=$vObj->getURL("account/save_account_details")?>',
                                dataType:"json",
                                success:function (data) {
                                    if (data.success) {
                                        $(".msg-box").html('<div class="alert alert-success"><i class="icon-ok-sign"></i>' + data.success_message + '</div>');
                                        if (data.firstname == '' && data.lastname == '') {
                                            $("#fn_ln").html(data.user_name);
                                        } else {
                                            $("#fn_ln").html(data.firstname + " " + data.lastname);
                                        }
                                        setTimeout(function () {
                                            //$(".alert").hide();
                                        }, 2000);
                                        $("#save_loader").css("display", "none");
                                    }
                                    else {
                                        $(".msg-box").html('<div class="alert alert-danger"><i class="icon-remove-sign"></i>' + data.success_message + '</div>');
                                        $("#user_result").attr("src", "<?=ASSETS_PATH?>img/not_ok.png");
                                    }
                                }
                            });
                        }
                        if (typeof is_return !== 'undefined') {
                            window.return_to = 1;
                        }

                    }
                    jQuery("#user_result").css("display", "");
                }
            });
        }

    }
</script>
<!-- USERNAME VALIDATION -->
<script type="text/javascript">
    jQuery(function () {
        // Switch slide buttons
        $('.slider-button').click(function () {
            if ($(this).hasClass("on")) {
                $(this).closest('div').removeClass('success');
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
                $(this).closest('div').addClass('success');
            }
        });
    });


    function change_details() {
        check_username('return');
        var setIntVAl = null;
        setIntVAl = setInterval(function () {
            if (typeof return_to !== 'undefined') {
                if (return_to == 0 || return_to == 1) {
                    if (return_to == 1) {
                        var first_name = $("#first_name").val();
                        var last_name = $("#last_name").val();
                        var email = $("#email").val();
                        var username = $("#username").val();
                        var cp = $("#new_confirm_pw").val();

                        if (username == "" || username == undefined) {
                            $('#username').css({
                                border:'1px solid red'
                            });
                            $('#username').focus();
                            return false;
                        } else {
                            $('#username').css({
                                border:''
                            });

                        }
                    } else if (return_to == 0) {
                        //alert('not available');
                    }
                    clearInterval(setIntVAl);
                }
            }
        }, 500);


    }

    function change_details_studio() {
        $("#save_loader").css("display", "");
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email").val();
        var company_name = $("#company_name").val();
        var company_job_title = $("#company_job_title").val();
        var data = "first_name=" + first_name + "&last_name=" + last_name + "&email=" + email + "&company_name=" + company_name + "&company_job_title=" + company_job_title;
        $.ajax({
            type:"POST",
            data:data,
            url:'<?=$vObj->getURL("account/save_account_details")?>',
            dataType:"json",
            success:function (data) {
                if (data.success) {
                    $(".msg-box").html('<div class="alert alert-success"><i class="icon-ok-sign"></i>' + data.success_message + '</div>');
                    $("#fn_ln").html(data.firstname + " " + data.lastname);
                    setTimeout(function () {
                        $(".alert").hide();
                    }, 2000);
                    $("#save_loader").css("display", "none");
                }
                else {
                    $(".msg-box").html('<div class="alert alert-danger"><i class="icon-remove-sign"></i>' + data.success_message + '</div>');

                }
            }
        });
    }
    function update_linked_client(linked_type) {
        $.ajax({
            type:"POST",
            data:"linked_type=" + linked_type,
            url:'<?= $vObj->getURL("account/update_linked_status"); ?>',
            success:function (data) {
                window.location.reload();
            }
        });
    }

    jQuery(document).ready(function(){
        jQuery('#pro_image_avatar_btn-edit').click(function(){
            jQuery("#fileupload").trigger('click');
        });
    })


</script>


<!-- main container -->
<div class="content">

<div class="container-fluid">
<div id="pad-wrapper">
<div><?php if ($profile_updated != "") {
    echo $profile_updated;
}?></div>
<div class="grid-wrapper">
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<?php

$this->load->view('left_coloumn');


?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar personal-info">
<div class="row-fluid form-wrapper">


    <div class="span12">
        <div class="container-fluid padding-adjusted">


            <div class="span4 default-header">
                <h4>Account Details</h4>
            </div>
            <div class="clearfix"></div>
            <div class="msg-box">

            </div>


            <form class="new_user_form inline-input new-c-magic acnt-dtl c-magic" _lpchecked="1"
                  enctype="multipart/form-data" name="profile_form" method="post"
                  action="<?php //= $vObj->getURL('account/save_account_details') ?>"
                  onsubmit="return form_acc_details();">
                
                <?php if ($get_user['role_id'] == 3 || $get_user['role_id'] == 4) {
                    ?>
                    <div class="field-box">
                        <div class="span3"><label><?= LABEL_JOB_TITLE ?></label></div>
                        <div class="span9">
                            <input class="span12" type="text" name="company_job_title"
                                                  id="company_job_title"
                                                  value="<?php echo $get_user['company_job_title']; ?>">
                        </div>
                    </div>
                
                    <div class="field-box">
                        <div class="span3"><label><?= LABEL_LOCATION ?></label></div>
                        <div class="span9">
                            <span class="box-location">
                                <?= $get_user['city'] ?>
                            </span>
                        </div>
                    </div>
                
                    <?php
                    $onclick = "change_details_studio();";
                } else {
                    $onclick = "change_details();";
                }
                ?>
                
                <div class="field-box">
                    <div class="span3"><label><?= LABEL_FIRST_LAST_NAME ?></label></div>

                    <div class="round-input-group pull-left span9">

                        <input class="span6" type="text"
                               name="first_name" id="first_name" placeholder="FirstName"
                               value="<?php echo $get_user['firstname']; ?>">
                        <input class="span6" type="text"
                               name="last_name" id="last_name" placeholder="LastName"
                               value="<?php echo $get_user['lastname']; ?>">
                    </div>
                </div>
                <div class="field-box">
                    <div class="span3"><label><?= LABEL_EMAIL ?></label></div>
                    <div class="span9"><input class="span12" type="text" name="email" id="email"
                                              value="<?php echo $get_user['email']; ?>" readonly=""></div>
                </div>

                <div class="span12 field-box">
                    <div class="span3"><label><?= LABEL_PASSWORD ?></label></div>
                    <div class="span9"><input class="span12" type="password"
                                              name="new_confirm_pw" id="new_confirm_pw"
                                              value="<?php echo $get_user['password_len']; ?>"></div>
                </div>
               <?php if ($get_user['role_id'] == 2 || $get_user['role_id'] == 5) { ?>
                <div class="field-box">
                    <div class="span3"><label><?= LABEL_PUBLIC_PROFILE ?></label></div>
                    <div class="span9 btn-input-group">
                        <span class="btninput-group-addon"><?= str_replace("https://", '', LIVE_SITE_URL) ?></span>
                        <input class="btninput-group-rest span9" type="text"
                               name="username" id="username"
                               value="<?=($get_user['profile_url'] == '') ? $get_user['username'] : $get_user['profile_url']; ?>">
                        <img id="loader" src="<?= ASSETS_PATH ?>img/loader.gif"
                             style="display: none; width:20px"/>
                        <img src="" id="user_result" style="display: none; width:20px"/>
                    </div>
                </div>
                <?php
            }?>
              

                <div class="span4 pull-right actn-btns">
                    <?php if( ($this->session->userdata('role_id') == 3) || ($this->session->userdata('role_id') == 4) ){ ?>
                    <img id="save_loader" src="<?=ASSETS_PATH?>img/loader.gif" style="width: 20px; display: none;">
                    <?php } ?>
                    <input type="button" name="submit" class="btn-flat success pull-right"
                           value="<?= LABEL_SAVE ?>" onclick="<?=$onclick?>">
                    <input type="reset" class="btn-flat white pull-right"
                           value="<?= LABEL_CANCEL_BUTTON ?>" onclick="accountcancel()">
                </div>
            </form>
            <div class="clearfix"></div>
            <div class="separator" style="padding: 0px;box-shadow: 0px 0px #ccc"></div>
        </div>
    </div>
</div>
<div class="container-fluid padding-adjusted">
<!--    <div class="span12">
        <h4><?php echo LABEL_MODULE_SOCIAL ?></h4>
        <span class="t-magic pull-left"><?php echo LABEL_MODULE_SOCIAL_SUB_TEXT ?></span>
    </div>-->

<!--    <div class="span12 c-magic linked-bottom">
        <div class="span10 offset1">
            <ul class="linked-accounts-list">
                <li>
                    <div class="span12">
                        <div class="span2 pull-left social-acnt">
                            <img class="img-circle"
                                 src="<?php echo ASSETS_PATH . "/img/facebook_social.png" ?>">
                        </div>
                        <div class="span8 pull-left social-acnt-title">
                            <h4>Facebook</h4>

                            <p>Announce updates directly from Tyroe</p>
                        </div>
                        <div class="span2 pull-left social-acnt-link">
                            <input class="btn-flat" type="button" value="LINK">

                            <?php  if ($fb_user) {
                            $fb_user_data = unserialize($fb_user['user_data']);
                            $fb_username = $fb_user_data[1]['name'];

                            ?>
                            <div class="slider-frame on success">
                                                        <span class="slider-button on" data-off-text="OFF"
                                                              data-on-text="ON">ON</span>
                            </div>
                            <?php
                        }
                            if (empty($fb_user)) {
                                if ($social_fbdata['status_sl'] == 1) {
                                    ?>
                                    <div class="span2 pull-left social-acnt-link">
                                        <input class="btn-flat fb_btn"
                                               onclick="update_linked_client('fb')"
                                               type="button" value="Unlink">
                                    </div>
                                    <?php } else if ($social_fbdata['status_sl'] == 0) { ?>
                                    <div class="span2 pull-left social-acnt-link">
                                        <input class="btn-flat fb_btn"
                                               onclick="window.open('<?= $vObj->getURL("fbclient/index/$id"); ?>','facebook','width=400,height=400');"
                                               type="button" value="LINK">
                                    </div>
                                    <?php } ?>


                                <?php } ?>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="span12">
                        <div class="span2 pull-left social-acnt">
                            <img class="img-circle"
                                 src="<?php echo ASSETS_PATH . "img/twitter_social.png" ?>">
                        </div>
                        <div class="span8 pull-left social-acnt-title">
                            <h4>Twitter</h4>

                            <p>Announce updates directly from Tyroe</p>
                        </div>
                        <?php if (empty($tw_user)) {
                        if ($social_twdata['status_sl'] == 0) {
                            ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat"
                                       onclick="window.open('<?= $vObj->getURL("twitterclient/index/$id"); ?>','twitter','width=400,height=400,scrollbars=auto');"
                                       type="button" value="LINK">
                            </div>
                            <?php } else { ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat"
                                       onclick="update_linked_client('tw')"
                                       type="button" value="Unlink">
                            </div>
                            <?php } ?>
                        <?php } else if ($tw_user) { ?>
                        <div class="slider-frame on success">
                                    <span class="slider-button on"
                                          data-off-text="OFF"
                                          data-on-text="ON">ON</span>
                        </div>
                        <?php } ?>
                    </div>
                </li>
                <li>
                    <div class="span12">
                        <div class="span2 pull-left social-acnt">
                            <img class="img-circle"
                                 src="<?php echo ASSETS_PATH . "img/google_social.png" ?>">
                        </div>
                        <div class="span8 pull-left social-acnt-title">
                            <h4>Google+</h4>

                            <p>Announce updates directly from Tyroe</p>
                        </div>
                        <?php if (empty($gp_user)) { ?>
                        <?php if ($social_gpdata['status_sl'] == 0) { ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat"
                                       onclick="window.open(' <?= $vObj->getURL("googleclient/index/$id"); ?>','googleplus','width=400,height=400,scrollbars=auto');"
                                       type="button" value="LINK">
                            </div>
                            <?php } else { ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat"
                                       onclick="update_linked_client('gp')"
                                       type="button" value="Unlink">
                            </div>
                            <?php } ?>
                        <?php } else if ($gp_user) { ?>
                        <div class="slider-frame on success">
                                                                    <span class="slider-button on"
                                                                          data-off-text="OFF"
                                                                          data-on-text="ON">ON</span>
                        </div>
                        <?php } ?>
                    </div>
                </li>
                <li>
                    <div class="span12">
                        <div class="span2 pull-left social-acnt">
                            <img class="img-circle"
                                 src="<?php echo ASSETS_PATH . "img/linked_social.png" ?>">
                        </div>
                        <div class="span8 pull-left social-acnt-title">
                            <h4>LinkedIn</h4>

                            <p>Announce updates directly from Tyroe</p>
                        </div>
                        <?php if (empty($in_user)) {
                        if ($social_indata['status_sl'] == 0) {
                            ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat" type="button"
                                       onclick="window.open('<?= $vObj->getURL("linkedinclient/linkedin/$id"); ?>','linkedin','width=400,height=400,scrollbars=auto');"
                                       value="LINK">
                            </div>
                            <?php } else { ?>
                            <div class="span2 pull-left social-acnt-link">
                                <input class="btn-flat"
                                       onclick="update_linked_client('in')"
                                       type="button" value="Unlink">
                            </div>
                            <?php } ?>
                        <?php } else if ($in_user) { ?>
                        <div class="slider-frame on success">
                                <span class="slider-button on"
                                      data-off-text="OFF"
                                      data-on-text="ON">ON</span>
                        </div>
                        <?php } ?>
                    </div>
                </li>
                <div class="clearfix"></div>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>-->

    <input type="button" class="btn-flat white t-magic pull-right btn-del-acc" value="DELETE MY ACCOUNT">


</div>

</div>

<!-- END RIGHT COLUMN -->
<div>


</div>

</div>
</div>
</div>
</div>

<div style="display: none;" class="modal fade" id="myModal" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Sorry to See you go</h4>

            </div>
            <div class="modal-body">
                <div class="center prof-dtl">
                    <div class="tab-margin">
                        <div class="sugg-msg"></div>
                        <input type="hidden" name="callfrom" value="profile_top">

                        <div class="field-box">
                            Before you leave ,please let us know if you have any suggestions that could help us improve
                            our service for others.
                        </div>
                        <div class="field-box">
                            <textarea name="" id="" class="leaving-message"></textarea>
                        </div>
                        <div class="field-box">
                            <button type="button"
                                    class="btn btn-flat white btn-save btn-del-sugg"><?= LABEL_SEND_ANNONYMOUS?>
                            </button>
                            <img class="suggestion_loader" src="<?=ASSETS_PATH?>img/small_loader.gif" style="display: none" width="20px"/>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <img src="<?=ASSETS_PATH?>img/small_loader.gif" width="20px" style="display:none" class="delete_acc_loader"/>
                <button type="button" class="btn btn-flat btn-save btn-delete-account"
                        value="<?= LABEL_DELETE_MY_ACCOUNT ?>">
                    <?= LABEL_DELETE_MY_ACCOUNT ?>
                </button>
            </div>
        </div>
    </div>
</div>
<?php
//}
?>
<input type="hidden" value="<?php echo $get_user['user_id']?>" id="user_id"/>
<input type="hidden" value="<?php echo $get_user['role_id']?>" id="role_id"/>
<div style="display: none;" class="modal myModal_delete_my_account" id="myModal_delete_my_account" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="max-height: 100px;background: #fff">
            <div class="modal-body" style="max-height: 100px;">
                <p class="delete-message-1">Are you sure you want to delete account ?</p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white btn-yes-delete" id="btn_yes_delete">Yes</button>
                <button type="button" class="btn btn-flat white" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal myModal_delete_my_account_1" id="myModal_delete_my_account_1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm Account Deletion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                   <i class="icon-warning-sign"></i>
                   <div class="confirmation-text">
                       <span class="l1">Are you sure ?</span><br/>
                       <span class="l2">All account data will be permanently deleted. This can't be undone.</span><br/>
                       <span class="note" id="model_confirmation_note"><strong>Note:</strong> There are <strong id="team_member_count">4 team member(s)</strong>of Tyroe who will no longer have access to account</span><br/>
                   </div>
                   <div class="clearfix"></div>
                </div>
                <p class="delete-message-2">Type DELETE to confirm</p>
                <p>
                    <textarea class="delete-confirm-text" id="delete_confirm_text"></textarea>
                </p>
                <label class="delete-studio-error-message" id="delete-studio-error-message">Error message</label>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-glow btn-cancel-delete-studio" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-glow primary btn-delete-studio" id="btn_delete_studio">Delete your account</button>
            </div>
        </div>
    </div>
</div>
<!--Dashboard Artist End-->