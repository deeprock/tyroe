<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    function rejectInvitaion(job_id) {
        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("job/RejectInvitation")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".status" + job_id).remove();
                    $(".result" + job_id).css("display", "");
                    $(".result" + job_id).html("Rejeted");
                }
            },
            failure: function (errMsg) {
            }
        });

    }

    function acceptInvitaion(job_id) {
        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("job/AcceptInvitation")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".status" + job_id).remove();
                    $(".result" + job_id).css("display", "");
                    $(".result" + job_id).html("Accepted");
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<div class="content">

    <div class="container-fluid">

        <div id="pad-wrapper">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column">
                            <div class="span10 default-header">
                                <h4><?= LABEL_CANDIDATE_ROLES ?></h4>
                            </div>
                            <div class="filter-result"></div>
                            <!-- START OPENING DETAILS -->
                            <?php
                            if ($get_invite_jobs[0]['job_id'] != '') {

                                for ($tyr = 0; $tyr < count($get_invite_jobs); $tyr++) {
                                    ?>
                                    <div
                                        class="row-fluid no-filter job-opening<?php echo $get_invite_jobs[$tyr]['job_id']; ?>">
                                        <div class="span12 group" id="top">

                                            <!-- TITLE -->
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <h5>

                                                        <?php echo $get_invite_jobs[$tyr]['job_title']; ?>

                                                    </h5>
                                                </div>
                                                <?php if ($get_invite_jobs[$tyr]['invitation_status'] == 0) { ?>
                                                    <div
                                                        class="span4 status<?php echo $get_invite_jobs[$tyr]['job_id']; ?>">
                                                        <ul class="actions pull-right">
                                                            <li><a href="javascript:void(0);"
                                                                   onclick="acceptInvitaion('<?= $get_invite_jobs[$tyr]['job_id'] ?>');">Available</a>
                                                            </li>
                                                            <li><a href="javascript:void(0);"
                                                                   onclick="rejectInvitaion('<?= $get_invite_jobs[$tyr]['job_id'] ?>');">Not
                                                                    Available</a></li>
                                                        </ul>
                                                    </div>
                                                    <div
                                                        class=" span4 span_result result<?= $get_invite_jobs[$tyr]['job_id']; ?>"
                                                        style="display: none"></div>
                                                <?php } else { ?>
                                                    <div class="span4 span_result">
                                                        <?php if ($get_invite_jobs[$tyr]['invitation_status'] == 1) {
                                                            echo "Available";
                                                        } elseif ($get_invite_jobs[$tyr]['invitation_status'] == -1) {
                                                            echo "Not Available";
                                                        } ?>
                                                    </div>
                                                <?php } ?>

                                            </div>

                                            <!-- INFO -->
                                            <div class="row-fluid" id="opening-stats">
                                                <div class="row-fluid">
                                                    <div class="span8">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                <span style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                        Studio:</p><span
                                                                        style="width:290px; float:left;"> <strong><?php echo $get_invite_jobs[$tyr]['username']; ?></strong></span></span>

                                                                <span style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                        Reviewer:</p><span
                                                                        style="width:290px; float:left;"><strong><?php echo $get_invite_jobs[$tyr]['username']; ?></strong></span></span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid" style="margin-top: 20px;">
                                                    <div class="span8">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                        <span
                                                            style="width: 100%; float:left;"><p
                                                                style="width: 120px; float:left;">
                                                                Country:
                                                            </p><span
                                                                style="width:290px; float:left;"><?php echo $get_invite_jobs[$tyr]['job_location']; ?></span></span>




                                                                <span
                                                                    style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                        City:</p><span
                                                                        style="width:290px; float:left;"><?php echo $get_invite_jobs[$tyr]['job_location']; ?>
                                                                       </span></span>



                                                                        <span
                                                                            style="width: 100%; float:left;"><p
                                                                                style="width: 120px; float:left;">
                                                                                Skills:</p><span
                                                                                style="width:290px; float:left;"><?php
                                                                                for ($a = 0; $a < count($get_invite_jobs[$tyr]['skills']); $a++) {
                                                                                    echo $get_invite_jobs[$tyr]['skills'][$a]['creative_skill'] . ", ";

                                                                                }

                                                                                ?>
                                                                            </span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span4">
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                To start:</p><span
                                                                style="width:112px; float:left;"><?php echo date('d M Y', $get_invite_jobs[$tyr]['start_date']); ?></span></span>
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                Duration:</p><span
                                                                style="width:112px; float:left;"><?php echo $get_invite_jobs[$tyr]['username']; ?></span></span>
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                Quantity:</p><span
                                                                style="width:112px; float:left;"><?php echo $get_invite_jobs[$tyr]['job_quantity']; ?></span></span>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            } else {
                                ?>
                                <div class="row-fluid">
                                    <div id="top" class="span12 people">
                                        <span class="name">
                                    <?php
                                     echo "No record";
                                    ?>
                                        </span>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>


                            <!-- END OPENING DETAILS -->

                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->

                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
