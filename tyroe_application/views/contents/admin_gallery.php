<!-- main container -->
<script>
    var image_path = '<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE;?>';
</script>
<style>
#fixed_masonry_layout{
    width:100%;
}
#fixed_masonry_layout .span3{
    margin:10px;
    width:370px;
}
</style>
<link href="<?= ASSETS_PATH ?>css/bar/style.css" rel="stylesheet" />
<link href="<?= ASSETS_PATH ?>css/bar/metro.css" rel="stylesheet" />
<div class="content">
    <?php
    $sel_html  = '<div class="pull-right span2" style="margin:-24px 10px 0px 0px;"> <select class="span2 pagination_limit" title="Select number for per page">';
    $option_val_inc = 10;
    $tyr_gallery_limit = $this->session->userdata('tyr_gallery_limit');
    for($sel=0;$sel<=8;$sel++){
        $option_val_inc =$option_val_inc+10;
        $is_sel = '';
        if($tyr_gallery_limit==$option_val_inc){
            $is_sel = 'selected="selected"';
        }
        $sel_html.='<option '.$is_sel.' value="'.$option_val_inc.'">'.$option_val_inc.'</option>';
    }
    $sel_html  .= '</select></div>';
    echo $sel_html;
    ?>
    <div class="container admin_gallery_images hide">
            <?php echo $gallery_data; ?>
    </div>
</div>
<!-- end main container -->

<script>
    var pagi_href;;
    pagi_href = '<?= base_url('gallery/page/1'); ?>';

    $(document).ready(function (){
        setTimeout(function (){
            $('.tyr_gallery_pagination > li').each(function(ele_index, ele){
                var this_each_ele = $('.tyr_gallery_pagination > li').eq(ele_index).find('a');
                var this_each_text = this_each_ele.text();
                if(this_each_text==1){
                    this_each_ele.trigger('click');
                    return false;
                }
            });
        },400);

        //$('img').on('load',function (){
        <?php /* $(window).on('load',function (){
            var container = document.querySelector('#fixed_masonry_layout');
            var msnry = new Masonry( container, {
                columnWidth: 10,
                itemSelector: '.span3'
            });
        })
         */ ?>
        //

        $(document).delegate('.pagination_limit','change',function (){
            var this_ele = $(this);
            var this_val=this_ele.val();
            var target_first = $('.tyr_gallery_pagination > li').eq(0).find('a');
            target_first.attr('href',pagi_href);
            target_first.trigger('click');

        })

        $(document).delegate('.tyr_gallery_pagination > li > a','click',function (evnt){
            evnt.preventDefault();
            var this_ele = $(this);
            var this_href = this_ele.attr('href');
            if(this_href=='javascript:void(0)' || this_href=='$'){
                return;
            }
            $('.gallery_pagi_loader').show();
            var page_limit = $('.pagination_limit').val();
            var jqxhr = $.ajax( {
                    cache:false,
                    url:this_href,
                    data:({"limit":page_limit}),
                    type:'GET'
                    }).done(function(data) {
                        //history.pushState({}, '', this_href);
                        $('.tyr_gallery_pagination li.hide').remove();

                        $('.admin_gallery_images').removeClass('hide');

                        $('.admin_gallery_images').html(data);
                            $('img').on('load',function (){
                            var container = document.querySelector('#fixed_masonry_layout');
                            var msnry = new Masonry( container, {
                                columnWidth: 10,
                                itemSelector: '.span3'
                            });
                        });
                    })
                    .fail(function() {
                        $('#show_pagination_loading').modal('show');
                        $('.admin_gallery_images').removeClass('hide');
                        var fail_msg = '<p style="color:#f00; font-weight:bold;">Pagination failed try again or something went wrong</p>';
                        $('#show_pagination_loading #myModalLabel').html(fail_msg);


                    })
                    .always(function() {
                        setTimeout(function (){
                            $('.gallery_pagi_loader').hide();
                        },400);

                    });
            return false;
        })

        $(document).delegate('click','a',function (eve){
            var href = $(this).attr('href');
            if(href =='#'){
                eve.preventDefault();
            }
        });

        setTimeout(function (){





        }, 1000);
        <?php /*
        var load_more_media = false;
        var loaded_img_count = null;
        loaded_img_count = $(".admin_gallery_images").find('.span3').length;
             $(window).scroll(function() {
                 if($(window).scrollTop() + window.innerHeight == $(document).height()) {
                     return false;
                     var url = '<?=$vObj->getURL("gallery/get_gallery_data/")?>';
                     if(load_more_media ==true){ return false; }
                     load_more_media = true;
                     $.ajax({
                         type:'GET',
                         url:url,
                         data:({loaded_images:loaded_img_count}),
                         success:function (data) {
                             var last_ele = $('.admin_gallery_images .span3:last');
                             last_ele.after(data);
                             var new_gallery_len  =last_ele.nextAll().length;
                             loaded_img_count = parseInt(loaded_img_count)+parseInt(new_gallery_len);
                             $('img').on('load',function (){
                                 var container = document.querySelector('#fixed_masonry_layout');
                                 var msnry = new Masonry( container, {
                                     columnWidth: 10,
                                     itemSelector: '.span3'
                                 });
                             });

                         },
                         complete:function (){
                             load_more_media = false;
                         }
                     })
             }
             });

        */ ?>
        $("body").delegate('.show_full_gallery_data_in_modal','click', function (e) {
            var this_ele = $(this);
            var ele_type = this_ele.attr('data-type');
            var data_url = this_ele.attr('data-url');

            $('#show_gallery_media_modal').modal('show');
            $('#show_gallery_media_modal .close').css({'height':'20px'});
            var modal_html = null;
            if(ele_type=='video'){
                modal_html = '<iframe width="580" height="400" src="'+data_url+'"></iframe>';
                $('#show_gallery_media_modal').css({"width":"49%","left":"46%"});
            } else {
                $('#show_gallery_media_modal').css({"width":"80%","left":"30%"});

                modal_html = '<img src="'+data_url+'" />';
            }
            $('#show_gallery_media_modal #myModalLabel').text(ele_type.toUpperCase());
            $('#show_gallery_media_modal .modal-body').html(modal_html);
            var modal_width = $('#show_gallery_media_modal').css('width').replace('px','');
            var html_width = $('html').css('width').replace('px','');

        });
    })
</script>

<div class="modal hide" id="show_gallery_media_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="gallery_pagi_loader loader-save-holder">
    <div class="loader-save"></div>
</div>