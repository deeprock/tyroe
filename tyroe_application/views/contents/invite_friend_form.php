<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">

    function InviteFriend() {

        var invite_message = $("#invite_message").val();
        var friend_name = $("#friend_name").val();
        var friend_email = $("#friend_email").val();

        if(friend_name == ''){ $("#friend_name").css({ border: '1px solid red'}); return false; } else {$("#friend_name").css({ border: '0px'}); }
        if(friend_email == ''){ $("#friend_email").css({ border: '1px solid red'}); return false; } else {$("#friend_email").css({ border: '0px'}); }

        if (friend_name != "" && friend_email != "") {
            var data = "invite_message=" + invite_message + "&friend_name=" + friend_name + "&friend_email=" + friend_email;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("account/invite_friend")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".primary").remove();
                        $("#thanks").after('<label>Thanks.</label>');
                        return false;
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                    else {
                        $(".notification-box-message").css("color", "#b81900")
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                },
                failure: function (errMsg) {
                }
            });

        } else {
            $(".notification-box-message").css("color", "#b81900")
            $(".notification-box-message").html("Fields Required");
            $(".notification-box").show(100);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 5000);
        }
    }
</script>
<!-- main container -->
<div class="content">

    <div class="container-fluid">
        <div id="pad-wrapper">
            <div><?php if ($profile_updated != "") {
                    echo $profile_updated;
                };?></div>
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php

                    $this->load->view('left_coloumn');


                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">


                            <div class="span12">
                                <div class="container-fluid">


                                    <div class="span4 default-header">
                                        <h4>Coupons</h4>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Your Current coupons: </b></h5>
                                        <ul class="content-box-data">
                                            <?php
                                            for($c=0; $c<count($coupons); $c++){
                                                if($coupons[$c]['coupon_type'] == 'g'){
                                                    ?>
                                                    <li>$<?=$coupons[$c]['coupon_value']?> A Welcome gift from tyroe.com</li>
                                                    <?php
                                                } else if($coupons[$c]['coupon_status'] == '1'){
                                                    ?>
                                                    <li>$<?=$coupons[$c]['coupon_value']?> accepted invitation.</li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Invite Friends & Collegues to Earn New Coupons: </b></h5>

                                    </div>
                                    <div class="field-box">
                                        <div class="content-box">
                                            <div
                                                class="content-box-data">
                                                    <label><?= LABEL_NAME_TEXT ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="friend_name" id="friend_name"
                                                           value="">
                                                <div class="clearfix"></div>
                                                    <label><?= LABEL_EMAIL ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="friend_email" id="friend_email"
                                                           value="">
                                                <div class="clearfix"></div>
                                                    <label><?= LABEL_FROM_TEXT ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="txt_from" id="txt_from"
                                                           value="<?=$email;?>" readonly="">
                                                <div class="clearfix"></div>
                                                    <label><?= LABEL_MESSAGE_TEXT ?>:</label>
                                                    <textarea class="span9" name="invite_message" id="invite_message"
                                                              placeholder="" rows="15" readonly="">Hi name,&#13;&#13;I've started using this awesome new system called Tyroe.com.&#13;
                                                        &#13;It hooks my portfolio up with some massive studio and I thought it could be a good way for you to find work (or people to wotk with).
                                                        &#13;Check it out and sign up if you like it. It's free.
                                                        &#13;<a href="">Click here to join</a>
                                                        &#13;Cheers
                                                        &#13;<?=$name?>
                                                    </textarea>
                                                <div class="clearfix"></div>
                                                    <label id="thanks"></label>
                                                    <input type="submit" name="submit" class="btn-glow primary"
                                                           value="<?= LABEL_SEND_INVITATION_BUTTON ?>"
                                                           onclick="InviteFriend();">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Pending Coupons: </b></h5>
                                        <ul class="content-box-data">
                                            <?php
                                            for($c=0; $c<count($coupons); $c++){
                                                if($coupons[$c]['coupon_type'] == 'r' && $coupons[$c]['coupon_status'] == '0'){
                                                    $pending_count = 1;
                                                    ?>
                                                    <li>$<?=$coupons[$c]['coupon_value']?> pending invitation.</li>
                                                    <?php
                                                }
                                            }
                                            if($pending_count == 0)
                                            {
                                                echo "No invitation for pending.";
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Used Coupons: </b></h5>
                                        <ul class="content-box-data">
                                            No coupon used.
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <?php
    //}
    ?>
    <!--Dashboard Artist End-->