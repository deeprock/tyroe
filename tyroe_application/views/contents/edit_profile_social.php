<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<div class="social-network-container">
<!--    <section class="social_link_section">
    <?php
    if ($get_social_link == "") {
        ?>
        <h4 class="bolded-head">Connect your Social Network</h4>
        <p>Select networks from dropdown menu and add URL </p>
        <?php
    } else {
        ?>
        <div class="row-fluid">
            <div class="public-social-network-highlight pull-left span12">
                <nav id="social-connected-profile">
                    <ul class="public-social-list">
        <?php
        foreach ($get_social_link as $key => $social) {
            ?>
                                <li>
                                    <a target="_blank" class="<?php /* = $social['social_type']; */ ?>" href="<?= $social['social_link']; ?>">
                                        <i class="font-icon-social-<?= $social['social_type']; ?>"></i>
                                    </a>
                                    <span data-social-id="<?= $social['social_id']; ?>" class="icon-remove-circle btn-remove-social"></span>
                                </li>
            <?php
        }
        ?>
                    </ul>
                </nav>
            </div>
        </div>
        <?php
    }
    ?>
    </section>-->
    <!--<div class="row-fluid">
        <div class="network-url-container">
            <div class="tab-margin span2 user-pro-url-adjust pull-left">
                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-ani"><i id="icn-social" class="font-icon-social-facebook"></i></button>
                    <input type="hidden" name="social_value" id="social_value" value=""/>
                    <button data-toggle="dropdown" class="btn dropdown-toggle network-btn-support btn-anbs">
                        <i class="caret"></i>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="javascript:void(0)" class="social_label" data-value="linkedin">LinkedIn</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="twitter">Twitter</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="facebook">Facebook</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="dribbble">Dribbble</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="github">Github</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="smugmug">SmugMug</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="500px">500px</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="vimeo">Vimeo</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="flickr">Flickr</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="youtube">YouTube</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="pinterest">Pinterest</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="tumblr">Tumblr</a></li>
                        <li><a href="javascript:void(0)" class="social_label" data-value="google-plus">Google+</a></li>
                    </ul>
                </div>
            </div>
            <div class="span10 profile-url-tab default-margin-left">
                <input type="text" class="form-control span12 pull-left" id="social_url" name="social_url"
                       placeholder="Profile URL">
                <button type="button" id="social_url_btn" class="button-main button-large btn-connect btn-theme-anb">CONNECT
                </button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>-->


    <!--<div class="row-fluid">
    <div class="avail-work-container pull-left span12">
    <section class="avail-section">
    <?php /* if ($availability == "") {
     */ ?>
        <span id="chk-avail">
        <h4 class="bolded-head">What is your availability for work? </h4>
        <p>Keep recruiters in the loop with your status! </p>
        </span>
    <?php
    /*
      } else */ if ($availability == "1") {
        ?>
            <div class="span9 chk-av-current">
                <span id="chk-avail" class="green-badge offset5">available for work</span>
            </div>
             <span style="display: table;" class="green-badge-auto">available for work</span>
        
        <?php
    } else if ($availability == "2") {
        ?>
            <div class="span9 chk-av-current">
                <span id="chk-avail" class="green-badge offset5">available soon</span>
            </div>
            <span style="display: table;" class="green-badge-auto">available soon</span>
        <?php
    } else if ($availability == "3") {
        ?>
            <div class="span9 chk-av-current">
                <span id="chk-avail" class="grey-badge offset5">not available for work</span>
            </div>
            <span style="display: table;background-color: #333333;"
                  class="green-badge-auto">not available for work</span>
        <?php
    } else {
        ?>
        
                <h4 class="bolded-head">What is your availability for work? </h4>
                <p>Keep recruiters in the loop with your status! </p>
        
        <?php
    }
    ?>
    </section>
        <div class="clearfix"></div>
    <div id="accordionArea" class="accordion">
    <div class="accordion-group">
    <div class="accordion-heading accordionize">
        <a class="accordion-toggle inactive collapsed edit-avail-ops" href="#acc-4" data-parent="#accordionArea"
           data-toggle="collapse">EDIT AVAILABILITY <i
                class="font-icon-cog pull-right"></i></a>
    </div>
    <div style="height: 0px;" id="acc-4" class="accordion-body collapse">
        <div class="accordion-inner">
            <div class="row-fluid">
                <div class="span12">
                    <div class="availability-options span12">
                        <div data-val="1" class="span12 default-margin-left availability-ops custom-cursor-pointer">
                            <div class="span3">
                                <div class="radio radio-drop pull-right"><span class=""><input type="radio" value="1"
                                                                                               id="optionsRadios2"
                                                                                               name="optionsRadios"></span>
                                </div>
                                <div class="custom-box edit-avl-chk">
                                    <input type="checkbox" id="c1" name="cc" data-chk-val="1" class="chk_avail"/>
                                    <label for="c1"><span></span></label>
                                </div>
                            </div>
                            <div class="span9">
                                <br clear="all">
                                <span class="green-badge offset1">available for work</span>
                            </div>
                        </div>
    
                        <div data-val="2" class="span12 default-margin-left availability-ops custom-cursor-pointer">
                            <div class="span3">
                                <div class="radio radio-drop pull-right" id=""><span class="">
                            <input type="radio" value="0"
                                   id="optionsRadios2"
                                   name="optionsRadios"></span></div>
                                <div class="custom-box edit-avl-chk">
                                    <input type="checkbox" id="c2" name="cc" data-chk-val="2"  class="chk_avail"/>
                                    <label for="c2"><span></span></label>
                                </div>
                            </div>
                            <div class="span9">
                                <br clear="all">
                                <span class="green-badge avail-soon">available soon</span>
                            </div>
                        </div>
                        <div data-val="3" class="span12 default-margin-left availability-ops custom-cursor-pointer">
                            <div class="span3">
                                <div class="radio radio-drop pull-right"><span class="">
                                        <input type="radio" value="-1" id="optionsRadios2" name="optionsRadios"></span>
                                </div>
                                <div class="custom-box edit-avl-chk">
                                    <input type="checkbox" id="c3" name="cc" data-chk-val="3" class="chk_avail"/>
                                    <label for="c3"><span></span></label>
                                </div>
                            </div>
                            <div class="span9">
                                <br clear="all">
                                <span class="grey-badge offset1">not available for work</span>
                                span class="normal-note">I'm open to offers.</span
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    
    
    <input type="hidden" name="work_availability" id="work_availability" value="">
    
    
    </div>
    </div>-->
    <div class="row-fluid edit-avail-web-btn-option">
        <nav id="social-connected-profile-full">
            <ul class="public-social-list">
                <?php
                foreach ($get_social_link as $key => $social) {
                    ?>
                    <li id="social_link_<?= $social['social_id']; ?>">
                        <a target="_blank" href="<?= $social['social_link']; ?>">
                            <i class="font-icon-social-<?= $social['social_type']; ?>"></i>
                        </a>
                    </li>
                    <?php
                }
                ?>
            </ul>
        </nav>
        <button class="btn-flat gray edit-web-link-button" id="edit_available_link_button">ADD WEB LINKS</button>
        <?php
        $availability_text = 'SET WORK AVAILABILITY';
        $background_color = '';

        if ($availability == 1) {
            $availability_text = 'AVAILABLE FOR WORK';
            $background_color = 'background-color:#96bf48';
        } else if ($availability == 2) {
            $availability_text = 'AVAILABLE SOON';
            $background_color = 'background-color:#5ba0a3';
        } else if ($availability == 3) {
            $availability_text = 'NOT AVAILABLE';
            $background_color = 'background-color:#b85e80';
        }
        ?>
        <button class="btn-flat inverse edit-available-link-button" id="edit_web_link_button" style="<?= $background_color ?>"><?= $availability_text ?></button>
        <div class="clearfix"></div>
    </div>
</div>

</div>
<div class="clearfix"></div>
</form>
</div>
</div>

<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery.uniform.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var checked_val = '<?php echo $availability ?>';
        if (checked_val) {
            $('#c' + checked_val).prop('checked', true);
        }
        $('.chk_avail').change(function () {
            if ($(this).is(':checked')) {
                var value = $(this).attr('data-chk-val');
                $('input:checkbox').not(this).prop('checked', false);

            }
            else {
                value = "0";
            }

            if (value == "1") {
                $('.avail-work-container').find('.avail-section').html('<div class="span9 chk-av-current"><span id="chk-avail" class="green-badge offset5">available for work</span></div>');
                //$('.avail-work-container').find('#chk-avail').css('background-color', '#59B958');
            } else if (value == "2") {
                $('.avail-work-container').find('.avail-section').html('<div class="span9 chk-av-current"><span id="chk-avail" class="green-badge offset5">available soon</span></div>');

            } else if (value == "3") {
                $('.avail-work-container').find('.avail-section').html('<div class="span9 chk-av-current"><span id="chk-avail" class="grey-badge offset5">not available for work</span></div>');

            } else if (value == "0") {
                $('.avail-work-container').find('.avail-section').html('<h4 class="bolded-head">What is your availability for work? </h4><p>Keep recruiters in the loop with your status! </p>');

            }

            var data = "availability=" + value + "&callfrom=availablity";
            $.ajax({
                type: 'POST',
                data: data,
                url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                dataType: 'json',
                success: function (responseData) {
                    if (responseData.status) {
                        $('.loader-save-holder').hide();
                        if (responseData.total_rows > 0) {
                            $(".single-team").find($(".complete-tick-mark")).css("color", "#96BF48");
                        } else {
                            $(".single-team").find($(".complete-tick-mark")).css("color", "");
                        }
//                                                           new Morris.Donut({
//                                                               element: 'hero-donut',
//                                                               width:300,
//                                                               data: [
//                                                                   {label: 'Profile', value: responseData.scorer.profile_scorer },
//                                                                   {label: 'Resume', value: responseData.scorer.resume_scorer },
//                                                                   {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                                                                   {label: 'Incomplete', value: responseData.scorer.incomplete }
//                                                               ],
//                                                               colors: ["#30a1ec", "#76bdee", "#c4dafe", "#F9785B"],
//                                                               formatter: function (y) {
//                                                                   return y + "%"
//                                                               }
//                                                           });
                        var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                        update_profile_completness_top_chart(prof_comp);
//                                                           $("#score-chart-input-field").val(responseData.scorer.fullstatus);
//                                                           $(".knob").knob();
//                                                           $("#score-chart-input-field").val(responseData.scorer.fullstatus+"%");
                        publish_unpublish_btn(responseData.scorer.fullstatus);
                    }
                }
            });
        });
    });

    function validateURL(textval) {
        var urlregex = new RegExp(
                "^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
        return urlregex.test(textval);
    }
    $(document).on("click", ".social_label", function () {
        var path_img = '<?= ASSETS_PATH . "/img/"; ?>';
        var selected_label = $(this).attr("data-value");
        $("#social_value").val($(this).attr("data-value"));
        if (selected_label == "linkedin") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-linkedin");
            //$("#img-social").attr("src", path_img + "Linkedin-s.png")
        }
        else if (selected_label == "twitter") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-twitter");
            //$("#img-social").attr("src", path_img + "Twitter-s.png")
        }
        else if (selected_label == "facebook") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-facebook");
            //$("#img-social").attr("src", path_img + "Facebook-s.png")
        }
        else if (selected_label == "dribbble") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-dribbble");
            //$("#img-social").attr("src", path_img + "Dribble-s.png")
        }
        else if (selected_label == "github") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-github");
            //$("#img-social").attr("src", path_img + "Github-s.png")
        }
        /*else if (selected_label == "smugmug") {
         $("#icn-social").removeClass();
         $("#icn-social").addClass("font-icon-social-smugmug");
         //$("#img-social").attr("src", path_img + "SmugMug-s.png")
         }*/
        else if (selected_label == "500px") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-500px");
            //$("#img-social").attr("src", path_img + "500px-s.png")
        }
        else if (selected_label == "vimeo") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-vimeo");
            //$("#img-social").attr("src", path_img + "Vimeo-s.png")
        }
        else if (selected_label == "flickr") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-flickr");
            //$("#img-social").attr("src", path_img + "Flicker-s.png")
        }
        else if (selected_label == "youtube") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-youtube");
            //$("#img-social").attr("src", path_img + "Youtube-s.png")
        }
        else if (selected_label == "pinterest") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-pinterest");
            //$("#img-social").attr("src", path_img + "Pinterest-s.png")
        }
        else if (selected_label == "tumblr") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-tumblr");
            //$("#img-social").attr("src", path_img + "Tumblr-s.png")
        }
        else if (selected_label == "google-plus") {
            $("#icn-social").removeClass();
            $("#icn-social").addClass("font-icon-social-google-plus");
            //$("#img-social").attr("src", path_img + "Google-s.png")
        }
        else {
            alert("Please select Web");
            return false;
        }

    });
    $(document).on("click", "#social_url_btn", function () {

        if ($('#social_url').val() == "") {
            $('#social_url').css({
                border: '1px solid red'
            });
            $('#social_url').focus();
            return false;
        }

        /*if ($('#social_url').val() != "") {
         var email = $('#social_url').val();
         var pattern = /^[A-z0-9]+([A-z0-9]+[\.\_]?[A-z0-9]+)*@[A-z0-9]{2,}(\.[A-z0-9]{1,}){1,2}[A-z]$/;
         if (!pattern.test(email)) {
         alert("Please Provide Email Format Eg: test@test.com");
         $('#social_url').css({
         border: '1px solid red'
         });
         $('#social_url').focus();
         return false;
         }
         $('#social_url').css({
         border: ''
         });
         }
         */
        var urlPattern = new RegExp('/|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i/');
        var social_val = $("#social_value").val();
        var social_url = $("#social_url").val();
        if (social_url.match(urlPattern)) {
            social_url = social_url;
        } else {
            social_url = 'http://' + social_url;
        }
        var valid = validateURL(social_url)
        if (valid) {
            if (social_val == "") {
                social_val = "facebook";
            }
            var data = "social_val=" + social_val + "&social_url=" + social_url + "&callfrom=social";
            if (social_val != "" && social_url != "") {
                $('#social-link-add-loader').show();
                $.ajax({
                    type: 'POST',
                    data: data,
                    url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                    dataType: 'json',
                    success: function (responseData) {
                        if (responseData.status) {
                            //$(".public-social-network-highlight").find('.public-social-list').append('<li><a target="_blank" href="<?= $social['social_link']; ?>"><i class="font-icon-social-' + social_val + '"></i></a></li>');
                            if (responseData.cnt > 1) {
                                $(".public-social-network-highlight").find('.public-social-list').append('<li><a target="_blank" href="' + social_url + '"><i class="font-icon-social-' + social_val + '"></i></a><span data-social-id=' + responseData.social_id + ' class="icon-remove-circle btn-remove-social"></span></li>');
                            } else {
                                $(".social_link_section").html('<div class="row-fluid"><div class="public-social-network-highlight pull-left span12"><nav id="social-connected-profile"><ul class="public-social-list"><li><a target="_blank" href="' + social_url + '"><i class="font-icon-social-' + social_val + '"></i></a><span data-social-id=' + responseData.social_id + ' class="icon-remove-circle btn-remove-social"></span></li></ul></nav></div></div>');
                            }

                            $("#social-connected-profile-full ul").append('<li id="social_link_'+ responseData.social_id +'"><a target="_blank" href="' + social_url + '"><i class="font-icon-social-' + social_val + '"></i></a></li>');
                            $("#social-footer ul").append('<li data-id="' + responseData.social_id + '"><a target="_blank" href="' + social_url + '"><i class="font-icon-social-' + social_val + '"></i></a></li>');
                            //$(".public-social-network-highlight").find('.public-social-list').append('<li><a class="' + social_val + '" href="' + social_url + '">' + social_val + '</a></li>');
//                        $('.loader-save-holder').hide();

                            if (responseData.total_rows > 0) {
                                $(".single-team").find($(".complete-tick-mark")).css("color", "#96BF48");
                            }
                        }
                        $('#social-link-add-loader').hide();
//                    new Morris.Donut({
//                        element: 'hero-donut',
//                        width:300,
//                        data: [
//                            {label: 'Profile', value: responseData.scorer.profile_scorer },
//                            {label: 'Resume', value: responseData.scorer.resume_scorer },
//                            {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                            {label: 'Incomplete', value: responseData.scorer.incomplete }
//                        ],
//                        colors: ["#30a1ec", "#76bdee", "#c4dafe", "#F9785B"],
//                        formatter: function (y) {
//                            return y + "%"
//                        }
//                    });
//                    update_knob(responseData.scorer.fullstatus);
                        if (responseData.scorer.resume_scorer + responseData.scorer.portfolio_scorer + responseData.scorer.profile_scorer > "74") {
                            $('.custom-p-color').html('Congratulations, you can publish your profile!');
                        } else {
                            $('.custom-p-color').html('Reach 75% and get published!');
                        }

                        var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                        update_profile_completness_top_chart(prof_comp);
                        publish_unpublish_btn(responseData.scorer.fullstatus);

                        // add social link on main profile
                    }
                });
            }
        }
        else {
            $('#social_url').focus();
            alert("Please Provide valid Url");
            return false;
        }

    });
    /*Social Remove Section Start*/

    $(document).on('click', '.btn-remove-social', function () {
        var obj = this;
        var item_id = $(this).attr('data-social-id');
        //console.log($('#social-footer ul li[data-id='+item_id+']'));return;
        $.ajax({
            type: 'POST',
            data: {
                'item_id': item_id,
                'callfrom': 'removesocial'
            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).closest('li').remove();

                $('#social-footer ul li[data-id="' + item_id + '"]').remove();
                if (data.total_rows == 0) {
                    $(".social_link_section").html('<h4 class="bolded-head">Connect your Social Network</h4><p>Select networks from dropdown menu and add URL </p>');
                    //$("." + data.clas).find($(".complete-tick-mark")).css("color", "");
                    if (data.total_rows == 0) {
                        $(".single-team").find($(".complete-tick-mark")).css("color", "");
                    }
                }

                if (data.scorer.resume_scorer + data.scorer.portfolio_scorer + data.scorer.profile_scorer > "74")
                {
                    $('.custom-p-color').html('Congratulations, you can publish your profile!');
                } else
                {
                    $('.custom-p-color').html('Reach 75% and get published!');
                }
//                    new Morris.Donut({
//                        element: 'hero-donut',
//                        width:300,
//                        data: [
//                            {label: 'Profile', value: data.scorer.profile_scorer },
//                            {label: 'Resume', value: data.scorer.resume_scorer },
//                            {label: 'Portfolio', value: data.scorer.portfolio_scorer },
//                            {label: 'Incomplete', value: data.scorer.incomplete }
//                        ],
//                        colors: ["#30a1ec", "#76bdee", "#c4dafe", "#F9785B"],
//                        formatter: function (y) {
//                            return y + "%"
//                        }
//                    });

                $("#social_link_"+ item_id).remove();
                var prof_comp = parseInt(data.scorer.profile_scorer) + parseInt(data.scorer.resume_scorer) + parseInt(data.scorer.portfolio_scorer);
                update_profile_completness_top_chart(prof_comp);
//                    $("#score-chart-input-field").val(data.scorer.fullstatus);
//                    $(".knob").knob();
//                    $("#score-chart-input-field").val(data.scorer.fullstatus+"%");
                publish_unpublish_btn(data.scorer.fullstatus);
            }
        });
    })

    /*Social Remove Section End*/
</script>

