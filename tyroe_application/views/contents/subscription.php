<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');

                    ?>

                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">
                            <div class="span12">
                                <div class="container-fluid">
                                    <div class="span4 default-header">
                                        <h4><?= LABEL_SCHEDULE_NOTIFICATION ?></h4>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>