<script>

    $("#job_tags").select2({
        tags:[""],
        tokenSeparators: [","]
    });

    $("#start_date").datepicker({showCheckbox : false,format: "mm/dd/yyyy"});
    <?php
     if($opening_details['start_date']=="" || $opening_details['start_date']==0)
     {
    ?>
    $("#start_date").datepicker('setValue', new Date());
    <?php } ?>
    $("#end_date").datepicker({showCheckbox : false,format: "mm/dd/yyyy"});
    <?php
     if($opening_details['end_date']=="" || $opening_details['end_date']==0)
     {
    ?>
    $("#end_date").datepicker('setValue', new Date());
    <?php } ?>

    function getcities(country_id){
        $('.loader-save-holder').show();
        var data ="country_id=" + country_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("jobopening/getcities")?>',
            dataType: "json",
            success :function(response){
                $('.loader-save-holder').hide();
                $('#citybox').html(response.dropdown);
            }
        });
    }
    function dateDiff( date1, date2 ) {
      //Get 1 day in milliseconds
      var one_day=1000*60*60*24;

      // Convert both dates to milliseconds
      var date1_ms = date1.getTime();
      var date2_ms = date2.getTime();

      // Calculate the difference in milliseconds
      var difference_ms = date2_ms - date1_ms;

      // Convert back to days and return
      return Math.round(difference_ms/one_day);
    }
     function saveopening(){
        var studio_id = $("#studio_id").val();
        var job_id = $("#job_id").val();
        var job_title = $("#job_title").val();
        var job_desc = $("#job_desc").val();
        var job_industry = $("#industry").val();
        var flag = $("#flag").val();
        var job_country = $("#job_country").val();
        var job_city = $("#city option:selected").text();
        var job_startdate = $("#start_date").val();
        var job_enddate = $("#end_date").val();
        var job_type = $("#job_type:checked").val();
        var job_level = $("#job_level:checked").val();
        var job_tags = $("#job_tags").val();
        var date1 = new Date(job_startdate);
        var date2 = new Date(job_enddate);
        console.log(date1);
        console.log(date2);
        var diff= dateDiff(date1,date2);
        if(diff < 0)
        {
            $('#end_date').css({ border: '1px solid red'});
            $('#end_date').focus();
            alert('End date cannot be greater than start date');
            return false;
        }
        /*if (studio_id == '') {
            $('#studio_id').css({ border: '1px solid red'});
            $('#studio_id').focus();
            return false;
        } else {
            $('#studio_id').removeAttr("style")
        }*/
        if (studio_id == '') {
            $('#studio_id').css({ border: '1px solid red'});
            $('#studio_id').focus();
            return false;
        } else {
            $('#studio_id').removeAttr("style")
        }
        if (job_title == '') {
            $('#job_title').css({ border: '1px solid red'});
            $('#job_title').focus();
            return false;
        } else {
            $('#job_title').removeAttr("style")
        }
        if (job_desc == '') {
            $('#job_desc').css({ border: '1px solid red'});
            $('#job_desc').focus();
            return false;
        } else {
            $('#job_desc').removeAttr("style")
        }
        if (job_industry == '') {
            $('#industry').css({ border: '1px solid red'});
            $('#industry').focus();
            return false;
        } else {
            $('#industry').removeAttr("style")
        }
        if (job_title == '') {
            $('#job_country').css({ border: '1px solid red'});
            $('#job_country').focus();
            return false;
        } else {
            $('#job_country').removeAttr("style")
        }
        if (job_city == '') {
           $('#city').css({ border: '1px solid red'});
           $('#city').focus();
           return false;
        } else {
           $('#city').removeAttr("style")
        }
        if (job_startdate == '') {
           $('#start_date').css({ border: '1px solid red'});
           $('#start_date').focus();
           return false;
        } else {
           $('#start_date').removeAttr("style")
        }
        if (job_enddate == '') {
           $('#end_date').css({ border: '1px solid red'});
           $('#end_date').focus();
           return false;
        } else {
           $('#end_date').removeAttr("style")
        }
        if (job_type == '') {
           $('#job_type').css({ border: '1px solid red'});
           $('#job_type').focus();
           return false;
        } else {
           $('#job_type').removeAttr("style")
        }
        if (job_level == '') {
           $('#job_level').css({ border: '1px solid red'});
           $('#job_level').focus();
           return false;
        } else {
           $('#job_level').removeAttr("style")
        }
        var data = "studio_id="+studio_id+"&job_id="+job_id+"&job_title="+job_title+"&job_desc="+job_desc+"&job_industry="+job_industry+"&flag="+flag+
            "&job_country="+job_country+"&job_city="+job_city+"&job_startdate="+job_startdate+"&job_enddate="+job_enddate+"&job_type="+job_type+"&job_level="+job_level+
            "&job_tags="+job_tags;

        //alert(studio_id+" "+job_id+" "+job_title+" "+job_desc+" "+job_industry+" "+flag+" "+job_country+" "+job_city+" "+job_startdate+" "+job_enddate+" "+job_type+" "+job_level+" "+job_tags);
        //alert(data);
        $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?=$vObj->getURL("jobopenings/saveopening")?>',
                    dataType:"json",
                    success : function(data){
                      $('#myModal').modal('hide');
                        if (data.success) {
                               $(".abc").addClass("alert alert-success");
                               $(".alert-success").html('<i class="icon-ok-sign"></i>'+data.success_message);

                               $('.alert-success').fadeIn(500);
                               $('.alert-success').delay(2000).fadeOut(1500);
                               if(data.flag == 'new'){
                                   $('tbody.all_openings').prepend(data.html)
                               } else {
                                   var job_id = '#job_id_'+data.id;
                                   $(job_id).html(data.html);
                               }
                                $('.loader-save-holder').hide();
                        }
                        else {
                            $(".abc").addClass("alert alert-danger");
                            $(".alert-danger").html('<i class="icon-remove-sign"></i>'+"Fail to Save Experience");
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                            $('.alert-success').css('display', 'block');
                            $("#error_msg").html("Fail to save experience");
                        }
                    },
                    error : function(errorthrown){
                        console.log(errorthrown.error);
                    }
                });
    }
    function EnableFields(val){
        if(val != ''){
            $('form[name="register_form"]').find('input,textarea,select').removeAttr('disabled');
        } else {
            $('form[name="register_form"]').find('input,textarea,select').attr('disabled','disabled');
        }
    }

        var val = $('#studio_id').val();
        EnableFields(val);
</script>
<div class="span5 tab-margin">
    <div class="container-fluid">
        <div class="span4 default-header headerdiv">
            <h4>
                <?php
                if ($flag == "edit") {
                    echo TITLE_ACTION_EDIT . " " . LABEL_JOB_OPENING;
                } else {
                    echo LABEL_TOOLBAR_NEW . " " . LABEL_JOB_OPENING;
                }
                ?>
            </h4>
        </div>
        <div class="field-box error">
                    <span><?php echo validation_errors();

                        if ($msg != "") {
                            echo $msg;
                        } ?></span>
        </div>
        <?php
        if($flag=="edit")
        {
        ?>
        <h4>STUDIO: <?=$studio_name?></h4>
        <input type="hidden" value="edit" name="flag" id="flag">
        <input type="hidden" value="<?= $opening_details['job_id'] ?>" name="job_id" id="job_id">
        <input type="hidden" value="<?= $opening_details['user_id'] ?>" name="studio_id" id="studio_id">
        <?php
        }else{
        ?>
            <select onchange="EnableFields(this.value);" name="studio_id" id="studio_id">
                <option value="" class="display_hide">Select Studio</option>
                <?php foreach($studios as $studio)
                {
                ?>
                    <option value="<?=$studio['user_id']?>"><?=$studio['username']?></option>
                <?php
                }
                ?>
            </select>
        <?php
        }
        ?>

        <form class="inline-input" _lpchecked="1"
              enctype="multipart/form-data" name="register_form" method="post">
            <div class="basic_info">
                <div class="clearfix"></div>
                <div class="field-box bottom-margin">
                    <h4><a href="javascript:void(0)">Job Opening Details</a></h4>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_JOB_TITLE ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span3" type="text"
                                                         name="job_title" id="job_title"
                                                         value="<?= $opening_details['job_title'] ?>"></div>
                </div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_JOB_DESCRIPTION ?>:</label></div>
                    <div class="span3 tab-margin"><textarea rows="4" class="span3" name="job_desc" id="job_desc"><?= str_ireplace("<br />","\r\n",$opening_details['job_description']) ?></textarea></div>
                </div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_JOB_INDUSTRY ?>:</label></div>
                    <div class="ui-select span3"><select name="industry" id="industry">
                            <option value="" class="display_hide">SELECT INDUSTRY</option>

                            <?php
                            foreach($industries as $industry)
                            {
                                $selected="";
                                if($opening_details['industry_id']==$industry['industry_id'])
                                {
                                $selected="selected=selected";
                                }
                            ?>
                                <option <?=$selected?> value="<?=$industry['industry_id']?>"><?=$industry['industry_name']?></option>
                            <?php
                            }
                            ?>
                    </select></div>
                </div>
                <div class="clearfix"></div>

                <div class="span4 field-box span5 tab-margin bottom-margin">
                    <div class="span2 tab-margin"><label><?=LABEL_COUNTRY?>:</label></div>
                    <!--<div class="span4 tab-margin">-->
                    <div class="ui-select span3">
                        <select id="job_country" name="job_country" onchange="getcities(this.value)">
                            <option value="" class="display_hide">Select Country</option>
                        <?php //$user_country_dropdown;
                        foreach($get_countries as $country)
                        {
                            $selected="";
                            if($country['id']==$opening_details['country_id'])
                            {
                                $selected="selected=selected";
                            }
                            ?>
                            <option <?=$selected?> value="<?=$country['id']?>"><?=$country['name']?></option>
                        <?php
                        }
                        ?>
                       </select>
                    </div>
                </div>

                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
                    <div class="ui-select span3 all-states1" id="citybox">
                        <select class="span3 inline-input" name="city" id="city">
                            <option value="" class="display_hide">Select City</option>
                            <?php
                            foreach ($cities as $city) {
                                $selected="";
                                if($city['city']==$opening_details['job_city'])
                                    $selected="selected=selected";
                                ?>
                                <option <?=$selected?> value="<?=$city['city'] ?>"><?=$city['city']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_START_DATE ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span2" type="text"
                                                         name="start_date" id="start_date"
                                                         value="<?=date('m/d/Y', $opening_details['start_date']);?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_END_DATE ?>:</label></div>
                    <div class="span3 tab-margin"><input class="span2" type="text"
                                                         name="end_date" id="end_date"
                                                         value="<?=date('m/d/Y', $opening_details['end_date']);?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_TYPE ?>:</label></div>
                    <div class="span3 tab-margin">
                           <?php
                           foreach($get_job_type as $type)
                           {
                               $checked="";
                               if($type['job_type_id']==$opening_details['job_type'])
                               {
                                   $checked="checked=checked";
                               }

                           ?>
                           <input <?=$checked?>  type="radio" id="job_type" value="<?=$type['job_type_id']?>" data-id="<?=$type['job_type']?>" name="job_type"><?=$type['job_type']?>
                           <?php
                           }
                           ?>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_JOB_LEVEL ?>:</label></div>
                    <div class="span3 tab-margin">
                        <?php
                        foreach($get_job_level as $level)
                        {
                            $checked="";
                               if($level['job_level_id']==$opening_details['job_level_id'])
                               {
                                   $checked="checked=checked";
                               }
                        ?>
                        <input <?=$checked?> type="radio" id="job_level" class="job_level" value="<?=$level['job_level_id']?>" data-id="<?=$level['level_title']?>" name="job_level"><?=$level['level_title']?>
                        <?php
                        }
                        ?>
                </div>
                    <?php
                    foreach ($skills as $skill)
                    {
                        $skillnames[]= $skill['creative_skill'];
                    }

                      $skillnamesval=implode(",",$skillnames);
                    ?>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_SKILL_TITLE ?>:</label></div>
                    <div class="span3 tab-margin"><input type="text" name="job_tags" id="job_tags" value="<?=$skillnamesval?>">
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="span5 field-box actions">
                <input type="button" name="submit" onclick="saveopening()" class="btn-glow primary"
                       value="<?= LABEL_SAVE_CHANGES ?>">
                <!--<span>OR</span>
                <input type="reset" value="Cancel" class="reset">-->
            </div>
        </form>
    </div>
    <span id="message"></span>

