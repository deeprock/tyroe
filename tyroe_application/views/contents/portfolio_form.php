<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>jQueryFileUploader/css/jquery.fileupload.css">
<style>
    .bar {
        height: 18px;
        background: green;
    }
</style>
<script>

    jQuery(".btn-save").click(function(){
        jQuery("#portfolio_form").submit();
    })

    $(function () {
        'use strict';
        var url = '<?=ASSETS_PATH?>uploads/index.php?dir=portfolio';
        $('#fileupload').click(function () {
            $('#progress .bar').css(
                    'width',
                    0 + '%'
            );
        })
        $('#fileupload').fileupload({
            url: url,
            dataType: 'json',
            done: function (e, data) {
                //jQuery("input[type='submit']").after('<img id="loader" src="<?= ASSETS_PATH ?>img/loader.gif"  width="20px"/>');
                $.each(data.result.files, function (index, file) {
                    $('<p/>').text(file.name).appendTo('#files');
                });
                $("#image").html($("#files").html());
                //jQuery("#loader").remove();

            },
            progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                //var progress = parseInt(data.loaded * 100, 10);
                $('#progress .bar').css(
                        'width',
                        progress + '%'
                );
            }
        }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
    });
</script>
<script type="text/javascript">

    function AddPortfolio() {
        if($("#media_id").val() > 0){
            var media_featured = 0;
            if($("#media_featured").is(':checked')){ media_featured = 1; }
            var data = "media_featured="+media_featured+"&image_id=<?=$media_data['image_id']?>";
            $.ajax({
                type:"POST",
                data:data,
                url:'<?=$vObj->getURL("portfolio/save_image")?>',
                dataType:"json",
                success:function (data) {
                    if (data.success) {
                        $(".field-box").hide();
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                }
            });
        } else {
            var image = $(".files").html();
            var media_title = $("#media_title").val();
            var media_featured = 0;
            if($("#media_featured").is(':checked')){ media_featured = 1; }
            var image_description = $("#image_description").val();

            var data = "media_title=" + media_title + "&image_description=" + image_description + "&image=" + image+"&media_featured="+media_featured;
            $.ajax({
                type:"POST",
                data:data,
                url:'<?=$vObj->getURL("portfolio/save_image")?>',
                dataType:"json",
                success:function (data) {
                    if (data.success) {
                        $(".field-box").hide();
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                    else {
                        $(".notification-box-message").css("color", "#b81900");
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                },
                failure:function (errMsg) {
                }
            });
        }
    }
</script>
<!-- IMAGE UPLOADER START -->

<!-- IMAGE UPLOADER END -->


<!-- START RIGHT COLUMN -->


        <div class="span5 tab-margin">
            <div class="container-fluid">
                <div class="span4 default-header tab-margin headerdiv">
                    <h4><?= LABEL_ADD_PORTFOLIO ?></h4>
                </div>
                <div class="field-box">
                    <span id="error_msg"><?php //echo validation_errors();
                        //if($msg!=""){echo $msg;}
                        ?></span>
                </div>
                <form class="new_user_form inline-input" _lpchecked="1"
                      enctype="multipart/form-data" name="portfolio_form" id="portfolio_form"
                      method="post" action="<?=$vObj->getURL("portfolio/save_image");?>">
                    <div class="field-box span5 tab-margin">
                        <div class="span2 tab-margin"> <label><?= LABEL_MEDIA_TITLE ?>:</label></div>
                            <div class="span3 tab-margin"><input class="span3" type="text"
                               name="media_title" id="media_title"
                               value="<?php echo $media_data['media_title']; ?>"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="field-box span5 tab-margin margin-adjust">
                        <div class="span2 tab-margin">  <label><?= LABEL_FEATURED ?>:</label></div>
                            <div class="span3 tab-margin"> <input class="" type="checkbox"
                               name="media_featured" id="media_featured"
                               value="1" <?php if ($media_data['media_featured'] == 1) {
                            echo "checked";
                        } ?>>
                            </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="field-box span5 tab-margin">
                        <div class="span2 tab-margin"> <label><?= LABEL_IMAGE_DESCRIPTION ?>:</label></div>
                            <div class="span3 tab-margin"><textarea class="span3 inline-input"
                                  name="image_description"
                                  id="image_description"><?php echo $media_data['media_description']; ?></textarea></div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="field-box span5 tab-margin">
                        <div class="span2 tab-margin"> <label><?= LABEL_IMAGE ?>:</label></div>
                            <div class="span3 tab-margin"> <?php if (!empty($media_data['image_id'])) { ?><img src="<?=ASSETS_PATH_PORTFOLIO_THUMB . $media_data['media_name'];?>"/><?php
                                                        }
                                                        ?></div>
                        <input type="hidden" id="img_name" value="<?=$media_data['media_name']?>"
                               name="img_name"/>
                        <input type="hidden" value="<?=$media_data['image_id']?>" id="media_id" name="media_id"/>
                        <textarea style="display: none" name="image" id="image"></textarea>
                        <?php if (empty($media_data['image_id'])) { ?>
                        <!-- UPLOADER AREA -->
                        <span class="btn btn-success fileinput-button">
                                <i class="glyphicon glyphicon-plus"></i>
                                <span>Select files...</span>
                                <!-- The file input field used as target for the file upload widget -->
                                <input id="fileupload" type="file" name="files[]" multiple>
                            </span>

                        <br>
                        <br>
                        <!-- The global progress bar -->
                        <div id="progress" class="progress">
                            <div class="bar" style="width: 0%;"></div>
                        </div>
                        <!-- The container for the uploaded files -->
                        <div id="files" class="files"></div>
                        <!-- UPLOADER AREA -->
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="span1 pop-btn field-box actions tab-margin">
                        <input type="submit" class="btn-glow primary pull-right" value="<?= LABEL_SAVE_CHANGES ?>">
                        <!--<span>OR</span>
                        <input type="reset" value="Cancel" class="reset"
                               onclick="window.history.back()">-->
                    </div>
                        <div class="clearfix"></div>
                </form>
            </div>
            <span id="message"></span>
        </div>

<!-- END RIGHT COLUMN -->







