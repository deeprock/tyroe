<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script src="https://code.jquery.com/jquery-latest.min.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/lib/select2.css">
<link rel="stylesheet" type="text/css" href="<?=ASSETS_PATH?>css/register.css">
<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
<script src="<?= ASSETS_PATH ?>js/handlebars.js"></script>
<script src="<?= ASSETS_PATH ?>js/select2.min.js"></script>

<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img src="<?= ASSETS_PATH ?>img/logo-dark.png" class="logo"/>
    </a>
    <script id="company_location_template" type="text/x-handlebars-template">
        {{#locations}}
        <div class="location_row">
            <div class="icon"><i class="fa fa-globe"></i></div>
            <div class="location_name">{{city}}, {{country}}</div>
            <div class="select_box">
                <input type="checkbox" class="select_location" id="location_{{city_id}}_{{country_id}}">
            </div>
        </div>
        {{/locations}}
        <div class="location_row">
            <div class="icon"><i class="fa fa-globe"></i></div>
            <div class="location_name green">Add new location</div>
            <div class="select_box">
                <input type="checkbox" class="select_location select_location_new" id="location_0_0">
            </div>
        </div>
    </script>
    <div class="span4 box">
        <form _lpchecked="1" class="new_user_form inline-input" method="post"
              action="<?= $vObj->getURL('register/save_user') ?>" name="RegForm" onsubmit="return form_submit();">
            <div class="content-wrap">
                <?php
//                $studio_message = "<b>Thank you for registering your company.</b>
//                                   <br><br>
//                                   During this BETA period, we are manually approving companies. Please be patient and we will be in touch with you as quickly as possible!
//                                   <br><br>
//                                   Andrew & Alwyn - Founders of Tyroe.";
                if($role_id==3){ //echo "<span class='no-account'>".$studio_message."</span>";
                } else if($role_id==4){ //echo "<span class='no-account'>Thank you for registration. Your account will be active by Studio.</span>";
                } else {
                ?>
                <h6><?= LABEL_CREATE_ACCOUNT ?></h6>

                <div class="field-box error">
                    <?php
                    echo validation_errors();
                    if ($msg != null) {
                        ?>
                        <div class="alert alert-warning">
                            <i class="icon-warning-sign"></i>
                            <?php echo $msg; ?>
                        </div>
                    <?php

                    }
                    ?>
                </div>
                
                <?php if(!isset($is_company_invite_active)){?>
                <div class="field-box">
                    <div class="btn-group" id="user_switcher">
                        <!--For invite reviewer-->
                        <button onclick="return false;" class="glow left <?=$talent_active?> talent_btn">I'm talent </button>
                        <button onclick="return false;"  class="glow right <?=$company_active?> company_btn">I'm a company</button>
                    </div>
                </div>
                <span class="half_seperator"></span>
                <?php
                }
                ?>
                <div class="field-box company-box" style="display:<?= $company_active=='' ? 'none' : 'block' ?>">
                    <?php if(isset($is_company_invite_active) && $is_company_invite_active == true){?>
                        <div class="company-name-disabled"><?php echo $company_name; ?><i class="fa fa-check-circle"></i></div>
                        <input type="hidden" name="company_name" placeholder="<?= LABEL_COMPANY_NAME ?>" id="company_name" value="<?php echo $company_name; ?>">
                    <?php }  else { ?> 
                        <input class="span12" <?= $company_active=='' ? '' : 'readonly' ?>  type="text" name="company_name" placeholder="<?= LABEL_COMPANY_NAME ?>" id="company_name" value="<?php echo $company_name; ?>" class="ui-autocomplete-input" autocomplete="on">
                    <?php } ?>
                    
                    
                    <div class="searching_company_name_result"></div>
                    <div class="company_locations" id="company_locations">
                        <?php if(isset($is_company_invite_active) && $is_company_invite_active == true){?>
                        <input type="hidden" name="country" value="<?=$company_country_id?>"/>
                        <input type="hidden" name="city" value="<?=$company_city_id?>"/>
                        <?php
                        }
                        ?>
                    </div>
                    <?php if(!isset($is_company_invite_active)){?>
                    <div class="field-box active" style="margin-bottom: 25px;" id="location_select">
                        <div class="row-fluid" style="margin-bottom: 15px;">
                            <div class="span6">
                                <select name="country" placeholder="Country" id="country" onchange="getcities(this.value);" class="country" value="<?php echo set_value('country'); ?>">
                                    <option></option>
                                    <?php
                                    foreach ($get_countries as $country) {
                                    ?>
                                    <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="span6" id="citybox">
                                <select name="city" id="city" class="city" value="<?php echo set_value('city'); ?>">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <?php
                    }
                    ?>
                    <span class="half_seperator"></span>
                    <input class="span12" type="text" name="job_title" placeholder="<?= LABEL_JOB_TITLE ?>" id="job_title" value="<?php echo $job_title; ?>">
                </div>
                <div class="btn-group singup-group">
                    <input class="span6" type="text" name="user_firstname" placeholder="<?= LABEL_FIRST_NAME ?>" id="user_firstname" value="<?php echo set_value('user_firstname'); ?>">
                    <input class="span6" type="text" name="user_lastname" placeholder="<?= LABEL_LAST_NAME ?>" id="user_lastname" value="<?php echo set_value('user_lastname'); ?>">
                </div>
<!--                <input class="span12" type="text" name="user_name" placeholder="<?= LABEL_USER_NAME ?>" id="user_name" value="<?php echo set_value('user_name'); ?>">-->
                 <?php if(isset($is_company_invite_active) && $is_company_invite_active == true){?>
                    <div class="user-email-disabled"><?php echo $user_email; ?><i class="fa fa-check-circle"></i></div>
                    <input type="hidden" name="user_email" placeholder="<?= LABEL_COMPANY_NAME ?>" id="user_email" value="<?php echo $user_email; ?>">
                <?php }  else { ?> 
                    <input class="span12"  type="text" name="user_email" placeholder="<?= LABEL_EMAIL ?>" id="user_email" value="<?php echo set_value('user_email'); ?>">
                <?php } ?>
                    
                <?php if($company_active == ''){ ?>
<!--                    <input class="span12"  type="text" name="user_email" placeholder="<?= LABEL_EMAIL ?>" id="user_email" value="<?php echo set_value('user_email'); ?>">-->
                <?php }else{?>
<!--                    <input class="span12" readonly type="text" name="user_email" placeholder="<?= LABEL_EMAIL ?>" id="user_email" value="<?php echo $user_email; ?>">-->
                <?php } ?>
                <input class="span12" type="password" name="user_password" placeholder="<?= LABEL_PASSWORD ?>"
                       id="user_password" value="<?php echo set_value('user_password'); ?>">
                <div class="tearms_box" id="tearms_box">
                    <input type="checkbox" name="tearms_checkbox" id="tearms_checkbox">&nbsp;&nbsp;&nbsp;<a href="javascript:void(0)">Terms & Conditions</a>
                </div>
                <div class="action">
                    <input type="submit" class="btn-flat primary prim-ary-login" value="<?= LABEL_SIGNUP_BUTTON ?>">
                    <input type="hidden" name="invite_user_id" value="<?= $invite_user_id ?>"/>
                    <input type="hidden" name="invite_coupon_id" value="<?= $invite_coupon_id ?>"/>
                    <input type="hidden" name="user_type" id="user_type" value="talent"/>
                    <?php if(isset($is_company_invite_active) && $is_company_invite_active == true){?>
                    <input type="hidden" name="new_location" id="new_location" value="<?=$company_city_id.'_'.$company_country_id?>"/>
                    <input type="hidden" name="parent_id" id="parent_id" value="<?=$invite_studio_id?>"/>
                    <input type="hidden" name="parent_company_studio_id" id="parent_company_studio_id" value="<?=$invite_studio_id?>"/>
                    <input type="hidden" name="invite_job_id" value="<?=$invite_job_id?>"/>
                    <input type="hidden" name="is_company_invite_active" id="is_company_invite_active" value="1"/>
                    <?php } else{ ?>
                    <input type="hidden" name="new_location" id="new_location" value=""/>
                    <input type="hidden" name="parent_id" id="parent_id" value="0"/>
                    <input type="hidden" name="parent_company_studio_id" id="parent_company_studio_id" value="0"/>
                    <?php
                    }
                     ?>
                    <input type="hidden" name="error_id" id="error_id" value="<?=$error_id?>"/>
                </div>
                <?php } ?>
            </div>
        </form>
    </div>
    <div class="span4 no-account">
        <p><?= LABEL_ALREADY_ACCOUNT ?></p>
        <a href="<?= $vObj->getURL('login') ?>"><?= LABEL_SIGNIN ?></a>
    </div>
</div>
<script type="text/javascript">
    var source  = [ ];
    var mapping = { };
//    function direct_on_dashboard(email, pw) {
//        var error_id = $('#error_id').val();
//        if (email != "" && pw != "" && error_id != 1) {
//            $.ajax({
//                type: 'POST',
//                data: {user_email: '<?=$_REQUEST["user_email"]?>', pass: '<?=$_REQUEST["user_password"]?>'},
//                url: '<?=$vObj->getURL("login/login_auth")?>',
//                dataType: 'json',
//                async : false,
//                success: function (data) {
//                    var role = data.role;
//                    console.log(role);
//                    if (data.success) {
//                        console.log(role);
//                        //session redirect_to_job_overview &&  redirect_to_job_opening from api buttons redirection
//                        if( role == 3 || role == 4 ){
//                            window.location = '<?=$vObj->getURL("home")?>';
//                        } else {
//                            console.log("Role: "+role);
//                            <?php if($this->session->userdata('redirect_to_job_overview') == 'yes' ){?>
//                                window.location = '<?=$vObj->getURL("openings")?>';
//                            <?php }else if($this->session->userdata('redirect_to_job_opening') == 'yes'){ ?>
//                                window.location = '<?=$vObj->getURL("openings")?>';
//                            <?php }else{ ?>
//                                window.location = '<?=$vObj->getURL("profile")?>';
//                            <?php } ?>
//                        }
//                    }
//                }
//            });
//        }
//    }

    //direct_on_dashboard('<?=$_REQUEST['user_email']."','".$_REQUEST['user_password'];?>');

    $(document).ready(function(){

        $('.company_btn').click(function() {
            $(this).addClass('active');
            $('.company-box').css("display","");
            $('.talent_btn').removeClass('active');
            $('#user_type').val('company');
            clear_form_fields();
        });

        $('.talent_btn').click(function() {
            $(this).addClass('active');
            $('.company-box').css("display","none");
            $('.company_btn').removeClass('active');
            $('#user_type').val('talent');
            clear_form_fields();
        });

        if($("#company_name").val()){
            $('.talent_btn').removeClass('active');
            $('.company_btn').addClass('active');
            $('.company-box').css('display','');
        }


        
//        $(document).on('keyup','#company_name',function(a){
//            var searching_value = $("#company_name").val();
//            $.ajax({
//              url: "<?= $vObj->getURL("register/get_existing_studios"); ?>",
//              type: 'POST',
//              cache: false,
//              dataType: 'JSON',
//              data:{
//                    searching_value : searching_value
//              },
//              timeout:8000,
//              async:false,
//              success : function(responseData){
//                    if(create_source_mapping(responseData)){
//                        $( "#company_name" ).autocomplete({
//                            source: source,
//                            select: function( event, ui ) {
//                                var selected_value = mapping[ui.item.value];
//                                $("#location_select").hide().removeClass('active');
//                                $.ajax({
//                                    url: "<?= $vObj->getURL("register/get_studios_locations"); ?>",
//                                    type: 'POST',
//                                    cache: false,
//                                    dataType: 'JSON',
//                                    data:{
//                                          selected_value : selected_value
//                                    },
//                                    timeout:8000,
//                                    async:false,
//                                    success : function(responseData){
//                                            $('#company_locations').empty();
//                                            var company_location_template_source = $("#company_location_template").html(); 
//                                            var company_location_template = Handlebars.compile(company_location_template_source); 
//                                            $('#company_locations').append(company_location_template(responseData));
//                                            $('#company_locations').fadeIn().addClass('active');
//                                            location_selection();
//                                    }
//                                });
//                            }
//                        });
//                    }
//                }
//            });
//        });

        $( "#company_name" ).autocomplete({
            source: function( request, response ) {
                
                if($('#company_locations').hasClass('active'))
                     $('#company_locations').hide().removeClass('active');
                if(!$('#company_locations').hasClass('active'))
                     $("#location_select").show().addClass('active');
                $("#parent_company_studio_id").val(0);
                $('#new_location').val('');
                var searching_value = request.term;
                $.ajax({
                    url: "<?= $vObj->getURL("register/get_existing_studios"); ?>",
                    dataType: "json",
                    type: 'POST',
                    data: {searching_value : searching_value},
                    success: function(data) {
                            response($.map(data, function(item) {
                            return {
                                label: item.label,
                                value: item.label,
                                id: item.value
                                };
                        }));
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var selected_value = ui.item.id;
                $("#parent_company_studio_id").val(selected_value);
                $("#location_select").hide().removeClass('active');
                $("#parent_id").val(selected_value);
                $.ajax({
                    url: "<?= $vObj->getURL("register/get_studios_locations"); ?>",
                    type: 'POST',
                    cache: false,
                    dataType: 'JSON',
                    data:{
                        selected_value : selected_value
                    },
                    timeout:8000,
                    async:false,
                    success : function(responseData){
                        console.log(responseData.locations.length);
                        $('#company_locations').empty();
                        var company_location_template_source = $("#company_location_template").html(); 
                        var company_location_template = Handlebars.compile(company_location_template_source); 
                        $('#company_locations').append(company_location_template(responseData));
                        $('#company_locations').fadeIn().addClass('active');
                        location_selection();
                    }
                });
            }
        });
        
        $('#country').select2({
            placeholder: 'Country'
        });
        $('#city').select2({
            placeholder: 'City'
        });
        
        <?php
        if(isset($is_company_active) && $is_company_active == true){
        ?>
        $('.company_btn').click();
        <?php
        }
        ?>
                
        <?php
        if(isset($is_company_invite_active) && $is_company_invite_active == true){
        ?>
            $('.company_btn').addClass('active');
            $('.company-box').css("display","");
            $('.talent_btn').removeClass('active');
            $('#user_type').val('company');
        <?php
        }
        ?>        
    });
    
function getcities(country_id) {
    var parent_company_id = $("#parent_company_studio_id").val();
    var data = "country_id=" + country_id+'&parent_company_id='+parent_company_id+'&source=register';
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("register/getcities")?>',
        dataType: "json",
        success: function (response) {
            $('#city').select2('destroy');
            $('#citybox').html(response.dropdown);
            $('#citybox #city').select2();
        }
    });
}

function clear_form_fields(){
    $('#company_name').css({border: ''}).val("");
    $('#job_title').css({border: ''}).val("");
    $('#s2id_country').css({border: ''});
    $('#s2id_city').css({border: ''});
   
    $('#user_firstname').css({border: ''}).val("");
    $('#user_lastname').css({border: ''}).val("");
    $('#user_name').css({border: ''}).val("");
    $('#user_email').css({border: ''}).val("");
    $('#user_password').css({border: ''}).val("");
}

//function create_source_mapping(raw){
//    source  = [ ];
//    mapping = { };
//    for(var i = 0; i < raw.length; ++i) {
//        source.push(raw[i].label);
//        mapping[raw[i].label] = raw[i].value;
//    }
//    return true;
//}

function location_selection(){
    $('.select_location').unbind('click').click(function(){
       if($(this).hasClass('active')){
           return false
       }else{
            $('.select_location').prop('checked',false).removeClass('active');
            $(this).prop('checked',true).addClass('active');
            var id_arr = $(this).attr('id').split('_');
            var city_country = id_arr[1]+'_'+id_arr[2];
            if($(this).hasClass('select_location_new')){
                $("#location_select").show().addClass('active');
                $('#new_location').val('new'); 
            }else{
                $('#new_location').val(city_country); 
                $("#location_select").hide().removeClass('active');
            }
       }
    });
}
</script>

