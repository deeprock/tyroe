<?php
$class = '';
if($job_detail['archived_status'] == 1){
    $class = 'disabled';
}

?>

<script type="text/javascript">
var msg_to = '';
var total_person = <?php echo count($get_shortlist); ?>;
$(document).ready(function () {
    $('.job_backicon').hide();
    $('#myModal_shortlisted').modal('hide');

});
$(document).on("click", ".btn-msg", function () {
    $(".sugg-msg").html("");
    msg_to = $(this).attr("data-value");
    $("#hd_email_to" + msg_to).val(msg_to);
    $('#myModal_shortlisted').modal('show');
    $('.modal-body').css('height', 146);
});
//Suggestion meesage Sending
$(document).on("click", ".btn-send-email", function () {
    if ($('#email-subject').val() == "") {
        $('#email-subject').css({
            border: '1px solid red'
        });
        $('#email-subject').focus();
        return false;
    }
    if ($('#email-subject').val() != "") {
        $('#email-subject').css({
            border: ''
        });
    }
    if ($('.email-message').val() == "") {
        $('.email-message').css({
            border: '1px solid red'
        });
        $('.email-message').focus();
        return false;
    }

    if ($('.email-message').val() != "") {
        $('.email-message').css({
            border: ''
        });
    }
    //else{
    $(this).text('Sending..');
    $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
    $(this).removeClass('btn-send-email');
    $(this).addClass('add_email_dummyclass');

    var email_message = $(".email-message").val();
    var email_subject = $("#email-subject").val();
    var email_to = msg_to;
    var data = "callfrom=send_email&email_message=" + email_message + "&email_subject=" + email_subject + "&email_to=" + email_to;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/access_module");?>',
        dataType: 'json',
        success: function (responseData) {
            if (responseData.success) {
                $(".sugg-msg").show();
                $(".sugg-msg").html("Your message send success.");
                $('.add_email_dummyclass').text('Send');
                $('.add_email_dummyclass').addClass('btn-send-email');
                $('.btn-send-email').removeClass('add_email_dummyclass').hide();
                $('.small_loader').remove();
                $('.email_close').show();
            }
        }
    });
});
function rescale() {
    var size = {width: $(window).width(), height: $(window).height() }
    var offset = 20;
    var offsetBody = 240;
    //$('#myModal').css('height', size.height - offset );
    $('.modal-body').css('height', size.height - (offset + offsetBody));
    $('#myModal_shortlisted').css('top', '3%');
}

$(window).bind("resize", rescale);
/*Add in openings*/
$(document).on("click", ".add_in_opening_jobs", function () {
    /*First Param =  JOB ID*/
    /*Second Param =  Tyroe ID*/
    $('.loader-save-holder').show();
    var job_tyroe_id = $(this).data('value');
    var job_tyroe_arr = (job_tyroe_id).split(",");
    var job_id = job_tyroe_arr[0];
    var tyroe_id = job_tyroe_arr[1];
    $(this).addClass('disable_opening');
    $(this).removeClass('add_in_opening_jobs');
    $.ajax({
        type: "POST",
        url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
        dataType: "json",
        data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'candidates'},
        success: function (data) {
            if (data.success) {
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
            $('.loader-save-holder').hide();
        },
        failure: function (errMsg) {
        }
    });
});
/*Add in openings*/

/*Add profile in favourite*/
$(document).on("click", ".fav_profile", function favouriteCandidate() {
    $('.loader-save-holder').show();
    var uid = $(this).data('value');
    var data = "uid=" + uid;
    var current_btn = this;
    var current_star = $(this).find("i");
    if ($(current_btn).hasClass('star_btn')) {
        $(current_btn).css("background", "#2388d6");
        $(current_star).css("color", "#ffffff");
        $(current_btn).removeClass('star_btn')
    } else {
        $(current_btn).css("background", "#ffffff");
        $(current_star).css("color", "#2388d6");
        $(current_btn).addClass('star_btn')
        $(this).blur();
    }
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(current_btn).css("background", "#2388d6");
                $(current_star).css("color", "#ffffff");
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(current_btn).css("background", "");
                $(current_star).css("color", "");
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
            $('.loader-save-holder').hide();
        },
        failure: function (errMsg) {
        }
    });
});
/*Add profile in favourite*/

//  shortlist to candidate

var total_shortlisted = '<?php echo count($get_shortlist); ?>';

var emails = [];
$(document).on('click', '.bulk_email_tyroes', function () {

    $('.bulk_emails').each(function (i) {
        emails[i] = $(this).val();
    })
    $(".bulksugg-msg").html("");
    $('#myModal_bulk').modal('show');
    rescale();
    $('.modal-body').css('height', 146);
});
$(document).on("click", ".btn_send_bulk_email", function () {
    if ($('#bulkemail-subject').val() == "") {
        $('#bulkemail-subject').css({
            border: '1px solid red'
        });
        $('#bulkemail-subject').focus();
            return false;
    }
    if ($('#bulkemail-subject').val() != "") {
        $('#bulkemail-subject').css({
            border: ''
        });
    }
    if ($('.bulkemail-message').val() == "") {
        $('.bulkemail-message').css({
            border: '1px solid red'
        });
        $('.bulkemail-message').focus();
        return false;
    }
    if ($('.bulkemail-message').val() != "") {
        $('.bulkemail-message').css({
            border: ''
        });
    }
    //else{
    $(this).text('Sending..');
    $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
    $(this).removeClass('btn_send_bulk_email');
    $(this).addClass('add_email_dummyclass');
    var email_message = $(".bulkemail-message").val();
    var email_subject = $("#bulkemail-subject").val();
    var data = "callfrom=send_email&sending_action=bulk_email&email_message=" + email_message + "&email_subject=" + email_subject + "&emails=" + emails;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/access_module");?>',
        dataType: 'json',
        success: function (responseData) {
            if (responseData.success) {
                $(".sugg-msg").show();
                $(".sugg-msg").html("Your message send success.");
                $('.add_email_dummyclass').text('Send');
                $('.add_email_dummyclass').addClass('btn_send_bulk_email');
                $('.btn_send_bulk_email').removeClass('add_email_dummyclass').hide();
                $('.small_loader').remove();
                $('.email_close').show();
            }
        }
    });
    //return true;

    //}
});

var jquery_total_page = $('.filter_editor .jquery_pagination').size();
var start_pages = 1;
if (jquery_total_page > 5) {
    var end_pages = 5;
} else {
    end_pages = jquery_total_page;
}

//create pages
var jquery_pagination_html = '';
var page_number = 1;
var pre_page = 0;
pagination_genrator();
</script>
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<?php
//$this->load->view('left_coloumn');
?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span12 ">
<!--Old Div-->
<!--This area for single layout pagination purpose-->
<div class="single_person_pagination">
    <?php
    if ($page_number > 1) {
        $pos_id = ($page_number - 1) * 12;
    } else {
        $pos_id = 0;
    }
    if($get_candidates != ''){
    for ($tyr = $pos_id; $tyr < count($get_shortlist); $tyr++) {

        ?>
        <input type="hidden" id="user_unique_id<?php echo $pos_id + 1; ?>"
               class="users_ids shortlist_id<?= $get_shortlist[$tyr]['user_id'] ?>" data-id="<?php echo $pos_id + 1; ?>"
               value="<?= $get_shortlist[$tyr]['user_id'] ?>"/>
        <?php $pos_id++;
    }} ?>
</div>
<!--End single layout pagination purpose-->
<div class="right-column filter_editor" style="position:relative; padding: 0">
<?php if ($get_shortlist != '') { ?>
    <div class="row-fluid header sep-hd">
        <div class="span12">
            <div class="alert alert-success alert-rt-sec">
                <p class="rt-heading-alert">Looking good!</p>
                <a class="cross-alert" href="javascript:void(0);">X</a>

                <div class="clearfix"></div>
                <p class="rt-text-alert">You have shortlisted some impressive Tyroe's. It's now time to arrange interviews and select the perfect person for this job. Below are some options available for you to get things moving.</p>
            </div>
        </div>
        <div class="clearfix"></div>
        <?php if (intval($job_role['is_admin']) == 1) { ?>
            <div class="span8 st-btn-sec">
                <button class="pull-left btn btn-primary bulk_email_tyroes  rt-green"<?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> >BULK EMAIL TYROE'S</button>
<!--                <button class="pull-left  btn btn-primary generate_pdf rt-green" data-value="<?= $job_id ?>" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?>>PRINT PDF
                    SUMMARY
                </button>-->
                <form class="pull-left"  action="<?= $vObj->getURL("openingoverview/get_csvfile"); ?>" method="post">
                    <input type="hidden" value="<?= $job_id ?>" name="job_id">
                    <input type="submit" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="btn btn-primary rt-green input-gr" value="EXPORT CSV"/>
                </form>
                <?php if($get_jobs['archived_status'] == 0){ ?>
                <button class="pull-left  btn btn-default add_to_archive rt-white" data-value="<?= $job_id ?>">ARCHIVE
                    THIS JOB
                </button>
                <?php } ?>
            </div>
        <?php } ?>
    </div>
    <div class="jquery_pagination">
    <?php
    if ($page_number > 1) {
        $pos_id = ($page_number - 1) * 12;
    } else {
        $pos_id = 1;
    }
    $jquery_page_items_count = 0;
    for ($tyr = 0; $tyr < count($get_shortlist); $tyr++) {
        
        $inv_bg_class = '';
        $inv_status_layer = 0;
        $invite_tooltip = 0;
        if($get_shortlist[$tyr]['invitation_status'] == 0){
            $inv_bg_class = 'invite-status-bg';
            $invite_tooltip = 1;
        }else if($get_shortlist[$tyr]['invitation_status'] == 1){
            $inv_status_layer = 1; 
        }else if($get_shortlist[$tyr]['invitation_status'] == -1){
            $inv_status_layer = 1; 
        }  
        
        $main_user_id = $get_shortlist[$tyr]['user_id'];
        $image;
        if ($get_shortlist[$tyr]['media_name'] != "") {
            $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_shortlist[$tyr]['media_name'];
        } else {
            $image = ASSETS_PATH_NO_IMAGE;
        }

        if ($jquery_page_items_count == 12) {
            $jquery_page_items_count = 0;
            ?></div><div class="jquery_pagination"   style="display: none"><?php
        }

        ?>
        <input type="hidden" value="<?= $get_shortlist[$tyr]['email'] ?>" class="bulk_emails">
        <div class="row-fluid rg-bg hiderow<?= $get_shortlist[$tyr]['job_detail_id'] ?>">
            <div class="container <?=$inv_bg_class?>" style="position: relative;" id="job_user_<?=$get_candidates[$tyr]['user_id']?>">
                <div class="span12 editor-pck row-id<?= $get_shortlist[$tyr]['user_id'] ?>">
                    <div class="row-fluid">
                        <div class="span12" id="top">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="edt_pck_sec">
                                        <div class="user-dp-holder">
                                            <a href="javascript:void(0)"
                                               onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_shortlist[$tyr]['user_id']; ?>)" style="position: relative;display: inline-block;">
                                                <img src="<?php echo $image; ?>" class="img-circle avatar-80">
                                                 <?php
                    if (intval($get_shortlist[$tyr]['availability']) == 1) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#96bf48" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE FOR WORK"></i>
                    <?php
                    } else if (intval($get_shortlist[$tyr]['availability']) == 2) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#5ba0a3" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE SOON"></i>
                    <?php
                    } else if (intval($get_shortlist[$tyr]['availability']) == 3) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i>
                    <?php
                    }else{
                    ?>
                       <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i> 
                    <?php    
                    }
                    ?>
<!--                                                <div class="span7 pull-right text-center hidden-phone round-box">
                                                    <?php if ($get_candidates[$tyr]['invitation_status'] == 1) { ?>
                                                        <i class="icon-circle icon-large under-avatar-status"
                                                           id="green"></i>
                                                    <?php } else { ?>
                                                        <i class="icon-circle icon-large under-avatar-status"
                                                           id="yellow"></i>
                                                    <?php } ?>
                                                </div>-->
                                            </a>
                                        </div>
                                        <div class="editors_picked" id="editors_pick">
                                            <div class="editors_pick-info" id="editors_pick_info">
                                                <!--<a href="javascript:void(0);" class="name" value="<?php /*=$main_user_id*/?>"><?php /*echo $get_shortlist[$tyr]['username']; */?></a>-->
                                                <a href="javascript:void(0)"
                                                   onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_shortlist[$tyr]['user_id']; ?>)"
                                                   class="name"><?php echo $get_shortlist[$tyr]['firstname'] . " " . $get_shortlist[$tyr]['lastname']; ?></a>
                                                        <span class="location"><?php
                                                            /*for ($a = 0; $a < count($get_shortlist[$tyr]['skills']); $a++) {
                                                                $temp = $get_shortlist[$tyr]['skills'][$a]['creative_skill'];
                                                                echo ($a > 0)?", ":"";
                                                                if(!empty($temp)){
                                                                    echo $temp;
                                                                }
                                                            }*/
                                                            echo rtrim($get_shortlist[$tyr]['industry_name'] . ', ' . $get_shortlist[$tyr]['extra_title'], ', ');
                                                            ?></span>
                                                <span
                                                    class="tags"><?php echo $get_shortlist[$tyr]['city'] . ", " . $get_shortlist[$tyr]['country']; ?></span>
                                                <br clear="all">
                                                <?php if (intval($job_role['is_reviewer']) == 1) { ?>
<!--                                                    <div class="rt-dec-btn">
                                                        <a class="btn-flat success review_tyroe"
                                                           data-value="<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?>"
                                                           data-id="<?php echo $get_candidates[$tyr]['user_id']; ?>">Review</a>
                                                        <a class="btn-flat inverse decline_tyroe"
                                                           data-id="<?php echo $get_candidates[$tyr]['user_id']; ?>"
                                                           data-name="<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?>">Decline</a>
                                                    </div>-->
                                                <?php } ?>
<!--                                                <div class="btn-group large">
                                                    <?php if (intval($job_role['is_admin']) == 1) { ?>
                                                        <button class="glow left" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> data-toggle="dropdown"><i
                                                                class="icon-plus-sign"></i></button>
                                                        <ul class="dropdown-menu">

                                                            <li><a href="javascript:void(0)" class=""
                                                                   style="color:#999999"><?= "Add to opening:"; ?></a>
                                                            </li>
                                                            <?php
                                                                if($get_openings_dropdown != ''){
                                                                    for ($op = 0; $op < count($get_openings_dropdown); $op++) { ?>
                                                                <?php $duplicate_job = 0;
                                                                foreach ($get_shortlist[$tyr]['tyroe_jobs'] as $tyroe_job) {
                                                                    ?>
                                                                    <?php if ($tyroe_job['job_id'] == $get_openings_dropdown[$op]['job_id']) {
                                                                        $duplicate_job = 1;
                                                                        ?>
                                                                        <li><a href="javascript:void(0)"
                                                                               class="disable_opening"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                        </li>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <?php if ($duplicate_job == 0) { ?>
                                                                    <li><a href="javascript:void(0)"
                                                                           class="add_in_opening_jobs"
                                                                           data-value="<?= $get_openings_dropdown[$op]['job_id'] . "," . $get_shortlist[$tyr]['user_id']; ?>"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } }$get_openings_dropdown ?>
                                                        </ul>
                                                    <?php } ?>
                                                    <?= $get_shortlist[$tyr]['is_favourite'] ?>
                                                    <?php if ($get_shortlist[$tyr]['is_favourite'] == '') { ?>
                                                        <button <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="glow middle fav_profile star_btn"
                                                                data-value="<?= $get_shortlist[$tyr]['user_id'] ?>"><i
                                                                class="icon-star"></i></button>
                                                    <?php } else { ?>
                                                        <button <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="glow middle fav_profile"
                                                                style="background: #2388d6;"
                                                                data-value="<?= $get_shortlist[$tyr]['user_id'] ?>"><i
                                                                class="icon-star" style="color:#ffffff"></i></button>
                                                    <?php } ?>
                                                    <?php if (intval($job_role['is_admin']) == 1) { ?>
                                                        <button <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="glow right btn-msg"
                                                                data-value="<?= $get_shortlist[$tyr]['user_id'] ?>"><i
                                                                class="icon-envelope "></i></button>
                                                    <?php } ?>
                                                    <input type="hidden" name="hd_email_to"
                                                           id="hd_email_to<?= $get_shortlist[$tyr]['user_id'] ?>"
                                                           value="<?= $get_shortlist[$tyr]['user_id'] ?>"/>

                                                </div>-->

                                            </div>
                                            <div class="editors_showcase-container" id="editors-showcase">
                                                <div class="editors_showcase_holder">
                                                    <div
                                                        class="full-profile_overlay black_overlay_separate<?= $get_shortlist[$tyr]['user_id'] ?>"
                                                        onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_shortlist[$tyr]['user_id']; ?>)">
                                                        <div class="full-profile_overlay_holder">
                                                            <p>View Full Profile</p>
                                                            <i class="icon-picture"></i>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <?php
                                                        for ($a = 0; $a < 3; $a++) {
                                                            if ($get_shortlist[$tyr]['portfolio'][$a]['media_name']) {
                                                                $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_shortlist[$tyr]['portfolio'][$a]['media_name'];
                                                                if (!file_exists($portfolio)) {
                                                                    $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                } else {
                                                                    $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_shortlist[$tyr]['portfolio'][$a]['media_name'];
                                                                }
                                                            } else {
                                                                $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                            }
                                                            ?>
                                                            <div class="showcase_thumb pull-right">
                                                                <a href="javascript:void(0);"
                                                                   class="score_img_anchor<?= $a ?>"
                                                                   data-id="<?= $get_shortlist[$tyr]['user_id'] ?>">
                                                                    <?= (($a == 2) && ($get_shortlist[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                    <?php if ($a == 0) { ?>
                                                                        <?php if ($get_shortlist[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                            <div
                                                                                class="tyroe_score_over_img tyroeScoreHolder<?= $get_shortlist[$tyr]['user_id'] ?>">
                                                                                <div
                                                                                    class="tyroe_score_holder"><?= $get_shortlist[$tyr]['get_tyroes_total_score'] ?></div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                    <img src="<?php echo $portfolio ?>"
                                                                         alt="editor-picks-<?= $get_shortlist[$tyr]['is_user_featured'] ?>">
                                                                </a>
                                                            </div>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php if (intval($job_role['is_admin']) == 1) { ?>
                                                    <div class="span12 add_to_shortlist_controls margin-left-default cd-btn-sp margin-top-2">
                                                        <a <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> href="javascript:void(0)"
                                                           class="btn btn-flat success span3 pull-right add_candidate_from_shortlist"
                                                           data-id="<?= $get_shortlist[$tyr]['user_id'] ?>"
                                                           data-value="<?= $get_shortlist[$tyr]['job_detail_id'] ?>" <?= $class ?>>+
                                                            Candidate</a>
                                                        <a href="javascript:void(0)"
                                                           class="reviews_toggle span7 pull-left btn-default btn ar-down rt-feed-btn"
                                                           data-id="<?= $get_shortlist[$tyr]['job_detail_id'] ?>"
                                                           data-value="<?= $get_shortlist[$tyr]['user_id'] ?>">View
                                                            feedback and reviews </a>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                <?php } ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                    </div>
                                    <?php if (intval($job_role['is_admin']) == 1) { ?>

                                                                                <div
                                                                                    class="reviews_box<?php echo $get_shortlist[$tyr]['job_detail_id']; ?> span12"
                                                                                    style="display: none;margin: 40px auto 0 auto;"></div>
                                                                            <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if($invite_tooltip == 1){
                ?>
                <span class="invite-status-suspended" alt="zxczczxc" data-toggle="tooltip" data-placement="left" title="Tyroe has not indicated if they are intrested in this job yet."><i class="icon-question-sign"></i></span>
                <?php
                }
                ?> 
            </div>
            <div class="clearfix"></div>
            <div class="editor-seperator editor-seperator-row<?= $get_shortlist[$tyr]['user_id'] ?>"></div>
        </div>
        <?php
        $jquery_page_items_count++;
        $pos_id++;
    }
    ?>
    </div>
    <div class="span11 pagination  jquery_p" style="display:none;margin-left: 0px;">
        <ul class="pull-right jquery_pagination_container"></ul>
    </div>
<?php } else { ?>
    <div class="container">
        <div class="col-md-12">
            <div class="alert alert-warning error_adjsut">
                <i class="icon-warning-sign"></i>
                <span>No shortlist found</span>
            </div>
        </div>
    </div>
<?php } ?>
</div>
</div>
</div>

<!-- end main container -->
<div style="display: none;" class="modal fade" id="myModal_shortlisted" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
             <div class="global-popup_details">
                <div class="global-popup_holder global-popup ">
                    <div class="modal-header job_create_header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Send Mail</h4>

                    </div>
                    <div class="span8 email_content_body">
                        <div class="global-popup_body ">
                            <div class="center prof-dtl">
                                <div class="tab-margin">
                                   <div class="sugg-msg alert alert-info" style="display: none"></div>
                                   <div class="clearfix"></div>
                                   <div class="field-box">
                                       <input type="text" name="email-subject" id="email-subject" placeholder="Subject"/>
                                   </div>
                                   <div class="field-box">
                                        <textarea name="" id="" class="email-message" placeholder="Message"></textarea>
                                   </div>
                               </div>
                           </div>
                        </div>
                        <div class="global-popup_footer">
                            <button type="button" class="btn btn-flat white email_close"  style="display: none" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-flat btn-save btn-send-email" value="<?= LABEL_SUBMIT ?>">
                                <?= LABEL_SUBMIT ?>
                            </button>
                       </div>
                   </div>
                   <div class="clearfix"></div>
               </div>
           </div>
        </div>
</div>
<!--For Bulk Emails-->
<div style="display: none;" class="modal fade" id="myModal_bulk" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>
                </div>
                <div class="padding-35px global-popup_body">
                    <div class="center prof-dtl">
                        <div class="tab-margin">
                            <div class="sugg-msg alert alert-info" style="display: none"></div>
                            <div class="field-box">
                                <input type="text"  name="email-subject" id="bulkemail-subject" placeholder="Subject"/>
                            </div>
                            <div class="field-box">
                                <textarea name="" id="" class="bulkemail-message" placeholder="Message"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer job_create_footer">
                    <button type="button" class="btn btn-flat white email_close"  style="display: none" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-flat btn-save btn_send_bulk_email" value="<?= LABEL_SUBMIT ?>">
                        <?= LABEL_SUBMIT ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>