<!DOCTYPE html>
<html class="login-bg">
<head>
<title>Tyroe Jobs - <?=$job_detail['job_title'].' | '.$job_location?></title>

<meta http-equiv="cache-control" content="max-age=0"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/>
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
<meta http-equiv="pragma" content="no-cache"/>

<?php
    $keywords = array();
    foreach($skills as $val){
        $keywords[] = $val['creative_skill'];
    }
    $description = strip_tags(substr($job_company_detail['job_description'], 0, 255));
    
    if($job_company_detail['logo_image'] !=''){
        $logo_url = LIVE_SITE_URL.'assets/uploads/company_logo/'.$job_company_detail['logo_image'];
    }else{
        $logo_url = LIVE_SITE_URL.'assets/img/default-avatar.png';
    }
?>

<meta name="description" content="<?=$description?>">
<meta name="keywords" content="<?=implode(',',$keywords).','.trim($job_location)?>">
<meta name="author" content="">
<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">-->


<!-- for Facebook -->          
<meta property="og:title" content="Tyroe Jobs - <?=$job_detail['job_title'].' | '.trim($job_location)?>" />
<meta property="og:type" content="Tyroe Jobs" />
<meta property="og:image" content="<?=$logo_url?>" />
<meta property="og:url" content="<?=LIVE_SITE_URL.'jobs/'.$job_url_title?>" />
<meta property="og:description" content="<?=$description?>" />
<meta property="og:site_name" content="<?=LIVE_SITE_URL?>" />

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>
<!-- bootstrap -->
<link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap.css" rel="stylesheet">
<link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-responsive.css" rel="stylesheet">
<link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet">

<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/layout.css">
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/elements.css">
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/icons.css">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />

<!-- libraries -->
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/lib/font-awesome.css">
<!-- this page specific styles -->
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/signin.css" type="text/css" media="screen"/>
<!-- open sans font -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/custom.css">
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/public_job.css">

</head>
<body>
<!-- main container -->
<div class="row-fluid public-job-container">
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <div class="brand-logo">
                <a class="brand" href="<?= LIVE_SITE_URL ?>"><img class="logo-main" src="<?= ASSETS_PATH ?>img/logo_tyroe.png"></a>
            </div>
        </div>
    </div>
</div>
<div class="job-container">
    <div class="job-content">
        <div class="left-col">
                <div class="company-logo-holder" style="width: 145px;height: 145px;margin: auto;">
                <?php 
                if($company_detail['logo_image'] !=''){

                    $w = '';
                    $h = '';
                    $margin_top = '';
                    $margin_left = '';

                    $logo_url = LIVE_SITE_URL.'assets/uploads/company_logo/'.$company_detail['logo_image'];
                    list($width, $height, $type, $attr) = getimagesize($logo_url);           
                    $maxWidth = intval(145);
                    $maxHeight = intval(145);
                    $final_height = '';
                    $final_width = '';

                    if(width > height){
                        $final_width = $maxWidth;
                        $final_height = ($height/$width)*$final_width;
                        if($final_height < $maxHeight){
                            $margin_top = (intval($maxHeight) - intval($final_height))/2;
                        }

                    }

                    if($height > $width){
                        $final_height = $maxHeight;
                        $final_width = $final_height*($width/$height);

                        if($final_width < $maxWidth){
                            $margin_left = (intval($maxWidth) - intval($final_width))/2;
                        }

                        if($margin_left <= 0){
                            $margin_left = 0;
                        }

                    }

                    if($width < $maxWidth && $height < $maxHeight){
                        $final_height = $height;
                        $final_width = $width;
                        $margin_left = (intval(145) - intval($final_width))/2;
                        $margin_top = (intval(145) - intval($final_height))/2;
                        if($margin_left <= 0){
                            $margin_left = 0;
                        }

                        if($margin_top <= 0){
                            $margin_top = 0;
                        }

                    }

                    $w = $final_width;
                    $h = $final_height;    

                ?>
                <img class="company-logo" src="<?=$logo_url?>" style="height:<?=$h?>px;width:<?=$w?>px;margin-left:<?=$margin_left?>px;margin-top:<?=$margin_top?>px"/>
                <?php
                }else{
                ?>
                <img class="company-logo" src="<?=LIVE_SITE_URL.'assets/img/default-avatar.png'?>"/>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="right-col">
            <div class="text-row company_name"><?=$company_detail['company_name']?></div>
            <div class="text-row job_title"><?=$job_detail['job_title']?></div>
            <div class="text-row location"><?=$job_location?></div>
            <div class="text-row description"><?=$job_detail['job_description']?></div>
            <div class="text-row data">
                <div class="data-field-1"><i class="icon-calendar"></i>&nbsp;&nbsp;<?=$start_date?></div>
                <div class="data-field-2"><i class="icon-time"></i>&nbsp;&nbsp;<?=$time_remaining?></div>
                <div class="data-field-3"><i class="icon-legal"></i>&nbsp;&nbsp;<?php
                if($job_detail['job_type'] == 1)
                    echo "Casual";
                if($job_detail['job_type'] == 2)
                    echo "Part-time";
                if($job_detail['job_type'] == 3)
                    echo "Full-time";
                ?>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="text-row dt-label skills">
                <?php
                foreach($skills as $val){
                ?>
                <span class="label skill"><?=$val['creative_skill']?></span>
                <?php
                }
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="job-footer">
        <a href="<?=LIVE_SITE_URL.'jobview/'.$job_url_title?>" class="btn-flat primary apply-btn">Apply with <strong>Tyroe</strong></a>
    </div>
</div>
</body>
</html>