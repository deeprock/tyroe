<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.min.js"></script>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/signin.css" type="text/css" media="screen"/>
<script type="text/javascript">
    function valid_login() {
       var user_email = $("#user_email").val();
        if (user_email != "") {
            var data = "user_email=" + user_email;
            var live_url = '<?=$vObj->getURL("login/PasswordRequest")?>';
            $.ajax({
                type: "POST",
                data: data,
                url: live_url,
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        //window.location = live_url+"home";
                        $("#user_email").hide();
                        $(".btn-glow").hide();
                        $("#error_msg").html(data.success_message);
                        $("#back-btn").show();
                    }
                    else {
                        $("#error_msg").html(data.success_message_error);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
        else {

            $("#error_msg").html("Please Provide Valid Email");
        }

    }

    function goBack(){
        window.history.back();
    }

</script>

<!-- main container -->


<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img class="logo" src="<?= ASSETS_PATH ?>img/logo-dark.png">
    </a>

    <div class="span4 box">
        <div class="content-wrap">
            <h6><?= LABEL_PASSWORD_FORGOT ?></h6>
            <span id="error_msg"></span><br>
            <input class="span12" type="email" name="user_email"
                   id="user_email" placeholder="<?= LABEL_EMAIL ?>">

            <a class="btn-glow primary login" onclick="valid_login();"><?= LABEL_SUBMIT ?></a>
            <a class="btn-glow primary login" onclick="goBack();" id="back-btn"><?= LABEL_BACK ?></a>
        </div>
    </div>

    <div class="span4 no-account">
        <p><?= LABEL_NO_ACCOUNT ?></p>
        <a href="<?= $vObj->getURL('register') ?>"><?= LABEL_SIGNUP ?></a>
        <br>
        <p><?= LABEL_ALREADY_ACCOUNT ?></p>
        <a href="<?= $vObj->getURL('login') ?>"><?= LABEL_SIGNIN ?></a>
    </div>
</div>