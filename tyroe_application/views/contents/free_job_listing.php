<?php if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>

<script type="text/javascript">
    $(document).ready(function () {

        init_read_more_job_description();

        $(document).on("click", ".btn_process", function () {
//            var row_id = $(this).closest('.openings-container').find('.row_id').val();
            var row_id = $(this).attr('value');
            $('.loader-save-holder').show();
            if ($(this).text() == "No") {
                $(".job-opening" + row_id).prepend('<div class="disabled-opening"><a class="btn-flat danger pull-right btn_process">Declined</a></div>');
                $(".job-opening" + row_id).find(".opening-alert").html('');
                var data = "job_id=" + row_id;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?= $vObj->getURL("jobopening/RejectInvitation") ?>',
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('.loader-save-holder').hide();
                    }
                });
            } else if ($(this).text() == "Yes") {
                $("<div class=\"pull-left span12 opening-review-container\"><a class=\"btn-flat white\">In review</a><a class=\"btn-flat primary\">you are a candidate</a></div>").insertAfter($(this).closest('.openings-container').find('.basic-detail'));
                $(this).closest('.alert-info').html('<i class="icon-ok-sign"></i> I\'m interested in this job. Please consider my profile.').addClass('alert-success').removeClass('alert-info');
                var data = "job_id=" + row_id;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?= $vObj->getURL("jobopening/AcceptInvitation") ?>',
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        $('.loader-save-holder').hide();
                    }
                });
            }
            else if ($(this).text() == "Declined") {
                alert(row_id);
            }
        });

        /*Hire Me - Start*/
        $(document).on("click", ".hire-me", function () {
            //This function belongs to hire me
            $('.loader-save-holder').show();
            var job_id = $(this).parent().parent().find(".row_id").val();
            var studio_reviewer_id = $(this).closest('.openings-container').find('.row_id').attr("data-studio-reviewer");
            var data = "job_id=" + job_id + "&studio_reviewer_id=" + studio_reviewer_id;
            var obj = $(this);
            $.ajax({
                type: "POST",
                data: data,
                url: '<?= $vObj->getURL("jobopening/JobApply") ?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(obj).closest('.openings-container').find('.applicant-bar').css('display', '');
                        $(obj).closest('.openings-container').find('.hire-me').remove();
                    }
                    $(".hirejobs_message").html('<i class="icon-exclamation-sign"></i>' + data.success_message);
                    $("#hire-notify").css("top", "0");
                    $("#hire-notify").css("display", "");
                    setTimeout(function () {
                        $("#hire-notify").css('display', 'none');
                    }, 5000);
                    $('.loader-save-holder').hide();
                }
            });
        });
        /*Hire Me - End*/
    });

    function init_read_more_job_description() {
        $(".jQueryClassReadmoreParagraphFunc").each(function (index, value) {
            var getHeight = $(this).height();
            if (getHeight > 260) {
                $(this).after('<a href="javascript:void(0)" class="jQueryClassReadmoreFunc">Read More</a>');
                $(this).css({"height": '255px'});
            }
        });

        $(document).on('click', '.jQueryClassReadmoreFunc', function () {
            var main_content_obj = $(this).parent().children(".jQueryClassReadmoreParagraphFunc");
            var getHeight = main_content_obj.children('span').height();
            main_content_obj.animate({"height": getHeight + "px"});
            $(this).removeClass('jQueryClassReadmoreFunc');
            $(this).addClass('jQueryClassReadlessFunc');
            $(this).text('Read Less');
        });
        $(document).on('click', '.jQueryClassReadlessFunc', function () {
            var main_content_obj = $(this).parent().children(".jQueryClassReadmoreParagraphFunc");
            main_content_obj.animate({"height": "260px"});
            $(this).removeClass('jQueryClassReadlessFunc');
            $(this).addClass('jQueryClassReadmoreFunc');
            $(this).text('Read More');
        });
    }

    function DeleteOpening(job_id) {

        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?= $vObj->getURL("jobopening/DeleteOpening") ?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".job-opening" + job_id).hide();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }

    function JobApply(job_id, action) {
        var bool = "";
        if (action == "No") {
            action = "-1";
            bool = "invite";
        } else if (action == "Yes") {
            action = "1";
            bool = "invite";
        } else if (action == "Declined") {
            action = "0";
            bool = "invite";
        }
        var data = "job_id=" + job_id + "&action=" + action + "&bool=" + bool;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?= $vObj->getURL("jobopening/JobApply") ?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloum_job');
                    ?>
                    <!-- END LEFT COLUMN -->
                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column">
                            <div class="span9 default-header">
                                <h4><?= LABEL_MODULE_OPENINGS ?></h4>
                            </div>
                            <div class="span3" id="job-filter-dropdown">
                                <div class="ui-select find">
                                    <select class="span5 inline-input" name="orderby"
                                            id="orderby" onchange="FilterJobs();">
                                        <option value="">sort</option>
                                        <option value="asc">Ascending</option>
                                        <option value="desc">Descending</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- START OPENING DETAILS -->
                            <div class="filter-result"></div>
                            <?php
                            if (is_array($job_detail)) {
                                //foreach ($job_detail as $jobs) {
                                for ($ind = 0; $ind < count($job_detail); $ind++) {
                                    $logo_url = $job_detail[$ind]['company_logo'];

                                    $w = '';
                                    $h = '';
                                    $margin_top = '';
                                    $margin_left = '';
                                    if ($logo_url != '') {
                                        $logo_url = UPLOAD_PATH . 'company_logo/' . $job_detail[$ind]['company_logo'];
                                        list($width, $height, $type, $attr) = getimagesize($logo_url);
                                        $maxWidth = intval(86);
                                        $maxHeight = intval(86);
                                        $final_height = '';
                                        $final_width = '';

                                        if (width > height) {
                                            $final_width = $maxWidth;
                                            $final_height = ($height / $width) * $final_width;
                                            if ($final_height < $maxHeight) {
                                                $margin_top = (intval($maxHeight) - intval($final_height)) / 2;
                                            }
                                        }


                                        if ($height > $width) {
                                            $final_height = $maxHeight;
                                            $final_width = $final_height * ($width / $height);

                                            if ($final_width < $maxWidth) {
                                                $margin_left = (intval($maxWidth) - intval($final_width)) / 2;
                                            }

                                            if ($margin_left <= 0) {
                                                $margin_left = 0;
                                            }
                                        }

                                        if ($width < $maxWidth && $height < $maxHeight) {
                                            $final_height = $height;
                                            $final_width = $width;
                                            $margin_left = (intval(150) - intval($final_width)) / 2;
                                            $margin_top = (intval(150) - intval($final_height)) / 2;
                                            if ($margin_left <= 0) {
                                                $margin_left = 0;
                                            }

                                            if ($margin_top <= 0) {
                                                $margin_top = 0;
                                            }
                                        }


                                        $w = $final_width;
                                        $h = $final_height;
                                    } else {
                                        $logo_url = LIVE_SITE_URL . 'assets/img/default-avatar.png';
                                        $default = 'default';
                                    }
                                    ?>
                                    <div class="row-fluid no-filter job-opening<?php echo $job_detail[$ind]['job_id']; ?>">
                                    <?php
                                    #Invitaion job declined
                                    if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "-1") {
                                        ?>
                                            <div class="disabled-opening"><a class="btn-flat danger pull-right btn_process">Declined</a></div>
                                            <?php
                                        }
                                        ?>
                                        <div class="span12 group tab-margin" id="top">
                                            <!-- INFO -->
                                            <div class="row-fluid">
                                                <div class="logo-col" style="float: left;width: 86px;height: 86px;margin-right: 3%;background: #fff;">
                                                    <img class="company-logo-image <?= $default ?>" src="<?= $logo_url ?>" style="width:<?= $w ?>px;height:<?= $h ?>px;margin-top:<?= $margin_top ?>px;margin-left:<?= $margin_left ?>px">
                                                </div>
                                                <div class="openings-container" style="width: 80%;">
                                                    <input type="hidden" class="row_id" name="row_id"
                                                           value="<?= $job_detail[$ind]['job_id']; ?>" data-studio-reviewer="<?= $job_detail[$ind]['user_id']; ?>"/>

                                                    <div class="span8 basic-detail tab-margin">
                                                        <h2 class="opening-tittle"><?php echo $job_detail[$ind]['company_name']; ?></h2>

                                                        <h3 class="opening-post"><?php echo $job_detail[$ind]['job_title']; ?></h3>
                                                        <h4 class="opening-location"><?php echo $job_detail[$ind]['job_city'] . "," . $job_detail[$ind]['job_location']; ?></h4>
                                                    </div>
        <?php
        #Shortlisted job
        if ($job_detail[$ind]['job_status_id'] == "") {
            ?>
                                                        <div class="span4 tab-margin pull-right">
                                                            <a class="btn-flat success pull-right hire-me">Hire me</a>
                                                        </div>
                                                        <div class="pull-left span12 opening-review-container applicant-bar" style="display:none">
                                                            <a class="btn-flat applicant">you are an applicant</a>
                                                        </div>
        <?php } else if ($job_detail[$ind]['job_status_id'] == "3") { ?>
                                                        <div class="pull-left span12 opening-review-container">
                                                            <a class="btn-flat success">you have been shortlisted</a>
                                                        </div>
                                                        <?php
                                                    } #tyroe applied for this job or invited job is accepted
                                                    else if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "1") {
                                                        ?>
                                                        <div class="pull-left span12 opening-review-container">
                                                            <a class="btn-flat white">In review</a>
                                                            <a class="btn-flat primary">you are a candidate</a>
                                                        </div>
            <?php
        } else if ($job_detail[$ind]['job_status_id'] == "2") {
            ?>
                                                        <div class="pull-left span12 opening-review-container applicant-bar">
                                                            <a class="btn-flat applicant">you are an applicant</a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>
                                                    <div style="margin-bottom: 10px">
                                                        <p class="opening-detail jQueryClassReadmoreParagraphFunc" style="overflow: hidden;float: none;line-height: 20px;"><span><?php echo $job_detail[$ind]['job_description']; ?></span></p>
                                                    </div>

                                                    <div class="clearfix"></div>
                                                    <div class="span12 tab-margin pull-left">
                                                        <div class="span4 tab-margin pull-left"><span
                                                                class="opening-date"><i
                                                                    class="icon-calendar"></i> <?php echo date("dS F, Y", $job_detail[$ind]['start_date']); ?> </span>
                                                        </div>
                                                        <div class="span4 tab-margin pull-left"><span
                                                                class="opening-month"><i
                                                                    class="icon-time"></i> <?php echo $job_detail[$ind]['duration']; ?></span>
                                                        </div>
                                                        <div class="span4 tab-margin pull-left"><span
                                                                class="opening-time"><i
                                                                    class="icon-legal"></i> <?php echo $job_detail[$ind]['job_type']; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="span12 tab-margin pull-left">
                                                        <ul class="opening-skils-requirements">
        <?php
        for ($a = 0; $a < count($job_detail[$ind]['skills']); $a++) {
            if ($job_detail[$ind]['skills'][$a]['creative_skill']) {
                ?>
                                                                    <li>
                                                                        <span><?php echo $job_detail[$ind]['skills'][$a]['creative_skill']; ?></span>
                                                                    </li>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>

                                                </div>
        <?php
        #Invitaion for job
        if ($job_detail[$ind]['job_status_id'] == "1" && $job_detail[$ind]['invitation_status'] == "0") {
            ?>
                                                    <div class="span12 pull-left tab-margin opening-alert">
                                                        <div class="alert alert-info">
                                                            <i class="icon-exclamation-sign"></i>
                                                            You are a candidate for this job. Are you interested?
                                                            <div class="pull-right alert-ops">
                                                                <a class="btn-flat danger btn_process" value="<?= $job_detail[$ind]['job_id']; ?>">No</a>
                                                                <a class="btn-flat success btn_process" value="<?= $job_detail[$ind]['job_id']; ?>">Yes</a>
                                                            </div>
                                                        </div>
                                                    </div>
            <?php
        }
        ?>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
        <?php
    }
    ?><?php
}
?>
                            <!-- END OPENING DETAILS -->

                            <div id="footer_pagination">
                                <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                                      method="post" action="<?= $vObj->getURL('jobopening') ?>">
                                    <div class="span12 pagination"><?= $pagination ?></div>
                                    <input type="hidden" value="<?= $page ?>" name="page">
                                    <input type="hidden" value="<?= $country_id ?>" name="country_id">
                                    <input type="hidden" value="<?= $skill_level_id ?>" name="skill_level_id">
                                    <input type="hidden" value="<?= $creative_skill ?>" name="creative_skill">
                                    <input type="hidden" value="<?= $job_city ?>" name="job_city">
                                    <input type="hidden" value="<?= $keyword ?>" name="keyword">
                                    <input type="hidden" value="<?= $start_date ?>" name="start_date">
                                    <input type="hidden" value="<?= $job_type ?>" name="job_type">
                                    <input type="hidden" value="<?= $job_filter ?>" name="job_filter">
                                    <input type="hidden" value="<?= $orderby ?>" name="orderby">
                                </form>
                            </div>
                        </div>
                        <!-- END RIGHT COLUMN -->
                        <div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
