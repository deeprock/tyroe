
<!-- main container -->
  <div class="content">

       <div class="container-fluid">




           <div id="pad-wrapper" id="new-user">

               <div class="grid-wrapper">

               <div class="row-fluid show-grid">

                       <!-- START LEFT COLUMN -->
                   <div class="span4 avatar-profile">
                                      <div class="row-fluid" id="side-list">
                                          <div class="row-fluid" >
                                              <div class="span12">
                                                  <a href="<?= $vObj->getURL('profile') ?>" class="name">Profile</a>
                                                  <span class="subtext">Edit your information and settings</span>
                                                  <div class="pointer">
                                                      <div class="arrow"></div>
                                                      <div class="arrow_border"></div>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row-fluid" id="active">
                                              <div class="span12">
                                                  <a href="<?= $vObj->getURL('resume') ?>" class="name">Resume</a>
                                                  <span class="subtext">Showcase your experience and skills</span>
                                              </div>
                                          </div>
                                          <div class="row-fluid">
                                              <div class="span12">
                                                  <a href="<?= $vObj->getURL('portfolio') ?>" class="name">Portfolio</a>
                                                  <span class="subtext">Upload your best images and videos</span>
                                              </div>
                                          </div>
                                          <div class="row-fluid">
                                              <div class="span12">
                                                  <a href="<?= $vObj->getURL('subscription') ?>" class="name">Subscription</a>
                                                  <span class="subtext">Account and billing information</span>
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                       <!-- END LEFT COLUMN -->

                       <!-- START RIGHT COLUMN -->
                       <div class="span8 sidebar">
                           <div class="right-column">
                               <div class="row-fluid header">
                                   <h5 class="resume-title">Edit Resume</h5>
                               </div>
                               <div class="row-fluid header">
                                   <div class="span12">
                                           <h4>Experience</h4>
                                           <span class="pull-right button-center">
                                   <a class="btn-flat success" id="invite">+ Add Experience</a></span>
                                   </div>

                               </div>
                               <!-- START TEAM DETAILS -->

                               <!-- JOB DETAIL START -->
                               <div class="row-fluid">
                                   <div class="span12 people" id="top">
                                       <div class="row-fluid">
                                           <div class="span8">
                                               <div class="span12 job-info">
                                                   <a href="#" class="job-header">Junior Graphic Designer</a>
                                                   <p><strong>Company:</strong> Saatchi and Saatchi
                                                   <a href="http://www.saatchi.com/"><span>http://www.saatchi.com/</span></a></p>
                                                   <p><strong>Date:</strong> 1st Jan 2013 - Present</p>
                                               </div>
                                           </div>
                                           <div class="span4">
                                               <ul class="actions pull-right">
                                                   <li><i class="table-edit"></i></li>
                                                   <li class="last"><i class="table-delete"></i></li>
                                               </ul>
                                           </div>
                                       </div>
                                       <div class="row-fluid">
                                           <p>There are many variations of passages of Lorem Ipsum available but the majority have humour suffered alteration in believable some formhumour , by injected humour, or randomised words which don't look even slightly believable.
                                           </p>
                                       </div>
                                   </div>
                               </div>

                               <!-- JOB DETAIL START -->
                               <div class="row-fluid">
                                   <div class="span12 people" id="both">
                                       <div class="row-fluid">
                                           <div class="span8">
                                               <div class="span12 job-info">
                                                   <a href="#" class="job-header">Junior Graphic Designer</a>
                                                   <p><strong>Company:</strong> Saatchi and Saatchi
                                                   <a href="http://www.saatchi.com/"><span>http://www.saatchi.com/</span></a></p>
                                                   <p><strong>Date:</strong> 1st Jan 2013 - Present</p>
                                               </div>
                                           </div>
                                           <div class="span4">
                                               <ul class="actions pull-right">
                                                   <li><i class="table-edit"></i></li>
                                                   <li class="last"><i class="table-delete"></i></li>
                                               </ul>
                                           </div>
                                       </div>
                                       <div class="row-fluid">
                                           <p>There are many variations of passages of Lorem Ipsum available but the majority have humour suffered alteration in believable some formhumour , by injected humour, or randomised words which don't look even slightly believable.
                                           </p>
                                       </div>
                                   </div>
                               </div>


                               <!-- START TEAM DETAILS -->
                               <div class="row-fluid sections">
                                   <div class="span12 people">
                                       <div class="span8">
                                           <h4>Education</h4>
                                       </div>
                                       <div class="span4">
                                           <span class="pull-right">
                                           <a class="btn-flat success" id="resume-add">+ Add Education</a></span>
                                       </div>
                                   </div>
                               </div>
                               <div class="row-fluid">
                                   <div class="span12 people" id="top">
                                       <div class="span8">
                                           <h4>Skills</h4>
                                       </div>
                                       <div class="span4">
                                           <span class="pull-right">
                                           <a class="btn-flat success" id="resume-add">+ Add Skills</a></span>
                                       </div>
                                   </div>
                               </div>
                               <div class="row-fluid">
                                   <div class="span12 people" id="top">
                                       <div class="span8">
                                           <h4>Awards</h4>
                                       </div>
                                       <div class="span4">
                                           <span class="pull-right">
                                           <a class="btn-flat success" id="resume-add">+ Add Awards</a></span>
                                       </div>
                                   </div>
                               </div>
                               <div class="row-fluid">
                                   <div class="span12 people" id="top">
                                       <div class="span8">
                                           <h4>References</h4>
                                       </div>
                                       <div class="span4">
                                           <span class="pull-right">
                                           <a class="btn-flat success" id="resume-add">+ Add References</a></span>
                                       </div>
                                   </div>
                               </div>

                           </div>
                       </div>
                   </div>





               </div>
           </div>
       </div>
   </div>
   <!-- end main container -->