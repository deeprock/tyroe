<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//echo date('d M Y', $jobs['start_date']) . " - " . date('d M Y', $jobs['end_date']); die('Call');
//echo '<pre>';print_r($get_countries );echo '</pre>';die('Call');

?>

<script src="<?= ASSETS_PATH ?>js/fuelux.wizard.js"></script>
<script type="text/javascript">
    $(function () {
        /*var dt2_description_current = $("#dt2_description").height();
        if( dt2_description_current > 300)
        {
            $("#dt2_description").css('height','300');
            $("#dt2_description").css('overflow-y','auto');
        }*/

        var warm_welcome_job_id = '';
        var $wizard = $('#fuelux-wizard'),
                $btnPrev = $('.wizard-actions .btn-prev'),
                $btnNext = $('.wizard-actions .btn-next'),
                $btnFinish = $(".wizard-actions .btn-finish");
        var job_title;
        var job_description;
        var job_country;
        var job_city;
        var job_startdate;
        var job_enddate;
        var job_type;
        var job_level;
        var job_tags;
        $wizard.wizard().on('finished',function (e) {
            // wizard complete code
        }).on("changed", function (e) {
                    var step = $wizard.wizard("selectedItem");
                    // reset states
                    $btnNext.removeAttr("disabled");
                    $btnPrev.removeAttr("disabled");
                    $btnNext.show();
                    $btnFinish.hide();

                    if (step.step === 1) {
                        $btnPrev.attr("disabled", "disabled");
                    } else if (step.step === 3) {
                        $btnNext.hide();
                        $btnFinish.show();
                    }
                });

        $("#startdate").datepicker({
            'showCheckbox': false
        }).on('changeDate', function(ev){
                    $(this).datepicker('hide');
        });
        $("#enddate").datepicker({
            'showCheckbox': false
        }).on('changeDate', function(ev){
                    $(this).datepicker('hide');
        });

        $('#btn-next').on('click', function () {
            $('.searching_reviewer_result').html('');
            job_title = $('#job_title').val();
            job_description = $('#job_description').val();
            job_description = job_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />');
            job_country = $('#country').val();
            job_city = $('#city').val();
            job_startdate = $('#startdate').val();
            job_enddate = $('#enddate').val();
            job_tags = $('#job_tags').val();
            var date1 = new Date(job_startdate);
            var date2 = new Date(job_enddate);
            var timeDiff = dateDiff(date1, date2);

            var studio_name = $("#hidden_username").val();
            var job_country_name = $("#country option:selected").text();
            var job_city_name = $("#city option:selected").text();
            var job_type_name = $('.job_type').attr('data-id');
            var job_level_name = $('.job_level').attr("data-id");
            var date_my_diff = "";
            /*if (timeDiff.years > 0) {
                date_my_diff = timeDiff.years + " Years(s) " + timeDiff.months + "  Month(s) " + timeDiff.days + " Day(s) ";
            } else {
                date_my_diff = timeDiff.months + "  Month(s) " + " Day(s) ";
            }*/
            var content  = '<div class="span12 margin-left-default">';
                content += '<h1>'+ studio_name +'</h1>';
                content += '<h3 class="dt-1">' + job_title + '</h3><br>';
            content += '<h5 class="dt-2">' + job_city_name + "," + job_country_name + '</h5>';
            content += '<p class="dt-2 jqueryClassAutoHeight" id="dt2_description">'+ job_description + '</p>';
            content += '<div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">';
            content += '<div class="span4 text-left st-1"><span class="opening-date"><i class="icon-calendar"></i>' + job_startdate +'</span></div>';
            content += '<div class="span4 text-center st-1"><span class="opening-month"><i class="icon-time"></i> ' + timeDiff + '</span></div>';
            content += '<div class="span4 text-center st-1"><span class="opening-time"><i class="icon-legal"></i> '+ job_type_name +'</span></div></div>';
            var jobTags = job_tags.split(',');
            content += '<p class="dt-label pull-left">';
                 for (var s=0; s<jobTags.length; s++){
                     content += '<span class="label">'+jobTags[s]+'  </span>';
                 }
            content +=  '</p></div>';
            $(".preview_job").html(content);
        });
        $btnPrev.on('click', function () {
            $wizard.wizard('previous');
        });
        $btnNext.on('click', function () {
            var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
            var error= 0;
            var job_title = $('#job_title').val();
            var job_description = $('#job_description').val();
            var job_industry = $('#job_industry').val();
            var job_country = $('#country').val();
            var job_city = $('#city').val();
            var job_startdate = $('#startdate').val();
            var job_enddate = $('#enddate').val();
            var job_type = $('.job_type:checked').val();
            var job_level= $('.job_level:checked').val();
            var job_tags = $('#job_tags').val();
            if($.trim(job_title) === "")
            {
                $('#job_title').css({ border: '1px solid red'});
                error++;
            }else{
                $('#job_title').css("border","");
            }
            if($.trim(job_description) === "")
            {
                $('#job_description').css({ border: '1px solid red'});
                error++;
            }else{
                $('#job_description').css("border","");
}
            if($.trim(job_industry) === "")
            {
                $('.job_industry').css({ border: '1px solid red'});
                error++;
            }else{
                $('.job_industry').css("border","");
}
            if($.trim(job_country) === "")
            {
                $('.country').css({ border: '1px solid red'});
                error++;
            }else{
                $('.country').css("border","");
}
            if($.trim(job_city) === "")
            {
                $('.city').css({ border: '1px solid red'});
                error++;
            }else{
                $('.city').css("border","")
            }
            if($.trim(job_startdate) === "")
            {
                $('#startdate').css({ border: '1px solid red'});
                error++;
            }else{
                $('#startdate').css("border","");
            }
            if($.trim(job_enddate) === "")
            {
                $('#enddate').css({ border: '1px solid red'});
                error++;
            }else{
                $('#enddate').css("border","");
            }
            if($.trim(job_type) === "")
            {
                $('.type-extra-padding').css({ border: '1px solid red'});
                error++;
            }else{
                $('.type-extra-padding').css("border","");
            }
            if($.trim(job_level) === "")
            {
                $('.job-level').css({ border: '1px solid red'});
                error++;
            }else{
                $('.job-level').css("border","");
            }
            if($.trim(job_tags) === "")
            {
                $('#s2id_job_tags').css({ border: '1px solid red'});
                error++;
            }else{
                $('#s2id_job_tags').css("border","");
            }
            if(error > 0)
            {
                $('.modal_error_box').show();
                $('.modal_error_box').html(error_empty);
            }
            else{
                $wizard.wizard('next');
                var paraHeight = $(".preview_job").find('.jqueryClassAutoHeight').height();
                if(paraHeight > 180){
                    $(".preview_job").find('.jqueryClassAutoHeight').css({"height":"180px","overflow":"auto"});
                }else{
                    $(".preview_job").find('.jqueryClassAutoHeight').css({"height":paraHeight+"px"});
                }
            }
        });
    });
    $(document).on('click','button.close, a.close, .job_close',function(){
        $('#job_title').css("border","");
        $('#job_description').css("border","");
        $('.job_industry').css("border","");
        $('.country').css("border","");
        $('.city').css("border","");
        $('#startdate').css("border","");
        $('#enddate').css("border","");
        $('.job-level').css("border","");
        $('#s2id_job_tags').css("border","");
        $('.type-extra-padding').css("border","");
        $('.modal_error_box').hide();
        $('#fuelux-wizard').wizard('previous');
        $('#fuelux-wizard').wizard('previous');
        $('#createjobform')[0].reset();
        $('#job_industry').select2('destroy');
        $('#job_industry').select2();
        $('#country').select2('destroy');
        $('#country').select2();
        $('#city').select2('destroy');
        $('#city').select2();
    });
</script>


<script type="text/javascript">



    function getcities(country_id) {
        var data = "country_id=" + country_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("jobopening/getcities")?>',
            dataType: "json",
            success: function (response) {
                $('.city').select2('destroy');
                $('#citybox').html(response.dropdown);
                $('#city').select2();

            }

        });
    }
    function postjob() {
        $('.loader-save-holder').show();
        $('#post_job').after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
        var reviewers = $('#reviewer_container input[name="reviewer_ids[]"]').map(function () {
            return this.value
        }).get();
        var team_moderation_val = $('.team_moderation').text();
        if (team_moderation_val == 'OFF') {
            team_moderation_val = 0;
        } else if (team_moderation_val == 'ON') {
            team_moderation_val = 1;
        }
        var job_description = $('#job_description').val();
        job_description = job_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />');
        var data = {
            'job_title': $('#job_title').val(),
            'job_description': job_description,
            'job_country': $('#country').val(),
            'job_city': $("#city option:selected").val(), //take id of city .. don't  use string
            'job_startdate': $('#startdate').val(),
            'job_enddate': $('#enddate').val(),
            'job_type': $('.job_type:checked').val(),
            'job_level': $('.job_level:checked').val(),
            'job_tags': $('#job_tags').val(),
            'job_industry': $('#job_industry').val(),
            'job_reviewers': reviewers,
            'team_moderation': team_moderation_val
        }
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("jobopening/SaveOpening")?>',
            datatype: 'json',
            success: function (job_id) {
                $('body').removeClass('.modal-open').css({"overflow":"auto"});
                $('#myModal').modal('hide');
                warm_welcome_job_id = $.parseJSON(job_id);
                $('body').css({"overflow":"auto"});
                $('#post_job').removeAttr('onClick');
                $('.loader-save-holder').hide();
               $('.small_loader').remove();
               $('.warm-welcome-container').show();

            }
        });
    }

    function DeleteOpening(job_id) {
        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("jobopening/DeleteOpening")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".job-opening" + job_id).hide();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });

    }
    function JobApply(job_id) {

        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("jobopening/JobApply")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    //console.log(data.success_message);
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
    //getting the exact date difference
    function dateDiff(dateFrom, dateTo) {
        /*var from = {
            d: dateFrom.getDate(),
            m: dateFrom.getMonth() + 1,
            y: dateFrom.getFullYear()
        };

        var to = {
            d: dateTo.getDate(),
            m: dateTo.getMonth() + 1,
            y: dateTo.getFullYear()
        };

        var daysFebruary = to.y % 4 != 0 || (to.y % 100 == 0 && to.y % 400 != 0) ? 28 : 29;
        var daysInMonths = [0, 31, daysFebruary, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

        if (to.d < from.d) {
            to.d += daysInMonths[parseInt(to.m)];
            from.m += 1;
        }
        if (to.m < from.m) {
            to.m += 12;
            from.y += 1;
        }

        return {
            days: to.d - from.d,
            months: to.m - from.m,
            years: to.y - from.y
        };*/

        var diff = Math.floor(dateTo.getTime() - dateFrom.getTime());
        var day = 1000* 60 * 60 * 24;
        var days = (diff/day);
        var months = Math.floor(days/31);
        var years = Math.floor(months/12);

       /*if(months > 0){
           var totalDaysInMonth = months * 31;
           days =   Math.abs(totalDaysInMonth - days);
       }
       if(years > 0){
           var totalMonthInYear = years * 12;
           months =    Math.abs(totalMonthInYear - months);
       }*/
        days = Math.round(days);
        if(days == 1){
            var message = days + " Day ";
        }else if(days >= 0 &&  days != 1){
            var message = days + " Days ";
        }
        /*if(months == 1){
            message += months + " Month ";
        }else if(months > 1){
            message += months + " Month(s) ";
        }

        if(years == 1){
            message += years + " Year \n";
        }else if(years > 1){
            message += years + " Year(s) \n";
        }*/
        return message
    }

</script>
<script type="text/javascript">
    $(document).on('click', '#btn_widgets_and_sharing', function () {
        $('#widgets_and_sharing').modal('show');
        $('body').css({"overflow":"hidden"})
    });
    $(document).ready(function () {
        $('#myModal').modal('hide');
        $('.btn-create-job').click(function () {
            $(".sugg-msg").html("");
            $('#myModal').modal('show');
            $('body').css({"overflow":"hidden"})
        });

        /*create job_tags*/
        $("#job_tags").select2({
            tags: [""],
            tokenSeparators: [","],
            placeholder: 'TAGS: Photoshop, Character Design, Python, Maya'
        });
        /*End create job_tags*/

        /* Creating emails tags */
        $("#invitation_emails").select2({
            tags: [""],
            tokenSeparators: [","]
        });

    });

    function rescale() {
        var size = {width: $(window).width(), height: $(window).height() }
        var offset = 20;
        var offsetBody = 150;
        //$('#myModal').css('height', size.height - offset );
        $('.modal-body').css('height', size.height - (offset + offsetBody));
        $('#myModal').css('top', '3%');
    }
    $(window).bind("resize", rescale);

    //existing reviewer searching
    $(document).on('keyup', '#search_existing_reviewer', function (a) {
        $('.searching_reviewer_result').css({'display':'block','float':'none'});
        var searching_value = $("#search_existing_reviewer").val()
        $.ajax({
            type: 'POST',
            data: ({'searching_value': searching_value}),
            url: "<?= $vObj->getURL("jobopening/search_existing_reviewer"); ?>",
            success: function (html) {
                if ($('#search_existing_reviewer').val() == '') {
                    $('.searching_reviewer_result').html('');
                } else {
                    $('.searching_reviewer_result').html(html);
                }
            }
        });

    });

    $(document).on('click', '.select_reviewer', function () {

        var user_value = $(this).data('value');
        $('.display_list'+user_value).remove();
        $('.parent_row'+user_value).remove();
        var previous_html = $('#reviewer_container').html();
        previous_html += '<li class="parent_row' + user_value + ' hideOnclose">';
        previous_html += $('.list_parent_row' + user_value).html();
        previous_html += '</li>';
        $('#reviewer_container').html(previous_html);
        $('.list_parent_row' + user_value).remove();
    });
    $(document).on('click', '#send_invitaion', function () {
        var invitation_emails = $("#invitation_emails").val();
        data = 'sending_action=invitaion&emails=' + invitation_emails;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("search/send_email");?>',
            success: function (result) {
                if (result) {
                    $('#invitation_emails').val('');
                    alert('invitaion send successfully');
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).on('click', '.remove-btn', function (e) {
        $(this).parent().remove()
    });
    $(document).on('click', '.ready_btn', function () {
        $('.warm-welcome-container').hide();
    })

    jQuery(document).ready(function () {
        $(".dt-show-text").click(function(){
            $(".warm-welcome-container").css('display','none');
            $('.btn-ready').trigger("click");
        });
        $(".warm-video").hide();
        $(".warm-two").hide();
        var total_warm_container = $('.warm-welcome-container .welcome-wrapper').size();
        var warm_conatiner_position = 1;
        /*Cloning*/
        jQuery(".clone-btn").click(function () {
            var clone_html = jQuery(this).parent().html();
            var clone_htmls = clone_html.replace('<input name="site_btn[]" class="btn f-right cus-btn-mov clone-btn" value="+ADD SITE" type="button">', '<input name="remove_btn" class="btn f-right cus-btn-mov remove-btn" value="-REMOVE" type="button">');
            var clone_div = '<div class="span12 field-box c-magic custom-borders clone-div">' + clone_htmls + '</div>';
            jQuery('.actions').before(clone_div);
        });
        /**/
        jQuery(".warmNext").click(function () {
            warm_conatiner_position++;
            $('.welcome-wrapper').hide();
            $('.warmarea' + warm_conatiner_position).show();
        });
        jQuery(".btn-ready").click(function () {
            var job_name = $('#job_title').val().toLowerCase();
            job_name = job_name.replace(/[^\w\s]/gi, '');
            var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_name + '-' + generate_number_id(warm_welcome_job_id);
            window.location = redirect_url;
        });

        $(".btn-create-job").click(function(){
            $(".select2-container .select2-input").focus();
            $(".select2-container .select2-input").blur();
        })

    });

    function applicaint_call(job_id, job_title) {
        $('input[name="applicaint_url_trigger"]').val('yes');
        var job_title = job_title.toLowerCase();
        job_title = job_title.replace(/ /gi, '-');
        var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
        $('#A_C_S_jumping_from').attr('action', redirect_url)
        $('#A_C_S_jumping_from').submit();
    }
    function shortlist_call(job_id, job_title) {
        $('input[name="shortlist_url_trigger"]').val('yes');
        var job_title = job_title.toLowerCase();
        job_title = job_title.replace(/ /gi, '-');
        var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
        $('#A_C_S_jumping_from').attr('action', redirect_url)
        $('#A_C_S_jumping_from').submit();
    }
    function candidate_call(job_id, job_title) {
        $('input[name="candidate_url_trigger"]').val('yes');
        var job_title = job_title.toLowerCase();
        job_title = job_title.replace(/ /gi, '-');
        var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
        $('#A_C_S_jumping_from').attr('action', redirect_url)
        $('#A_C_S_jumping_from').submit();
    }
</script>
<div style="display:none" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="warm-welcome-container" style="display:none ">
    <div class="welcome-overlay"></div>
    <div class="welcome-wrapper warm-one warmarea1">
        <h4>
            Congratulations <?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>
            !</h4>

        <h1>You have created a new job</h1>

        <p>Before you get started, we would like to outline a few key features that will make sure you find the best
            candidate possible, in the shortest amount of time. </p>

        <div class="welcome-buttons-container next-btn-wrapper">
            <a class="warmNext next btn btn-large btn-default" href="javascript:void(0);">Next</a>

            <div class="clearfix"></div>
            <a class="dt-show-text" href="javascript:void(0);">Got it! Don't show me again.</a>
        </div>
    </div>
    <div class="welcome-wrapper warm-menu warm-two warmarea21" style="display: none">
        <h1>Shortlist Process</h1>

        <p> - People that apply to your job posting are added as "Applicants"<br>
            - Upgrade an "Applicant" to "Candidate" level for team members to review<br>
            - Find additional "Candidates" by searching our extensive database<br>
            - Work together to "Shortlist" the best person for your company</p>

        <div class="container width-100">
            <div class="row-fluid">
                <div class="warm-menu-holder span12 margin-top-4">
                    <img src="<?= ASSETS_PATH ?>img/process-img.png" alt="Interactive-menu">
                </div>
            </div>
        </div>
        <div class="welcome-buttons-container next-btn-wrapper">
            <a href="javascript:void(0);" class="warmNext next btn btn-large btn-default">Next</a>

            <div class="clearfix"></div>
            <a href="javascript:void(0);" class="dt-show-text">Got it! Don't show me again.</a>
        </div>
    </div>
    <div class="welcome-wrapper warm-menu warm-three warmarea2 warm-sp" style="display: none">
        <h1>Job Opening Tabs </h1>

        <div class="container width-100">
            <div class="row-fluid">
                <div class="warm-menu-holder span12 margin-top-10 margin-bottom-10">
                    <img src="<?= ASSETS_PATH ?>img/opening-tab.png" alt="Interactive-menu">

                </div>
                <div class="next-btn-wrapper ready-con ready-td">
                    <a class="button-main button-large btn new-nav-btn1 btn-flat btn-ready primary btn-success btn-td ready_btn" href="javascript:void(0);">I'm
                        ready!</a>

                    <div class="clearfix"></div>
                    <a href="javascript:void(0);" class="dt-show-text">Got it! Don't show me again.</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="content">
<div class="container-fluid">
    <div id="pad-wrapper">
        <div class="grid-wrapper">
            <div class="row-fluid show-grid">
                <!-- START LEFT COLUMN -->
                <?php
                if ($this->session->userdata('role_id') == "2") {
                    $this->load->view('left_coloum_job');
                }
                ?>
                <!-- END LEFT COLUMN -->
                <!-- START RIGHT COLUMN -->
                <div class="span12">
                    <div class="right-column" id="open-jobs">
                        <div class="row-fluid header">
                            <?php
                            if ($this->session->userdata('role_id') != "4") {
                                ?>
                                <div class="span8 pull-right" id="top-buttons">
                                    <?php
                                    if ($archived == 1) {
                                        ?>
                                        <a class="btn-flat white pull-right uppercase" href="<?= $vObj->getURL("openings") ?>">Back
                                            to openings</a>
                                    <?php
                                    } else {
                                        ?>
                                        <a class="btn-flat white pull-right uppercase" href="<?= $vObj->getURL("ArchivedOpenings") ?>">Archived
                                            Jobs</a>

                                    <?php
                                    }
                                    if ($total_jobs['total'] < 25) {
                                        ?>
                                    <a class="btn-flat success pull-right uppercase" href="<?=LIVE_SITE_URL.'opening/create_job'?>">Create
                                            New Job</a>
<!--                                        <a class="btn-flat success pull-right uppercase btn-create-job" href="javascript:void(0)">Create
                                            New Job</a>-->
                                    <?php
                                    }
                                    ?>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($this->session->userdata('role_id') == STUDIO_ROLE) {
                            ?>
                            <!--<div  id="btn_widgets_and_sharing" class="alert alert-standard fade in head-text"><!--<a data-dismiss="alert" href="#" class="close">×</a><strong>Heads up!</strong> You can easily advertise these jobs on your own website. <a href="javascript:void(0)" ><label class="label black pull-right">SETUP NOW</label></a> </div>-->
                        <?php
                        }
                        ?>
                        <!-- START OPENING DETAILS -->
                        <div class="filter-result"></div>
                        <?php
                        if ($job_detail != '') {
                            ?>
                            <!--This form for job_candidate, applicaint, shortlist jumping-->
                            <form id="A_C_S_jumping_from" action="" method="post" style="margin: 0">
                                <input type="hidden" name="applicaint_url_trigger">
                                <input type="hidden" name="candidate_url_trigger">
                                <input type="hidden" name="shortlist_url_trigger">
                            </form>
                            <?php
                            foreach ($job_detail as $jobs) {
                                ?>
                                <div class="row-fluid no-filter job-opening<?php echo $jobs['job_id']; ?>">
                                    <div class="span12" id="">
                                        <div class="row-fluid" id="testopening-stats">
                                            <div class="span12">
                                                <div class="row-fluid" id="job-main-holder">
                                                    <div class="job-upper-part">

                                                        <div class="span6">
                                                            <div style="text-align:center;">
                                                                <a href="<?= $vObj->getURL("openings/" . strtolower($vObj->cleanString($jobs['job_title']) . '-' . $vObj->generate_job_number($jobs['job_id']))) ?>" class="btn-flat white">
                                                                    <div class="data">
                                                                        <span class="number"><?php echo stripslashes($jobs['job_title']); ?></span>
                                                                    </div>
                                                                    <?php echo $jobs['job_city'] . "," . $jobs['job_location']; ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="span2" id="cand_dates">
                                                            <div style="text-align:center;">
                                                                <a href="javascript:void(0)" onclick="applicaint_call('<?php echo $vObj->generate_job_number($jobs['job_id']); ?>','<?php echo $vObj->cleanString($jobs['job_title']); ?>')" class="btn-flat white">
                                                                    <div class="data">
                                                                        <span class="number"><?= $jobs['total_applicants'] ?></span>
                                                                    </div>
                                                                    APPLICANTS
                                                                    <?php if ($jobs['newest_total_applicants'] > 0) { ?>
                                                                        <span class="candidate-badge"><p><?= $jobs['newest_total_applicants'] ?></p></span>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="span2" id="cand_dates">
                                                            <div style="text-align:center;">
                                                                <a href="javascript:void(0)" onclick="candidate_call('<?php echo $vObj->generate_job_number($jobs['job_id']); ?>','<?php echo $vObj->cleanString($jobs['job_title']); ?>')" class="btn-flat white">
                                                                    <div class="data">
                                                                        <span class="number"><?= $jobs['total_candidates'] ?></span>
                                                                    </div>
                                                                    CANDIDATES
                                                                    <?php if ($jobs['newest_total_candidates'] > 0) { ?>
                                                                        <span class="candidate-badge"><p><?= $jobs['newest_total_candidates'] ?></p></span>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="span2" id="cand_dates">
                                                            <div style="text-align:center;">
                                                                <a href="javascript:void(0)" onclick="shortlist_call('<?php echo $vObj->generate_job_number($jobs['job_id']); ?>','<?php echo $vObj->cleanString($jobs['job_title']); ?>')" class="btn-flat white">
                                                                    <div class="data">
                                                                        <span class="number green-text"><?= $jobs['total_shortlist'] ?></span>
                                                                    </div>
                                                                    SHORTLISTED
                                                                    <?php if ($jobs['newest_total_shortlist'] > 0) { ?>
                                                                        <span class="candidate-badge"><p><?= $jobs['newest_total_shortlist'] ?></p></span>
                                                                    <?php } ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="job-lower-part">
                                                        <div class="span6" style="text-align: center;"><?php if ($jobs['get_total_reviewed'] != 100) { ?>
                                                            <a href="javascript:void(0);" class="btn-flat danger hide"style="display: none" >Reviews
                                                                    Pending</a> <?php } ?></div>
                                                        <div class="span2" style="text-align: center;">
                                                            <span class="opening-date"><i class="icon-calendar"></i> <?php echo date('dS F, Y', $jobs['start_date']) ?> </span>
                                                        </div>
                                                        <div class="span2" style="text-align: center;">
                                                            <span class="opening-month"><i class="icon-time"></i><?= $vObj->dateDifference($jobs['start_date'],$jobs['end_date']); ?></span>
                                                        </div>
                                                        <div class="span2" style="text-align: center;">
                                                            <span class="opening-time"><i class="icon-legal"></i><?= $jobs['job_type'] ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            <?php
                            }
                        } else {
                            ?>
                            <div class="alert alert-warning">
                                <i class="icon-warning-sign"></i>
                                You don't have any active job openings. Let's get started now.
                            </div>
                        <?php
                        }
                        ?>
                        <!-- END OPENING DETAILS -->
                        <?php
                        if ($archived_pagination == true)
                            $action = 'jobopening/ArchivedOpenings';
                        else
                            $action = 'openings';
                        ?>
                        <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search" method="post" action="<?= $vObj->getURL($action) ?>">
                            <div class="span12 pagination"><?= $pagination ?></div>
                            <input type="hidden" value="<?= $page ?>" name="page">
                        </form>
                    </div>


                </div>
                <!-- END RIGHT COLUMN -->

                <div>


                </div>


            </div>
        </div>
    </div>
</div>
<!--New job modal start-->
<div style="display: none;" class="modal modal_job_openingss fadess" id="myModalss" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
</div>
</div>
<!--new job modal end-->

<div style="display: none;" class="modal modal_job_opening fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="job_create_container">
        <div class="job_create_details">
            <div class="modal-content job_create_holder global-popup ">
                <div class="modal-header job_create_header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">
                    <div class="wizard row" id="fuelux-wizard">
                        <ul class="wizard-steps">
                            <li class="active" data-target="#step1">
                                <span class="step">1</span>
                                <span class="title">Job <br> details</span>
                            </li>
                            <li data-target="#step2" class="">
                                <span class="step">2</span>
                                <span class="title">Review <br> team</span>
                            </li>
                            <li data-target="#step3" class="">
                                <span class="step">3</span>
                                <span class="title">Settings <br> Preview</span>
                            </li>
                        </ul>
                    </div>
                </h4>

            </div>
                <div class="job_create_body">
                <div class="step-content">
                    <div id="step1" class="step-pane active">
                        <div class="row form-wrapper">
                            <div class="span5 center-block">
                                <div class="modal_error_box alert alert-danger">
                                </div>
                                <form id="createjobform">
                                    <div class="field-box">
                                        <input type="text" class="form-control" placeholder="Job Title" name="job_title" id="job_title" onkeypress="return checkspecialchars(event)">
                                        <div class="specialcharserror" style="display:none;">Job title can only contain 'A-Z','a-z','-','_' and '.'</div>
                                        <input type="hidden" name="hidden_username" id="hidden_username" value="<?= $studio_name ?>">
                                    </div>
                                    <div class="field-box">
                                        <textarea name="" class="form-control" id="job_description" cols="30" rows="6" placeholder="Description"></textarea>
                                    </div>
                                    <div class="field-box" style="margin-bottom: 25px;">
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <select name="job_industry" placeholder="Specialities" id="job_industry" class="job_industry">
                                                    <option></option>
                                                    <?php
                                                    foreach ($job_industries as $industry) {
                                                        ?>
                                                        <option value="<?= $industry['industry_id'] ?>"><?= $industry['industry_name'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-box" style="margin-bottom: 25px;">
                                        <div class="row-fluid">
                                            <div class="span6">
                                                <select name="country" id="country" onchange="getcities(this.value);" placeholder="Country" class="country">
                                                    <option></option>
                                                    <?php
                                                    foreach ($get_countries as $country) {
                                                        ?>
                                                        <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                            <div class="span6" id="citybox">
                                                <select  name="city" id="city" class="city">
                                                    <option></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-box">
                                        <div class="row-fluid">
                                           <div class="span6 date-period">
                                                <input type="text" name="startdate" id="startdate" placeholder="Start-Date">
                                            </div>
                                            <div class="span6 date-period">
                                                <input type="text" name="enddate" id="enddate" placeholder="End-Date">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" field-box lined margin-30 job-type sr-sec">
                                        <div class="row-fluid type-extra-padding">
                                        <?php
                                        foreach ($get_job_type as $type) {
                                            ?>
                                            <div class="span4">
                                                <div class="job-type-holder">
                                                    <input type="radio" class="job_type" value="<?= $type['job_type_id'] ?>" data-id="<?= $type['job_type'] ?>" name="job_type">
                                                    <span><?= $type['job_type'] ?></span>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                       </div>
                                    </div>

                                    <div class="field-box  job-level sr-sec-2">
                                        <div class="row-fluid">
                                        <?php
                                        foreach ($get_job_level as $level) {
                                            ?>
                                            <div class="span4">
                                                <div class="job-level-holder">
                                                    <input type="radio" class="job_level" value="<?= $level['job_level_id'] ?>" data-id="<?= $level['level_title'] ?>" name="job_level">
                                                    <span><?= $level['level_title'] ?></span>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                        ?>
                                        </div>
                                    </div>
                                    <br clear="all">

                                    <div id="field-box">
                                        <div class="row-fluid"><input type="text" name="job_tags" data-placeholder="TAGS: Photoshop, Character Design, Python, Maya" id="job_tags" class="span12 form-control" style="padding-left:20px;height: 45px"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="step2" class="step-pane">
                        <div class="row form-wrapper">
                            <div class="span4 center-block">
                                <h3 class="label-head">Add existing reviewers</h3>
                                <div class="field-box searchReviewArea">
                                    <input type="text" id="search_existing_reviewer" placeholder="Search <?= $collaborators ?> Collaborators" class="radius-search">

                                    <ul class="searching_reviewer_result ext-rvw-list">

                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="span5 center-block">
                                <div class="legend-sep"></div>
                            </div>

                            <div class="span5 center-block">
                                <h3 class="label-head">Invite new reviewers</h3>
                                <div class="field-box">
                                    <div class="row-fluid">
                                        <div class="span9">
                                            <input type="text" name="invitation" placeholder="Emails" id="invitation_emails">
                                        </div>
                                        <div class="span3">
                                            <button class="btn-flat success" id="send_invitaion">Send invite</button>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="clearfix"></div>

                            <div class="form_users_list">
                                <ul class="form-list-holder" id="reviewer_container">
                                    <li>
                                        <div class="span5 center-block">
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <div class="span2 user-display">
                                                        <?php
                                                        $image;
                                                        if ($team_admin['media_name'] != "") {
                                                            $image = ASSETS_PATH_PROFILE_THUMB . $team_admin['media_name'];
                                                        } else {
                                                            $image = ASSETS_PATH_NO_IMAGE;
                                                        }
                                                        ?>
                                                        <img src="<?php echo $image; ?>" alt="avatar" class="img-circle avatar hidden-phone">
                                                    </div>
                                                    <div class="span10">
                                                        <a href="javascript:void(0);" class="name"><?php echo $team_admin['firstname'] . ' ' . $team_admin['lastname']; ?></a>
                                                        <span class="subtext"><?php echo $team_admin['user_occupation']; ?> <span class="tag">Admin</span></span>
                                                    </div>
                                                </div>
                                                <div class="span4">
                                                    <a href="#" class="mail-user"><?php echo $team_admin['email']; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="step3" class="step-pane">
                        <div class="row-fluid form-wrapper">
                            <div class="span10 center-block">
                                <form>
                                    <div class="team-mod-setting-container">
                                        <h3>Settings</h3>
                                        <div class="field-box" id="rt_description">
                                            <div class="span10">
                                                <h6>Team moderation</h6>
                                                <p>Allow adminstrators to moderate all feedback shared with artists. If turned off, team members can share thier helpful feedback directly to artists on behalf of your company.</p>
                                            </div>
                                            <div class="span2">
                                                <div class="slider-frame pull-right">
                                                    <span data-on-text="ON" data-off-text="OFF" class="slider-button team_moderation">OFF</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>

                                    <div class="preview_job">
                                        <h3>Preview</h3>
                                    </div>
                                    <div class="field-box" id="rt_description1"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="step4" class="step-pane">
                        <div class="row form-wrapper payment-info">
                            <div class="col-md-8">
                                <form>
                                    <div class="field-box">
                                        <label>Subscription Plan:</label>
                                        <select id="plan">
                                            <option value="66">Basic - $2.99/month (USD)</option>
                                            <option value="67">Pro - $9.99/month (USD)</option>
                                            <option value="68">Premium - $49.99/month (USD)</option>
                                        </select>
                                    </div>
                                    <div class="field-box">
                                        <label>Credit Card Number:</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="field-box">
                                        <label>Expiration:</label>
                                        <input type="text" placeholder="MM" style="width:60px;display:inline" class="form-control">
                                        &nbsp; / &nbsp;
                                        <input type="text" placeholder="YYYY" style="width:85px;display:inline" class="form-control">
                                    </div>
                                    <div class="field-box">
                                        <label>Card CVC Number:</label>
                                        <input type="text" class="form-control">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="modal-footer job_create_footer">
                    <div class="wizard-actions">
                        <button class="btn-flat white job_close" data-dismiss="modal" aria-hidden="true" type="button" >
                             Close
                        </button>
                        <button data-last="Finish" class="btn-flat primary btn-next margin-left-2" type="button" style="display: inline-block;" id="btn-next">
                            Next
                        </button>
                        <button class="btn-flat success btn-finish" type="button" style="display: none;" id="post_job" onclick="postjob()">
                            Post Job
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Widgets & sharing Modal start-->
<div style="display: none;" class="modal fade" id="widgets_and_sharing" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="row-fluid st-header">
        <div class="modal-body">
            <div class="md-supporter">
                <div>
                    <button type="button" class="close tr-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h3>Social Share</h3>

                    <p class="st-text">Click here to quickly notify your socical media networks about this job</p>
                </div>

                <div id="social_widgets_share_icons" class="margin-bottom-10"></div>
                <h3>Buttons</h3>

                <p class="ft-text margin-top-1">Add buttons to your website job boards to link them directly to this
                    job</p>

                <form>
                    <div class="span3" style="margin-left: 0">
                        <div class="radio-wrap">
                            <input type="radio" name="btn_widgets" class="get_widgets_color" value="blue-btn" checked="checked">
                        </div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn-flat primary">Apply with <b>Tyroe</b></button>
                    </div>
                    <div class="span3">
                        <div class="radio-wrap">
                            <input type="radio" name="btn_widgets" class="get_widgets_color" value="grey-btn">
                        </div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn-flat gray">Apply with <b>Tyroe</b></button>
                    </div>
                    <div class="span3">
                        <div class="radio-wrap">
                            <input type="radio" name="btn_widgets" class="get_widgets_color" value="white-btn">
                        </div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn-flat white">Apply with <b>Tyroe</b></button>
                    </div>
                    <div class="span3">
                        <div class="radio-wrap">
                            <input type="radio" name="btn_widgets" class="get_widgets_color" value="black-btn">
                        </div>
                        <div class="clearfix"></div>
                        <button type="button" class="btn-flat inverse">Apply with <b>Tyroe</b></button>
                    </div>
                    <div class="clearfix"></div>
                    <p class="ft-text-2 margin-top-4 margin-bottom-2">Copy & paste this code</p>

                    <div id="wideget_code_area" contenteditable="false" style="margin-left: 0;height:120px;box-shadow: 1px 1px 1px #ccc,-1px -1px 1px #ccc;">
                        &lt;div id="widgets-btn"&gt;
                        &lt;script type="text/javascript"&gt;
                        var jobid = '';
                        var comapnyname =
                        '<?= $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>';
                        var job_url = '<?= $vObj->getURL("openings"); ?>';
                        var btn_theme = 'blue-btn';
                        &lt;/script&gt;
                        &lt;script type="text/javascript" src="<?= ASSETS_PATH ?>js/api_tyroe.js"&gt;
                        &lt;/script&gt;
                        &lt;/div&gt;
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('#country').select2({
            placeholder: 'Country'
        });
        $('#city').select2({
            placeholder: 'City'
        });
        $('#job_industry').select2({
            placeholder: 'Specialities'
        });
    })
</script>
<!--Widgets & sharing Modal End-->