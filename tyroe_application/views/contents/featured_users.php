<script type="text/javascript">
    $(document).ready(function () {
        //count_of_count

        $(document).delegate('a', 'click', function (evnt) {
            var this_ele = $(this);
            var this_href = this_ele.attr('href');
            if (this_href == '#') {
                evnt.preventDefault();
            }

        })

        var user_profile_json = 0;
        <?php if($get_featured_users_total_counts) { ?>
        user_profile_json = <?php echo json_encode($get_featured_users_total_counts); //echo json_encode(array_unique($get_featured_users_total_counts)); ?>;
        user_profile_data = <?=$get_featured_users_total_counts?>;
        var json_length = user_profile_json.length;
        <?php } else { ?>
        var json_length = 0;
        <?php }  ?>
        $(".count_of_count").text('1 of ' + json_length);

        $(document).on("click", ".getuser_profile, .prev_profile, .next_profile", function () {
            var load_ele = $('.loader-save-holder');
            load_ele.show();
            var this_ele = $(this);
            var profileurl = this_ele.attr('data-profileurl');
            var userid = this_ele.attr('data-userid');
            $.ajax({
                type: "POST",
                url: "<?= $vObj->getURL("searchpublicprofile/index"); ?>",
                data: ({"current_userid": userid}),
                success: function (success_data) {
                    if (success_data.search('<h6>No Access to This account</h6>') >= 0) {
                        //IF ACCOUNT NOT AVAILABLE
                    } else {
                        var prev_profile_ele = $('.prev_profile');
                        var next_profile_ele = $('.next_profile');
                        var data = user_profile_json;
                        console.log(data);
                        json_length = user_profile_json.length;
                        var json_index = functiontofindIndexByKeyValue(data, "tyr_rev_id", userid);
//                        var json_index = data.map(function (d) {
//                            return d['tyr_rev_id'];
//                        }).indexOf(userid);
                      
                        $(".count_of_count").html(parseInt(json_index + 1) + ' of ' + json_length);
                        if (typeof user_profile_json[json_index + 1] !== 'undefined') {
                            var next_id = user_profile_json[json_index + 1]['tyr_rev_id'];
                            next_profile_ele.show();
                        } else {
//                                var next_id = 1;
//                                next_profile_ele.hide();
                            var next_id = user_profile_json[0]['tyr_rev_id'];
                        }
                        if (typeof user_profile_json[json_index - 1] !== 'undefined') {
                            var prev_id = user_profile_json[json_index - 1]['tyr_rev_id'];
                            prev_profile_ele.show();
                        } else {
                            var prev_id = user_profile_json[json_length - 1]['tyr_rev_id'];
                        }

                        prev_profile_ele.attr('data-userid', prev_id);
                        next_profile_ele.attr('data-userid', next_id);

                        //feat_controlls
                        $(".feat_controlls, .add-feature, .feat_controlls, .decline-featured").attr('data-id', userid);
                        $(".ft_tyr_list").hide();
                        $(".ft_profile_result").remove();
                        $(".feat_controlls, .featured_control_main, .featured_control_main_sep").show();
                        $(".ft_tyr_list").before('<div class="ft_profile_result"></div>');


                        setTimeout(function () {
                            $(".ft_profile_result").html(success_data);
                            $("#profile_iframe").show();
                            $("#profile_iframe").height($("#profile_iframe").contents().find("body").outerHeight(true));
                            $('#profile_iframe').css({"border": "0"});
                        }, 1000);
                    }

                },
                complete: function () {
                    load_ele.hide();
                }
            })
        });

        $(document).on("click", ".tyr_ft_listview", function () {
            $(".ft_profile_result").remove();
            $(".ft_tyr_list").show();
            $(".feat_controlls, .featured_control_main, .featured_control_main_sep").hide();
            //

        })

        $(document).on("click", ".add-feature", function () {
            var data_id = $(this).attr("data-id");
           $('.loader-save-holder').show();

            var obj = $(this);
            $.ajax({
                type: 'POST',
                data: "data_id=" + data_id,
                url: "<?= $vObj->getURL("featured/add_to_featured"); ?>",
                success: function (data) {
                    $('.loader-save-holder').hide();
                    if (data == 1) {
                        $('.div_featured_' + data_id).fadeOut(700, function () {
                            $(this).remove();

                        })

                        $(".ft_profile_result").remove();
                        $(".ft_tyr_list").show();
                        $(".feat_controlls, .featured_control_main, .featured_control_main_sep").hide();

                        $.getJSON("<?= $vObj->getURL("featured/get_userjson"); ?>", function (json_data) {
                            user_profile_json = json_data;
                        });

                        ///user_profile_json = user_profile_json;

                    }
                }
            });
        });

        $(document).on("click", ".decline-featured", function () {
            $('.loader-save-holder').show();
            var data_id = $(this).attr("data-id");
            var obj = $(this);
            $.ajax({
                type: 'POST',
                data: "data_id=" + data_id,
                url: "<?= $vObj->getURL("featured/decline_featured"); ?>",
                success: function (data) {
                    $('.loader-save-holder').hide();
                    if (data == 1) {
                        obj.closest('div.featured-record-div').fadeOut(700);

                        $(".ft_profile_result").remove();
                        $(".ft_tyr_list").show();
                        $(".feat_controlls, .featured_control_main, .featured_control_main_sep").hide();


                        $.getJSON("<?= $vObj->getURL("featured/get_userjson"); ?>", function (json_data) {
                            user_profile_json = json_data;
                        });

                        $('.div_featured_' + data_id).fadeOut(700, function () {
                            $(this).remove();

                        })
                    }
                }
            });
        });
    });
    function functiontofindIndexByKeyValue(arraytosearch, key, valuetosearch) {
 
        for (var i = 0; i < arraytosearch.length; i++) {

        if (arraytosearch[i][key] == valuetosearch) {
        return i;
        }
        }
        return null;
    }

</script>
<div style="display: none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="content">
    <div class="container-fluid padding-default">
        <div class="row">
            <div class="container">
                <div class="span12 featured_control_main hide">
                    <div class="span3 pull-left featured_list_view_container">
                        <a class="btn-flat primary tyr_ft_listview pull-left">LIST VIEW</a>

                        <div class="pagi_holder pull-left">
                            <ul class="pagination pull-left">
                                <li><a class="prev_profile" href="#">‹</a></li>
                                <li><a class="next_profile" href="#">›</a></li>
                            </ul>
                        </div>
                        <p class="count_of_count">1 of 5</p>
                    </div>
                    <div class="span2 pull-right">
                        <a href="javascript:void(0)" class="btn-flat success add-feature" data-id="">Feature</a>
                        <a href="javascript:void(0)" class="btn-flat inverse decline-featured" data-id="">Decline</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="editor-seperator featured_control_main_sep hide"></div>
        <div id="pad-wrapper" class="padding-default">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid push-down-20 ft_tyr_list">
                    <div class="right-column filter_editor padding-default" style="position:relative;">
                        <?php

                        //limited_words

                        //print_array($get_featured_users,false,'$get_featured_users');
                        if ($get_featured_users != '') {
                            if ($page_number > 1) {
                                $pos_id = ($page_number - 1) * 12;
                            } else {
                                $pos_id = 1;
                            }
                            for ($tyr = 0; $tyr < count($get_featured_users); $tyr++) {
                                $main_user_id = $get_featured_users[$tyr]['user_id'];
                                $user_profile_url = $get_featured_users[$tyr]['profile_url'];
                                $image;
                                if ($get_featured_users[$tyr]['media_name'] != "") {
                                    $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_featured_users[$tyr]['media_name'];
                                } else {
                                    $image = ASSETS_PATH_NO_IMAGE;
                                }
                                ?>
                                <div class="row-fluid featured-record-div rg-bg div_featured_<?= $main_user_id; ?>">
                                    <div class="container">
                                    <div class="span12 editor-pck">
                                        <div class="row-fluid">
                                            <div class="span12" id="top">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="edt_pck_sec">
                                                        <div class="user-dp-holder">
                                                            <a data-userid="<?= $main_user_id; ?>"
                                                               data-profileurl="<?= $user_profile_url; ?>"
                                                               class="getuser_profile"
                                                               href="javascript:void(0)" <?php /*onclick="getsingle_user(<?php echo $pos_id + 1; ?>,<?php echo $get_featured_users[$tyr]['user_id']; ?>)" */ ?>><img
                                                                    src="<?php echo $image; ?>"
                                                                    class="img-circle avatar-80"></a>
                                                        </div>
                                                        <div class="editors_picked" id="editors_pick">
                                                            <div class="editors_pick-info" id="editors_pick_info">
                                                                <a data-userid="<?= $main_user_id; ?>"
                                                                   data-profileurl="<?= $user_profile_url; ?>"
                                                                   href="javascript:void(0)" <?php /*  onclick="getsingle_user(<?php echo $pos_id + 1; ?>,<?php echo $get_featured_users[$tyr]['user_id']; ?>)"*/ ?>
                                                                   class="getuser_profile name"><?php echo $get_featured_users[$tyr]['firstname'] . ' ' . $get_featured_users[$tyr]['lastname']; ?></a>
                                                            <span class="location">
                                                                <?php
                                                                //
                                                                /*for ($a = 0; $a < count($get_featured_users[$tyr]['skills']); $a++) {
                                                                    $temp = $get_featured_users[$tyr]['skills'][$a]['creative_skill'];
                                                                    echo ($a > 0) ? ", " : "";
                                                                    if (!empty($temp)) {
                                                                        echo $temp;
                                                                    }
                                                                }*/echo rtrim($get_featured_users[$tyr]['industry_name'] . ', ' . $get_featured_users[$tyr]['extra_title'], ', ');
                                                                ?>
                                                            </span>
                                                                <span
                                                                    class="tags"><?php echo $get_featured_users[$tyr]['city'] . ", " . $get_featured_users[$tyr]['country']; ?></span>


                                                                <a href="javascript:void(0)"
                                                                   class="btn-flat success add-feature"
                                                                   data-id="<?= $get_featured_users[$tyr]['user_id'] ?>">Feature</a>

                                                                <a href="javascript:void(0)"
                                                                   class="btn-flat inverse decline-featured"
                                                                   data-id="<?php echo $get_featured_users[$tyr]['user_id']; ?>">Decline</a>
                                                                <br clear="all">
                                                            </div>
                                                            <div class="editors_showcase-container" id="editors-showcase">
                                                                <div class="editors_showcase_holder" >
                                                                    <div class="getuser_profile full-profile_overlay black_overlay_separate<?= $get_featured_users[$tyr]['user_id'] ?>"
                                                                         data-userid="<?= $main_user_id; ?>"
                                                                         data-profileurl="<?= $user_profile_url; ?>" >
                                                                        <div class="full-profile_overlay_holder">
                                                                            <p>View Full Profile</p>
                                                                            <i class="icon-picture"></i>
                                                                        </div>
                                                                    </div>

                                                                    <div class="row-fluid">
                                                                        <?php
                                                                        for ($a = 0; $a < 3; $a++) {
                                                                            if($get_featured_users[$tyr]['portfolio'][$a]['media_name']){
                                                                               $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_featured_users[$tyr]['portfolio'][$a]['media_name'];
                                                                               if (!file_exists($portfolio)) {
                                                                                   $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                               }else{
                                                                                   $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_featured_users[$tyr]['portfolio'][$a]['media_name'];
                                                                               }
                                                                            }else{
                                                                                $portfolio = ASSETS_PATH.'/img/image-icon.png';
                                                                            }
                                                                            ?>
                                                                            <div class="showcase_thumb pull-right">
                                                                                <a href="javascript:void(0);" class="score_img_anchor<?=$a?>" data-id="<?= $get_featured_users[$tyr]['user_id'] ?>">
                                                                                    <?= (($a == 2) && ($get_featured_users[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                                    <?php if ($a == 0) { ?>
                                                                                        <?php if ($get_featured_users[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                                            <div class="tyroe_score_over_img tyroeScoreHolder<?= $get_featured_users[$tyr]['user_id'] ?>">
                                                                                                <div
                                                                                                    class="tyroe_score_holder"><?= $get_featured_users[$tyr]['get_tyroes_total_score'] ?></div>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php } ?>
                                                                                    <img
                                                                                        src="<?php echo $portfolio ?>"
                                                                                        alt="editor-picks-<?= $get_featured_users[$tyr]['is_user_featured'] ?>">
                                                                                </a>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="clearfix"></div>

                                </div> <div
                                    class="editor-seperator editor-seperator-row<?= $get_featured_users[$tyr]['user_id'] ?>"></div>
                                <?php
                                $pos_id++;
                            }
                            ?>
                            <!-- START PAGINATION -->
                            <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                                  method="post" action="<?= $vObj->getURL('featured') ?>">
                                <div class="span12 pagination"><?= $pagination ?></div>
                                <input type="hidden" value="<?= $page ?>" name="page">
                            </form>
                        <?php } else { ?>
                            <span>No Tyroes found</span>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>

</div>
