<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//if ($this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '4')
//{
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script>

</script>
<!-- main container -->

<?php if ($profile_updated != "") {
    echo $profile_updated;
}
?>



<!-- START RIGHT COLUMN -->

<div class="row-fluid form-wrapper">


    <div class="span12">
        <div class="container-fluid" style="width: 450px;">


            <div class="span4 default-header">
                <h4>Personal Inforamtion</h4>
            </div>

            <div class="span12 field-box">
                <label>Avatar:</label>

                <div class="profile-image span6">
                    <img src="<?php echo $get_user['image']; ?>" class="avatar img-circle">


                </div>
            </div>
            <div class="span12 field-box">
                <label><?= LABEL_PUBLIC_PROFILE ?>:</label>
                <label class="span9"><?php echo $get_user['profile_url']; ?></label>
            </div>
            <div class="field-box">
                <label><?= LABEL_EMAIL ?>:</label>
                <label class="span9"><?php echo $get_user['email']; ?></label>
            </div>
            <div class="field-box">
                <label><?= LABEL_USER_NAME ?>:</label>
                <label class="span9"><?php echo $get_user['username']; ?></label>
            </div>
            <div class="field-box">
                <label><?= LABEL_COUNTRY ?>:</label>
                <label class="span9"><?php echo $get_user['country']; ?></label>
            </div>

            <div class="span12 field-box">
                <label><?= LABEL_STATE ?>:</label>
                <label class="span9"><?php echo $get_user['state']; ?></label>
            </div>
            <div class="span12 field-box">
                <label><?= LABEL_CITY ?>:</label>
                <label class="span9"><?php echo $get_user['city']; ?></label>
            </div>
            <div class="span12 field-box">
                <label><?= LABEL_CELLPHONE ?>:</label>
                <label class="span9"><?php echo $get_user['cellphone']; ?></label>
            </div>
            <?php if ($param) { ?>
            <script>
                jQuery(document).ready(function () {
                    $(window).load(function () {
                        $("html,body").animate({scrollTop:1000}, 1000);
                    });
                })
            </script>
            <form name="recom_form" action="<?=$vObj->getURL("profile/save_recommendation");?>" method="post">
                <div class="field-box">
                    <label><b><?= "Recommendation"; ?></b></label>
                    <br><br>

                    <div class="span12 field-box">
                        <label><?= "Name" ?>:</label>
                        <label class="">
                            <input type="text" name="recom_name"/>
                        </label>
                    </div>
                    <div class="span9 field-box">
                        <label><?= "Recommendation" ?>:</label>
                        <textarea name="recom" class="span9"></textarea>
                    </div>
                    <div class="span11 field-box actions">
                        <input type="submit" class="btn-glow primary" value="<?= LABEL_RECOMMENDATION_TITLE ?>">
                        <span>OR</span>
                        <input type="reset" value="Cancel" class="reset" onclick="window.history.back()">
                        <input type="hidden" name="recommend_id" value="<?=$recommend_id?>">
                    </div>
                </div>
            </form>
            <?php } ?>
        </div>
    </div>
</div>

<!-- END RIGHT COLUMN -->







<?php
//}
?>
<!--Dashboard Artist End-->