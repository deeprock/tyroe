<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/signin.css" type="text/css" media="screen"/>
<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img class="logo" src="<?= ASSETS_PATH ?>img/logo-dark.png">
    </a>

    <div class="span4 box">
        <div class="content-wrap">
            <h6>No Access to This account</h6>

        </div>
    </div>
</div>