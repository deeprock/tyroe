<style type="text/css">
    <?php foreach($css_files as $css_file){
        echo file_get_contents($css_file);
    } ?>
</style>
<div class="container-fluid">
        <div id="pad-wrapper">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <div class="span12 ">
                        <div class="right-column filter_editor" style="position:relative;">
                            <?php
                            for ($tyr = 0; $tyr < count($get_shortlist); $tyr++) {
                                $main_user_id = $get_shortlist[$tyr]['user_id'];
                                $image;
                                if ($get_shortlist[$tyr]['media_name'] != "") {
                                    $image = ASSETS_PATH_PROFILE_THUMB . $get_shortlist[$tyr]['media_name'];
                                } else {
                                    $image = ASSETS_PATH_NO_IMAGE;
                                }
                                ?>
                                <input type="hidden" value="<?=$get_shortlist[$tyr]['email']?>" class="bulk_emails">
                                <div class="row-fluid hiderow<?=$get_shortlist[$tyr]['job_detail_id']?>">
                                    <div class="span11 editor-pck row-id<?=$get_shortlist[$tyr]['job_detail_id']?>">
                                        <div class="row-fluid">
                                            <div class="span12" id="top">
                                                <div class="row-fluid">
                                                    <div class="span12">
                                                        <div class="span2 text-right">
                                                            <a href="javascript:void(0)" onclick="getsingle_user(<?php echo $pos_id+1; ?>,<?php echo $get_shortlist[$tyr]['user_id']; ?>)">
                                                                <img src="<?php echo $image; ?>" class="img-circle avatar-80">
                                                                <div class="span7 pull-right text-center hidden-phone">
                                                                    <?php if($get_candidates[$tyr]['invitation_status']==1){ ?>
                                                                        <i class="icon-circle icon-large under-avatar-status" id="green"></i>
                                                                    <?php }else{?>
                                                                        <i class="icon-circle icon-large under-avatar-status" id="yellow"></i>
                                                                    <?php } ?>
                                                                </div>
                                                            </a>
                                                        </div>
                                                        <div class="span10" id="editors_pick">
                                                            <div class="span4" id="editors_pick_info">
                                                                <!--<a href="javascript:void(0);" class="name" value="<?php/*=$main_user_id*/?>"><?php /*echo $get_shortlist[$tyr]['username']; */?></a>-->
                                                                <a href="javascript:void(0)"  class="name"><?php echo $get_shortlist[$tyr]['firstname']." ".$get_shortlist[$tyr]['lastname']; ?></a>
                                                                                                <span class="location"><?php
                                                                                                    for ($a = 0; $a < count($get_shortlist[$tyr]['skills']); $a++) {
                                                                                                        $temp = $get_shortlist[$tyr]['skills'][$a]['creative_skill'];
                                                                                                        echo ($a > 0)?", ":"";
                                                                                                        if(!empty($temp)){
                                                                                                            echo $temp;
                                                                                                        }
                                                                                                    }
                                                                                                    ?></span>
                                                                <span class="tags"><?php echo $get_shortlist[$tyr]['city'] . " " . $get_shortlist[$tyr]['country']; ?></span>
                                                                <?php
                                                                if ($get_shortlist[$tyr]['availability'] == "1") {
                                                                    ?>
                                                                    <span class="label label-info">Available Now</span>
                                                                <?php
                                                                } else if ($get_shortlist[$tyr]['availability'] == "2") {
                                                                    ?>
                                                                    <span class="label label-info">Available soon</span>
                                                                <?php
                                                                } else if ($get_shortlist[$tyr]['availability'] == "3") {
                                                                    ?>
                                                                    <span class="label label-info">Not available for work</span>
                                                                <?php
                                                                }
                                                                ?>
                                                                <br clear="all">
                                                            </div>
                                                            <div class="span8" id="editors-showcase">
                                                                <?php
                                                                for ($a = 0; $a < count($get_shortlist[$tyr]['portfolio']); $a++) {
                                                                    $portfolio = $get_shortlist[$tyr]['portfolio'][$a]['media_name'];
                                                                    ?>
                                                                    <a href="javascript:void(0);">
                                                                        <?=( ($a==2) && ($get_shortlist[$tyr]['is_user_featured']==1) )?'<span class="editors-badge"><p></p></span>':'';?>
                                                                        <img src="<?php echo ASSETS_PATH_PORTFOLIO1_THUMB . $portfolio ?>"
                                                                             alt="editor-picks-<?=$get_shortlist[$tyr]['is_user_featured']?>"> </a>
                                                                <?php
                                                                }
                                                                ?>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                            <div class="reviews_box<?php echo $get_shortlist[$tyr]['job_detail_id']; ?> span10" style="margin: 0 auto;">
                                                                    <?php for($a=0; $a<count($get_shortlist[$tyr]['reviewers']); $a++){
                                                                        $image;
                                                                        if ($get_shortlist[$tyr]['reviewers'][$a]['media_name'] != "") {
                                                                            $image = ASSETS_PATH_PROFILE_THUMB . $get_shortlist[$tyr]['reviewers'][$a]['media_name'];
                                                                        } else {
                                                                            $image = ASSETS_PATH_NO_IMAGE;
                                                                        }
                                                                        ?>
                                                                        <div class="reviewers">
                                                                            <div class="span5">
                                                                                <img src="<?php echo $image; ?>" class="img-circle avatar-80">
                                                                                <div class="clearfix"></div>
                                                                                <span class="reviewer_name"><?php echo $get_shortlist[$tyr]['reviewers'][$a]['firstname'].' '.$get_shortlist[$tyr]['reviewers'][$a]['lastname']; ?></span>
                                                                                <div class="clearfix"></div>
                                                                                <span class="designation">CEO</span>
                                                                                <div class="clearfix"></div>
                                                                                <?php if($get_shortlist[$tyr]['reviewers'][$a]['action_interview'] != ''){ ?>
                                                                                    <span class="label label-info"><?php echo $get_shortlist[$tyr]['reviewers'][$a]['action_interview']; ?></span>
                                                                                <?php } ?>
                                                                                <?php if($get_shortlist[$tyr]['reviewers'][$a]['action_shortlist'] != ''){ ?>
                                                                                    <span class="label label-info"><?php echo $get_shortlist[$tyr]['reviewers'][$a]['action_shortlist']; ?></span>
                                                                                <?php } ?>

                                                                            </div>
                                                                            <div class="span5">
                                                                                <?php if($get_shortlist[$tyr]['reviewers'][$a]['job_id'] != ''){ ?>

                                                                                    <p><b><?php echo $get_shortlist[$tyr]['reviewers'][$a]['level_title']; ?>: </b><?php echo $get_shortlist[$tyr]['reviewers'][$a]['review_comment']; ?></p>
                                                                                <?php }else{ ?>
                                                                                    <span class="label label-info">Review pending</span>
                                                                                <?php } ?>
                                                                            </div>
                                                                            <?php if($get_shortlist[$tyr]['reviewers'][$a]['job_id'] != ''){ ?>
                                                                                <div class="span2"><?php echo round($get_shortlist[$tyr]['reviewers'][$a]['review_average']); ?></div>
                                                                            <?php } ?>
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    <?php } ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="editor-seperator editor-seperator-row<?=$get_shortlist[$tyr]['user_id']?>"></div>
                                </div>
                                <?php
                                $pos_id++;
                            }
                            ?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>