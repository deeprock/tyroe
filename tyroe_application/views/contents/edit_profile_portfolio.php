<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script src="<?= ASSETS_PATH ?>js/masonry.pkgd.min.js"></script>
<script src="<?= ASSETS_PATH ?>js/imagesloaded.pkgd.min.js"></script>
<script src="<?= ASSETS_PATH ?>dropzone/dropzone.js"></script>
<link href="<?= ASSETS_PATH ?>dropzone/css/dropzone.css" rel="stylesheet"/>
<script src="<?= ASSETS_PATH ?>js/jquery-ui.min.js"></script><!--Uploading with progress-->
<script src="<?= ASSETS_PATH ?>dragdrop/filereader.js"></script>
<script src="<?= ASSETS_PATH ?>dragdrop/script.js"></script>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>dragdrop/style.css"><!--Uploading with progress-->
<style type="text/css">
    .image-disp-container {
        margin: 10% 0 0 -7%;
    }

    .image-disp-container input:first-child {
        margin-bottom: 10px;
    }
</style>
<?php //echo '<pre>';print_r($image_media_tags); echo '</pre>';?>
<script type="text/javascript">
    var total_images = 0;
<?php
/* $filtered_array = array_filter($image_media_tags);
  if(is_array($image_media_tags) && !empty($filtered_array)) {
  $json_tags = json_encode($image_media_tags);
  $tagsjson_not_null=true;
  echo "var json_media_tags = ".$json_tags.";";
  } */
?>


    var photoupload_ajaxurl = null;
    window.photoupload_ajaxurl = '<?= $vObj->getURL("profile/save_profile") . "/callfrom/latestwork"; ?>';

    $(document).ready(function () {
        //portfolio-image-column1
//        setTimeout(function () {
//            var left_image_section_height = $("#portfolio-image-column1").css("height").replace('px', '');
//            var right_image_section_height = $("#portfolio-image-column2").css("height").replace('px', '');
//            //console.log(left_image_section_height+'____left_image_section_height');
//            //console.log(right_image_section_height+'____right_image_section_height');
//        }, 100);


        $(".select_media_img_tags").select2({
            tags: [""], <?php /* //Do not remove tags:[""] because plugin thrown an error "query function not defined for Select2" :( */ ?>
            tokenSeparators: [",", " "]
        });

        //setTimeout(function (){
        //}, 10000)


        $('#full-width-box').on('dragenter', function (e) {
            e.originalEvent.stopPropagation();
            e.originalEvent.preventDefault();
            //$(".file-dropper-parent").show();
            $('#full-width-box').addClass('single-showcase');
            //console.log('reached dragenter');
            //$(".file-dropper-parent").fadeIn('50');
            //$('body').addClass('dragenter');
        });

        $('#full-width-box').on('dragged', function (e) {
            alert('dragged');
            e.originalEvent.stopPropagation();
            e.originalEvent.preventDefault();
            //$(".file-dropper-parent").show();
            $('#full-width-box').removeClass('single-showcase');
            //console.log('reached dragenter');
            //$(".file-dropper-parent").fadeIn('50');
            //$('body').addClass('dragenter');
        });

        $('.file-dropper-parent').on('dragleave', function (e) {
            //alert('mouseout');
            //$('.file-dropper-parent').hide();
            e.originalEvent.stopPropagation();
            e.originalEvent.preventDefault();
            $('#full-width-box').removeClass('single-showcase');
            return false;
        });

    });


    function portfolioImageRescale() {
        $('.cell').each(function () {
            var marginTop = (parseFloat($(this).find('.latest-work-disp').css('height').replace('px', '')) / 2) - parseFloat($(this).find('ul').css('height').replace('px', '')) / 2;
            var marginLeft = (parseFloat($(this).find('.latest-work-disp').css('width').replace('px', '')) / 2) - parseFloat($(this).find('ul').css('width').replace('px', '')) / 2;
            $(this).find('ul').css('margin-top', marginTop + 'px');
            $(this).find('ul').css('margin-left', marginLeft + 'px');
        });
    }
    $(window).bind("resize", portfolioImageRescale);
    $(document).on('click', '.enable-sortable-event', function () {


    });
</script>
<?php //print_r($video['media_type']); ?>
<section class="margin-80 embed-video-section" id="intro-box" style="display:<?=
//$video!=null?"block":"none";
$video != null ? "none" : "block";

//($video['media_type'] == "video" && $video != "" ? "none" : "block")
?>">
             <?php /* <i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark <?=($video['media_type']=="video" && $video!=""?"complete-mark":"uncomplete-mark")?>"></i>  */ ?>
    <div class="container">
        <div class="row-fluid">
            <div class="span10 offset1">
                <div class="video-container area-darker embed-video-sec">
                    <div class="clearfix"></div>
                    <h1 class="text-center embed-video-ico-support"><i class="icon-film"></i></h1>

                    <h3 class="text-center">Embed a Video</h3>

                    <p class="text-center embed-note">Show off your skills with your latest demo reel or movie</p>

                    <div class="row-fluid">
                        <div class="network-url-container span8 offset2">
                            <div class="tab-margin span2 user-pro-url-adjust pull-left">
                                <div class="btn-group">
                                    <button class="btn btn-ani-embed network-btn">
                                        <i class="<?= ($video['video_type'] == "" ? "font-icon-social-vimeo" : ($video['video_type'] == "vimeo" ? "font-icon-social-vimeo" : "font-icon-social-youtube")) ?>"></i>
                                    </button>
                                    <input type="hidden" value="<?= ($video['video_type'] == "" ? "vimeo" : ($video['video_type'] == "vimeo" ? "vimeo" : "youtube")) ?>" class="video-combo-selected-val video-section-combo-selected-val">
                                    <button data-toggle="dropdown" class="btn dropdown-toggle network-btn-support btn-anbs">
                                        <i class="caret"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="javascript:void(0)" class="video-combo" data-item-id="vimeo">Vimeo</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0)" class="video-combo" data-item-id="youtube">Youtube</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="span10 profile-url-tab">
                                <input type="text" class="form-control pull-left span12 video-link video-embed-url-field video-section-embed-url-field" value="<?= $video['media_url'] ?>" placeholder="Video URL">
                                <button type="button" class="button-main button-large btn-connect btn-theme-anb video-section-embed-url-btn">
                                    EMBED
                                </button>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <a class="no-video-note dont-own-note dont-own-video dontownavideo" href="javascript:void(0);">Don't own a video, display a full-width image instead.</a>
                <span class="arrow"></span>
            </div>
        </div>
    </div>
</section>

<div class="clearfix"></div>

<section data-callfrom="fullwidth" id="full-width-box" class="margin-80 full-width-image-section full-width-image-section-fields <?php /* dropzone-uploader */ ?>" style="display:<?php echo($video['media_type'] == "image" && $video != "" ? "none" : "block"); ?>">
    <div class="file-dropper-parent file_drop_full_width_box_featured_bg">
        <div class="file-dropper-child">
            <div class="dropbox drag-btn">
                <img src="../assets/img/cloud-drop.png" alt="dropbox">
                <h5>DROP FILES HERE</h5>
            </div>
        </div>
    </div>
    <?php /* <i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark <?=($video['media_type']=="image" && $video!=""?"complete-mark":"uncomplete-mark")?>"></i>  */ ?>
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <div class="video-container">
                    <div>
                        <h3 class="text-center">Add a full-width image</h3>

                        <p class="text-center">Showcase a single image in beautiful high resolution</p>
                        <button type="button" class="button-main button-large btn-theme-anb btn-drag drag-btn fullrs_ftimg">DRAG A SINGLE IMAGE HERE</button>
                        <p class="text-center text-grey hdr_fullimagetext">or click to upload</p>
                        <div class="hide">
                            <input type="file" id="hdr-full-image-input-file" class="hdr-full-image-input-file" name="file-input" />
                        </div>
                        <a href="javascript:void(0);" class="text-center embed-video-click">Embed a Video instead</a>

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <span class="arrow"></span>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section>
<section class="margin-80 intro-box_featured_bg featured-bg view-vid-img <?= ($video['media_type'] == "image" && $video != "" ? "default_padd no-backgound" : "") ?>" id="intro-box-dropdownft" style="display: <?= ($video != "" ? "block" : "none") ?>;background-color: <?= $this->session->userdata['featured_theme']['backgroundA'] ?>">
    <div class="file-dropper-parent file_drop_intro_box_featured_bg">
        <div class="file-dropper-child">
            <div class="dropbox drag-btn">
                <img src="../assets/img/cloud-drop.png" alt="dropbox">
                <h5>DROP FILES HERE</h5>
            </div>
        </div>
    </div>

    <input type="hidden" name="edit-video-fullimg" id="edit-video-fullimg" value="<?= ($video['media_type'] == "image" && $video != "" ? "image" : "video") ?>"/>
    <?= (($video["media_name"] != "" && $video["media_type"] == "image") ? '<img src="' . ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $video["media_name"] . '" alt="video">' : '') ?>
    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <div class="video-plugin-location <?= (($video['media_type'] == "video" && $video != "") ? 'video-container video-profile-res' : ''); ?>">

                    <section class="drag-img-container" style="display:<?= ($video['media_type'] == "image" && $video != "" ? "block" : "none") ?>">
                        <a class="button-main button-large btn-theme-anb drg-img dont-own-video draggsingle_imgfirst">Drag a Single Image here</a>

                        <div class="clearfix"></div>
                        <a class="embed-vid-ins btn-edit-video emb-vid-ins inverse">Embed a video instead</a>
                    </section>
                    <?php
                    if (strtolower($video['video_type']) == 'youtube') {
                        $youtube_vid_url = $video[media_url];
                        $youtube_vid_url = str_replace(array('https::', 'http::'), '', $youtube_vid_url);
                        if (strpos($youtube_vid_url, 'watch') >= 0) {
                            $youtube_vid_url = str_replace('watch?v=', 'embed/', $youtube_vid_url);
                        }
                        //
                        ?>
                        <iframe class="embed-video-section-iframe" width="560" height="315" src="<?= $youtube_vid_url; ?>" frameborder="0" allowfullscreen=""></iframe>
                        <?php
                    } else {
                        ?>

                        <?=
                        //<iframe width="560" height="315" src="//www.youtube.com/embed/nOEw9iiopwI" frameborder="0" allowfullscreen=""></iframe>


                        (($video['media_name'] != "" && $video['media_type'] == "video") ? '<iframe class="embed-video-section-iframe" src="' . $video['media_name'] . '?byline=0&amp;portrait=0&amp;badge=0&amp;color=FF6056"></iframe>' : '')
                        ?>
                        <?php /* =(($video['media_name']!="" && $video['media_type']=="video")?'<iframe class="embed-video-section-iframe" src="'.$video['media_name'].'?byline=0&amp;portrait=0&amp;badge=0&amp;color=FF6056"></iframe>':(($video["media_name"]!="" && $video["media_type"]=="image")?'<img src="'.ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE.$video["media_name"].'" alt="video">':'')) */ ?>

                        <?php
                    }
                    ?>

                </div>
                <span class="arrow full-width-arrow" style="display:<?= ($video['media_type'] == "image" ? "none" : "block") ?>"></span>
            </div>
            <div class="row-fluid">
                <div class="span12 video-description-area-accordion">
                    <div id="accordionArea" class="accordion">
                        <div class="accordion-group">
                            <div class="accordion-heading accordionize featured-description-bg">
                                <a href="#oneArea" data-parent="#accordionArea" id="feature_BGB" data-toggle="collapse" class="accordion-toggle inactive collapsed" style="background-color: <?= $this->session->userdata['featured_theme']['backgroundB'] ?>">
                                    <strong class="featured-description-text" style="color: <?= $this->session->userdata['featured_theme']['text'] ?>">Shot Breakdown</strong>
                                    <span class="font-icon-arrow-simple-down"></span>
                                </a>
                            </div>
                            <div style="height: 0px;" class="accordion-body collapse" id="oneArea">
                                <div class="accordion-inner">
                                    <div contenteditable="true" data-name="video_description" class="video-description featured-description-text" data-placeholder="Video Description"><?= ($video['media_description'] != "" ? $video['media_description'] : "Video description") ?></div>
                                    <input type="hidden" name="callfrom" value="video_desc"/>
                                    <input type="hidden" name="item_id" id="item_id" value="<?= $video['image_id'] ?>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row-fluid video_note_rowfluid">
                <div class="span12 note-own-dont" style="display: <?= ($video['media_type'] == "video" && $video != "" ? "block" : "none") ?>">
                    <p class="text-center dont-own-note"><a href="javascript:void(0);" class="dont-own-video video_already_added" style="color: <?= $this->session->userdata['featured_theme']['text'] ?>">Don't own a video, display a full-width image instead.</a></p>
                    <a class="btn-flat white btn-edit-video">EDIT VIDEO</a>
                </div>
            </div>
        </div>
    </div>
</section>

<section data-callfrom="latestwork" class="dropzone-uploader text" id="latestwork_dropdown_notearea" style="display:<?= ($latestWorkImagesColumn1 != "" ? "none" : "block") ?>"> <?php /* <!--id="gallery-box"--> */ ?>
    <div class="file-dropper-parent latestwork_dropdown_notearea_file_dropper">
        <div class="file-dropper-child">
            <div class="dropbox drag-btn">
                <img src="../assets/img/cloud-drop.png" alt="dropbox">
                <h5>DROP FILES HERE</h5>
            </div>
        </div>
    </div>
    <div class="container">

        <div class="row-fluid">
            <div class="span12">
                <div class="gallery-best-container">
                    <?php /* <i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark <?=($latestWorkImagesColumn2!="" && $latestWorkImagesColumn2==""?"complete-mark":"uncomplete-mark")?>"></i> */ ?>
                    <div class="latestworks_parentupload_controlls">
                        <?php
                        //$latestwork_portfolioimages=null;
                        $filter_latestwork_portfolioimages = array_filter($latestwork_portfolioimages);
                        if (is_array($latestwork_portfolioimages) && !empty($filter_latestwork_portfolioimages)) {
                            $latestwork_upload_notes_class = ' hide';
                        } else {
                            $latestwork_upload_notes_class = '';
                        }
                        ?>
                        <div class="latestwork_upload_notes<?= $latestwork_upload_notes_class; ?>">

                            <h3 class="text-center">Latest Work</h3>
                            <h1 class="text-center embed-image-ico-support"><i class="icon-picture"></i></h1>
                            <h5 class="text-center">Create a gallery of your best images</h5>
                            <p class="text-center">Add eye-catching images that really show off your skills</p>
                        </div>
                        <button class="button-main button-large btn-theme-anb btn-drag drag-btn dragdropbtn" id="open_gallery_modal" type="button">
                            DRAG IMAGES HERE
                        </button>
                        <p class="text-center text-grey">or click to upload</p>
                        <div class="hide">
                            <input type="file" id="dropbox-file-input" name="file-input" multiple="">
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <span class="arrow"></span>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <!--<div id="dropbox">
                <div class="text">
                        Drop Images Here
                </div>
        </div>-->
    <span class="upload-progress"></span>
</section>

<section class="pad_sec" id="services-box">

    <?php /* <section data-callfrom="latestwork" class="margin-80 dropzone-uploader latestwork_div text" id="dropbox00" style="display:<?=($latestWorkImagesColumn1=="" && $latestWorkImagesColumn2==""?"none":"block")?>"> */ ?>


    <?php
    if (is_array($latestwork_portfolioimages) && !empty($filter_latestwork_portfolioimages)) {
        $show_hide_latestwork = '';
    } else {
        $show_hide_latestwork = ' hide ';
    }
    ?>


    <div class="container latestwork_parent<?= $show_hide_latestwork; ?>" id="latest-on-images-dropbox">
        <div class="portfolio-cloud-dropper"></div>
        <div class="row-fluid">

            <div class="span12">
                <h3>Latest Works</h3>
            </div>
        </div>
        <!--<div id="loading-preview"></div>-->
        <div class="row-fluid">
            <div id="portfolio-image-column" class="span12 sortable-images">
                <?php
                foreach ($latestwork_portfolioimages as $key => $v) {
                    $ordercol1 = $v['media_sortorder'];
                    $media_imagesection = $v['media_imagesection'];
                    //IF EVEN ORDER NUM THEN SKIP LOOP;
                    ?>
                    <div class="cell portfolio_latestwork_id" id="portfolioimage-<?= $v['image_id'] ?>-<?= $v['media_sortorder'] ?>" style="">
                        <div class="latest-work-holder">
                            <img style="width:100%;height:100%;" src="<?= ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $v['media_name'] ?>">

                            <div class="latest-work-disp" style="background-color: <?= $this->session->userdata['gallery_theme']['overlay'] . ',0.5' ?>">
                                <ul class="user-port-work-ops">
                                    <li>
                                        <a href="javascript:void(0);" class="customize_order"><i class="icon-pencil"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
    </div>
</section>
<script type="text/javascript">
<?php /*
  var column1_limit_start = '<?=$latestWorkImagesColumn1Start?>';
  var column2_limit_start = '<?=$latestWorkImagesColumn2Start?>';
  $(document).on('click', '.btn-load-more-portfolio', function () {
  var $btn = $(this);
  $btn.button('loading');

  $.ajax({
  type: 'POST',
  data: {
  'callfrom': 'loadmore_images',
  'column1': column1_limit_start,
  'column2': column2_limit_start
  },
  url: '<?=$vObj->getURL("profile/save_profile");?>',
  dataType: 'json',
  success: function (data) {
  $btn.button('reset');
  $(data.column1_images).each(function (i, obj) {
  var html = '<div class="cell portfolio_latestwork_id" id="portfolioimage-' + obj.image_id + '-' + obj.media_sortorder + '"><div class="latest-work-holder"><img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>' + obj.media_name + '"><div class="latest-work-disp"><ul class="user-port-work-ops"><li><a href="javascript:void(0);"  class="sortable-description-event"><i class="icon-pencil"></i></a></li><li><a href="javascript:void(0);" class="enable-sortable-event"><i class="icon-random"></i></a></li><li><a href="javascript:void(0);" class="sortable-remove-event"><i class="icon-remove-circle"></i></a></li></ul><div style="display: none;" class="image-disp-container row-fluid sortable-image-description-container"><div class="span12"><input type="text" name="media_title" placeholder="Title" id="media_title" class="span12  sortable-image-title-text" value="' + obj.media_title + '"/><input type="text" name="media_tag" placeholder="Tag" id="media_tag" class="span12  sortable-image-tag-text" value="' + obj.media_tag + '"/><textarea class="form-control span12 sortable-image-description-textarea"  placeholder="Description">' + obj.media_description + '</textarea><button class="btn btn-default pull-right new-nav-btn1-color sortable-image-description-textarea-btn">Save</button></div></div></div></div></div>'
  $('#portfolio-image-column1').append(html);
  });
  $(data.column2_images).each(function (i, obj) {
  var html = '<div class="cell portfolio_latestwork_id" id="portfolioimage-' + obj.image_id + '-' + obj.media_sortorder + '"><div class="latest-work-holder"><img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>' + obj.media_name + '"><div class="latest-work-disp"><ul class="user-port-work-ops"><li><a href="javascript:void(0);"  class="sortable-description-event"><i class="icon-pencil"></i></a></li><li><a href="javascript:void(0);" class="enable-sortable-event"><i class="icon-random"></i></a></li><li><a href="javascript:void(0);" class="sortable-remove-event"><i class="icon-remove-circle"></i></a></li></ul><div style="display: none;" class="image-disp-container row-fluid sortable-image-description-container"><div class="span12"><input type="text" name="media_title" placeholder="Title" id="media_title" class="span12  sortable-image-title-text" value="' + obj.media_title + '"/><input type="text" name="media_tag" placeholder="Tag" id="media_tag" class="span12  sortable-image-tag-text" value="' + obj.media_tag + '"/><textarea class="form-control span12 sortable-image-description-textarea"  placeholder="Description">' + obj.media_description + '</textarea><button class="btn btn-default pull-right new-nav-btn1-color sortable-image-description-textarea-btn">Save</button></div></div></div></div></div>'
  $('#portfolio-image-column2').append(html);
  });
  column1_limit_start = data.column1_start;
  column2_limit_start = data.column2_start;
  }
  });
  }); */ ?>
    $(document).on('click', '.sortable-remove-event', function () {
        var obj = $(this);
        var this_parent_ele = $(this).closest('.portfolio_latestwork_id');
        var str_id = this_parent_ele.attr('id');
        //$(this).closest('.portfolio_latestwork_id');
        var this_ele = obj;
        var this_class = this_ele.attr('class');
        var top_parent_ele_left = this_ele.closest('#portfolio-image-column1');
        var top_parent_ele_right = this_ele.closest('#portfolio-image-column2');
        var action_from_column = null;
        if (typeof top_parent_ele_left.prop('tagName') !== 'undefined') {
            var reorder_images_target = $('#portfolio-image-column1');
            action_from_column = 1;
        } else if (typeof top_parent_ele_right.prop('tagName') !== 'undefined') {
            var reorder_images_target = $('#portfolio-image-column2');
            action_from_column = 2;
        }
        if (action_from_column == null) {
            error.log('invalid column data');
            return false;
        }
        var arr_id = str_id.split('-');
        var image_id = arr_id[1];

        var re_order_idz = Array();
        var re_order_num = Array();
        var incre = 0;

        reorder_images_target.find('.portfolio_latestwork_id').each(function () {

            var incre_ele = $(this);
            var this_ele_id = incre_ele.attr('id');
            var this_ele_id_num = this_ele_id.split('-')[1];
            if (this_ele_id != str_id) {
                incre++;
            }
            var new_id_attr = null;
            new_id_attr = 'portfolioimage-' + this_ele_id_num + '-' + incre;
            if (this_ele_id != str_id && (typeof this_ele_id_num !== 'undefined' || this_ele_id_num != null)) {
                re_order_idz[incre] = this_ele_id_num;
                re_order_num[incre] = incre;
                //re_order_num[] = incre;
            }

        })



        $.ajax({
            type: 'POST',
            data: {
                'image_id': image_id,
                'callfrom': 'latestwork_remove',
                'action_from_column': action_from_column,
                're_order_idz': re_order_idz,
                're_order_num': re_order_num


            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            //dataType: 'json',
            success: function (data) {
                if (is_JSON(data)) {
                    //data = $(data).parseJSON();
                    data = $.parseJSON(data)

                }
                this_parent_ele.remove();
                var incre = 0;
                reorder_images_target.find('.portfolio_latestwork_id').each(function () {
                    incre++;
                    var incre_ele = $(this);
                    var this_ele_id = incre_ele.attr('id');
                    var this_ele_id_num = this_ele_id.split('-')[1];
                    var new_id_attr = null;
                    new_id_attr = 'portfolioimage-' + this_ele_id_num + '-' + incre;
                    incre_ele.attr('id', new_id_attr);
                })

<?php /*
  Z-STYLE IMAGE SCRIPT COMMENTED - START
  //var parent_ele =$(obj).closest('#portfolio-image-column1');
  var this_parent_box = this_ele.closest('.portfolio_latestwork_id');
  var this_parent_box_index = this_parent_box.index();
  var this_ele_img_src = this_parent_box.find('img').attr('src');
  var col_left_ele = $('#portfolio-image-column1');
  var col_right_ele = $('#portfolio-image-column2');
  //this_parent_box.addClass('removeit_forsortorder');
  var top_parent_ele_left = this_ele.closest('#portfolio-image-column1');
  var top_parent_ele_right = this_ele.closest('#portfolio-image-column2');
  var last_left_index = col_left_ele.find('.portfolio_latestwork_id:last').index();
  var last_right_index = col_right_ele.find('.portfolio_latestwork_id:last').index();
  var right_side_html = '';
  var left_side_html = '';

  //            if(typeof top_parent_ele_left.prop('tagName')!=='undefined'){
  //             //LEFT HTML;
  //             this_parent_box.nextAll().each(function (index, ele){
  //             console.log(ele);
  //
  //             //var html_ele_clone = ele.clone();
  //
  //             //console.log(html_ele_clone);
  //             right_side_html+=html_ele_clone;
  //             //console.log(ele.find('img').attr('sc'));
  //             })
  //             console.log(right_side_html);
  //             } else if(typeof top_parent_ele_right.prop('tagName')!=='undefined'){
  //             //RIGHT HTML;
  //             }
  //
  //
  //             undefined_abc;

  if (typeof top_parent_ele_left.prop('tagName') !== 'undefined') {
  var parent_ele = top_parent_ele_left;
  var left_loop_len = last_left_index - this_parent_box_index;
  var right_loop_len = last_left_index - this_parent_box_index;

  left_loop_len = left_loop_len + 1;
  //console.log(left_loop_len+'___left_loop_len');
  for (left_inc = 0; left_inc <= left_loop_len; left_inc++) {
  var lefttarg_eq = left_inc + this_parent_box_index;
  //console.log(lefttarg_eq+'__lefttarg_eq');
  var left_target_ele = $('#portfolio-image-column1').find('.portfolio_latestwork_id').eq(lefttarg_eq);
  left_target_ele.addClass('removeit_forsortorder');
  var left_attr_img_src = left_target_ele.find('img').attr('src');
  var left_ele_class_attr = left_target_ele.attr('class');
  var left_target_ele_clone = left_target_ele.clone();

  if (typeof left_target_ele_clone[0] === 'undefined') {
  //console.log(lefttarg_eq+'__UNDEFINED_lefttarg_eq');
  } else if (left_attr_img_src == this_ele_img_src) {
  //console.log('reached same ele');
  } else {
  right_side_html += left_target_ele_clone[0].outerHTML;
  }
  }

  for (right_inc = 0; right_inc <= right_loop_len; right_inc++) {
  var righttarg_eq = right_inc + this_parent_box_index;
  var right_target_ele = $('#portfolio-image-column2').find('.portfolio_latestwork_id').eq(righttarg_eq);
  right_target_ele.addClass('removeit_forsortorder');
  var right_attr_img_src = right_target_ele.find('img').attr('src');
  var right_ele_class_attr = right_target_ele.attr('class');
  var right_target_ele_clone = right_target_ele.clone();

  if (typeof right_target_ele_clone[0] === 'undefined') {
  //console.log(lefttarg_eq+'__UNDEFINED_lefttarg_eq');
  } else if (right_attr_img_src == this_ele_img_src) {
  //console.log('reached same ele');
  } else {
  left_side_html += right_target_ele_clone[0].outerHTML;
  }
  }



  } else if (typeof top_parent_ele_right.prop('tagName') !== 'undefined') {
  //console.log(this_parent_box_index+'__this_parent_box_index');
  //console.log(last_left_index+'__last_left_index');


  var left_loop_len = last_left_index - this_parent_box_index;
  var right_loop_len = last_right_index - this_parent_box_index;

  //this_parent_box;
  //this_parent_box_index;

  for (left_inc = 0; left_inc <= left_loop_len; left_inc++) {
  var lefttarg_eq = left_inc + this_parent_box_index + 1;
  var left_target_ele = $('#portfolio-image-column1').find('.portfolio_latestwork_id').eq(lefttarg_eq);
  left_target_ele.addClass('removeit_forsortorder');
  var left_attr_img_src = left_target_ele.find('img').attr('src');
  var left_ele_class_attr = left_target_ele.attr('class');
  var left_target_ele_clone = left_target_ele.clone();

  if (typeof left_target_ele_clone[0] === 'undefined') {
  //console.log(lefttarg_eq+'__UNDEFINED_lefttarg_eq');
  } else if (left_attr_img_src == this_ele_img_src) {
  //console.log('reached same ele');
  } else {
  right_side_html += left_target_ele_clone[0].outerHTML;
  }
  }

  for (right_inc = 0; right_inc <= right_loop_len; right_inc++) {
  var righttarg_eq = right_inc + this_parent_box_index + 1;
  //console.log(righttarg_eq+'__righttarg_eq');
  var right_target_ele = $('#portfolio-image-column2').find('.portfolio_latestwork_id').eq(righttarg_eq);
  right_target_ele.addClass('removeit_forsortorder');
  var right_attr_img_src = right_target_ele.find('img').attr('src');
  var right_ele_class_attr = right_target_ele.attr('class');
  var right_target_ele_clone = right_target_ele.clone();

  if (typeof right_target_ele_clone[0] === 'undefined') {
  //console.log(lefttarg_eq+'__UNDEFINED_lefttarg_eq');
  } else if (right_attr_img_src == this_ele_img_src) {
  //console.log('reached same ele');
  } else {
  left_side_html += right_target_ele_clone[0].outerHTML;
  }
  }


  }

  $(".removeit_forsortorder").remove();

  this_parent_box.remove();

  if (typeof left_side_html !== 'undefined' && left_side_html != null) {
  if (typeof $('#portfolio-image-column1').find('.portfolio_latestwork_id:last').prop('tagName') !== 'undefined') {
  $('#portfolio-image-column1').find('.portfolio_latestwork_id:last').after(left_side_html);
  } else {
  $('#portfolio-image-column1').append(left_side_html);
  }

  }

  if (typeof right_side_html !== 'undefined' && right_side_html != null) {
  if (typeof $('#portfolio-image-column2').find('.portfolio_latestwork_id:last').prop('tagName') !== 'undefined') {
  $('#portfolio-image-column2').find('.portfolio_latestwork_id:last').after(right_side_html);
  } else {
  $('#portfolio-image-column2').append(right_side_html);
  }
  }


  //
  //             jQuery("#portfolio-image-column1").find(".portfolio_latestwork_id").each(function (){
  //             var this_ele =  jQuery(this);
  //             console.log(this_ele.attr('id'));
  //             });
  //             *

  $(".removeit_forsortorder").removeClass('removeit_forsortorder');





  Z-STYLE IMAGE SCRIPT COMMENTED - END
 */ ?>
                ///

                //this_parent_box.remove();
                ///parent_ele.remove();
                //return false;
                //
                //$(obj).closest('.portfolio_latestwork_id').remove();



//                setTimeout(function () {
//                    $(".sortable-remove-event").removeAttr('disabled').removeClass('disabled');
//                    var leftimage_len = $('#portfolio-image-column1').find('.portfolio_latestwork_id').length;
//                    var rightimage_len = $('#portfolio-image-column2').find('.portfolio_latestwork_id').length;
//                    if ((leftimage_len <= 0 || typeof leftimage_len === 'undefined') && (rightimage_len <= 0 || typeof rightimage_len === 'undefined')) {
//                        $(".latestwork_parent").hide();
//                        $(".latestwork_upload_notes").show();
//                    }
//                }, 500);

                if (data.cnt == 0) {
                    $("#services-box").hide();
                }
                console.log(data.scorer.resume_scorer);
                console.log(data.scorer.portfolio_scorer);
                console.log(data.scorer.profile_scorer);
                console.log(data.scorer.incomplete);

                if (data.scorer.resume_scorer + data.scorer.portfolio_scorer + data.scorer.profile_scorer > "74")
                {
                    $('.custom-p-color').html('Congratulations, you can publish your profile!');
                } else
                {
                    $('.custom-p-color').html('Reach 75% and get published!');
                }
                if (data.scorer.resume_scorer == "" || data.scorer.resume_scorer == "0")
                {
                    var resume_color = "#F9785B";
                }
                else
                {
                    var resume_color = "#76bdee";
                }
                if (data.scorer.portfolio_scorer == "" || data.scorer.portfolio_scorer == "0")
                {
                    var portfolio_color = "#F9785B";
                }
                else
                {
                    var portfolio_color = "#c4dafe";
                }
//            new Morris.Donut({
//                element: 'hero-donut',
//                width: 300,
//                data: [
//                    {label: 'Profile', value: data.scorer.profile_scorer },
//                    {label: 'Resume', value: data.scorer.resume_scorer },
//                    {label: 'Portfolio', value: data.scorer.portfolio_scorer },
//                    {label: 'Incomplete', value: data.scorer.incomplete }
//                ],
//                colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                formatter: function (y) {
//                    return y + "%"
//                }
//            })
                var prof_comp = parseInt(data.scorer.profile_scorer) + parseInt(data.scorer.resume_scorer) + parseInt(data.scorer.portfolio_scorer);
                update_profile_completness_top_chart(prof_comp);
//            $("#score-chart-input-field").val(data.scorer.fullstatus);
//            $(".knob").knob();
//            $("#score-chart-input-field").val(data.scorer.fullstatus + "%");
                publish_unpublish_btn(data.scorer.fullstatus);
            }
        });
    });
    $(document).on('click', '.sortable-description-event', function () {
        var str_id = $(this).closest('.portfolio_latestwork_id').attr('id');
        var arr_id = str_id.split('-');
        var image_id = arr_id[1];
        $(this).closest(".user-port-work-ops").hide();
        $(this).closest('ul').next('.sortable-image-description-container').show();
        $(this).closest('ul').next('.sortable-image-description-container').show().find('.sortable-image-title-text').focus();

        //$(this).closest('ul').next('div')
    });


    $(document).on('keyup', '.sortable-image-title-text, .sortable-image-tag-text, .sortable-image-description-textarea', function (eve) {
        if (eve.keyCode == 27) {
            $(this).closest('.sortable-image-description-container').hide();
            $(this).closest(".latest-work-disp").find(".user-port-work-ops").show();
            $('.sortable-image-title-text').focus(false);
        }
    });

    $(document).on('click', '.sortable-image-description-textarea-btn', function () {
        var this_ele = $(this);
        var str_id = $(this).closest('.portfolio_latestwork_id').attr('id');
        var arr_id = str_id.split('-');
        var image_id = arr_id[1];
        var description = $(this).prev('.sortable-image-description-textarea').val();
        var image_title = $(this).parent().find('.sortable-image-title-text').val();
        //var image_tag = $(this).parent().find('.sortable-image-tag-text').val();
        var select_two_id = $(this).parent().find('.select2-container').attr('id');
        var image_tag = null;
        if (typeof select_two_id !== 'undefined') {
            image_tag = $("#" + select_two_id).select2("val");
        } else {
            image_tag = '_undefined!_';
        }

        var obj = this;
        $.ajax({
            type: 'POST',
            data: {
                'image_id': image_id,
                'description': description,
                'image_title': image_title,
                'image_tag': image_tag,
                'callfrom': 'latestwork_description'

            },
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (data) {
                $(obj).closest('.sortable-image-description-container').hide();
                $(".user-port-work-ops").show();
            }
        });


    });
    $(document).ready(function () {
<?= ($video['media_name'] != "" && $video['media_type'] == "video") ? "$('.full-width-image-section-fields').hide();" : (($video["media_name"] != "" && $video['media_type'] == "image") ? "$('.embed-video-section').hide();$('.video-description-area-accordion').hide();" : "$('.full-width-image-section-fields').hide();") ?>

    });
    $(document).on('click', '.dont-own-video', function () {
        var this_ele = $(this);
        if (this_ele.hasClass('video_already_added')) {
            $("#intro-box-dropdownft").hide();

            $("#full-width-box").show();



            /*//embed-video-section-iframe
             //video_note_rowfluid

             intro-box_featured_bg featured-bg view-vid-img


             $(".embed-video-section-iframe, .video_note_rowfluid").hide();
             $(".drag-img-container").show();
             $('.video-description-area-accordion').panret().hide(); */
            ///drag-img-container
            return;
        } else if (this_ele.hasClass('dontownavideo')) {
            $("#intro-box-dropdownft").hide();
            $("#full-width-box").show();
            /*
             var video_container = $(".video-container");
             var video_container_tagname = video_container.prop("tagName");
             if (typeof video_container_tagname !== 'undefined') {
             $("#intro-box-dropdownft").hide();
             $("#full-width-box").show();
             //embed-video-section
             $(".embed-video-section#intro-box").hide();
             //video_container.hide();
             //            $(".embed-video-section").hide();
             //             $(".view-vid-img").show();
             //             $('.full-width-image-section-fields').hide();
             //             $('.video-description-area-accordion').hide();
             }
             return; */
        }
        //video_already_added
        if (typeof $(".view-vid-img").find("img").prop("tagName") !== "undefined" && $(".view-vid-img").find("img").is(":visible") == false) {
            $(".embed-video-section").hide();
            $(".view-vid-img").show();
            $('.full-width-image-section-fields').show();
            $('.video-description-area-accordion').hide();
        } else {
            $('.embed-video-section').hide();
            $('.view-vid-img').hide();
            $('.video-description-area-accordion').hide();
            $('.full-width-image-section-fields').show();
            $('.video-plugin-location').removeClass("video-container");
            //console.log('reached###');
        }
    });

    $(document).on('click', '.embed-video-click', function () {
        $('.embed-video-section').show();
        $('.video-description-area-accordion').show();
        $('.full-width-image-section-fields').hide();
        $('.embed-video-section').find('.video-container').removeClass("video-container");
    });

    $(document).on('click', '.btn-edit-video', function () {
        $('.embed-video-section').show();
        $('.embed-video-section').find('.video-container').removeClass("video-container");
        //$('.video-description-area-accordion').show();
        $('.view-vid-img').hide();
    });

    $(document).on('click', '.video-section-embed-url-btn', function () {
        var url = $.trim($('.video-section-embed-url-field').val());
        var video_type = $('.video-section-combo-selected-val').val();
        /*if(checkUrl(url)=="Youtube" && video_type=="youtube"){
         alert("YOUTUBE")
         }
         else if(checkUrl(url)=="Vimeo" && video_type=="vimeo"){
         alert("VIMEO")
         }
         else{
         alert("Please provide");
         }*/
        if (url != "") {
            var console_check = null;
            var boolean_video_checker = false;
            var playerurl = "";
            var media_url = "";
            if (ytVidId(url) != false && video_type == "youtube") {
                boolean_video_checker = true;
                playerurl = url;
                var u_tube_url = playerurl;
                u_tube_url = u_tube_url.replace('watch?v=', 'embed/');
                media_url = u_tube_url;
                playerurl = media_url;
            }
            else if (vimeoVidId(url) != false && video_type == "vimeo") {
                boolean_video_checker = true;
                playerurl = "//player.vimeo.com/video/" + vimeoVidId(url);
                media_url = url;
            }
            else {
                if (checkUrl(url) == "Vimeo" && video_type == "vimeo") {
                    boolean_video_checker = true;
                    playerurl = "//player.vimeo.com/video/" + vimeoVidId('http://vimeo.com/' + url);
                    media_url = 'http://vimeo.com/' + url;
                } else if (checkUrl(url) == "Youtube" && video_type == "youtube") {
                    alert('reached');
                    boolean_video_checker = true;
                    playerurl = 'http://youtube.com/' + url;
                    var u_tube_url = playerurl;
                    u_tube_url = u_tube_url.replace('watch?v=', 'embed/');
                    media_url = u_tube_url;
                    playerurl = media_url;
                } else {

                    boolean_video_checker = false;
                }
            }

            if (boolean_video_checker) {
                $('.loader-save-holder').show();
                $.ajax({
                    type: 'POST',
                    data: {
                        'video_url': playerurl,
                        'video_desc': '',
                        'media_type': 'video',
                        'video_type': video_type,
                        'media_url': media_url,
                        'callfrom': 'video_url'
                    },
                    url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                    dataType: 'json',
                    success: function (data) {
                        $('.loader-save-holder').hide();
                        //$(".view-vid-img").removeClass("default_padd");
                        $(".view-vid-img").removeClass("default_padd no-backgound");
                        $(".view-vid-img").find('img').remove();
                        $('.view-vid-img').show();
                        $(".full-width-arrow").show();
                        $(".accordion-heading").show();
                        $('.note-own-dont').show();
                        $('.embed-video-section').hide();
                        $('.drag-img-container').hide();

                        //$('.video-plugin-location').html('<iframe class="embed-video-section-iframe" src="' + playerurl + '"></iframe>');
                        //<iframe width="560" height="315" src="//www.youtube.com/embed/nOEw9iiopwI" frameborder="0" allowfullscreen></iframe>
                        $(".embed-video-section-iframe").remove();
                        $('.video-plugin-location .drag-img-container').after('<iframe class="embed-video-section-iframe" src="' + playerurl + '"></iframe>');
                        $(".video_note_rowfluid").show();
                        ///

                        $('.video-plugin-location').addClass("video-container");
                        $('.complete-tick-mark').css("color", "#96BF48");
                        if (data.scorer.resume_scorer + data.scorer.portfolio_scorer + data.scorer.profile_scorer > "74")
                        {
                            $('.custom-p-color').html('Congratulations, you can publish your profile!');
                        } else
                        {
                            $('.custom-p-color').html('Reach 75% and get published!');
                        }
                        if (data.scorer.resume_scorer == "" || data.scorer.resume_scorer == "0")
                        {
                            var resume_color = "#F9785B";
                        }
                        else
                        {
                            var resume_color = "#76bdee";
                        }
                        if (data.scorer.portfolio_scorer == "" || data.scorer.portfolio_scorer == "0")
                        {
                            var portfolio_color = "#F9785B";
                        }
                        else
                        {
                            var portfolio_color = "#c4dafe";
                        }
//                    new Morris.Donut({
//                        element: 'hero-donut',
//                        width: 300,
//                        data: [
//                            {label: 'Profile', value: data.scorer.profile_scorer },
//                            {label: 'Resume', value: data.scorer.resume_scorer },
//                            {label: 'Portfolio', value: data.scorer.portfolio_scorer },
//                            {label: 'Incomplete', value: data.scorer.incomplete }
//                        ],
//                        colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                        formatter: function (y) {
//                            return y + "%"
//                        }
//                    });
                        var prof_comp = parseInt(data.scorer.profile_scorer) + parseInt(data.scorer.resume_scorer) + parseInt(data.scorer.portfolio_scorer);
                        update_profile_completness_top_chart(prof_comp);
//                    $("#score-chart-input-field").val(data.scorer.fullstatus);
//                    $(".knob").knob();
//                    $("#score-chart-input-field").val(data.scorer.fullstatus + "%");
                        publish_unpublish_btn(data.scorer.fullstatus);
                    }
                });
            }
            else {
                alert("This is not a correct url for " + video_type);
            }

        }
        else {

            $('.video-section-embed-url-field').css({
                border: '1px solid red'
            });
            $('.video-section-embed-url-field').focus();
            return false;

        }
    });
    $(document).on('click', '.video-combo', function () {
        var selected = $(this).attr('data-item-id');
        $('.video-section-combo-selected-val').val(selected);
        var src = '';
        if (selected == 'vimeo')
            src = 'font-icon-social-vimeo';
        if (selected == 'youtube')
            src = 'font-icon-social-youtube';
        $(this).closest('.btn-group').find('.network-btn').html('<i class="' + src + '">');

    });

    $(document).on('click', '.drag-btn', function () {
        $(this).closest('.dropzone-uploader').find('.dz-message').trigger('click');
    });

    $(document).on('click', '.btn-edit-vid', function () {
        var hid_image = $("#edit-video-fullimg").val();
        if (hid_image == "image") {
            $("#full-width-box").show();
        }
        else {
            $(".embed-video-section").show();
        }
        $(".view-vid-img").hide();
    });

    $(document).ready(function () {
        $(document).delegate('.dragdropbtn', 'click', function () {
            $("#dropbox-file-input").click();
        });


        $(".dropzone-uploader").each(function () {
            var callfrom = $(this).attr("data-callfrom");
            if (callfrom != "latestwork") {
                $(this).dropzone({clickable: true, url: "<?= $vObj->getURL("profile/save_profile") . "/callfrom/"; ?>" + callfrom});
            }

        });
    });


    $(document).on('focus', '*[contenteditable=true]', function (e) {
        $(this).selectText()
        //document.execCommand('selectAll',false,null);
    });

    $(document).on('blur', '*[contenteditable=true]', function () {
        var placeholder = $(this).attr('data-placeholder');
        var text = $.trim($(this).text());
        if (text == "") {
            $(this).text($(this).attr('data-placeholder'));
        }
    });

    $.fn.selectText = function () {
        var obj = this;
        setTimeout(function () {
            var doc = document;
            var element = obj[0];
            if (doc.body.createTextRange) {
                var range = document.body.createTextRange();
                range.moveToElementText(element);
                range.select();
            } else if (window.getSelection) {
                var selection = window.getSelection();
                var range = document.createRange();
                range.selectNodeContents(element);
                selection.removeAllRanges();
                selection.addRange(range);
            }
        }, 10);

    };
    $(document).on('blur', '.video-description', function () {
        var data = {};
        $('.loader-save-holder').show();
        $(this).closest('.video-description').find('*[contenteditable]');
        var is_editable_element = $(this).attr("contenteditable");
        if (is_editable_element == "true") {
            var attr_name = $(this).attr('data-name');

            if (attr_name == "video_description") {
                data['video_description'] = $(this).html();
                data['callfrom'] = "video_desc";
                data['item_id'] = "<?= $video['image_id'] ?>";
            }
        }
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    $('.loader-save-holder').hide();
                }
            }
        });

    });
<?php /* ///jQuery(".margin-80.dropzone-uploader.text").fadeIn(); //ADDED BY FAISAL CODE WILL BE REMOVED AFTER TESTING..... */ ?>
    function checkUrl(test_url) {
        var testLoc = document.createElement('a');
        testLoc.href = test_url.toLowerCase();
        url = testLoc.hostname;
        var what;
        if (url.indexOf('youtube.com') !== -1) {
            what = 'Youtube';
        } else if (url.indexOf('vimeo.com') !== -1) {
            what = 'Vimeo';
        } else {
            what = 'None';
        }
        return what;
    }

    jQuery(".latest-work-disp").hover(function () {
        portfolioImageRescale();
    });

</script>
<div style="display: none;" class="modal model-gallary-order modal_main_container" id="model_gallary_order" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding-bottom: 20px;padding-top:20px;">
                <h3 class="modal-title" >Edit/Move/Delete</h3>
            </div>
            <div class="modal-body" style="max-height: inherit">
                <div class="gallary-container">
                    <ul id="sortable" class="gallery-list sort-image-container">
                        <?php
                        foreach ($latestwork_portfolioimages as $v) {
                            ?>
                            <li class="ui-state-default image-holder-item" id="image-holder-item-<?= $v['image_id'] ?>">
                                <a class="remove-media remove-media-image"><i class="icon-trash"></i></a>
                                <div class="caption-edit-box">
                                    <textarea class="cpt-text">This is caption of image</textarea>
                                    <a class="cpt-close">close</a>&nbsp;&nbsp;&nbsp;<a class="cpt-save">save</a>
                                </div>
                                <div class="gallery-image-div">
                                    <img class="gall-img" src="<?= ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $v['media_name'] ?>" />
                                </div>
                                <p class="image-caption-text">This is caption of image</p>
                                <div class="desc-loader"><img src="<?= ASSETS_PATH ?>img/ajax-loader-small.gif"/></div>
                            </li>
                            <?php
                        }
                        ?>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white" data-dismiss="modal" id="save_gallery_close">Close</button>
                <button type="button" class="btn btn-success btn-save model-btn-save" id="save_gallary_order" value="Save changes">SAVE CHANGES</button>
                <!--                <button type="button" class="btn btn-flat white btn-save-gallary-order" >Save</button>
                                <button type="button" class="btn btn-flat white" data-dismiss="modal">Cancel</button>-->
            </div>
        </div>
    </div>
</div>
<script>
    var order = new Array();
    var open_upload_file = true;
    var is_gallery_modal_open = false;
    $(window).load(function () {
        var masonary_container = document.querySelector('#portfolio-image-column');
        var msnry = new Masonry(masonary_container, {
            itemSelector: '.portfolio_latestwork_id'
        });
        
        imagesLoaded(masonary_container, function() {
            msnry.layout();
            $(window).trigger('resize');
        });
    });

    $(window).resize(function () {
        if (is_gallery_modal_open) {
            resize_modal_thumbnails();
        }
    });

    $(document).ready(function () {
        customize_order_of_image();


        $('#model_gallary_order').on('shown', function () {
            resize_modal_thumbnails();
            is_gallery_modal_open = true;
        });

        $('#model_gallary_order').on('hide', function () {
            is_gallery_modal_open = false;
        });

        $('#save_gallary_order').click(function () {
            //console.log(order);
            $('#model_gallary_order').modal('hide');
            $('.loader-save-holder').show();
            save_portfolio_order(true);
        });
        model_image_actions();
    });

    var open_modal_timeout = 0;

    function customize_order_of_image() {
        $('.customize_order').unbind('click').click(function () {
            open_upload_file = false;
            load_gallery_modal();
        });
    }

    function load_gallery_modal() {
        $('.loader-save-holder').show();
        if (total_images > 0) {
            open_modal_timeout = setTimeout(function () {
                load_gallery_modal();
            }, 2000);
            return false;
        } else if (open_modal_timeout != 0) {
            clearTimeout(open_modal_timeout);
            open_modal_timeout = 0;
        }
        var data = {};
        data['callfrom'] = "get_user_media_to_sort";
        var full_width_image_path = $('#full_width_image_path').val();
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    $('#sortable').empty();
                    order = new Array();
                    $.each(responseData.images, function (index, value) {
                        var li = $("<li class='ui-state-default custom_modal_img' id='image-holder-item-" + value.image_id + "'></li>");

                        var img_div = $('<div class="thumbnail_img" style="background-image:url(\'' + full_width_image_path + "modal_" + value.media_name + '\');"</div>');
                        //<div class="thumbnail" style="background-image:url(\''++ full_width_image_path + "300x300_" + value.media_name + '\');"
//                        img_div.addClass('img_div');
//                        img_div.css('background-image',"url('"+full_width_image_path + "300x300_" + value.media_name+"')");
                        li.append(img_div);
                        //li.append($('<img src="' + full_width_image_path + "300x300_" + value.media_name + '" /><div class="gallery-save-loader"><i class="icon-spinner icon-spin"></i></div>'));
                        li.append($('<a class="remove-media remove-media-image"><i class="icon-trash"></i></a>'));
                        li.append($('<textarea class="gallery-image-description" placeholder="Add a caption">' + value.media_description + '</textarea><input type="hidden" class="description-value" value="' + value.media_description + '"/> <div class="gallery-image-description-btns"><button class="gallery-description-btn save-description cpt-save">Add</button><button class="gallery-description-btn cancel-description-save">Cancel</button></div>'));
                        order.push(value.image_id);
                        $('#sortable').append(li);

                    });
                    sortable();
                    model_image_actions();
                    $('.loader-save-holder').hide();
                    $('#model_gallary_order').modal('show');
                }
            }
        });

    }

    function resize_modal_thumbnails() {
        var content_height = $('#model_gallary_order').height() - 70 - 60 - 50;
        $('#model_gallary_order').children('.modal-dialog').children('.modal-content').children('.modal-body').children('.gallary-container').css('max-height', content_height);
        $('#model_gallary_order').children('.modal-dialog').children('.modal-content').children('.modal-body').children('.gallary-container').css('min-height', content_height);
    
        var new_height = 0;
        $.each($('.gallery-list').children(), function (index, li_ele) {
            var ele = $(li_ele);
            var orig_ratio = 253 / 300;
            var width = ele.width();
            if(new_height == 0){
                new_height = parseInt(width / orig_ratio);
            }
            ele.height(new_height);
            ele.children('.gallery-image-description').width(width - 24);
        });
    }

    function sortable() {
        $("#sortable").sortable({
            placeholder: "myDragDropHighLight",
            zIndex: 9999,
            helper: 'clone',
            start: function(e, ui){
                ui.placeholder.height(ui.item.height());
            },
            update: function (event, ui) {
                order = new Array();
                $('#sortable li').each(function () {
                    var arr = $(this).attr('id').split('-');
                    order.push(arr[3]);
                });
            }
        });
    }
    var images_to_be_loaded = 0;
    function save_portfolio_order(remove_deleted_images) {
        var data = {};
        var asset_full_profile_image_path = '<?= ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE ?>';
        var gallery_theme_overlay = "<?= $this->session->userdata['gallery_theme']['overlay'] ?>";
        data['callfrom'] = "save_portfolio_order";
        data['order'] = order;
        data['remove_deleted_images'] = remove_deleted_images;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    // load masonary layout again.....
                    $("#portfolio-image-column").html('');
                    var masonary_container = null;
                    var msnry;
                    $.each(responseData.lastest_image, function (index, image) {
                        var image_div = $("<div style='' id='portfolioimage-" + image['image_id'] + "-" + image['media_sortorder'] + "'></div>");
                        image_div.addClass('cell');
                        image_div.addClass('portfolio_latestwork_id');

                        var latest_work_holder = $("<div class='latest-work-holder'></div>");
                        var img_holder_gallery = $('<img src="' + asset_full_profile_image_path + image['media_name'] + '" style="width:100%;height:100%;"/>');
                        img_holder_gallery.on('load', function () {
                            if (images_to_be_loaded > 0)
                                images_to_be_loaded--;
                            if (images_to_be_loaded == 0) {
                                masonary_container = document.querySelector('#portfolio-image-column');
                                msnry = new Masonry(masonary_container, {
                                    // options
                                    itemSelector: '.portfolio_latestwork_id'
                                });
                                $(".latest-work-disp").unbind('hover').hover(function () {
                                    portfolioImageRescale();
                                });
                            }
                        });
                        latest_work_holder.append(img_holder_gallery);

                        var overlay_div = $('<div class="latest-work-disp" style="background-color:' + gallery_theme_overlay + ',0.5"><ul class="user-port-work-ops"><li><a href="javascript:void(0);" class="customize_order"><i class="icon-pencil"></i></a></li></ul></div>');
                        latest_work_holder.append(overlay_div);
                        image_div.append(latest_work_holder);
                        images_to_be_loaded++;
                        $("#portfolio-image-column").append(image_div);
                    });
                    if(masonary_container !== null){
                        imagesLoaded(masonary_container, function() {
                            msnry.layout();
                            $(window).trigger('resize');
                        });
                    }
                    if ($("#portfolio-image-column").html() != '') {
//                        imagesLoaded( document.querySelector('#container'), function( instance ) {
//                            console.log('all images are loaded');
//                        });
                        customize_order_of_image();
                    } else {
                        $(".latestwork_upload_notes").show();
                        $(".latestwork_parent").hide();
                    }
                    $('.loader-save-holder').hide();
                }
            }
        });
    }

    function save_portfolio_order_1() {
        order = new Array();
        $('#sortable li').each(function () {
            var arr = $(this).attr('id').split('-');
            order.push(arr[3]);
        });
        var data = {};
        data['callfrom'] = "save_portfolio_order";
        data['order'] = order;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (responseData) {

            }
        });
    }

    function save_portfolio_order_2() {
        order = new Array();
        $('#sortable li').each(function () {
            var arr = $(this).attr('id').split('-');
            order.push(arr[3]);
        });
        var data = {};
        data['callfrom'] = "save_portfolio_order";
        data['order'] = order;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("profile/save_profile"); ?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.status) {
                    window.location.reload();
                }
            }
        });
    }

    function model_image_actions() {

        $(".gallery-image-description").focus(function () {
            $(this).addClass('textarea-description-focus');
        });

        $(".gallery-image-description").focusout(function () {
            var li_obj = $(this).parents('li');
            if (li_obj.children('textarea').val() === li_obj.children('.description-value').val()) {
                li_obj.children('textarea').removeClass('textarea-description-focus');
            }
        });

        $('.image-caption-text').unbind('click').click(function () {
            $('.image-caption-text').show();
            $('.caption-edit-box').slideUp('slow');
            $(this).parent().children('.caption-edit-box').slideDown('slow');
            $(this).hide();
        });

        $('.cpt-save').unbind('click').click(function () {
            var that = $(this);
            var par_obj = $(this).parent().parent();
            $(this).parents('li').children('.gallery-save-loader').show();
            var text = par_obj.children('textarea').val();
            var arr = par_obj.attr('id').split('-');
            var image_id = arr[3];
            var description = text;
//           par_obj.children('.caption-edit-box').slideUp('slow');
//           par_obj.children('.desc-loader').show();
            var data = {};
            data['callfrom'] = "save_user_media_desc";
            data['image_id'] = image_id;
            data['description'] = description;
            $.ajax({
                type: 'POST',
                data: data,
                url: '<?= $vObj->getURL("profile/save_profile"); ?>',
                dataType: 'json',
                success: function (responseData) {
                    that.parents('li').children('textarea').scrollTop(0);
                    that.parents('li').children('textarea').removeClass('textarea-description-focus');
                    that.parents('li').children('.gallery-save-loader').hide();
                    that.parents('li').children('.description-value').val(text);
                }
            });
        });

        $(".cancel-description-save").unbind('click').click(function () {
            $(this).parents('li').children('textarea').val($(this).parents('li').children('.description-value').val());
            $(this).parents('li').children('textarea').scrollTop(0);
            $(this).parents('li').children('textarea').removeClass('textarea-description-focus');
        });

        $('.cpt-close').unbind('click').click(function () {
            var par_obj = $(this).parent().parent();
            var text = par_obj.children('.image-caption-text').html();
            par_obj.children('.caption-edit-box').children('.cpt-text').val(text);
            par_obj.children('.caption-edit-box').slideUp('slow');
            par_obj.children('.image-caption-text').show();
        });

        $('.remove-media-image').unbind('click').click(function () {
            var par_obj = $(this).parent();
            var id = par_obj.attr('id');
            id = id.split('-');
            var index = order.indexOf(parseInt(id[3]));
            if (index > -1) {
                order.splice(index, 1);
            }
            par_obj.remove();
            sortable();
        });

    }
</script>