<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//if ($this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '4')
//{
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
$(document).ready(function () {

    $("input:radio[name=payment_type]").click(function() {
        var credit_type = $(this).val();
        if(credit_type=='paypal'){
            $(".credit_detail").hide();
        }else{
            $(".credit_detail").show();
        }
    });
   /* $(".credit_type").on("click", function () {
        alert($(this).val());
    })*/

});
function DeleteExperience(job_id) {

    var data = "job_id=" + job_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteExperience")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".experience" + job_id).hide();
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });

}

</script>

<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {


        $(function () {
            $('.btn_pop').bind('click', function (e) {
                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired
                $('#element_to_pop_up').bPopup();
                                       $('#element_to_pop_up').html("<h3>The subscription upgrade feature is not yet enabled </h3>");
                                       $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');
                /*var param2=this.text;
                var sel_id=$('input[name=rd_subscription]:checked').val();
                var identifier = this.id;
                var data = "";
                var param = "";
                var classes = "";
               *//* if(identifier == ''){
                    classes = $(this).attr("class").split(' ');
                    identifier = classes[1];
                    param = sel_id;
                }*//*
                param = sel_id+"/"+param2;
                if(identifier == 'subs_admin_form'){
                    var url = '<?=$vObj->getURL("publicprofile/profile")?>';
                }
                $.ajax({
                    type:'POST',
                    url: url,
                    data : data,
                    success:function(data){
                        $('#element_to_pop_up').bPopup();
                        $('#element_to_pop_up').html(data);
                        $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');

                    }
                })*/


            });

        });

    })(jQuery);
</script>
<!-- main container -->
<div class="content">

    <div class="container-fluid">
        <div id="pad-wrapper">
            <div><?php if ($profile_updated != "") {
                    echo $profile_updated;
                };?></div>
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php

                    $this->load->view('left_coloumn');


                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">


                            <div class="span12">
                                <div class="container-fluid">


                                    <div class="span4 default-header">
                                        <h4>Subscription</h4>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Status: </b>You currently have a Tyroe Free account.</h5>
                                    </div>
                                    <div class="field-box">
                                        <div class="content-box">
                                            <span>Upgrade to Tyroe Pro now to access these added features:</span>
                                            <ul class="content-box-data">


                                                <li> See Real Studio and Reviewer names in your activity feed</li>
                                                <li> Connect your profile to LinkedIn to import your resume</li>
                                                <li> Engage with the Tyroe editor staff to get reviews and featuring
                                                </li>
                                                <li> Choose how you get your notifications (via email or one of your
                                                    social accounts)
                                                </li>
                                                <li> See full details about your exposure (who has viewed your profile,
                                                    view all of your feedback and coomments and more)
                                                </li>
                                                <li> View all available openings including the studio names</li>
                                                <li> Express interest in opening that suit your skills</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <form class="new_user_form inline-input" _lpchecked="1"
                                            name="subscription_form"
                                            action="<?= $vObj->getURL('account/subscription') ?>" method="post">
                                    <div class="field-box">
                                        <h5><b>Choose Subscription: </b></h5>
                                    </div>
                                    <div class="span12">
                                        <input type="radio" name="subsc_type" value="m"
                                               class="user">
                                        <span class=""><?= LABEL_MONTHLY_ACCOUNT ?>&nbsp;&nbsp; $8-/ mth</span>
                                    </div>
                                    <div class="span12">
                                        <input type="radio" checked="" name="subsc_type" value="q"
                                               class="user">
                                        <span class=""><?= LABEL_QUARTERLY_ACCOUNT ?>&nbsp;&nbsp; $22-/ qtr</span>
                                    </div>
                                    <div class="span12">
                                        <input type="radio" name="subsc_type" class="user" value="y">
                                        <span class=""><?= LABEL_ANNUAL_ACCOUNT ?>&nbsp;&nbsp; $8-/ yr</span>
                                    </div>

                                    <div class="field-box">
                                        <h5><b>Use your coupon for this payment? <a href="">(how do i get coupons?)</a>
                                            </b></h5>
                                    </div>
                                    <?php
                                        if(is_array($current_coupons)){
                                            for($c=0; $c<count($current_coupons); $c++){
                                    ?>
                                    <div class="span12">
                                        <input type="checkbox" value="<?=$current_coupons[$c]['allot_id']."-".$current_coupons[$c]['coupon_value'];?>" name="coupons[]" class="user">
                                        <span class="">$<?=$current_coupons[$c]['coupon_value']." ".$current_coupons[$c]['coupon_label'] ?></span>
                                    </div>
                                    <?php
                                            }
                                        } else {
                                    ?>
                                    <div class="span12">
                                        <span class="">No coupons available.</span>
                                    </div>
                                    <?php } ?>

                                    <div class="field-box">
                                        <h5><b>Automatically use your coupons for future payments if you have some?</b>
                                        </h5>
                                    </div>
                                    <div class="span12">
                                        <input type="checkbox" name="allow_use_copon" id="allow_use_copon" class="user">
                                        <span class="">Yes please!</span>
                                    </div>

                                    <div class="field-box">
                                        <h5><b>If you don't have have enough coupons, how do you want to pay future
                                                payments?</b></h5>
                                    </div>
                                    <div class="span12">
                                        <div class="span4">
                                            <input type="radio" checked="" name="payment_type" class="user" value="credit_card">
                                            <span class="">Credit Card</span>
                                        </div>
                                        <div class="span4">
                                            <input type="radio" name="payment_type" class="user" value="paypal">
                                            <span class="">PayPal</span>
                                        </div>
                                        <span class="span12 credit-card-ops">
                                                <input type="radio" checked="" name="card_type" class="user"
                                                       value="visa"><span>Visa</span>
                                            <input type="radio" checked="" name="card_type" class="user"
                                                   value="mc"><span>mc</span>

                                            </span>

                                    </div>
                                    <div class="span12 credit_detail">
                                        <ul class="content-box-data f-payment ">

                                            <li>
                                                <span class="span3 lab-cc">Card number </span><span><input type="text"
                                                                                                    name="credit_card_num"
                                                                                                    id="credit_card_num"/></span>
                                            </li>
                                            <li><span class="span3 lab-cc">Name on card </span><span><input type="text"
                                                                                                        name="name_card"
                                                                                                        id="name_card"/></span>
                                                </li>
                                            <li><span class="span3 lab-cc">Expiry </span><span><input type="text"
                                                                                                  name="expiry_1"
                                                                                                  id="expiry_1"
                                                                                                  class="span1"/>  <input
                                                            type="text" name="expiry_2" id="expiry_2"
                                                            class="span1"/></span></li>
                                            <li><p><span class="span3 lab-cc">CCV</span><span><input type="text" name="ccv"
                                                                                              id="ccv"/></span></p></li>
                                    </div>


                                    <div class="span11 actions">
                                        <!--<input type="submit" name="submit" class="btn-glow primary"
                                               value="<?php/*= LABEL_UPGRADE_TYROE */?>">-->
                                        <a class="btn-glow primary btn_pop" id="subs_admin_form"
                                                                                                                     href="javascript:void (0);"><?= LABEL_UPGRADE_TYROE ?></a>
                                        <input type="hidden" name="flag" value="flag"/>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <?php
    //}
    ?>
    <!--Dashboard Artist End-->
    <div id="element_to_pop_up">
    </div>