<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">

    function InviteFriend() {

        var invite_message = $("#invite_message").val();
        var friend_name = $("#friend_name").val();
        var friend_email = $("#friend_email").val();


        if (friend_name != "" && friend_email != "") {
            var data = "invite_message=" + invite_message + "&friend_name=" + friend_name + "&friend_email=" + friend_email;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("account/invite_friend")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                    else {
                        $(".notification-box-message").css("color", "#b81900")
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                },
                failure: function (errMsg) {
                }
            });

        } else {
            $(".notification-box-message").css("color", "#b81900")
            $(".notification-box-message").html("Fields Required");
            $(".notification-box").show(100);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 5000);
        }
    }
</script>
<!-- main container -->
<div class="content">

    <div class="container-fluid">
        <div id="pad-wrapper">
            <div><?php if ($profile_updated != "") {
                    echo $profile_updated;
                };?></div>
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php

                    $this->load->view('left_coloumn');


                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">


                            <div class="span12">
                                <div class="container-fluid">


                                    <div class="span4 default-header">
                                        <h4>Coupons</h4>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Your Current coupons: </b></h5>
                                        <ul class="content-box-data">


                                            <li> $4 I A Welcome gift from tyroe.com</li>
                                            <li> Accepted your invitation</li>
                                            <li> Accepted your invitation</li>
                                        </ul>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Invite Friends & Collegues to Earn New Coupons: </b></h5>

                                    </div>
                                    <div class="field-box">
                                        <div class="content-box">
                                            <div
                                                class="content-box-data">
                                                <form class="new_user_form inline-input" _lpchecked="1"
                                                      enctype="multipart/form-data" name="profile_form" method="post"
                                                      action="<?php//= $vObj->getURL('account/save_account_details') ?>">

                                                    <label><?= LABEL_NAME_TEXT ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="friend_name" id="friend_name"
                                                           value="<?php echo $get_user['email']; ?>">

                                                    <label><?= LABEL_EMAIL ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="friend_email" id="friend_email"
                                                           value="">
                                                    <label><?= LABEL_FROM_TEXT ?>:</label>
                                                    <input class="span8" type="text"
                                                           name="txt_from" id="txt_from"
                                                           value="">
                                                    <label><?= LABEL_MESSAGE_TEXT ?>:</label>
                                                    <textarea class="span9" name="invite_message" id="invite_message"
                                                              rows="15"></textarea>
                                                    <div class="span11 actions">
                                                        <input type="submit" name="submit" class="btn-glow primary"
                                                               value="<?= LABEL_SEND_INVITATION_BUTTON ?>"
                                                               onclick="InviteFriend();">
                                                    </div>
                                                </form>


                                            </div>
                                        </div>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Pending Coupons: </b></h5>
                                        <ul class="content-box-data">


                                            <li> $4 I A Welcome gift from tyroe.com</li>
                                            <li> Accepted your invitation</li>
                                            <li> Accepted your invitation</li>
                                        </ul>
                                    </div>
                                    <div class="field-box">
                                        <h5><b>Used Coupons: </b></h5>
                                        <ul class="content-box-data">


                                            <li> $4 I A Welcome gift from tyroe.com</li>
                                            <li> Accepted your invitation</li>
                                            <li> Accepted your invitation</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <?php
    //}
    ?>
    <!--Dashboard Artist End-->