<script type="text/javascript">
    function favouriteCandidate() {
                   var uid = $("#uid").val();
                   var data = "uid=" + uid ;
                   $.ajax({
                       type: "POST",
                       data: data,
                       url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
                       dataType: "json",
                       success: function (data) {
                           if (data.success) {
                               $(".notification-box-message").html(data.success_message);
                               $(".notification-box").show(100);
                               setTimeout(function () {
                                   $(".notification-box").hide();
                               }, 5000);
                           }
                           else {
                               $(".notification-box-message").css("color","#b81900")
                               $(".notification-box-message").html(data.success_message);
                               $(".notification-box").show(100);
                               setTimeout(function () {
                                   $(".notification-box").hide();
                               }, 5000);
                               //$("#error_msg").html(data.success_message);
                           }
                       },
                       failure: function (errMsg) {
                       }
                   });
               }
</script>
<div class="content">

<div class="container-fluid">

<!-- upper main stats -->

<!-- end upper main stats -->

<div id="pad-wrapper">

<div class="grid-wrapper">

<div class="row-fluid show-grid">

<!-- START LEFT COLUMN -->
<div class="span4">

    <div class="row-fluid header-side">
        <div class="span8">
            <h4>Filters</h4>
        </div>
        <div class="span4">
            <a class="btn-flat gray">CLEAR</a>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">
                <form class="new_user_form inline-input">
                    <div class="field-box">
                        <div class="ui-select find">
                            <select>
                                <option selected="">Status</option>
                                <option>Option 1</option>
                                <option>Option 2</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">
                <form class="new_user_form inline-input">
                    <div class="field-box">
                        <div class="ui-select find">
                            <select>
                                <option selected="">Department</option>
                                <option>Custom selects</option>
                                <option>Pure css styles</option>
                            </select>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">
                <form class="new_user_form inline-input">
                    <div class="field-box">
                        <div class="ui-select find">
                            <select>
                                <option selected="">Project</option>
                                <option>Computer Animation</option>
                                <option>Fashion</option>
                                <option>Graphic Design</option>
                                <option>Illustration</option>
                                <option>Industrial Design</option>
                                <option>Interaction Design</option>
                                <option>Motion Graphics</option>
                                <option>Photography</option>
                                <option>UI/UX</option>
                                <option>Web Design</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">
                <form class="new_user_form inline-input">
                    <div class="field-box">
                        <div class="ui-select find">
                            <select>
                                <option selected="">Location</option>
                                <option>Computer Animation</option>

                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


</div>
<?php
//$this->load->view('left_coloumn');
?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar">
    <div class="right-column" style="position:relative;">

        <!-- margin pointer -->
        <div class="left-pointer" id="candidate">
            <div class="left-arrow"></div>
            <div class="left-arrow-border"></div>
        </div>


        <div class="row-fluid header">
            <div class="span12">
                <h4>Candidates</h4>

                <div class="btn-group settings pull-right left-pad-20">
                    <button class="btn glow" id="batch"><i class="icon-cog"></i></button>
                    <button class="btn glow dropdown-toggle" id="batch" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li><a href="#">Batch Contact</a></li>
                        <li><a href="#">Batch Copy to Job</a></li>
                        <li><a href="#">Batch Shortlist</a></li>
                        <li><a href="#">Batch Hide</a></li>
                    </ul>
                </div>
                <div class="button-center pull-right">
                    <a class="btn-flat primary" id="invite" href="__search.html">Find more
                        candidates</a>
                </div>
            </div>
        </div>

        <!-- START CANDIDATE ROW -->
        <?php
        $image;
        foreach ($get_candidate as $candidate) {
            if ($candidate['media_name'] != "") {
                $image = ASSETS_PATH_PROFILE_THUMB.$candidate['media_name'];
                //$image=ASSETS_PATH_PROFILE_THUMB.$candidate['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            ?>
            <input type="hidden" id="uid" name="uid" value="<?php echo $candidate['user_id']; ?>">
            <div class="row-fluid">
                <div class="span12 people" id="top">
                    <div class="row-fluid">

                        <div class="span8">
                            <div class="span12">
                                <div class="span3" id="push-down-10">
                                    <input type="checkbox" class="user">
                                    <img src="<?php echo $image; ?>" class="img-circle avatar-55">
                                </div>

                                <div class="span7">
                                    <a href="<?= $vObj->getURL("findtalent/CandidateDetail/" . $candidate['user_id']) ?>"
                                       class="name"
                                       style="margin:0px;"><?php echo $candidate['username']; ?></a>
                                                        <span class="location"
                                                              style="display: block;"><?php echo $candidate['state'] . " " . $candidate['country']; ?></span>
                                    <span class="tags"
                                          style="display: block;"><?php echo $candidate['username']; ?></span>
                                    <span class="label label-info">Available Now</span>
                                    <span class="label label-success">Featured</span>
                                </div>

                                <div class="span1">

                                    <i class="icon-circle icon-large candidate-status"
                                       id="green"></i>
                                </div>
                            </div>
                        </div>

                        <div class="span4 align-right" id="push-down-10">
                            <a href="__opening-candidate-01.html" class="btn-flat default"
                               id="larger"><i class="icon-tasks"></i></a>
                            <a href="#" class="btn-flat gray" id="larger"><i
                                    class="icon-picture"></i></a>

                            <div class="btn-group settings">
                                <button class="btn glow"><i class="icon-cog"></i></button>
                                <button class="btn glow dropdown-toggle" data-toggle="dropdown">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" style="text-align: left;">
                                    <li><a href="javascript:void(0);" onclick="favouriteCandidate();">Favourite</a></li>
                                    <li><a href="#">Set As Candidate</a></li>
                                    <li>
                                        <a href="<?= $vObj->getURL("findtalent/ViewJobs/" . $candidate['user_id']) ?>">Shortlist</a>
                                    </li>
                                    <li><a href="#">Hide</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php

        }
        ?>
    </div>
</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>
</div>
</div>
<!-- end main container -->