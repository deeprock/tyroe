<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
$gallery_theme_arr = array('text' => '#323A45', 'background' => '#FFFFFF', 'overlay' => 'rgba(255, 255 ,255, 0.5)', 'button' => '#F97E76');
?>
<script>
    $(window).resize(function(){
        var three_images_height = $(".full-profile_overlay").next('.row-fluid').height();
        $(".full-profile_overlay").css('height',three_images_height);
        var three_images_html = $(".full-profile_overlay").next('.row-fluid').html();
        $(".tyroe_score_over_img").css('height',$(".showcase_thumb").height());
        var morris_donut = $(document).find('reviewer-donut');
    });
    $(function () {
        $('#country').select2({
            placeholder: 'Country'
        });
        $('#city').select2({
            placeholder: 'City'
        });
        $('#job_industry').select2({
            placeholder: 'Specialities'
        });
    })
</script>
<script type="text/javascript">
    var warm_welcome_job_id = '';
    $(document).on('click', '.remove-btn', function (e) {
        $(this).parent().remove()
    });
    $(document).ready(function () {
        $(".dt-show-text").click(function () {
            $(".warm-welcome-container").css('display', 'none');
            $('.btn-ready').trigger("click");
        });
        $(".under-avatar-status").tooltip();
        $(".warm-video").hide();
        $(".warm-two").hide();
        var total_warm_container = $('.warm-welcome-container .welcome-wrapper').size();
        var warm_conatiner_position = 1;
        /*Cloning*/
        $(".clone-btn").click(function () {
            var clone_html = $(this).parent().html();
            var clone_htmls = clone_html.replace('<input name="site_btn[]" class="btn f-right cus-btn-mov clone-btn" value="+ADD SITE" type="button">', '<input name="remove_btn" class="btn f-right cus-btn-mov remove-btn" value="-REMOVE" type="button">');
            var clone_div = '<div class="span12 field-box c-magic custom-borders clone-div">' + clone_htmls + '</div>';
            $('.actions').before(clone_div);
        });
        /**/
        $(".warmNext").click(function () {
            warm_conatiner_position++;
            $('.welcome-wrapper').hide();
            $('.warmarea' + warm_conatiner_position).show();
        });
        $(".btn-ready").click(function () {
            var job_name = $('#dashboard_job_title').val().toLowerCase();
            job_name = job_name.replace(/[^a-zA-Z\s]/g, "").replace(/[\s]/g, "-");
            var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_name + '-' + generate_number_id(warm_welcome_job_id);
            window.location = redirect_url;
        });
    });
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>

<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script src="<?= ASSETS_PATH ?>js/fuelux.wizard.js"></script>
<script type="text/javascript">
//job posting

$(document).ready(function () {


    var $wizard = $('#fuelux-wizard'),
        $btnPrev = $('.wizard-actions .btn-prev'),
        $btnNext = $('.wizard-actions .btn-next'),
        $btnFinish = $(".wizard-actions .btn-finish");
    var studio_name;

    var job_title;
    var job_description;
    var job_country;
    var job_city;
    var job_startdate;
    var job_enddate;
    var job_type;
    var job_level;
    var job_tags;
    $wizard.wizard().on('finished',function (e) {
        // wizard complete code
    }).on("changed", function (e) {
            var step = $wizard.wizard("selectedItem");
            // reset states
            $btnNext.removeAttr("disabled");
            $btnPrev.removeAttr("disabled");
            $btnNext.show();
            $btnFinish.hide();

            if (step.step === 1) {
                $btnPrev.attr("disabled", "disabled");
            } else if (step.step === 3) {
                $btnNext.hide();
                $btnFinish.show();
            }
        });
    /*$("#startdate").datepicker({'showCheckbox':false}).on('changeDate',function(ev){customSetDate(ev,this)});*/
    $("#startdate").datepicker({
        'showCheckbox': false
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });
    $("#enddate").datepicker({
        'showCheckbox': false
    }).on('changeDate', function (ev) {
            $(this).datepicker('hide');
        });

    /*$(document).mouseup(function (e) {
     $('#startdate').Close();
     $('#enddate').Close();

     });*/

    $('#btn-next').on('click', function () {
        $('.searching_reviewer_result').html('');
        job_title = $('#dashboard_job_title').val();
        job_description = $('#job_description').val();
        job_description = job_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />');
        job_country = $('#country').val();
        job_city = $('#city').val();
        job_startdate = $('#startdate').val();
        job_enddate = $('#enddate').val();
        job_tags = $('#job_tags').val();
        var date1 = new Date(job_startdate);
        var date2 = new Date(job_enddate);
        var timeDiff = dateDiff(date1, date2);

        var studio_name = $("#hidden_username").val();
        var job_country_name = $("#country option:selected").text();
        var job_city_name = $("#city option:selected").text();
        var job_type_name = $('.job_type').attr('data-id');
        var job_level_name = $('.job_level').attr("data-id");
        var date_my_diff = "";
        /*if (timeDiff.years > 0) {
         date_my_diff = timeDiff.years + " Years(s) " + timeDiff.months + "  Month(s) " + timeDiff.days + " Day(s) ";
         } else {
         date_my_diff = timeDiff.months + "  Month(s) " + " Day(s) ";
         }*/
        var content = '<div class="span12 margin-left-default">';
        content += '<h1>' + studio_name + '</h1>';
        content += '<h3 class="dt-1">' + job_title + '</h3><br>';
        content += '<h5 class="dt-2">' + job_city_name + "," + job_country_name + '</h5>';
        content += '<p class="dt-2 jqueryClassAutoHeight">' + job_description + '</p>';
        content += '<div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">';
        content += '<div class="span4 text-left st-1"><span class="opening-date"><i class="icon-calendar"></i>' + job_startdate + '</span></div>';
        content += '<div class="span4 text-center st-1"><span class="opening-month"><i class="icon-time"></i> ' + timeDiff + '</span></div>';
        content += '<div class="span4 text-center st-1"><span class="opening-time"><i class="icon-legal"></i> ' + job_type_name + '</span></div></div>';
        var jobTags = job_tags.split(',');
        content += '<p class="dt-label pull-left">';
        for (var s = 0; s < jobTags.length; s++) {
            content += '<span class="label">' + jobTags[s] + '  </span>';
        }
        content += '</p></div>';
        $(".preview_job").html(content);

    });
    $btnPrev.on('click', function () {
        $wizard.wizard('previous');
    });
    $btnNext.on('click', function () {
        var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
        var error = 0;
        var job_title = $('#dashboard_job_title').val();
        var job_description = $('#job_description').val();
        var job_industry = $('#job_industry').val();
        var job_country = $('#country').val();
        var job_city = $('#city').val();
        var job_startdate = $('#startdate').val();
        var job_enddate = $('#enddate').val();
        var job_type = $('.job_type:checked').val();
        var job_level = $('.job_level:checked').val();
        var job_tags = $('#job_tags').val();
        if ($.trim(job_title) === "") {
            $('#dashboard_job_title').css({ border: '1px solid red'});
            error++;
        } else {
            $('#dashboard_job_title').css("border", "");
        }
        if ($.trim(job_description) === "") {
            $('#job_description').css({ border: '1px solid red'});
            error++;
        } else {
            $('#job_description').css("border", "");
        }
        if ($.trim(job_industry) === "") {
            $('.job_industry').css({ border: '1px solid red'});
            error++;
        } else {
            $('.job_industry').css("border", "");
        }
        if ($.trim(job_country) === "") {
            $('.country').css({ border: '1px solid red'});
            error++;
        } else {
            $('.country').css("border", "");
        }
        if ($.trim(job_city) === "") {
            $('.city').css({ border: '1px solid red'});
            error++;
        } else {
            $('.city').removeAttr("style");
        }
        if ($.trim(job_startdate) === "") {
            $('#startdate').css({ border: '1px solid red'});
            error++;
        } else {
            $('#startdate').css("border", "");
        }
        if ($.trim(job_enddate) === "") {
            $('#enddate').css({ border: '1px solid red'});
            error++;
        } else {
            $('#enddate').css("border", "");
        }
        if ($.trim(job_type) === "") {
            $('.type-extra-padding').css({ border: '1px solid red'});
            error++;
        } else {
            $('.type-extra-padding').css("border", "");
        }
        if ($.trim(job_level) === "") {
            $('.job-level').css({ border: '1px solid red'});
            error++;
        } else {
            $('.job-level').css("border", "");
        }
        if ($.trim(job_tags) === "") {
            $('#s2id_job_tags').css({ border: '1px solid red'});
            error++;
        } else {
            $('#s2id_job_tags').css("border", "");
        }
        if (error > 0) {
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
        }
        else {
            $wizard.wizard('next');
            var paraHeight = $(".preview_job").find('.jqueryClassAutoHeight').height();
            if (paraHeight > 180) {
                $(".preview_job").find('.jqueryClassAutoHeight').css({"height": "180px", "overflow": "auto"});
            } else {
                $(".preview_job").find('.jqueryClassAutoHeight').css({"height": paraHeight + "px"});
            }
        }
    });

    $(".rt-create-btn").click(function () {
        setTimeout(
            function () {
                $(".select2-container .select2-choices .select2-search-field .select2-input").focus();
                $(".select2-container .select2-choices .select2-search-field .select2-input").blur();
            }, 1000);

    });
});
$(document).on('click', 'button.close, a.close, .job_close', function () {
    $('#dashboard_job_title').css("border", "");
    $('#job_description').css("border", "");
    $('.job_industry').css("border", "");
    $('.country').css("border", "");
    $('.city').css("border", "");
    $('#startdate').css("border", "");
    $('#enddate').css("border", "");
    $('.job-level').css("border", "");
    $('#s2id_job_tags').css("border", "");
    $('.type-extra-padding').css("border", "");
    $('.modal_error_box').hide();
    $('#fuelux-wizard').wizard('previous');
    $('#fuelux-wizard').wizard('previous');
    $('#createjobform')[0].reset();
    $('#job_industry').select2('destroy');
    $('#job_industry').select2();
    $('#country').select2('destroy');
    $('#country').select2();
    $('#city').select2('destroy');
    $('#city').select2();
});
</script>

<script type="text/javascript">
function getcities(country_id) {
    var data = "country_id=" + country_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("jobopening/getcities")?>',
        dataType: "json",
        success: function (response) {
            $('#city').select2('destroy');
            $('#citybox').html(response.dropdown);
            $('#citybox #city').select2();
        }

    });
}
function postjob() {
    $('#post_job').after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
    var reviewers = $('#reviewer_container input[name="reviewer_ids[]"]').map(function () {
        return this.value
    }).get();
    var team_moderation_val = $('.team_moderation').text();
    if (team_moderation_val == 'OFF') {
        team_moderation_val = 0;
    } else if (team_moderation_val == 'ON') {
        team_moderation_val = 1;
    }
    var data = {
        //'job_title': $('#job_title').val(),
        'job_title': $('#dashboard_job_title').val(),
        'job_description': $('#job_description').val().replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />'),
        'job_country': $('#country').val(),
        'job_city': $("#city option:selected").val(), //don't take id of city .. use string
        'job_startdate': $('#startdate').val(),
        'job_enddate': $('#enddate').val(),
        'job_type': $('.job_type:checked').val(),
        'job_level': $('.job_level:checked').val(),
        'job_tags': $('#job_tags').val(),
        'job_industry': $('#job_industry').val(),
        'job_reviewers': reviewers,
        'team_moderation': team_moderation_val
    }
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("jobopening/SaveOpening")?>',
        datatype: 'json',
        success: function (job_id) {
            $('body').removeClass('.modal-open').css({"overflow":"auto"});
            $('#myModal_postjob').modal('hide');
            $('.small_loader').remove();
            $('#post_job').removeAttr('onClick');
            $('.warm-welcome-container').show();
            warm_welcome_job_id = job_id;
        }
    });
}
function DeleteOpening(job_id) {
    var data = "job_id=" + job_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("jobopening/DeleteOpening")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".job-opening" + job_id).hide();
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });

}
function JobApply(job_id) {

    var data = "job_id=" + job_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("jobopening/JobApply")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                //console.log(data.success_message);
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });
}
//getting the exact date difference
function dateDiff(dateFrom, dateTo) {
    /*var from = {
     d: dateFrom.getDate(),
     m: dateFrom.getMonth() + 1,
     y: dateFrom.getFullYear()
     };

     var to = {
     d: dateTo.getDate(),
     m: dateTo.getMonth() + 1,
     y: dateTo.getFullYear()
     };

     var daysFebruary = to.y % 4 != 0 || (to.y % 100 == 0 && to.y % 400 != 0) ? 28 : 29;
     var daysInMonths = [0, 31, daysFebruary, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

     if (to.d < from.d) {
     to.d += daysInMonths[parseInt(to.m)];
     from.m += 1;
     }
     if (to.m < from.m) {
     to.m += 12;
     from.y += 1;
     }

     return {
     days: to.d - from.d,
     months: to.m - from.m,
     years: to.y - from.y
     };*/
    var diff = Math.floor(dateTo.getTime() - dateFrom.getTime());
    var day = 1000 * 60 * 60 * 24;
    var days = (diff / day);
    var months = Math.floor(days / 31);
    var years = Math.floor(months / 12);

    /*if(months > 0){
     var totalDaysInMonth = months * 31;
     days =   Math.abs(totalDaysInMonth - days);
     }
     if(years > 0){
     var totalMonthInYear = years * 12;
     months =    Math.abs(totalMonthInYear - months);
     }*/
    days = Math.round(days);
    if (days == 1) {
        var message = days + " Day ";
    } else if (days >= 0 && days != 1) {
        var message = days + " Days ";
    }
    /*if(months == 1){
     message += months + " Month ";
     }else if(months > 1){
     message += months + " Month(s) ";
     }

     if(years == 1){
     message += years + " Year \n";
     }else if(years > 1){
     message += years + " Year(s) \n";
     }*/
    return message
}
//existing reviewer searching
$(document).on('keyup', '#search_existing_reviewer', function (a) {
    $('.searching_reviewer_result').css({'display': 'block', 'float': 'none'});
    var searching_value = $("#search_existing_reviewer").val()
    $.ajax({
        type: 'POST',
        data: ({'searching_value': searching_value}),
        url: "<?= $vObj->getURL("jobopening/search_existing_reviewer"); ?>",
        success: function (html) {
            if ($('#search_existing_reviewer').val() == '') {
                $('.searching_reviewer_result').html('');
            } else {
                $('.searching_reviewer_result').html(html);
            }
        }
    });

});

$(document).on('click', '.select_reviewer', function () {
    var user_value = $(this).data('value');
    $('.display_list' + user_value).remove();
    $('.parent_row' + user_value).remove();
    var previous_html = $('#reviewer_container').html();
    previous_html += '<li class="parent_row' + user_value + ' hideOnclose">';
    previous_html += $('.list_parent_row' + user_value).html();
    previous_html += '</li>';
    $('#reviewer_container').html(previous_html);
    $('.list_parent_row' + user_value).remove();
});
$(document).on('click', '#send_invitaion', function () {
    var invitation_emails = $("#invitation_emails").val();
    data = 'sending_action=invitaion&emails=' + invitation_emails;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/send_email");?>',
        success: function (result) {
            if (result) {
                $('#invitation_emails').val('');
                alert('invitaion send successfully');
            }
        }
    });
});
</script>
<script type="text/javascript">
//email , model function of tyroe

$(document).on('click', '.job_post', function () {
    $('#myModal_postjob').modal('show');
    rescale();
});
$(document).ready(function () {
    var user_email = '';
    $('#myModal').modal('hide');
    $('.btn-msg').click(function () {
        //getting user email
        user_email = $(this).data('value');
        $(".sugg-msg").html("");
        var msg_to = $(this).attr("data-value");
        $("#hd_email_to").val(msg_to);
        $('#myModal').modal('show');
        rescale();
        $('.modal-body').css('height', 146);
    });
    //Suggestion meesage Sending
    $('.btn-send-email').click(function () {
        if ($('#email-subject').val() == "") {
            $('#email-subject').css({
                border: '1px solid red'
            });
            $('#email-subject').focus();
            return false;
        }

        if ($('#email-subject').val() != "") {
            $('#email-subject').css({
                border: ''
            });
        }


        if ($('.email-message').val() == "") {
            $('.email-message').css({
                border: '1px solid red'
            });
            $('.email-message').focus();
            return false;
        }

        if ($('.email-message').val() != "") {
            $('.email-message').css({
                border: ''
            });
        }
        //else{
        $(this).text('Sending..');
        $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
        $(this).removeClass('btn-send-email');
        $(this).addClass('add_email_dummyclass');


        var email_message = $(".email-message").val();
        var email_subject = $("#email-subject").val();
        var email_to = user_email;
        var data = "callfrom=send_email&email_message=" + email_message + "&email_subject=" + email_subject + "&email_to=" + email_to;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("search/access_module");?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.success) {
                    $(".sugg-msg").show();
                    $(".sugg-msg").html("Your message send success.");
                    $('.add_email_dummyclass').text('Send');
                    $('.add_email_dummyclass').addClass('btn-send-email');
                    $('.btn-send-email').removeClass('add_email_dummyclass').hide();
                    $('.small_loader').remove();
                    $('.email_close').show();
                }
            }
        });
        //return true;

        //}
    });
    /*create job_tags*/
    $("#job_tags").select2({
        tags: [""],
        tokenSeparators: [","]
    });
    /*End create job_tags*/

    /* Creating emails tags */
    $("#invitation_emails").select2({
        tags: [""],
        tokenSeparators: [","]
    });
});
function rescale() {
    var size = {width: $(window).width(), height: $(window).height() }
    var offset = 20;
    var offsetBody = 240;
    //$('#myModal').css('height', size.height - offset );
    $('.modal-body').css('height', size.height - (offset + offsetBody));
    $('#myModal').css('top', '3%');
    $('#myModal_postjob').css('top', '3%');
}
$(window).bind("resize", rescale);

$(document).ready(function () {
    /*Add in openings*/
    $(".add_in_opening_jobs").click(function () {
        /*First Param =  JOB ID*/
        /*Second Param =  Tyroe ID*/
        $('.loader-save-holder').show();
        var job_tyroe_id = $(this).data('value');
        var job_tyroe_arr = (job_tyroe_id).split(",");
        var job_id = job_tyroe_arr[0];
        var tyroe_id = job_tyroe_arr[1];
        $(this).addClass('disable_opening');
        $(this).removeClass('add_in_opening_jobs');
        $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'add_by_search'},
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Add in openings*/

    /*Add profile in favourite*/
    $(".fav_profile").click(function favouriteCandidate() {
        $('.loader-save-holder').show();
        var uid = $(this).data('value');
        var data = "uid=" + uid;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Add profile in favourite*/

    /*Hide Search*/
    $('.hide_search').click(function () {
        var tyroe_id = $(this).data('value');
        $.ajax({
            type: "POST",
            data: {tyroe_id: tyroe_id},
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                var total_candidate = $('.number_of_candidate').text();
                $('.number_of_candidate').text(total_candidate - 1);
                $('.editor_pck_sinlge' + tyroe_id).fadeOut(200);
                if (total_candidate == 1) {
                    $('.number_of_candidate').text('There is no any latest editor pick');
                }

            },
            failure: function (errMsg) {
            }
        });
    })
    /*Hide Search*/
    <?php if($this->session->userdata('role_id')==2)
    {
    ?>

    // Morris Bar Chart
    <?php
    if(!isset($profile_review[0]['technical']) || $profile_review[0]['technical'] == '')
    {
        $profile_review[0]['technical'] = '0';
    }
    if(!isset($profile_review[0]['creative']) || $profile_review[0]['creative'] == '')
    {
        $profile_review[0]['creative'] = '0';
    }
    if(!isset($profile_review[0]['impression']) || $profile_review[0]['impression'] == '')
    {
        $profile_review[0]['impression'] = '0';
    }
    ?>
//    Morris.Bar({
//        element: 'hero-bar',
//        data: [
//            {device: 'Technical', sells: '<?=$profile_review[0]['technical']?>'},
//            {device: 'Creative', sells: '<?=$profile_review[0]['creative']?>'},
//            {device: 'Impression', sells: '<?=$profile_review[0]['impression']?>'}
//        ],
//        xkey: 'device',
//        ykeys: ['sells'],
//        labels: ['Sells'],
//        barRatio: 0.4,
//        xLabelMargin: 10,
//        hideHover: 'auto',
//        barColors: ["#3d88ba"]
//
//    });
    <?php } ?>
    /* $(".activity_val").on("click", function () {
     var id = $(this).attr("data-value");
     $('.id').val(id);
     if ($(this).attr("data-id") == '3') {
     $("#form_stream").attr("action", "
    <?= $vObj->getURL('exposure/feedback_rating') ?>");
     } else if ($(this).attr("data-id") == '6') {
     $("#form_stream").attr("action", "
    <?= $vObj->getURL('openings') ?>");
     }
     $("#form_stream").submit();
     });
     */

    $("select").on("change", function () {
        var str = "";
        $("select option:selected").each(function () {
            str += $(this).text() + " ";
        });
        if ($(this).val() != "") {
            $(".activity_type_label").text(str);
        }
    })
    /*Clear button */
    $("#clear_btn").click(function () {
        $(this).closest('form').find("input[type=text], textarea,select").val("");
        FilterStreams();
    });

    $('.stream_keyword').donetyping(function () {
        FilterStreams();
    }, 500);

});
$(document).on("click", ".search_by", function () {

    if ($(this).attr("data-value") == "2") {
        $("#stream_duration").val("week");
    }
    else if ($(this).attr("data-value") == "3") {
        $("#stream_duration").val("month");
    }
    else if ($(this).attr("data-value") == "1") {
        $("#stream_duration").val("all");
    }
    FilterStreams();

});

function FilterStreams() {
    $('.loader-save-holder').show();
    var activity_type = $("#activity_type").val();
    var stream_keyword = $(".stream_keyword").val();
    var stream_duration = $("#stream_duration").val();
    var data = "activity_type=" + activity_type + "&stream_keyword=" + stream_keyword + "&stream_duration=" + stream_duration +
        "&pagename=dashboard";
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("stream/FilterStream")?>',

        success: function (data) {
            $('.loader-save-holder').hide();
            $(".no-filter").hide();
            $(".filter-result").html(data);
            $(".stream_pagination").remove();
        }
    });
}
function applicaint_call(job_id, job_title) {
    $('input[name="applicaint_url_trigger"]').val('yes');
    var job_title = job_title.toLowerCase();
    job_title = job_title.replace(/ /gi, '-');
    var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
    $('#form_stream').attr('action', redirect_url)
    $('#form_stream').submit();
}
function shortlist_call(job_id, job_title) {
    $('input[name="shortlist_url_trigger"]').val('yes');
    var job_title = job_title.toLowerCase();
    job_title = job_title.replace(/ /gi, '-');
    var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
    $('#form_stream').attr('action', redirect_url)
    $('#form_stream').submit();
}
function candidate_call(job_id, job_title) {
    $('input[name="candidate_url_trigger"]').val('yes');
    var job_title = job_title.toLowerCase();
    job_title = job_title.replace(/ /gi, '-');
    var redirect_url = "<?=$vObj->getURL("openings")?>/" + job_title + '-' + job_id;
    $('#form_stream').attr('action', redirect_url)
    $('#form_stream').submit();
}
function set_graph_size() {
    var window_width = $(window).width();
    //$('body').css({"opacity":"0.3"});
    if (window_width <= 474) {
        $('.resize_class:eq(1) .offset3 .knob-wrapper').css({'width': '100%'});
    } else if (window_width <= 766) {
        $('.resize_class:eq(0) .offset3 .knob-wrapper').css(
            {'float': 'none',
                'position': 'relative',
                'margin': '0 auto',
                'width': '205px'}
        );
        $('.resize_class:eq(1) .offset3 .knob-wrapper').removeAttr('style');
    } else if (window_width <= 980) {
        $('.resize_class').eq(0).removeClass('span4');
        $('.resize_class').eq(0).addClass('span6');
        $('.resize_class').eq(1).removeClass('span4');
        $('.resize_class').eq(1).addClass('span6');
        $('.resize_class').eq(2).removeClass('span4');
        $('.resize_class').eq(2).addClass('span12');
        $('.resize_class:eq(2) .offset3').removeClass('span12');
        $('.resize_class:eq(2) .offset3').removeClass('pull-right');
        $('.resize_class:eq(2) .offset3').addClass('span4');

    } else if (window_width <= 1085) {
        $('body').addClass('change');

    }
    if (window_width > 766) {
        $('.resize_class:eq(0) .offset3 .knob-wrapper').removeAttr('style');
    }
    if (window_width > 981) {
        $('.resize_class').eq(0).removeClass('span6');
        $('.resize_class').eq(0).addClass('span4');
        $('.resize_class').eq(1).removeClass('span6');
        $('.resize_class').eq(1).addClass('span4');
        $('.resize_class').eq(2).removeClass('span12');
        $('.resize_class').eq(2).addClass('span4');
        $('.resize_class:eq(2) .offset3').removeClass('span4');
        $('.resize_class:eq(2) .offset3').addClass('pull-right');
        $('.resize_class:eq(2) .offset3').addClass('span12');
    }
    /*if (window_width > 1085) {
     $('body').removeClass('change');

     }*/

}
blurinterval = setInterval(function () {
    $('body').css({"opacity": "1"});
}, 1500);
$(window).bind("resize", set_graph_size);
function gettyroe_profile(tyroe_id) {
    $('#user' + tyroe_id).val(tyroe_id);
    $('#user' + tyroe_id).attr('name', 'tyroe_id');
    $('#page_pos' + tyroe_id).attr('name', 'page_position');
    $('#latest_editorpic_form').submit();
}
function getlatest_editorlist() {
    $('#latest_editorpic_form').submit();
}
<?php if ($this->session->userdata('role_id') == '4') { ?>
function applicaint_call() {
    $('input[name="applicaint_url_trigger"]').val('yes');
    $('#A_C_S_jumping_from').submit();
}
function shortlist_call() {
    $('input[name="shortlist_url_trigger"]').val('yes');
    $('#A_C_S_jumping_from').submit();
}
function candidate_call() {
    $('input[name="candidate_url_trigger"]').val('yes');
    $('#A_C_S_jumping_from').submit();
}
<?php } ?>
</script>
<div style="display: none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<?php
/*STUDIO AND REVIEWER SECTION START*/
if ($this->session->userdata('role_id') == '3' || $this->session->userdata('role_id') == '4') {
    if ($this->session->userdata('role_id') == '3') {
        ?>
        <div class="warm-welcome-container" style="display:none ">
            <div class="welcome-overlay"></div>
            <div class="welcome-wrapper warm-one warmarea1">
                <h4>
                    Congratulation <?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname'); ?>
                    !</h4>

                <h1>You have created a new job</h1>

                <p>Before you get started, we would like to outline a few key features that will make sure you find the
                    best candidate possible, in the shortest amount of time. </p>

                <div class="welcome-buttons-container next-btn-wrapper">
                    <a class="warmNext next btn btn-large btn-default" href="javascript:void(0);">Next</a>

                    <div class="clearfix"></div>
                    <a class="dt-show-text" href="javascript:void(0);">Got it! Don't show me again.</a>
                </div>
            </div>
            <div class="welcome-wrapper warm-menu warm-two warmarea2" style="display: none">
                <h1>Shortlist Process</h1>

                <p> - People that apply to your job posting are added as "Applicants".<br>
                    - Upgrade an "Applicant" to "Candidate" level for tema members to review.<br>
                    - Find additional "Candidate" by searching our extensive database .<br>
                    - Work together to "Shortlist" the best person for your company.</p>

                <div class="container width-100">
                    <div class="row-fluid">
                        <div class="warm-menu-holder span12 margin-top-4">
                            <img src="<?= ASSETS_PATH ?>img/process-img.png" alt="Interactive-menu">
                        </div>
                    </div>
                </div>
                <div class="welcome-buttons-container next-btn-wrapper">
                    <a href="javascript:void(0);" class="warmNext next btn btn-large btn-default">Next</a>

                    <div class="clearfix"></div>
                    <a href="javascript:void(0);" class="dt-show-text">Got it! Don't show me again.</a>
                </div>
            </div>
            <div class="welcome-wrapper warm-menu warm-three warmarea21 warm-sp" style="display: none">
                <h1>Job Opening Tabs </h1>

                <div class="container width-100">
                    <div class="row-fluid">
                        <div class="warm-menu-holder span12 margin-top-10 margin-bottom-10">
                            <img src="<?= ASSETS_PATH ?>img/opening-tab.png" alt="Interactive-menu">

                        </div>
                        <div class="next-btn-wrapper ready-con ready-td">
                            <a class="button-main button-large btn new-nav-btn1 btn-flat btn-ready primary btn-success btn-td"
                               href="javascript:void(0);">I'm
                                ready!</a>

                            <div class="clearfix"></div>
                            <a href="javascript:void(0);" class="dt-show-text">Got it! Don't show me again.</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    <!-- main container -->
    <div class="content">
    <div class="container-fluid">
        <?php if ($this->session->userdata('role_id') == '3' || $this->session->userdata('role_id') == '4') { ?>
            <div id="main-stats">
                <div class="row-fluid stats-row" id="stats-padding">
                    <div class="span12">
                        <div class="span3 stat first">
                            <div class="data">
                                <span class="number"><?= $get_active_jobs; ?></span>
                                Jobs
                            </div>
                            <span class="date">Active</span>
                        </div>
                        <div class="span3 stat">
                            <div class="data">
                                <span class="number red-text"><?= $get_total_candidate; ?></span>
                                Candidate
                            </div>
                            <span class="date">Pending Review</span>
                        </div>
                        <div class="span3 stat">
                            <div class="data">
                                <span class="number"><?= $get_total_reviewed; ?></span>
                                Reviews
                            </div>
                            <span class="date">Completed</span>
                        </div>
                        <div class="span3 stat last">
                            <div class="data">
                                <span class="number"><?= $total_new_tyroe_in_this_month ?></span>
                                Tyroes
                            </div>
                            <span class="date">New this month</span>
                        </div>
                    </div>

                </div>
            </div>
            <div id="main-stats" style="margin-top: 0;">
                <div class="row-fluid stats-row">
                    <?php if ($this->session->userdata('role_id') == '3') { ?>
                        <div class="span6 stat-large first text-center">
                            <div class="no-images" style="margin-bottom: 0">
                                <i class="icon-book"></i>

                                <div class="clearfix"></div>
                                <h5 class="rt-text-15">Create a new job opening</h5>

                                <p>
                                    Lets start by creating a new job opening and invite an awesome team of
                                    reviewers to help you find the best person for the job. </p>
                                <?php //if ($total_jobs['total'] < 25) {
                                    if(true){ ?>
<!--                                    <a href="javascript:void(0)" class="btn btn-success job_post rt-create-btn">CREATE
                                        OPENING</a>-->
                                        <a href="<?=LIVE_SITE_URL.'opening/create_job'?>" class="btn btn-success rt-create-btn">CREATE
                                        OPENING</a>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="<?php if ($this->session->userdata('role_id') == '3') {
                        echo 'span6';
                    } else {
                        echo 'span12';
                    } ?> stat-large last text-center">
                        <div class="no-images" style="margin-bottom: 0">
                            <i class="icon-search"></i>

                            <div class="clearfix"></div>
                            <h5 class="rt-text-15">Discover amazing new talent</h5>

                            <p>There are currently <?= $total_tyroes['total_tyroe'] ?> talented entry-level to mid-level
                                artists in our
                                creative database looking for their next big break in the industry. </p>

                            <form action="<?= $vObj->getURL("search") ?>" method="post" style="margin-bottom: 0;">
                                <div class="btn-group">
                                    <input placeholder="Enter keywords..." type="text" name="dashboard_search_keyword"/>
                                    <button type="submit" class="btn btn-success right rt-search-btn">Search</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
    <div class="container-fluid padding-default">
    <div id="pad-wrapper" class="no-padding">
    <div class="grid-wrapper">
    <div class="row-fluid show-grid">
    <!-- START RIGHT COLUMN -->
    <div class="span12" id="opening-overview">
    <div class="right-column" style="position:relative;padding: 0;">

    <!-- START FEATURED ARTISTS -->
    <div class="container">
        <div class="row-fluid header sections">
            <div class="span12 tr-pd-40">
                <h4>Latest Editor Picks</h4>
            </div>
        </div>
    </div>
    <form id="latest_editorpic_form" action="<?= $vObj->getURL("search") ?>" method="post">
        <input type="hidden" name="home_featured" value="featured">
        <?php
        $image;
        if($get_candidate != ''){
                for ($tyr = 0; $tyr < count($get_candidate); $tyr++) {

            if ($get_candidate[$tyr]['media_name'] != "") {
                $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_candidate[$tyr]['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            ?>
            <input type="hidden" id="uid" name="uid" value="<?php echo $get_candidate[$tyr]['user_id']; ?>">
            <input type="hidden" id="uemail<?php echo $get_candidate[$tyr]['user_id']; ?>" name="uemail"
                   value="<?php echo $get_candidate[$tyr]['email']; ?>">
            <div class="row-fluid rg-bg">
                <div class="container">
                    <div class="span12 editor-pck  editor_pck_sinlge<?php echo $get_candidate[$tyr]['user_id']; ?>">
                        <div class="row-fluid">
                            <div class="span12" id="top">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="edt_pck_sec">
                                            <div class="user-dp-holder">
                                                <a href="javascript:void(0)"
                                                   onclick="gettyroe_profile('<?php echo $get_candidate[$tyr]['user_id']; ?>')" style="position: relative;display: inline-block;"><img
                                                        src="<?php echo $image; ?>" class="img-circle avatar-80">
                                                
                                                <?php  
                    if (intval($get_candidate[$tyr]['availability']) == 1) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#96bf48" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE FOR WORK"></i>
                    <?php
                    } else if (intval($get_candidate[$tyr]['availability']) == 2) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#5ba0a3" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE SOON"></i>
                    <?php
                    } else if (intval($get_candidate[$tyr]['availability']) == 3) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i>
                    <?php
                    }else{
                    ?>
                       <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i> 
                    <?php    
                    }
                    ?>
                                                </a>
                                            </div>
                                            <input type="hidden"
                                                   id="user<?php echo $get_candidate[$tyr]['user_id']; ?>"/>
                                            <input type="hidden"
                                                   id="page_pos<?php echo $get_candidate[$tyr]['user_id']; ?>"
                                                   value="<?= $tyr + 1 ?>"/>

                                            <div class="editors_picked" id="editors_pick">
                                                <div class="editors_pick-info" id="editors_pick_info">
                                                    <a href="javascript:void(0)"
                                                       onclick="gettyroe_profile('<?php echo $get_candidate[$tyr]['user_id']; ?>')"
                                                       class="name"><?php if(isset($has_profile_access) && $has_profile_access == 0){
                                                                    echo "Name Hidden";
                                                                }else {
                                                                    echo $get_candidate[$tyr]['firstname'] . ' ' . $get_candidate[$tyr]['lastname'];
                                                                }?></a>
                                        <span class="tags"><?php
                                            /*for ($a = 0; $a < count($get_candidate[$tyr]['skills']); $a++) {
                                                $temp = $get_candidate[$tyr]['skills'][$a]['creative_skill'];
                                                echo ($a > 0) ? ", " : "";
                                                if (!empty($temp)) {
                                                    echo $temp;
                                                }

                                            }*/
                                            echo rtrim($get_candidate[$tyr]['industry_name'] . ', ' . $get_candidate[$tyr]['extra_title'], ', ');
                                            ?></span>
                                                    <span
                                                        class="location"><?php echo $get_candidate[$tyr]['country'] . ", " . $get_candidate[$tyr]['city']; ?></span>
                                                    <br clear="all">
                                                    <?php 
                                                     if(isset($has_profile_access) && $has_profile_access == 1) {
                                                    ?>
                                                    <div class="btn-group large">
                                                        <button class="glow left" data-toggle="dropdown">
                                                            <i class="icon-plus-sign"></i></button>
                                                        <ul class="dropdown-menu">

                                                            <li>
                                                                <a href="javascript:void(0)" class=""
                                                                   style="color:#999999"><?= "Add to opening:"; ?></a>
                                                            </li>
                                                            <?php if($get_openings_dropdown != ''){
                                                                
                                                                    for ($op = 0; $op < count($get_openings_dropdown); $op++) { ?>
                                                                <?php $duplicate_job = 0;
                                                                
                                                                if($get_candidate[$tyr]['tyroe_jobs'] != ''){
                                                                   foreach ($get_candidate[$tyr]['tyroe_jobs'] as $tyroe_job) {
                                                                    ?>
                                                                    <?php if ($tyroe_job['job_id'] == $get_openings_dropdown[$op]['job_id']) {
                                                                        $duplicate_job = 1;
                                                                        ?>
                                                                        <li>
                                                                            <a href="javascript:void(0)"
                                                                               class="disable_opening"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                        </li>
                                                                    <?php } ?>
                                                                <?php } }?> 
                                                                
                                                                
                                                                
                                                                <?php if ($duplicate_job == 0) { ?>
                                                                    <li>
                                                                        <a href="javascript:void(0)"
                                                                           class="add_in_opening_jobs"
                                                                           data-value="<?= $get_openings_dropdown[$op]['job_id'] . "," . $get_candidate[$tyr]['user_id']; ?>"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                    </li>
                                                                <?php } ?>
                                                            <?php } } ?>
                                                        </ul>
                                                        <?php if ($get_candidate[$tyr]['is_favourite'] == '') { ?>
                                                            <button type="button" class="glow middle fav_profile"
                                                                    data-value="<?= $get_candidate[$tyr]['user_id'] ?>">
                                                                <i class="icon-star"></i></button>
                                                        <?php } else { ?>
                                                            <button type="button" class="glow middle"
                                                                    style="background: #2388d6;">
                                                                <i class="icon-star" style="color:#ffffff"></i></button>
                                                        <?php } ?>
                                                        <button type="button" href="#myModal"
                                                                class="glow middle btn-msg"
                                                                data-value="<?= $get_candidate[$tyr]['user_id'] ?>">
                                                            <i class="icon-envelope"></i></button>
                                                        <button type="button" class="glow right hide_search"
                                                                data-value="<?= $get_candidate[$tyr]['user_id'] ?>">
                                                            <i class="icon-trash"></i></button>
                                                    </div>
                                                     <?php } ?>
                                                </div>
                                                <div class="editors_showcase-container" id="editors-showcase">
                                                    <div class="editors_showcase_holder">
                                                        <div
                                                            class="full-profile_overlay black_overlay_separate<?= $get_candidate[$tyr]['user_id'] ?>"
                                                            onclick="gettyroe_profile('<?php echo $get_candidate[$tyr]['user_id']; ?>')">
                                                            <div class="full-profile_overlay_holder">
                                                                <p>View Full Profile</p>
                                                                <i class="icon-picture"></i>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <?php
                                                            for ($a = 0; $a < 3; $a++) {
                                                                if($get_candidate[$tyr]['portfolio'][$a]['media_name']){
                                                                   $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_candidate[$tyr]['portfolio'][$a]['media_name'];
                                                                   if (!file_exists($portfolio)) {
                                                                       $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                   }else{
                                                                       $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_candidate[$tyr]['portfolio'][$a]['media_name'];
                                                                   }
                                                                }else{
                                                                    $portfolio = ASSETS_PATH.'/img/image-icon.png';
                                                                }
                                                                ?>
                                                                <div class="showcase_thumb pull-right">
                                                                    <a href="javascript:void(0);"
                                                                       class="score_img_anchor<?= $a ?>"
                                                                       data-id="<?= $get_candidate[$tyr]['user_id'] ?>">
                                                                        <?= (($a == 2) && ($get_candidate[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                        <?php if ($a == 0) { ?>
                                                                            <?php if ($get_candidate[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                                <div
                                                                                    class="tyroe_score_over_img tyroeScoreHolder<?= $get_candidate[$tyr]['user_id'] ?>">
                                                                                    <div
                                                                                        class="tyroe_score_holder"><?= $get_candidate[$tyr]['get_tyroes_total_score'] ?></div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <img
                                                                            src="<?php echo $portfolio ?>"
                                                                            alt="editor-picks-<?= $get_candidate[$tyr]['is_user_featured'] ?>">
                                                                    </a>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php if ($tyr == 0) { ?>
                    <div class="editor-seperator sep-left-adjust"></div>
                <?php } ?>
            </div>

        <?php }   } ?>
      
        
        <div class="container">
            <div class="span11" id="view-more-last">
                            <span class="pull-right button-center"><a class="btn-flat gray" id="comment"
                                                                      onclick="getlatest_editorlist()"
                                                                      href="javascript:void(0)">View
                                    More</a></span>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
    <!-- START ACTIVITY STREAM -->
    <div class="editor-seperator sep-left-adjust"></div>
    <div class="row-fluid header section">
        <div class="container">
            <div class="span12 tr-pd-40">
                <div class="span3 pull-left"><h4 class="dashboard-activity-adjust">Activity Stream</h4></div>

                <div class="span3 pull-left">
                    <div class="container span12">

                        <div class="field-box">
                            <div class="ui-select find">
                                <select class="span5 inline-input" name="activity_type" id="activity_type"
                                        onchange="FilterStreams();">
                                    <option value="">Select Type</option>
                                    <?php
                                    foreach ($get_activity_type as $key => $activity_type) {
                                        ?>
                                        <option
                                            value="<?php echo $activity_type['activity_type_id'] ?>"><?php echo $activity_type['activity_title'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="field-box span3 pull-left dashboard-keywords">
                    <div class="span12 ctrls">
                        <input type="text" name="stream_keyword" value="<?= $stream_keyword ?>" placeholder="Keywords"
                               class="search span12 padding-1 stream_keyword" onkeyup="FilterStreams();">
                    </div>
                </div>

                <div class="container span3 pull-left dashboard-btngroup">
                    <div class="field-box">
                        <div class="btn-group pull-right c-magic">
                            <button class="glow left search_by" data-value="1">All</button>
                            <button class="glow search_by" data-value="2">This Week</button>
                            <button class="glow right search_by" data-value="3">This Month</button>
                            <input type="hidden" name="stream_duration" id="stream_duration" value="">
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="container">
        <div class="span10 mar-left-act_stream">
            <form enctype="multipart/form-data" id="form_stream" class="form_stream" name="stream" method="post"
                  action="<?php//= $vObj->getURL('exposure/feedback_rating') ?>">
                <input type="hidden" name="applicaint_url_trigger">
                <input type="hidden" name="candidate_url_trigger">
                <input type="hidden" name="shortlist_url_trigger">
                <input type="hidden" value="" name="id" class="id">
            </form>
            <div class="row-fluid">
                <div class="span12">
                    <div class="navbar navbar-inverse">
                        <ul style="margin: 0 10px 10px 10px; list-style:none;">
                            <li class="stream-container">
                                <div class="stream-dialog">
                                    <div class="filter-result"></div>
                                    <div class="no-filter notes">
                                        <div class="notes">
                                            <?php
                                            if (is_array($job_activity) == "") {
                                                ?>
                                                <div class="alert alert-info">
                                                    <i class="icon-exclamation-sign"></i>
                                                    There is no activity
                                                </div><?php

                                            } else {
                                                foreach ($job_activity as $job_activity_log) {
                                                    $icon;
                                                    $text;
                                                    $link = "";
                                                    $href = "javascript:void(0)";
                                                    $onclick = "";
                                                    if ($job_activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                                                        /*JOB APPLICANT SECTION*/

                                                        $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                        $icon = $return_activity_stream['icon'];
                                                        $text = $return_activity_stream['text'];
                                                        $href = $return_activity_stream['href'];
                                                        $onclick = $return_activity_stream['onclick'];

                                                    } else if ($job_activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                                                        /*Profile Favourited Section*/
                                                        $icon = LABEL_ICON_STAR;
                                                        $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                        $text = "You added " . $activity_data_email['tyroe_name'] . " to your Favourites";
                                                        $href = $vObj->getURL('favourite');

                                                    } else if ($job_activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                                                        /*JOB CANDIDATE Section*/
                                                        $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                        $icon = $return_activity_stream['icon'];
                                                        $text = $return_activity_stream['text'];
                                                        $href = $return_activity_stream['href'];
                                                        $onclick = $return_activity_stream['onclick'];

                                                    } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                        /*JOB SHORTLIST Section*/
                                                        $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                        $icon = $return_activity_stream['icon'];
                                                        $text = $return_activity_stream['text'];
                                                        $href = $return_activity_stream['href'];
                                                        $onclick = $return_activity_stream['onclick'];

                                                    } else if ($job_activity_log['activity_type_id'] == LABEL_NEW_JOB_LOG) {
                                                        /*OPENINGS Section*/
                                                        $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                        $icon = $return_activity_stream['icon'];
                                                        $text = $return_activity_stream['text'];
                                                        $href = $return_activity_stream['href'];

                                                    } else if ($job_activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                        /*Feedback Section*/
                                                        $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                        if(count($return_activity_stream) == 0)continue;
                                                        $icon = $return_activity_stream['icon'];
                                                        $text = $return_activity_stream['text'];
                                                        $href = $return_activity_stream['href'];
                                                        $onclick = $return_activity_stream['onclick'];
                                                    }

                                                    ?>
                                                    <a href="<?= $href ?>" class="activity_val" data-id="<?= $link ?>"
                                                       data-value="<?= $id ?>" onclick="<?= $onclick ?>">
                                                                                <span class="item">
                                                                                <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                                                                                    <?= $text ?>
                                                                                    <span class="time"><i
                                                                                            class="icon-time"></i> <?php echo get_activity_time($job_activity_log['created_timestamp']); ?></span>
                                                    </a></span>
                                                <?php
                                                }

                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <form method="post" action="" id="jumptoform">
                <input type="hidden" name="job_id" id="job_id" value="">
                <input type="hidden" name="job_title" id="job_title" value="">
                <input type="hidden" name="action" id="action" value="">
            </form>

        </div>
    </div>
    <div class="container">
        <div class="span11"><span class="pull-right button-center"><a href="<?= $vObj->getURL("activity-stream") ?>"
                                                                      class="btn-flat gray" id="comment">View
                    More</a></span></div>
    </div>
    </div>

    </div>
    </div>
    <!-- END RIGHT COLUMN -->
    <div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- end main container -->


<?php
} else {
    ?>
    <!-- main container -->
    <div class="content">

    <div class="container-fluid">

    <!-- upper main stats -->

    <?php
    $this->load->view('upper_stats');
    ?>

    <!-- end upper main stats -->
    <div id="pad-wrapper" class="dashboard-pad-wrapper">
    <div class="grid-wrapper">
    <div class="row-fluid show-grid">
    <!-- START RIGHT COLUMN -->
    <div class="span12 custom-border-left" id="opening-overview">
    <div class="right-column" style="position:relative;">

        <!-- statistics chart built with jQuery Flot -->
        <div class="row-fluid chart">
            <h4 class="dashboard-head-adjust">
                Profile Performance </h4>

            <div class="btn-group pull-right">
                <button class="glow left" id="week">WEEK</button>
                <button class="glow right active" id="month">MONTH</button>
                <!--<button class="glow right">YEAR</button>-->
            </div>
            <div class="clearfix"></div>
            <div class="span12">
                <div id="statsChart"></div>
            </div>
        </div>
        <!-- end statistics chart -->

        <!--charts-->
        <div class="row-fluid user-dashboard-chart-container">
            <div class="span12 tyroe-profile-score-box" style="padding-left: 44px">
                <div class="row-fluid">
                    <div class="span9 score-badge-content">
                         <?php
                        $title = '';
                        $text = '';
    //                  $avg = intval($profile_review[0]['technical']) + intval($profile_review[0]['creative']) + intval($profile_review[0]['impression']);
                        $title = 'Your Ranking';
                        $text = 'Once you start getting some <a href="'.base_url().'feedback">Feedback & Scores</a> from Studios you will be given an inductry ranking that will appear here.';
                        $avg = intval($tyroe_review_score);
                        if($avg > 0 && $avg < 50){
                            $title = 'Thirsty';
                            $text = 'Skills not perfected, but an obvious strong desire to prove themselves.';
                        }else if($avg >= 50 && $avg <= 79){
                            $title = 'Pressed';
                            $text = 'Hyper-focused on perfecting their skills and obsessively climbing the ranks.';
                        }else if($avg >= 80){
                            $title = 'Slayer';
                            $text = 'Spectacular abilities with impressive skills. Ready for industry challenges.';
                        }

                        $profile_complete = 0;
                        $profile_complete = intval($profile_completness['profile_scorer']) + intval($profile_completness['resume_scorer']) + intval($profile_completness['portfolio_scorer']);
                        ?>
                        <div class="span3">
                            <img id="score-badge-img" src="<?=LIVE_SITE_URL.'assets/img/default-avatar.png'?>" class="img-circle score-badge-logo">
                        </div>
                        <div class="span6" style="margin-top: 15px;">
                              <div class="badge-title"><?=$title?></div>
                              <div class="badge-text"><?=$text?></div>
                        </div>
                    </div>
                    <div class="knob-wrapper span3" class="uneditable-input" style="padding-left: 44px">
                        <input class="knob home_knob" type="text"
                               value="<?= $profile_complete ?>" data-thickness=".3" data-width="150" data-height="150" data-fontsize="22" data-inputColor="#333"
                               data-readonly="true" data-fgColor="#30a1ec" data-bgColor="#d4ecfd">
                        <div class="base-label">
                            <span class="base-label-color"></span>&nbsp;&nbsp;
                            <span class="base-label-text">Profile Completed</span>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!--charts-->

    <!-- START ACTIVITY STREAM -->
    <div class="row-fluid header section padding-top-1k">
        <div class="span12">
            <h4 class="dashboard-activity-adjust">Activity Stream</h4>

            <div class="container span3 pull-right dashboard-btngroup">
                <div class="field-box">
                    <div class="btn-group pull-right c-magic">
                        <button class="glow left search_by" data-value="1">All</button>
                        <button class="glow search_by" data-value="2">This Week</button>
                        <button class="glow right search_by" data-value="3">This Month</button>
                        <input type="hidden" name="stream_duration" id="stream_duration" value="">
                    </div>
                </div>
            </div>

            <div class="field-box span3 pull-right dashboard-keywords">
                <div class="span12 ctrls">
                    <input type="text" name="stream_keyword" value="<?= $stream_keyword ?>" placeholder="Keywords"
                           class="search span12 padding-1 stream_keyword">
                </div>
            </div>
            <div class="span3 pull-right">
                <div class="container span12">

                    <div class="field-box">
                        <div class="ui-select find">
                            <select class="span5 inline-input" name="activity_type" id="activity_type"
                                    onchange="FilterStreams();">
                                <option value="">Select Type</option>
                                <?php
                                foreach ($get_activity_type as $key => $activity_type) {
                                    ?>
                                    <option
                                        value="<?php echo $activity_type['activity_type_id'] ?>"><?php echo $activity_type['activity_title'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="navbar navbar-inverse">
                <ul style="margin: 0 10px 10px 10px; list-style:none;">
                    <li class="stream-container">
                        <div class="stream-dialog">
                            <div class="filter-result"></div>
                            <div class="no-filter notes">
                                <div class="notes">

                                    <?php
                                    if (is_array($job_activity) == "") {
                                        ?>
                                        <div class="alert alert-info">
                                            <i class="icon-exclamation-sign"></i>
                                            There is no activity
                                        </div>
                                        <!--<a href="#" class="item">
                                            There is no activity

                                        </a>--><?php

                                    } else {
                                        //Tyroe Free Stream
                                        foreach ($job_activity as $job_activity_log) {
                                            $icon;
                                            $text;
                                            $link = "";
                                            $href = "javascript:void(0)";
                                            $onclick = "";
                                            if ($job_activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                                                /*JOB APPLICANT SECTION*/
                                                $return_activity_stream = $vObj->activity_sub_type_text($job_activity_log); // Get the text...
                                                $icon = $return_activity_stream['icon'];
                                                $text = $return_activity_stream['text'];
                                                $href = $return_activity_stream['href'];
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $activity_sub_type = $activity_data_email['activity_sub_type'];

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_PROFILE_VIEW_JOB_LOG) {
                                                /*Profile Views Section*/
                                                $icon = LABEL_ICON_VIEW;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_VIEW_PROFILE_TEXT_LOG . " " . $activity_data_email['job_title'] . ", " . $activity_data_email['company_name'];


                                            } else if ($job_activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                                                /*Profile Favourited Section*/
                                                $icon = LABEL_ICON_STAR;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_FAVOURITED_PROFILE_TEXT . " by " . $activity_data_email['job_title'] . " from " . $activity_data_email['company_name'];

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_RECOMMEND_COMPLETE) {
                                                /*Recommedation Section*/
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_RECOMMEND_COMPLETE_TEXT . "  " . $activity_data_email['job_title'] . " at " . $activity_data_email['company_name'];
                                                $href = $vObj->getURL("profile/#acc-5");

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                /*Feedback Section*/
                                                $icon = LABEL_ICON_ENVLOPE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_FEEDBACK_ACTIVITY_TEXT  . $activity_data_email['reviewer_company_name'];
                                                // $url = "feedback/".$activity_data_email['rev_id'];
                                                $url = "feedback/" . strtolower(str_replace(' ', '-', $activity_data_email['rev_name']) . "-" . $vObj->generate_job_number($activity_data_email['rev_id']));
                                                $href = $vObj->getURL($url);

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                /*JOB SHORTLIST Section*/
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_SHORTLIST_JOB_LOG_TEXT . " " . $activity_data_email['job_title'] . " at " . $activity_data_email['company_name'];
                                                $url = "opening/" . strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                                                $href = $vObj->getURL($url);

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                                                /*JOB CANDIDATE Section*/
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_JOB_CANDIDATE_ACTIVITY_TEXT . " " . $activity_data_email['job_title'] . " at " . $activity_data_email['company_name'];
                                                $url = "opening/" . strtolower(str_replace(' ', '-', $activity_data_email['job_title']) . "-" . $vObj->generate_job_number($activity_data_email['job_id']));
                                                $href = $vObj->getURL($url);

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_NEW_JOB_LOG) {
                                                $icon = LABEL_ICON_STAR;
                                                $text = LABEL_NEW_JOB_TEXT_LOG;

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_HIDE_JOB_LOG) {
                                                $icon = LABEL_ICON_MOVE;
                                                $text = LABEL_HIDE_JOB_TEXT_LOG;

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_INVITE_TYROE_JOB_LOG) {
                                                $icon = LABEL_ICON_MOVE;
                                                $text = $job_activity_log['username'] . " " . LABEL_ADD_CANDIDATE_TEXT_LOG;

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                $icon = LABEL_ICON_MOVE;
                                                $text = LABEL_SHORTLIST_JOB_TEXT_LOG . " " . $job_activity_log['job_title'];
                                                $link = LABEL_SHORTLIST_JOB_LOG;
                                                $id = $job_activity_log['job_id'];

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_INVITE_JOIN_TYROE_JOB_LOG) {
                                                $icon = LABEL_ICON_MOVE;
                                                $text = $job_activity_log['username'] . " " . LABEL_INVITE_TYRO_TEXT_LOG;

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_INVITE_JOIN_ACCEPT_JOB_LOG) {
                                                $icon = LABEL_ICON_MOVE;
                                                $text = $job_activity_log['username'] . " " . LABEL_INVITE_TYRO_ACCEPT_TEXT_LOG;

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_APPLICAINT_MOVE_TO_CANDIDATE) {
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_CANDIDATE_CONFIRM_TEXT . " " . $activity_data_email['job_title'] . ' at ' . $job_activity_log['company_name'];

                                            } else if ($job_activity_log['activity_type_id'] == LABEL_CANDIDATE_MOVE_TO_SHORTLIST) {
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = LABEL_SHORTLIST_JOB_TEXT_LOG . " " . $activity_data_email['job_title'] . " job at " . $job_activity_log['company_name'];
                                            } else if ($job_activity_log['activity_type_id'] == LABEL_ACCEPT_JOB_INVITATION) {
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = $activity_data_email['firstname'] . " " . $activity_data_email['lastname'] . " " . LABEL_ACCEPT_JOB_INVITATION_TEXT . " of " . $activity_data_email['job_title'] . " job";
                                                $id = $job_activity_log['job_id'];
                                                $onclick = "candidate_call('" . $job_activity_log['job_id'] . "')";
                                            } else if ($job_activity_log['activity_type_id'] == LABEL_REVIEWED_ACTIVITY) {
                                                $icon = LABEL_ICON_MOVE;
                                                $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                $text = $activity_data_email['firstname'] . " " . $activity_data_email['lastname'] . " " . LABEL_REVIEWED_ACTIVITY_TEXT;
                                                $id = $job_activity_log['job_id'];
                                                //$onclick = "candidate_call('".$activity_log['job_id']."')";
                                            }
                                            ?>
                                            <a href="<?= $href ?>" onclick="<?= $onclick ?>" class="activity_val"
                                               data-id="<?= $link ?>" data-value="<?= $id ?>">
                                                <span class="item">
                                                <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                                                    <?= $text ?>
                                                    <span class="time"><i
                                                            class="icon-time"></i> <?php echo get_activity_time($job_activity_log['created_timestamp']); ?></span>
                                            </a></span><?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <input type="hidden" value="" name="id" class="id">


    <div class="separator t-magic"></div>
    <div class="span12"><span class="pull-right button-center"><a href="<?= $vObj->getURL("activity-stream") ?>"
                                                                  class="btn-flat gray" id="comment">View
                More</a></span></div>
    </div>

    </div>
    </div>

    </div>
    </div>
    </div>

    <!-- end main container -->


<?php
}
?>

<div id="element_to_pop_up"></div>
<div style="display: none;" class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>

                </div>
                <div class="span8 email_content_body">
                    <div class="global-popup_body ">
                        <div class="center prof-dtl">
                            <div class="tab-margin">
                                <div class="sugg-msg alert alert-info" style="display: none"></div>
                                <div class="clearfix"></div>
                                <div class="field-box">
                                    <input type="text" name="email-subject" id="email-subject" placeholder="Subject"/>
                                </div>
                                <div class="field-box">
                                    <textarea name="" id="" class="email-message" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="global-popup_footer">
                        <button type="button" class="btn btn-flat white email_close" style="display: none"
                                data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-flat btn-save btn-send-email" value="<?= LABEL_SUBMIT ?>">
                            <?= LABEL_SUBMIT ?>
                        </button>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>



<?php if ($this->session->userdata('role_id') == '3') { ?>
    <!--new job modal end-->

    <div style="display: none;" class="modal modal_job_opening  fade" id="myModal_postjob" role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true">
    <div class="job_create_container">
    <div class="job_create_details">
    <div class="modal-content job_create_holder global-popup ">
    <div class="modal-header job_create_header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">
            <div class="wizard row" id="fuelux-wizard">
                <ul class="wizard-steps">
                    <li class="active" data-target="#step1">
                        <span class="step">1</span>
                        <span class="title">Job <br> details</span>
                    </li>
                    <li data-target="#step2" class="">
                        <span class="step">2</span>
                        <span class="title">Review <br> team</span>
                    </li>
                    <li data-target="#step3" class="">
                        <span class="step">3</span>
                        <span class="title">Settings <br> Preview</span>
                    </li>
                </ul>
            </div>
        </h4>
    </div>
    <div class="job_create_body">
    <div class="step-content">
    <div id="step1" class="step-pane active">
        <div class="row form-wrapper">
            <div class="span5 center-block">
                <div class="modal_error_box alert alert-danger">
                </div>
                <form id="createjobform">
                    <div class="field-box">
                        <input type="text" class="form-control" placeholder="Job Title" name="job_title"
                               id="dashboard_job_title" onkeypress="return checkspecialchars(event)">
                        <input type="hidden" name="hidden_username" id="hidden_username" value="<?= $studio_name ?>">
                    </div>
                    <div class="field-box">
                        <textarea name="" class="form-control" id="job_description" cols="30" rows="6"
                                  placeholder="Description"></textarea>
                    </div>
                    <div class="field-box" style="margin-bottom: 25px;">
                        <div class="row-fluid">
                            <div class="span12">
                                <select name="job_industry" placeholder="Specialities" id="job_industry"
                                        class="job_industry">
                                    <option></option>
                                    <?php
                                    foreach ($job_industries as $industry) {
                                        ?>
                                        <option
                                            value="<?= $industry['industry_id'] ?>"><?= $industry['industry_name'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="field-box" style="margin-bottom: 25px;">
                        <div class="row-fluid">
                            <div class="span6">
                                <select name="country" placeholder="Country" id="country"
                                        onchange="getcities(this.value);" class="country">
                                    <option></option>
                                    <?php
                                    foreach ($get_countries as $country) {
                                        ?>
                                        <option
                                            value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="span6" id="citybox">
                                <select name="city" id="city" class="city">
                                    <option></option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="field-box">
                        <div class="row-fluid">
                            <div class="span6 date-period">
                                <input type="text" name="startdate" id="startdate" placeholder="Start-Date">
                            </div>
                            <div class="span6 date-period">
                                <input type="text" name="enddate" id="enddate" placeholder="End-Date">
                            </div>
                        </div>
                    </div>
                    <div class=" field-box lined margin-30 job-type sr-sec">
                        <div class="row-fluid type-extra-padding">
                            <?php
                            foreach ($get_job_type as $type) {
                                ?>
                                <div class="span4">
                                    <div class="job-type-holder">
                                        <input type="radio" class="job_type"
                                               value="<?= $type['job_type_id'] ?>"
                                               data-id="<?= $type['job_type'] ?>"
                                               name="job_type"><span><?= $type['job_type'] ?></span></div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="field-box job-level sr-sec-2">
                        <div class="row-fluid">
                            <?php
                            foreach ($get_job_level as $level) {
                                ?>
                                <div class="span4">
                                    <div class="job-level-holder"><input type="radio" class="job_level"
                                                                         value="<?= $level['job_level_id'] ?>"
                                                                         data-id="<?= $level['level_title'] ?>"
                                                                         name="job_level"><span><?= $level['level_title'] ?></span>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>
                    <br clear="all">

                    <div id="field-box">
                        <div class="row-fluid"><input type="text" name="job_tags"
                                                      data-placeholder="TAGS: Photoshop, Character Design, Python, Maya"
                                                      id="job_tags" class="span12 form-control"
                                                      style="padding-left:20px;height: 45px"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div id="step2" class="step-pane">
        <div class="row form-wrapper">
            <div class="span4 center-block">
                <h3 class="label-head">Add existing reviewers</h3>

                <div class="field-box searchReviewArea">
                    <input type="text" id="search_existing_reviewer" placeholder="Search <?= $collaborators ?> Collaborators"
                           class="radius-search">

                    <ul class="searching_reviewer_result ext-rvw-list">

                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="span5 center-block">
                <div class="legend-sep"></div>
            </div>

            <div class="span5 center-block">
                <h3 class="label-head">Invite new reviewers</h3>

                <div class="field-box">
                    <div class="row-fluid">
                        <div class="span9">
                            <input type="text" name="invitation" placeholder="Emails" id="invitation_emails">
                        </div>
                        <div class="span3">
                            <button class="btn-flat success" id="send_invitaion">Send invite</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="clearfix"></div>

            <div class="form_users_list">
                <ul class="form-list-holder" id="reviewer_container">
                    <li>
                        <div class="span5 center-block">
                            <div class="row-fluid">
                                <div class="span8">
                                    <div class="span2 user-display">
                                        <?php
                                        $image;
                                        if ($team_admin['media_name'] != "") {
                                            $image = ASSETS_PATH_PROFILE_THUMB . $team_admin['media_name'];
                                        } else {
                                            $image = ASSETS_PATH_NO_IMAGE;
                                        }
                                        ?>
                                        <img src="<?php echo $image; ?>" alt="avatar"
                                             class="img-circle avatar hidden-phone">
                                    </div>
                                    <div class="span10">
                                        <a href="javascript:void(0);"
                                           class="name"><?php echo $team_admin['firstname'] . ' ' . $team_admin['lastname']; ?></a>
                                        <span class="subtext"><?php echo $team_admin['user_occupation']; ?> <span
                                                class="tag">Admin</span></span>
                                    </div>
                                </div>
                                <div class="span4">
                                    <a href="#" class="mail-user"><?php echo $team_admin['email']; ?></a>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div id="step3" class="step-pane">
        <div class="row-fluid form-wrapper">
            <div class="span10 center-block">
                <form>
                    <div class="team-mod-setting-container">
                        <h3>Settings</h3>

                        <div class="field-box" id="rt_description">
                            <div class="span10">
                                <h6>Team moderation</h6>

                                <p>Allow adminstrators to moderate all feedback shared with artists. If turned off, team
                                    members can share thier helpful feedback directly to artists on behalf of your
                                    company.</p>
                            </div>
                            <div class="span2">
                                <div class="slider-frame pull-right">
                                    <span data-on-text="ON" data-off-text="OFF" class="slider-button team_moderation">OFF</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="preview_job">
                        <h3>Preview</h3>
                    </div>
                    <div class="field-box" id="rt_description1"></div>
                </form>
            </div>
        </div>
    </div>
    <div id="step4" class="step-pane">
        <div class="row form-wrapper payment-info">
            <div class="col-md-8">
                <form>
                    <div class="field-box">
                        <label>Subscription Plan:</label>
                        <select id="plan">
                            <option value="66">Basic - $2.99/month (USD)</option>
                            <option value="67">Pro - $9.99/month (USD)</option>
                            <option value="68">Premium - $49.99/month (USD)</option>
                        </select>
                    </div>
                    <div class="field-box">
                        <label>Credit Card Number:</label>
                        <input type="text" class="form-control">
                    </div>
                    <div class="field-box">
                        <label>Expiration:</label>
                        <input type="text" placeholder="MM" style="width:60px;display:inline" class="form-control">
                        &nbsp; / &nbsp;
                        <input type="text" placeholder="YYYY" style="width:85px;display:inline" class="form-control">
                    </div>
                    <div class="field-box">
                        <label>Card CVC Number:</label>
                        <input type="text" class="form-control">
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
    </div>
    <div class="modal-footer job_create_footer">
        <div class="wizard-actions">
            <button class="btn-flat white job_close" data-dismiss="modal" aria-hidden="true" type="button">
                Close
            </button>
            <button data-last="Finish" class="btn-flat primary btn-next margin-left-2" type="button"
                    style="display: inline-block;" id="btn-next">
                Next
            </button>
            <button class="btn-flat success btn-finish" type="button" style="display: none;" id="post_job"
                    onclick="postjob()">
                Post Job
            </button>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready(function () {
        if ($(window).width() == 768) {
        }
    });
</script><!--Dashboard Artist End-->


