<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<div class="content">

<div class="container-fluid">
<div id="pad-wrapper">

    <div class="grid-wrapper">

        <div class="row-fluid show-grid">
            <!-- START LEFT COLUMN -->
            <?php
            $this->load->view('left_coloumn');
            ?>
            <!-- END LEFT COLUMN -->

            <!-- START RIGHT COLUMN -->
            <div class="span8 sidebar personal-info">
                <div class="row-fluid form-wrapper">
                    <div class="span12">
                        <div class="container-fluid">
                            <div class="span4 default-header">
                                <h4><?= LABEL_CREATE_REVIEWER ?></h4>
                            </div>
                            <div class="field-box error">
                                        <span><?php echo validation_errors();

                                            //if($msg!=""){echo $msg;}
                                            ?></span>
                            </div>
                            <form class="new_user_form inline-input" _lpchecked="1"
                                  enctype="multipart/form-data" name="reviewer_form" id="reviewer_form" method="post"
                                  action="<?= $vObj->getURL('reviewer/UpdateReviewer') ?>"
                                  onsubmit="return form_reviewer();">
                                <input type="hidden" value="<?php echo $get_reviewer['user_id']?>" name="edit_id">
                                <input type="hidden" value="<?php echo $get_reviewer['image_id']?>" name="image_id">
                                <input type="hidden" value="<?php echo $get_reviewer['created_timestamp']?>" name="created_timestamp">
                                <div class="span12 field-box">
                                    <label>Profile Pic:</label>

                                    <div class="profile-image span6">
                                        <?php
/*                                        $image;
                                        if ($get_reviewer['media_name'] != "") {
                                            $image = ASSETS_PATH_REVIEWER_THUMB . $get_reviewer['media_name'];
                                        } else {
                                            $image = ASSETS_PATH_NO_IMAGE;
                                        }*/
                                        ?>
                                        <img src="<?php echo $get_reviewer['image'];?>" class="avatar img-circle">

                                        <p>Upload a different photo...</p>
                                        <!--<input type="file" id="userfile" name="userfile">-->
                                        <input type="file" id="file_upload" name="file_upload">
                                        <script type="text/javascript">
                                            <?php $timestamp = time();?>
                                            $(function () {
                                                var image_name = '';
                                                var cnt = 0;
                                                var base_url = '<?=ASSETS_PATH?>';
                                                $('#file_upload').uploadify({
                                                    'formData': {
                                                        'timestamp': '<?php echo $timestamp;?>',
                                                        'token': '<?php echo md5('unique_salt' . $timestamp);?>'
                                                    },
                                                    'fileTypeExts': '*.jpg; *.JPG; *.jpeg; *.JPEG; *.png; *.PNG; *.gif; *.GIF; *.bmp; *.BMP',
                                                    'fileSizeLimit': '2MB',
                                                    'swf': '<?=ASSETS_PATH?>' + 'js/uplodify_js/uploadify.swf',
                                                    'uploader': '<?=LIVE_SITE_URL?>' + 'reviewer/upload_image',
                                                    'onUploadSuccess': function (file, data, response, fileObj) {

                                                        cnt++;
                                                        data = JSON.parse(data);
                                                        console.log(data);
                                                        var image_ext = data[0].file_ext;
                                                        var image_path = data[0].full_path;
                                                        var img_name = data[0].file_name;
                                                        //image_name = img_name.replace(image_ext, '_thumb' + image_ext, img_name);
                                                        if (cnt <= '4') {
                                                            $('#thumbnail').append('<img src=\"' + base_url + 'uploads/reviewer/' + img_name + '\" width="95" style="float:left;margin-left:1em;"/>');
                                                        }
                                                        $('#image').val(img_name);
                                                        // $("input[name='image']").val(img_name);
                                                        /* $('.imageNames').append(img_name + ',');
                                                         $('.imagePaths').append(image_path + ',');*/
                                                    }
                                                });
                                            });
                                        </script>

                                    </div>
                                    <!-- <div id="thumbnail"></div>-->
                                    <input type="hidden" name="image" value="" id="image">
                                </div>
                                <div class="field-box">
                                    <label><?= LABEL_FIRST_NAME ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_firstname" id="user_firstname"
                                           value="<?php echo $get_reviewer['firstname']; ?>">
                                </div>
                                <div class="field-box">
                                    <label><?= LABEL_LAST_NAME ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_lastname" id="user_lastname"
                                           value="<?php echo $get_reviewer['lastname']; ?>">
                                </div>

                                    <input class="span9" type="hidden"
                                   name="user_email" id="user_email"
                                   value="<?php echo $get_reviewer['email']; ?>" readonly="">

                                <div class="field-box">
                                    <label><?= LABEL_USER_NAME ?>:</label>
                                    <input class="span9 inline-input" type="text"
                                           name="user_name" id="user_name"
                                           value="<?php echo $get_reviewer['username']; ?>">
                                </div>
                                <div class="field-box">
                                    <label><?= LABEL_PASSWORD ?>:</label>
                                    <input class="span9 inline-input" type="password"
                                           name="user_password" id="user_password"
                                           value="<?php echo $get_reviewer['password']; ?>">
                                </div>
                                <div class="field-box">
                                    <label><?= LABEL_CONFIRM_PASSWORD ?>:</label>
                                    <input class="span9 inline-input" type="password"
                                           name="user_confirmpass" id="user_confirmpass"
                                           value="<?php echo $get_reviewer['password']; ?>">
                                </div>

                                <div class="span12 field-box">
                                    <label>Country:</label>

                                    <div class="ui-select span6">
                                        <select class="span5 inline-input" name="user_country"
                                                id="user_country">
                                            <option value="">Select Country</option>
                                            <?php
                                            foreach ($get_countries as $key => $countries) {
                                                ?>
                                                <option <?php if ($countries['country_id'] == $get_reviewer['country_id']) {
                                                echo "selected='selected'";
                                                } else {
                                                }
                                                ?>
                                                    value="<?php echo $countries['country_id'] ?>"><?php echo $countries['country'] ?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_STATE ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_state" id="user_state"
                                           value="<?php echo $get_reviewer['state']; ?>">
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_CITY ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_city" id="user_city"
                                           value="<?php echo $get_reviewer['city']; ?>">
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_ZIPCODE ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_zipcode" id="user_zipcode"
                                           value="<?php echo $get_reviewer['zip']; ?>">
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_ADDRESS ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_address" id="user_address"
                                           value="<?php echo $get_reviewer['address']; ?>">
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_PHONE ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_phone" id="user_phone"
                                           value="<?php echo $get_reviewer['phone']; ?>">
                                </div>
                                <div class="span12 field-box">
                                    <label><?= LABEL_CELLPHONE ?>:</label>
                                    <input class="span9" type="text"
                                           name="user_cellphone" id="user_cellphone"
                                           value="<?php echo $get_reviewer['cellphone']; ?>">
                                </div>

                                <div class="span11 field-box actions">
                                    <input type="submit" name="submit" class="btn-glow primary"
                                           value="<?= LABEL_SAVE_CHANGES ?>">
                                    <span>OR</span>
                                    <input type="reset" onclick="window.history.back()" value="Cancel" class="reset">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END RIGHT COLUMN -->
            <div>


            </div>


        </div>
    </div>
</div>
</div>
