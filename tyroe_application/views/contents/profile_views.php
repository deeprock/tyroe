<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!-- main container -->
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div><?php if ($profile_updated != "") {
                    echo $profile_updated;
                };?></div>
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">
                            <div class="span12">
                                <div class="container-fluid">
                                    <div class="span4 default-header">
                                        <h4>Profile Views</h4>
                                    </div>
                                    <!-- USERS LISTING -->
                                    <?php
                                    if (is_array($viewers_data)) {
                                        for ($pv = 0; $pv < count($viewers_data); $pv++) {

                                            ?>
                                            <div class="row-fluid">
                                                <div id="top" class="span12 people">
                                                    <div class="row-fluid">
                                                        <div class="span11">
                                                            <div class="span11">
                                                                <div id="push-down-10" class="span2">
                                                                    <img class="avatar-55" src="<?= $viewers_data[$pv]['media_name'] ?>">
                                                                </div>
                                                                <div class="span10">
                                                                    <span style="width: 100%; float:left; clear: both;"><p
                                                                            class="span12"
                                                                            style="width:100px; float:left;"><strong>When: </strong>
                                                                        </p><span
                                                                            style="width: 335px; float:left;"><?= date("d M Y", $viewers_data[$pv]['created_timestamp']); ?></span></span>
                                                                    <?php
                                                                    if ($this->session->userdata("role_id") == TYROE_FREE_ROLE) {
                                                                        ?>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Reviewer: </strong></p><span
                                                                                style="width: 335px; float:left;">Pro's can see this (<a
                                                                                    href="<?= $vObj->getURL('account/subscription') ?>">upgrade
                                                                                    now</a>)</span></span>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Studio: </strong></p><span
                                                                                style="width: 335px; float:left;"> Pro's can see this (<a
                                                                                    href="<?= $vObj->getURL('account/subscription') ?>">upgrade
                                                                                    now</a>)</span></span>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Opening: </strong></p><span
                                                                                style="width: 335px; float:left;"> Pro's can see this (<a
                                                                                    href="<?= $vObj->getURL('account/subscription') ?>">upgrade
                                                                                    now</a>)</span></span>
                                                                    <?php
                                                                    } else {
                                                                        ?>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Reviewer: </strong></p><span
                                                                                style="width: 335px; float:left;"><?= $viewers_data[$pv]['reviewer']; ?></span></span>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Studio: </strong></p><span
                                                                                style="width: 335px; float:left;"> <?= $viewers_data[$pv]['studio']; ?></span></span>
                                                                        <span
                                                                            style="width: 100%; float:left; clear: both;"><p
                                                                                class="span12"
                                                                                style="width:100px; float:left;">
                                                                                <strong>Opening: </strong></p><span
                                                                                style="width: 335px; float:left;"> <?= $viewers_data[$pv]['job_title']; ?></span></span>
                                                                    <?php
                                                                    }
                                                                    ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                    } else {
                                        ?>
                                        <div class="row-fluid">
                                            <div id="top" class="span12 people">
                                                <span class="name">
                                            <?php
                                            echo "No one has visited your profile recently.";
                                            ?>
                                                </span>
                                            </div>
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    <!-- USERS LISTING -->

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
    //}
    ?>