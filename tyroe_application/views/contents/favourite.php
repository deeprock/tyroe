<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!--<script src="<?php/*= ASSETS_PATH */?>js/jquery-1.9.1.min.js"></script>
<script src="<?php/*= ASSETS_PATH */?>js/popup/popup.js"></script>-->
<script type="text/javascript">
//Global variables
var msg_to = '';
var click_id = 0;
var search_list_inner_html = '';
var total_person = <?php echo count($get_rows_for_pagination); ?>;
<?php
if($page_number > 1){
        $pos_id = ($page_number-1) * 12;
    }else{
        $pos_id=1;
    }
?>
var page_position = <?php echo $pos_id; ?>;
//for home page searching area

//End for home page searching area====
$(document).ready(function () {
    $('#myModal').modal('hide');
    $(document).on("click", ".btn-msg", function () {
        $(".sugg-msg").html("");
        msg_to = $(this).attr("data-value");
        $("#hd_email_to" + msg_to).val(msg_to);
        $('#myModal').modal('show');
        rescale();
        $('.modal-body').css('height', 146);
    });

    //Suggestion meesage Sending
    $(document).on("click", ".btn-send-email", function () {
        if ($('#email-subject').val() == "") {
            $('#email-subject').css({
                border: '1px solid red'
            });
            $('#email-subject').focus();
            return false;
        }

        if ($('#email-subject').val() != "") {
            $('#email-subject').css({
                border: ''
            });
        }


        if ($('.email-message').val() == "") {
            $('.email-message').css({
                border: '1px solid red'
            });
            $('.email-message').focus();
            return false;
        }

        if ($('.email-message').val() != "") {
            $('.email-message').css({
                border: ''
            });
        }
        //else{
        $(this).text('Sending..');
        $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
        $(this).removeClass('btn-send-email');
        $(this).addClass('add_email_dummyclass');
        var email_message = $(".email-message").val();
        var email_subject = $("#email-subject").val();
        var email_to = msg_to;
        var data = "callfrom=send_email&email_message=" + email_message + "&email_subject=" + email_subject + "&email_to=" + email_to;
        $.ajax({
            type: 'POST',
            data: data,
            url: '<?=$vObj->getURL("search/access_module");?>',
            dataType: 'json',
            success: function (responseData) {
                if (responseData.success) {
                    $(".sugg-msg").show();
                    $(".sugg-msg").html("Your message send success.");
                    $('.add_email_dummyclass').text('Send');
                    $('.add_email_dummyclass').addClass('btn-send-email');
                    $('.btn-send-email').removeClass('add_email_dummyclass').hide();
                    $('.small_loader').remove();
                    $('.email_close').show();
                }
            }
        });
        //return true;

        //}
    });
});
function rescale() {
    var size = {width: $(window).width(), height: $(window).height() }
    var offset = 20;
    var offsetBody = 240;
    //$('#myModal').css('height', size.height - offset );
    $('.modal-body').css('height', size.height - (offset + offsetBody));
    $('#myModal').css('top', '3%');
}

$(window).bind("resize", rescale);
$(document).ready(function () {
    $(".batch-selection").on("click", function () {
        if (this.text == "Batch Contact") {

        } else if (this.text == "Batch Add to Job") {
            /* var chk_count = $("input[type=selected_tyroe_chk]").length;
             if (chk_count <= 0) {
             alert("Haven't select any user ");

             }*/

        }
        else if (this.text == "Batch Remove") {
            var id_s = $('input[name="selected_tyroe_chk"]:checked');
            $.each(id_s, function (index, item) {
                var tyroes_val = item.value;
                $(".no-filter-tyroe.detailed-tyroe" + tyroes_val).show();
                $(".tyroe" + tyroes_val).hide();
            });


        }
    });

    /*Add in openings*/
    $(document).on("click", ".add_in_opening_jobs", function () {
        /*First Param =  JOB ID*/
        /*Second Param =  Tyroe ID*/
        $('.loader-save-holder').show();
        var job_tyroe_id = $(this).data('value');
        var job_tyroe_arr = (job_tyroe_id).split(",");
        var job_id = job_tyroe_arr[0];
        var tyroe_id = job_tyroe_arr[1];
        $(this).addClass('disable_opening');
        $(this).removeClass('add_in_opening_jobs');
        $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'add_by_search'},
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Add in openings*/

    /*Hide Search*/
    $(document).on("click", ".hide_search", function () {
        var tyroe_id = $(this).data('value');
        $.ajax({
            type: "POST",
            data: {tyroe_id: tyroe_id},
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".row-id" + tyroe_id).hide(500);
                    $(".editor-seperator-row" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    });
    /*Hide Search*/

});
function AddToJob() {
    var job_id = $("#job_id").val();
    var tyreo_arr = [];
    var id_s = $('input[name="selected_tyroe_chk"]:checked');
    $.each(id_s, function (index, item) {
        var tyroes_val = item.value;
        tyreo_arr.push(tyroes_val);
    });
    var tyroe_id = tyreo_arr.join(",");
    if (tyroe_id == "") {

    }
    if (job_id == "") {
        alert("Job Selection Required");
    } else {
        var data = "tyroe_id=" + tyroe_id + "&job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".detailed-tyroe" + tyroe_id).hide(500);
                    $(".selected.tyroe" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
}

function AddSelection(tyro_id) {
    $(".detailed-tyroe" + tyro_id).hide();
    $(".tyroe" + tyro_id).show();
}

function HideTyroe(tyroe_id) {
    var data = "tyroe_id=" + tyroe_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".detailed-tyroe" + tyroe_id).hide(500);
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });


}

function ContactTyroe(tyro_id) {

}
</script>

<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {

        // DOM Ready
        $(function () {

            // Binding a click event
            // From jQuery v.1.7.0 use .on() instead of .bind()
            $('#invite').bind('click', function (e) {

                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired
                $('#element_to_pop_up').bPopup();

            });

        });

    })(jQuery);
</script>
<script>
    //selecting single user
    function getsingle_user(page_number, search_user_id) {
        page_position = page_number;
        $
        $('.content .profile_container').removeClass('container');
        $('.content .profile_container').addClass('container-fluid');
        $('.content .profile_container.container-fluid').css({"padding": "0"});
        $('.loader-save-holder').show();
        $('.pagination_controllers').removeClass('hidden');
        if (click_id == 0) {
            search_list_inner_html = $('.filter_editor').html();
            click_id = 1;
        }
        $.ajax({
            type: 'POST',
            data: ({'current_userid': search_user_id}),
            url: "<?= $vObj->getURL("searchpublicprofile/index"); ?>",
            success: function (html) {
                $('.single_num_of_records').text(page_position + ' of ' + total_person);
                setTimeout(function () {
                    $('.loader-save-holder').hide();
                    $('.filter_editor').html(html);
                    $("#single-team").contents().find(".btn_mail_n_remove").show();
                    $('#single-team').contents().find('.favmargin').css({"margin-top":"0px"});
                    var iframe_email_controllers_html = $("#single-team").contents().find(".btn_mail_n_remove").html();
                    $('.email_sending_btns').html(iframe_email_controllers_html);
                    initFrame();
                    $('.email_sending_btns').find('ul+button.middle').addClass('un_favourite').removeAttr('onClick').attr({'data-value': search_user_id}).css({background: '#2388d6'}).children('i').css({"color": "#ffffff"});
                    $('#profile_iframe').show();
                    $('#profile_content').remove();

                }, 3000);
            }
        });
    }
    //creating single user pagination
    $(document).on("click", ".listview_btn", function () {
        $('.content .profile_container').removeClass('container-fluid');
        $('.content .profile_container').addClass('container');
        $('.loader-save-holder').show();
        $('.filter_editor').html('');
        $('.filter_editor').html(search_list_inner_html);
        $('.pagination_controllers').addClass('hidden');
        setTimeout(function () {
            $('.loader-save-holder').hide();
        }, 3000);
        click_id = 0;
    });
    $(document).on('click', '.single_next', function () {
        page_position++;
        if (page_position > total_person) {
            page_position = 1;
            page_found = $('#user_unique_id' + page_position).attr('data-id');
            page_position = page_found;
            page_user_id = $('#user_unique_id' + page_position).val();
            getsingle_user(page_position, page_user_id);
        } else {
            page_found = $('#user_unique_id' + page_position).attr('data-id');
            page_position = page_found;
            page_user_id = $('#user_unique_id' + page_position).val();
            getsingle_user(page_position, page_user_id);
        }

    });
    $(document).on('click', '.single_back', function () {
        page_position--;
        if (page_position < 1) {
            page_position = total_person;
            page_found = $('#user_unique_id' + page_position).attr('data-id');
            page_position = page_found;
            page_user_id = $('#user_unique_id' + page_position).val();
            getsingle_user(page_position, page_user_id);
        } else {
            page_found = $('#user_unique_id' + page_position).attr('data-id');
            page_position = page_found;
            page_user_id = $('#user_unique_id' + page_position).val();
            getsingle_user(page_position, page_user_id);
        }
    });
    //End creating single user pagination
    //End selecting single user

    //unfavourite profile
    $(document).on('click', '.un_favourite', function () {
        $('.loader-save-holder').show();
        var uid = $(this).data('value');
        var data = "uid=" + uid + "&fav_action=unfavourite";
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    if ($('.listview_btn').is(':visible')) {
                        $('.listview_btn').trigger('click');
                    }
                    $('.hiderow' + uid).fadeOut();
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.loader-save-holder').hide();

            },
            failure: function (errMsg) {
                $('.loader-save-holder').hide();
            }
        });
    });
</script>
<div style="display: none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<!-- main container -->
<!-- Element to pop up -->
<div id="element_to_pop_up">
    <div class="job-selection">
        <div class="field-box">
            <label>Select Job:</label>

            <div class="ui-select find">
                <select name="job_id" id="job_id">
                    <option value="">Select Job</option>
                    <?php
                    foreach ($get_jobs as $jobs) {
                        ?>
                        <option value="<?php echo $jobs['job_id']; ?>"><?php echo $jobs['job_title']; ?></option>
                    <?php
                    }
                    ?>
                </select></div>
            <div class="span9">
                <a class="btn-flat user-search" onclick="AddToJob();">SUBMIT</a>
            </div>
        </div>
    </div>

    <div class="contact-form" style="display: none">
        <div class="field-box">
            <label>Contact Form</label>
        </div>
    </div>
</div>

<div class="public-profile" style="display: none">
    <iframe class="public-profile-frame"></iframe>
</div>
<div class="content favcontent" style="padding-top: 0px;padding-bottom: 0px;">
<!--<div class="container-fluid">
    <div class="row-fluid">
        <div id="Search_filters" class="span11">
            <div class="row">
                <div class="field-box pull-right">
                    <div class="ui-select">
                        <select>
                            <option selected="">Dropdown</option>
                            <option>Custom selects</option>
                            <option>Pure css styles</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>-->


<div class="container-fluid profile_container padding-default">


<div id="pad-wrapper" class="no-padding">
    <!--<div class="row-fluid header z-margin-b">
        <div id="admin_users_top_header" class="span9">
            <h5 class="resume-title"><?php/*= LABEL_FAVOURITE_TYROES */?></h5>
        </div>
    </div>-->
    <div class="pagination_controllers hidden">
        <div class="editor-seperator"></div>
        <div class="container-fluid">
            <div class="row-fluid">
                <div id="Search_filters" class="span11 margin-top-5">
                    <div class="row">
                        <div id="editors_pick" class="rt-edit-list">
                            <div id="editors_pick_info">
                                <div class="span4 offset1">
                                    <button class="btn btn-primary listview_btn list-rt-btn">LIST VIEW</button>
                                    <div class="btn-group">
                                        <button class="btn btn-default single_back glow left" type="button"><i
                                                class="icon-angle-left"></i></button>
                                        <button class="btn btn-default single_next glow right" type="button"><i
                                                class="icon-angle-right"></i></button>
                                    </div>
                                    <span class="single_num_of_records"></span>
                                </div>
                                <div class="span4 pull-right">
                                    <div class="email_sending_btns pull-right btn-group large ">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="editor-seperator"></div>
</div>
<div class="grid-wrapper">

    <div class="row-fluid show-grid ">


        <!-- START RIGHT COLUMN -->
        <!--This area for single layout pagination purpose-->
        <div class="single_person_pagination">
            <?php
            if ($page_number > 1) {
                $pos_id = 1;
            } else {
                $pos_id = 1;
            }
            if($get_rows_for_pagination != ''){
            for ($tyr = 0; $tyr < count($get_rows_for_pagination); $tyr++) {
                ?>
                <input type="hidden" id="user_unique_id<?php echo $pos_id; ?>" class="users_ids"
                       data-id="<?php echo $pos_id; ?>" value="<?= $get_rows_for_pagination[$tyr]['user_id'] ?>"/>
                <?php $pos_id++;
            } }?>
        </div>
        <!--End single layout pagination purpose-->
        <div class="">
            <div class="right-column filter_editor no-padding" style="position:relative;">
                <?php
                if ($page_number > 1) {
                    $pos_id = (($page_number - 1) * 12) + 1;
                } else {
                    $pos_id = 1;
                }
                if ($get_tyroes != '') {
                    for ($tyr = 0; $tyr < count($get_tyroes); $tyr++) {
                        $main_user_id = $get_tyroes[$tyr]['user_id'];
                        $image;
                        if ($get_tyroes[$tyr]['media_name'] != "") {
                            //$image = ASSETS_PATH_PROFILE_THUMB . $get_tyroes[$tyr]['media_name'];
                            $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_tyroes[$tyr]['media_name'];
                        } else {
                            $image = ASSETS_PATH_NO_IMAGE;
                        }
                        ?>
                        <div class="row-fluid rg-bg hiderow<?= $get_tyroes[$tyr]['user_id'] ?>">
                            <div class="container">
                                <div class="span12 editor-pck row-id<?= $get_tyroes[$tyr]['user_id'] ?>">
                                    <div class="row-fluid">
                                        <div class="span12" id="top">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <div class="edt_pck_sec">
                                                        <div class="user-dp-holder">
                                                            <a href="javascript:void(0)"
                                                               onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)" style="position: relative;display: inline-block;"><img
                                                                    src="<?php echo $image; ?>"
                                                                    class="img-circle avatar-80">
                                                            
                                                            
                                                            
                                                            <?php
                    if (intval($get_tyroes[$tyr]['availability']) == 1) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#96bf48" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE FOR WORK"></i>
                    <?php
                    } else if (intval($get_tyroes[$tyr]['availability']) == 2) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#5ba0a3" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE SOON"></i>
                    <?php
                    } else if (intval($get_tyroes[$tyr]['availability']) == 3) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i>
                    <?php
                    }else{
                    ?>
                       <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i> 
                    <?php    
                    }
                    ?>
                                                            </a>
                                                        </div>
                                                        <div class="editors_picked" id="editors_pick">
                                                            <div class="editors_pick-info" id="editors_pick_info">
                                                                <!--<a href="javascript:void(0);" class="name" value="<?php/*=$main_user_id*/?>"><?php /*echo $get_tyroes[$tyr]['username']; */?></a>-->
                                                                <a href="javascript:void(0)"
                                                                   onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)"
                                                                   class="name"><?php if(isset($has_profile_access) && $has_profile_access == 0){
                                                                    echo "Name Hidden";
                                                                }else {
                                                                    echo $get_tyroes[$tyr]['firstname'] . ' ' . $get_tyroes[$tyr]['lastname'];
                                                                }?></a></a>
                                            <span class="location"><?php
                                                /*for ($a = 0; $a < count($get_tyroes[$tyr]['skills']); $a++) {
                                                    $temp = $get_tyroes[$tyr]['skills'][$a]['creative_skill'];
                                                    echo ($a > 0)?", ":"";
                                                    if(!empty($temp)){
                                                        echo $temp;
                                                    }

                                                }*/
                                                echo rtrim($get_tyroes[$tyr]['industry_name'] . ', ' . $get_tyroes[$tyr]['extra_title'], ', ');
                                                ?></span>
                                                                <span
                                                                    class="tags"><?php echo $get_tyroes[$tyr]['city'] . "," . $get_tyroes[$tyr]['country']; ?></span>

                                                                <br clear="all">

                                                                <div class="btn-group large">
                                                                    <?php
                                                                     $class = 'left';
                                                                     $style = 'border-radius: 4px 4px 4px 4px;';
                                                                    if(isset($has_profile_access) && $has_profile_access == 1) {
                                                                        $class = 'middle';
                                                                        $style = '';
                                                                    ?>
                                                                    <button class="glow left" data-toggle="dropdown"><i
                                                                            class="icon-plus-sign"></i></button>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="javascript:void(0)" class=""
                                                                               style="color:#999999"><?= "Add to opening:"; ?></a>
                                                                        </li>
                                                                        <?php for ($op = 0; $op < count($get_openings_dropdown); $op++) { ?>
                                                                            <?php $duplicate_job = 0;
                                                                            foreach ($get_tyroes[$tyr]['tyroe_jobs'] as $tyroe_job) {
                                                                                ?>
                                                                                <?php if ($tyroe_job['job_id'] == $get_openings_dropdown[$op]['job_id']) {
                                                                                    $duplicate_job = 1;
                                                                                    ?>
                                                                                    <li><a href="javascript:void(0)"
                                                                                           class="disable_opening"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                                    </li>

                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <?php if ($duplicate_job == 0) { ?>
                                                                                <li><a href="javascript:void(0)"
                                                                                       class="add_in_opening_jobs"
                                                                                       data-value="<?= $get_openings_dropdown[$op]['job_id'] . "," . $get_tyroes[$tyr]['user_id']; ?>"><?= $get_openings_dropdown[$op]['job_title']; ?></a>
                                                                                </li>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                    </ul>
                                                                    <?php } ?> 
<!--                                                                    <button class="glow middle un_favourite"
                                                                            style="background: #2388d6;"
                                                                            data-value="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                        <i
                                                                            class="icon-star" style="color:#ffffff"></i>
                                                                    </button>-->
                                                                     
                                                                    <button class="glow <?= $class ?> un_favourite"
                                                                            data-value="<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                                            style="background: #2388d6;<?=$style?>" ><i
                                                                            class="icon-star" style="color:#ffffff"></i>
                                                                    </button>
                                                                     <?php if(isset($has_profile_access) && $has_profile_access == 1) { ?>
                                                                        <button class="glow middle btn-msg"
                                                                                data-value="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                            <i class="icon-envelope "></i></button>
                                                                        
                                                                    <button class="glow right hide_search"
                                                                            data-value="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                        <i class="icon-trash"></i></button>
                                                                    <input type="hidden" name="hd_email_to"
                                                                           id="hd_email_to<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                                           value=""/>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class="editors_showcase-container"
                                                                 id="editors-showcase">
                                                                <div class="editors_showcase_holder">
                                                                    <div
                                                                        class="full-profile_overlay black_overlay_separate<?= $get_tyroes[$tyr]['user_id'] ?>"
                                                                        onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_tyroes[$tyr]['user_id']; ?>)">
                                                                        <div class="full-profile_overlay_holder">
                                                                            <p>View Full Profile</p>
                                                                            <i class="icon-picture"></i>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row-fluid">
                                                                        <?php
                                                                        //for ($a = 0; $a < count($get_tyroes[$tyr]['portfolio']); $a++) {
                                                                        for ($a = 0; $a < 3; $a++) {
                                                                            if($get_tyroes[$tyr]['portfolio'][$a]['media_name']){
                                                                               $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_tyroes[$tyr]['portfolio'][$a]['media_name'];
                                                                               if (!file_exists($portfolio)) {
                                                                                   $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                               }else{
                                                                                   $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_tyroes[$tyr]['portfolio'][$a]['media_name'];
                                                                               }
                                                                            }else{
                                                                                $portfolio = ASSETS_PATH.'/img/image-icon.png';
                                                                            }
                                                                            ?>
                                                                            <div class="showcase_thumb pull-right">
                                                                                <a href="javascript:void(0);"
                                                                                   class="score_img_anchor<?= $a ?>"
                                                                                   data-id="<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                                    <?= (($a == 2) && ($get_tyroes[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                                    <?php if ($a == 0) { ?>
                                                                                        <?php if ($get_tyroes[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                                            <div
                                                                                                class="tyroe_score_over_img tyroeScoreHolder<?= $get_tyroes[$tyr]['user_id'] ?>">
                                                                                                <div
                                                                                                    class="tyroe_score_holder">
                                                                                                    <?= $get_tyroes[$tyr]['get_tyroes_total_score'] ?>
                                                                                                </div>
                                                                                            </div>
                                                                                        <?php } ?>
                                                                                    <?php
                                                                                    }
                                                                                    ?>
                                                                                    <img src="<?php echo $portfolio ?>"
                                                                                         alt="editor-picks-<?= $get_tyroes[$tyr]['is_user_featured'] ?>">
                                                                                </a>
                                                                            </div>
                                                                        <?php
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="editor-seperator editor-seperator-row<?= $get_tyroes[$tyr]['user_id'] ?>"></div>
                        </div>
                        <?php
                        $pos_id++;
                    } ?>
                    <!-- START PAGINATION -->
                    <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                          method="post" action="<?= $vObj->getURL('favourite') ?>">
                        <div class="span11 pagination"><?= $pagination ?></div>
                        <input type="hidden" value="<?= $page ?>" name="page">
                    </form>
                <?php } else { ?>
                    <div class="span12 alert alert-warning span10 center-block" style="float: none;margin-top: 20px !important">
                        <i class="icon-warning-sign"></i>
                        You have not favourited any talent yet.
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>

<!-- end main container -->

<div style="display: none;" class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>

                </div>
                <div class="span8 email_content_body">
                    <div class="global-popup_body ">
                        <div class="center prof-dtl">
                            <div class="tab-margin">
                                <div class="sugg-msg alert alert-info" style="display: none"></div>
                                <div class="clearfix"></div>
                                <div class="field-box">
                                    <input type="text" name="email-subject" id="email-subject" placeholder="Subject"/>
                                </div>
                                <div class="field-box">
                                    <textarea name="" id="" class="email-message" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="global-popup_footer">
                        <button type="button" class="btn btn-flat white email_close" style="display: none"
                                data-dismiss="modal">Close
                        </button>
                        <button type="button" class="btn btn-flat btn-save btn-send-email" value="<?= LABEL_SUBMIT ?>">
                            <?= LABEL_SUBMIT ?>
                        </button>

                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>