<?php if($get_records != ''){ ?>
        <?php  foreach($get_records as $get_record){
            $image;
            if ($get_record['media_name'] != "") {
                $image = ASSETS_PATH_PROFILE_THUMB . $get_record['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            ?>
            <li class="display_list<?php echo $get_record['user_id']; ?>">
                <div class="row-fluid">
                    <div class="span12" id="ext-rvw_img">
                        <div class="span3 text-center">
                            <img src="<?php echo $image; ?>" alt="avatar" class="img-circle avatar hidden-phone">
                        </div>
                        <div class="span6 text-left">
                            <a href="javascript:void(0);" class="name"><?php echo $get_record['firstname'].' '.$get_record['lastname']; ?></a>
                            <span class="subtext"><?php echo $get_record['job_title']; ?></span>
<!--                            <span class="add_rvw"><?php echo $get_record['email']; ?></span>-->
                        </div>
                        <div class="span3">
                            <a href="javascript:void(0)" class="btn-flat success select_reviewer select-reviewer-btn" data-value="<?php echo $get_record['user_id']; ?>" style="margin-top: 20px;">ADD</a> 
                        </div>
                    </div>
                </div>
            </li>
        <li class="list_parent_row<?php echo $get_record['user_id']; ?>" style="display: none">
            <div class="span5 center-block">
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span2 user-display">
                            <?php
                            $image;
                            if ($get_record['media_name'] != "") {
                                $image = ASSETS_PATH_PROFILE_THUMB . $get_record['media_name'];
                            } else {
                                $image = ASSETS_PATH_NO_IMAGE;
                            }
                            ?>
                            <img src="<?php echo $image; ?>" alt="avatar" class="img-circle avatar hidden-phone">
                        </div>
                        <div class="span10">
                            <input type="hidden" value="<?php echo $get_record['user_id']; ?>" name="reviewer_ids[]">
                            <a href="javascript:void(0);" class="name"><?php echo $get_record['firstname'] . ' ' . $get_record['lastname']; ?></a>
                            <span class="subtext"><?php echo $get_record['job_title']; ?><span class="tag">Reviewer</span></span>
                        </div>
                    </div>
                    <div class="span4">
                        <a href="#" class="mail-user"><?php echo $get_record['email']; ?></a>
                    </div>
                </div>
            </div>
        </li>
        <?php } ?>
<?php }else{ ?>
   <span>reviewer not found</span>
<?php } ?>