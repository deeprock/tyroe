<script src="<?= ASSETS_PATH ?>js/fuelux.wizard.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    var assest_path = "<?= ASSETS_PATH ?>";
    var pub_key = "<?= STRIPE_PUBLIC_KEY ?>";
    var card_no = '';
    $(document).ready(function () {

        $('.slider-button').click(function () {
            if ($(this).hasClass("active")) {
                $(this).closest('div').removeClass('success');
                $(this).removeClass('active').html($(this).data("off-text"));
                $(this).css('margin-left','0px');
            } else {
                $(this).addClass('active').html($(this).data("on-text"));
                $(this).closest('div').addClass('success');
                $(this).css('margin-left','29px');
            }
        });

        $("#is_admin_reviewer").click(function(){
            if($(this).hasClass('active')){
                $(this).attr('value','0');
                $(this).removeClass('active');
            }else{
                $(this).attr('value','1');
                $(this).addClass('active');
            }
        });

        $('.order_now').click(function () {
            var pakg_type = $(this).attr('value');
            $('#package_type').val(pakg_type);
            if (pakg_type == 1) {
                $('#pkg_text').html('Lite Package :  ');
                $('#amt').val(90);
            }

            if (pakg_type == 2) {
                $('#pkg_text').html('Awesome Package :  ');
                $('#amt').val(180);
            }

            $('#cc_number').focus();
            $(".amount-details").show();
            $(".amount-details-btn").hide();
            cal_total_amt();
        });

        $('#select_country').change(function () {
            var id = $(this).val();
            if (id == 2) {
                $('#tax').val(10);
                $("#service_tax_warning").fadeIn();
                $('#gst-block').show();
                cal_total_amt();
            } else {
                $('#gst-block').hide();
                $('#tax').val(0);
                cal_total_amt();
                $("#service_tax_warning").fadeOut();
            }
        });

        $(".cc_number").keyup(function () {
            //var cc_len = ($(this).val().length);
            if (true) {
                var type = Stripe.card.cardType($(this).val());
                switch (type) {
                    case "Visa":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/visa.png');
                        $("#cc_type_img").show();
                        break;
                    case "MasterCard":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/mastercard.png');
                        $("#cc_type_img").show();
                        break;
                    case "American Express":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/amex.png');
                        $("#cc_type_img").show();
                        break;
                    case "Discover":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/discover.png');
                        $("#cc_type_img").show();
                        break;
                    case "Diners Club":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/diners.png');
                        $("#cc_type_img").show();
                        break;
                    case "JCB":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/jcb.png');
                        $("#cc_type_img").show();
                        break;
                    case "Maestro":
                        $("#cc_type_img").attr('src', assest_path + 'img/creditcards/maestro.png');
                        $("#cc_type_img").show();
                        break;
                    default:
                        $("#cc_type_img").hide();
                }
            }
        });

        $('#post_job').click(function () {

            if ($('#package_type').val() == 0) {
                alert('Please select package');
                return;
            }

            $('#cc_number').css({border: ''});
            $('#c_month').css({border: ''});
            $('#c_year').css({border: ''});
            $('#security_code').css({border: ''});
            $('#address').css({border: ''});
            $('#select_country').css({border: ''});
            $('#a_city').css({border: ''});
            $('#a_state').css({border: ''});
            $('#a_potscode').css({border: ''});

            var cc_number = $('#cc_number').val();
            var c_month = $('#c_month').val();
            var c_year = $('#c_year').val();
            var security_code = $('#security_code').val();
            var address = $('#address').val();
            var country = $('#select_country').val();
            var city = $('#a_city').val();
            var state = $('#a_state').val();
            var potscode = $('#a_postcode').val();

            if ($.trim(cc_number) == "") {
                $('#cc_number').parent().addClass('error');
                $('#cc_number').focus();
                return false;
            } else {
                $('#cc_number').parent().removeClass('error');
            }

            if ($.trim(c_month) == "") {
                $('#c_month').parent().addClass('error');
                $('#c_month').focus();
                return false;
            } else {
                $('#c_month').parent().removeClass('error');
            }

            if ($.trim(c_year) == "") {
                $('#c_year').parent().addClass('error');
                $('#c_year').focus();
                return false;
            } else {
                $('#c_year').parent().removeClass('error');
            }

            if ($.trim(security_code) == "") {
                $('#security_code').parent().addClass('error');
                $('#security_code').focus();
                return false;
            } else {
                $('#security_code').parent().removeClass('error');
            }

            if ($.trim(address) == "") {
                $('#address').parent().addClass('error');
                $('#address').focus();
                return false;
            } else {
                $('#address').parent().removeClass('error');
            }

            if ($.trim(country) == "") {
                $('#select_country').parent().addClass('error')
                $('#select_country').focus();
                return false;
            } else {
                $('#select_country').parent().removeClass('error')
            }

            if ($.trim(city) == "") {
                $('#a_city').parent().addClass('error');
                $('#a_city').focus();
                return false;
            } else {
                $('#a_city').parent().removeClass('error');
            }

            if ($.trim(state) == "") {
                $('#a_state').parent().addClass('error');
                $('#a_state').focus();
                return false;
            } else {
                $('#a_state').parent().removeClass('error');
            }

            if ($.trim(potscode) == "") {
                $('#a_postcode').parent().addClass('error');
                $('#a_postcode').focus();
                return false;
            } else {
                $('#a_postcode').parent().removeClass('error');
            }


            var ccNum = $('.cc_number').val(), cvcNum = $('.security_code').val(), expMonth = $('.c_month').val(), expYear = $('.c_year').val();
            var error = false;
            if (!Stripe.validateCardNumber(ccNum)) {
                error = true;
                reportError('The credit card number appears to be invalid.');
                return;
            }

            // Validate the CVC:
            if (!Stripe.validateCVC(cvcNum)) {
                error = true;
                reportError('The CVC number appears to be invalid.');
                return;
            }

            // Validate the expiration:
            if (!Stripe.validateExpiry(expMonth, expYear)) {
                error = true;
                reportError('The expiration date appears to be invalid.');
                return;
            }

            Stripe.setPublishableKey(pub_key);
            card_no = ccNum;
            $('.loader-save-holder').show();
            $('#btn-prev').hide();
            $('#post_job').hide();
            $('#btn_apply').hide();
            if (!error) {
                // Get the Stripe token:
                Stripe.createToken({
                    number: ccNum,
                    cvc: cvcNum,
                    exp_month: expMonth,
                    exp_year: expYear

                }, stripeResponseHandler);

            } else {
                $('#btn-prev').show();
                $('#post_job').show();
                $('#btn_apply').show();
            }
        });

        $("#btn_apply").click(function () {
            if ($('#package_type').val() == 0) {
                alert('Please select package');
                return;
            }
            $("#coupon_error_div").hide();
            $("#coupon_success_div").hide();
            $('.loader-save-holder').show();
            if ($.trim($("#coupon_code").val()) == '') {
                return;
            }
            $.ajax({
                type: "POST",
                url: '<?= $vObj->getURL("payment/get_coupons_details") ?>',
                dataType: "json",
                data: {coupon_code: $.trim($("#coupon_code").val())},
                success: function (data) {
                    $('.loader-save-holder').hide();
                    if (data.status === 'ERROR') {
                        $("#coupon_error").html(data.data);
                        $("#coupon_error_div").show();
                        $("#discount").val('0');
                        $("#discount_type").val('0');
                        $("#coupon_id").val('');
                        $(".discount_div").hide();
                        cal_total_amt();

                    } else {
                        var type = parseInt(data.data.type);
                        var discount = parseInt(data.data.discount);
                        if (type == 1) {
                            discount = discount / 100;
                        }
                        $("#discount").val(discount);
                        $("#coupon_id").val(data.data.coupon_id);
                        $("#discount_type").val(type);
                        $("#coupon_success").html("Congratulations! Your coupon has been applied.");
                        $("#coupon_success_div").show();
                        $(".discount_div").show();
                        cal_total_amt();
                    }
                }
            });
        });
    });

    function cal_total_amt() {

        $('#pkg_amount').text(parseFloat($("#amt").val()).toFixed(2));

        var orig_amt = parseInt($("#amt").val());
        var tax = parseInt($("#tax").val());
        var discount = parseInt($("#discount").val());
        var discount_type = parseInt($("#discount_type").val());

        var discount_amt = 0;
        var tax_amt = 0;
        if (discount_type === 1) {
            discount_amt = discount;
            orig_amt = orig_amt - discount;
        } else if (discount_type === 2) {
            discount_amt = (discount * orig_amt) / 100;
            discount = (100 - discount) / 100;
            orig_amt = discount * orig_amt;
        }

        if (tax != 0) {
            tax_amt = ((tax * orig_amt) / 100);
            orig_amt = orig_amt + tax_amt;
        }

        $(".amount").text("$"+parseFloat(orig_amt).toFixed(2));
        $('#discount_amount').text(parseFloat(discount_amt).toFixed(2));
        $('#tax_amount').text(parseFloat(tax_amt).toFixed(2));
        $('#total_amount').text(parseFloat(orig_amt).toFixed(2));
    }

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;
        return true;
    }

    function reportError(msg) {
        $("#modal_error").html(msg);
        $("#open_modal").click();
        $('#btn_pay_now').removeAttr("disabled");
    }

    function stripeResponseHandler(status, response) {
        // Check for an error:
        if (response.error) {
            $('.loader-save-holder').hide();
            reportError(response.error.message);
            $('#btn-prev').show();
            $('#post_job').show();
            $('#btn_apply').show();

        } else { // No errors, submit the form:
            var responce = postjob(response['id']);
        }
    }
</script>
<script type="text/javascript" src="//cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/master/src/js/bootstrap-datetimepicker.js"></script>
<script>
    $(function () {
        $('#country').select2({
            placeholder: 'Country'
        });

        $('#select_country').select2({
            placeholder: 'Country1'
        });

        $("#s2id_select_country").children(".select2-choice").children("span").text('Country');

        $('#city').select2({
            placeholder: 'City'
        });

        $('#job_industry').select2({
            placeholder: 'Specialities'
        });

//        $("#startdate").datepicker({
//            'showCheckbox': false,
//        }).on('changeDate', function(e){
//            $(this).datepicker('hide');
//        });
//
//        $("#enddate").datepicker({
//            'showCheckbox': false,
//            'autoclose' : true
//        }).on('changeDate', function(e){
//            $(this).datepicker('hide');
//        });

        $('#startdate').datetimepicker({
            pickTime: false
        });
        $('#enddate').datetimepicker({
            pickTime: false
        });
        $("#startdate").on("dp.change", function (e) {
            $("#startdate").datetimepicker('hide');
            $('#enddate').data("DateTimePicker").setMinDate(e.date);
        });
        $("#enddate").on("dp.change", function (e) {
            $("#enddate").datetimepicker('hide');
        });



        $("#job_tags").select2({
            tags: [""],
            tokenSeparators: [","],
            placeholder: 'TAGS: Photoshop, Character Design, Python, Maya'
        });

//        $("#invitation_emails").select2({
//            tags: [""],
//            tokenSeparators: [","]
//        });
    });
</script>
<script type="text/javascript">
    $(function () {
        var $wizard = $('#fuelux-wizard'),
                $btnPrev = $('.wizard-actions .btn-prev'),
                $btnNext = $('.wizard-actions .btn-next'),
                $btnPostJob = $("#post_job");


        $wizard.wizard().on('finished', function (e) {
            // wizard complete code
        }).on("changed", function (e) {
            var step = $wizard.wizard("selectedItem");
            if (step.step === 1) {
                $btnPrev.hide();
                $btnNext.show();
            }

            if (step.step > 1 || step.step < 5) {
                $btnNext.show();
                $btnPrev.show();
                $btnPostJob.hide();
            }

            if (step.step === 5) {
                $btnPostJob.show();
                $btnNext.hide();
            }

        });

        $btnNext.click(function () {

            var step = $wizard.wizard("selectedItem");
            if (step.step === 1) {
                var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
                var error = 0;
                var job_title = $('#job_title').val();
                var job_description = $('#job_description').val();
                var job_industry = $('#job_industry').val();
                var job_country = $('#country').val();
                var job_city = $('#city').val();
                var job_startdate = $('#startdate').val();
                var job_enddate = $('#enddate').val();
                var job_type = $('.job_type:checked').val();
                var job_level = $('.job_level:checked').val();
                var job_tags = $('#job_tags').val();
                if ($.trim(job_title) === "")
                {
                    $('#job_title').parents(".field-box").addClass('error');
                    error++;
                } else {
                    $('#job_title').parents(".field-box").removeClass('error');
                }

                if ($.trim(job_description) === "")
                {
                    $('#job_description').parents(".field-box").addClass('error');
                    error++;
                } else {
                    $('#job_description').parents(".field-box").removeClass('error');
                }

                if ($.trim(job_industry) === "")
                {
                    $('.job_industry').parents(".field-box").addClass('error');
                    error++;
                } else {
                    $('.job_industry').parents(".field-box").removeClass('error');
                }

                if ($.trim(job_country) === "")
                {
                    $('#countybox').addClass('error');
                    error++;
                } else {
                    $('#countybox').removeClass('error');
                }

                if ($.trim(job_city) === "")
                {
                    $('#citybox').addClass('error');
                    error++;
                } else {
                    $('#citybox').removeClass('error');
                }

                if ($.trim(job_startdate) === "")
                {
                    $('#startdate').parents(".span6").addClass('error');
                    error++;
                } else {
                    $('#startdate').parents(".span6").removeClass('error');
                }

                if ($.trim(job_enddate) === "")
                {
                    $('#enddate').parents(".span6").addClass('error');
                    error++;
                } else {
                    $('#enddate').parents(".span6").removeClass('error');
                }

                if ($.trim(job_type) === "")
                {
                    $('.job-type').addClass('error');
                    error++;
                } else {
                    $('.job-type').removeClass('error');
                }

                if ($.trim(job_level) === "")
                {
                    $('.job-level').addClass('error');
                    error++;
                } else {
                    $('.job-level').removeClass('error');
                }

                if ($.trim(job_tags) === "")
                {
                    $('#s2id_job_tags').parents(".field-box").addClass('error');
                    error++;
                } else {
                    $('#s2id_job_tags').parents(".field-box").removeClass('error');
                }

                if (error > 0) {
                    $('.modal_error_box').show();
                    $('.modal_error_box').html(error_empty);
                } else {

                    job_title = $('#job_title').val();
                    job_description = $('#job_description').val();
                    job_description = job_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />');
                    job_country = $('#country').val();
                    job_city = $('#city').val();
                    job_startdate = $('#startdate').val();
                    job_enddate = $('#enddate').val();
                    job_tags = $('#job_tags').val();
                    var date1 = new Date(job_startdate);
                    var date2 = new Date(job_enddate);
                    var timeDiff = dateDiff(date1, date2);

                    var studio_name = $("#hidden_username").val();
                    var job_country_name = $("#country option:selected").text();
                    var job_city_name = $("#city option:selected").text();
                    var job_type_name = $('.job_type').attr('data-id');
                    var job_level_name = $('.job_level').attr("data-id");
                    var date_my_diff = "";

                    var content = '<div class="span12 margin-left-default">';
                    content += '<h1>' + studio_name + '</h1>';
                    content += '<h3 class="dt-1">' + job_title + '</h3>';
                    content += '<h5 class="dt-2">' + job_city_name + "," + job_country_name + '</h5><br/>';
                    content += '<p class="dt-2 jqueryClassAutoHeight" id="dt2_description" style="min-height: 20px;width: 58%;">' + job_description + '</p>';
                    content += '<div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">';
                    content += '<div class="span4 text-left st-1 short-info"><span class="opening-date"><i class="icon-calendar"></i>' + job_startdate + '</span></div>';
                    content += '<div class="span4 text-center st-1 short-info"><span class="opening-month"><i class="icon-time"></i> ' + timeDiff + '</span></div>';
                    content += '<div class="span4 text-center st-1 short-info"><span class="opening-time"><i class="icon-legal"></i> ' + job_type_name + '</span></div><div class="clearfix"></div></div>';
                    var jobTags = job_tags.split(',');
                    content += '<p class="dt-label pull-left">';
                    for (var s = 0; s < jobTags.length; s++) {
                        content += '<span class="label">' + jobTags[s] + '  </span>';
                    }
                    content += '</p></div>';
                    $("#job-details").html(content);
                    $('.modal_error_box').hide();
                    $wizard.wizard('next');
                }

            } else {
                $wizard.wizard('next');
            }

        });

        $btnPrev.click(function () {
            $wizard.wizard('previous');
        });

    });

//existing reviewer searching
    $(document).on('keyup', '#search_existing_reviewer', function (a) {
        $('.searching_reviewer_result').css({'display': 'block', 'float': 'none'});
        var reviewers = new Array();
        var i = 0
        $('#studio_reviewers .job-team-tr').each(function (index, value) {
            if (index != 0) {
                reviewers[i] = $.trim($(this).attr('value'));
                i++;
            }
        });

        var searching_value = $("#search_existing_reviewer").val()
        $.ajax({
            type: 'POST',
            data: 'searching_value=' + searching_value + '&search_type=new_job_opening&reviewers=' + reviewers,
            url: "<?= $vObj->getURL("jobopening/search_existing_reviewer"); ?>",
            success: function (html) {
                if ($('#search_existing_reviewer').val() == '') {
                    $('.searching_reviewer_result').html('');
                } else {
                    $('.searching_reviewer_result').html(html);
                }

            }
        });

    });

//select searched reviewer
    $(document).on('click', '.select_reviewer', function () {
        var user_value = $(this).data('value');
        $('.loader-save-holder').show();
        $.ajax({
            type: 'POST',
            data: 'reviewer_id=' + user_value + '&add_reviewer=new_job_opening_reviewer',
            url: "<?= $vObj->getURL("jobopening/add_reviewers_from_job"); ?>",
            success: function (html) {
                $('.search_existing_reviewer').val('');
                $('.searching_reviewer_result').empty();
                if ($('.no_reviewer_record').length > 0) {
                    $('#studio_reviewers').empty();
                    $('#studio_reviewers').html(html);
                } else {
                    var obj = $(html);
                    $('#studio_reviewers').append(obj);
                    obj.focus()
                }
                job_level_role_actions();
                $('.loader-save-holder').hide();
            }
        });
    });

    function getcities(country_id) {
        var data = "country_id=" + country_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?= $vObj->getURL("jobopening/getcities") ?>',
            dataType: "json",
            success: function (response) {
                $('.city').select2('destroy');
                $('#citybox').html(response.dropdown + '<span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Location - City</span>');
                $('#city').select2();
            }

        });
    }

    function job_level_role_actions() {
        $('.delet-job-reviewer').unbind('click').click(function () {
            var obj = $(this);
            obj.parent().parent().remove();
        });
    }

//getting the exact date difference
    function dateDiff(dateFrom, dateTo) {
        var diff = Math.floor(dateTo.getTime() - dateFrom.getTime());
        var day = 1000 * 60 * 60 * 24;
        var days = (diff / day);
        var months = Math.floor(days / 31);
        var years = Math.floor(months / 12);

        days = Math.round(days);
        if (days == 1) {
            var message = days + " Day ";
        } else if (days >= 0 && days != 1) {
            var message = days + " Days ";
        }
        return message
    }

    function postjob(token) {
        $('#stripeToken').attr('value', token);
        $('#c_c_number').attr('value', card_no);
        var reviewers = new Array();
        var invite_emails = new Array();
        var i = 0;
        var j = 0;
        $('#studio_reviewers .job-team-tr').each(function (index, value) {
            if (index != 0) {
                if ($(this).hasClass('email-invite')) {
                    invite_emails[j] = $.trim($(this).attr('value'));
                } else {
                    reviewers[i] = $.trim($(this).attr('value'));
                    i++;
                }

            }
        });

        var team_moderation_val = $('.team_moderation').text();
        if (team_moderation_val == 'OFF') {
            team_moderation_val = 0;
        } else if (team_moderation_val == 'ON') {
            team_moderation_val = 1;
        }
        //temporary fix
        //team_moderation_val = 0;
        
        var job_description = $('#job_description').val();
        job_description = job_description.replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '<br />');

        var job_quantity = $('#no-of-positions').val();

        var data = {
            'job_title': $('#job_title').val(),
            'job_description': job_description,
            'job_country': $('#country').val(),
            'job_city': $("#city option:selected").val(), //take id of city .. don't  use string
            'job_startdate': $('#startdate').val(),
            'job_enddate': $('#enddate').val(),
            'job_type': $('.job_type:checked').val(),
            'job_level': $('.job_level:checked').val(),
            'job_tags': $('#job_tags').val(),
            'job_industry': $('#job_industry').val(),
            'job_reviewers': reviewers,
            'team_moderation': team_moderation_val,
            'job_quantity': job_quantity,
            'invite_emails': invite_emails,
            'is_admin_reviewer':$("#is_admin_reviewer").attr('value')
        };

        $.ajax({
            type: 'POST',
            data: data,
            url: '<?= $vObj->getURL("jobopening/SaveOpening") ?>',
            datatype: 'json',
            async: false,
            success: function (job_id) {
                var new_job_id = $.parseJSON(job_id);
                $('#post_job_id').attr('value', new_job_id);

                var f = $("#payment_form");
                // Insert the token into the form so it gets submitted to the server
//            f.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
//            f.append("<input type='hidden' name='number_c' value='" + card_no + "' />");

                // Submit the form:
                f.get(0).submit();
            }
        });
    }


    $(document).on('click', '#send_invitaion', function () {
        $('#invitation_error').hide();
        var invitation_emails = $.trim($("#invitation_emails").val());
        if (invitation_emails == '') {
            $('#invitation_error').removeClass('alert-success').addClass('alert-error').fadeIn().find('span').text('<?= MESSAGE_SEND_REVIEWER_INVITATION_ERROR ?>');
            setTimeout(function () {
                $('#invitation_error').fadeOut();
            }, 2000);
            return false;
        }

        /**** NEW CODE ***/
        var invalid = false;
        var invite_emails = invitation_emails.split(',');
        $.each(invite_emails, function (index, value) {
            var emailRegexStr = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/igm;
            var isvalid = emailRegexStr.test($.trim(value));
            if (isvalid == false) {
                invalid = true;
            }
        });


        if (invalid) {
            $('#invitation_error').removeClass('alert-success').addClass('alert-error').fadeIn().find('span').text('Please enter valid email addresss.');
            setTimeout(function () {
                $('#invitation_error').fadeOut();
            }, 2000);
            return false;
        }
        /**** NEW CODE ***/

        $.ajax({
            type: 'POST',
            data: {
                emails: invite_emails
            },
            url: '<?= $vObj->getURL("jobopening/check_email_present") ?>',
            datatype: 'json',
            async: false,
            success: function (result) {
                var data = JSON.parse(result);
                if (data.success == true) {
                    $("#invitation_emails").val('');
                    if (data.err_emails.length > 0) {
                        var err_msg = data.err_emails.join(', ') + " already present in system, cannot send invitation";
                        $('#invitation_error').removeClass('alert-success').addClass('alert-error').fadeIn().find('span').text(err_msg);
                        setTimeout(function () {
                            $('#invitation_error').fadeOut();
                        }, 5000);
                    }


                    $.each(data.invite_emails, function (index, value) {
                        var html = '<tr class="table_rows job-team-tr email-invite" value="' + value + '">' +
                                '<td><p class="rt-name">Name not available</p>' +
                                '<span class="rt-title">no title</span></td>' +
                                '<td class="role-check-td"></td>' +
                                '<td class="role-check-td text-center"><a class="reviewer_check_option active" value="2"><i class="fa fa-check-circle"></i></a></td>' +
                                '<td style="text-align: right;padding-right: 13px"><a href="mailto:' + value + '">' + value + '</a></td>' +
                                '<td class="status_td"><a href="javascript:void(0)"><label class="label label-info rt-pending">Pending</label></a></td>' +
                                '<td><a href="javascript:void(0)" class="delet-job-reviewer" id="delet_job_reviewer_0_0_0"><i class="fa fa-times"></i></a></td></tr>';
                        var obj = $(html);
                        if ($('.no_reviewer_record').length > 0) {
                            $('#studio_reviewers').empty();
                            $('#studio_reviewers').append(obj);
                        } else {
                            $('#studio_reviewers').append(obj);
                        }
                    });
                    job_level_role_actions();
                }
            }
        });
    });

</script>
<div style="display:none" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="content">
    <div class="job-create-header">

    </div>
    <div class="job_creation_container">
        <div class="job_create_details">
            <div class="job_create_holder global-popup ">
                <div class="step-nav job_create_header">
                    <h4 class="modal-title">
                        <div class="wizard row" id="fuelux-wizard">
                            <ul class="wizard-steps">
                                <li class="active" data-target="#step1">
                                    <span class="step">1</span>
                                    <span class="title">Create</span>
                                </li>
                                <li data-target="#step2" class="">
                                    <span class="step">2</span>
                                    <span class="title">Team</span>
                                </li>
                                <li data-target="#step3" class="">
                                    <span class="step">3</span>
                                    <span class="title">Manage</span>
                                </li>
                                <li data-target="#step4" class="">
                                    <span class="step">4</span>
                                    <span class="title">Preview</span>
                                </li>
                                <li data-target="#step5" class="">
                                    <span class="step">5</span>
                                    <span class="title">Publish</span>
                                </li>
                            </ul>
                        </div>
                    </h4>
                </div>
                <div class="hor-sep"></div>
                <div class="job_create_body">
                    <div class="step-content">
                        <div id="step1" class="step-pane active">
                            <div class="row form-wrapper job-create-step-1">
                                <div class="center-block">
                                    <div class="modal_error_box alert alert-danger"></div>
                                    <form id="createjobform">
                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <input type="text" class="form-control span12" placeholder="Job Title" name="job_title" id="job_title" onkeypress="return checkspecialchars(event)">
                                                <div class="specialcharserror" style="display:none;">Job title can only contain 'A-Z','a-z','-','_' and '.'</div>
                                                <input type="hidden" name="hidden_username" id="hidden_username" value="<?= $company_detail['company_name'] ?>">
                                                <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter Job Title</span>
                                            </div>
                                        </div>
                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <textarea name="" class="form-control span12" id="job_description" cols="30" rows="6" placeholder="Description"></textarea>
                                                <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter Job Description</span>
                                            </div>
                                        </div>
                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <select name="job_industry" placeholder="Specialities" id="job_industry" class="job_industry">
                                                        <option></option>
                                                        <?php
                                                        foreach ($job_industries as $industry) {
                                                            ?>
                                                            <option value="<?= $industry['industry_id'] ?>"><?= $industry['industry_name'] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter Job Specialities</span>
                                        </div>
                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <div class="span6" id="countybox">
                                                    <select name="country" id="country" onchange="getcities(this.value);" placeholder="Country" class="country">
                                                        <option></option>
                                                        <?php
                                                        foreach ($get_countries as $country) {
                                                            ?>
                                                            <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Location - Country</span>
                                                </div>
                                                <div class="span6" id="citybox">
                                                    <select  name="city" id="city" class="city">
                                                        <option></option>
                                                    </select>
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Location - City</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <input type="text" name="startdate" id="startdate" class="span12" placeholder="Start-Date">
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Start-Date</span>
                                                </div>
                                                <div class="span6">
                                                    <input type="text" name="enddate" id="enddate" class="span12" placeholder="End-Date">
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job End-Date</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" field-box lined job-type sr-sec">
                                            <div class="row-fluid">
                                                <?php
                                                foreach ($get_job_type as $type) {
                                                    ?>
                                                    <div class="span4">
                                                        <div class="job-type-holder">
                                                            <input type="radio" class="job_type" value="<?= $type['job_type_id'] ?>" data-id="<?= $type['job_type'] ?>" name="job_type">
                                                            <span><?= $type['job_type'] ?></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Type</span>
                                        </div>

                                        <div class="field-box job-level sr-sec-2">
                                            <div class="row-fluid">
                                                <?php
                                                foreach ($get_job_level as $level) {
                                                    ?>
                                                    <div class="span4">
                                                        <div class="job-level-holder">
                                                            <input type="radio" class="job_level" value="<?= $level['job_level_id'] ?>" data-id="<?= $level['level_title'] ?>" name="job_level">
                                                            <span><?= $level['level_title'] ?></span>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                            <span class="alert-msg"><i class="icon-remove-sign"></i> Please select Job Level</span>
                                        </div>
                                        <br clear="all">

                                        <div class="field-box">
                                            <div class="row-fluid">
                                                <input type="text" name="job_tags" data-placeholder="TAGS: Photoshop, Character Design, Python, Maya" id="job_tags" class="span12 form-control" style="padding-left:20px;height: 45px">
                                            </div>
                                            <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter at least one Job Tag</span>
                                        </div>
                                        <div class="clear"></div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div id="step2" class="step-pane">
                            <div class="row form-wrapper">
                                <div class="job_review_team_container">
                                    <div id="main-stats" style="margin-top: 0;border: none;">
                                        <div class="stats-row">
                                            <div class="span5 stat-large text-center box-show">
                                                <div class="no-images" style="margin-bottom: 0;">
                                                    <i class="fa fa-plus-circle"></i>
                                                    <div class="clearfix"></div>
                                                    <h5 class="rt-text-15">Add team member</h5>
                                                    <p>Select existing team members to help you discover the best talent for this job opening.</p>
                                                    <div class="field-box">
                                                        <input type="text"  class="search_existing_reviewer radius-search-input" id="search_existing_reviewer" placeholder="Search team members">
                                                        <ul class="searching_reviewer_result ext-rvw-list-1"></ul>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="vertical-sep"></div>
                                            <div class="span5 stat-large text-center box-show">
                                                <div class="no-images" style="margin-bottom: 0">
                                                    <i class="fa fa-envelope-o"></i>
                                                    <div class="clearfix"></div>
                                                    <h5 class="rt-text-15">Invite new team member</h5>
                                                    <p>Looking for someone new to help out? Send them an email invitation and get them involved today.</p>
                                                </div>
                                                <div class="field-box invite-box">
                                                    <div class="alert alert-success" id="invitation_error" style="display: none; width: 65% !important;margin-left: 5%;">
                                                        <span></span>
                                                    </div>
                                                    <div class="invite-form">
                                                        <input type="text" class="invite-form-input" name="invitation" placeholder="Enter email addresses..." id="invitation_emails">
                                                        <button class="btn-success invite-form-btn" id="send_invitaion">Invite</button>
                                                    </div>
                                                </div>
                                                <div class="clear"></div>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                    </div><div class="clear"></div>
                                    <div class="show-grid margin-adjust">
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="table-responsive create-job-custom-table">
                                        <table class="table table-hover tr-thead">
                                            <thead>
                                                <tr>
                                                    <th><div class="position-relative"><a href="javascript:void(0)">Name</a></div></th>
                                            <th><div class="position-relative" style="text-align: center"><span class="line"></span><a href="javascript:void(0)">Job Administrator</a></div></th>
                                            <th><div class="position-relative" style="text-align: center"><span class="line"></span><a href="javascript:void(0)">Job Reviewer</a></div></th>
                                            <th><div class="position-relative" style="text-align: right;padding-right: 8px;"><span class="line"></span><a href="javascript:void(0)">Email</a></div></th>
                                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)">Status</a></div></th>
                                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)"></a></div></th>
                                            </tr>
                                            </thead>
                                            <tbody id="studio_reviewers" class="team-tr">
                                                <tr class="table_rows job-team-tr" value="<?= $user_detail['user_id'] ?>">
                                                    <td><p class="rt-name"><?= stripslashes($user_detail['firstname']) . ' ' . stripslashes($user_detail['lastname']) ?></p>
                                                        <span class="rt-title"><?= stripslashes($user_detail['job_title']); ?></span></td>
                                                    <td class="role-check-td" style="text-align: center"><a class="reviewer_check_option active" value="1"><i class="fa fa-check-circle"></i></a></td>
                                                    <td class="role-check-td" style="text-align: center">
                                                        <a class="reviewer_check_option" value="0" id="is_admin_reviewer" style="height: 24px;margin: 0px;width: 100%;display: inline-block;">
                                                            <i class="fa fa-check-circle"></i>
                                                        </a>
                                                    </td>
                                                    <td style="text-align: right;padding-right: 13px"><a href="mailto:<?= $user_detail['email'] ?>"><?= $user_detail['email'] ?></a></td>
                                                    <td class="status_td">
                                                        <a href="javascript:void(0)" class="reviewer_status_sl"><label class="label label-success" style="text-align: center">Active</label></a>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step3" class="step-pane">
                            <div class="row-fluid form-wrapper">
                                <div class="span10 center-block">
                                    <div class="team-mod-setting-container">
                                        <div class="field-box" id="rt_description">
                                            <div class="span10">
                                                <h6>Team Moderation</h6>
                                                <p>Allow administrators to moderate all feedback shared with artists. If turned off, team members can share their helpful feedback directly to artists on behalf of your company.</p>
                                            </div>
                                            <div class="span2">
                                                <div class="slider-frame">
                                                    <span data-on-text="ON" data-off-text="OFF" class="slider-button team_moderation">OFF</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="team-mod-setting-container last" style="border-bottom: none">
                                        <div class="field-box" id="rt_description">
                                            <div class="span10">
                                                <h6>Number of Positions.</h6>
                                                <p>Looking for more that one person? Simply increase the number of available positions.</p>
                                            </div>
                                            <div class="span2">
                                                <input type="number" class="no-of-positions" id="no-of-positions" value="1" min="1" max="99" >
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="step4" class="step-pane">
                            <?php
//                        if($logo_url != ''){
//                            $logo_url = UPLOAD_PATH.'company_logo/'.$job_detail[$ind]['company_logo'];
//
//                        }else{
//                            $logo_url = LIVE_SITE_URL.'assets/img/default-avatar.png';
//                        }
                            ?>
                            <div class="preview_job">
                                <div class="left-col">
                                    <div class="company-logo-holder">
                                        <?php
                                        if ($company_detail['logo_image'] != '') {

                                            $w = '';
                                            $h = '';
                                            $margin_top = '';
                                            $margin_left = '';

                                            $logo_url = LIVE_SITE_URL . 'assets/uploads/company_logo/' . $company_detail['logo_image'];
                                            list($width, $height, $type, $attr) = getimagesize($logo_url);
                                            $maxWidth = intval(200);
                                            $maxHeight = intval(200);
                                            $final_height = '';
                                            $final_width = '';

                                            if (width > height) {
                                                $final_width = $maxWidth;
                                                $final_height = ($height / $width) * $final_width;
                                                if ($final_height < $maxHeight) {
                                                    $margin_top = (intval($maxHeight) - intval($final_height)) / 2;
                                                }
                                            }

                                            if ($height > $width) {
                                                $final_height = $maxHeight;
                                                $final_width = $final_height * ($width / $height);

                                                if ($final_width < $maxWidth) {
                                                    $margin_left = (intval($maxWidth) - intval($final_width)) / 2;
                                                }

                                                if ($margin_left <= 0) {
                                                    $margin_left = 0;
                                                }
                                            }

                                            if ($width < $maxWidth && $height < $maxHeight) {
                                                $final_height = $height;
                                                $final_width = $width;
                                                $margin_left = (intval(200) - intval($final_width)) / 2;
                                                $margin_top = (intval(200) - intval($final_height)) / 2;
                                                if ($margin_left <= 0) {
                                                    $margin_left = 0;
                                                }

                                                if ($margin_top <= 0) {
                                                    $margin_top = 0;
                                                }
                                            }

                                            $w = $final_width;
                                            $h = $final_height;
                                            ?>
                                            <img class="company-logo" src="<?= $logo_url ?>" style="height:<?= $h ?>px;width:<?= $w ?>px;margin-left:<?= $margin_left ?>px;margin-top:<?= $margin_top ?>px"/>
                                            <?php
                                        } else {
                                            ?>
                                            <img class="company-logo" src="<?= LIVE_SITE_URL . 'assets/img/default-avatar.png' ?>"/>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="right-col job-details" id="job-details">
                                    <div class="span12 margin-left-default">
                                        <h1>tommoody</h1>
                                        <h3 class="dt-1">vcxvxv</h3>
                                        <h5 class="dt-2">Adelaide,Australia</h5><br/>
                                        <p class="dt-2 jqueryClassAutoHeight" id="dt2_description" style="min-height: 20px;width: 58%;">
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                            xcvcxvcxv xcvcxvcxv xcvcxvcxv
                                        </p>
                                        <div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">
                                            <div class="text-left st-1 short-info">
                                                <span class="opening-date">
                                                    <i class="icon-calendar"></i>12/23/2014
                                                </span>
                                            </div>
                                            <div class="text-center st-1 short-info">
                                                <span class="opening-month"><i class="icon-time"></i> 4 Days </span>
                                            </div>
                                            <div class="text-center st-1 short-info">
                                                <span class="opening-time"><i class="icon-legal"></i> Casual</span>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <p class="dt-label pull-left"><span class="label">xvcxvcxv  </span><span class="label">xvxcv  </span></p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="step5" class="step-pane">
                            <div class="container-fluid payment_block">
                                <div id="in_pricing">
                                    <div class="head">
                                        <h3>Post a Job for 30 days</h3>
                                        <h6>Find better talent, faster.</h6>
                                    </div>
                                    <div class="row charts_wrapp">
                                        <!-- Plan Box -->
                                        <div class="plan-box pro lite-pkg">
                                            <div class="plan">
                                                <div class="wrapper">
                                                    <h3>Lite</h3>
                                                    <div class="price">
                                                        <span class="dollar">$</span>
                                                        <span class="qty">90</span>
                                                        <span class="month">/ 30 day access</span>
                                                    </div>
                                                    <div class="features">
                                                        <p>
                                                            <strong>Promoted</strong>
                                                            job post
                                                        </p>
                                                        <p>
                                                            <strong>Applicant</strong>
                                                            tracking system
                                                        </p>
                                                        <p>
                                                            <strong>&nbsp;</strong>
                                                            &nbsp;
                                                        </p>
                                                    </div>
                                                    <a class="order order_now" href="javascript:void(0)" id="order_lite_pkg" value="1">ORDER NOW</a>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- Plan Box -->
                                        <div class="plan-box pro awasome-pkg">
                                            <div class="plan">
                                                <div class="wrapper">
                                                    <img class="ribbon" src="<?= ASSETS_PATH ?>img/badge.png">
                                                    <h3>Awesome</h3>
                                                    <div class="price">
                                                        <span class="dollar">$</span>
                                                        <span class="qty">180</span>
                                                        <span class="month">/ 30 day access</span>
                                                    </div>
                                                    <div class="features">
                                                        <p>
                                                            <strong style="color: #1189D9;">Unlimited Talent Search</strong>
                                                        </p>
                                                        <p>
                                                            <strong>Promoted</strong>
                                                            job post
                                                        </p>
                                                        <p>
                                                            <strong>Applicant</strong>
                                                            tracking system
                                                        </p>
                                                    </div>
                                                    <a class="order order_now" href="javascript:void(0)" id="order_awasome_pkg" value="2">ORDER NOW</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                                <div class="clear"></div>
                                <div id="faq">
                                    <div class="section_header">
                                        <h3>Frequently asked questions</h3>
                                    </div>
                                    <div>
                                        <div class="span12 block">
                                            <div class="box span5">
                                                <p class="title">Why use Tyroe Search to find talent?</p>
                                                <p class="answ">
                                                    Glad you asked! Seriously though, if you urgently need to find local rockstar talent, this is perfect for you and your entire team.  Our database is full of amazing junior artists that have already been vetted and sorted for you.</p>
                                            </div>
                                            <div class="box span5">
                                                <p class="title">Can I message talent when I find them?</p>
                                                <p class="answ">
                                                    Absolutely. This is the beauty of the Tyroe Talent Search. Once unlocked, you will have access to all our members pixel-perfect profiles and be able to contact them directly. What are you waiting for, start a conversation.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </div>

                                        <div class="span12 block">
                                            <div class="box span5">
                                                <p class="title">What will I be able to view?</p>
                                                <p class="answ">
                                                    Absolutely everything about the Tyroe. Unlocking the search allows you to view their complete profiles, up to date availability, and full contact details.</p>
                                            </div>
                                            <div class="box span5">
                                                <p class="title">What happens when access finishes?</p>
                                                <p class="answ">
                                                    All your favourited searches and talent will be waiting for you when you reactivate. The best part is that all messaging is handled through your own mail system so you can keep conversations alive and kicking.</p>
                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <form action="<?= LIVE_SITE_URL ?>payment/job_access" method="post" id="payment_form">
                                    <div class="payment-section" id="payment_section">
                                        <div class="span6 block">
                                            <div class="header">
                                                <h3>Credit Card Details</h3>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="box form-block">
                                                <div class="form-row">
                                                    <input class="input-control cc_number" type="text" id="cc_number" onkeypress="return isNumberKey(event);" placeholder="Credit Card Number">
                                                    <img src="<?= ASSETS_PATH ?>img/creditcards/visa.png" id="cc_type_img"/>
                                                    <div class="alert-msg"><i class="icon-remove-sign"></i> Please enter Credit Card Number</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-row">
                                                    <input class="input-control c_month" type="text" id="c_month" onkeypress="return isNumberKey(event);" placeholder="MM" autocomplete="off">
                                                    <span class="sep">&nbsp;/&nbsp;</span>
                                                    <input class="input-control c_year" type="text" id="c_year" onkeypress="return isNumberKey(event);" placeholder="YYYY" autocomplete="off">
                                                    <div class="alert-msg"><i class="icon-remove-sign"></i> Please enter Credit Card Expiry Date</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-row">
                                                    <input class="input-control security_code" type="password" id="security_code" autocomplete="off" onkeypress="return isNumberKey(event);" placeholder="Security Code">
                                                    <div class="alert-msg"><i class="icon-remove-sign"></i> Please enter Credit Card Security Code</div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="vertical-sep"></div>
                                        <div class="span6 block">
                                            <div class="header">
                                                <h3>Billing Address</h3>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="box form-block">
                                                <div class="form-row">
                                                    <input class="input-control address" type="text" name="address" id="address" placeholder="Address">
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter Street Address</span>
                                                </div>
                                                <div class="form-row">
                                                    <select class="select-country" id="select_country" name="select_country" placeholder="Country">
                                                        <option value="">Country</option>
                                                        <?php
                                                        foreach ($get_countries as $country) {
                                                            ?>
                                                            <option value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                    <span class="alert-msg"><i class="icon-remove-sign"></i> Please enter Billing Address Country</span>
                                                </div>
                                                <div class="form-row">
                                                    <input class="input-control a_city" type="text" name="a_city" id="a_city" placeholder="City">
                                                    <input class="input-control a_state" type="text" name="a_state" id="a_state" placeholder="State">
                                                    <input class="input-control a_postcode" type="text" name="a_postcode" id="a_postcode" placeholder="Postcode">
                                                    <div class="alert-msg"><i class="icon-remove-sign"></i> Please enter Billing Address Location</div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="alert-message alert alert-warning" id="service_tax_warning" style="display: none;padding: 10px;"><i class="icon-warning-sign"></i>GST (10%) applicable for studios located in Australia</div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="hor-sep"></div>
                                        <div class="clearfix"></div>
                                        <div class="amount-section">
                                            <div class="span6">
                                                <div class="form-block">
                                                    <div class="form-row">
                                                        <input class="input-control coupon_code" type="text" name="coupon_code" id="coupon_code" placeholder="Coupon Code">
                                                        <a href="javascript:void(0)" class="btn btn-apply" id="btn_apply">APPLY</a>
                                                        <div class="clearfix"></div>
                                                        <div class="alert alert-error" id="coupon_error_div" style="display: none;">
                                                            <i class="icon-remove-sign"></i>
                                                            <span id="coupon_error">10% service tax applicable for Studios located in Australia</span>
                                                        </div>
                                                        <div class="alert alert-success" id="coupon_success_div" style="display: none;">
                                                            <i class="icon-ok-sign"></i>
                                                            <span id="coupon_success">GST (10%) applicable for studios located in Australia</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="span6">
                                                <div class="amount-details alert-info">
                                                    <div class="label-row" id="pkg_text"><a id="select_package" href="#in_pricing" style="font-weight: normal">Select Package</a> : </div><div class="label-amount">$<span id="pkg_amount">0</span></div>
                                                    <div class="clearfix"></div>
                                                    <div class="label-row discount_div">Coupon Discount :  </div><div class="label-amount discount_div" >$<span id="discount_amount">0</span></div>
                                                    <div class="clearfix"></div>
                                                    <div id="gst-block" style="display: none;">
                                                        <div class="label-row">GST(10%) :  </div><div class="label-amount" >$<span id="tax_amount">0</span></div>
                                                        <div class="clearfix"></div>
                                                    </div>

                                                    <div class="hor-sep"></div>
                                                    <div class="label-row total-label">Total :  </div><div class="label-amount total-amount">$<span id="total_amount">0</span></div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="amount-details-btn alert-info">
                                                    <a id="select_package" href="#in_pricing">Select Package</a>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <div class="hidden">
                                        <input type="text" name="amt" id="amt" value="0"/>
                                        <input type="text" name="tax" id="tax" value="0"/>
                                        <input type="text" name="discount" id="discount" value="0"/>
                                        <input type="text" name="discount_type" id="discount_type" value="0"/>
                                        <input type="text" name="coupon_id" id="coupon_id" value=""/>
                                        <input type="text" name="package_type" id="package_type" value="0"/>
                                        <input type="text" name="job_id" id="post_job_id" value="0"/>
                                        <input type='hidden' name='stripeToken' id="stripeToken" value='0' />
                                        <input type='hidden' name='number_c' id="c_c_number" value='0' />
                                    </div>
                                </form>


                                <div class="hidden">
                                    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal_payment" id="open_modal">
                                    </button>
                                </div>

                                <div class="modal fade" id="myModal_payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;background-color: white">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Error</h4>
                                            </div>
                                            <div class="modal-body" id="modal_error">

                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Ok</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="hor-sep"></div>
                <div class="job_create_footer">
                    <div class="wizard-actions">
                        <button data-last="Finish" class="btn-flat white btn-prev margin-left-2" type="button" id="btn-prev">
                            Back
                        </button>
                        <button data-last="Finish" class="btn-flat primary btn-next margin-left-2" type="button" style="display: inline-block;" id="btn-next">
                            Next
                        </button>
                        <button class="btn-flat success btn-pay-now inactive" id="post_job">Post Job</button>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>

