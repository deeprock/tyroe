<div class="content">
    <div class="container-fluid">
        <div class="row" id="moderation_title" style="<?php if(count($pending_reviews) == 0) { ?>display: none<?php } ?>">
            <div class="span12">
                <div class="main-note">
                    Advanced Moderation
                </div>
                <div class="tag-line">
                    The Feedback listed here are pending for approval. Please advice which Feedback should be sent to<br/>
                    the Candidate, and which should remain for internal reference only.
                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                <div class="row-fluid" id="all_modertaion_done" style="<?php if(count($pending_reviews) > 0) { ?>display: none<?php } ?>;margin-top:30px">
                    <div class="span6 offset3">
                      <div class="alert alert-success text-center" style="margin-bottom:0px">
                        <i class="icon-check-sign"></i>
                        <span class="text-center text-success">
                            Nice work! You have moderated all feedback from your team.
                        </span>
                    </div>  
                    </div>
                </div>
                <div class="table-responsive" style="<?php if(count($pending_reviews) == 0) { ?>display: none<?php } ?>" id="moderation_table">
                    <table class="table table-hover tr-thead">
                        <thead>
                            <tr>
                                <th class="span2"><div class="position-relative">Job/Reviewer</div></th>
                                <th class="span2"><div class="position-relative"><span class="line"></span>Score</div></th>
                                <th class="span4"><div class="position-relative"><span class="line"></span>Comment</div></th>
                                <th class="span2"><div class="position-relative"><span class="line"></span>Action</div></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($pending_reviews as $pending_review) { ?>
                            <tr style="height: 120px">
                                    <td>
                                        <div class="job-title">
                                            <a href="<?= $vObj->getURL("openings/" . strtolower($vObj->cleanString($pending_review['job_title']) . '-' . $vObj->generate_job_number($pending_review['job_id']))) ?>"><?= $pending_review['job_title'] ?></a>
                                        </div>
                                        <div class="reviewer-name">
                                            Reviewer: <?= ucfirst($pending_review['firstname']) . ' ' . ucfirst($pending_review['lastname']) ?>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="row-fluid">
                                            <div class="span12">
                                                <div class="span3 text-left">
                                                    <?php
                                                    $average = round((intval($pending_review['impression']) + intval($pending_review['technical']) + intval($pending_review['creative'])) / 3);
                                                    $title = 'Thirsty';
                                                    if ($average > 49 && $average < 80) {
                                                        $title = 'Pressed';
                                                    } else if ($average > 79) {
                                                        $title = 'Slayer';
                                                    }
                                                    ?>
                                                    <img src="<?= LIVE_SITE_URL . 'assets/img/default-avatar.png' ?>" class="img-circle rate-badge-logo" 
                                                         data-toggle="tooltip" data-placement="bottom" title="<?= $title ?>">
                                                </div>
                                                <div class="span3">
                                                    <div class="score-place"
                                                         data-toggle="tooltip" data-placement="bottom" title="Impression"><?= $pending_review['impression'] ?></div>
                                                </div>
                                                <div class="span3">
                                                    <div class="score-place"
                                                         data-toggle="tooltip" data-placement="bottom" title="Technical"><?= $pending_review['technical'] ?></div>
                                                </div>
                                                <div class="span3">
                                                    <div class="score-place" data-toggle="tooltip" data-placement="bottom" title="Creative"><?= $pending_review['creative'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="jQueryClassReadmoreParagraphFunc" style="overflow: hidden"><span>
                                        <?php
                                        if (trim($pending_review['anonymous_feedback_message']) == '')
                                            echo 'No comment provided';
                                        else
                                            echo nl2br(trim($pending_review['anonymous_feedback_message']));
                                        ?>
                                        </span>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="<?= base_url() . 'moderation' ?>/update_review/<?= $pending_review['review_id'] ?>/2" class="btn btn-default btn-internal-only update-moderation">
                                            <i class="fa fa-thumbs-down"></i>&nbsp;&nbsp;Internal Use Only</a>
                                        <a href="<?= base_url() . 'moderation' ?>/update_review/<?= $pending_review['review_id'] ?>/1" class="btn btn-success btn-candidate-show update-moderation">
                                            <i class="fa fa-thumbs-up"></i>&nbsp;&nbsp;Send to Candidate</a>
                                    </td>
                                </tr>
                                <?php }
                                ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    
    $(document).ready(function(){
        $('.jQueryClassReadmoreParagraphFunc').each(function(){
            var getHeight = $(this).height();
            if(getHeight > 60){
                $(this).after('<a href="javascript:void(0)" class="jQueryClassReadmoreFunc">Read More</a>');
                $(this).css({"height":'60px'});
            }
        });
    });
    
    
    $(document).on('click','.jQueryClassReadmoreFunc',function(){
        var getHeight = $(this).parent().children('.jQueryClassReadmoreParagraphFunc').children('span').height();
        $(this).parent().children('.jQueryClassReadmoreParagraphFunc').animate({"height":getHeight+"px"});
        $(this).removeClass('jQueryClassReadmoreFunc');
        $(this).addClass('jQueryClassReadlessFunc');
        $(this).text('Read Less');
    });
    
    $(document).on('click','.jQueryClassReadlessFunc',function(){
        var getHeight = $(this).parent().children('.jQueryClassReadmoreParagraphFunc').height();
        if(getHeight > 60){
            $(this).parent().children('.jQueryClassReadmoreParagraphFunc').animate({"height":"60px"});
            $(this).removeClass('jQueryClassReadlessFunc');
            $(this).addClass('jQueryClassReadmoreFunc');
            $(this).text('Read More');
        }
    });
    
    $(document).on('click', '.update-moderation', function (e) {
        e.preventDefault();
        var obj = $(this);
        $.ajax({
            url: $(this).attr('href'),
            type: "POST",
            dataType: "json",
            success: function (data) {
                if (data.status == 'SUCCESS') {
//                    $(".notification-box-message").html(data.message);
//                    $(".notification-box").show(100);
//                    setTimeout(function () {
//                        $(".notification-box").hide();
//                    }, 5000);
                    obj.parent('td').parent('tr').remove();
                    if($("#moderation_table").children('table').children('tbody').children().length == 0){
                        $("#pending_review_alert").hide();
                        $("#moderation_table").hide();
                        $("#all_modertaion_done").show();
                        $("#moderation_title").hide();
                    }
                }
            }
        });
    });
</script>
