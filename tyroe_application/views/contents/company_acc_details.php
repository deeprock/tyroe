<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//if ($this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '4')
//{
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#save_company_details').click(function(){
            $('#description').css({border: ''});
            $('#industry_select').css({border: ''});
            $('#website_input').css({border: ''});
          
            var description = $('#description').val();
            var industry_select = $('#industry_select').val();
            var website_input = $('#website_input').val();
            
            if ($.trim(description) == "") {
                $('#description').css({
                    border: '1px solid red'
                });
                $('#description').focus();
                return false;
            }

            if ($.trim(industry_select) == "") {
                $('#industry_select').css({
                    border: '1px solid red'
                });
                $('#industry_select').focus();
                return false;
            }

            if ($.trim(website_input) == "") {
                $('#website_input').css({
                    border: '1px solid red'
                });
                $('#website_input').focus();
                return false;
            }
            
            $('#company_details_form').submit(); 
        });
    });
</script>


<script type="text/javascript">
    jQuery(function () {
        // Switch slide buttons
        $('.slider-button').click(function () {
            if ($(this).hasClass("on")) {
                $(this).closest('div').removeClass('success');
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
                $(this).closest('div').addClass('success');
            }
        });
    });
</script>

<?php
if(isset($company_detail_saved)){
?>
<script type="text/javascript">
$(document).ready(function(){
    $(".notification-box-message").html("company details saved successfully");
    $(".notification-box").show(100);
    setTimeout(function () {
        $(".notification-box").hide();
    }, 5000);
});
</script>
<?php
}
?>



<!-- main container -->
<div class="content">

<div class="container-fluid">
<div id="pad-wrapper">
<div><?php if ($profile_updated != "") {
    echo $profile_updated;
}?></div>
<div class="grid-wrapper">
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<?php

$this->load->view('left_coloumn');


?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar personal-info">
<div class="row-fluid form-wrapper">


    <div class="span12">
        <div class="container-fluid padding-adjusted">
            <div class="span4 default-header">
                <h4>Company Details</h4>
            </div>
            <div class="clearfix"></div>
            <div class="msg-box"></div>
            <form class="new_user_form inline-input new-c-magic acnt-dtl c-magic" enctype="multipart/form-data" name="profile_form" method="post" 
                  action="<?=$vObj->getURL('account/save_company_account_details')?>" id="company_details_form">
                
                <div class="field-box">
                    <div class="span3"><label>Name</label></div>
                    <div class="span9"><strong><?php echo $company_detail['company_name']?></strong></div>
                </div>
                
                <div class="field-box">
                    <div class="span3"><label>Studio Locations</label></div>
                    <div class="span9">
                        <ul class="company-locations">
                            <?php
                            foreach($company_locations as $location){
                            ?>
                            <li class="company-locations-placeholder"><?=$location['city'];?>, <?=$location['country'];?></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                </div>
                
                <div class="field-box">
                    <div class="span3"><label>Logo</label></div>
                    <div class="span9">
                        <div class="company-logo-box" id="company_logo_box">
                            <?php if($company_detail['logo_image'] != '') {
                            $w = '';
                            $h = '';
                            $margin_top = '';
                            $margin_left = '';
                           
                                $logo_url = UPLOAD_PATH.'company_logo/'.$company_detail['logo_image'];
                                list($width, $height, $type, $attr) = getimagesize($logo_url);           
                                $maxWidth = intval(150);
                                $maxHeight = intval(150);
                                $final_height = '';
                                $final_width = '';

                                if(width > height){
                                    $final_width = $maxWidth;
                                    $final_height = ($height/$width)*$final_width;
                                    if($final_height < $maxHeight){
                                        $margin_top = (intval($maxHeight) - intval($final_height))/2;
                                    }

                                }


                                if($height > $width){
                                    $final_height = $maxHeight;
                                    $final_width = $final_height*($width/$height);

                                    if($final_width < $maxWidth){
                                        $margin_left = (intval($maxWidth) - intval($final_width))/2;
                                    }

                                    if($margin_left <= 0){
                                        $margin_left = 0;
                                    }

                                }

                                if($width < $maxWidth && $height < $maxHeight){
                                    $final_height = $height;
                                    $final_width = $width;
                                    $margin_left = (intval(150) - intval($final_width))/2;
                                    $margin_top = (intval(150) - intval($final_height))/2;
                                    if($margin_left <= 0){
                                        $margin_left = 0;
                                    }

                                    if($margin_top <= 0){
                                        $margin_top = 0;
                                    }

                                }


                                $w = $final_width;
                                $h = $final_height;    
                            ?>
                            <img id="uploadPreview" class="uploadPreview" src="<?php echo ASSETS_PATH . 'uploads/company_logo/'.$company_detail['logo_image']?>" style="width:<?=$w?>px;height:<?=$h?>px;margin-top:<?=$margin_top?>px;margin-left:<?=$margin_left?>px">
                            <?php } else {?>
                            <img id="uploadPreview" class="uploadPreview default" src="<?php echo ASSETS_PATH . 'img/default-avatar.png'?>" >
                            <?php }?>
                        </div>    
                        <input type="file" id="logo_image" name="image" onchange="preview_logo(this);" accept="image/*"/>
                    </div>
                </div>

                <div class="field-box">
                    <div class="span3"><label>Description</label></div>
                    <div class="span9">
                        <textarea class="description-textarea" name="description" id="description" placeholder="Company Description"><?php echo $company_detail['company_description']?></textarea>
                    </div>
                </div>
                
                <div class="field-box">
                    <div class="span3"><label>Industry</label></div>
                    <div class="span9">
                        <select class="industry-select" name="industry" id="industry_select">
                            <option value="">Select Industry</option>
                            <?php
                            foreach($industries as $industry){
                                if($company_detail['industry_id'] == $industry['id']){
                                ?>
                                <option value="<?=$industry['id']?>" selected="selected"><?=$industry['name']?></option>
                                <?php
                                }else{
                                ?>
                                <option value="<?=$industry['id']?>"><?=$industry['name']?></option>
                                <?php    
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
                
                <div class="field-box">
                    <div class="span3"><label>Website</label></div>
                    <div class="span9">
                        <input type="text" value="<?php echo $company_detail['website']?>" class="website-input" id="website_input" name="website" placeholder="Company Website"/>
                    </div>
                </div>
            </form>
            <div class="clearfix"></div>
            <?php
            if(isset($record_update_message)){
            ?>
            <div class="alert alert-info">
                <i class="icon-info-sign"></i>
                <?php echo $record_update_message?>
            </div>
            <?php
            }
            ?>
            <div class="clearfix"></div>
            <div class="span4 pull-right actn-btns">
                    <img id="save_loader" src="<?=ASSETS_PATH?>img/loader.gif" style="width: 20px; display: none;">
                    <input type="button" name="submit" class="btn-flat success pull-right"
                           value="<?= LABEL_SAVE ?>" id="save_company_details">
                    <input type="reset" class="btn-flat white pull-right"
                           value="<?= LABEL_CANCEL_BUTTON ?>" >
            </div>
            <div class="clearfix"></div>
            
            
        </div>
    </div>
</div>


</div>

<!-- END RIGHT COLUMN -->
<div>


</div>

</div>
</div>
</div>
</div>


<?php
//}
?>
<script type="text/javascript">
    
    function preview_logo(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                document.getElementById('uploadPreview').src=e.target.result;
                var img = new Image();
                img.src = e.target.result;
                img.onload = function () {
                    change_aspect_ratio(parseInt(this.width), parseInt(this.height))
                };
            }
        }
    }
    
//    function preview_logo() {
//        var reader = new FileReader();
//        var img = new Image();
//        reader.readAsDataURL(document.getElementById("logo_image").files[0]);
//        reader.onload = function (e) {
//            img.src = e.target.result;
//            img.onload = function () {
//                $("uploadPreview").attr('src','');
//                $("uploadPreview").attr('src',e.target.result);
//                change_aspect_ratio(parseInt(this.width), parseInt(this.height))
//            };
//        };
//    };
    
    function change_aspect_ratio(width, height, imgsrc) {
        var maxWidth = 150; // Max width for the image
        var maxHeight = 150;    // Max height for the image
        var final_height = 0;
        var final_width = 0;
        
        $('.uploadPreview').show().css({
            'width':'',
            'height':'',
            'margin-left':'',
            'margin-top':''
        });

        if(width > height){
            final_width = maxWidth;
            final_height = (height/width)*final_width;
            var margin_top = 0;
            if(final_height < maxHeight){
                margin_top = (parseInt(maxHeight) - parseInt(final_height))/2
            }
            
            if(margin_left <= 0){
                margin_left = 0;
            }
            
            
            $('.uploadPreview').show().css({
                'width':final_width+'px',
                'height':final_height+'px',
                'margin-top':margin_top+'px'
            });
        }

        // Check if current height is larger than max
        if(height > width){
            final_height = maxHeight;
            final_width = final_height*(width/height);
            var margin_left = 0;
            
            if(final_width < maxWidth){
                margin_left = (parseInt(maxWidth) - parseInt(final_width))/2
            }
            
            if(margin_left <= 0){
                margin_left = 0;
            }
            
            $('.uploadPreview').show().css({
                'width':final_width+'px',
                'height':final_height+'px',
                'margin-left':margin_left+'px'
            });
        }
        
        if(width < maxWidth && height < maxHeight){
            final_height = height;
            final_width = width;
            var margin_left = (parseInt(150) - parseInt(final_width))/2;
            var margin_top = (parseInt(150) - parseInt(final_height))/2;
            if(margin_left <= 0){
                margin_left = 0;
            }
            if(margin_top <= 0){
                margin_top = 0;
            }
            
            $('.uploadPreview').show().css({
                'width':final_width+'px',
                'height':final_height+'px',
                'margin-left':margin_left+'px',
                'margin-top':margin_top+'px'
            });
        }
        
        
    }
    
</script>
<!--Dashboard Artist End-->