<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    jQuery(function () {
        // Switch slide buttons
        $('.slider-button').click(function () {
            if ($(this).hasClass("active")) {
                $(this).closest('div').removeClass('success');
                $(this).removeClass('active').html($(this).data("off-text"));
            } else {
                $(this).addClass('active').html($(this).data("on-text"));
                $(this).closest('div').addClass('success');
            }
            var option_value = $(this).html();
            var option_type = $(this).attr('type-value');
            var data = "option_value=" + option_value + "&option_type=" + option_type;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("account/save_notification")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {

                    }
                    else {

                    }
                }
            });
        });
    });
    function AddNotification() {

        var notification_process = $("#notification_process").val();
        var notification_type = $("#notification_type").val();

        if (notification_process != "" && notification_type != "") {
            var data = "notification_process=" + notification_process + "&notification_type=" + notification_type;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("account/save_notification")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".box-alert").addClass("alert alert-success");
                        $(".box-alert").html("<i class='icon-ok-sign'></i>" + data.success_message);
                        $(".box-alert").show(100);
                        setTimeout(function () {
                            $(".alert-success").removeClass().addClass("box-alert");
                            $(".icon-ok-sign").removeClass();
                            $(".box-alert").hide();
                        }, 2000);
                    }
                    else {
                        $(".box-alert").addClass("alert alert-danger");
                        $(".box-alert").html("<i class='icon-remove-sign'></i>" + data.success_message);
                        $(".box-alert").show(100);
                        setTimeout(function () {
                            $(".alert-danger").removeClass().addClass("box-alert");
                            $(".icon-remove-sign").removeClass();
                            $(".box-alert").hide();
                        }, 2000);
                    }
                },
                failure: function (errMsg) {
                }
            });

        } else {
            $(".box-alert").addClass("alert alert-danger");
            $(".box-alert").html("<i class='icon-remove-sign'></i>" + "Fields Required");
            $(".box-alert").show(100);
            setTimeout(function () {
                $(".alert-danger").removeClass().addClass("box-alert");
                $(".icon-remove-sign").removeClass();
                $(".box-alert").hide();
            }, 2000);
        }
    }
</script>
<div class="content">
<div class="container-fluid padding-adjusted">
<div id="pad-wrapper">
<div class="grid-wrapper">
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<?php
$this->load->view('left_coloumn');
?>

<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar personal-info">
<div class="row-fluid form-wrapper">
<div class="span12">
<div class="container-fluid">
<div class="span4 default-header">
    <h4 class="header-adjusted"><?= LABEL_SCHEDULE_NOTIFICATION ?></h4>
</div>
<div class="clearfix"></div>
<div class="box-alert" style="display: none">
    <i id="icon_alert" class="icon-ok-sign"></i>
</div>
<form class="new_user_form inline-input new-c-magic c-magic" _lpchecked="1"
      enctype="multipart/form-data" name="schedule_ntf_form" method="post">


<div class="span12 c-magic">
    <div class="span10 offset1">
        <ul class="linked-accounts-list email-notifications">
<!--Getting all notifications-->
            <?php
            foreach ($selected_notification as $key => $notification) {
                $status;

                if ($notification['option_id'] == '15') {
                    $notification_heading = LABEL_NOTIFICATION_TEXT_JOB;
                    $notification_sub_text = LABEL_NOTIFICATION_SUB_TEXT_JOB;
                } else if ($notification['option_id'] == '16') {
                    $notification_heading = LABEL_NOTIFICATION_TEXT_FEEDBACK;
                    $notification_sub_text = LABEL_NOTIFICATION_SUB_TEXT_FEEDBACK;
                } else if ($notification['option_id'] == '17') {
                    $notification_heading = LABEL_NOTIFICATION_TEXT_PROFILE_VIEW;
                    $notification_sub_text = LABEL_NOTIFICATION_SUB_TEXT_PROFILE_VIEW;
                } else if ($notification['option_id'] == '18') {
                    $notification_heading = LABEL_NOTIFICATION_TEXT_STAFF_NEWSLETTER;
                    $notification_sub_text = LABEL_NOTIFICATION_SUB_TEXT_STAFF_NEWSLETTER;
                } else if ($notification['option_id'] == '19') {
                    $notification_heading = LABEL_NOTIFICATION_TEXT_TYROE_NEWSLETTER;
                    $notification_sub_text = LABEL_NOTIFICATION_SUB_TEXT_TYROE_NEWSLETTER;
                }
                $class = "";
                $class_span = "";
                if ($notification['status_sl'] == "0") {
                    $status = "OFF";
                } else {
                    $status = "ON";
                    $class = "on success";
                    $class_span = "on active";
                }
                ?>
                <li>
                    <div class="span12">
                        <div class="span10 pull-left social-acnt-title">
                            <h4><?= $notification_heading ?></h4>

                            <p><?= $notification_sub_text ?></p>
                        </div>
                        <div class="span2 pull-left social-acnt-link">
                            <div class="slider-frame <?= $class ?>">
                                   <span
                                       class="slider-button <?= $class_span ?>"
                                       type-value='<?= $notification['schedule_id'] ?>'
                                       data-off-text="OFF"
                                       data-on-text="ON"><?= $status ?></span>
                            </div>
                        </div>
                    </div>
                </li>
            <?php
            }
            ?>

        </ul>
    </div>
</div>


</form>
</div>
</div>
</div>


</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>
</div>
</div>