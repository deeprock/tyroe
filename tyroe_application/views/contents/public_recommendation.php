<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script>
   jQuery(document).ready(function(){
       /*$(".text_counter").keypress(function(){
           var textarea_counter = parseInt($('.text_counter').val().length);
           var show_counter = 250-textarea_counter;
           $(".counter").text(show_counter);
           if(show_counter < 0 ){
               $(".counter").addClass("red-counter");
               $(".counter").removeClass("light-red-counter");
               $(".submit_disabled").css("display","block");
           } else if(show_counter < 20 ){
               $(".counter").addClass("light-red-counter");
               $(".counter").removeClass("red-counter");
               $(".submit_disabled").css("display","none");
           } else {
               $(".counter").removeClass("red-counter");
               $(".counter").removeClass("light-red-counter");
               $(".submit_disabled").css("display","none");
           }
       });*/

       $(".text_counter").keyup(function(){
           var textarea_counter = parseInt($('.text_counter').val().length);
           var show_counter = 250-textarea_counter;
           $(".counter").text(show_counter);
           if(show_counter < 0 ){
               $(".counter").addClass("red-counter");
               $(".counter").removeClass("light-red-counter");
               $(".submit_disabled").css("display","block");
           } else if(show_counter < 20 ){
               $(".counter").addClass("light-red-counter");
               $(".counter").removeClass("red-counter");
               $(".submit_disabled").css("display","none");
           } else {
               $(".counter").removeClass("red-counter");
               $(".counter").removeClass("light-red-counter");
               $(".submit_disabled").css("display","none");
           }
       });
   });
</script>

<style type="text/css">
    .navbar.navbar-inverse{display: none;}
</style>

<!-- main container -->

<?php if ($profile_updated != "") {
    echo $profile_updated;
}
?>


<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<style type="text/css">
    #single-team .team-name h2, #single-team .team-name h3, #single-team .team-name p {
        float: none;
    }
    .content{
        margin-left: 0px !important;   }
</style>
<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img class="logo" src="<?= ASSETS_PATH ?>img/logo-dark.png">
    </a>
    <div class="span4 box">
        <div class="content-wrap">
            <form name="recom_form" action="<?=$vObj->getURL("profile/save_recommendation");?>" method="post" onsubmit="return recommend_form();">
                <?php
                if($recommend_status=="-1" && $status_sl!="-1")
                {
                    echo "Thanks for submitting the recommendation to ".$get_user['firstname'] . " " . $get_user['lastname'];
                } else {
                ?>
                <h6><?= "RECOMMENDATION FOR ".$get_user['firstname'] . " " . $get_user['lastname'] ?></h6>
                <span id="error_msg"></span>
                <input class="span12" type="text" name="recom_name" id="recom_name" placeholder="Name" value="">
                <input class="span12" type="text" name="recom_company" id="recom_company" placeholder="Company" value="">
                <input class="span12" type="text" name="recom_dept" id="recom_dept" placeholder="Job Title" value="">
                <textarea class="span12 text_counter" name="recom" id="recom" placeholder="Please add a short recommendation here" ></textarea>
                <div style="display: none" id="big_loader" ><img src="<?=ASSETS_PATH?>img/big_loader.gif" width="50px" /></div>
                <div class="login_btn text-right">
                    <span class="counter text-right">250</span>
                    <input type="submit" class="btn-glow primary" value="<?= LABEL_SUBMIT ?>">
                    <span class="submit_disabled"></span>
                </div>
                <input type="hidden" name="recommend_id" value="<?=$recommend_id?>">
                <?php } ?>
            </form>
        </div>
    </div>

    <div class="span4 no-account">
        <p><?= LABEL_NEED_ACCOUNT_FOR_COMPANY ?></p>
        <a href="<?= $vObj->getURL('register/index/company') ?>"><?= LABEL_REGISTERATION ?></a>
    </div>
</div>
