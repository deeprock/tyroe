<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!-- main container -->
<div class="content">

<div class="container-fluid">

<!-- upper main stats -->

<div id="main-stats">
    <div class="row-fluid stats-row" id="stats-padding">
        <div class="span12">
            <div class="span3 stat first">
                <div class="data">
                    <span class="number">2457</span>
                    Total
                </div>
                <span class="date">Members</span>
            </div>
            <div class="span3 stat">
                <div class="data">
                    <span class="number">104</span>
                    New
                </div>
                <span class="date">Members this week</span>
            </div>
            <div class="span3 stat">
                <div class="data">
                    <span class="number">4</span>
                    Jobs
                </div>
                <span class="date">Currently Open</span>
            </div>
            <div class="span3 stat last">
                <div class="data">
                    <span class="number red-text">3</span>
                    Reviews
                </div>
                <span class="date">Outstanding</span>
            </div>
        </div>

    </div>
</div>

<!-- end upper main stats -->
<div id="pad-wrapper">
<div class="grid-wrapper">
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<div class="span4">
    <div class="row-fluid">
        <div class="row-fluid">
            <div class="span8 offset2">
                <?php
                if (isset($system_modules['navigation_hotspot'])) {
                    foreach ($system_modules['navigation_hotspot'] as $k => $v) {
                        ?>
                        <a class="btn-flat success span12" href="<?= $vObj->getURL($v['link']) ?>"
                           id="home-button"><?= $v['module_title'] ?></a>
                    <?php
                    }
                }
                ?>
            </div>
        </div>

        <!-- morris bar & donut charts -->
        <div class="row-fluid home-chart">

            <div class="span12 chart">
                <div id="reviewer-donut" style="height: 180px;">
                </div>
                <h5 style="text-align: center;">Reviewer Status</h5>
            </div>
            <div class="span12 chart">
                <div id="candidate-donut" style="height: 180px;">
                </div>
                <h5 style="text-align: center;">Candidate Status</h5>
            </div>
        </div>

    </div>

</div>
<!-- END LEFT COLUMN -->


<!-- START RIGHT COLUMN -->
<div class="span8 sidebar" id="opening-overview">
<div class="right-column" style="position:relative;">


<!-- START ACTIVITY STREAM -->
<div class="row-fluid header">
    <div class="span12">
        <h4>Latest Activity Stream</h4>
        <span class="pull-right button-center"><a class="btn-flat gray" id="comment">View All</a></span>
    </div>
</div>

<div class="row-fluid">
    <div class="span12">
        <div class="navbar navbar-inverse">
            <ul style="margin: 0 10px 10px 10px; list-style:none;">
                <li class="stream-container">
                    <div class="stream-dialog">

                        <div class="notes">

                            <a href="#" class="item">
                                <i class="icon-star"></i> Andrew McDonald added a new review
                                <span class="time"><i class="icon-time"></i> 13 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-star"></i> Sabrina Scalfari added a new review
                                <span class="time"><i class="icon-time"></i> 18 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-envelope-alt"></i> New message from Alejandra
                                <span class="time"><i class="icon-time"></i> 28 min.</span>

                                <div class="opening-comment">I definitely feel we should get John in for an interview.
                                    What does everyone else think?
                                </div>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-move"></i> Moved to shortlist folder
                                <span class="time"><i class="icon-time"></i> 1 day.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-star"></i> Andrew McDonald added a new review
                                <span class="time"><i class="icon-time"></i> 13 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-star"></i> Sabrina Scalfari added a new review
                                <span class="time"><i class="icon-time"></i> 18 min.</span>
                            </a>
                            <a href="#" class="item">
                                <i class="icon-envelope-alt"></i> New message from Alejandra
                                <span class="time"><i class="icon-time"></i> 28 min.</span>

                                <div class="opening-comment">I definitely feel we should get John in for an interview.
                                    What does everyone else think?
                                </div>
                            </a>
                        </div>

                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>


<!-- START JOB OPENINGS -->
<div class="row-fluid header sections">
    <div class="span12">
        <h4>Current Job Openings</h4>
        <span class="pull-right button-center"><a class="btn-flat gray" id="comment">View All</a></span>
    </div>
</div>
<!-- START OPENING DETAILS -->
<div class="row-fluid">
    <div class="span12 group" id="top">

        <!-- TITLE -->
        <div class="row-fluid">
            <div class="span8">
                <h4>
                    <a href="__opening-overview.html">Texture Artist - VFX</a>
                </h4>
            </div>
            <div class="span4">
                <ul class="actions pull-right">
                    <li><a href="__opening-overview.html"><i class="table-edit"></i></a></li>
                    <li><a href="#"><i class="table-settings"></i></a></li>
                    <li class="last"><i class="table-delete"></i></li>
                </ul>
            </div>
        </div>

        <!-- INFO -->
        <div class="row-fluid" id="opening-stats">
            <div class="span4">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="label label-success">Reviews Completed</div>
                        <p><strong>Location:</strong> Sydney, Australia</p>

                        <p><strong>Date:</strong> 1st Jan - 31st May</p>

                        <p><strong>Level:</strong> Mid Level</p>

                        <p><strong>Quantity:</strong> 1</p>
                    </div>
                </div>
            </div>
            <div class="span8">
                <div class="row-fluid">
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-review-team.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number">4</span>
                                </div>
                                REVIEWERS
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-candidates.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number">6</span>
                                </div>
                                CANDIDATES
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-shortlisted.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number green-text">2</span>
                                </div>
                                SHORTLISTED
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-hidden.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number red-text">1</span>
                                </div>
                                HIDDEN
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END OPENING DETAILS -->
<!-- START OPENING DETAILS -->
<div class="row-fluid">
    <div class="span12 group">

        <!-- TITLE -->
        <div class="row-fluid">
            <div class="span8">
                <h4>
                    <a href="__opening-overview.html">Lighting TD - Iron Man 4 </a>
                </h4>
            </div>
            <div class="span4">
                <ul class="actions pull-right">
                    <li><a href="__opening-overview.html"><i class="table-edit"></i></a></li>
                    <li><a href="#"><i class="table-settings"></i></a></li>
                    <li class="last"><i class="table-delete"></i></li>
                </ul>
            </div>
        </div>

        <!-- INFO -->
        <div class="row-fluid" id="opening-stats">
            <div class="span4">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="label label-success">Reviews Completed</div>
                        <p><strong>Location:</strong> Sydney, Australia</p>

                        <p><strong>Date:</strong> 1st Jan - 31st May</p>

                        <p><strong>Level:</strong> Mid Level</p>

                        <p><strong>Quantity:</strong> 1</p>
                    </div>
                </div>
            </div>
            <div class="span8">
                <div class="row-fluid">
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-review-team.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number">4</span>
                                </div>
                                REVIEWERS
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-candidates.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number">6</span>
                                </div>
                                CANDIDATES
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-shortlisted.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number green-text">2</span>
                                </div>
                                SHORTLISTED
                            </a>
                        </div>
                    </div>
                    <div class="span3">
                        <div style="text-align:center;">
                            <a href="__opening-hidden.html" class="btn-flat white">
                                <div class="data">
                                    <span class="number red-text">1</span>
                                </div>
                                HIDDEN
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- END OPENING DETAILS -->


<!-- START FEATURED ARTISTS -->
<div class="row-fluid header sections">
    <div class="span12">
        <h4>New Featured Artists</h4>
        <span class="pull-right button-center"><a class="btn-flat gray" id="comment">View All</a></span>
    </div>
</div>
<!-- START CANDIDATE ROW -->
<div class="row-fluid">
    <div class="span12 group" id="top">
        <div class="row-fluid" id="user_info">

            <div class="span8 push-down-10" id="search-align">
                <div class="span12">
                    <div class="span3" id="avatar">
                        <img src="img/avatar_01.png" class="img-circle avatar_search_large">


                    </div>

                    <!-- user information -->
                    <div class="span8">
                        <a href="user-profile.html" class="name" style="margin:0px;">Stephen McFly</a>
                        <span class="location" style="display: block;">Sydney, Australia</span>
                        <span class="tags" style="display: block;">Maya, Mari, Animation, Photography, Illustration, Python, Texture Painting, Modeling</span>
                        <span class="label label-info">Available Now</span>
                        <span class="label label-success">Featured</span>


                    </div>
                </div>
            </div>

            <div class="span4 phone-offset-search">
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_5.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_6.jpg">

                        <div class="img-grid-score">
                            <div class="thumb-score">
                                <span>7.2</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_7.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_8.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- START CANDIDATE ROW -->
<div class="row-fluid">
    <div class="span12 group">
        <div class="row-fluid" id="user_info">

            <div class="span8 push-down-10" id="search-align">
                <div class="span12">
                    <div class="span3" id="avatar">
                        <img src="img/avatar_02.png" class="img-circle avatar_search_large">


                    </div>

                    <!-- user information -->
                    <div class="span8">
                        <a href="user-profile.html" class="name" style="margin:0px;">Stephen McFly</a>
                        <span class="location" style="display: block;">Sydney, Australia</span>
                        <span class="tags" style="display: block;">Maya, Mari, Animation, Photography, Illustration, Python, Texture Painting, Modeling</span>
                        <span class="label label-info">Available Now</span>
                        <span class="label label-success">Featured</span>


                    </div>
                </div>
            </div>

            <div class="span4 phone-offset-search">
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_1.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_2.jpg">

                        <div class="img-grid-score">
                            <div class="thumb-score">
                                <span>4.8</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_3.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_4.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- START CANDIDATE ROW -->
<div class="row-fluid">
    <div class="span12 group">
        <div class="row-fluid" id="user_info">

            <div class="span8 push-down-10" id="search-align">
                <div class="span12">
                    <div class="span3" id="avatar">
                        <img src="img/avatar_03.png" class="img-circle avatar_search_large">


                    </div>

                    <!-- user information -->
                    <div class="span8">
                        <a href="user-profile.html" class="name" style="margin:0px;">Stephen McFly</a>
                        <span class="location" style="display: block;">Sydney, Australia</span>
                        <span class="tags" style="display: block;">Maya, Mari, Animation, Photography, Illustration, Python, Texture Painting, Modeling</span>
                        <span class="label label-info">Available Now</span>
                        <span class="label label-success">Featured</span>


                    </div>
                </div>
            </div>

            <div class="span4 phone-offset-search">
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_9.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_10.jpg">
                    </div>
                </div>
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_1.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_2.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<!-- START CANDIDATE ROW -->
<div class="row-fluid">
    <div class="span12 group">
        <div class="row-fluid" id="user_info">

            <div class="span8 push-down-10" id="search-align">
                <div class="span12">
                    <div class="span3" id="avatar">
                        <img src="img/avatar_04.png" class="img-circle avatar_search_large">


                    </div>

                    <!-- user information -->
                    <div class="span8">
                        <a href="user-profile.html" class="name" style="margin:0px;">Stephen McFly</a>
                        <span class="location" style="display: block;">Sydney, Australia</span>
                        <span class="tags" style="display: block;">Maya, Mari, Animation, Photography, Illustration, Python, Texture Painting, Modeling</span>
                        <span class="label label-info">Available Now</span>
                        <span class="label label-success">Featured</span>


                    </div>
                </div>
            </div>

            <div class="span4 phone-offset-search">
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_3.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_4.jpg">
                    </div>
                </div>
                <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_5.jpg">
                    </div>
                    <div class="span6 img-grid">
                        <img src="portfolio/art_thumb_6.jpg">
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


</div>

</div>
</div>

<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>
</div>
<!-- end main container -->


<!--Dashboard Artist End-->

