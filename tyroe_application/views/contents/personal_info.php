<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
//if ($this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '4')
//{
?>
<!-- main container -->
<div class="content">

    <div class="container-fluid">
        <div id="pad-wrapper">
            <div></div>
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php

                    $this->load->view('left_coloumn');


                    ?>


                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">


                            <div class="span12">
                                <div class="container-fluid">


                                    <div class="span4 default-header">
                                        <h4>Personal Inforamtion</h4>
                                    </div>

                                    <div class="span12 field-box">
                                        <label>Avatar:</label>

                                        <div class="profile-image span6">
                                            <img src="<?php echo $get_user['image']; ?>" class="avatar img-circle">


                                        </div>
                                    </div>
                                    <?php


                                    ?>

                                    <div class="span12 field-box">
                                        <label><?= LABEL_PUBLIC_PROFILE ?>:</label>
                                        <label class="span9"><?php echo LIVE_SITE_URL."profile".$get_user['profile_url']; ?></label>
                                    </div>
                                    <div class="field-box">
                                        <label><?= LABEL_EMAIL ?>:</label>
                                        <label class="span9"><?php echo $get_user['email']; ?></label>
                                    </div>
                                    <div class="field-box">
                                        <label><?= LABEL_USER_NAME ?>:</label>
                                        <label class="span9"><?php echo $get_user['username']; ?></label>
                                    </div>
                                    <div class="field-box">
                                        <label><?= LABEL_COUNTRY ?>:</label>
                                        <label class="span9"><?php echo $get_user['country']; ?></label>
                                    </div>

                                    <div class="span12 field-box">
                                        <label><?= LABEL_STATE ?>:</label>
                                        <label class="span9"><?php echo $get_user['state']; ?></label>
                                    </div>
                                    <div class="span12 field-box">
                                        <label><?= LABEL_CITY ?>:</label>
                                        <label class="span9"><?php echo $get_user['city']; ?></label>
                                    </div>
                                    <div class="span12 field-box">
                                        <label><?= LABEL_CELLPHONE ?>:</label>
                                        <label class="span9"><?php echo $get_user['cellphone']; ?></label>
                                    </div>

                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>

    <?php
    //}
    ?>
    <!--Dashboard Artist End-->