<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.resume-clone-anchor',function(e){
            var clone=$(this).closest('.resume_info').find('.clone_resume:last').clone();
                clone.find('.remove-resume-clone').show();
                clone.find('.seprator-anchor').show();
                clone.find('input:text').each(function() {
                        $(this).val('');
                });
                clone.find('textarea').html('');
                clone.find('input:checkbox').prop('checked', false);
                $(clone).insertAfter($(this).closest('.resume_info').find('.clone_resume:last'))

        })

        $(document).on('click','.remove-resume-clone',function(e){
                    var clone=$(this).closest('.clone_resume').remove();
                })
    })
</script>
<div class="clone_resume">
<div class="field-box">
                                            <label><?= LABEL_JOB_TITLE ?>:</label>
                                            <input class="span9" type="text"
                                                   name="job_title[]"
                                                   value="">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_COMPANY_NAME ?>:</label>
                                            <input class="span9" type="text"
                                                   name="company_name[]"
                                                   value="">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_CURRENT_JOB ?>:</label>
                                            <input type="checkbox" class="user" name="current_job[]" onclick="CurrentJob();">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_START_DATE ?>:</label>
                                            <input class="span9" type="text" name="job_start[]"
                                                   value="">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_END_DATE ?>:</label>
                                            <input class="span9 inline-input" type="text"
                                                   name="job_end[]"
                                                   value="">
                                        </div>

                                        <div class="field-box" style="margin-bottom: 0px;">
                                            <label><?= LABEL_JOB_DESCRIPTION ?>:</label>
                                            <textarea class="span9 inline-input"
                                                   name="job_description[]" ></textarea>

                                        </div>
                                        <div class="field-box actions" style="margin-top:0px;margin-bottom: 13px;">
                                            <a href="javascript:void(0)" class="resume-clone-anchor">Add More</a>
                                            <span class="seprator-anchor" style="display: none;">|</span>
                                            <a href="javascript:void(0)" class="remove-resume-clone" style="display: none">Remove</a>
                                        </div>
                                        <div style="border-bottom: 1px solid #EDF2F7; float: left;height: 1px;width: 100%;margin-bottom: 33px;">&nbsp;</div>
</div>
