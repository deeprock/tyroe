<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('click','.portfolio-clone-anchor',function(e){
            var clone=$(this).closest('.portfolio_info').find('.clone_portfolio:last').clone();
                clone.find('.remove-portfolio-clone').show();
                clone.find('.seprator-anchor').show();
                clone.find('input:text').each(function() {
                        $(this).val('');
                });
                clone.find('textarea').html('');
                clone.find('input:checkbox').prop('checked', false);
                clone.find('.uploadify-queue').remove();
                clone.find('#file_upload'+(index-1)).replaceWith('<input type="file" id="file_upload'+index+'" name="file_upload">')
                $(clone).insertAfter($(this).closest('.portfolio_info').find('.clone_portfolio:last'))
                uploadify_intialize()

        })
        $(document).on('click','.remove-portfolio-clone',function(e){
                    var clone=$(this).closest('.clone_portfolio').remove();
                })

        uploadify_intialize()
    })
    var index=0;
    function uploadify_intialize(){
    <?php $timestamp = time();?>
            var image_name = '';
                        var cnt = 0;
                        var portfolio_path = '<?=ASSETS_PATH?>uploads/portfolio1/';

            $("#file_upload"+index).uploadify({

                            'formData': {
                                'timestamp': '<?php echo $timestamp;?>',
                                'token': '<?php echo md5('unique_salt' . $timestamp);?>'
                            },
                            'fileTypeExts': '*.jpg; *.JPG; *.jpeg; *.JPEG; *.png; *.PNG; *.gif; *.GIF; *.bmp; *.BMP',
                            'fileSizeLimit': '2MB',
                            'swf': '<?=ASSETS_PATH?>' + 'js/uplodify_js/uploadify.swf',
                            'uploader': '<?=LIVE_SITE_URL?>' + 'portfolio/upload_image',
                            'onUploadSuccess': function (file, data, response, fileObj) {
                                cnt++;

                                data = JSON.parse(data);
                                var image_ext = data[0].file_ext;
                                var image_path = data[0].full_path;
                                var img_name = data[0].file_name;
                                if (cnt <= '4') {
                            //        $('#thumbnail').append('<img src=\"' + portfolio_path + img_name + '\" width="95" style="float:left;margin-left:1em;"/>');
                                }
                              //  $('.imageNames').append(img_name + ',');
                                //$('.imagePaths').append(image_path + ',');
                            }

                        });
        index++;
    }
</script>

<div class="clone_portfolio">
<div class="field-box">
                                            <label><?= LABEL_MEDIA_TITLE ?>:</label>
                                            <input class="span9" type="text"
                                                   name="media_title[]"
                                                   value="">
                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_IMAGE_DESCRIPTION ?>:</label>
                                            <textarea class="span9 inline-input"
                                                      name="image_description[]" ></textarea>

                                        </div>
                                        <div class="field-box">
                                            <label><?= LABEL_IMAGE ?>:</label>
                                            <input type="file" id="file_upload0" name="file_upload">
                                            <div class="thumbnail"></div>
                                            <input type="hidden" class="imagenames" name="imagenames[]">
                                        </div>
                                        <div class="field-box actions" style="margin-top:0px;margin-bottom: 13px;">
                                            <a href="javascript:void(0)" class="portfolio-clone-anchor">Add More</a>
                                            <span class="seprator-anchor" style="display: none;">|</span>
                                            <a href="javascript:void(0)" class="remove-portfolio-clone" style="display: none">Remove</a>
                                        </div>
                                        <div style="border-bottom: 1px solid #EDF2F7; float: left;height: 1px;width: 100%;margin-bottom: 33px;">&nbsp;</div>
</div>