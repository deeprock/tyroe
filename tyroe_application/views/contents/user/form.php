<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<style>
    .resume_info, .portfolio_info, .subscription_info {
        display: none;
    }
</style>
<script type="text/javascript">
    function isValidEmail(emailText) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailText);
    }

    function submit() {
        var email = $("#email").val();
        var password = $("#password").val();
        var confirmpass = $("#confirmpass").val();
        var user_name = $("#username").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var user_country = $("#user_country").val();
       // var user_state = $("#user_state").val();
        var user_city = $("#user_city").val();
        var cellphone = $("#user_cellphone_code").val() + '-' + $("#cellphone").val();
        var flag = $("#flag").val();
        var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
        if (email == '') {
            $('#email').css({ border: '1px solid red'});
            $('#email').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else if (!isValidEmail(email)) {
            $.post( email);
            $('.modal_error_box').show();
            $('.modal_error_box').html('<strong>Error!</strong> Enter a valid email address');
            return false;
        } else {
            $('#email').removeAttr("style")
        }
        if (password == '') {
            $('#password').css({ border: '1px solid red'});
            $('#password').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#password').removeAttr("style")
        }
        if (confirmpass == '') {
            $('#confirmpass').css({ border: '1px solid red'});
            $('#confirmpass').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        }else{
            $('#confirmpass').removeAttr("style")
        }
        if (user_name == '') {
            $('#username').css({ border: '1px solid red'});
            $('#username').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#username').removeAttr("style")
        }
        if (firstname == '') {
            $('#firstname').css({ border: '1px solid red'});
            $('#firstname').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#firstname').removeAttr("style")
        }
        if (lastname == '') {
            $('#lastname').css({ border: '1px solid red'});
            $('#lastname').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#lastname').removeAttr("style")
        }
        if (user_country == '') {
            $('#user_country').css({ border: '1px solid red'});
            $('#user_country').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#user_country').removeAttr("style")
        }
        /*if (user_state == '') {
            $('#user_state').css({ border: '1px solid red'});
            $('#user_state').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#state').removeAttr("style")
        }*/
        if (user_city == '') {
            $('#user_city').css({ border: '1px solid red'});
            $('#user_city').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#city').removeAttr("style")
        }
        if (password == confirmpass) {
                       $('#confirmpass').removeAttr("style")
        } else {
               $('#confirmpass').css({ border: '1px solid red'});
               $('#confirmpass').focus();
               $('.modal_error_box').show();
               $('.modal_error_box').html('<strong>Error!</strong> Passwords do not matched');
               return false;
        }
        $('.loader-save-holder').show();
        var formdata = jQuery("#admin_tyroe_form").serialize();
        $.ajax({
           data:formdata,
           url:'<?=$vObj->getURL("user/save")?>',
           type : "POST",
           dataType:"json",
           success : function(data){
               $('.loader-save-holder').hide();
               if(data.duplicate_email)
               {
                   $('.modal_error_box').show();
                   $('.modal_error_box').html('<strong>Error!</strong> Email already exist, try a different email address');
               }else if(data.duplicate_username)
               {
                   $('.modal_error_box').show();
                   $('.modal_error_box').html('<strong>Error!</strong> Username already Registered, try a different username');
               }
               else
               {
                   $(".abc").addClass("alert alert-success");
                   $(".alert-success").html('<i class="icon-ok-sign"></i>'+data.success_message);
                   $('.modal-backdrop, .modal-backdrop.fade.in').css('opacity', '0');
                   $('.modal.fade.in').hide();
                   $('.alert-success').fadeIn(500);
                   $('.alert-success').delay(2000).fadeOut(1500);
                   if(data.flag == '1'){
                       $('tbody.all_tyroes').prepend(data.html)
                   } else {
                       var tyroe_id = '#tyroe_id_'+data.id;
                       $(tyroe_id).html(data.html);
                   }
               }
           }
        });
    }

    function display_section(obj, visible) {
        $('.basic_info').hide();
        $('.resume_info').hide();
        $('.portfolio_info').hide();
        $('.subscription_info').hide();
        $('.' + visible).show();
        $('#active').removeAttr('id');
        $(obj).closest('.row-fluid').attr('id', 'active')
    }

    //Get states and cities by country
    function getcitiesstates(country_id,stateBox,citiesBox){
        $.ajax({
            type: "POST",
            data:{country_id:country_id},
            url: '<?=$vObj->getURL("studios/get_cities_states")?>',
            success: function(data){
                $('#AjaxResponse').html(data);
               // var states = $('#AjaxResponse').find('#StatesDiv').html();
                var cities = $('#AjaxResponse').find('#CitiesDiv').html();
                //$(stateBox).html(states);
                $(citiesBox).html(cities);
            }
        });
    }



</script>
<!-- START RIGHT COLUMN -->
<div id="AjaxResponse" class="display_hide"></div>
<div class="span5 tab-margin">
    <div class="container-fluid">
        <div class="span4 default-header bottom-margin headerdiv">
            <h4>
                <?php
                if ($flag == "edit") {
                    echo LABEL_ADMIN_EDIT_USER;
                } else {
                    echo LABEL_ADMIN_CREATE_USER;
                }
                ?>
            </h4>
        </div>
        <div class="modal_error_box alert alert-danger">
        </div>

        <form class="inline-input add_user_form" _lpchecked="1"
              enctype="multipart/form-data" name="register_form" method="post" id="admin_tyroe_form"
              action=">
            <div class="basic_info">
                <?php include dirname(__FILE__) . "/new_basic.php"; ?>
            </div>
            <div class="span4 tab-margin field-box actions">
                <div class="span4 tab-margin field-box actions">

                    <input type="submit" name="submit" class="btn-glow primary" value="<?= LABEL_SAVE_CHANGES ?>"
                           onclick="submit()">
                    <!--<span>OR</span>
                    <input type="reset" value="Cancel" class="reset">-->
                </div>
        </form>
    </div>
</div>

<!-- END RIGHT COLUMN -->