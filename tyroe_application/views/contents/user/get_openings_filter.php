<?php
if (!empty($job_detail)) {
    for ($job = 0; $job < count($job_detail); $job++) {
        ?>
        <tr id="job_id_<?= $job_detail[$job]['job_id'] ?>">
            <td class="col1"><input type="checkbox" name="bulk_check[]" id="opening_check" class="opening_check"
                                    value="<?= $job_detail[$job]['job_id'] ?>"/><?= $job_detail[$job]['job_id'] ?></td>
            <!--<td></td>-->
            <td class="col2"><p class="rt-name"><?=$job_detail[$job]['studio_name'] ?></p><span class="rt-title"><?= $job_detail[$job]['industry_name'] ?></span></td>
            <td class="col3"><?= $job_detail[$job]['job_title'] ?></td>
            <td class="col4"><a HREF="#"><?= $job_detail[$job]['email'] ?></a> </td>
            <td class="col5"><?=date('M j,Y',$job_detail[$job]['created_timestamp']) ?></td>
            <td class="col6"><?= $job_detail[$job]['team'] ?></td>
            <td class="col7"><?= $job_detail[$job]['ap'] ?></td>
            <td class="col8"><?= $job_detail[$job]['cands'] ?></td>
            <td class="col9"><?= $job_detail[$job]['sl'] ?></td>
            <td class="col10"><a href="javascript:void(0)" class="btn_pop" id="<?= $job_detail[$job]['job_id'] ?>">Edit</a> <a href="javascript:void(0)" class="delete_opening" id="<?= $job_detail[$job]['job_id'] ?>">Delete</a></td>
        </tr>
    <?php
    }
    ?>
<?php } else { ?>
<tr style="text-align: center;"><td colspan="11"><strong>No match found</strong></td></tr>
<?php } ?>
<!--|||-->
<form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
              method="post" action="<?= $vObj->getURL('jobopenings') ?>">
    <div class="span12 pagination"><?= $pagination ?></div>
    <input type="hidden" value="<?= $page ?>" name="page">
    <input type="hidden" value="<?=$job_title?>" name="job_title">
    <input type="hidden" value="<?=$search_by?>" name="search_by">
    <input type="hidden" value="<?=$filter_value?>" name="filter_value">
    <input type="hidden" value="<?=$filter_condition?>" name="filter_condition">
    <input type="hidden" value="<?=$filter_column?>" name="filter_column">
    <input type="hidden" value="<?=$sorting?>" name="sorting">
    <input type="hidden" value="<?=$sort_column?>" name="sort_column">
    <input type="hidden" value="<?=$sort_type?>" name="sort_type">
</form>