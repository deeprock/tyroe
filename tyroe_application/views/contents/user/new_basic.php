<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">


</script>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin bottom-margin">
    <h4><a href="javascript:void(0)">Account Details</a></h4>
</div>

<div class="clearfix"></div>
<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_EMAIL ?>:</label></div>
    <div class="span3 tab-margin"><input name="email" class="span3" type="text" id="email"
                                         value="<?= $email ?>">
        <img id="loader" src="<?= ASSETS_PATH ?>img/loader.gif" style="display: none" width="20px"/>
        <img src="" id="user_result" style="display: none; margin: -35px -25px 0px 0px; float:right;" width="20px"/>
    </div>
</div>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_PASSWORD ?>:</label></div>
    <div class="span3 tab-margin"><input class="span3 inline-input" type="password"
                                         name="password" id="password"
                                         value="<?=$password?>"></div>
</div>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_CONFIRM_PASSWORD ?>:</label></div>
    <div class="span3 tab-margin"><input class="span3 inline-input" type="password"
                                         name="confirmpass" id="confirmpass"
                                         value="<?=$password?>"></div>
</div>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin bottom-margin">
    <h4><a href="javascript:void(0)">Profile Details</a></h4>
</div>
<div class="clearfix"></div>
<!--<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_USER_NAME ?>:</label></div>
    <div class="span3 tab-margin"><input class="span3" type="text"
                                         name="username" id="username"
                                         value="<?= $username ?>"></div>
</div>
<div class="clearfix"></div>-->
<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_FIRST_NAME ?>:</label></div>
    <div class="span3 tab-margin"><input class="span3" type="text"
                                         name="firstname" id="firstname"
                                         value="<?= $firstname ?>"></div>
</div>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin">
    <div class="span2 tab-margin"><label><?= LABEL_LAST_NAME ?>:</label></div>
    <div class="span3 tab-margin"><input class="span3" type="text"
                                         name="lastname" id="lastname"
                                         value="<?= $lastname ?>"></div>
</div>
<div class="clearfix"></div>
<div class="span5 tab-margin field-box bottom-margin">
    <div class="span2 tab-margin"><label>Country:</label></div>
    <div class="ui-select span3">
        <select class="span3 inline-input" name="user_country"
                id="user_country" onchange="getcitiesstates(this.value,'#user_state','#user_city')">
            <option value="">Select Country</option>
            <?php
            foreach($get_countries as $country)
            {
                $selected="";
                if($country['id']==$country_id)
                {
                    $selected="selected=selected";
                }
                ?>
                <option <?=$selected?> value="<?=$country['id']?>"><?=$country['name']?></option>
            <?php
            }
            ?>
        </select>
    </div>
</div>
<div class="clearfix"></div>
<!--<div class="span5 tab-margin field-box bottom-margin">
    <div class="span2 tab-margin"><label><?php/*= LABEL_STATE */?>:</label></div>
    <div class="result-states"></div>
    <div class="ui-select span3 all-states">
        <select class="span3 inline-input" name="user_state" id="user_state">
            <option value="">Select State</option>
            <?php
/*            foreach ($get_states as $key => $states) {
                */?>
                <option <?php /*if ($states['states_id'] == $state) {
                    echo "selected='selected'";
                } else {
                } */?>
                    value="<?php /*echo $states['states_id'] */?>"><?php /*echo $states['states_name'] */?></option>
            <?php
/*            }
            */?>
        </select>
    </div>
</div>-->
<div class="clearfix"></div>
<div class="span5 tab-margin field-box">
    <div class="span2 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
    <div class="ui-select span3 all-states">
    <select class="span3 inline-input" name="user_city" id="user_city">
        <option value="">Select City</option>
        <?php
        foreach ($get_cities as $cities) {
            ?>
            <option <?php if ($cities['city_id'] == $city || $cities['city_id'] == $city) {
                echo "selected='selected'";
            } ?>
                value="<?php echo $cities['city_id'] ?>"><?php echo $cities['city'] ?></option>
        <?php
        }
        ?>
    </select>
   </div>
</div>
<div class="clearfix"></div>
<div class="field-box span5 tab-margin bottom-margin <?php if($role_id != REVIEWER_ROLE) echo "display_hide";?>">
    <div class="span2 tab-margin"><label><?= LABEL_STUDIO ?>:</label></div>
    <div class="span3 tab-margin"><label><?=$studio_name['username']?></label></div>
</div>
<div class="clearfix"></div>
<div class="span5 tab-margin field-box">
    <div class="span2 tab-margin"><label><?= LABEL_CELLPHONE ?>:</label></div>

    <input class="span1" type="number"
           name="user_cellphone_code" id="user_cellphone_code"
           value="<?php echo $code;
           ?>" maxlength="3">

    <input class="span2" type="text"
           name="cellphone" id="cellphone"
           value="<?php echo $number; ?>">
</div>
<div class="clearfix"></div>
<input type="hidden" value="<?= $flag ?>" name="flag" id="flag">
                    <input type="hidden" value="<?= $user_id ?>" name="item_id" id="item_id">
