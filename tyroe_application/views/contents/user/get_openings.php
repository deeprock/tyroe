<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    var delete_id = 0;
    var delete_type = 'single';
    var opening_obj;
    $(function () {
        $("body").delegate('.btn_pop', 'click', function (e) {
            // Prevents the default action to be triggered.
            $('.loader-save-holder').show();
            e.preventDefault();
            var item_id = this.id;
            var url = '<?= $vObj->getURL("jobopenings/form/") ?>';
            $.ajax({
                type: 'POST',
                url: url,
                data: "item_id=" + item_id,
                success: function (data) {
                    if (data) {
                        var jqObj = jQuery(data);
                        $('.modal-title').html(jqObj.find(".headerdiv").html());
                        jqObj.find(".headerdiv").remove();
                        var getfunc = jqObj.find(".btn-glow").attr("onclick");
                        $('.btn-save').attr("onclick", getfunc);
                        jqObj.find(".btn-glow").remove();
                        $('.form-body').html(jqObj);
                        $('#myModal').modal('show');
                        $('.loader-save-holder').hide();
                        $('body').addClass('modal-open');
                        $('body').css({"overflow": "hidden"})
                        /*$('#element_to_pop_up').html(data);
                         var x_axis = Math.floor(window.innerWidth/2);
                         var popup_width=$('#element_to_pop_up').css('width')
                         var actual_width=x_axis-(parseInt(popup_width.replace('px',''))/2)-40;
                         $('#element_to_pop_up').bPopup( {position: [actual_width, 10]});
                         $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');*/
                    }
                }
            });
        });
        $("body").delegate('.delete_opening', 'click', function (e) {
            e.preventDefault();
            opening_obj = $(this);
            delete_id = this.id;
            delete_type = 'single'
//job_delete_approval
            $('#myModal_delete_opening').modal('show');
        });
        $("body").delegate('#btn_delete_studio', 'click', function (e) {
            $('#delete-studio-error-message').hide();
            if ($.trim($('#delete_confirm_text').val()) == '') {
                $('#delete-studio-error-message').html("Please enter DELETE text").show();
                return false;
            }

            if ($.trim($('#delete_confirm_text').val()) != 'DELETE') {
                $('#delete-studio-error-message').html("Please enter correct text").show();
                return false;
            }
            $('.loader-save-holder').show();
            if (delete_type == 'single') {
                var url = '<?= $vObj->getURL("jobopenings/delete_opening") ?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: "delete_id=" + delete_id,
                    dataType: "json",
                    success: function (data) {
                        $('.loader-save-holder').hide();
                        $('#job_delete_approval').modal('hide');
                        if (data.success)
                        {
                            $(".abc").fadeIn('500');
                            $(".abc").addClass("alert alert-success");
                            $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                            setTimeout(function () {
                                $('tr[id=job_id_' + delete_id + ']').fadeOut(1000, function () {
                                    $(this).remove();
                                });
                            }, 1000);
                            setTimeout(function () {
                                $(".abc").fadeOut('1500');
                            }, 3000);
                        } else {
                            $(".abc").fadeIn('500');
                            $(".abc").addClass("alert alert-danger");
                            $(".alert-danger").html('<i class="icon-remove-sign"></i>' + "Fail to delete Studio");
                            setTimeout(function () {
                                $(".abc").fadeOut('1500');
                            }, 3000);
                        }
                    }
                });
            } else {
                var url = '<?= $vObj->getURL("studios/bulkaction") ?>';
                $.ajax({
                    type: "POST",
                    data: delete_id,
                    url: url,
                    dataType: "json",
                    success: function (data) {

                        $.each(data.json_idz, function (key, val) {
                            $('#job_id_' + val).fadeOut(700);
                        });
                        $(".abc").show();
                        $(".abc").addClass("alert alert-success");
                        $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                        setTimeout(function () {
                            $(".abc").fadeOut('1500');
                        }, 2000);
                        $('.loader-save-holder').hide();
                    }
                });
            }
            $('#myModal_delete_opening').modal('hide');
            $('#delete_confirm_text').val('');
        });
        $('#bulkaction').on('click', function () {
            var delete_ids = $('.bulk_form').serialize();
//console.log(delete_ids);
            if (delete_ids == "")
            {
                alert("Please select a record first");
            } else
            {
                delete_id = delete_ids;
                delete_type = 'bulk'
//job_delete_approval
                $('#myModal_delete_opening').modal('show');
            }
        });
        $('#job_title_filter').donetyping(function () {
            $('.loader-save-holder').show();
            var search_title = $("#job_title_filter").val();
            var search_by = $("#search_by option:selected").val();
            var action = $('#filter_tyroes').val();
            $.ajax({
                type: "POST",
                data: "bool=" + true + "&job_title=" + search_title + "&search_by=" + search_by,
                url: '<?= $vObj->getURL("jobopenings") ?>',
                success: function (data) {
                    $('.loader-save-holder').hide();
                    var alldata = data;
                    var data_arr = alldata.split('<!--|||-->');
                    $("tbody.all_openings").html(data_arr[0]);
                    $('#footer_pagination').html(data_arr[1]);
                }
            });
            return false;
        }, 500);
        $('#btn_filter').on('click', function () {
            if ($('#filter_div').is(':visible'))
            {
                $('#filter_div').fadeOut(500);
            }
            else
            {
                $('#filter_div').fadeIn(500);
            }
        });
        $('#add_filter').on('click', function () {
            var column = $('#filter_column').val();
            var condition = $('#filter_condition').val();
            var value = $('#filter_value').val();
            if (value == "")
            {
                alert('ERROR! Empty value in the value filter found');
            }
            else
            {
                $('.loader-save-holder').show();
                $.ajax({
                    data: "filter_column=" + column + "&filter_condition=" + condition + "&filter_value=" + value + "&bool=" + true,
                    url: '<?= $vObj->getURL("jobopenings") ?>',
                    type: "POST",
                    success: function (data) {
                        $('.loader-save-holder').hide();
                        var alldata = data;
                        var data_arr = alldata.split('<!--|||-->');
                        $("tbody.all_openings").html(data_arr[0]);
                        $('#footer_pagination').html(data_arr[1]);
                        $('#filter_div').fadeOut(700);
                    }
                });
            }
        });
        $('.sorter').on('click', function () {
            $('.loader-save-holder').show();
            var sort_col = $(this).attr('data-column');
            var sort_type = $(this).attr('data-stype');
            var job_title = $("#job_title_filter").val();
            var search_by = $("#search_by option:selected").val();
            var url = '<?= $vObj->getURL("jobopenings") ?>';
            var obj = $(this);
            $.ajax({
                type: "POST",
                data: "sort_column=" + sort_col + "&sort_type=" + sort_type + "&sorting=" + true + "&bool=" + true + "&job_title=" + job_title + "&search_by=" + search_by,
                url: url,
                success: function (data) {
                    var alldata = data;
                    var data_arr = alldata.split('<!--|||-->');
                    $("tbody.all_openings").html(data_arr[0]);
                    $('#footer_pagination').html(data_arr[1]);
                    if (sort_type == "ASC")
                    {
                        obj.attr('data-stype', 'DESC');
                    }
                    else
                    {
                        obj.attr('data-stype', 'ASC');
                    }
                    $('.loader-save-holder').hide();
                }
            });
        });

    });
</script>
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

</style>
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b tr-mr">
                <div id="admin_users_top_header" class="span9">
                    <h5 class="resume-title">Manage Openings</h5>
                    <input class="span4 pull-left search tr-search layout" type="text" placeholder="type a Job title" name="job_title_filter" id="job_title_filter" value="<?= $job_title ?>">
                    <select class="margin-default btn" id="search_by" name="search_job_by">
                        <option value="job_title" <?php if ($search_by == "job_title") echo 'Selected' ?>>Job Title</option>
                        <option value="company_name" <?php if ($search_by == "company_name") echo 'Selected' ?>>Company Name</option>
                    </select>

                </div>
                <div class="span3 topRightHead">
                    <a href="javascript:void(0)" class="pull-left selectedDel"  id="bulkaction">Delete Selected</a>
                    <a href="javascript:void(0)" class="newopening btn-flat primary go-btn pull-right btn_pop no-margin custom-btn-color">+ New Opening</a>
                </div>
            </div>

            <div class="row-fluid show-grid">
                <?php
                echo $this->session->userdata('success_message');
                $this->session->unset_userdata('success_message');
                ?>
            </div>
            <div class="show-grid margin-adjust">

                <div class="clearfix"></div>
            </div>
            <div class="abc" style="display:none">
                <i class="icon"></i> <!--Please select any record-->
            </div>
            <div class="row-fluid"> <!--removed classes are  header z-margin-b-->
                <form class="bulk_form inline-input" id="admin_openings" name="bulk_form">
                    <table class="table admin_opeing_table">
                        <thead>
                            <tr>
                                <th class="col1"><input type="checkbox" name="master_check" id="master_check" onchange="multiplechecks($(this), '.opening_check');"><a href="javascript:void(0)" class="sorter" data-column="job_id" data-stype="ASC">ID</a></th>
                                <!--<th class="span1"></th>-->
                                <th class="col2"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.username" data-stype="ASC">Studio</a></th>
                                <th class="col3"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_JOBS ?>.job_title" data-stype="ASC">JOB TITLE</a></th>
                                <th class="col4"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.email" data-stype="ASC">EMAIL</a> </th>
                                <th class="col5"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_JOBS ?>.created_timestamp" data-stype="ASC">CREATED</a> </th>
                                <th class="col6"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="team" data-stype="ASC">TEAM</a> </th>
                                <th class="col7"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="applicant" data-stype="ASC">AP</a></th>
                                <th class="col8"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="candidate" data-stype="ASC">CAN</a></th>
                                <th class="col9"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="shortlisted" data-stype="ASC">SL</a></th>
                                <th class="col10"><div class="position-relative"><span class="line"></span></div></th>
                        </tr>
                        </thead>
                        <tbody class="all_openings team-tr">
                            <?php
                            for ($job = 0; $job < count($job_detail); $job++) {
                                ?>
                                <tr id="job_id_<?= $job_detail[$job]['job_id'] ?>">
                                    <td class="col1"><input type="checkbox" name="bulk_check[]" id="opening_check" class="opening_check"
                                                            value="<?= $job_detail[$job]['job_id'] ?>"/><?= $job_detail[$job]['job_id'] ?></td>
                                                 <!--<td></td>-->
                                    <td class="col2"><p class="rt-name"><?= $job_detail[$job]['studio_name'] ?></p><span class="rt-title"><?= $job_detail[$job]['industry_name'] ?></span></td>
                                    <td class="col3"><?= $job_detail[$job]['job_title'] ?></td>
                                    <td class="col4"><a HREF="#"><?= $job_detail[$job]['email'] ?></a> </td>
                                    <td class="col5"><?= date('M j,Y', $job_detail[$job]['created_timestamp']) ?></td>
                                    <td class="col6"><?= $job_detail[$job]['team'] ?></td>
                                    <td class="col7"><?= $job_detail[$job]['ap'] ?></td>
                                    <td class="col8"><?= $job_detail[$job]['cands'] ?></td>
                                    <td class="col9"><?= $job_detail[$job]['sl'] ?></td>
                                    <td class="col10"><a href="javascript:void(0)" class="btn_pop" id="<?= $job_detail[$job]['job_id'] ?>">Edit</a> <a href="javascript:void(0)" class="delete_opening" id="<?= $job_detail[$job]['job_id'] ?>">Delete</a></td>
                                </tr>
                                <?php
                            }
                            ?>

                        </tbody>
                    </table>
                </form>
                <div id="footer_pagination">
                    <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                          method="post" action="<?= $vObj->getURL('jobopenings') ?>">
                        <div class="span12 pagination"><?= $pagination ?></div>
                        <input type="hidden" value="<?= $page ?>" name="page">
                        <input type="hidden" value="<?= $job_title ?>" name="job_title">
                        <input type="hidden" value="<?= $search_by ?>" name="search_by">
                        <input type="hidden" value="<?= $filter_value ?>" name="filter_value">
                        <input type="hidden" value="<?= $filter_condition ?>" name="filter_condition">
                        <input type="hidden" value="<?= $filter_column ?>" name="filter_column">
                        <input type="hidden" value="<?= $sorting ?>" name="sorting">
                        <input type="hidden" value="<?= $sort_column ?>" name="sort_column">
                        <input type="hidden" value="<?= $sort_type ?>" name="sort_type">
                    </form>
                </div>
                   <!-- <input type="hidden" value="<?php /* = $item_id */ ?>" name="item_id">
                    <input type="hidden" value="<?php /* = $page */ ?>" name="page">
                    <input type="hidden" value="<?php /* = $sort_by */ ?>" name="sort_by">
                    <input type="hidden" value="<?php /* = $flag */ ?>" name="flag">-->

            </div>
        </div>
    </div>
    <!-- end main container -->

    <!--Edit/New Opening Modal form-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
        <div class="global-popup_container">
            <div class="global-popup_details">
                <div class="global-popup_holder global-popup ">
                    <div class="global-popup_header job_create_header">
                        <button type="button" class="close top30px" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="clearfix"></div>
                    <div class="global-popup_body form-body padding0px padding-left-15px">
                    </div>
                    <div class="clearfix"></div>
                    <div class="global-popup_footer ">
                        <button type="button" class="btn-flat white pull-right" data-dismiss="modal">Close</button>
                        <button type="button" class="btn-flat primary btn-save pull-right margin-right-5px">Save changes</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- job delete approval-->
    <div class="modal fade" id="job_delete_approval" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="global-popup_container">
            <div class="global-popup_details">
                <div class="global-popup_holder global-popup ">
                    <div class="global-popup_header job_create_header">
                        <button type="button" class="close top30px" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                    </div>
                    <div class="clearfix"></div>
                    <div class="global-popup_body form-body padding0px padding-left-15px confirm_body">
                    </div>
                    <div class="clearfix"></div>
                    <div class="global-popup_footer confirm_footer">
                        <button type="button" class="btn-flat white pull-right" data-dismiss="modal">Close</button>
                        <button type="button" class="btn-flat primary btn-save pull-right margin-right-5px">Save changes</button>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <div style="display: none;" class="modal myModal_delete_studio_2" id="myModal_delete_opening" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Confirm Account Deletion</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-warning">
                        <i class="icon-warning-sign"></i>
                        <div class="confirmation-text">
                            <span class="l1">Are you sure ?</span><br/>
                            <span class="l2">All Job-Opening data will be permanently deleted. This can't be undone.</span><br/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <p class="delete-message-2">Type DELETE to confirm</p>
                    <p>
                        <textarea class="delete-confirm-text" id="delete_confirm_text"></textarea>
                    </p>
                    <label class="delete-studio-error-message" id="delete-studio-error-message">Error message</label>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-glow btn-cancel-delete-studio" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-glow primary btn-delete-studio" id="btn_delete_studio">Delete Job Opening</button>
                </div>
            </div>
        </div>
    </div>




