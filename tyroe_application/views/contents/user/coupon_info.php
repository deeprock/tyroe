<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {
        $(function () {
            $('.btn_pop').bind('click', function (e) {
                // Prevents the default action to be triggered.
                e.preventDefault();
                var param2 = this.text;
                var sel_id = $('input[name=rd_coupon]:checked').val();
                if ((sel_id == undefined) && (param2 == 'Edit')) {
                    //alert("Error! Please select any Record.");
                    $(".abc").addClass("alert alert-warning").removeAttr('style');
                    $(".icon").addClass("icon-warning-sign");
                    $(".alert-warning").html('<i class="icon-warning-sign"></i>' + 'Error! Please select any Record');
                    setTimeout(function () {
                        $(".abc").hide();
                    }, 2000);
                    return false;

                }

                if ((sel_id == undefined) && (param2 == 'Delete')) {
                    //alert("Error! Please select any Record.");
                    $(".abc").addClass("alert alert-warning").removeAttr('style');
                    $(".icon").addClass("icon-warning-sign");
                    $(".alert-warning").html('<i class="icon-warning-sign"></i>' + 'Error! Please select any Record');
                    setTimeout(function () {
                        $(".abc").hide();
                    }, 2000);
                    return false;
                }
                setTimeout(function () {
                    $(".abc").hide();
                }, 2000);
                $(".abc").hide();
                // Triggering bPopup when click event is fired
                var identifier = this.id;
                var data = "";
                var param = "";
                var classes = "";

                param = sel_id + "/" + param2;
                if (identifier == 'new_coupon') {
                    var url = '<?=$vObj->getURL("subscription/NewCoupon/")?>' + param;
                } else if (identifier == 'delete' && param2 == 'Delete') {
                    var url = '<?=$vObj->getURL("subscription/DeleteCoupon/")?>' + param;
                }
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        if (identifier == 'delete' && param2 == 'Delete') {
                            if (data) {
                                /*====01-07-2014====*/
                                $(".abc").addClass("alert alert-success");
                                $(".alert-success").html('<i class="icon-ok-sign"></i>' + 'Record Deleted Successfully');
                                setTimeout(function () {
                                    $(".abc").hide();
                                }, 2000);
                                $('.modal-backdrop, .modal-backdrop.fade.in').css('opacity', '0');
                                $('.modal.fade.in').hide();
                                $('.alert-success').css('display', 'block');
                                /*====01-07-2014====*/
                                $('input[name=rd_coupon]:checked').closest("tr").remove()
                            }
                        }
                        var jqObj = jQuery(data);
                        $('.modal-title').html(jqObj.find(".headerdiv").html());
                        jqObj.find(".headerdiv").remove();
                        var getfunc = jqObj.find(".btn-glow").attr("onclick");
                        $('.btn-save').attr("onclick", getfunc);
                        jqObj.find(".btn-glow").remove();
                        $('.modal-body').html(jqObj);
                        /*$('#element_to_pop_up').html(data);
                         var x_axis = Math.floor(window.innerWidth / 2);
                         var popup_width = $('#element_to_pop_up').css('width')
                         var actual_width = x_axis - (parseInt(popup_width.replace('px', '')) / 2) - 40;
                         $('#element_to_pop_up').bPopup({position: [actual_width, 10]});

                         $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');*/

                    }
                })
            });
            function myfilter() {
                var id = $("#cid").val();
                var title = $("#coupon_title").val();
                var label = $("#coupon_label").val();
                var status = $("#coupon_status").val();
                var data = "bool=" + true + "&title=" + title + "&id=" + id + "&label=" + label + "&status=" + status;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?=$vObj->getURL("subscription/getcoupons")?>',
                    /*dataType: "json",*/
                    success: function (data) {
                        $(".all-coupons").hide();
                        var alldata = jQuery(data);
                        $(".result").html(data);
                        $(".pagination").remove();
                        $('.table-cus-footer').html(" ");
                        $('.table-cus-footer').html($(alldata).filter(".pagination"));
                    }
                });
            }

            $(".filter-tab").keypress(function (e) {
                if (e.which === 13) {
                    myfilter();
                }
            });
            $("#coupon_status").change(function () {
                myfilter();
            });
        });

    })(jQuery);
</script>
<!-- POP-UP -->
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

    .table-filter {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

</style>

<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid show-grid">
                <div class="row-fluid header z-margin-b">
                    <h5 class="resume-title">Manage Coupons</h5>
                </div>
                <ul class="nav nav-tabs">
                    <li><a href="<?= $vObj->getURL('subscription') ?>">Subscriptions</a></li>
                    <li><a href="<?= $vObj->getURL('subscription/getpayment') ?>">Payments</a></li>
                    <li class="active"><a href="<?= $vObj->getURL('subscription/getcoupons') ?>">Coupons</a></li>

                </ul>
                   <span class="pull-right margin-adjust button-center">
                       <a class="btn-flat primary btn_pop" id="new_coupon" href="javascript:void (0);"
                          data-toggle="modal" data-target="#myModal">New</a>
                    <a class="btn-flat primary btn_pop " id="new_coupon"
                       href="javascript:void (0);" data-toggle="modal" data-target="#myModal">Edit</a>
                       <a class="btn-flat primary btn_pop delete" id="delete"
                          href="javascript:void (0);">Delete</a>
                   </span>
            </div>


            <div class="clearfix"></div>


            <div class="abc" style="display:none">
                <i class="icon"></i> <!--Please select any record-->
            </div>


            <form class="new_user_form inline-input" _lpchecked="1" enctype="multipart/form-data" name="opening_form"
                  method="post" action="<?= $vObj->getURL('subscription/getcoupons') ?>">
                <div class="row-fluid show-grid">
                    <?php
                    echo $this->session->userdata('success_message');
                    $this->session->unset_userdata('success_message');
                    ?>
                </div>

                <table class="table">
                    <thead class="table-head">
                    <tr class="table-filter filter-tab custom-table-head">
                        <th></th>
                        <th><input class="span1" type="text" name="cid" id="cid" value="<?= $search ?>"
                                   placeholder="Filter">
                        </th>
                        <th><input class="span2" type="text" name="coupon_title" id="coupon_title"
                                   value="<?= $search ?>"
                                   placeholder="Filter Name">
                        </th>
                        <th><input class="span2" type="text" name="coupon_label" id="coupon_label"
                                   value="<?= $search ?>"
                                   placeholder="Filter Label"></th>
                        <th></th>
                        <th>
                            <div class="ui-select span6">
                                <select name="coupon_status" id="coupon_status">
                                    <option value="">Filter Status</option>
                                    <option value="1">used</option>
                                    <option value="0">unused</option>
                                </select>
                            </div>
                        </th>
                        <th></th>
                        <th></th>
                        <th><input class="span2" type="text" name="filter_payment" id="filter_payment"
                                   value="<?= $search ?>"
                                   placeholder="Filter Payments"></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('coupon_id','<?= $new_flag ?>',this)">ID<?= $sort_img_user_id ?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('username','<?= $new_flag ?>',this)">Tyroe<?= $sort_img_email ?></a>
                        </th>
                        <th><a href="javascript:void(0)" onclick="ordering('coupon_label','<?= $new_flag ?>',this)">Coupon
                                Label<?= $sort_img_firstname ?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('coupon_value','<?= $new_flag ?>',this)">Value<?= $sort_img_lastname ?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('role_id','<?= $new_flag ?>',this)">Status<?= $sort_img_role_id ?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('role_id','<?= $new_flag ?>',this)">Issued<?= $sort_img_role_id ?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('role_id','<?= $new_flag ?>',this)">Used<?= $sort_img_role_id ?></a>
                        </th>
                        <th><a href="javascript:void(0)" onclick="ordering('role_id','<?= $new_flag ?>',this)">Used
                                For<?= $sort_img_role_id ?></a></th>
                    </tr>
                    </thead>
                    <tbody class="result"></tbody>
                    <tbody class="all-coupons">
                    <?php
                    foreach ($data as $k => $v) {
                        if ($k % 2 == 0)
                            $class = "table-odd";
                        else
                            $class = "table-default-r";
                        ?>
                        <tr class="<?= $class ?>">
                            <td><input type="radio" name="rd_coupon" id="rd_coupon"
                                       value="<?= $v['allot_id'] ?>"/></td>
                            <td><?= $v['allot_id'] ?></td>
                            <td><?= $v['username'] ?></td>
                            <td><?= $v['coupon_label'] ?></td>
                            <td>$<?= $v['coupon_value'] ?></td>
                            <td><?= ($v['status_coupon'] == 1) ? "Not Used" : "Used"; ?></td>
                            <td><?= date("d-m-Y", $v['coupon_issued']) ?></td>
                            <td><?= ($v['coupon_used'] > 0) ? date("d-m-Y", $v['coupon_used']) : " " ?></td>
                            <td><?= $v['coupon_used_for'] ?></td>
                        </tr>
                    <?php
                    }
                    ?>
                    </tbody>
                    <tfoot class="table-cus-footer">
                    <tr class="pagination">
                        <td colspan="9"><?= $pagination ?></td>
                    </tr>
                    </tfoot>
                </table>
                <input type="hidden" value="<?= $item_id ?>" name="item_id">
                <input type="hidden" value="<?= $page ?>" name="page">
                <input type="hidden" value="<?= $sort_by ?>" name="sort_by">
                <input type="hidden" value="<?= $flag ?>" name="flag">
            </form>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-save">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- end main container -->




