<?php
if (!empty($data)) {
    foreach ($data as $k => $v) {
        if ($k % 2 == 0)
            $class = "table-odd";
        else
            $class = "table-default-r";
        ?>
        <tr class="<?= $class ?>">
            <td><input type="radio" name="rd_coupon" id="rd_coupon"
                       value="<?= $v['coupon_id'] ?>"/></td>
            <td><?= $v['allot_id'] ?></td>
            <td><?= $v['username'] ?></td>
            <td><?= $v['coupon_label'] ?></td>
            <td>$<?= $v['coupon_value'] ?></td>
            <td><?= ($v['status_coupon'] == 1) ? "Not Used" : "Used"; ?></td>
            <td><?= date("d-m-Y", $v['coupon_issued']) ?></td>
            <td><?= ($v['coupon_used'] > 0) ? date("d-m-Y", $v['coupon_used']) : " " ?></td>
            <td><?= $v['coupon_used_for'] ?></td>
        </tr>
    <?php
    }
    ?>
<?php } else { ?>
<tr class="table-odd" style="text-align: center;"><td colspan="9"><strong>No match found</strong></td></tr>
<?php } ?>
<div class="div_pagination">
    <tr class="pagination">
        <td colspan="9"><?=$pagination?></td>
    </tr>
</div>