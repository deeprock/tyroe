<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">

    $("#chkall_tyroe").click(function(){
        if($("#chkall_tyroe").is(':checked')){
            $("#tyroe_id").prop("disabled",true);
        } else {
            $("#tyroe_id").prop("disabled",false);
        }
    });

    function NewCoupon() {
        var check_all = $('input[name=chkall_tyroe]:checked').val();
        var tyroe_id = $("#tyroe_id").val();
        if (check_all) {
            tyroe_id = "0";
        }
        var coupon_value = $("#coupon_value").val();
        if($.isNumeric(coupon_value) == false){
            alert("Please enter Numeric values");
            $("#coupon_value").val("");
            return false;
        }
        var coupon_label = $("#coupon_label1").val();
        var edit_id = $("#edit_id").val();
        var allot_id = $("#allot_id").val();
        if((tyroe_id == "")&&(check_all==undefined)){alert("Please Select any Tyroe or All Tyroe.");return false;}
        var data = "tyroe_id=" + tyroe_id + "&coupon_value=" + coupon_value + "&coupon_label=" + coupon_label
                + "&edit_id=" + edit_id + "&allot_id=" + allot_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("subscription/SaveCoupon")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);

                }
                else {
                    $(".notification-box-message").css("color", "#b81900");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
</script>

<div class="row-fluid form-wrapper">
    <div class="span12">
        <div class="container-fluid">
            <div class="span4 default-header bottom-margin headerdiv">
                <h4>
                    <?php
                    if ($flag == "edit") {
                        echo LABEL_EDIT_COUPON;
                    } else {
                        echo LABEL_ADD_COUPON;
                    }
                    ?>
                </h4>
            </div>
            <div class="field-box">
                <p><span class="span4"><?= LABEL_TYROE ?>:</span>
                    <?php if ($flag != "edit") { ?>
                    <select class=" inline-input" name="tyroe_id"
                            id="tyroe_id">
                        <option value="">Choose Tyroe</option>
                        <?php
                        foreach ($get_tyroes as $key => $get_tyroe) {
                            ?>
                            <option <?php if ($selected_coupon['tyroe_id'] == $get_tyroe['user_id']) {
                                echo "selected='selected'";
                            } else {
                            }?>
                                value="<?php echo $get_tyroe['user_id'] ?>"><?php echo $get_tyroe['username'] ?></option>
                        <?php
                        }
                        ?>
                    </select>
                    <p>
                    <div class="span12"><input class="cust-inp-cj rd-inp"
                                               type="checkbox" <?php if ($selected_coupon['tyroe_id'] == "0") {
                            echo "checked='checked'";
                        }?> name="chkall_tyroe" id="chkall_tyroe"
                            ><span class="rd-lab">All Tyroes(careful!)</span> </div></p>
                    <?php } else { echo $tyroe_name; ?><input type="hidden" name="tyroe_id" id="tyroe_id" value="<?= $selected_coupon['user_id'] ?>"/><?php } ?>

                </p>
                <!--<p>
                    <div class="span12"><input class="cust-inp-cj"
                            type="checkbox" <?php /*if ($selected_coupon['tyroe_id'] == "0") {
                            echo "checked='checked'";
                        }*/?> name="chkall_tyroe" id="chkall_tyroe"
                            ><span>All Tyroes(careful!)</span> </div></p>-->


                <p><span class="span4">Value:</span>
                    $<input class="span3" type="text"
                            name="coupon_value" id="coupon_value"
                            value="<?= $selected_coupon['coupon_value'] ?>"></p>

                <p><span class="span4">Label:</span>
                    <input class="span8" type="text"
                           name="coupon_label1" id="coupon_label1"
                           value="<?= $selected_coupon['coupon_label'] ?>"></p>
                <input type="hidden" name="edit_id" id="edit_id" value="<?= $selected_coupon['coupon_id'] ?>"/>
                <input type="hidden" name="allot_id" id="allot_id" value="<?= $selected_coupon['allot_id'] ?>"/>
                <input type="submit" name="submit" class="btn-glow primary"
                       value="<?= LABEL_SAVE ?>"
                       onclick="NewCoupon();">

            </div>
        </div>
    </div>
</div>


