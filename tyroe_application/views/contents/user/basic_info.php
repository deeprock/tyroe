<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="field-box">
    <label><?= LABEL_FIRST_NAME ?>:</label>
    <input class="span9" type="text"
           name="firstname" id="firstname"
           value="<?=$firstname?>">
</div>
<div class="field-box">
    <label><?= LABEL_LAST_NAME ?>:</label>
    <input class="span9" type="text"
           name="lastname" id="lastname"
           value="<?=$lastname?>">
</div>
<div class="field-box">
    <label><?= LABEL_EMAIL ?>:</label>
    <input name="email" class="span9" type="text"
           value="<?=$email?>">
</div>
<div class="field-box">
    <label><?= LABEL_USER_NAME ?>:</label>
    <input class="span9 inline-input" type="text"
           name="username" id="username"
           value="<?=$username;?>">
</div>
<div class="field-box">
    <label><?= LABEL_PASSWORD ?>:</label>
    <input class="span9 inline-input" type="password"
           name="password" id="password"
           value="<?=$password?>">
</div>
<div class="field-box">
    <label><?= LABEL_CONFIRM_PASSWORD ?>:</label>
    <input class="span9 inline-input" type="password"
           name="confirmpass" id="confirmpass"
           value="<?=$confirmpass?>">
</div>

<div class="span12 field-box">
    <label><?= LABEL_COUNTRY ?>:</label>

    <div class="ui-select span6">
        <select class="span5 inline-input" name="country_id"
                id="user_country" onclick="GetStates('state')">
            <option value="">Select Country</option>
            <?php
            foreach ($get_countries as $key => $countries) {
                ?>
                <option <?php if ($countries['country_id'] == $country_id) {
                    echo "selected";
                    //echo "selected='selected'";
                } ?>
                        value="<?php echo $countries['country_id'] ?>"><?php echo $countries['country'] ?></option>
                <?php
            }
            ?>
        </select>
    </div>
</div>
<div class="span12 field-box">
    <label><?= LABEL_STATE ?>:</label>
    <input class="span9" type="text"
           name="state" id="state"
           value="<?=$state?>">
</div>
<div class="span12 field-box">
    <label><?= LABEL_CITY ?>:</label>
    <input class="span9" type="text"
           name="city" id="city"
           value="<?=$city?>">
</div>
<div class="span12 field-box">
    <label><?= LABEL_ZIPCODE ?>:</label>
    <input class="span9" type="text"
           name="zip" id="zip"
           value="<?=$zip?>">
</div>
<div class="span12 field-box">
    <label><?= LABEL_ADDRESS ?>:</label>
    <input class="span9" type="text"
           name="address" id="address"
           value="<?=$address?>">
</div>
<div class="span12 field-box">
    <label><?= LABEL_PHONE ?>:</label>
    <input class="span9" type="text"
           name="phone" id="phone"
           value="<?=$phone?>">
</div>
<div class="span12 field-box">
    <label><?= LABEL_CELLPHONE ?>:</label>
    <input class="span9" type="text"
           name="cellphone" id="cellphone"
           value="<?=$cellphone?>">
</div>
