<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<!--<script type="text/javascript" src="<?php/*= ASSETS_PATH */?>js/jquery-min.js"></script>-->

<!--<script src="<?php/*= ASSETS_PATH */?>js/popup/popup.js"></script>-->
<script type="text/javascript">

    (function ($) {
        $(function () {
            $('.edit_notification').bind('click', function (e) {
                e.preventDefault();
                var notification_id = $(this).attr('id');
                $('.loader-save-holder').show();
                url = '<?=$vObj->getURL("configuration/manage_notification")?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: "notification_id="+notification_id,
                    success: function (data) {
                        $('#notification_table').hide();
                        $('#notification_area').html(data);
                        $('.loader-save-holder').hide();
                    }
                });
            });
        });
    })(jQuery);
</script>
<!-- POP-UP -->
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }
</style>
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b">
                <h5 class="resume-title"><?=LABEL_MANAGE_SYSTEM_CONFIGRUARTION?></h5>
            </div>
            <ul class="nav nav-tabs">
                        <li ><a href="<?= $vObj->getURL('configuration/system_variables') ?>">System Variables</a></li>
                        <li class="active"><a href="<?= $vObj->getURL('configuration/notification_templates') ?>">Notification Templates</a></li>
                       </ul>
                <div class="abc" style="display: none;">
                    <i class=""></i><!--Please select any record-->
                </div>

               <!-- <div class="alert alert-success" style="display: none">
                    <i class="icon-ok-sign"></i> Successfully record added
                </div>
-->
            <div id="notification_area">
                <table class="table table-hover" id="notification_table">
                    <thead>
                    <tr>
                        <th style="width: 40px;"><a href="javascript:void(0)">ID</a></th>
                        <th style="width: 400px;"><span class="line"></span><a href="javascript:void(0)">Title</a></th>
                        <th style="width: 400px;"><span class="line"></span><a href="javascript:void(0)">Use</a></th>
                        <th><span class="line"></span><a href="javascript:void(0)">Subject</a></th>
                        <th style="width: 40px;"><span class="line"></span></th>
                    </tr>
                    </thead>
                    <tbody class="all_notifications">
                    <?php
                    foreach ($get_notifications as $k => $v) {
                        ?>
                        <tr>
                            <td><?= $v['notification_id'] ?></td>
                            <td><?= $v['title'] ?></td>
                            <td><?= stripslashes($v['description']) ?></td>
                            <td><?= stripslashes($v['subject']) ?></td>
                            <td><a class="edit_notification" id="<?= $v['template_id'] ?>" href="javascript:void (0);">Edit</a></td>
                        </tr>
                    <?php
                    }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- end main container -->
<!--<div id="element_to_pop_up">
</div>-->

<!-- Modal -->
<!--<div  style="display: none;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
           <div class="global-popup_holder global-popup ">
               <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="global-popup_body popup-modal-body">
                    ...
                </div>
                <div class="global-popup_footer">
                    <button type="button" class="btn btn-flat white email_close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>--><!-- /.modal -->
<!-- end main container -->
