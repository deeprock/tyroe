<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<meta HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=UTF-8" xmlns="http://www.w3.org/1999/html">
<!--<script type="text/javascript" src="<?php/*= ASSETS_PATH */?>js/tinymce/jquery-1.9.0.min.js"></script>-->
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    function save_notification(){
        var title = $('#title').val();
        var subject = $('#subject').val();
        var description = $('#description').val();
        var edit_id =$('#edit_id').val();
        var template = tinyMCE.get('template');
        template = template.getContent();
        console.log(template);
        if(title == "")
        {
            $('#title').css({ border: '1px solid red'});
            $('#title').focus();
            return;
        }
        else
        {
            $('#title').removeAttr("style");
        }
        if(subject == "")
        {
            $('#subject').css({ border: '1px solid red'});
            $('#subject').focus();
            return;
        }
        else
        {
            $('#subject').removeAttr("style");
        }
        var data = {"title" : title,"description" : description , "template" : template , "subject" : subject , "edit_id" : edit_id};

        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("configuration/SaveNotification")?>',
            dataType: "json",
            success: function (data) {
                if(data.success){

                    $(".abc").show();
                    $(".abc").addClass("alert alert-success");
                    $(".alert-success").html('<i class="icon-ok-sign"></i>'+data.success_message);
                    setTimeout(function () {
                        location.reload();
                    }, 2500);
                }
            }
        });
    }
</script>

<div class="row-fluid form-wrapper">
    <div class="span12">
        <div class="container-fluid">

            <div class="span4 default-header bottom-margin headerdiv">
                <h4><?= LABEL_EDIT_NOTIFICATION_TEMPLATE; ?></h4>
            </div>
            <div class="field-box">
                <p><span class="span2" style="line-height: 43px;">Special Variables:</span>
                                        <div class="control-group span8" style="margin-left:0;">
                                            <div class="controls">
                                                [$url_tyroe] = to link something with tyroe.com<br>
                                                <?php
                                                switch($call){
                                                    case 'applyforjob':
                                                ?>
                                                        [$job_title] = For job title<br>
                                                        [$company_name] = For company name<br>
                                                        [$url_hireme] = to link something to the filtered opening page showing single job
                                                <?php
                                                        break;
                                                    case 'invitedforjob':
                                                ?>
                                                        [$job_title] = For job title<br>
                                                        [$company_name] = For company name<br>
                                                        [$url_viewinvitation] = to link something to the filtered opening page showing single job
                                                <?php
                                                        break;
                                                    case 'shortlistforjob':
                                                ?>
                                                        [$job_title] = For job title<br>
                                                        [$company_name] = For company name<br>
                                                        [$url_viewshortlistjob] = to link something to the filtered opening page showing single job
                                                <?php
                                                        break;
                                                    case 'gotreview';
                                                ?>
                                                        [$reviewer_job_title] = for reviewer job title<br>
                                                        [$reviewer_name] = for reviewer full name<br>
                                                        [$reviewer_company_name]= for reviewer company name <br>
                                                        [$job_title] = For job title<br>
                                                        [$url_viewfeedback] = to link something to the filtered feedback page showing single review

                                                <?php
                                                        break;
                                                    case 'profileviewed';
                                                ?>
                                                        [$viewer_job_title] = for viewer job title <br>
                                                        [$viewer_name] = for viewer name<br>
                                                        [$viewer_company_name] = for viewer company name
                                                <?php
                                                        break;
                                                    case 'gotfavourited';
                                                ?>
                                                        [$performer_job_title] = for performer job title <br>
                                                        [$performer_name] = for performer name<br>
                                                        [$performer_company_name] = for performer company name
                                                        <?php
                                                        break;
                                                    case 'receiverecommend';
                                                ?>
                                                        [$referrer_name] = for referrer name<br>
                                                        [$referrer_company] = for referrer company name<br>
                                                        [$url_approve_recommendation] = to link something to the recommendation approval section of profile
                                                <?php
                                                        break;
                                                    case 'acceptinvite';
                                                ?>
                                                        [$tyroe_name] = for tyroe name<br>
                                                        [$job_title] = for current accepted job title<br>
                                                        [$url_viewopening] = to link something to the job opening dashboard of the accepted job
                                                <?php
                                                        break;
                                                    case 'rejectinvite';
                                                ?>
                                                        [$tyroe_name] = for tyroe name<br>
                                                        [$job_title] = for current rejected job title<br>
                                                        [$url_search] = to link something to the tyroe search page
                                                <?php
                                                        break;
                                                    case 'jobcreated';
                                                ?>
                                                        [$job_title] = for current created job title<br>
                                                        [$url_openingdashboard] = to link something to the current created job dashboard page
                                                <?php
                                                        break;
                                                    case 'revieweradded';
                                                ?>
                                                        [$studio_admin_name] = for studio admin name<br>
                                                        [$studio_admin_company_name] = for studio admin company's name<br>
                                                        [$job_title] = for job title of the job in which reviewer is added <br>
                                                        [$url_openingdashboard] = to link something to the job dashboard page in which reviewer is added
                                                <?php
                                                        break;
                                                    case 'studioshortlist';
                                                ?>
                                                        [$job_title] = for job title<br>
                                                        [$company_name] = for company name<br>
                                                        [$studio_admin_name] = for studio admin name<br>
                                                        [$tyroe_name] = for tyroe name<br>
                                                        [$url_viewshortlisted] = to link something to the job dashboard page in which tyroe is shortlisted
                                                <?php
                                                        break;
                                                    case 'studioinvitetyroe';
                                                ?>
                                                        [$job_title] = for job title<br>
                                                        [$studio_admin_name] = for studio admin name<br>
                                                        [$url_invitereviewer] = to link something to the registration page
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </p>
                <br clear="all">
                <p><span class="span2"><?= LABEL_NOTIFICATION_TITLE ?>:</span>
                    <input class="span8" type="text"
                           name="title" id="title"
                           value="<?= stripslashes($selected_notification['title']) ?>">
                </p>
                <br clear="all">
                <p><span class="span2"><?= LABEL_NOTIFICATION_DESCRIPTION ?>:</span>
                    <textarea name="description" id="description"
                              class="span8"><?=stripslashes($selected_notification['description']) ?></textarea>
                </p>
                <br clear="all">
                <p>
                    <span class="span2">Subject:</span>
                    <textarea name="subject" id="subject"
                                                  class="span8"><?=stripslashes($selected_notification['subject']) ?></textarea>
                </p>
                <br clear="all">

                <p><span class="span2"><?= LABEL_NOTIFICATION_EMAILTEMPLATE ?>:</span>
                <div class="control-group span8" style="margin-left:0;">
                    <div class="controls">
                        <textarea name="template" id="template" class="span12 small_editor">
                            <?= stripslashes(htmlspecialchars_decode($selected_notification['template'])); ?>
                        </textarea>
                    </div>
                </div>
                </p>
                <br clear="all">
                <!-- <div class="control-group">
                    <span class="4"><?php/*=LABEL_NOTIFICATION_EMAILTEMPLATE*/?></span>
                    <div class="controls" >
                    <textarea name="template" id="template" class="span8 ckeditor m-wrap">
                                                                <?php/*= htmlspecialchars_decode($selected_notification['template']); */?></textarea>
                    </div>
                </div>-->
                <!--<p><span class="span4" style="line-height: 46px;"><?php/*= LABEL_NOTIFICATION_SOCIALTEMPLATE */?>:</span>
                <div class="control-group span8" style="margin-left:0;">
                    <div class="controls">
                        <textarea name="social_template" id="social_template" class="span8 ckeditor" rows="6">
                            <?php/*= htmlspecialchars_decode($selected_notification['social_template']);*/?>
                        </textarea>
                    </div>
                </div>
                </p>-->
                <input type="hidden" name="edit_id" id="edit_id" value="<?= $selected_notification['template_id'] ?>"/>
                <br>
                <div class="abc" style="display: none;">
                    <i class=""></i><!--Please select any record-->
                </div>
                <input type="button" name="submit" class="btn-flat success pull-right" style="margin-right: 250px;" value="<?= LABEL_SAVE ?>"
                       onclick="save_notification()"/>
                <a href="<?=$vObj->getURL("configuration/notification_templates")?>" class="btn-flat pull-right" id="backtonotification" style="margin-right: 50px;">Back</a>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        $('.loader-save-holder').show();
        tinymce.init({
            selector: "#template",
            theme: "modern",
            height: 250,
            plugins: [
                 "advlist autolink link image lists charmap  preview hr anchor",
                 "visualblocks visualchars code fullscreen insertdatetime nonbreaking",
                 "save table contextmenu directionality template paste textcolor"
           ],
           /*content_css: "css/content.css",*/
           toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
           style_formats: [
                {title: 'Bold text', inline: 'b'},
                {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                {title: 'Example 1', inline: 'span', classes: 'example1'},
                {title: 'Example 2', inline: 'span', classes: 'example2'},
                {title: 'Table styles'},
                {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
            ]
         });
        $('.loader-save-holder').hide();
</script>