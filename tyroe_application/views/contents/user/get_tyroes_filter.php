<?php
if(!empty($data)){
    foreach ($data as $k => $v) {
        $featured ="";
        if($v['role_id']==1 || $v['role_id']==4){
            $featured= 'N/A';
        }elseif($v['publish_account'] == 0){
            $featured = "Not Published";
        }elseif($v['featured_status']=='1'){
            $featured = '<i class="icon-star" style="color: #f20;"></i>';
        }elseif($v['featured_status']=='0'){
            $featured = '<i class="icon-star-empty" style="color: #f20;"></i>';
        }elseif($v['featured_status']=='-1'){
            $featured = '<i class="icon-minus-sign" style="color:#D5D5D5;"></i>';
        }
        $group ="";
        if($v['role_id']==1){
            $group = "Admin";
        }elseif($v['role_id']==2){
            $group = "Tyroe";
        }elseif($v['role_id']==4){
            $group = "Reviewer";
        }elseif($v['role_id']==5){
            $group  = "pro";
        }
        ?>
        <tr  id="tyroe_id_<?=$v['user_id']?>">
            <td align="center"><input type="checkbox" name="bulk_check[]" class="tyroe_check"
                                      value="<?= $v['user_id'] ?>"></td>
            <td><?= $v['user_id'] ?></td>
            <td><p class="rt-name"><?= $v['firstname']." ".$v['lastname'] ?></p></td>
            <td><?=$featured?></td>
            <td><?= $group ?></td>
            <td><a href="mailto:<?= $v['email'] ?>"><?= $v['email'] ?></a></td>
            <td><?= date('M j,Y',$v['created_timestamp']) ?></td>
            <td><?php if($v['visit_time']=="") echo "NOT AVAILABLE"; else echo date('M j,Y',$v['visit_time']);?></td>
            <td class="status_td">
                <?php if($v['status_sl'] == 0){?>
                    <span class="label label-info rt-pending">Pending</span>
                <?php }else{ ?>
                    <span class="label label-success">Active</span>
                <?php } ?>
            </td>
            <td><a href="javascript:void(0)" class="btn_pop" id="<?= $v['user_id'] ?>">edit</a> <a href="javascript:void(0)" class="user_delete" id="<?= $v['user_id']?>">delete</a> </td>
        </tr>
    <?php
    }
    ?>
<?php } else { ?>
    <tr style="text-align: center;"><td colspan="9"><strong>No match found</strong></td></tr>
<?php } ?>
<!--do not remove the following sing-->
<!--|||-->
<form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                         method="post" action="<?= $vObj->getURL('user/listing')?>">
               <div class="span12 pagination"><?= $pagination ?></div>
               <input type="hidden" value="<?= $page ?>" name="page">
                <input type="hidden" value="<?=$filter_type?>" name="filter_type">
                <input type="hidden" value="<?=$username?>" name="username">
                <input type="hidden" value="<?=$sorting?>" name="sorting">
                <input type="hidden" value="<?=$sort_column?>" name="sort_column">
                <input type="hidden" value="<?=$sort_type?>" name="sort_type">
</form>
