<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
?>
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)
    }

</style>
<!-- POP-UP -->
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    var delete_id = 0;
    var delete_type = 'single';
    var user_obj;
    (function ($) {
        // DOM Ready
        $(function () {

            $("body").delegate('.btn_pop', 'click', function (e) {
                $('.loader-save-holder').show();
                var item_id = this.id;
                var url = '<?= $vObj->getURL("user/form/") ?>';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: "item_id=" + item_id,
                    success: function (data) {
                        $('.loader-save-holder').hide();
                        if (data) {
                            var jqObj = jQuery(data);
                            $('.modal-title').html(jqObj.find(".headerdiv").html());
                            jqObj.find(".headerdiv").remove();
                            var getfunc = jqObj.find(".btn-glow").attr("onclick");
                            $('.btn-save').attr("onclick", getfunc);
                            jqObj.find(".btn-glow").remove();
                            $('.popup-modal-body').html(jqObj);
                            $('#myModal').modal('show');
                            $('body').addClass('modal-open');
                            $('body').css({"overflow": "hidden"});
                        }
                    }
                });
            });
            $("body").delegate('.user_delete', 'click', function (e) {
                user_obj = $(this);
                delete_id = this.id;
                $('#myModal_delete_user').modal('show');
            });
            $("body").delegate('#btn_delete_studio', 'click', function (e) {
                $('#delete-studio-error-message').hide();
                if ($.trim($('#delete_confirm_text').val()) == '') {
                    $('#delete-studio-error-message').html("Please enter DELETE text").show();
                    return false;
                }

                if ($.trim($('#delete_confirm_text').val()) != 'DELETE') {
                    $('#delete-studio-error-message').html("Please enter correct text").show();
                    return false;
                }

                $('.loader-save-holder').show();
                e.preventDefault();
                var obj = $(this);
                if (delete_type == 'single') {
                    var url = '<?= $vObj->getURL("studios/delete_studio") ?>';
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: "delete_id=" + delete_id,
                        dataType: "json",
                        success: function (data) {
                            if (data.success)
                            {
                                $(".abc").show();
                                $(".abc").addClass("alert alert-success");
                                $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                                user_obj.parents('tr').remove();
                                setTimeout(function () {
                                    $(".abc").fadeOut('1500');
                                }, 3000);
                            } else {
                                $(".abc").show();
                                $(".abc").addClass("alert alert-danger");
                                $(".alert-danger").html('<i class="icon-remove-sign"></i>' + "Fail to delete Studio");
                                setTimeout(function () {
                                    $(".abc").fadeOut('1500');
                                }, 3000);
                            }
                            $('.loader-save-holder').hide();
                        }

                    });
                } else {
                    $('.loader-save-holder').show();
                    var url = '<?= $vObj->getURL("studios/bulkaction") ?>';
                    $.ajax({
                        type: "POST",
                        data: delete_id,
                        url: url,
                        dataType: "json",
                        success: function (data) {
                            $.each(data.json_idz, function (key, val) {
                                $('#tyroe_id_' + val).fadeOut(700);
                            });
                            $(".abc").show();
                            $(".abc").addClass("alert alert-success");
                            $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                            setTimeout(function () {
                                $(".abc").fadeOut('1500');
                            }, 2000);
                            $('.loader-save-holder').hide();
                        }
                    });
                }
                $('#myModal_delete_user').modal('hide');
                $('#delete_confirm_text').val('');
            });
            $('#username_filter').donetyping(function () {
                $('.loader-save-holder').show();
                var search_name = $("#username_filter").val();
                var action = $('#filter_tyroes').val();
                $.ajax({
                    type: "POST",
                    data: "filter_type=" + action + "&bool=" + true + "&username=" + search_name,
                    url: '<?= $vObj->getURL("user/listing") ?>',
                    success: function (data) {

                        var alldata = data;
                        var data_arr = alldata.split('<!--|||-->');
                        $("tbody.all_tyroes").html(data_arr[0]);
                        $('#footer_pagination').html(data_arr[1]);
                        $('.loader-save-holder').hide();
                    }
                });
                return false;
            }, 500);
            $('#bulkaction').on('click', function () {
                var delete_ids = $('.bulk_form').serialize();
                if (delete_ids == "")
                {
                    alert("Please select a record first");
                } else
                {
                    delete_type = 'bulk';
                    delete_id = delete_ids;
                    $('#myModal_delete_user').modal('show');
                }
            });
            $('#filter_tyroes').on('change', function () {
                var action = $(this).val();
                var bool = true;
                var search_name = $("#username_filter").val();
                if (action == "")
                {
                    return false;
                }
                else
                {
                    $('.loader-save-holder').show();
                    var url = '<?= $vObj->getURL("user/listing") ?>';
                    $.ajax({
                        type: "POST",
                        data: "filter_type=" + action + "&bool=" + true + "&username=" + search_name,
                        url: url,
                        success: function (data) {
                            var alldata = data;
                            var data_arr = alldata.split('<!--|||-->');
                            $("tbody.all_tyroes").html(data_arr[0]);
                            $('#footer_pagination').html(data_arr[1]);
                            $('.loader-save-holder').hide();
                        }
                    });
                }
            });
            $('.sorter').on('click', function () {
                $('.loader-save-holder').show();
                var sort_col = $(this).attr('data-column');
                var sort_type = $(this).attr('data-stype');
                var search_name = $("#username_filter").val();
                var url = '<?= $vObj->getURL("user/listing") ?>';
                var obj = $(this);
                $.ajax({
                    type: "POST",
                    data: "sort_column=" + sort_col + "&sort_type=" + sort_type + "&sorting=" + true + "&bool=" + true + "&username=" + search_name,
                    url: url,
                    success: function (data) {
                        var alldata = data;
                        var data_arr = alldata.split('<!--|||-->');
                        $("tbody.all_tyroes").html(data_arr[0]);
                        $('#footer_pagination').html(data_arr[1]);
                        if (sort_type == "ASC")
                        {
                            obj.attr('data-stype', 'DESC');
                        }
                        else
                        {
                            obj.attr('data-stype', 'ASC');
                        }
                        $('.loader-save-holder').hide();
                    }
                });
            });
        });

    })(jQuery);
</script>
<!-- POP-UP -->
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b tr-mr">
                <div id="admin_users_top_header" class="span9">
                    <h5 class="resume-title"><?= LABEL_USER_MANAGER ?></h5>
                    <input class="span4 pull-left search tr-search" placeholder="type user's name" type="text" name="username_filter" id="username_filter" value="<?= $username ?>">
                    <select name="filter_tyroes" class="filter_tyroes margin-default btn" id="filter_tyroes">
                        <option value="" disabled selected style="display:none; ">Filter Users</option>
                        <option <?php if ($filter_type == 'alphabetical') echo 'Selected="selected"'; ?> value="alphabetical">Alphabetical</option>
                        <option <?php if ($filter_type == 'mostrecent') echo 'Selected="selected"'; ?> value="mostrecent">Most Recent</option>
                    </select>
                </div>
                <div class="span3 topRightHead">
                    <a href="javascript:void(0)" class="pull-left selectedDel" id="bulkaction">Delete Selected</a>
                    <span class="left-margin">
                        <a href="javascript:void(0)" class="btn-flat primary go-btn pull-right btn_pop no-margin custom-btn-color"
                           id="new">+ <?= LABEL_TOOLBAR_NEW ?> User</a>
                    </span>
                </div>
            </div>
            <div class="row-fluid show-grid">
                <?php
                echo $this->session->userdata('success_message');
                $this->session->unset_userdata('success_message');
                ?>
            </div>
            <!--  /*====01-07-2014====*/-->
            <script type="text/javascript">
                $(document).ready(function () {
                    setTimeout(function () {
                        $(".alert-success").hide();
                    }, 2000);
                });</script>
            <?php
            $msg = $this->session->userdata('status_msg');
            $this->session->unset_userdata('status_msg');
            if (isset($msg) && $msg != '') {
                ?>
                <div class="alert alert-success">
                    <i class="icon-ok-sign"></i><?= $msg; ?>
                </div>
<?php } ?>
            <!--  /*====01-07-2014====*/-->
            <div class="abc">

            </div>
            <!--<div class="row-fluid show-grid">
                       <div class="span8">
                           <span>Search:</span>
                           <input class="span2" type="text" name="search" id="search" value="<?php /* =$search */ ?>">
                           <span>First name:</span>
                           <input type="checkbox" class="user" name="firstname" <?php /* =$firstname?"checked='checked'":"" */ ?> />
                           <span>Last name:</span>
                           <input type="checkbox" class="user" name="lastname" <?php /* =$lastname?"checked='checked'":"" */ ?> />
                           <span><a class="btn-flat primary go-btn" href="javascript:void(0)" onclick="listItemTask('','<?php /* =$vObj->getUrl($controller_name."/listing") */ ?>',this)"><?php /* =LABEL_TOOLBAR_GO */ ?></a></span>
                       </div>
                   </div>-->
            <form class="bulk_form inline-input" _lpchecked="1" enctype="multipart/form-data" name="user_form"
                  method="post">
                <table class="table table-hover tr-thead tr-pd">
                    <thead>
                        <tr>
                            <th><div class="position-relative"><input type="checkbox" name="tyroes_master_check" class="studio_master_check" onchange="multiplechecks($(this), '.tyroe_check')
                                            ;"></div></th>
                    <th><div class="position-relative"><a href="javascript:void(0)" class="sorter" data-column="u.user_id" data-stype="ASC">ID</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="u.firstname" data-stype="ASC">Name</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="f.featured_status" data-stype="ASC">Featured</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="u.role_id" data-stype="ASC">Group</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="u.email" data-stype="ASC">Email</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="u.created_timestamp" data-stype="ASC">Signed Up</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="tpv.visit_time" data-stype="ASC">Last Visit</a></div></th>
                    <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="s.status" data-stype="ASC">Status</a></div></th>
                    <th><div class="position-relative"><span class="line"></span></th>
                        </tr>
                        </thead>
                        <tbody class="all_tyroes team-tr">
                            <?php
                            foreach ($data as $k => $v) {
                                $featured = "";
                                if ($v['role_id'] == '1' || $v['role_id'] == '4' || $v['role_id'] == '3') {
                                    $featured = '<span class="rt-na">N/A</span>';
                                } elseif ($v['publish_account'] == 0) {
                                    $featured = '<span class="rt-na">Not Published</span>';
                                } elseif ($v['featured_status'] == '1') {
                                    $featured = '<i class="icon-star" style="color: #f5806c;font-size: 16px;"></i>';
                                } elseif ($v['featured_status'] == '0' && $v['publish_account'] == '1') {
                                    $featured = '<i class="icon-star-empty" style="color: #f5806c;font-size: 16px;"></i>';
                                } elseif ($v['featured_status'] == '-1') {
                                    $featured = '<i class="icon-minus-sign" style="color:#D5D5D5;font-size: 16px;"></i>';
                                }
                                $group = "";
                                if ($v['role_id'] == 1) {
                                    $group = "Admin";
                                } elseif ($v['role_id'] == 2) {
                                    $group = "Tyroe";
                                } elseif ($v['role_id'] == 3) {
                                    $group = "Studio Administrator";
                                } elseif ($v['role_id'] == 4) {
                                    $group = "Reviewer";
                                } elseif ($v['role_id'] == 5) {
                                    $group = "pro";
                                }
                                ?>
                                <tr  id="tyroe_id_<?= $v['user_id'] ?>">
                                    <td align="center"><input type="checkbox" name="bulk_check[]" class="tyroe_check"
                                                              value="<?= $v['user_id'] ?>"></td>
                                    <td><?= $v['user_id'] ?></td>
                                    <td><p class="rt-name"><?= $v['firstname'] . " " . $v['lastname'] ?></p></td>
                                    <td><?= $featured ?></td>
                                    <td><?= $group ?></td>
                                    <td><a href="mailto:<?= $v['email'] ?>"><?= $v['email'] ?></a></td>
                                    <td><?= date('M j,Y', $v['created_timestamp']) ?></td>
                                    <td><?php
                                        if ($v['visit_time'] == "")
                                            echo "Not Available";
                                        else
                                            echo date('M j,Y', $v['visit_time']);
                                        ?></td>
                                    <td class="status_td">
                                        <?php if ($v['status_sl'] == 0) { ?>
                                            <span class="label label-info rt-pending">Pending</span>
                                        <?php } else { ?>
                                            <span class="label label-success">Active</span>
    <?php } ?>
                                    </td>
                                    <td><a href="javascript:void(0)" class="btn_pop" id="<?= $v['user_id'] ?>">Edit</a> <a href="javascript:void(0)" class="user_delete" id="<?= $v['user_id'] ?>">Delete</a> </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <!--<tfoot class="table-cus-footer">
                        <tr class="pagination">
                            <td colspan="8"><?php /* = $pagination */ ?></td>
                        </tr>
                        </tfoot>-->
                </table>
            </form>
            <div id="footer_pagination">
                <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                      method="post" action="<?= $vObj->getURL('user/listing') ?>">
                    <div class="span12 pagination"><?= $pagination ?></div>
                    <input type="hidden" value="<?= $page ?>" name="page">
                    <input type="hidden" value="<?= $filter_type ?>" name="filter_type">
                    <input type="hidden" value="<?= $username ?>" name="username">
                    <input type="hidden" value="<?= $sorting ?>" name="sorting">
                    <input type="hidden" value="<?= $sort_column ?>" name="sort_column">
                    <input type="hidden" value="<?= $sort_type ?>" name="sort_type">
                </form>
            </div>
            <a href="#myModal_delete_user" style="display: none;" id="open_delete_modal">Delete Modal</a>
        </div>
    </div>
</div>

<!-- Modal -->
<div  style="display: none;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close top30px" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="global-popup_body popup-modal-body padding0px padding-left-15px"></div>
                <div class="clearfix"></div>
                <div class="global-popup_footer">
                    <button type="button" class="btn-flat white white-close pull-right" data-dismiss="modal">Close</button>
                    <button type="button" class="btn-flat primary btn-save pull-right margin-right-5px">Save changes</button>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div><!-- /.modal -->
<div id="element_to_pop_up">
</div>
<div style="display: none;" class="modal myModal_delete_studio_2" id="myModal_delete_user" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm Account Deletion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                    <i class="icon-warning-sign"></i>
                    <div class="confirmation-text">
                        <span class="l1">Are you sure ?</span><br/>
                        <span class="l2">All account data will be permanently deleted. This can't be undone.</span><br/>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <p class="delete-message-2">Type DELETE to confirm</p>
                <p>
                    <textarea class="delete-confirm-text" id="delete_confirm_text"></textarea>
                </p>
                <label class="delete-studio-error-message" id="delete-studio-error-message">Error message</label>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-glow btn-cancel-delete-studio" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-glow primary btn-delete-studio" id="btn_delete_studio">Delete User</button>
            </div>
        </div>
    </div>
</div>
<!-- end main container -->




