<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<script type="text/javascript">
    $(document).ready(function(){
        $('alert-success').removeAttr('style');
    });
</script>
<!-- POP-UP -->

<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b">
                <h5 class="resume-title"><?= LABEL_MANAGE_SYSTEM_CONFIGRUARTION ?></h5>
            </div>
            <ul class="nav nav-tabs">
                <li class="active"><a href="<?= $vObj->getURL('configuration/system_variables') ?>">System Variables</a>
                </li>
                <li><a href="<?= $vObj->getURL('configuration/notification_templates') ?>">Notification Templates</a>
                </li>

            </ul>
            <div class="row-fluid show-grid">
                <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="system_variables"
                      method="post" action="<?= $vObj->getURL('configuration/system_variables') ?>"
                      id="system_variables">

                    <span class="pull-right button-center">
                    <input type="submit" name="submit" class="btn-glow primary"
                           value="<?= LABEL_SAVE ?>">
                    </span>

                    <div class="span7">
                        <div class="container-fluid">
                            <p>
                                <script type="text/javascript">
                                    setTimeout(function(){
                                        $('.alert-success').css('display', 'none')
                                    }, 2000)
                                </script>
                                <?php
                                $msg = $this->session->userdata('status_msg');
                                $this->session->unset_userdata('status_msg');
                                if (isset($msg) && $msg != '') {
                                ?>
                            <div class="alert alert-success">
                                <i class="icon-ok-sign"></i><?= $msg; ?>
                            </div>
                            <?php } ?>
                            </p>

                            <div class="field-box">
                                <?php

                                foreach ($get_variable as $system_variable => $val) {
                                    if($val['variable_field_type'] == 't'){
                                    ?>
                                    <p><span class="span4"><?= constant($val['variable_title']); ?>:</span>
                                        <input class="span4" type="text"
                                               name="<?= $val['variable_name']; ?>" id="<?= $val['variable_name']; ?>"
                                               value="<?= $val['variable_value']; ?>">
                                    </p>
                                    <?php
                                    } else if($val['variable_field_type'] == 'r') {
                                    ?>
                                    <p><span class="span4"><?= constant($val['variable_title']); ?>:</span>
                                        <input class="span1" type="radio"
                                               name="<?= $val['variable_name']; ?>" id="<?= $val['variable_name']; ?>"
                                               value="1" <?php if($val['variable_value']==1)echo "checked";?>>Enable
                                        <input class="span1" type="radio"
                                               name="<?= $val['variable_name']; ?>" id="<?= $val['variable_name']; ?>"
                                               value="0" <?php if($val['variable_value']==0)echo "checked";?>>Disable
                                    </p>
                                    <?php
                                    }
                                }

                                ?>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div id="element_to_pop_up">
</div>