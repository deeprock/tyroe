<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {


        $(function () {
            $('.btn_pop').bind('click', function (e) {
                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired

                var param2 = this.text;
                var sel_id = $('input[name=rd_subscription]:checked').val();

                if (sel_id == "undefined" || sel_id == "") {
                    alert("Please select any record for edit");
                    //return false;
                }
                var identifier = this.id;
                var data = "";
                var param = "";
                var classes = "";
                /* if(identifier == ''){
                     classes = $(this).attr("class").split(' ');
                     identifier = classes[1];
                     param = sel_id;
                 }*/
                param = sel_id + "/" + param2;
                if (identifier == 'subs_admin_form') {
                    var url = '<?=$vObj->getURL("subscription/NewSubscription/")?>' + param;
                }
                $.ajax({
                    type:'POST',
                    url:url,
                    data:data,
                    success:function (data) {
                        /* $('#element_to_pop_up').bPopup();
                         $('#element_to_pop_up').html(data);*/
                        $('#element_to_pop_up').html(data);
                        var x_axis = Math.floor(window.innerWidth / 2);
                        var popup_width = $('#element_to_pop_up').css('width')
                        var actual_width = x_axis - (parseInt(popup_width.replace('px', '')) / 2) - 40;
                        $('#element_to_pop_up').bPopup({position:[actual_width, 10]});
                        $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');

                    }
                })


            });

        });

    })(jQuery);
</script>
<!-- POP-UP -->
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

    .table-filter {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

</style>

<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b">
                <h5 class="resume-title">Manage Coupons</h5>
            </div>
            <div class="row-fluid show-grid">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="<?= $vObj->getURL('subscription') ?>">Subscriptions</a></li>
                    <li><a href="<?= $vObj->getURL('subscription/getpayment') ?>">Payments</a></li>
                    <li><a href="<?= $vObj->getURL('subscription/getcoupons') ?>">Coupons</a></li>
                </ul>
                Subscription functionality is coming soon.
                <!--<div class="row-fluid header z-margin-b">
                    <h5 class="resume-title">Manage Subscription</h5>
                </div>
                <div class="show-grid">
                    <div class="span2" style="float: right; margin-bottom:5px">
                       <span class=""> <a class="btn-flat primary go-btn pull-right btn_pop" id="subs_admin_form"
                                              href="javascript:void (0);">New</a>

                            <a class="btn-flat primary go-btn pull-right btn_pop" id="subs_admin_form"
                               href="javascript:void (0);">Edit</a>
                        </span>
                    </div>
                </div>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1" enctype="multipart/form-data" name="user_form"
                  method="post" action="<?php/*= $vObj->getURL('subscription/index') */?>">
                <div class="row-fluid show-grid">
                    <?php/*
                    echo $this->session->userdata('success_message');
                    $this->session->unset_userdata('success_message');
                    */?>
                </div>

                <table class="table">
                    <thead class="table-head">
                    <tr class="table-filter admin-searchbar custom-table-head">
                        <th></th>
                        <th></th>
                        <th><input class="span2" type="text" name="search" id="search" value="<?php/*= $search */?>"
                                   placeholder="Filter Name">
                        </th>
                        <th>
                            <div class="ui-select span6">
                                <select name="filter_type" id="filter_type" class="span2">
                                    <option value="">Filter Type</option>
                                    <option value="">Monthly</option>
                                    <option value="">Quarterly</option>
                                    <option value="">Annual</option>
                                </select>
                            </div>
                        </th>
                        <th>
                            <div class="ui-select span6">
                                <select name="filter_status" id="filter_status" class="span2">
                                    <option value="">Filter Status</option>
                                    <option value="">Active</option>
                                    <option value="">Suspended</option>
                                </select>
                            </div>
                        </th>
                        <th></th>
                        <th></th>
                        <th>
                            <div class="ui-select span6">
                                <select name="filter_method" id="filter_method" class="span2">
                                    <option value="">Filter Method</option>
                                    <option value="">Credit Card</option>
                                    <option value="">Paypal</option>
                                </select>
                            </div>
                        </th>
                        <th></th>
                    </tr>
                    <tr>
                        <th></th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('subscription_id','<?php/*= $new_flag */?>',this)">ID<?php/*= $sort_img_user_id */?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('username','<?php/*= $new_flag */?>',this)">Tyroe<?php/*= $sort_img_email */?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('username','<?php/*= $new_flag */?>',this)">Type<?php/*= $sort_img_firstname */?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('subscription_type','<?php/*= $new_flag */?>',this)">Status<?php/*= $sort_img_lastname */?></a>
                        </th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('role_id','<?php/*= $new_flag */?>',this)">Started<?php/*= $sort_img_role_id */?></a>
                        </th>
                        <th><a href="javascript:void(0)" onclick="ordering('role_id','<?php/*= $new_flag */?>',this)">Next
                            Payment<?php/*= $sort_img_role_id */?></a></th>
                        <th><a href="javascript:void(0)"
                               onclick="ordering('subscription_card_type','<?php/*= $new_flag */?>',this)">Method<?php/*= $sort_img_role_id */?></a>
                        </th>
                        <th><a href="javascript:void(0)" onclick="ordering('role_id','<?php/*= $new_flag */?>',this)">Use
                            Coupons?<?php/*= $sort_img_role_id */?></a></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
/*                    foreach ($data as $k => $v) {
                        if ($k % 2 == 0)
                            $class = "table-odd";
                        else
                            $class = "table-default-r";
                        */?>
                    <tr class="<?php/*= $class */?>">
                        <td><input type="radio" name="rd_subscription" id="rd_subscription"
                                   value="<?php/*= $v['subscription_id'] */?>"/></td>
                        <td><?php/*= $v['subscription_id'] */?></td>
                        <td><?php/*= $v['username'] */?></td>
                        <td><?php/*= $v['subscription_type'] */?></td>
                        <td><?php/*= $v['status_sl'] */?></td>
                        <td><?php/*= $v['started_date'] */?></td>
                        <td><?php/*= $v['next_payment'] */?></td>
                        <td><?php/*= $v['payment_type'] */?></td>
                        <td><img src="<?php/*= $v['use_coupon'] */?>" alt="" height="25px" width="25px"/></td>

                    </tr>
                        <?php
/*                    }
                    */?>

                    </tbody>
                    <tfoot class="table-footer">
                    <tr>
                        <td colspan="9"><?php/*= $pagination */?></td>
                    </tr>
                    </tfoot>
                </table>
                <input type="hidden" value="<?php/*= $item_id */?>" name="item_id">
                <input type="hidden" value="<?php/*= $page */?>" name="page">
                <input type="hidden" value="<?= $sort_by ?>" name="sort_by">
                <input type="hidden" value="<?= $flag ?>" name="flag">
            </form>-->
        </div>
    </div>
</div>
<div id="element_to_pop_up">
</div>
<!-- end main container -->




