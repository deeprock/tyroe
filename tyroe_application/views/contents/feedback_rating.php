<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/morris.min.js"></script>
<script type="text/javascript">
    $(function createknob() {
        $(".knob").knob();
        $(".knobs").removeClass("knobs");
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
        $(".search_by").on("click", function () {
            if ($(this).attr("data-value") == "2") {
                $("#feedback_duration").val("week");
            }
            else if ($(this).attr("data-value") == "3") {
                $("#feedback_duration").val("month");
            }
            else if ($(this).attr("data-value") == "1") {
                $("#feedback_duration").val("all");
            }
            FilterFeedback();

        });

        $('#feedback_keyword').donetyping(function (){
            FilterFeedback();
        },500);

        $("select").on("change", function () {
            var str = "";
            $("select option:selected").each(function () {
                str += $(this).text() + " ";
            });
            if ($(this).val() != "") {
                $(".activity_type_label").text(str);
            }
        });
//.change();

        /*Clear button */
        $("#clear_btn").click(function () {
            $(this).closest('form').find("input[type=text], textarea,select").val("");
            FilterFeedback();
        });

    });

    /*function FilterFeedback() {

        var feedback_type = $("#feedback_type").val();
        var feedback_keyword = $("#feedback_keyword").val();
        var feedback_duration = $("#feedback_duration").val();
        var data = "feedback_type=" + feedback_type + "&feedback_keyword=" + feedback_keyword + "&feedback_duration=" + feedback_duration;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("feedback/filter_feedback")?>',

            success: function (data) {
                alert(sdd);
                $(".no-filter").hide();
                $(".filter-result").html(data);
                $(".knob").knob();
                *//*if (data.success) {

                 $(".no-filter").hide();
                 $(".filter-result").html(data.success_message);
                 }
                 else {
                 $(".notification-box-message").css("color", "#b81900")
                 $(".notification-box-message").html(data.success_message);
                 $(".notification-box").show(100);
                 setTimeout(function () {
                 $(".notification-box").hide();
                 }, 5000);
                 }*//*
            }

        });
    }*/
</script>
<div style="display:none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="content">

<div class="container-fluid">


<!-- upper main stats -->
<?php
$this->load->view('upper_stats');
?>
<!-- end upper main stats -->
<div id="pad-wrapper" class="margin-top-1">

<div class="grid-wrapper">

<div class="row-fluid show-grid">

<!-- START LEFT COLUMN -->
<?php
$this->load->view('left_coloumn');
?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar padding-top-11">
    <div class="right-column" style="position:relative;">

        <!-- margin pointer -->
        <!-- <div class="row-fluid ">
             <div class="span4 default-header">
                 <h4>Feedback & Ratings</h4>
             </div>
         </div>-->
        <div class="span12">
            <h4>Reviews | <span class="activity_type_label">All</span></h4>

            <div class="row-fluid margin-top-8">

                <div class="container span4 pull-right activity-last">
                    <div class="field-box">
                        <div class="btn-group pull-left stream-view">
                            <button id="week" class="glow left search_by" data-value="1">All</button>
                            <button id="month" class="glow search_by" data-value="2">This Week</button>
                            <button class="glow right search_by" data-value="3">This Month</button>
                            <input type="hidden" id="feedback_duration" value="">
                        </div>
                    </div>
                </div>

                <div class="field-box span4 pull-right activity-keyword tab-margin">
                    <div class="span12 ctrls">
                        <input type="text" placeholder="Search input.." class="search span12 padding-1" id="feedback_keyword" value="<?=$feedback_keyword?>">
                    </div>
                </div>

                <div class="span4 pull-right activity-search tab-margin">
                    <div class="container span12">

                        <div class="field-box">
                            <div class="ui-select find">
                                <select class="span5 inline-input"
                                        id="feedback_type" onchange="FilterFeedback();">
                                    <option value="">Select Type</option>
                                    <?php
                                    foreach ($get_job_openings as $key => $jobs) {
                                        if($feedback_type == $jobs['job_id'])
                                        {
                                            $selected='selected="selected"';
                                        }
                                        else
                                        {
                                            $selected="";
                                        }
                                        ?>
                                        <option <?=$selected?>
                                            value="<?php echo $jobs['job_id'] ?>"><?php echo $jobs['job_title'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="clearfix"></div>

            </div>
        </div>
        <div class="filter-result"></div>

                                                        <div class="no-filter notes">
        <!--<form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="feedback"
              method="post" action="<?php/*= $vObj->getURL('exposure/feedback_rating') */?>">-->
            <?php
            if ($get_feedback == NULL ) {
                ?>
                <div class="row-fluid">
                    <div id="top" class="span12  margin-top-8 alert alert-info">
                        <i class="icon-exclamation-sign"></i>
                        There is no Feedback yet
                    </div>
                </div>
            <?php
            }else{
            //$ind=0;
            //foreach ($get_feedback as $feedback) {
            for ($ind = 0; $ind < count($get_feedback); $ind++) {
            ?>
                <div class="row-fluid">
                    <div class="span12 people margin-top-8" id="top">
                        <div class="row-fluid feedback-review">
                                <div class="span12">
                                    <?php
                                    $logo_url = LIVE_SITE_URL.'assets/img/default-avatar.png';
                                    if($get_feedback[$ind]['company_logo_image'] != ''){
                                        $logo_url = ASSETS_PATH.'uploads/company_logo/'.$get_feedback[$ind]['company_logo_image'];
                                    }
        
                                    ?>
                                    <div class="span2" id="avatar" style="height: 80px;width: 80px;">
                                        <img src="<?= $logo_url?>" class="company-logo">
                                    </div>
                                    <div class="span10">
                                        <span class="name c-magic company-name"><?php echo $get_feedback[$ind]['company_name']?></span>
                                        <div class="clearfix"></div>
                                        <div class="subtext-label job-title"><?php echo $get_feedback[$ind]['job_title']?><span class="job-start-date"><i class="fa icon-calendar"></i>&nbsp;&nbsp;<?php echo $get_feedback[$ind]['job_start_date']?></span></div>
                                        <span class="subtext-label job-location"><?php echo $get_feedback[$ind]['city'] . "," . $get_feedback[$ind]['country'] ?></span>
                                    </div>
                                </div>
                                <!-- sliders -->
                                <?php
                                    if($get_feedback[$ind]['moderated'] == 1){ ?>
                                <div class="span12" style="margin: 0px;">
                                    <div class="span2">&nbsp;</div>
                                    <div class="sliders span10 border-top">
                                        <div class="slider-label span3">Level : </div><div class="slider-text span9"><span class="btn-flat white"><?php echo $get_feedback[$ind]['level_name']?></span></div>
                                        <div class="slider-label span3">Technical : </div>
                                        <div class="slider-text span9">
                                            <div class="row" style="margin: 0px">
                                                <div class="span10">
                                                    <div class="slider slider-technical<?php echo $get_feedback[$ind]['review_id'] ?>" ></div>
                                                </div>
                                                <div class="span2 text-center">
                                                    <div class="slider_number"><?= $get_feedback[$ind]['technical'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-label span3">Creative : </div>
                                        <div class="slider-text span9">
                                            <div class="row" style="margin: 0px">
                                                <div class="span10">
                                            <div class="slider slider-creative<?php echo $get_feedback[$ind]['review_id'] ?>"></div>
                                                </div>
                                                <div class="span2 text-center">
                                                    <div class="slider_number"><?= $get_feedback[$ind]['creative'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-label span3">Impression : </div>
                                        <div class="slider-text span9">
                                            
                                            <div class="row" style="margin: 0px">
                                                <div class="span10">
                                                    <div class="slider slider-impression<?php echo $get_feedback[$ind]['review_id'] ?>"></div>
                                                </div>
                                                <div class="span2 text-center">
                                                    <div class="slider_number"><?= $get_feedback[$ind]['impression'] ?></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slider-label span3">Comment : </div>
                                        <div class="slider-text span9">
                                            <div class="jQueryClassReadmoreParagraphFunc" style="overflow: hidden"><span>
                                             <?php
                                                if($get_feedback[$ind]['annonymous_message'] == ""){
                                                    $feedback_comment = "feedback message not available";
                                                }else{
                                                    $feedback_comment = "<strong>". $get_feedback[$ind]['job_level'] . "</strong>- " . nl2br($get_feedback[$ind]['annonymous_message']);
                                                }

                                                 echo $feedback_comment;
                                             ?>
                                            </span></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="span12" style="margin: 0px;">
                                    <?php
                                    $title = '';
                                    $text = '';
                                    $avg = $get_feedback[$ind]['profile_score'];
                                    if($avg < 50){
                                        $title = 'Thirsty';
                                        $text = 'Skills not perfected, but an obvious strong desire to prove themselves.';
                                    }else if($avg >= 50 && $avg <= 79){
                                        $title = 'Pressed';
                                        $text = 'Hyper-focused on perfecting their skills and obsessively climbing the ranks.';
                                    }else if($avg >= 80){
                                        $title = 'Slayer';
                                        $text = 'Spectacular abilities with impressive skills. Ready for industry challenges.';
                                    }
                                    ?>
                                    <div class="span2">&nbsp;</div>
                                    <div class="span10 border-top" style="margin: 0px;margin-top: 15px;padding-top: 30px;">
                                        <div class="rate-badge-container">
                                            <div class="span9 rate-badge-content">
                                                <div class="badge-title"><?=$title?></div>
                                                <div class="badge-text"><?=$text?></div>
                                            </div>
                                            <div class="span3 rate-badge">
                                                <img src="<?=LIVE_SITE_URL.'assets/img/default-avatar.png'?>" class="img-circle rate-badge-logo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } else { ?>
                                <div class="span12" style="margin: 0px;">
                                    <div class="span2">&nbsp;</div>
                                    <div class="sliders span10 border-top">
                                        <div style="  border: 1px solid #edf2f7;padding: 5px;border-radius: 5px;font-size: 12px;">
                                            <b>Your Profile was reviewed.</b> The Studio has decided to make the feedback visible to staff only.
                                        </div>
                                    </div>
                                </div>
                        <?php } ?>
                                <!-- end sliders -->
                            <div class="row-fluid ui-elements">
                                <div class="span12 margin-top-4">
                                  
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        // jQuery UI Sliders
                        $(".slider-technical" +<?=$get_feedback[$ind]['review_id']?>).slider({
                            range: "min",
                            value: <?=$get_feedback[$ind]['technical']?>,
                            min: 1,
                            max: 100,
                            slide: function( event, ui ) {return false;}
                        });
                        $(".slider-creative" +<?=$get_feedback[$ind]['review_id']?>).slider({
                            range: "min",
                            value: <?=$get_feedback[$ind]['creative']?>,
                            min: 1,
                            max: 100,
                            slide: function( event, ui ) {return false;}
                        });
                        $(".slider-impression" +<?=$get_feedback[$ind]['review_id']?>).slider({
                            range: "min",
                            value: <?=$get_feedback[$ind]['impression']?>,
                            min: 1,
                            max: 100,
                            slide: function( event, ui ) {return false;}
                        });
                    });

                </script>
                <?php
                // $ind++;
            } ?>

              <?php
            }
            ?>
    </div>
        <!--pagination>-->
                    <div id="footer_pagination">
                        <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="stream"
                                              method="post" action="<?= $vObj->getURL('feedback') ?>">
                            <div class="span12 pagination"><?= $pagination ?></div>
                            <input type="hidden" value="<?= $page ?>" name="page">
                            <input type="hidden" value="<?= $feedback_type ?>" name="feedback_type">
                            <input type="hidden" value="<?= $feedback_keyword ?>" name="feedback_keyword">
                            <input type="hidden" value="<?= $feedback_duration ?>" name="feedback_duration">
                        </form>
                    </div>
                    <!--pagination end-->
   <!-- </form>-->
</div>


</div>
</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>
    <script type="text/javascript">
    
    $(document).ready(function(){
        $('.jQueryClassReadmoreParagraphFunc').each(function(){
            var getHeight = $(this).height();
            if(getHeight > 60){
                $(this).after('<a href="javascript:void(0)" class="jQueryClassReadmoreFunc">Read More</a>');
                $(this).css({"height":'60px'});
            }
        });
    });
    
    
    $(document).on('click','.jQueryClassReadmoreFunc',function(){
        var getHeight = $(this).parent().children('.jQueryClassReadmoreParagraphFunc').children('span').height();
        $(this).parent().children('.jQueryClassReadmoreParagraphFunc').animate({"height":getHeight+"px"});
        $(this).removeClass('jQueryClassReadmoreFunc');
        $(this).addClass('jQueryClassReadlessFunc');
        $(this).text('Read Less');
    });
    
    $(document).on('click','.jQueryClassReadlessFunc',function(){
        var getHeight = $(this).parent().children('.jQueryClassReadmoreParagraphFunc').height();
        if(getHeight > 60){
            $(this).parent().children('.jQueryClassReadmoreParagraphFunc').animate({"height":"60px"});
            $(this).removeClass('jQueryClassReadlessFunc');
            $(this).addClass('jQueryClassReadmoreFunc');
            $(this).text('Read More');
        }
    });
    
    </script>
<!-- end main container -->


<!-- build the charts -->
