<script type="text/javascript">
    function CurrentJob() {
        if ($('input[name=current_job]:checked').val()) {
            $("#job_end").attr('readonly', true);
            $("#job_end").css('background', "#d0dde9");
        }
        else {
            $("#job_end").attr('readonly', false);
            $("#job_end").css('background', "#fff");

        }

    }
    function AddExperience() {
        var company_name = $("#company_name").val();
        var job_title = $("#job_title").val();
        var current_job = $('input[name=current_job]:checked').val();
        var job_start = $("#job_start").val();
        var job_end = $("#job_end").val();
        var job_description = $("#job_description").val();
        var exp_id = $("#exp_id").val();

        if (job_title == '') {
            $('#job_title').css({ border: '1px solid red'});
            $('#job_title').focus();
            return false;
        } else {
            $('#job_title').removeAttr("style")
        }
        if (company_name == '') {
            $('#company_name').css({ border: '1px solid red'});
            $('#company_name').focus();
            return false;
        } else {
            $('#company_name').removeAttr("style")
        }
        if (job_start == '') {
            $('#job_start').css({ border: '1px solid red'});
            $('#job_start').focus();
            return false;
        } else {
            $('#job_start').removeAttr("style")
        }
        if (current_job != 'on') {
            if (job_end == '') {
                $('#job_end').css({ border: '1px solid red'});
                $('#job_end').focus();
                return false;
            } else {
                $('#job_end').removeAttr("style")
            }
        }


        if (current_job) {
            current_job = '1'
            job_end = ''
        }
        else {
            current_job = '0'
        }
        var data = "company_name=" + company_name + "&job_title=" + job_title + "&current_job=" + current_job + "&job_start=" + job_start + "&job_end=" + job_end + "&job_description=" + job_description + "&exp_id=" + exp_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("resume/save_experience")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".field-box").hide();
                    $("#message").html(data.success_message);
                }
                else {
                    $("#error_msg").html("Fail to save experience");
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<script src="<?= ASSETS_PATH ?>js/jquery.validation.js" type="text/javascript"></script>
<script type="text/javascript">
    function AddEducation() {
        var degree_title = $("#degree_title").val();
        var institute_name = $("#institute_name").val();
        var field_interest = $("#field_interest").val();
        var grade = $("#grade").val();
        var start_year = $("#start_year").val();
        var end_year = $("#end_year").val();
        var education_description = $("#education_description").val();
        var activities = $("#activities").val();
        var edu_id = $("#edu_id").val();

        if (degree_title == '') {
            $('#degree_title').css({ border: '1px solid red'});
            $('#degree_title').focus();
            return false;
        } else {
            $('#degree_title').removeAttr("style")
        }
        if (institute_name == '') {
            $('#institute_name').css({ border: '1px solid red'});
            $('#institute_name').focus();
            return false;
        } else {
            $('#institute_name').removeAttr("style")
        }
        if (start_year == '') {
            $('#start_year').css({ border: '1px solid red'});
            $('#start_year').focus();
            return false;
        } else {
            $('#start_year').removeAttr("style")
        }
        if (end_year == '') {
            $('#end_year').css({ border: '1px solid red'});
            $('#end_year').focus();
            return false;
        } else {
            $('#end_year').removeAttr("style")
        }
        if (field_interest == '') {
            $('#field_interest').css({ border: '1px solid red'});
            $('#field_interest').focus();
            return false;
        } else {
            $('#field_interest').removeAttr("style")
        }


        var data = "degree_title=" + degree_title + "&institute_name=" + institute_name + "&field_interest=" + field_interest + "&grade=" + grade + "&start_year=" + start_year + "&end_year=" + end_year + "&education_description=" + education_description + "&activities=" + activities + "&edu_id=" + edu_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("resume/save_education")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".field-box").hide();
                    $("#message").html(data.success_message).delay(5000);
                    window.location = "<?=$vObj->getURL("resume")?>";
                }
                else {
                    $("#error_msg").html("Fail to save experience");
                }
            },
            failure: function (errMsg) {
            }
        });
    }
</script>
<div id="experience" style="display: none">
    <form class="new_user_form inline-input" _lpchecked="1"
      enctype="multipart/form-data" id="profile_form" name="profile_form"
      method="post">
    <div class="field-box">
        <label><?= LABEL_JOB_TITLE ?>:</label>
        <input class="span9" type="text"
               name="job_title" id="job_title"
               value="<?php echo $exp_data['job_title']; ?>">
    </div>
    <div class="field-box">
        <label><?= LABEL_COMPANY_NAME ?>:</label>
        <input class="span9" type="text"
               name="company_name" id="company_name"
               value="<?php echo $exp_data['company_name']; ?>">
    </div>
    <div class="field-box">
        <label><?= LABEL_CURRENT_JOB ?>:</label>
        <input type="checkbox" class="user" <?php if ($exp_data['current_job'] == 1) {
            echo "checked=''";
        } ?> name="current_job" id="current_job" onclick="CurrentJob();">
    </div>
    <div class="field-box">
        <label><?= LABEL_START_DATE ?>:</label>
        <input class="span9" type="datetime" name="job_start" id="job_start"
               value="<?php echo $exp_data['job_start']; ?>">

        <div class="bfh-datepicker"></div>
    </div>
    <div class="field-box">
        <label><?= LABEL_END_DATE ?>:</label>
        <input class="span9 inline-input" type="text"
               name="job_end" id="job_end"
               value="<?php echo $exp_data['job_end']; ?>">
    </div>

    <div class="field-box">
        <label><?= LABEL_JOB_DESCRIPTION ?>:</label>
        <textarea class="span9 inline-input"
                  name="job_description"
                  id="job_description"><?= $exp_data['job_description'] ?></textarea>

    </div>
    <div class="span11 field-box actions">
        <input type="hidden" name="exp_id" id="exp_id"
               value="<?= $exp_data['exp_id'] ?>"/>
        <input type="button" class="btn-glow primary" onclick="AddExperience()"
               value="<?= LABEL_SAVE_CHANGES ?>">
        <span>OR</span>
        <input type="reset" value="Cancel" class="reset"
               onclick="window.history.back()">
    </div>
</form>
</div>

<div id="education" style="display: none">
    <form class="new_user_form inline-input" _lpchecked="1"
          enctype="multipart/form-data" id="profile_form" name="profile_form"
          method="post">
        <div class="field-box">
            <label><?= LABEL_DEGREE_TITLE ?>:</label>
            <input class="span9" type="text"
                   name="degree_title" id="degree_title"
                   value="<?php echo $education_data['degree_title']; ?>">
        </div>
        <div class="field-box">
            <label><?= LABEL_INSTITUTE_NAME ?>:</label>
            <input class="span9" type="text"
                   name="institute_name" id="institute_name"
                   value="<?php echo $education_data['institute_name']; ?>">
        </div>

        <div class="field-box">
            <label><?= LABEL_GRADE ?>:</label>
            <input class="span9" type="text" name="grade" id="grade"
                   value="<?php echo $education_data['grade_obtain']; ?>">
        </div>
        <div class="field-box">
            <label><?= LABEL_START_YEAR ?>:</label>
            <select name="start_year" id="start_year" class="span5 inline-input">
                <option value="">Select</option>
                <?php
                for ($i = 1980; $i <= 2013; $i++) {
                    if ($education_data['start_year'] == $i) {
                        $select = 'selected';
                    } else {
                        $select = "";
                    }
                    ?>
                    <option value="<?= $i ?>" <?= $select ?>  ><?= $i ?></option>
                    <?php
                }
                ?>

            </select>
        </div>
        <div class="field-box">
            <label><?= LABEL_END_YEAR ?>:</label>
            <select name="end_year" id="end_year" class="span5 inline-input">
                <option value="">Select</option>
                <?php
                for ($i = 1980; $i <= 2013; $i++) {
                    if ($education_data['end_year'] == $i) {
                        $select = 'selected';
                    } else {
                        $select = "";
                    }
                    ?>
                    <option value="<?= $i ?>"  <?= $select ?> ><?= $i ?></option>
                    <?php
                }
                ?>

            </select>

        </div>

        <div class="field-box">
            <label><?= LABEL_EDUCATION_DESCRIPTION ?>:</label>
            <textarea class="span9 inline-input"
                      name="education_description"
                      id="education_description"><?= $education_data['education_description']; ?></textarea>

        </div>
        <div class="field-box">
            <label><?= LABEL_FIELD ?>:</label>
            <textarea class="span9 inline-input"
                      name="field_interest"
                      id="field_interest"><?= $education_data['field_interest']; ?></textarea>
        </div>
        <div class="field-box">
            <label><?= LABEL_ACTIVITIES ?>:</label>
            <textarea class="span9 inline-input"
                      name="activities"
                      id="activities"><?= $education_data['activities']; ?></textarea>

        </div>
        <div class="span11 field-box actions">
            <input type="hidden" value="<?= $education_data['education_id']; ?>"
                   name="edu_id" id="edu_id"/>
            <input type="button" class="btn-glow primary" onclick="AddEducation();"
                   value="<?= LABEL_SAVE_CHANGES ?>">
            <span>OR</span>
            <input type="reset" value="Cancel" class="reset"
                   onclick="window.history.back()">
        </div>
    </form>
</div>