<?php
$option_value = "0";
if (is_array($get_cities) > 0) {
    $option_value = "";
}
?>
<div class="ui-select find">
    <select class="span5 inline-input" name="job_city"
            id="job_city" onchange="FilterJobs();">
        <option value="">Select City</option>
        <?php
        foreach ($get_cities as $key => $job_cities) {
            ?>
            <option
                value="<?php echo $job_cities['city_id'] ?>"><?php echo $job_cities['city'] ?></option>
        <?php
        }
        ?>
    </select>
</div>