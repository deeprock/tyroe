<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!--<script src="<?php/*= ASSETS_PATH */?>js/jquery-1.9.1.min.js"></script>
<script src="<?php/*= ASSETS_PATH */?>js/popup/popup.js"></script>-->
<script>
    //reviewer searching
    $(document).on('click', '.open_modal', function () {
        $('#myModal').show();
        $('.save_reviewer_info').text('Save changes');
        $('#myModalLabel').text('Add Reviewer');
        var inner_for_html = '<div class="alert alert-danger hidden error_modal_div"></div>';
        inner_for_html += '<div class="container-fluid">';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span12 center-block">';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>First & Last Name</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9 rt-first-ct">';
        inner_for_html += '<input class="rt-nd1 span6" type="text" id="firstname" placeholder="First name"/>';
        inner_for_html += '<input class="pull-right rt-nd1 span6" type="text" id="lastname" placeholder="Last name"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Username</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="text" id="username" placeholder="Username"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Email</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="text" id="email" placeholder="Email"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Password</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="password" id="password" placeholder="Password"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Company Name</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="text" id="" value="<?= $vObj->session->userdata('company_name') ?>" disabled="disabled" placeholder="Company Name"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Job Title</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="text" id="job_title" placeholder="Job Title"/>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Status</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<input class="width-100 rt-nd1" type="hidden" id="user_id" value=""/>';
        inner_for_html += '<select id="status_sl" class="rt-status rt-nd1 width-100">';
        inner_for_html += '<option value="1">Active</option>';
        inner_for_html += '<option value="0">Pending</option>';
        inner_for_html += '</select>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="row-fluid">';
        inner_for_html += '<div class="span3">';
        inner_for_html += '<label>Default Group</label>';
        inner_for_html += '</div>';
        inner_for_html += '<div class="span9">';
        inner_for_html += '<select id="default_group" class="rt-status rt-nd1 width-100">';
        inner_for_html += '<option value="3">Administrator</option>';
        inner_for_html += '<option value="4">Reviewer</option>';
        inner_for_html += '</select>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        inner_for_html += '</div>';
        $('.global-popup_body').html(inner_for_html);
    });
    $(document).on('keyup', '#reviewer_name', function (a) {
        $('.loader-save-holder').show();
        var fillter_text = $(this).val();
        var bool = true;
        $.ajax({
            type: 'post',
            data: 'fillter_text=' + fillter_text + '&bool=' + bool,
            url: "<?= $vObj->getURL("team"); ?>",
            success: function (html) {
                $('#fillter_area').html(html);
                $('.loader-save-holder').hide();
            }
        });
    });
    $(document).on('click', '.save_reviewer_info', function () {
        var firstname = $('#firstname').val();
        var lastname = $('#lastname').val();
        var username = $('#username').val();
        var job_title = $('#job_title').val();
        var email = $('#email').val();
        var password = $('#password').val();
        var user_id = $('#user_id').val();
        var status_sl = $('#status_sl').val();
        var default_group = $('#default_group').val();
        var action = 'add';
        var error = '';
        var pattern = /^[A-z0-9]+([A-z0-9]+[\.\_]?[A-z0-9]+)*@[A-z0-9]{2,}(\.[A-z0-9]{1,}){1,2}[A-z]$/;
        if (firstname == '') {
            $('#firstname').css({"border": "1px solid red"});
            error += 'first name is required<br>';
        }
        else
        {
            $('#firstname').css('border',"");
        }
        if (lastname == '') {
            $('#lastname').css({"border": "1px solid red"});
            error += 'last name is required<br>';
        }
        else{
            $('#lastname').css('border',"");
        }
        if (username == '') {
            $('#username').css({"border": "1px solid red"});
            error += 'username  is required<br>';
        }
        else{
            $('#username').css('border',"");
        }
        if (email == '') {
            $('#email').css({"border": "1px solid red"});
            error += 'email is required<br>';
        } else if (!pattern.test(email)) {
            $('#email').css({"border": "1px solid red"});
            error += 'Invalid email address <br>';
        }else{
            $('#email').css('border',"");
        }
        if (password == '') {
            $('#password').css({"border": "1px solid red"});
            error += 'password is required<br>';
        }else{
            $('#password').css('border',"");
        }
        if (error == '') {
            $('.loader-save-holder').show();
            $('.error_modal_div').addClass('hidden');
            $('.global-popup_body input[type="text"],.global-popup_body input[type="password"]').css({"border": "1px solid #cccccc"})
            var data = 'firstname=' + firstname + '&lastname=' + lastname + '&username=' + username + '&job_title=' + job_title + '&email=' + email + '&password=' + password + '&status_sl=' + status_sl + '&action=' + action + '&reviewer_id=' + user_id + '&default_group=' + default_group;

            $.ajax({
                type: 'post',
                data: data,
                url: "<?= $vObj->getURL("team/add_edit_reviewer"); ?>",
                dataType: 'json',
                success: function (return_data) {
                    if (return_data.success_result) {
                        if (user_id != '') {
                            $('.row_reviewer_no' + user_id).remove();
                        }
                        $(".notification-box-message").css({"color": "#005580"})
                        $(".notification-box-message").html(return_data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                        $('#myModal').modal('hide');
                        var previous_html = $('#studio_reviewers').html();
                        if(previous_html == "" || previous_html == undefined)
                        {
                            location.reload();
                        }
                        else{
                        $('#studio_reviewers').html(return_data.html + previous_html);
                        $('.alert.alert-warning').remove();
                        $('.global-popup_body input[type="text"],.global-popup_body input[type="password"]').val('');
                        }
                    } else {
                        $(".notification-box-message").css({"color": "rgb(184, 25, 0)"})
                        $(".notification-box-message").html(return_data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);

                    }
                    setTimeout(function(){
                        $('.loader-save-holder').hide();
                    },3000)
                }
            });
        } else {
            $('.error_modal_div').html(error);
            $('.error_modal_div').removeClass('hidden');
        }

    });
    $(document).on('click', '.btnclose', function () {
        $('.error_modal_div').addClass('hidden');
        $('.global-popup_body input[type="text"],.global-popup_body input[type="password"]').css({"border": "1px solid #cccccc"})
        $('.global-popup_body input[type="text"],.global-popup_body input[type="password"]').val('');
    });
    $(document).on('click', '.delete_reviewer', function () {
        var reviewer_id = $(this).data('value');
        var action = 'delete';
        if (confirm('Are you sure you want to do this?')) {
            $('.loader-save-holder').show();
            $.ajax({
                type: 'post',
                data: 'action=' + action + '&reviewer_id=' + reviewer_id,
                url: "<?= $vObj->getURL("team/add_edit_reviewer"); ?>",
                dataType: 'json',
                success: function (result) {
                    $(".notification-box-message").css({"color": "#005580"})
                    $(".notification-box-message").html(result.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    $(".row_reviewer_no" + reviewer_id).fadeOut(1200);
                    setTimeout(function(){
                        $('.loader-save-holder').hide();
                    },3000)
                }
            });
        }
    });
    $(document).on('click', '.edit_reviewer', function () {
        $('.loader-save-holder').show();
        var reviewer_id = $(this).data('value');
        var action = 'edit';
        $('.save_reviewer_info').text('Update');
        $('#myModalLabel').text('Update Reviewer');
        $.ajax({
            type: 'post',
            data: 'action=' + action + '&reviewer_id=' + reviewer_id,
            url: "<?= $vObj->getURL("team/add_edit_reviewer"); ?>",
            dataType: 'json',
            success: function (result) {
                $('.global-popup_body').html(result.html);
                $('#myModal').show();
                setTimeout(function(){
                    $('.loader-save-holder').hide();
                },3000)

            }
        });
    });
</script>
<script>


    $(document).on('click', '.sorter', function () {
        $('.loader-save-holder').show();
        var input_val = $('#reviewer_name').val();
        var sort_col = $(this).attr('data-column');
        var sort_type = $(this).attr('data-stype');
        var url = '<?=$vObj->getURL("team")?>';
        var obj = $(this);
        $.ajax({
            type: "POST",
            data: "sort_column=" + sort_col + "&sort_type=" + sort_type + "&sorting=" + true + "&bool=" + true + "&fillter_text=" + input_val,
            url: url,
            success: function (data) {
                var alldata = data;
                $('#fillter_area').html(alldata);
                if (sort_type == "ASC") {
                    obj.attr('data-stype', 'DESC');
                }
                else {
                    obj.attr('data-stype', 'ASC');
                }
                $('.loader-save-holder').hide();
            }
        });
    });
</script>
<div style="display: none;" class="loader-save-holder">
    <div class="loader-save"></div>
</div>
<div class="content">
    <div class="container">
        <div id="pad-wrapper">

            <div class="row-fluid header z-margin-b">
                <div id="admin_users_top_header" class="">
                    <h5 class="resume-title"><?= LABEL_MANAGE_STUDIO_REVIEWER ?></h5>

                </div>

                <div class="pull-right rt-new-team">

                        <input class="pull-left search tr-search" value="<?= $fillter_input_val ?>" type="text"
                               placeholder="Type a user's name..." name="reviewer_name" id="reviewer_name">

<!--                    <a href="javascript:void(0)" class="btn-flat open_modal success pull-right no-margin"
                       data-toggle="modal" data-target="#myModal">+ New Team member</a>-->
                </div>
            </div>
            <div id="fillter_area">

                    <div class="show-grid margin-adjust">
                        <div class="clearfix"></div>
                    </div>

                    <table class="table table-hover tr-thead">
                        <thead>
                        <tr>
                            <th><div class="position-relative"><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.firstname" data-stype="ASC">Name</a></div></th>
                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.role_id" data-stype="ASC">DEFAULT GROUP</a></div></th>
                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.created_timestamp" data-stype="DESC">Signed up</a></div></th>
                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.user_id" data-stype="DESC">Openings</a></div></th>
                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.email" data-stype="DESC">Email</a></div></th>
                            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.status_sl" data-stype="DESC">Status</a></div></th>
                            <th class="width74px"><div class="position-relative"><span class="line"></span></div></th>
                        </tr>
                        </thead>

                        <tbody id="studio_reviewers" class="team-tr">
                        <?php if ($get_reviewers != '') { ?>
                        <?php foreach ($get_reviewers as $get_reviewer) { ?>
                            <tr class="table_rows row_reviewer_no<?= $get_reviewer['user_id'] ?>">
                                <td><p class="rt-name"><?= stripslashes($get_reviewer['firstname']) . ' ' . stripslashes($get_reviewer['lastname']) ?></p>
                                    <span class="rt-title"><?= stripslashes($get_reviewer['job_title']); ?></span>
                                </td>
                                <td><?php
                                    if($get_reviewer['role_id'] == 3)
                                        echo "Administrator";
                                    else
                                        echo "Reviewer";
                                ?></td>
                                <td><?php echo date('M d, Y', $get_reviewer['created_timestamp']); ?></td>
                                <td style="width: 350px">
                                    <?php if ($get_reviewer['get_reviewer_openings'] != '') { ?>
                                        <?php
                                        $total_openings = '';
                                        foreach ($get_reviewer['get_reviewer_openings'] as $reviewer_opening) {
                                            ?>
                                            <?php $total_openings .= stripslashes($reviewer_opening['job_title']) . ','; ?>
                                        <?php } ?>
                                        <?php echo rtrim($total_openings, ','); ?>
                                    <?php } ?>
                                </td>
                                <td><a href="mailto:<?= $get_reviewer['email'] ?>"><?= $get_reviewer['email'] ?></a>
                                </td>
                                <td class="status_td">
                                    <?php if ($get_reviewer['status_sl'] == 1) { ?>
                                        <a href="javascript:void(0)" class="reviewer_status_sl" data-value="1"
                                           data-id="<?= $get_reviewer['user_id'] ?>"><label class="label label-success">Active</label></a>
                                    <?php } else if ($get_reviewer['status_sl'] == 0) { ?>
                                        <a href="javascript:void(0)" data-value="0"
                                           data-id="<?= $get_reviewer['user_id'] ?>"><label class="label label-info rt-pending">Pending</label></a>
                                    <?php } ?>
                                </td>
                                <td>
                                    <?php if(intval($get_reviewer['user_id']) != intval($user_id)) { ?>
                                    <a href="javascript:void(0)" data-toggle="modal" data-target="#myModal"
                                       class="edit_reviewer margin-right-2" data-value="<?= $get_reviewer['user_id'] ?>">Edit</a> <a
                                        href="javascript:void(0)" class="delete_reviewer"
                                        data-value="<?= $get_reviewer['user_id'] ?>">Delete</a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <div class="alert alert-warning">
                                    <i class="icon-warning-sign"></i>
                                    You don't have any team members yet. Send out some invites and share the work load.
                                </div>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <!-- START PAGINATION -->
                    <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                          method="post" action="<?= $vObj->getURL('team') ?>">
                        <div class="span12 pagination"><?= $pagination ?></div>
                        <input type="hidden" value="<?= $fillter_input_val ?>" name="fillter_text"/>
                        <input type="hidden" value="<?= $page ?>" name="page">
                    </form>

            </div>
        </div>
    </div>
</div>
<!-- main container -->
<!-- Modal -->
<div style="display: none" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
       <div class="global-popup_container">
         <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Add Reivewer</h4>
                </div>
                <div class="global-popup_body  padding-7 rt-modal">
                    <div class="alert alert-danger hidden error_modal_div">
                    </div>
                    <input type="text" id="firstname" placeholder="First name"/>
                    <input type="text" id="lastname" placeholder="Last name"/>
                    <input type="text" id="username" placeholder="Username"/>
                    <input type="text" id="job_title" placeholder="Job Title"/>
                    <input type="text" id="email" placeholder="Email"/>
                    <input type="password" id="password" placeholder="Password"/>
                    <input type="hidden" id="user_id" value=""/>
                    <select id="status_ASDASDsl" style="margin-top: -10px;">
                        <option value="1">Active</option>
                        <option value="0">Pending</option>
                    </select>
                </div>
                <div class="modal-footer rt-ft-modal">
                    <div class="container-fluid">
                        <div class="row-fluid">
                    <div class="span12">
                    <button type="button" class="btn-flat white btnclose" data-dismiss="modal">Close</button>
                    <button type="button" class="btn-flat primary btn-save save_reviewer_info">Save changes</button>
                    </div></div></div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(function ()  {
        $('#status_sl').select2();
    });
</script>
