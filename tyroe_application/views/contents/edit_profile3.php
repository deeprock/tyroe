<?php

if (!defined('BASEPATH')) exit('No direct script access allowed');


?>
<style type="text/css">

#single-team .team-name h2, #single-team .team-name h3, #single-team .team-name p {
    float: none;
}
    .content{
        margin-left: 0px !important;
    }

    /*PROFILE THEME START*/
    #single-team .span9{
        color:<?=$profile_theme['text']?>;
    }
    #single-team h1, #single-team h2, #single-team h3, #single-team h4, #single-team h5, #single-team h6{
        color:<?=$profile_theme['text']?> !important;
    }
    #single-team .btn-ani{
        background-color:<?=$profile_theme['background']?>;
    }
    #single-team .team-social ul.pub-soc-li li a i{
        color:<?=$profile_theme['icons']?>;
    }
    #single-team .team-social ul.pub-soc-li li a:hover i{
        color:<?=$profile_theme['icons_rollover']?>;
    }
    #single-team{
        background-color:<?=$profile_theme['background']?>;
    }

    #single-team-section .span9{
        color:<?=$profile_theme['text']?>;
    }
    #single-team-section h1, #single-team h2, #single-team h3, #single-team h4, #single-team h5, #single-team h6{
        color:<?=$profile_theme['text']?>;
    }
    /*#single-team-section .btn-ani{
            background-color:<?=$this->session->userdata['profile_theme']['background']?>;
    }
    #single-team-section .team-social ul.pub-soc-li li a i{
        color:<?=$this->session->userdata['profile_theme']['icons']?>;
    } */

    #social-connected-profile ul.public-social-list li a i {
        color:<?=$profile_theme['icons']?>;
    }

        /*#single-team-section .team-social ul.pub-soc-li li a:hover i{
            color:<?=$this->session->userdata['profile_theme']['icons_rollover']?>;
    } */

    #social-connected-profile ul.public-social-list li a:hover i {
        color:<?=$profile_theme['icons_rollover']?>;
    }


    #single-team-section{
        background-color:<?=$profile_theme['background']?>;
    }

        /*#dashboard-menu .pointer .arrow, #dashboard-menu .pointer .arrow_border {
            border-color:rgba(0, 0, 0, 0) <?=$this->session->userdata['profile_theme']['background']?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
    }*/ /*original work done by faisl bhai*/

    #sidebar-nav #dashboard-menu #left-arrow-pointer, #sidebar-nav #dashboard-menu #left-arrow-pointer-back {
        border-color:rgba(0, 0, 0, 0) <?=$profile_theme['background']?> rgba(0, 0, 0, 0) rgba(0, 0, 0, 0);
    }/*modified for the left arrow color only by aariz*/

        /*PROFILE THEME END*/

    /*FEATURED THEME START*/
    .featured-description-text{
        color:<?=$featured_theme['text']?> !important;
    }

    #intro-box a{
        color:<?=$featured_theme['text']?> !important;
        background:none repeat scroll 0 0 <?=$featured_theme['backgroundB']?> !important;
    }

    #feature_BGB{
        background:<?=(($video["media_name"] != "" && $video["media_type"] == "image") ? "none" : $featured_theme['backgroundB'])?> !important;
    }

    #intro-box {
        color :<?=$featured_theme['text']?> ;
        <?php
        if($video["media_name"] != "" && $video["media_type"] == "image"){
?>
        background:none repeat scroll 0 0 !important;
    <?php
        }
        else{?>
        background:none repeat scroll 0 0 <?php $featured_theme_backgroundA = $featured_theme['backgroundA'];
        echo $featured_theme_backgroundA!=null ? $featured_theme_backgroundA:"rgb(249, 127, 118 )";?>  !important;
        <?php
        }
        ?>
    }

    #intro-box .arrow{
        border-bottom-color:<?=$gallery_theme['background']?> !important;
    }
    /*FEATURED THEME END*/

    /*GALLERY THEME START*/
    #services-box h3{
        color:<?=$gallery_theme['text']?> !important;
    }
    #services-box{
        background:none repeat scroll 0 0 <?=$gallery_theme['background']?> !important;
    }
    #services-box .latest-work-disp h4{
        color:<?=$gallery_theme['text']?> !important;
    }
        /*buttons and overlay color remain*/
    /*GALLERY THEME END*/

    /*RESUME THEME START*/
    #latest-work h1, #latest-work h2, #latest-work h3, #latest-work h4, #latest-work h5, #latest-work h6{
        color:<?=$resume_theme['text']?> !important;
    }
    #latest-work{
        color:<?=$resume_theme['text']?> !important;
        background-color:<?=$resume_theme['background']?> !important;
    }
    footer span .arrow-down{
        /*working for drop arrow*/
        border-left: 20px solid rgba(0, 0, 0, 0);
        border-right: 20px solid rgba(0, 0, 0, 0);
        border-top: 20px solid <?=$resume_theme['background']?>;
    }
    .latest-work-disp{
        background-color:<?=$gallery_theme['overlay']?> !important;
        width:100%;
        padding: 20px;
        box-sizing: border-box;
    }
    /*RESUME THEME END*/


    /*FOOTER THEME START*/
    footer{
        background:none repeat scroll 0 0 <?php echo $footer_theme_backgroundA=$footer_theme['backgroundA'];?> !important;
    }

#social-footer ul li a i{
    color:<?=$footer_theme['icons']?> !important;
    opacity: 1;
}

#social-footer ul li a:hover i{
   color:<?=$footer_theme['icons_rollover']?> !important;
}
        /*FOOTER THEME END*/
</style>
<script src="<?= ASSETS_PATH ?>js/masonry.pkgd.min.js"></script>
<script src="<?= ASSETS_PATH ?>js/imagesloaded.pkgd.min.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery(document.body).on('click', '.icon-envelope-alt', function(event) {
        $('#emailModal').modal('show');
    });

    jQuery(document.body).on('click', '.sf-js-enabled a.alpha_contact', function(event) {
        $('#emailModal').modal('show');
    });

    $(".btn-modal-email").click(function(){
        $('#loader').show();
        $('.btn-modal-email').prop('disabled', true);
        var email_sender_name = $("#email_sender_name").val();
        var email_sender_message = $("#email_sender_message").val();
        var callfrom = "mail_to_profiler";
        var profiler_id = $("#profiler_id").val();
        var data = "email_sender_name=" + email_sender_name + "&email_sender_message=" + email_sender_message
                    + "&callfrom=" + callfrom + "&profiler_id=" + profiler_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("profile/save_profile")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $('#loader').css('display','none');
                    $('.prof-dtl-email .tab-margin').css('display','none');
                    $('.prof-dtl-email .return-message').css('display','');
                    $('.prof-dtl-email .return-message').html(data.message);
                } else {
                    $('.btn-modal-email').prop('disabled', false);
                    $('#loader').css('display','none');
                }
            },
            failure: function (errMsg) {
            }
        });
    });
});
</script>

<!--<script type="text/javascript" src="<?php/*= ASSETS_PATH */?>profile/anibus/js/main_1-may-2014.js"></script>-->
<div class="content">
<div id="navbar-sticky" class="navbar navbar-inverse <?=($this->session->userdata("user_id")!="" && $profile=="1" ?'':'public-sticky')?>">
    <div class="navbar-inner nav-fixed-top set_top_btn ">
        <ul class="nav pull-right">
            <li>
                <button type="button" class="button-main button-large new-nav-btn1  pull-left" id="edit_profile"><span>Edit Profile</span>
                </button>
            </li>
        </ul>
    </div>
</div>
<section class="" id="single-team">
    <div class="container">
        <div class="row">
            <div class="span3">
                <div class="team-img">
                    <img class="img-circle" alt="Person" src="<?php echo $get_user['image']; ?>">
                </div>
            </div>

        <div class="span9">
            <div class="team-name">
                <h2><?= (strcasecmp($firstname,'firstname')!=0?$firstname:"")?> <?=(strcasecmp($lastname,'lastname')!=0?$lastname:"")?></h2>
                <h3><?= $extra_title ?><?=$extra_title!=""&&$industry_name!=""?"|":""?><?=$industry_name?></h3>
                <span><?=$level?><?=$level!=""&&$experienceyear!=""?",":""?> <?=($experienceyear!=""?" with ".$experienceyear." years experience":"")?></span>
                <div class="clearfix"></div>
                <span><?=$city?><?=$city!=""&&$country!=""?",":""?><?=$country?></span>
            </div>
            <div class="team-description">
                <p><?= nl2br($user_biography) ?></p>
            </div>
            <div class="team-social">
                <ul class="pub-soc-li">
                    <?php
                    foreach ($get_social_link as $key => $social) {
                        ?>
                        <li><a href="<?= $social['social_link']; ?>"><i
                                    class="font-icon-social-<?= $social['social_type']; ?>"></i></a></li>
                    <?php
                    }
                    ?>

                    <!--<li><a href="#"><i class="font-icon-social-dribbble"></i></a></li>
                    <li><a href="#"><i class="font-icon-social-vimeo"></i></a></li>-->
                    <li style="position:relative; bottom:5px;">

                        <?php
                        if ($availability == "") {
                            ?>

                        <?php

                        } else if ($availability == "1") {
                            ?>
                            <a href="#" class="green-badge">Available for
                                work</a>
                        <?php
                        } else if ($availability == "2") {
                            ?>
                            <a href="#" class="green-badge">Available soon</a>
                        <?php
                        } else if ($availability == "3") {
                            ?>
                            <a href="#" class="grey-badge">not available for
                                work</a>
                        <?php
                        } else {
                           /* echo ' <a href="#" class="green-badge">Available for
                                                                    work</a>';*/
                        }
                        ?>
                    </li>
                    <li style="position:relative; bottom:5px;">
                        <!--<a href="#inline1" class="fancybox-various button-main button-mini">Contact Me</a>-->

                        <div style="display: none;" id="inline1">
                            <h3>Contact Me</h3>

                            <form action="#" class="contact-form" id="contact-form">
                                <p class="contact-name">
                                    <input type="text" name="name" value="" placeholder="Full Name" id="contact_name">
                                </p>

                                <p class="contact-email">
                                    <input type="text" name="email" value="" placeholder="Email Address"
                                           id="contact_email">
                                </p>

                                <p class="contact-message">
                                    <textarea cols="40" rows="15" name="message" placeholder="Your Message"
                                              id="contact_message"></textarea>
                                </p>

                                <p class="contact-submit">
                                    <a href="#" class="submit" id="contact-submit">Send Your Email</a>
                                </p>

                                <div id="response">

                                </div>
                            </form>

                        </div>
                    </li>
                </ul>
            </div>
        </div>

    </div>
    </div>
</section>
<?php
if ($video) {
    ?>
    <section class="padding-80 <?= (($video["media_name"] != "" && $video["media_type"] == "image") ? 'default_padd no-backgound' : '') ?>" id="intro-box" >
        <?= (($video['media_name'] != "" && $video['media_type'] == "video") ? '' : (($video["media_name"] != "" && $video["media_type"] == "image") ? '<img src="' . ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $video["media_name"] . '" alt="video">' : '')) ?>
        <div class="container">
            <div class="row">
                <div class="span12">
                    <div class="<?= (($video['media_type'] == "video") ? 'video-container' : ''); ?>"><?php
                        ?>
                        <!--<iframe
                            src="http://player.vimeo.com/video/50522981?byline=0&amp;portrait=0&amp;badge=0&amp;color=FF6056"></iframe>-->
                        <?= (($video['media_name'] != "" && $video['media_type'] == "video") ? '<iframe class="embed-video-section-iframe" src="' . $video['media_name'] . '?byline=0&amp;portrait=0&amp;badge=0&amp;color=FF6056"></iframe>' :"" ) ?>
                    </div>
                    <span class="arrow"></span>
                </div>
                <?php
                if ($video['media_type'] == "video") {
                    ?>
                    <div class="span12">
                        <div id="accordionArea" class="accordion">
                            <div class="accordion-group">
                                <div class="accordion-heading accordionize">
                                    <a href="#oneArea" data-parent="#accordionArea" data-toggle="collapse"
                                       class="accordion-toggle inactive collapsed">
                                        Shot Breakdown
                                        <span class="font-icon-arrow-simple-down"></span>
                                    </a>
                                </div>
                                <div style="height: 0px;" class="accordion-body collapse" id="oneArea">
                                    <div class="accordion-inner">
                                        <div><?= $video['media_description'] ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                <?php
                }
                ?>
            </div>
        </div>
    </section>
<?php
}
//checking if not null end
?>

<section class="pad_sec" id="services-box">
    <div class="container">
        <div class="row">
            <div class="span12">
                <h3>Latest Works</h3>
            </div>
        </div>
        <div class="row-fluid">
            <div id="portfolio-image-column" class="span12 sortable-images">
                <?php
                foreach ($latestwork_portfolioimages as $v) {
                $media_imagesection = $v['media_imagesection'];
                $ordercol1 = $v['media_sortorder'];
                    ?>
                    <div class="cell portfolio_latestwork_id" id="portfolioimage-<?= $v['image_id'] ?>-<?=$v['media_sortorder'] ?>">
                        <div class="latest-work-holder">
                            <img style="width:100%;height:100%;" src="<?php echo  ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $v['media_name'];  ?>">
                            <div class="latest-work-disp">
                                <p><?= $v['media_description'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <?php
            if(intval($latestwork_portfolioimages_count['total_count'])>=20) {
            ?>
            <div class="row-fluid">
                <div class="span12 text-center">
                    <button type="button" data-loading-text="Loading..." class="button-main inverted button-large margin-10 btn-load-more-portfolio">
                        LOAD MORE IMAGES
                    </button>
                </div>
            </div>
            <?php
            } ?>

                </div>
    </div>
    <script type="text/javascript">
//        if(typeof manage_section_heights==='undefined' || typeof manage_section_heights!=='function'){
//            function manage_section_heights() {
//                var left_col_ele=$('#portfolio-image-column1');
//                var right_col_ele=$('#portfolio-image-column2');
//                var left_col_height = $('#portfolio-image-column1').css('height').replace('px','');
//                var right_col_height = $('#portfolio-image-column2').css('height').replace('px','');
//
//
//                var divs_diff = Math.max(left_col_height,right_col_height)-Math.min(left_col_height,right_col_height);
//
//                if(left_col_height<right_col_height) {
//                    var result_target = '1';
//                    var col_left_or_right = left_col_height;
//                    var last_image_id = left_col_ele.find('.portfolio_latestwork_id:last').attr('id');
//                    var last_img_width=left_col_ele.find('.portfolio_latestwork_id:last').css('width');
//                } else if(left_col_height>right_col_height) {
//                    var result_target = '2';
//                    var col_left_or_right = right_col_height;
//                    var last_image_id = right_col_ele.find('.portfolio_latestwork_id:last').attr('id');
//                    var last_img_width=right_col_ele.find('.portfolio_latestwork_id:last').css('width');
//                }
//
//                if(divs_diff>300 && typeof result_target!=='undefined'){
//                    jQuery.ajax({
//                        type: 'GET',
//                        cache: false,
//                        data:  ({target_col:col_left_or_right, result_target:result_target,divs_diff:divs_diff,last_img_width:last_img_width,last_image_id:last_image_id}),
//                        url: '<?=$vObj->getURL("loadmore/fill_images");?>',
//                        success: function (success_data){
//                            if(is_JSON(success_data)){
//                                successdata_json=$.parseJSON(success_data);
//                                var left_html = ' ';
//                                var right_html = ' ';
//                                left_html = ' ';
//                                right_html = ' ';
//                                $.each(successdata_json,function(index,val){
//                                    var latest_work_id=val.latest_work_id;
//                                    var latest_work_image_src=val.latest_work_image_src;
//                                    var latest_work_sortorder=parseInt(val.latest_work_sortorder);
//                                    var latest_work_imagesection=parseInt(val.latest_work_imagesection);
//                                    var latest_work_title=val.latest_work_title;
//                                    var latest_work_description = val.latest_work_description;
//                                    var latest_work_tag_html = val.latest_work_tag_html;
//                                    var latest_work_tag = val.latest_work_tag;
//
//                                    if(latest_work_tag_html==null){
//                                        latest_work_tag_html=' ';
//                                    }
//
//                                    if(latest_work_title==null){
//                                        latest_work_title=' ';
//                                    }
//
//                                    if(latest_work_sortorder==null){
//                                        latest_work_sortorder=' ';
//                                    }
//
//                                    if(latest_work_description==null){
//                                        latest_work_description=' ';
//                                    }
//
//                                    if(latest_work_tag ==null){
//                                        latest_work_tag=' ';
//                                    }
//
//                                    var append_html = null;
//                                    append_html = '<div class="cell portfolio_latestwork_id" id="portfolioimage-'+latest_work_id+'-'+latest_work_sortorder+'">' +
//                                            '<div class="latest-work-holder">' +
//                                            '<img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>'+latest_work_image_src+'">' +
//                                            '<div class="latest-work-disp">' +
//                                            '<h4>'+latest_work_title+'</h4>' +
//                                            '<p>'+latest_work_description+'</p>' +
//                                            '<p class="font-italic">'+latest_work_tag_html+'</p>' +
//                                            ' </div></div> </div>';
//                                    //result_target
//
//                                    if(!isNaN(latest_work_sortorder)){
//                                        if(latest_work_imagesection%2==0){
//                                            right_html+=append_html;
//
//                                        } else {
//                                            left_html+=append_html;
//                                        }
//                                    }
//                                });
//                                var setIntval_for_fix_heights = null;
//                                setIntval_for_fix_heights = setInterval(function () {
//                                    if (result_target == 1) {
//                                        var rslt_trget = $("#portfolio-image-column1 .portfolio_latestwork_id:last")
//                                        var rslt_trget_tagname = rslt_trget.prop('tagName');
//                                        var rslt_dt = left_html;
//                                    } else if (result_target == 2) {
//                                        var rslt_trget = $("#portfolio-image-column2 .portfolio_latestwork_id:last")
//                                        var rslt_trget_tagname = rslt_trget.prop('tagName');
//                                        var rslt_dt = right_html;
//                                    }
//                                    if (typeof rslt_trget_tagname !== 'undefined') {
//                                        rslt_trget.after(rslt_dt);
////                                        console.log('>>>>rslt_trget');
////                                        console.log(rslt_trget);
////                                        console.log('>>>>rslt_dt');
////                                        console.log(rslt_dt);
//                                        clearInterval(setIntval_for_fix_heights);
//                                    }
//
//                                }, 100);
//                            }
//                        }
//                    })
//                }
//            }
//        }

        jQuery(window).load(function() {

        });

//        setTimeout(function (){
//            manage_section_heights();
//
//        }, 400);
        var total_images = <?= $latestwork_portfolioimages_count['total_count'] ?>;
        var total_images_loaded = 20;
        var image_page = 1;
        var is_ajax_loadmore_req = null;
        var profile_user_id = <?= $profile_user_id ?>;
        jQuery(document).on('click', '.btn-load-more-portfolio', function () {
            if(is_ajax_loadmore_req==true){
                return;
            }
            is_ajax_loadmore_req = true;
            var loadingbtn_target =  $(".btn-load-more-portfolio");
            loadingbtn_target.text('LOADING...');
            jQuery.ajax({
                type: 'GET',
                data: {
                    'callfrom':'loadmore_images',
                    'page_no':image_page,
                    'profile_user_id':profile_user_id
                },
                url: '<?=$vObj->getURL("loadmore/loadmore_images");?>',
                //dataType: 'json',
                complete: function (){
                    setTimeout(function () {
                        is_ajax_loadmore_req=false;
                    },100);
                },
                success: function (data) {
                    //return false;
                    if($.trim(data).length>0){
                        var latest_work_data = $.parseJSON(data);
                        //var next_photos_limit = latest_work_data.length;
                        if(latest_work_data==0 || latest_work_data==null || typeof latest_work_data==='undefined' || latest_work_data=='null') {
                            loadingbtn_target.text('NO MORE IMAGE AVAILABLE');
                            loadingbtn_target.attr('disabled','disabled');
                            setTimeout(function (){
                                loadingbtn_target.parent().fadeOut(700, function (){
                                    loadingbtn_target.parent().remove();
                                });
                                //loadingbtn_target.parent().remove();

                            }, 2000);
                            return;
                        }

                        var images_loaded = 0;
                        var msnry;
                        var masonary_container = null;
                        $.each(latest_work_data,function(index,val){
                            var latest_work_id=val.latest_work_id;
                            var latest_work_image_src=val.latest_work_image_src;
                            var latest_work_sortorder=parseInt(val.latest_work_sortorder);
                            var latest_work_imagesection=parseInt(val.latest_work_imagesection);
                            var latest_work_title=val.latest_work_title;
                            var latest_work_description = val.latest_work_description;
                            var latest_work_tag = val.latest_work_tag;
                            var latest_work_tag_html = val.latest_work_tag_html;
                            if(latest_work_tag_html==null){
                                latest_work_tag_html=' ';
                            }
                            if(latest_work_title==null){
                                latest_work_title=' ';
                            }
                            if(latest_work_sortorder==null){
                                latest_work_sortorder=' ';
                            }
                            if(latest_work_description==null){
                                latest_work_description=' ';
                            }
                            if(latest_work_tag ==null){
                                latest_work_tag=' ';
                            }

                            var append_html = null;
                            append_html = $('<div class="cell portfolio_latestwork_id" id="portfolioimage-'+latest_work_id+'-'+latest_work_sortorder+'">' +
                                    '<div class="latest-work-holder">' +
                                    '<img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>'+latest_work_image_src+'">' +
                                    '<div class="latest-work-disp" style="width:auto;">' +
                                    '<p>'+latest_work_description+'</p>' +
                                    ' </div></div> </div>');
                            images_loaded++;
                            total_images_loaded++;
                            $("#portfolio-image-column").append(append_html);
                            
                            append_html.children('.latest-work-holder').children('img').on('load', function () {
                                if (images_loaded > 0)
                                    images_loaded--;
                                if (images_loaded === 0) {
                                    masonary_container = document.querySelector('#portfolio-image-column');
                                    msnry = new Masonry(masonary_container, {
                                        // options
                                        itemSelector: '.portfolio_latestwork_id'
                                    });
                                }
                            });
                            
                        });
                        if(masonary_container != null){
                            imagesLoaded(masonary_container, function() {
                                msnry.layout();
                                $(window).trigger('resize');
                            });
                        }
                        if(total_images_loaded == total_images){
                            loadingbtn_target.remove();
                        }
                        image_page++;

//                        setTimeout(function () {
//                            //console.log($("#portfolio-image-column1 .portfolio_latestwork_id:last"));
//                            $("#portfolio-image-column1 .portfolio_latestwork_id:last").after(left_html);
//                            $("#portfolio-image-column2 .portfolio_latestwork_id:last").after(right_html);
//                            manage_section_heights();
//                        }, 100)
                        loadingbtn_target.text('LOAD MORE IMAGES');
                    } else {
                        loadingbtn_target.text('NO MORE IMAGE AVAILABLE');
                        loadingbtn_target.attr('disabled','disabled');
                        setTimeout(function (){
                            loadingbtn_target.parent().remove();
                        }, 2000)
                    }
                }
            });
        })
<?php
        /*
         *
         * Z-STYLE IMAGES
        var column1_limit_start = '<?=$latestWorkImagesColumn1Start?>';
        var column2_limit_start = '<?=$latestWorkImagesColumn2Start?>';
        jQuery(document).on('click', '.btn-load-more-portfolio', function () {
            var $btn = jQuery(this);
            $btn.button('loading');

            jQuery.ajax({
                type: 'POST',
                data: {
                    'callfrom': 'loadmore_images',
                    'column1': column1_limit_start,
                    'column2': column2_limit_start

                },
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json',
                success: function (data) {
                    $btn.button('reset');
                    jQuery(data.column1_images).each(function (i, obj) {
                        var html = '<div class="cell portfolio_latestwork_id" id="portfolioimage-' + obj.image_id + '-' + obj.media_sortorder + '"><div class="latest-work-holder"><img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>' + obj.media_name + '"><div class="latest-work-disp"><ul class="user-port-work-ops"><li><a href="javascript:void(0);"  class="sortable-description-event"><i class="icon-pencil"></i></a></li><li><a href="javascript:void(0);" class="enable-sortable-event"><i class="icon-random"></i></a></li><li><a href="javascript:void(0);" class="sortable-remove-event"><i class="icon-remove-circle"></i></a></li></ul><div style="display: none;" class="image-disp-container row-fluid sortable-image-description-container"><div class="span12"><textarea class="form-control span12 sortable-image-description-textarea">' + obj.media_description + '</textarea><button class="btn btn-default pull-right new-nav-btn1-color sortable-image-description-textarea-btn">Save</button></div></div></div></div></div>'
                        jQuery('#portfolio-image-column1').append(html);
                    });
                    $(data.column2_images).each(function (i, obj) {
                        var html = '<div class="cell portfolio_latestwork_id" id="portfolioimage-' + obj.image_id + '-' + obj.media_sortorder + '"><div class="latest-work-holder"><img style="width:100%;height:100%;" src="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>' + obj.media_name + '"><div class="latest-work-disp"><ul class="user-port-work-ops"><li><a href="javascript:void(0);"  class="sortable-description-event"><i class="icon-pencil"></i></a></li><li><a href="javascript:void(0);" class="enable-sortable-event"><i class="icon-random"></i></a></li><li><a href="javascript:void(0);" class="sortable-remove-event"><i class="icon-remove-circle"></i></a></li></ul><div style="display: none;" class="image-disp-container row-fluid sortable-image-description-container"><div class="span12"><textarea class="form-control span12 sortable-image-description-textarea">' + obj.media_description + '</textarea><button class="btn btn-default pull-right new-nav-btn1-color sortable-image-description-textarea-btn">Save</button></div></div></div></div></div>'
                        jQuery('#portfolio-image-column2').append(html);
                    });
                    column1_limit_start = data.column1_start;
                    column2_limit_start = data.column2_start;
                }
            });
        }); */

         ?>
    </script>
</section>

<section id="latest-work">

    <div class="container">
        <div class="row">
            <div class="span12">
                <h3>Resume</h3>
            </div>
        </div>
        <?php
        if($get_visibility != ''){
        if ($get_visibility[0]['visible_section'] == "exp" && $get_visibility[0]['visibility'] == '0' && $get_experience!="") {
            ?>
            <section class="experience">
                <div class="row">
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <a class="division" name="education"></a>

                        <h2>Work Experience</h2>
                    </div>
                    <!-- span8 -->
                </div>
                <!-- row -->
                <?php
                foreach ($get_experience as $k => $v) {
                    if ($v['current_job'] > 0) {
                        $current_job_index = $k;
                    }
                    ?>
                    <div class="row">
                        <div class="span4 info-seperator">
                            <h4><?= $v['job_title'] ?></h4>
                            <h4><?= $v['company_name'] ?></h4>
                            <h4><?= ($v['current_job'] > 0) ? "Present" : date('m/d/Y', $v['job_end']) ?></h4>
                        </div>
                        <!-- span4 -->
                        <div class="span8">
                            <p><?= $v['job_description'] ?></p>
                        </div>
                        <!-- span8 -->
                    </div><!-- row -->

                <?php
                }
                ?>


                <div class="row">
                    <hr style="border-color: rgb(165, 179, 185);" class="span12">
                </div>
            </section>
        <?php
        }
        if ($get_visibility[1]['visible_section'] == "edu" && $get_visibility[1]['visibility'] == '0' && $get_education!="") {
            ?>
            <section class="education">
                <div class="row">
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <a class="division" name="education"></a>

                        <h2>Education</h2>
                    </div>
                    <!-- span8 -->
                </div>
                <!-- row -->
                <?php
                foreach ($get_education as $k => $v) {

                    ?>
                    <div class="row">
                        <div class="span4 info-seperator">
                            <h4><?= ($v['edu_position'] == "" ? LABEL_PROFILE_RESUME_EDUCATION_DEGREE : $v['edu_position']) ?></h4>
                            <h4><?= ($v['edu_organization'] == "" ? LABEL_PROFILE_RESUME_EDUCATION_SCHOOL : $v['edu_organization']) ?></h4>
                            <h4><?= ($v['current_job'] > 0) ? "Present" : date('m/d/Y', $v['end_year']) ?></h4>
                        </div>
                        <!-- span4 -->
                        <div class="span8">
                            <p><?= ($v['education_description'] != "" ? $v['education_description'] : LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION) ?> </p>
                        </div>
                        <!-- span8 -->
                    </div><!-- row -->

                <?php
                }
                ?>
                <div class="row">
                    <hr style="border-color: rgb(165, 179, 185);" class="span12">
                </div>
            </section>
        <?php
        }
        if ($get_visibility[2]['visible_section'] == "award" && $get_visibility[2]['visibility'] == '0' && $get_awards!="") {
            ?>
            <section class="award">

                <div class="row">
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <a class="division" name="education"></a>
                        <h2>Awards</h2>
                    </div>
                    <!-- span8 -->
                </div>
                <!-- row -->
                <?php foreach ($get_awards as $k => $v) {
                    //$v['skill_name'] = stripslashes($v['skill_name']);
                    ?>
                    <div class="row">
                        <div class="span4 info-seperator">
                            <h4><?= ($v['award_organization'] != "" ? $v['award_organization'] : LABEL_PROFILE_RESUME_AWARD_ORGANIZATION) ?></h4>
                            <h4><?= ($v['award'] != "" ? $v['award'] : LABEL_PROFILE_RESUME_AWARD) ?></h4>
                            <h4><?= ($v['award_year'] != "" ? $v['award_year'] : LABEL_PROFILE_RESUME_AWARD_YEAR) ?></h4>
                        </div>
                        <!-- span4 -->
                        <div class="span8">
                            <p><?= ($v['award_description'] != "" ? $v['award_description'] : LABEL_PROFILE_RESUME_AWARD_DESCRIPTION) ?></p>
                        </div>
                        <!-- span8 -->
                    </div>
                <?php
                }
                ?>
                <!-- row -->

                <div class="row">
                    <hr style="border-color: rgb(165, 179, 185);" class="span12">
                </div>
            </section>
        <?php
        }
        if ($get_visibility[3]['visible_section'] == "skill" && $get_visibility[3]['visibility'] == '0' && $get_skills!="") {
            ?>
            <section class="skill">

                <div class="row">
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <a class="division" name="education"></a>

                        <h2>Skills</h2>
                    </div>
                    <!-- span8 -->
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <ul id="tags">
                            <?php foreach ($get_skills as $k => $v) {
                                $v['skill_name'] = stripslashes($v['skill_name']);
                                ?>
                                <li><a href="javascript:void(0)"
                                       class="button-main button-large"><?= $v['skill_name'] ?></a></li>
                            <?php
                            }
                            ?>
                        </ul>
                    </div>
                    <!-- span8 -->
                </div>
                <!-- row -->
                <?php if ($get_visibility[4]['visible_section'] == "recommendation" && $get_visibility[4]['visibility'] == '0' && $get_recommendation_approved_list!="" && $get_recommendation_approved_list[0]['recommend_status']=="1") { ?>
                <div class="row">
                         <hr style="border-color: rgb(165, 179, 185);" class="span12">
                </div>
                <?php } ?>
            </section>
        <?php
        }
        if ($get_visibility[4]['visible_section'] == "recommendation" && $get_visibility[4]['visibility'] == '0' && $get_recommendation_approved_list!="" && $get_recommendation_approved_list[0]['recommend_status']=="1") {
            ?>
            <section class="recommend-container">

                <div class="row">
                    <div class="span4"></div>
                    <!-- Keep this div empty to align -->
                    <div class="span8">
                        <a class="division" name="education"></a>

                        <h2>Recommendations</h2>
                    </div>
                    <!-- span8 -->
                </div>
                <!-- row -->
                <?php
                foreach ($get_recommendation_approved_list as $k => $v) {
                    ?>
                    <div class="row">
                        <div class="span4 info-seperator">
                            <h4><?= $v['recommend_name'] ?></h4>
                            <h4><?= $v['recommend_company'] ?></h4>
                            <h4><?= $v['recommend_dept'] ?></h4>
                        </div>
                        <!-- span4 -->
                        <div class="span8">
                            <p><?= $v['recommendation'] ?></p>
                        </div>
                        <!-- span8 -->
                    </div>
                    <!-- row -->


                <?php
                }
                ?>
                <!--<div class="row">
                    <hr style="border-color: rgb(165, 179, 185);" class="span12">
                </div>-->
            </section>
        <?php
        }}
        ?>
    </div>
</section>
<footer>
    <span style="border-top: 20px solid <?= $resume_theme['background'] ?>;"
          class="arrow-down"></span>

    <div class="container">
        <div class="row">
            <nav id="social-footer">
                <ul>
                    <?php
                    foreach ($get_social_link as $key => $social) {
                        ?>
                        <li>
                            <a target="_blank" class="<?php/*= $social['social_type']; */ ?>"
                               href="<?= $social['social_link']; ?>">
                                <i class="font-icon-social-<?= $social['social_type']; ?>"></i>
                            </a>
                        </li>
                    <?php
                    }
                    ?>

                </ul>
            </nav>
        </div>
    </div>
</footer>

<section id="footer-credits">
    <div class="container">
        <div class="row">
            <div class="span12">
                <p class="credits"><a href="http://www.tyroe.com" target="_blank">&copy; Built with TYROE.</a></p>
            </div>
        </div>
    </div>
</section>
<div class="clearfix"></div>

<!-- Back To Top -->
<a id="back-to-top" href="#">
    <i class="font-icon-arrow-simple-up"></i>
</a>
<!-- End Back to Top -->

<div id="element_to_pop_up">
</div>

    <div style="display: none;" class="modal fade" id="emailModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title mota-title pull-left">Send message to <?=$firstname . " " . $lastname?></h4>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-body">
                    <div class="center prof-dtl-email mdl-prf">
                        <div class="tab-margin row-fluid">
                            <input type="text" class="form-control pop-title-field height-adjust span12" placeholder="Sender Name" value="" id="email_sender_name" name="email_sender_name">
                            <textarea name="email_sender_message" class="span12" placeholder="Message" id="email_sender_message"></textarea>
                            <input type="hidden" name="profiler_id"  id="profiler_id" value="<?=$profiler_id?>">
                        </div>
                        <div class="return-message" style="display: none"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" data-close-crop="close">Close</button>
                    <button type="button" class="btn btn-primary btn-save btn-modal-email" value="<?= LABEL_SAVE_CHANGES ?>">Send
                    </button>
                    <img width="25px" id="loader" src="<?=ASSETS_PATH?>img/loader.gif" style="display:none"/>
                </div>
            </div>
        </div>
    </div>

<style>
    .prof-dtl-email .tab-margin input.height-adjust{height:49px;}
</style>
    <?php if( ($controller_name == 'publicprofile') && ($is_message_show == 1) ){ ?>
    <script>
        $('header').css('margin-top',58);
        $('div.content').css('margin-top',60);
    </script>
        <?php } ?>
    <?php//print_r($get_visibility);echo "d";print_r($get_recommendation_approved_list);die; ?>

    <script>
    $(document).ready(function(){
        var masonary_container = document.querySelector('#portfolio-image-column');
        var msnry = new Masonry(masonary_container, {
            itemSelector: '.portfolio_latestwork_id'
        });
        
        imagesLoaded(masonary_container, function() {
            msnry.layout();
            $(window).trigger('resize');
        });
    });
    </script>

