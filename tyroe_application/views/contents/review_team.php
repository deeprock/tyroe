<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    function InviteReviewer(reviewer_id) {
        var job_id = '<?php echo $job_id ?>';
        var data = "job_id=" + job_id + "&reviewer_id=" + reviewer_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/InviteReviewer")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".job-opening" + job_id).hide();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<!-- main container -->
<div class="content">

    <div class="container-fluid">

        <!-- upper main stats -->
        <?php
        $this->load->view('navigation_top');
        ?>
        <!-- end upper main stats -->

        <div id="pad-wrapper">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column" style="position:relative;">

                            <!-- margin pointer -->
                            <div class="left-pointer" id="review-team">
                                <div class="left-arrow"></div>
                                <div class="left-arrow-border"></div>
                            </div>


                            <div class="row-fluid header">
                                <div class="span12">
                                    <h4>Review Team</h4>

                                    <div class="btn-group settings pull-right left-pad-20">
                                        <button class="btn glow" id="batch"><i class="icon-cog"></i></button>
                                        <button class="btn glow dropdown-toggle" id="batch" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Batch Contact</a></li>
                                            <li><a href="#">Batch Add to Existing Job</a></li>
                                            <li><a href="#">Batch Remove</a></li>
                                        </ul>

                                    </div>
                                    <div class="button-center pull-right">
                                        <a class="btn-flat primary" id="invite">Invite New Reviewer</a>
                                    </div>
                                </div>

                            </div>
                            <!-- START TEAM DETAILS -->

                            <!-- PERSON START -->

                            <?php
                            foreach ($get_reviewer as $reviewer) {
                                $image;
                                if ($reviewer['media_name']) {
                                    $image = ASSETS_PATH_REVIEWER_THUMB . $reviewer['media_name'];
                                } else {
                                    $image = ASSETS_PATH_NO_IMAGE;
                                }
                                ?>


                                <input type="hidden" name="reviewer_id" id="reviewer_id"
                                       value="<?php echo $reviewer['user_id']; ?>">
                                <div class="row-fluid">
                                    <div class="span12 people" id="top">
                                        <div class="row-fluid" id="opening-stats">
                                            <div class="span10">
                                                <div class="span12">
                                                    <input type="checkbox" class="user">
                                                    <img src="<?php echo $image ?>" class="img-circle avatar-55">
                                                    <a href="#"
                                                       class="name_sidebar"><?php echo $reviewer['username']; ?></a>
                                                    <span class="label label">Review Missing</span>
                                                </div>
                                            </div>
                                            <div class="span2">
                                                <div class="btn-group settings pull-right">
                                                    <button class="btn glow"><i class="icon-cog"></i></button>
                                                    <button class="btn glow dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="#">Contact</a></li>
                                                        <li><a href="javascript:void(0);"
                                                               onclick="InviteReviewer('<?php echo $reviewer['user_id'] ?>');">Add
                                                                to Existing Job</a></li>
                                                        <li><a href="#">Remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            <?php
                            }
                            ?>


                            <!-- END OPENING DETAILS -->

                            <!-- <div class="row-fluid header sections">
                                 <div class="span12">
                                         <h4>All Reviewers</h4>
                                         <div class="btn-group settings pull-right" style="margin: 0 20px;">
                                                 <button class="btn glow" id="batch"><i class="icon-cog"></i></button>
                                                 <button class="btn glow dropdown-toggle" id="batch" data-toggle="dropdown">
                                                     <span class="caret"></span>
                                                 </button>
                                                     <ul class="dropdown-menu">
                                                         <li><a href="#">Batch Contact</a></li>
                                                         <li><a href="#">Batch Add to Existing Job</a></li>
                                                         <li><a href="#">Batch Remove</a></li>
                                                     </ul>

                                                 </div>
                                 </div>

                             </div>-->
                            <!-- START TEAM DETAILS -->

                            <!-- PERSON START -->
                            <!-- <div class="row-fluid">
                                 <div class="span12 people" id="top">
                                     <div class="row-fluid" id="opening-stats">
                                         <div class="span8">
                                             <div class="span12">
                                                 <input type="checkbox" class="user">
                                                 <img src="img/avatar_07.png" class="img-circle avatar-55">
                                                 <a href="#" class="name_sidebar">Alejandra Galvan Castillo</a>
                                             </div>
                                         </div>
                                         <div class="span4">
                                             <span class="pull-right button-center">
                                                 <a class="btn-flat primary" id="comment">+ Add</a></span>
                                         </div>
                                     </div>
                                 </div>
                             </div>-->


                        </div>
                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
    <!-- end main container -->
</div>