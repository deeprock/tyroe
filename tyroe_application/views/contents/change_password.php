<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.min.js"></script>
<script type="text/javascript">
    function PasswordChange() {
        var password = $('#user_password').val();
        var confirm_password = $('#user_confirmpass').val();
        if (password == "") {
            $('#user_password').css({
                border: '1px solid red'
            });
            $('#user_password').focus();
            return false;
        }

        if (password != "") {
            $('#user_password').css({
                border: ''
            });
        }

        if (confirm_password == "") {
            $('#user_confirmpass').css({
                border: '1px solid red'
            });
            $('#user_confirmpass').focus();
            return false;
        }

        if (confirm_password != "") {
            $('#user_confirmpass').css({
                border: ''
            });
        }

        if (confirm_password != password) {
            alert("Password not matched");
            $('#user_confirmpass').css({
                border: '1px solid red'
            });
            $('#user_confirmpass').focus();
            return false;
        }


        var user_id = $("#user_id").val();

        if (user_id != "" && confirm_password != "") {
            var data = "user_id=" + user_id + "&pass=" + confirm_password;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("login/PasswordChanged")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".span12").hide();
                        $("#box-heading").html("<?=LABEL_PASSWORD_CHANGED?>");
                        $(".btn-glow").hide();
                        $("#error_msg").html(data.success_message);
                    }
                    else {
                        $("#error_msg").html("Invalid Email/Password");
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
        else {

            $("#error_msg").html("Provide Authentication Details");
        }
    }
</script>


<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img src="<?= ASSETS_PATH ?>img/logo-dark.png" class="logo"/>
    </a>

    <div class="span4 box">
        <form _lpchecked="1" class="new_user_form inline-input" method="post"
              action="<?= $vObj->getURL('login/PasswordChanged') ?>" name="RegForm">
            <div class="content-wrap">
                <h6 id="box-heading"><?= LABEL_PASSWORD_CHANGE ?></h6>

                <div class="field-box error">
                  <span id="error_msg"><?php //echo validation_errors();

                      /* if ($msg != null) {
                           echo $msg;
                       }*/
                      ?></span>
                </div>
                <input type="hidden" name="user_id" id="user_id" value="<?= $email['user_id'] ?>">
                <input class="span12" type="text" name="user_email" readonly="" placeholder="<?= LABEL_EMAIL ?>"
                       id="user_email" value="<?= $email['email'] ?>">
                <input class="span12" type="password" name="user_password"
                       placeholder="<?= LABEL_NEW_PASSWORD ?>"
                       id="user_password" value="<?php //echo set_value
                ('user_password'); ?>">
                <input class="span12" type="password" name="user_confirmpass"
                       id="user_confirmpass" placeholder="<?=
                LABEL_CONFIRM_PASSWORD
                ?>"
                       value="<?php //echo set_value('user_confirmpass'); ?>">

                <div class="action">
                    <input type="button" class="btn-glow primary prim-ary-login"
                           onclick="PasswordChange();" value="<?= LABEL_SUBMIT ?>">
                </div>
            </div>
        </form>
    </div>

    <div class="span4 no-account">
        <p><?= LABEL_ALREADY_ACCOUNT ?></p>
        <a href="<?= $vObj->getURL('login') ?>"><?= LABEL_SIGNIN ?></a>
    </div>
</div>
