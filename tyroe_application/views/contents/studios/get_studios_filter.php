<?php
if (!empty($data)) {
    foreach ($data as $k => $v) {
     ?>
        <tr id="Studio-id-<?=$v['user_id']?>">
            <td align="center"><input type="checkbox" name="bulk_check[]" class="studio_check"
                                                              value="<?=$v['user_id']?>"></td>
            <td><?=$v['user_id']?></td>
            <td><p class="rt-name"><?=$v['company_name']?></p>
                <span class="rt-title"><?=$v['industry_name']?></span></td>
            <td class="status_td">
                <?php 
                
                if($v['status'] == 'Pending'){?>
                    <span class="label label-info rt-pending" style="background-color: #da4f49;"><?=$v['status'] ?></span>
                <?php }else if($v['status'] == 'Suspended'){?>
                    <span class="label label-info" style="background-color: #b85e80 !important;margin: 0px !important;"><?=$v['status'] ?></span>
                <?php }else{ ?>
                    <span class="label label-success"><?=$v['status'] ?></span>
                <?php } ?>
            </td>
            <td><?=($v['city']=="")?'Not available':$v['city']?>, <?=($v['country']=="")?'Not available':$v['country']?></td>
            <td><a href="mailto:<?=$v['email']?>"><?=$v['email']?></a></td>
            <td><?=date('M j,Y',$v['created_timestamp']) ?></td>
            <td><?php if($v['visit_time']=="") echo "NOT AVAILABLE"; else echo date('M j,Y',$v['visit_time']);?></td>
            <td><a href="javascript:void(0)" class="btn_pop" id="<?=$v['user_id']?>">Edit</a> &nbsp;&nbsp;<a href="javascript:void(0)" class="btn_delete btn_delete_<?=$v['user_id']?>" id="<?=$v['user_id']?>">Delete</a></td>
        </tr>
    <?php
    }
    ?>

<?php
} else { ?>
<tr style="text-align: center;">
    <td colspan="9"><strong>No match found</strong></td>
</tr>
<?php } ?>
<!--do not remove the following sing-->
<!--|||-->
<form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                   method="post" action="<?= $vObj->getURL('studios/listing')?>">
         <div class="span12 pagination"><?= $pagination ?></div>
         <input type="hidden" value="<?= $page ?>" name="page">
         <input type="hidden" value="<?=$filter_type?>" name="filter_type">
         <input type="hidden" value="<?=$studio_name?>" name="studio_name">
         <input type="hidden" value="<?=$sorting?>" name="sorting">
         <input type="hidden" value="<?=$sort_column?>" name="sort_column">
         <input type="hidden" value="<?=$sort_type?>" name="sort_type">

</form>
