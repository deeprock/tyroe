<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<style>
    .table-head {
        background: linear-gradient(to bottom, #2C3742 0%, #28303A 100%) repeat scroll 0 0 rgba(0, 0, 0, 0);
        color: #D6D6D6;
    }

    .table-footer {
        background: linear-gradient(to bottom, #A4AFBA 0%, #8A929C 100%) repeat scroll 0 0 rgba(0, 0, 0, 0)

    }

</style>
<!-- POP-UP -->
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    /*function CallFunc(obj){
        //alert(132);
        var item_id = obj.id;
        var url = '<?=$vObj->getURL("studios/form/")?>';
        $.ajax({
            type: 'POST',
            url: url,
            data: "item_id=" + item_id,
            success: function (data) {
                if (data) {
                    var jqObj = jQuery(data);
                    $('.modal-title').html(jqObj.find(".headerdiv").html());
                    jqObj.find(".headerdiv").remove();
                    var getfunc = jqObj.find(".btn-glow").attr("onclick");
                    $('.btn-save').attr("onclick", getfunc);
                    jqObj.find(".btn-glow").remove();
                    $('.modal-body').html(jqObj);
                    $('#myModal').modal('show');
                    *//*$('#element_to_pop_up').html(data);
                     var x_axis = Math.floor(window.innerWidth/2);
                     var popup_width=$('#element_to_pop_up').css('width')
                     var actual_width=x_axis-(parseInt(popup_width.replace('px',''))/2)-40;
                     $('#element_to_pop_up').bPopup( {position: [actual_width, 10]});
                     $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');*//*
                }
            }
        });
    }*/
    (function ($) {
      // DOM Ready
        $(function () {
            $("body").delegate('.btn_pop','click', function (e) {
                // Prevents the default action to be triggered.
                e.preventDefault();
                $('.loader-save-holder').show();
                var item_id = this.id;
                var url = '<?=$vObj->getURL("studios/form/")?>';

                $.ajax({
                    type:'POST',
                    url:url,
                    data:"item_id="+item_id,
                    success:function (data) {

                        if (data) {
                            var jqObj = jQuery(data);
                            $('.modal-title').html(jqObj.find(".headerdiv").html());
                            jqObj.find(".headerdiv").remove();
                            var getfunc = jqObj.find(".btn-glow").attr("onclick");
                            $('.btn-save').attr("onclick",getfunc);
                            jqObj.find(".btn-glow").remove();
                            $('.popup-modal-body').html(jqObj);
                            $('#myModal').modal('show');
                            $('.loader-save-holder').hide();
                            $('body').addClass('modal-open');
                            $('body').css({"overflow":"hidden"})
                            /*$('#element_to_pop_up').html(data);
                            var x_axis = Math.floor(window.innerWidth/2);
                            var popup_width=$('#element_to_pop_up').css('width')
                            var actual_width=x_axis-(parseInt(popup_width.replace('px',''))/2)-40;
                            $('#element_to_pop_up').bPopup( {position: [actual_width, 10]});
                            $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');*/
                        }
                    }
                });
            });
            
            
            $("body").delegate('.btn_delete', 'click', function (e) {
                $('#delete-studio-error-message').hide();
                var delete_id = this.id;
                $('#delete_studio_id').attr('value',delete_id);
                $('#delete_studio_type').attr('value','single');
                $('.loader-save-holder').show();
                $.ajax({
                    type: 'POST',
                    data: {
                        delete_id : delete_id
                    },
                    url: '<?=$vObj->getURL("studios/get_studio_team_member");?>',
                    dataType: 'json',
                    success: function (responseData) {
                    console.log(responseData);    
                        if(responseData.reviewer_count > 0){
                            $('#team_member_count').html(' '+responseData.reviewer_count+' team member(s) ');
                            $('#model_confirmation_note').show();
                        }else{
                            $('#model_confirmation_note').hide();
                        }
                        $('.loader-save-holder').hide();
                        $('#myModal_delete_studio_2').modal('show');
                    }
                });
            });
            
//            $("body").delegate('#btn_yes_delete', 'click', function (e) {
//                $('#myModal_delete_studio_1').modal('hide');
//                $('#myModal_delete_studio_2').modal('show');
//            });

            $("body").delegate(".btn-cancel-delete-studio",'click',function(){
                $('#delete_confirm_text').val('');
            });
            
            $("body").delegate('#btn_delete_studio', 'click', function (e) {
                $('#delete-studio-error-message').hide();
                if($.trim($('#delete_confirm_text').val()) == ''){
                    $('#delete-studio-error-message').html("Please enter DELETE text").show();
                    return false;
                }
                
                if($.trim($('#delete_confirm_text').val()) != 'DELETE'){
                    $('#delete-studio-error-message').html("Please enter correct text").show();
                    return false;
                }
                
                
                if($('#delete_studio_type').val() == 'single'){
                    var delete_id = $('#delete_studio_id').val();
                    var obj = $('.btn_delete_'+delete_id);
                    $('#myModal_delete_studio_2').modal('hide');
                    e.preventDefault();
                    $('.loader-save-holder').show();
                    var url = '<?=$vObj->getURL("studios/delete_studio")?>';
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: "delete_id=" + delete_id,
                        dataType: "json",
                        success: function (data) {
                            if (data.success) {

                                $(".abc").show();
                                $(".abc").addClass("alert alert-success");
                                $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                                obj.parents('tr').remove();
                                setTimeout(function () {
                                    $(".abc").fadeOut('1500');
                                }, 2000);
                            } else {
                                $(".abc").show();
                                $(".abc").addClass("alert alert-danger");
                                $(".alert-danger").html('<i class="icon-remove-sign"></i>' + "Fail to delete Studio");
                                setTimeout(function () {
                                    $(".abc").fadeOut('1500');
                                }, 2000);
                            }
                            $('.loader-save-holder').hide();
                            $('#delete_confirm_text').val('');
                        }
                    });
                }else if($('#delete_studio_type').val() == 'bulk'){
                    $('#myModal_delete_studio_2').modal('hide');
                    var delete_ids = $('.bulk_form').serialize();
                    $('.loader-save-holder').show();
                    var url = '<?=$vObj->getURL("studios/bulkaction")?>';
                    $.ajax({
                        type:"POST",
                        data:delete_ids,
                        url: url,
                        dataType:"json",
                        success: function(data){
                            $('.loader-save-holder').hide();
                            $.each(data.json_idz,function (key,val){
                                  $('#Studio-id-'+val).fadeOut(700);
                            });
                            $(".abc").show();
                            $(".abc").addClass("alert alert-success");
                            $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                            setTimeout(function () {
                                $(".abc").fadeOut('1500');
                            }, 2000);
                        }
                    });
                }
                $('#delete_confirm_text').val('');
            });


            $('#studio_name').donetyping(function (){
                $('.loader-save-holder').show();
                    var studio_name = $(this).val();
                    var action = $('#filter_studio').val();
                    var data = "bool=" + true +"&studio_name=" + studio_name+"&filter_type="+action;
                    $.ajax({
                        type: "POST",
                        data: data,
                        url: '<?=$vObj->getURL("studios/listing")?>',
                        success: function (data) {
                            $('.loader-save-holder').hide();
                            var alldata = data;
                            var data_arr = alldata.split('<!--|||-->');
                            $("tbody.all_studios").html(data_arr[0]);
                            $('#footer_pagination').html(data_arr[1]);
                            $('.loader-save-holder').hide();
                        }

                    });
                    return false;
            },500);

            $('#bulkaction').on('click',function(){
                var delete_ids = $('.bulk_form').serialize();
                if(delete_ids==""){
                    alert("Please select a record first");
                }else{
                    $('#myModal_delete_studio_2').modal('show');
                    $('#delete_studio_type').attr('value','bulk');
                }
            });

            $('#filter_studio').on('change',function(){
              // alert($(this).val());
                $('.loader-save-holder').show();
                var action=$(this).val();
                var studio_name = $('#studio_name').val();
                var data = "bool=" + true +"&studio_name=" + studio_name+"&filter_type="+action;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?=$vObj->getURL("studios/listing")?>',
                    success: function (data) {
                        $('.loader-save-holder').hide();
                        var alldata = data;
                        var data_arr = alldata.split('<!--|||-->');
                        $("tbody.all_studios").html(data_arr[0]);
                        $('#footer_pagination').html(data_arr[1]);
                    }
                });

            });

            $('.sorter').on('click',function(){
               $('.loader-save-holder').show();
               var sort_col = $(this).attr('data-column');
               var sort_type = $(this).attr('data-stype');
               var studio_name = $('#studio_name').val();
               var url = '<?=$vObj->getURL("studios/listing")?>';
               var obj = $(this);
               $.ajax({
                   type:"POST",
                   data:"sort_column="+sort_col+"&sort_type="+sort_type+"&sorting="+true+"&bool="+true+"&studio_name="+studio_name,
                   url:url,
                   success:function(data){
                       var alldata = data;
                       var data_arr = alldata.split('<!--|||-->');
                       $("tbody.all_studios").html(data_arr[0]);
                       $('#footer_pagination').html(data_arr[1]);
                       if(sort_type=="ASC")
                       {
                           obj.attr('data-stype','DESC');
                       }
                       else
                       {
                           obj.attr('data-stype','ASC');
                       }
                       $('.loader-save-holder').hide();
                   }
               });
           });



});

    })(jQuery);
</script>
<script>
    /*$(function(){
        $(".btn-edit").on('click',function(){
            $('#myModal').modal('show');
        });
    });*/
</script>
<!-- POP-UP -->
<div style="display: none;" class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="row-fluid header z-margin-b tr-mr">
                <div id="admin_users_top_header" class="span9">
                    <h5 class="resume-title"><?= LABEL_MANAGE_STUDIOS ?></h5>
                    <input class="span4 pull-left search tr-search" type="text" placeholder="Studio Name" name="studio_name" id="studio_name" value="<?=$studio_name?>">
                    <select name="filter_std" class="filter_studio margin-default btn" id="filter_studio">
                             <option value="" style="display: none;">Filter Studios</option>
                             <option <?php  if($filter_type=='alphabetical') echo 'Selected="selected"';?> value="alphabetical">Alphabetical</option>
                             <option <?php  if($filter_type=='mostrecent') echo 'Selected="selected"';?> value="mostrecent">Most Recent</option>
                    </select>
                </div>
                <div class="span3 topRightHead">
                    <a href="javascript:void(0)" class="pull-left selectedDel" id="bulkaction">Delete Selected</a>
                    <a href="javascript:void(0)" class="btn-flat primary go-btn pull-right btn_pop no-margin custom-btn-color">+ <?=LABEL_TOOLBAR_NEW?> Studio</a>
                </div>
            </div>

                <div class="row-fluid show-grid">
                    <?php
                    echo $this->session->userdata('success_message');
                    $this->session->unset_userdata('success_message');
                    ?>
                </div>
                <div class="show-grid margin-adjust">

                    <div class="clearfix"></div>
                </div>
                <!--<div class="abc" style="display:none">
                    <i class="icon-warning-sign"></i> Please select any record
                </div>-->

                <div class="abc" style="display:none">
                    <i class="icon"></i> <!--Please select any record-->
                </div>
                <form class="bulk_form inline-input">
                <table class="table table-hover tr-thead tr-pd">
                    <thead>
                    <tr>
                        <th><div class="position-relative"><input type="checkbox" name="studio_master_check" class="studio_master_check" onchange="multiplechecks($(this),'.studio_check');"></div></th>
                        <th><div class="position-relative"><a href="javascript:void(0)" class="sorter" data-column="st.user_id" data-stype="ASC">ID</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="c.company_name" data-stype="ASC">Name</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="stat.status" data-stype="ASC">Status</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="cont.country" data-stype="ASC">Country</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="st.email" data-stype="ASC">Email</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="st.created_timestamp" data-stype="ASC">Signed Up</a></div></th>
                        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="tpv.visit_time" data-stype="ASC">Last Visit</a></div></th>
                        <th><div class="position-relative"><span class="line"></span></div></th>
                    </tr>
                    </thead>
                    <tbody class="all_studios team-tr">
                    <?php
                    foreach ($data as $k => $v) {
                        ?>
                    <tr id="Studio-id-<?=$v['user_id']?>">
                        <td align="center"><input type="checkbox" name="bulk_check[]" class="studio_check"
                                                  value="<?=$v['user_id']?>"></td>
                        <td><?=$v['user_id']?></td>
                        <td><p class="rt-name"><?=$v['company_name']?></p>
                            <span class="rt-title"><?=$v['industry_name']?></span></td>
                        <td class="status_td">
                            <?php if($v['status'] == 'Pending'){?>
                                <span class="label label-info rt-pending" style="background-color: #da4f49;"><?=$v['status'] ?></span>
                           <?php }else if($v['status'] == 'Suspended'){?>
                                <span class="label label-info" style="background-color: #b85e80 !important;margin: 0px !important;"><?=$v['status'] ?></span>
                            <?php }else{ ?>
                                <span class="label label-success"><?=$v['status'] ?></span>
                            <?php } ?>
                        </td>
                        <td><?=($v['city']=="")?'Not available':$v['city']?>, <?=($v['country']=="")?'Not available':$v['country']?></td>
                        <td><a href="mailto:<?=$v['email']?>"><?=$v['email']?></a></td>
                        <td><?=date('M j,Y',$v['created_timestamp']) ?></td>
                        <td><?php if($v['visit_time']=="") echo "Not available"; else echo date('M j,Y',$v['visit_time']);?></td>
                        <td><a href="javascript:void(0)" class="btn_pop" id="<?=$v['user_id']?>">Edit</a> &nbsp;&nbsp;<a href="javascript:void(0)" class="btn_delete btn_delete_<?=$v['user_id']?>" id="<?=$v['user_id']?>">Delete</a></td>
                    </tr>
                        <?php
                    }
                    ?>

                    </tbody>

                </table>
                </form>
                <div id="footer_pagination">
                    <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
                                  method="post" action="<?= $vObj->getURL('studios/listing') ?>">
                        <div class="span12 pagination"><?= $pagination ?></div>
                        <input type="hidden" value="<?= $page ?>" name="page">
                        <input type="hidden" value="<?=$filter_type?>" name="filter_type">
                        <input type="hidden" value="<?=$studio_name?>" name="studio_name">
                        <input type="hidden" value="<?=$sorting?>" name="sorting">
                        <input type="hidden" value="<?=$sort_column?>" name="sort_column">
                        <input type="hidden" value="<?=$sort_type?>" name="sort_type">
                    </form>
                </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div  style="display: none;" class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup rt-md-width">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="global-popup_body popup-modal-body padding-7">
                    ...
                </div>
                <div class="clearfix"></div>
                <div class="global-popup_footer modal-footer ft-padding-7">
                    <div class="container-fluid">
                    <div class="row-fluid">
                        <div class="span12">
                        <div class="span3"></div>
                        <div class="span9 tab-margin">
                        <button type="button" class="btn btn-default white-close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save rt-save">Save changes</button>
                    </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div><!-- /.modal -->

<input type="hidden" id="delete_studio_id" value=""/>
<input type="hidden" id="delete_studio_type" value=""/>
<div style="display: none;" class="modal myModal_delete_studio_1" id="myModal_delete_studio_1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
           
            <div class="modal-body">
                <p class="delete-message-1">Are you sure you want to delete studio ?</p> 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-flat white btn-yes-delete" id="btn_yes_delete">Yes</button>
                <button type="button" class="btn btn-flat white" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<div style="display: none;" class="modal myModal_delete_studio_2" id="myModal_delete_studio_2" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Confirm Account Deletion</h4>
            </div>
            <div class="modal-body">
                <div class="alert alert-warning">
                   <i class="icon-warning-sign"></i>
                   <div class="confirmation-text">
                       <span class="l1">Are you sure ?</span><br/>
                       <span class="l2">All account data will be permanently deleted. This can't be undone.</span><br/>
                       <span class="note" id="model_confirmation_note"><strong>Note:</strong> There are <strong id="team_member_count">4 team member(s)</strong>of Tyroe who will no longer have access to account</span><br/>
                   </div>
                   <div class="clearfix"></div>
                </div>
                <p class="delete-message-2">Type DELETE to confirm</p>
                <p>
                    <textarea class="delete-confirm-text" id="delete_confirm_text"></textarea>
                </p>
                <label class="delete-studio-error-message" id="delete-studio-error-message">Error message</label>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-glow btn-cancel-delete-studio" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-glow primary btn-delete-studio" id="btn_delete_studio">Delete Studio</button>
            </div>
        </div>
    </div>
</div>

<!-- end main container -->




