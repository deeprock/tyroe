<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<style>
    .resume_info, .portfolio_info, .subscription_info {
        display: none;
    }

    .slider-frame {
        background-color: #d5dde4;
        border-radius: 15px;
        box-shadow: 0 1px 5px 0 rgba(0, 0, 0, 0.3) inset;
        display: inline-block;
        height: 23px;
        margin: 0 auto;
        position: relative;
        width: 87px;
    }

    .slider-button{
        background: none repeat scroll 0 0 #fff;
        border: 1px solid #d0dde9;
        border-radius: 9px;
        color: #000;
        cursor: pointer;
        display: block;
        font-family: sans-serif;
        font-size: 11px;
        font-weight: bold;
        height: 21px;
        line-height: 23px;
        text-align: center;
        transition: all 0.25s ease-in-out 0s;
        width: 56px;
    }
</style>
<script type="text/javascript">

    function isValidEmail(emailText) {
        var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
        return pattern.test(emailText);
    }

    function AddStudio() {

        var studio_status = $("#studio_status").val();
        var company_id = $("#company_id").val();
        var company_name = $("#company_name").val();
        var company_country = $('select[name="company_country"]').val();
        //var company_state = $('select[name="company_state"]').val();
        var company_city = $('select[name="company_city"]').val();
        var industry = $('select[name="industry_id"]').val();
        //alert(company_id+" "+company_name+" "+company_country+" "+company_state+" "+company_city);
        var studio_id = $("#studio_id").val();
        var firstname = $("#firstname").val();
        var lastname = $("#lastname").val();
        var username = $("#username").val();
        var email = $("#email").val();
        var password = $("#password").val();
        var confirmpass = $("#confirmpass").val();
        var user_country = $('select[name="studio_country"]').val();
        //var user_state = $('select[name="studio_state"]').val();
        var user_city = $('select[name="studio_city"]').val();
        var cellphone = $("#user_cellphone_code").val() + '-' + $("#cellphone").val();
        var flag = $("#flag").val();
        var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
        var email_check = '/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,6}$/';
        if (company_name == '') {
            $('#company_name').css({ border: '1px solid red'});
            $('#company_name').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#company_name').removeAttr("style")
        }
        if (company_country == '') {
            $('#company_country').css({ border: '1px solid red'});
            $('#company_country').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#company_country').removeAttr("style")
        }
        if (industry == '') {
            $('#industry_id').css({ border: '1px solid red'});
            $('#industry_id').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#industry_id').removeAttr("style")
        }
      /*  if (company_state == '') {
            $('#company_state').css({ border: '1px solid red'});
            $('#company_state').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#company_state').removeAttr("style")
        }*/
        if (company_city == '') {
            $('#company_city').css({ border: '1px solid red'});
            $('#company_city').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#company_city').removeAttr("style")
        }
        if (firstname == '') {
            $('#firstname').css({ border: '1px solid red'});
            $('#firstname').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#firstname').removeAttr("style")
        }
        if (lastname == '') {
            $('#lastname').css({ border: '1px solid red'});
            $('#lastname').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#lastname').removeAttr("style")
        }
        if (username == '') {
            $('#username').css({ border: '1px solid red'});
            $('#username').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#username').removeAttr("style")
        }
        if (email == '') {
            $('#email').css({ border: '1px solid red'});
            $('#email').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else if (!isValidEmail(email)) {
            $('.modal_error_box').show();
            $('.modal_error_box').html('<strong>Error!</strong> Invalid email address');
            return false;
        } else {
            $('#email').removeAttr("style")
        }

        if (flag == 'add') {
            if (password == '') {
                $('#password').css({ border: '1px solid red'});
                $('#password').focus();
                $('.modal_error_box').show();
                $('.modal_error_box').html(error_empty);
                return false;
            } else {
                $('#password').removeAttr("style")
            }
            if (confirmpass == '') {
                $('#confirmpass').css({ border: '1px solid red'});
                $('#confirmpass').focus();
                $('.modal_error_box').show();
                $('.modal_error_box').html(error_empty);
                return false;
            } else {
                if (password == confirmpass) {
                    $('#confirmpass').removeAttr("style")
                } else {
                    $('#confirmpass').css({ border: '1px solid red'});
                    $('#confirmpass').focus();
                    $('.modal_error_box').show();
                    $('.modal_error_box').html('<strong>Error!</strong> Password not matched');
                    return false;
                }
            }
        }
        if (user_country == '') {
            $('#studio_country').css({ border: '1px solid red'});
            $('#studio_country').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#studio_country2').removeAttr("style")
        }
        /*if (user_state == '') {
            $('#studio_state').css({ border: '1px solid red'});
            $('#studio_state').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#studio_state').removeAttr("style")
        }*/
        if (user_city == '') {
            $('#studio_city').css({ border: '1px solid red'});
            $('#studio_city').focus();
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
            return false;
        } else {
            $('#studio_city').removeAttr("style")
        }
        $('.loader-save-holder').show();
        var data = "company_name=" + company_name + "&company_country=" + company_country + "&company_city=" + company_city
            + "&firstname=" + firstname + "&lastname=" + lastname + "&username=" + username + "&email="
            + email + "&password=" + password + "&confirmpass=" + confirmpass
            + "&user_country=" + user_country + "&user_city=" + user_city
            + "&cellphone=" + cellphone
            + "&flag=" + flag + "&company_id=" + company_id + "&studio_id=" + studio_id + "&industry=" + industry
            + "&studio_status=" + studio_status;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("studios/save")?>',
            dataType: "json",
            success: function (data) {
                $('.loader-save-holder').hide();
                if (data.duplicate_email) {
                    $('#email').css({ border: '1px solid red'});
                    $('#email').focus();
                    $('.modal_error_box').show();
                    $('.modal_error_box').html('<strong>Error!</strong> Email already exist, try a different email address');
                } else if (data.duplicate_username) {
                    $('#username').css({ border: '1px solid red'});
                    $('#username').focus();
                    $('.modal_error_box').show();
                    $('.modal_error_box').html('<strong>Error!</strong> Username already Registered, try a different username');
                } else if (data.duplicate_company) {
                    $('#company_name').css({ border: '1px solid red'});
                    $('#company_name').focus();
                    $('.modal_error_box').show();
                    $('.modal_error_box').html('<strong>Error!</strong> Company name already exist, try a different company name');
                } else {
                    $(".abc").addClass("alert alert-success");
                    $(".alert-success").html('<i class="icon-ok-sign"></i>' + data.success_message);
                    $('.modal-backdrop, .modal-backdrop.fade.in').css('opacity', '0');
                    $('.modal.fade.in').hide();
                    $('.alert-success').fadeIn(500);
                    $('.alert-success').delay(2000).fadeOut(1500);
                    if (data.flag == '1') {
                        $('tbody.all_studios').prepend(data.html)
                    } else {
                        var studio_id = '#Studio-id-' + data.id;
                        $(studio_id).html(data.html);
                    }
                }
            },
            failure: function (errMsg) {
            }
        });
    }

    function display_section(obj, visible) {
        $('.basic_info').hide();
        $('.resume_info').hide();
        $('.portfolio_info').hide();
        $('.subscription_info').hide();
        $('.' + visible).show();
        $('#active').removeAttr('id');
        $(obj).closest('.row-fluid').attr('id', 'active')
    }


//Gets states and city by country
    function getcitiesstates(country_id,stateBox,citiesBox){
        //alert(country_id);
        $('.loader-save-holder').show();
        $.ajax({
            type: "POST",
            data:{country_id:country_id},
            url: '<?=$vObj->getURL("studios/get_cities_states")?>',
            success: function(data){
                $('.loader-save-holder').hide();
                $('#AjaxResponse').html(data);
                /*var states = $('#AjaxResponse').find('#StatesDiv').html();*/
                var cities = $('#AjaxResponse').find('#CitiesDiv').html();
                /*$(stateBox).html(states);*/
                $(citiesBox).html(cities);
            }
        });
    }

    function slider(){
        var this_ = $('.slider-button');
        if ($(this_).hasClass("on")) {
            $("#studio_status").val(0);
            $(this_).closest('div').removeClass('success');
            $(this_).removeClass('on').html($(this_).data("off-text"));
        } else {
            $("#studio_status").val(1);
            $(this_).addClass('on').html($(this_).data("on-text"));
            $(this_).closest('div').addClass('success');
        }
    }

    /*$(document).ready(function (){
        // Switch slide buttons
        $('.slider-button').click(function () {
            console.log("DDdddd");
            if ($(this).hasClass("on")) {
                $(this).closest('div').removeClass('success');
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
                $(this).closest('div').addClass('success');
            }
        });
    });*/
</script>

<!-- START RIGHT COLUMN -->
<div style="display: none;" id="AjaxResponse"></div>
<div class="container-fluid">
<div class="row-fluid">
<div class="span12 tab-margin rt-studio-md">
    <div class="row-fluid">
        <div class="span4 default-header headerdiv">
            <h4>
                <?php
                if ($flag == "edit") {
                    echo TITLE_ACTION_EDIT . " " . LABEL_STUDIO;
                } else {
                    echo LABEL_TOOLBAR_NEW . " " . LABEL_STUDIO;
                }
                ?>
            </h4>
        </div>
        <div class="modal_error_box alert alert-danger">
        </div>
        <div class="field-box error">
                    <span><?php echo validation_errors();

                        if ($msg != "") {
                            echo $msg;
                        } ?></span>
        </div>

        <form class="inline-input" _lpchecked="1"
              enctype="multipart/form-data" name="register_form" method="post">
            <div class="basic_info">
<!--
                 STUDIO STATUS 
                <div class="field-box span12 tab-margin bottom-margin">
                    <h4><a href="javascript:void(0)">Studio Status</a></h4>
                </div>
                <div class="clearfix"></div>
                <div class="span12 pull-left social-acnt-link bottom-margin">
                    <div class="slider-frame <?= $class ?>">
                                   <span
                                           class="slider-button <?= $class_span ?>"
                                           type-value='<?= $studio_status ?>'
                                           data-off-text="Pending"
                                           data-on-text="Active" onclick="slider()"><?= ($studio_status_text=="")?'Pending':$studio_status_text ?></span>
                    </div>
                    <input type="hidden" value="<?=($studio_status=="")?0:$studio_status?>" id="studio_status" name="studio_status">
                </div>-->
                <input type="hidden" value="<?=($studio_status=="")?0:$studio_status?>" id="studio_status" name="studio_status">
                <div class="clearfix"></div>
                <!-- STUDIO STATUS -->

                <div class="field-box span5 tab-margin bottom-margin">
                    <h4><a href="javascript:void(0)">Studio Details</a></h4>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span12 tab-margin bottom-margin   ">
                    <div class="span3 tab-margin"><label><?= LABEL_NAME_TEXT ?>:</label></div>
                    <input name="company_name" id="company_name" class="span9 md-sp" type="text"
                           value="<?= $company_name ?>">
                </div>
                <div class="clearfix"></div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label>Country:</label></div>
                    <div class="ui-select span9">
                       <select id="company_country" name="company_country" onchange="getcitiesstates(this.value,'#company_state','#company_city')">
                           <option value="" class="display_hide">Select Country</option>
                        <?php //$user_country_dropdown;
                        foreach($get_countries as $country)
                        {
                            $selected="";
                            if($country['id']==$company_country)
                            {
                                $selected="selected=selected";
                            }
                            ?>
                            <option <?=$selected?> value="<?=$country['id']?>"><?=$country['name']?></option>
                        <?php
                        }
                        ?>
                       </select>
                    </div>
                </div>
                <div class="clearfix"></div>

                <!--<div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?php/*= LABEL_STATE */?>:</label></div>
                    <div class="result-states1"></div>
                    <div class="ui-select span9 all-states1">
                        <select class="span3 inline-input" name="company_state" id="company_state">
                            <option value="" class="display_hide">Select State</option>
                            <?php
/*                            foreach ($get_states_company as $state) {
                                $selected="";
                                if($state['states_id']==$company_state)
                                    $selected="selected=selected";
                                */?>
                                <option <?php/*=$selected*/?> value="<?php/*=$state['states_id']; */?>"><?php /*echo $state['states_name']; */?></option>
                            <?php
/*                            }
                            */?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>-->
                <div class="field-box span12 tab-margin bottom-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
                    <div class="ui-select span9 all-states1">
                        <select class="span3 inline-input" name="company_city" id="company_city">
                            <option value="" class="display_hide">Select City</option>
                            <?php
                            foreach ($get_cities_company as $city) {
                                $selected="";
                                if($city['city']==$company_city)
                                    $selected="selected=selected";
                                ?>
                                <option <?=$selected?> value="<?=$city['city'] ?>"><?=$city['city']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                   <div class="field-box span12 tab-margin bottom-margin">
                       <div class="span3 tab-margin"><label><?= LABEL_JOB_INDUSTRY ?>:</label></div>
                       <div class="ui-select span9 all-states1">
                           <select class="span3 inline-input" name="industry_id" id="industry_id">
                               <option value="" class="display_hide">Industry</option>

                               <?php
                               echo '<pre>';print_r($job_industries);echo '</pre>';
                               foreach ($job_industries as $industry) {

                                   $selected="";
                                   if($industry['industry_id']==$industry_id)
                                       $selected="selected=selected";
                                   ?>
                                   <option <?=$selected?> value="<?=$industry['industry_id'] ?>"><?=$industry['industry_name']?></option>
                               <?php
                               }
                               ?>
                           </select>
                       </div>
                   </div>
                <div class="clearfix"></div>
                <div class="field-box bottom-margin">
                    <h4><a href="javascript:void(0)">Studio Admin Account Details</a></h4>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_FIRST_NAME ?>:</label></div>
                    <div class="span9 tab-margin"><input class="span12 md-sp" type="text"
                                                         name="firstname" id="firstname"
                                                         value="<?= $firstname ?>"></div>
                </div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_LAST_NAME ?>:</label></div>
                    <div class="span9 tab-margin"><input class="span12 md-sp" type="text"
                                                         name="lastname" id="lastname"
                                                         value="<?= $lastname ?>"></div>
                </div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_USER_NAME ?>:</label></div>
                    <div class="span9 tab-margin"><input class="span12 md-sp" type="text"
                                                         name="username" id="username"
                                                         value="<?= $username ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_EMAIL ?>:</label></div>
                    <div class="span9 tab-margin"><input name="email" id="email" class="span12 md-sp" type="text"
                                                         value="<?= $email ?>"></div>
                </div>
                <div class="clearfix"></div>
                <?php if (empty($flag) || $flag != 'edit') { ?>
                    <div class="field-box span12 tab-margin bottom-margin">
                        <div class="span3 tab-margin"><label><?= LABEL_PASSWORD ?>:</label></div>
                        <div class="span9 tab-margin"><input class="span12 inline-input md-sp" type="password"
                                                             name="password" id="password"
                                                             value="<?= $password ?>"></div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="field-box span12 tab-margin bottom-margin">
                        <div class="span3 tab-margin"><label><?= LABEL_CONFIRM_PASSWORD ?>:</label></div>
                        <div class="span9 tab-margin">
                            <input class="span12 inline-input md-sp" type="password"
                                   name="confirmpass" id="confirmpass"
                                   value="<?= $confirmpass ?>"></div>
                    </div>
                    <div class="clearfix"></div>
                    <input type="hidden" value="add" name="flag" id="flag">
                <?php } else { ?>
                    <input type="hidden" value="edit" name="flag" id="flag">
                    <input type="hidden" value="<?= $company_id ?>" name="company_id" id="company_id">
                    <input type="hidden" value="<?= $studio_id ?>" name="studio_id" id="studio_id">
                <?php } ?>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label>Country:</label></div>
                    <!--<div class="span4 tab-margin">-->
                    <div class="ui-select span9">
                        <select id="studio_country" name="studio_country" onchange="getcitiesstates(this.value,'#studio_state','#studio_city')">
                            <option value="" class="display_hide">Select Country</option>
                        <?php //$user_country_dropdown;
                        foreach($get_countries as $country)
                        {
                            $selected="";
                            if($country['id']==$studio_country)
                            {
                                $selected="selected=selected";
                            }
                            ?>
                            <option <?=$selected?> value="<?=$country['id']?>"><?=$country['name']?></option>
                        <?php
                        }
                        ?>
                       </select>
                    </div>
                </div>

                </div>
                <div class="clearfix"></div>
                <!--<div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?php/*= LABEL_STATE */?>:</label></div>
                    <div class="span9 tab-margin">
                        <div class="result-states2"></div>
                        <div class="ui-select span12 all-states2">
                            <select class="span3 inline-input" name="studio_state" id="studio_state">
                                <option value="" class="display_hide">Select State</option>
                                <?php
/*                                foreach ($get_states_studio as $state) {
                                    $selected="";
                                    if($state['states_id']==$studio_state)
                                        $selected="selected=selected";
                                    */?>
                                    <option <?php/*=$selected*/?> value="<?php/*=$state['states_id']*/?>"><?php /*echo $state['states_name']*/?></option>
                                <?php
/*                                }
                                */?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>-->
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_CITY ?>:</label></div>
                    <div class="ui-select span9 all-states1">
                        <select class="span3 inline-input" name="studio_city" id="studio_city">
                            <option value="" class="display_hide">Select City</option>
                            <?php
                            foreach ($get_cities_studio as $city) {
                                $selected="";
                                if($city['city']==$studio_city)
                                    $selected="selected=selected";
                                ?>
                                <option <?=$selected?> value="<?=$city['city'] ?>"><?=$city['city']?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span12 tab-margin bottom-margin">
                    <div class="span3 tab-margin"><label><?= LABEL_CELLPHONE ?>:</label></div>
                    <div class="span9 tab-margin">
                        <input class="span4 md-sp" type="number"
                               name="user_cellphone_code" id="user_cellphone_code"
                               value="<?php echo $code_cellphone;
                               ?>" maxlength="3">
                        <input class="span8 pull-right md-sp" type="text"
                               name="cellphone" id="cellphone"
                               value="<?= $cellphone ?>"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="span12 field-box actions">
                <input type="button" name="submit" onclick="AddStudio()" class="btn-glow primary"
                       value="<?= LABEL_SAVE_CHANGES ?>">
                <!--<span>OR</span>
                <input type="reset" value="Cancel" class="reset">-->
            </div>
        </form>
    </div>
</div>
</div>
    <span id="message"></span>
</div>

<!-- END RIGHT COLUMN -->





