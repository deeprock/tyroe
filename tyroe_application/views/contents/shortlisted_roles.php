<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="content">

    <div class="container-fluid">

        <div id="pad-wrapper">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column">
                            <div class="span10 default-header">
                                                            <h4><?= LABEL_SHORTLISTED_ROLES ?></h4>
                                                        </div>
                            <div class="filter-result"></div>
                            <!-- START OPENING DETAILS -->
                            <?php
                            if ($get_shorlisted_jobs[0]['job_id']!="") {

                                for ($tyr = 0; $tyr < count($get_shorlisted_jobs); $tyr++) {
                                    ?>
                                    <div
                                        class="row-fluid no-filter job-opening<?php echo $get_shorlisted_jobs[$tyr]['job_id']; ?>">
                                        <div class="span12 group" id="top">

                                            <!-- TITLE -->
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <h5>
                                                        <a href="javascript:void(0);">
                                                            <?php echo $get_shorlisted_jobs[$tyr]['job_title']; ?>
                                                        </a>
                                                    </h5>
                                                </div>
                                                <div class="span4 span_result">
                                                    <a>Not Available Now?</a>
                                                </div>

                                            </div>

                                            <!-- INFO -->
                                            <div class="row-fluid" id="opening-stats">
                                                <div class="row-fluid">
                                                    <div class="span8">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                                <span style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                       Studio:</p><span
                                                                        style="width:290px; float:left;"> <strong><?php echo $get_shorlisted_jobs[$tyr]['username']; ?></strong></span></span>

                                                                <span style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                        Reviewer:</p><span
                                                                        style="width:290px; float:left;"><strong><?php echo $get_shorlisted_jobs[$tyr]['username']; ?></strong></span></span>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row-fluid" style="margin-top: 20px;">
                                                    <div class="span8">
                                                        <div class="row-fluid">
                                                            <div class="span12">
                                                        <span
                                                            style="width: 100%; float:left;"><p
                                                                style="width: 120px; float:left;">
                                                               Country:
                                                            </p><span
                                                                style="width:290px; float:left;"><?php echo $get_shorlisted_jobs[$tyr]['job_location']; ?></span></span>




                                                                <span
                                                                    style="width: 100%; float:left;"><p
                                                                        style="width: 120px; float:left;">
                                                                       City:</p><span
                                                                        style="width:290px; float:left;"><?php echo $get_shorlisted_jobs[$tyr]['job_location']; ?>
                                                                       </span></span>



                                                                        <span
                                                                            style="width: 100%; float:left;"><p
                                                                                style="width: 120px; float:left;">
                                                                               Skills:</p><span
                                                                                style="width:290px; float:left;"><?php
                                                                                for ($a = 0; $a < count($get_shorlisted_jobs[$tyr]['skills']); $a++) {
                                                                                    echo $get_shorlisted_jobs[$tyr]['skills'][$a]['creative_skill'] . ", ";

                                                                                }

                                                                                ?>
                                                                            </span></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="span4">
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                Studio:</p><span
                                                                style="width:112px; float:left;"><?php echo $get_shorlisted_jobs[$tyr]['username']; ?></span></span>
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                Date:</p><span
                                                                style="width:112px; float:left;"><?php echo date('d M', $get_shorlisted_jobs[$tyr]['start_date']) . " - " . date('d M', $get_shorlisted_jobs[$tyr]['end_date']); ?></span></span>
                                                        <span style="width: 100%; float:left;"><p
                                                                style="width: 100px; float:left; margin-left: 0;">
                                                                Quantity:</p><span
                                                                style="width:112px; float:left;"><?php echo $get_shorlisted_jobs[$tyr]['job_quantity']; ?></span></span>


                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>
                                <?php
                                }
                            }
                            else{
                                ?>
                                <div class="row-fluid">
                                    <div id="top" class="span12 people">
                                        <span class="name">
                                    <?php
                                     echo "No record";
                                    ?>
                                        </span>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>


                            <!-- END OPENING DETAILS -->

                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->

                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
