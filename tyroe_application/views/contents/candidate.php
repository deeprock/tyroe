<script type="text/javascript">
var total_person = <?php echo count($get_candidates); ?>;

<?php if($get_candidates!= ''){
        $candidate_count = count($get_candidates);
     }else{
        $candidate_count = 0;
    }
    
    $class = '';
    if($job_detail['archived_status'] == 1){
        $class = 'disabled';
    }
    ?>
$(function () {
    //$('.rt-last-sec input[type="radio"]').select2();
    $('.job_backicon').hide();
    $('.candidate_counter').text(<?php echo $candidate_count; ?>)
});
function shortlist(jid, user_id) {
    var data = "jid=" + jid + "&user_id=" + user_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("openingoverview/shortlisting")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".candidate" + user_id).hide(500);
                //$("#message").html(data.success_message);
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide(200);
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide(200);
                }, 5000);
            }
        },
        failure: function (errMsg) {
        }
    });
}
function hideuser(tyroe_id, jobid) {
    var data = "action=candidate_hide&job_id=" + jobid + "&tyroe_id=" + tyroe_id;
    var candidate_count = $('.candidate_counter').text();
    candidate_count = parseInt(candidate_count) - 1;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".row-id" + tyroe_id).hide(500);
                $(".editor-seperator-row" + tyroe_id).hide(500);
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
            $('.candidate_counter').text(candidate_count);
        },
        failure: function (errMsg) {
        }
    });
}
//function of hide addto fav and mail sending placed on 6/3/2014
var msg_to = '';
$(document).ready(function () {
    $('#myModal_candidate').modal('hide');
    //Suggestion meesage Sending
});
$(document).on("click", ".btn-msg", function () {
    $(".sugg-msg").html("");
    msg_to = $(this).attr("data-value");
    $("#hd_email_to" + msg_to).val(msg_to);
    $('#myModal_candidate').modal('show');
    rescale();
    $('.modal-body').css('height', 146);
});
$(document).on("click", ".btn-send-email", function () {
    if ($('#email-subject').val() == "") {
        $('#email-subject').css({
            border: '1px solid red'
        });
        $('#email-subject').focus();
        return false;
    }
    if ($('#email-subject').val() != "") {
        $('#email-subject').css({
            border: ''
        });
    }
    if ($('.email-message').val() == "") {
        $('.email-message').css({
            border: '1px solid red'
        });
        $('.email-message').focus();
        return false;
    }

    if ($('.email-message').val() != "") {
        $('.email-message').css({
            border: ''
        });
    }
    //else{
    $(this).text('Sending..');
    $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
    $(this).removeClass('btn-send-email');
    $(this).addClass('add_email_dummyclass');

    var email_message = $(".email-message").val();
    var email_subject = $("#email-subject").val();
    var email_to = msg_to;
    var data = "callfrom=send_email&email_message=" + email_message + "&email_subject=" + email_subject + "&email_to=" + email_to;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/access_module");?>',
        dataType: 'json',
        success: function (responseData) {
            if (responseData.success) {
                $(".sugg-msg").show();
                $(".sugg-msg").html("Your message send success.");
                $('.add_email_dummyclass').text('Send');
                $('.add_email_dummyclass').addClass('btn-send-email');
                $('.btn-send-email').removeClass('add_email_dummyclass').hide();
                $('.small_loader').remove();
                $('.email_close').show();
            }
        }
    });
    //return true;

    //}
});
function rescale() {
    var size = {width: $(window).width(), height: $(window).height() }
    var offset = 20;
    var offsetBody = 240;
    //$('#myModal').css('height', size.height - offset );
    $('.modal-body').css('height', size.height - (offset + offsetBody));
    $('#myModal_candidate').css('top', '3%');
}

$(window).bind("resize", rescale);
/*Add in openings*/
$(document).on("click", ".add_in_opening_jobs", function () {
    /*First Param =  JOB ID*/
    /*Second Param =  Tyroe ID*/
    $('.loader-save-holder').show();
    var job_tyroe_id = $(this).data('value');
    var job_tyroe_arr = (job_tyroe_id).split(",");
    var job_id = job_tyroe_arr[0];
    var tyroe_id = job_tyroe_arr[1];
    $(this).addClass('disable_opening');
    $(this).removeClass('add_in_opening_jobs');
    $.ajax({
        type: "POST",
        url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
        dataType: "json",
        data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'candidates'},
        success: function (data) {
            if (data.success) {
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
            $('.loader-save-holder').hide();
        },
        failure: function (errMsg) {
        }
    });
});
/*Add in openings*/

/*Add profile in favourite*/
$(document).on("click", ".fav_profile", function favouriteCandidate() {
    $('.loader-save-holder').show();
    var uid = $(this).data('value');
    var data = "uid=" + uid;
    var current_btn = this;
    var current_star = $(this).find("i");
    if ($(current_btn).hasClass('star_btn')) {
        $(current_btn).css("background", "#2388d6");
        $(current_star).css("color", "#ffffff");
        $(current_btn).removeClass('star_btn')
    } else {
        $(current_btn).css("background", "#ffffff");
        $(current_star).css("color", "#2388d6");
        $(current_btn).addClass('star_btn')
        $(this).blur();
    }
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(current_btn).css("background", "#2388d6");
                $(current_star).css("color", "#ffffff");
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(current_btn).css("background", "");
                $(current_star).css("color", "");
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
            $('.loader-save-holder').hide();
        },
        failure: function (errMsg) {
        }
    });
});
/*Add profile in favourite*/

/*Hide Search*/
$(document).on("click", ".hide_search", function () {
    var tyroe_id = $(this).data('value');
    $.ajax({
        type: "POST",
        data: {tyroe_id: tyroe_id},
        url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".row-id" + tyroe_id).hide(500);
                $(".editor-seperator-row" + tyroe_id).hide(500);
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
        },
        failure: function (errMsg) {
        }
    });
});
/*Hide Search*/


var total_candidate = '<?php echo count($get_candidates); ?>';

//create pagination
var jquery_total_page = $('.filter_editor .jquery_pagination').size();
var start_pages = 1;
if (jquery_total_page > 5) {
    var end_pages = 5;
} else {
    var end_pages = jquery_total_page;
}
//create pages
var jquery_pagination_html = '';
var page_number = 1;
var pre_page = 0;
pagination_genrator();
</script>



<!-- START RIGHT COLUMN -->
<div class="row-fluid show-grid">
<!-- START LEFT COLUMN -->
<?php
//$this->load->view('left_coloumn');
?>

<div class="span12 ">
<!--New divs-->
<!--This area for single layout pagination purpose-->
<div class="single_person_pagination">
    <?php
    if ($page_number > 1) {
        $pos_id = ($page_number - 1) * 12;
    } else {
        $pos_id = 0;
    }
    
    if($get_candidates != ''){
    for ($tyr = $pos_id; $tyr < count($get_candidates); $tyr++) {
        ?>
        <input type="hidden" id="user_unique_id<?php echo $pos_id + 1; ?>"
               class="users_ids candidate_id<?= $get_candidates[$tyr]['user_id'] ?>"
               data-id="<?php echo $pos_id + 1; ?>" value="<?= $get_candidates[$tyr]['user_id'] ?>"/>
        <?php $pos_id++;
    } } ?>
</div>
<!--End single layout pagination purpose-->
<div class="right-column filter_editor" style="position:relative; padding: 0">

<div class="jquery_pagination">

    <?php if ($get_candidates != ''){ ?>
    <?php
    $jquery_page_items_count = 0;
    $opacity_decline_class = '';
    if ($decline_lists) {
        $opacity_decline_class = 'visibility_down';
    }
    if ($page_number > 1) {
        $pos_id = ($page_number - 1) * 12;
    } else {
        $pos_id = 1;
    }
    for ($tyr = 0;
    $tyr < count($get_candidates);
    $tyr++) {
        
    $inv_bg_class = '';
    $inv_status_layer = 0;
    $invite_tooltip = 0;
    if($get_candidates[$tyr]['invitation_status'] == 0){
        $inv_bg_class = 'invite-status-bg';
        $invite_tooltip = 1;
    }else if($get_candidates[$tyr]['invitation_status'] == 1){
        $inv_status_layer = 1; 
    }else if($get_candidates[$tyr]['invitation_status'] == -1){
        $inv_status_layer = 1; 
    }    
    //wrape pagination container
    if ($jquery_page_items_count == 12){
    $jquery_page_items_count = 0;
    ?></div>
<div class="jquery_pagination" style="display: none"><?php
}

$main_user_id = $get_candidates[$tyr]['user_id'];
$image;
if ($get_candidates[$tyr]['media_name'] != "") {
    $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_candidates[$tyr]['media_name'];
} else {
    $image = ASSETS_PATH_NO_IMAGE;
}
?>
<div
    class="row-fluid rg-bg <?=$inv_bg_class?> hiderow<?= $get_candidates[$tyr]['job_detail_id'] ?> <?php echo $opacity_decline_class1; ?>">
<div class="container" style="position: relative;" id="job_user_<?=$get_candidates[$tyr]['user_id']?>">
 <?php
 if($inv_status_layer == 1 && $get_candidates[$tyr]['invite_notification_status'] == 0){
 ?>
    <div class="invite-status-layer">
        <?php
        $margin_class="margin: 9% auto;";
            if(intval($job_role['is_admin']) == 0){
                $margin_class="margin: 7% auto;";
            }
        if($get_candidates[$tyr]['invitation_status'] == 1){
        ?>
        <div class="alert alert-success" style="<?= $margin_class ?>">
            <i class="icon-ok-sign" ></i> <strong>Nice!</strong>&nbsp;<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?> is very interested in this job.
            <a class="remove-anchor" value="<?=$get_candidates[$tyr]['job_detail_id']?>"><i class="fa fa-times-circle"></i></a>
        </div>
        <?php
        }else{
        ?>
        <div class="alert alert-danger" style="<?= $margin_class ?>">
            <i class="icon-remove-sign" style="color: #d5393e;"></i>
            <strong>Bummer!</strong>&nbsp;<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?> is not interested in this job.
            <a class="remove-anchor" value="<?=$get_candidates[$tyr]['job_detail_id']?>"><i class="fa fa-times-circle"></i></a>
        </div>
        <?php
        }
        ?>
    </div>  
 <?php
 }
 ?>
<div class="span12 editor-pck row-id<?= $get_candidates[$tyr]['user_id'] ?>">
<div class="row-fluid">
<div class="span12" id="top">
<div class="row-fluid">
<div class="span12">
    <div class="edt_pck_sec">
        <div class="user-dp-holder opacity-tr margin-top-6">
            <a href="javascript:void(0)"
               onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_candidates[$tyr]['user_id']; ?>)" style="position: relative;display: inline-block;">
                <img src="<?php echo $image; ?>" class="img-circle avatar-80">
                
                    <?php
                    if (intval($get_candidates[$tyr]['availability']) == 1) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#96bf48" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE FOR WORK"></i>
                    <?php
                    } else if (intval($get_candidates[$tyr]['availability']) == 2) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#5ba0a3" data-toggle="tooltip" data-placement="bottom" title="AVAILABLE SOON"></i>
                    <?php
                    } else if (intval($get_candidates[$tyr]['availability']) == 3) {
                        ?>
                        <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i>
                    <?php
                    }else{
                    ?>
                       <i class="icon-circle icon-large under-avatar-status avalability-mark-icon" style="color:#b85e80" data-toggle="tooltip" data-placement="bottom" title="NOT AVAILABLE"></i> 
                    <?php    
                    }
                    ?>
            </a>
        </div>
        <div class="editors_picked" id="editors_pick">
            <div class="editors_pick-info aplicant-tab margin-top-6" id="editors_pick_info">
                <div class="opacity-tr">
                    <a href="javascript:void(0)"
                       onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_candidates[$tyr]['user_id']; ?>)"
                       class="name"><?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?></a>
                                                    <span class="location"><?php
                                                        echo rtrim($get_candidates[$tyr]['industry_name'] . ', ' . $get_candidates[$tyr]['extra_title'], ', ');
                                                        ?></span>
                                            <span
                                                class="tags"><?php echo $get_candidates[$tyr]['city'] . ", " . $get_candidates[$tyr]['country']; ?></span>
                    <br clear="all">
                    <?php if (intval($job_role['is_reviewer']) == 1) { ?>
                        <div class="rt-dec-btn">
                            <a class="btn-flat success review_tyroe"
                               data-value="<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?>"
                               data-id="<?php echo $get_candidates[$tyr]['user_id']; ?>" <?= $class ?>>Review</a>
                            <a class="btn-flat inverse decline_tyroe"
                               data-id="<?php echo $get_candidates[$tyr]['user_id']; ?>"
                               data-name="<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?>" <?= $class ?>>Decline</a>
                        </div>
                    <?php } ?>
                </div>

                <a class="btn-flat danger undo_decline_tyroe rt-undo-btn"
                   data-id="<?php echo $get_candidates[$tyr]['user_id']; ?>"
                   data-type="candidate_undo"
                   data-value="<?php echo $get_candidates[$tyr]['reviewer_reviews_id']; ?>,<?php echo $get_candidates[$tyr]['job_detail_id']; ?>"
                   data-name="<?php echo $get_candidates[$tyr]['firstname'] . " " . $get_candidates[$tyr]['lastname']; ?>"
                   style="display: none" <?= $class ?>>Undo</a>
            </div>
            <div class="editors_showcase-container opacity-tr" id="editors-showcase">
                <div class="editors_showcase_holder">
                    <div
                        class="full-profile_overlay black_overlay_separate<?= $get_candidates[$tyr]['user_id'] ?>"
                        onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_candidates[$tyr]['user_id']; ?>)">
                        <div class="full-profile_overlay_holder">
                            <p>View Full Profile</p>
                            <i class="icon-picture"></i>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <?php
                        for ($a = 0; $a < 3; $a++) {
                            if ($get_candidates[$tyr]['portfolio'][$a]['media_name']) {
                                $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_candidates[$tyr]['portfolio'][$a]['media_name'];
                                if (!file_exists($portfolio)) {
                                    $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                } else {
                                    $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_candidates[$tyr]['portfolio'][$a]['media_name'];
                                }
                            } else {
                                $portfolio = ASSETS_PATH . '/img/image-icon.png';
                            }
                            ?>
                            <div class="showcase_thumb pull-right">
                                <a href="javascript:void(0);" class="score_img_anchor<?= $a ?>"
                                   data-id="<?= $get_candidates[$tyr]['user_id'] ?>">
                                    <?= (($a == 2) && ($get_candidates[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>

                                    <?php if ($a == 0) { ?>
                                        <?php if ($get_candidates[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                            <div
                                                class="tyroe_score_over_img tyroeScoreHolder<?= $get_candidates[$tyr]['user_id'] ?>">
                                                <div
                                                    class="tyroe_score_holder"><?= $get_candidates[$tyr]['get_tyroes_total_score'] ?></div>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>

                                    <img src="<?php echo $portfolio ?>"
                                         alt="editor-picks-<?= $get_candidates[$tyr]['is_user_featured'] ?>">
                                </a>
                            </div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
                <?php if ($opacity_decline_class == '' && intval($job_role['is_admin']) == 1){ ?>
                <div
                    class="span12 add_to_shortlist_controls margin-left-default cd-btn-sp margin-top-2">
                    <?php if ($get_jobs['archived_status'] == 1 || $inv_bg_class == 'invite-status-bg') { ?>
                    <a  href="javascript:void(0)"
                        class="btn btn-flat success span3 pull-right add_shortlist"
                        data-id="<?= $get_candidates[$tyr]['user_id'] ?>"
                        data-value="<?= $get_candidates[$tyr]['job_detail_id'] ?>" <?= $class ?>>+
                        shortlist</a>
                    <?php }else{ ?>
                    <a  href="javascript:void(0)"
                        class="btn btn-flat success span3 pull-right add_shortlist"
                        data-id="<?= $get_candidates[$tyr]['user_id'] ?>"
                        data-value="<?= $get_candidates[$tyr]['job_detail_id'] ?>" <?= $class ?>>+
                        shortlist</a>
                    <?php
                        } 
                     ?>
                    
                    <a href="javascript:void(0)"
                       class="reviews_toggle span7 pull-left btn-default btn ar-down rt-feed-btn"
                       data-id="<?= $get_candidates[$tyr]['job_detail_id'] ?>"
                       data-value="<?= $get_candidates[$tyr]['user_id'] ?>">View feedback and
                        reviews </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php } else if(intval($job_role['is_reviewer']) == 1 && (intval($get_candidates[$tyr]['reviewer_id'])) != 0){ ?>
                <div
                    class="span12 add_to_shortlist_controls margin-left-default cd-btn-sp margin-top-2">
                    <a href="javascript:void(0)"
                       class="reviews_toggle span12  btn-default btn ar-down"
                       data-id="<?= $get_candidates[$tyr]['job_detail_id'] ?>"
                       data-value="<?= $get_candidates[$tyr]['user_id'] ?>">View feedback and
                        review </a>
                </div>
                <div class="clearfix"></div>
            </div>
            <?php } ?>
            <div class="clearfix"></div>

        </div>
    </div>
    <div class="reviews_box<?php echo $get_candidates[$tyr]['job_detail_id']; ?> span12"
         style="display: none;margin: 40px auto 0 auto;" id="reviews_custom_id_<?= $get_candidates[$tyr]['user_id'] ?>"></div>
</div>
</div>
</div>
<?php if (intval($job_role['is_reviewer']) == 1){ ?>
</div>
<div class="span12 reviewer_reviews rev-edit-sec" id="reviews_edit_section<?= $get_candidates[$tyr]['user_id'] ?>"
     style="display: none">
    <div class="review_content_table_area content_section<?= $get_candidates[$tyr]['user_id'] ?>">
        <div class="reviewer_support_tools">
            <ul class="actions edit-ul-sec">
                <li><a href="javascript:void(0)" class="edit_reviews"
                       data-id="<?php echo $get_candidates[$tyr]['reviewer_review']['review_id'] ?>"
                       data-value="<?php echo $get_candidates[$tyr]['user_id']; ?>,<?php echo $get_candidates[$tyr]['firstname'] . ' ' . $get_candidates[$tyr]['lastname'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['level_id'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['technical'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['creative'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['impression'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['review_comment'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['action_shortlist'] ?>,<?php echo $get_candidates[$tyr]['reviewer_review']['action_interview'] ?>"
                        ><i class="table-edit"></i></a></li>
                <li class="last"><a href="javascript:void(0)" class="delete_tyroe_review"
                                    data-id="<?php echo $get_candidates[$tyr]['reviewer_review']['review_id'] ?>,<?php echo $get_candidates[$tyr]['user_id']; ?>"><i
                            class="table-delete"></i></a></li>
            </ul>
        </div>
        <div class="reviewer_support_disp">
            <div class="reviewer-support_status cn-label">
                <?php if ($get_candidates[$tyr]['reviewer_review']['action_shortlist']) { ?>
                    <span
                        class="label inverse"><?= $get_candidates[$tyr]['reviewer_review']['action_shortlist'] ?></span>
                <?php } ?>
                <?php if ($get_candidates[$tyr]['reviewer_review']['action_interview']) { ?>
                    <span
                        class="label inverse"><?= $get_candidates[$tyr]['reviewer_review']['action_interview'] ?></span>
                <?php } ?>
            </div>

            <div class="reviewer_support_disp_holder">
                <div class="reviewer_support_disp_area">
                    <p>
                        <strong><?php echo $level_result = ($get_candidates[$tyr]['reviewer_review']['level_title'] == 'Intern') ? 'Entry Level' : $get_candidates[$tyr]['reviewer_review']['level_title']; ?>
                            :</strong><span>
                                                    <?php echo $get_candidates[$tyr]['reviewer_review']['review_comment'] ?>
                                            </span></p>
                </div>
                <div
                    class="reviewer_support_disp_range rt-range review_range_sliders<?php echo $get_candidates[$tyr]['user_id']; ?>">
                    <div class="span12">
                        <div class="sliders">
                            <div class="slider edit_sliders_range  slider-technical"></div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="sliders">
                            <div class="slider edit_sliders_range slider-creative"></div>
                        </div>
                    </div>
                    <div class="span12">
                        <div class="sliders">
                            <div class="slider edit_sliders_range slider-impression"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</div>
<?php } ?>
</div>
<?php
if($invite_tooltip == 1){
?>
<span class="invite-status-suspended" alt="zxczczxc" data-toggle="tooltip" data-placement="left" title="Tyroe has not indicated if they are intrested in this job yet."><i class="icon-question-sign"></i></span>
<?php
}
?>    
</div>

</div>
<div class="clearfix"></div>
<div class="editor-seperator editor-seperator-row<?= $get_candidates[$tyr]['user_id'] ?>"></div>

<?php
$jquery_page_items_count++;
$pos_id++;
}
?>
</div>
<div class="span11 pagination  jquery_p" style="display:none;margin-left: 0px;">
    <ul class="pull-right jquery_pagination_container"></ul>
</div>
<?php } else { ?>
    <div class="container">
        <div class="col-md-12">
            <div class="alert alert-warning error_adjsut">
                <i class="icon-warning-sign"></i>
                <span>No candidate found</span>
            </div>
        </div>
    </div>
<?php } ?>

<div class="no-images candidate_no_image_section cd-sup">
    <div class="center">
        <i class="icon-user  icon-5x icon-muted"></i>
        <h6>Still haven't found the right candidate?</h6>

        <p>We have <?= $total_tyroes['total_tyroe'] ?> other artists in our database, so lets go check them out!</p>
        <a href="<?= $vObj->getURL("search") ?>" class="btn-glow primary">Find More Candidates</a>
    </div>
</div>
</div>
</div>
<!-- end main container -->

<div style="display: none;" class="modal fade rt-review-modal" id="myModal_give_review" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup " style="max-width: 650px">
                <div class="global-popup_header"  style="padding: 0px;padding-top: 10px;">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <div class="container-fluid">
                        <div class="row-fluid">
                            <div class="span12 center-block">
                                <h3 class="span12 modal-title" style="font-size: 18px;font-weight: 600px">Review Form</h3>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="global-popup_body modal-step1 jqueryClassPopupGet" style="  padding: 0px;padding-top: 35px;">
                    <div class="container-fluid" style="padding: 0px;">
                        <div class="row-fluid">
                            <div class="span12 center-block">
                                <div class="alert alert-warning" style="margin-bottom: 40px;margin-left: 20px;margin-right: 20px">
                                    <i class="icon-warning-sign"></i>
                                    This information is shared with <b>Studio Administrator</b> and <b>Candidate</b>
                                    <div class="clearfix"></div>
                                 </div>
                                <div class="field-box" style="padding: 0px 20px 0px 20px;  padding-bottom: 10px;">
                                    <label class="span2"><b>Level:</b></label>

                                    <div class="span9 rt-last-sec">
                                        <?php $a_count = 1;
                                        $reviewer_level = 0;
                                        foreach ($all_levels as $all_level){
                                            $all_level['level_title'] = str_replace("-level", "", $all_level['level_title']);
                                        ?>
                                        <?php if ($reviewer_level == 3){
                                        $reviewer_level = 0;
                                        ?></div>
                                    <label class="span3 margin-left-default"></label>

                                    <div class="span9 rt-last-sec"><?php
                                        } ?>
                                        
                                        <div class="span4"><input <?php if ($a_count == 1) {
                                                echo 'checked=checked';
                                            } ?> type="radio" value="<?= $all_level['job_level_id'] ?>"
                                                 data-id="<?= $all_level['level_title'] ?>" name="tyroe_level"><span
                                                class="rt-reviewer-label"><?= ($all_level['level_title'] == "Intern") ? "Entry Level" : $all_level['level_title'] . " Level"; ?></span>
                                        </div>
                                        <?php $reviewer_level++;
                                        $a_count++;
                                        } ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="field-box" style="padding: 0px 20px 0px 20px;  padding-bottom: 10px;">
                                    <label class="span2"><b>Impression:</b></label>
                                    <div class="span10" style="margin-top:6px;  padding-right: 20px;">
                                        <div class="row" style="margin: 0px">
                                            <div class="span10">
                                                <div class="sliders">
                                                    <div class="slider slider-impression"></div>
                                                </div>
                                            </div>
                                            <div class="span2 text-center">
                                                <div class="slider_number" style="margin-top: -12px" id="slider-impression-number">100</div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="field-box" style="padding: 0px 20px 0px 20px;  padding-bottom: 10px;">
                                    <label class="span2"><b>Creative:</b></label>

                                    <div class="span10" style="margin-top:6px;  padding-right: 20px;">
                                        <div class="row" style="margin: 0px">
                                            <div class="span10">
                                                <div class="sliders">
                                                    <div class="slider slider-creative"></div>
                                                </div>
                                            </div>
                                            <div class="span2 text-center">
                                                <div class="slider_number" style="margin-top: -12px" id="slider-creative-number">100</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="field-box" style="padding: 0px 20px 0px 20px;  padding-bottom: 10px;">
                                    <label class="span2"><b>Technical:</b></label>
                                    <div class="span10" style=" margin-top:6px;  padding-right: 20px;">
                                        <div class="row" style="margin: 0px">
                                            <div class="span10">
                                                <div class="sliders">
                                                    <div class="slider slider-technical"></div>
                                                </div>
                                            </div>
                                            <div class="span2 text-center">
                                                <div class="slider_number" style="margin-top: -12px" id="slider-technical-number">100</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="field-box" style="padding: 0px 20px 0px 20px;  padding-bottom: 10px;">
                                    <label class="span2"><b>Actions:</b></label>

                                    <div class="span10 rt-last-sec">
                                        <div class="span4">
                                            <input type="checkbox" value="shortlist"
                                                   name="action_shortlist">
                                            <span class="rt-reviewer-label">Shortlist</span>
                                        </div>
                                        <div class="span4">
                                            <input type="checkbox" value="interview" name="action_interview">
                                            <span class="rt-reviewer-label">Interview</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="row-fluid border-top review-form-badge" id="review-form-badge" style="padding-bottom: 30px;background-color: #F7F7F7;padding-left: 20px;padding-right: 20px;width: 93.9%;padding-top: 30px">
                                    <div class="span12">
                                        <div class="rate-badge-container">
                                            <div class="span10 rate-badge-content">
                                                <div class="badge-title" id="review-form-badge-title">Sapling</div>
                                                <div class="badge-text" id="review-form-badge-text">Solid grounding. Basic understanding.</div>
                                            </div>
                                            <div class="span2 rate-badge">
                                                <img id="review-form-badge-img" src="<?=LIVE_SITE_URL.'assets/img/default-avatar.png'?>" class="img-circle rate-badge-logo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="global-popup_body modal-step2" style="display: none;padding: 0px">
                    <div class="container-fluid" style="padding: 0px;">
                        <div class="row-fluid">
                            <div class="field-box">
                                <div class="span12" style="  padding: 10px;padding-left: 20px;">
                                    <div style="font-size: 15px;margin-bottom: 5px;">Comment for <b>Studio Administrator</b></div>
                                    <div class="muted" style="margin-bottom: 8px;">
                                    This is an internal studio note visible by your Studio Administrator only.
                                    </div>
                                    <div>
                                        <textarea class="span12 tyroe_comment" rows="4" style="resize: none;overflow: hidden; overflow-y: auto"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="field-box">
                                <div class="span12" style="  padding: 10px;padding-left: 20px;">
                                    <div style="font-size: 15px;margin-bottom: 5px;">Comment for <b>Candidate (annonymous)</b></div>
                                    <div class="muted" style="margin-bottom: 8px;">
                                    This note is sent directly to the Candidate to offer them helpful advice and feedback.<br/>
                                    For privacy reasons, this comment will be <b>sent annonymously on behalf of your studio</b>
                                    </div>
                                    <div>
                                        <textarea class="span12 anonymous_message" rows="4" style="resize: none;overflow: hidden; overflow-y: auto"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="global-popup_footer rt-rev-footer text-right" style="border-radius: 5px;">
                    <button type="button" class="btn-flat white review_close" data-dismiss="modal" aria-hidden="true">
                        Close
                    </button>
                    <button type="button" class="btn-flat success review_save green_submitbtn" style="margin-left: 10px;background: #96bf48;">Step 2
                    </button>
                    <button type="button" class="btn-flat primary review_update"
                            style="display: none;margin-left: 10px;">Update Review
                    </button>
                    <button type="button" class="btn-flat white   review_back" style="display: none">Back</button>
                    <button type="button" class="btn-flat success review_submit green_submitbtn"
                            style="display: none;margin-left: 10px;">
                        Submit review
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if (intval($job_role['is_reviewer']) == 1) { ?>
    <script type="text/javascript">
        // jQuery UI Sliders
        $('.edit_sliders_range').slider({
            slide: function (event, ui) {
                return false;
            }
        });
        
        if(typeof(technical_value) === 'undefined'){
            technical_value = 50;
        }
        
        if(typeof(creative_value) === 'undefined'){
            creative_value = 50;
        }
        
        if(typeof(impression_value) === 'undefined'){
            impression_value = 50;
        }
        
        average = (technical_value + creative_value + impression_value) / 3;
        average = Math.round(average);
        update_rate_badge(average);
        
        $("#slider-technical-number").text(technical_value);
        $("#slider-creative-number").text(creative_value);
        $("#slider-impression-number").text(impression_value);
        
        $(".slider-technical").slider({
            range: "min",
            value: technical_value,
            min: 1,
            max: 100,
            change: function () {
                current_value = $(this).slider('value');
                $("#slider-technical-number").text(current_value);
                technical_value = current_value;
                average = (technical_value + creative_value + impression_value) / 3;
                average = Math.round(average);
                $('.review_score_run_time').text(average);
                update_rate_badge(average);
            },
            slide: function(event, ui){
                $("#slider-technical-number").text(ui.value);
            }
        });
        $(".slider-creative").slider({
            range: "min",
            value: creative_value,
            min: 1,
            max: 100,
            change: function () {
                current_value = $(this).slider('value');
                $("#slider-creative-number").text(current_value);
                creative_value = current_value;
                average = (technical_value + creative_value + impression_value) / 3;
                average = Math.round(average);
                $('.review_score_run_time').text(average);
                update_rate_badge(average);
            },
            slide: function(event, ui){
                $("#slider-creative-number").text(ui.value);
            }
        });
        $(".slider-impression").slider({
            range: "min",
            value: impression_value,
            min: 1,
            max: 100,
            change: function () {
                current_value = $(this).slider('value');
                $("#slider-impression-number").text(current_value);
                impression_value = current_value;
                average = (technical_value + creative_value + impression_value) / 3;
                average = Math.round(average);
                $('.review_score_run_time').text(average);
                update_rate_badge(average);
            },
            slide: function(event, ui){
                $("#slider-impression-number").text(ui.value);
            }
        });
        
        function update_rate_badge(average){
            var title = '';
            var text = '';
            if(average < 50){
                title = 'Thirsty';
                text = 'Skills not perfected, but an obvious strong desire to prove themselves.';
            }else if(average >= 50 && average <= 79){
                title = 'Pressed';
                text = 'Hyper-focused on perfecting their skills and obsessively climbing the ranks.';
            }else if(average >= 80){
                title = 'Slayer';
                text = 'Spectacular abilities with impressive skills. Ready for industry challenges.';
            }
            
            $('#review-form-badge-title').html(title);
            $('#review-form-badge-text').html(text);
        }
    </script>
<?php } ?>


