<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    $("#award_title").keyup(function(event){
        if(event.keyCode == 13){
            //Add.click('<?=TABLE_AWARDS?>','award');

        }
    });
    function Add(table_name, coloum_name) {



        if ($('input[id="award_organization"]').val() == "") {
                    $('#award_organization').css({
                        border:'1px solid red'
                    });
                    $('#award_organization').focus();
                    return false;
                }
        else if ($('input[id="award_organization"]').val() != "") {
                                   $('#award_organization').css({
                                       border:''
                                   });
                       }

        if ($('input[id="award_title"]').val() == "") {
                           $('#award_title').css({
                               border:'1px solid red'
                           });
                           $('#award_title').focus();
                           return false;
                       }

               if ($('input[id="award_title"]').val() != "") {
                           $('#award_title').css({
                               border:''
                           });
               }

        if ($('input[id="award_website"]').val() == "") {
                    $('#award_website').css({
                        border:'1px solid red'
                    });
                    $('#award_website').focus();
                    return false;
                }
        else if ($('input[id="award_website"]').val() != "") {
                                           $('#award_website').css({
                                               border:''
                                           });
                               }

        if ($('input[id="award_year"]').val() == "") {
                    $('#award_year').css({
                        border:'1px solid red'
                    });
                    $('#award_year').focus();
                    return false;
                }
        else  if ($('input[id="award_year"]').val() != "") {
                                           $('#award_year').css({
                                               border:''
                                           });
                               }


        if ($('input[id="award_description"]').val() == "") {
                    $('#award_description').css({
                        border:'1px solid red'
                    });
                    $('#award_description').focus();
                    return false;
                }
        else  if ($('input[id="award_description"]').val() != "") {
                                           $('#award_description').css({
                                               border:''
                                           });
                               }

         if ($('input[id="award_description"]').val() != ""){

            var award = $('input[id="award_title"]').val();
            var aw_id = $("#aw_id").val();
            var award_organization = $("#award_organization").val();
            var award_website = $("#award_website").val();
            var award_year = $("#award_year").val();
            var award_description = $("#award_description").val();
            var data = "award=" + award + "&table_name=" + table_name + "&coloum_name=" + coloum_name + "&id=" + aw_id
                + "&award_organization=" + award_organization+ "&award_website=" + award_website+ "&award_year=" + award_year+ "&award_description=" + award_description;
            $.ajax({
                type:"POST",
                data:data,
                url:'<?=$vObj->getURL("resume/save")?>',
                dataType:"json",
                success:function (data) {
                    if (data.success) {
                        /*$(".field-box").hide();
                        $("#message").html(data.success_message).delay(5000);*/
                        window.location = "<?=$vObj->getURL("resume")?>";
                    }
                    else {
                        $("#error_msg").html(data.success_message);
                    }
                },
                failure:function (errMsg) {
                    console.log("Error");
                }
            });
        }
    }
</script>

<!--<div class="row-fluid form-wrapper">
    <div class="span12">-->
        <div class="container-fluid">
            <div class="span4 default-header tab-margin headerdiv">
                <h4>
                    <?php
                    if ($flag == "edit") {
                        echo LABEL_EDIT_AWARDS;
                    } else {
                        echo LABEL_ADD_AWARDS;
                    }
                    ?>
                </h4>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1"
                  enctype="multipart/form-data" name="profile_form" method="post">
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_AWARD_ORGANIZATION ?>:</label></div>
                        <div class="span3 tab-margin"> <input class="span3" type="text"
                           name="award_organization" id="award_organization"
                           value="<?= $award_data['award_organization']; ?>"></div>
                </div>
                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                                    <div class="span2 tab-margin"><label><?= LABEL_AWARD_TITLE ?>:</label></div>
                                        <div class="span3 tab-margin"> <input class="span3" type="text"
                                           name="award_title" id="award_title"
                                           value="<?= $award_data['award']; ?>"></div>
                                </div>
                                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                                    <div class="span2 tab-margin"><label><?= LABEL_WEBSITE ?>:</label></div>
                                        <div class="span3 tab-margin"> <input class="span3" type="text"
                                           name="award_website" id="award_website"
                                           value="<?= $award_data['award_website']; ?>"></div>
                                </div>
                                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                                    <div class="span2 tab-margin"><label><?= LABEL_YEAR ?>:</label></div>
                                        <div class="span3 tab-margin"> <input class="span3" type="text"
                                           name="award_year" id="award_year"
                                           value="<?= $award_data['award_year']; ?>"></div>
                                </div>
                                <div class="clearfix"></div>
                <div class="field-box span5 tab-margin">
                                    <div class="span2 tab-margin"><label><?= LABEL_DESCRIPTION ?>:</label></div>
                                        <div class="span3 tab-margin"> <input class="span3" type="text"
                                           name="award_description" id="award_description"
                                           value="<?= $award_data['award']; ?>"></div>
                                </div>
                                <div class="clearfix"></div>
                <div class="span3 field-box actions tab-margin pull-right">
                    <input type="hidden" id="aw_id" name="aw_id" value="<?=$award_data['award_id'];?>"/>
                    <input type="button" class="btn-glow primary pop-btn" onclick="Add('<?=TABLE_AWARDS?>','award');"
                           value="<?= LABEL_SAVE_CHANGES ?>">
                    <!--<span>OR</span>
                    <input type="reset" value="Cancel" class="reset" onclick="window.history.back()">-->
                </div>
            </form>
        </div>
        <span id="message" class="success_message"></span>
    <!--</div>
</div>-->
