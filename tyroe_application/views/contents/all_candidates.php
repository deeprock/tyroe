<script type="text/javascript">
    function shortlist(jid, user_id) {
        var data = "jid=" + jid + "&user_id=" + user_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/shortlisting")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".candidate" + studio).hide(500);
                    //$("#message").html(data.success_message);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide(200);
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide(200);
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }

    function hideuser(jid, studio) {
        var data = "jid=" + jid + "&studio=" + studio;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".candidate" + studio).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
    function InviteReviewer(tyroe_id) {
        var arr = [];
        if(tyroe_id==""){
            var ids = $('input[name="chk_candidate"]:checked');
            $.each(ids ,function(i, v) {
                var tyroe_val = v.value;
                arr.push(tyroe_val);
            });
            tyroe_id= arr.join(",");
        }
        var job_id = '<?php echo $job_id ?>';
        var data = "job_id=" + job_id + "&tyroe_id=" + tyroe_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".job-opening" + job_id).hide();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<div class="content">

    <div class="container-fluid">

        <!-- upper main stats -->
        <?php
        $this->load->view('navigation_top');
        ?>
        <!-- end upper main stats -->

        <div id="pad-wrapper">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');
                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column" style="position:relative;">

                            <!-- margin pointer -->
                            <div class="left-pointer" id="candidate">
                                <div class="left-arrow"></div>
                                <div class="left-arrow-border"></div>
                            </div>


                            <div class="row-fluid header">
                                <div class="span12">
                                    <h4>Candidates</h4>

                                    <div class="btn-group settings pull-right left-pad-20">
                                        <button class="btn glow" id="batch"><i class="icon-cog"></i></button>
                                        <button class="btn glow dropdown-toggle" id="batch" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Batch Contact</a></li>
                                            <li><a href="#" onclick="InviteReviewer('')">Batch Copy to Job</a></li>
                                            <li><a href="#">Batch Shortlist</a></li>
                                            <li><a href="#">Batch Hide</a></li>
                                        </ul>
                                    </div>
                                    <div class="button-center pull-right">
                                        <a class="btn-flat primary" id="invite" href="__search.html">Find more
                                            candidates</a>
                                    </div>
                                </div>
                            </div>
                            <!-- START CANDIDATE ROW -->
                            <?php
                            foreach ($get_candidate as $candidate) {
                                $image;
                                if ($candidate['media_name']) {
                                    $image = ASSETS_PATH_PROFILE_THUMB . $candidate['media_name'];
                                } else {
                                    $image = ASSETS_PATH_NO_IMAGE;
                                }

                                ?>

                                <div class="row-fluid candidate<?php echo $candidate['user_id'] ?>">
                                    <div class="span12 people" id="top">
                                        <div class="row-fluid">

                                            <div class="span8">
                                                <div class="span12">
                                                    <div class="span3" id="push-down-10">
                                                        <input type="checkbox" class="user" name="chk_candidate" id="chk_candidate" value="<?=$candidate['user_id']?>">
                                                        <img src="<?php echo $image; ?>" class="img-circle avatar-55">
                                                    </div>

                                                    <div class="span7">
                                                        <a href="<?= $vObj->getURL("findtalent/CandidateDetail/" . $candidate['user_id']) ?>"
                                                           class="name"
                                                           style="margin:0px;"><?php echo $candidate['username']; ?></a>
                                                        <span class="location"
                                                              style="display: block;"><?php echo $candidate['state'] . ', ' . $candidate['country']; ?></span>
                                                        <!--<span class="tags" style="display: block;"><?php /*echo $candidate['username'];*/?></span>-->
                                                        <span class="label label-info">Available Now</span>
                                                        <span class="label label-success">Featured</span>
                                                    </div>

                                                    <div class="span1">
                                                        <?php
                                                        if ($candidate['invitation_status'] == "0") {
                                                            ?>
                                                            <i class="icon-circle icon-large candidate-status"
                                                               id="green"></i>
                                                        <?php
                                                        } else if ($candidate['invitation_status'] == "1") {
                                                            ?>
                                                            <i class="icon-circle icon-large candidate-status"
                                                               id="green"></i>
                                                        <?php
                                                        } else {
                                                            ?>
                                                            <i class="icon-circle icon-large candidate-status"
                                                               id="green"></i>
                                                        <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="span4 align-right" id="push-down-10">
                                                <a href="<?= $vObj->getURL("findtalent/CandidateDetail/" . $candidate['user_id']) ?>"
                                                   class="btn-flat default"
                                                   id="larger"><i class="icon-tasks"></i></a>
                                                <a href="#" class="btn-flat gray" id="larger"><i
                                                        class="icon-picture"></i></a>

                                                <div class="btn-group settings">
                                                    <button class="btn glow"><i class="icon-cog"></i></button>
                                                    <button class="btn glow dropdown-toggle" data-toggle="dropdown">
                                                        <span class="caret"></span>
                                                    </button>
                                                    <ul class="dropdown-menu" style="text-align: left;">
                                                        <li><a href="#">Contact</a></li>
                                                        <li><a href="javascript:void(0)"
                                                               onclick="InviteReviewer('<?php echo $candidate['user_id']; ?>')">Copy
                                                                to Job</a></li>
                                                        <li><a href="javascript:void(0)"
                                                               onclick="shortlist('<?php echo $get_jobs['job_id']; ?>','<?php echo $candidate['user_id']; ?>');">Shortlist</a>
                                                        </li>
                                                        <li><a href="javascript:void(0)"
                                                               onclick="hideuser('<?php echo $get_jobs['job_id']; ?>','<?php echo $candidate['user_id']; ?>');">Hide</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                            ?>


                        </div>
                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<!-- end main container -->
