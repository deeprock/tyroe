<?php
$class = '';
if($job_detail['archived_status'] == 1){
    $class = 'disabled';
}

?>

<script type="text/javascript">
    var total_person = <?php echo count($get_applicants); ?>;
    <?php if($get_applicants != ''){
        $applicaint_count = count($get_applicants);
     }else{
        $applicaint_count = 0;
    }?>
    $(function () {
        $('.job_backicon').hide();
        $('.applicaint_counter').text(<?php echo $applicaint_count; ?>)
    });
    var total_applicaint = '<?php echo count($get_applicants); ?>';
    //create pagination
    var jquery_total_page = $('.filter_editor .jquery_pagination').size();
    var start_pages = 1;
    if (jquery_total_page > 5) {
        var end_pages = 5;
    } else {
        end_pages = jquery_total_page;
    }
    //create pages
    var jquery_pagination_html = '';
    var page_number = 1;
    var pre_page = 0;
    pagination_genrator();
</script>
<div class="row-fluid show-grid push-down-20">
    <!--This area for single layout pagination purpose-->
    <div class="single_person_pagination">
        <?php
        if ($page_number > 1) {
            $pos_id = ($page_number - 1) * 12;
        } else {
            $pos_id = 0;
        }
        if ($get_applicants != ''){
        for ($tyr = $pos_id; $tyr < count($get_applicants); $tyr++) {
            ?>
            <input type="hidden" id="user_unique_id<?php echo $pos_id + 1; ?>"
                   class="users_ids applicaint_id<?= $get_applicants[$tyr]['user_id'] ?>"
                   data-id="<?php echo $pos_id + 1; ?>" value="<?= $get_applicants[$tyr]['user_id'] ?>"/>
            <?php $pos_id++;
        }} ?>
    </div>
    <!--End single layout pagination purpose-->
    <div class="right-column filter_editor no-padding" style="position:relative;  padding: 0">
        <div class="jquery_pagination">
            <?php
            if ($get_applicants != ''){
            if ($page_number > 1) {
                $pos_id = ($page_number - 1) * 12;
            } else {
                $pos_id = 1;
            }
            $jquery_page_items_count = 0;
            for ($tyr = 0;
            $tyr < count($get_applicants);
            $tyr++) {
            $main_user_id = $get_applicants[$tyr]['user_id'];
            $image;
            if ($get_applicants[$tyr]['media_name'] != "") {
                $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_applicants[$tyr]['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            //wrape pagination container
            if ($jquery_page_items_count == 12){
            $jquery_page_items_count = 0;
            ?></div>
        <div class="jquery_pagination" style="display: none"><?php
            }
            ?>
            <div
                class="row-fluid rg-bg hiderow<?= $get_applicants[$tyr]['job_detail_id'] ?> <?= $opacity_decline_class ?>">
                <div class="container">
                    <div class="span12 editor-pck row-id<?= $get_applicants[$tyr]['job_detail_id'] ?>">
                        <div class="row-fluid">
                            <div class="span12" id="top">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="edt_pck_sec">
                                            <div class="user-dp-holder opacity-tr">
                                                <a href="javascript:void(0)"
                                                   onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_applicants[$tyr]['user_id']; ?>)"><img
                                                        src="<?php echo $image; ?>" class="img-circle avatar-80"></a>
                                            </div>
                                            <div class="editors_picked" id="editors_pick">
                                                <div class="editors_pick-info aplicant-tab" id="editors_pick_info">
                                                    <div class="opacity-tr">
                                                        <!--<a href="javascript:void(0);" class="name" value="<?php/*=$main_user_id*/?>"><?php /*echo $get_applicants[$tyr]['username']; */?></a>-->
                                                        <a href="javascript:void(0)"
                                                           onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_applicants[$tyr]['user_id']; ?>)"
                                                           class="name"><?php echo $get_applicants[$tyr]['firstname'] . ' ' . $get_applicants[$tyr]['lastname']; ?></a>
                                                <span class="location"><?php
                                                    /*for ($a = 0; $a < count($get_applicants[$tyr]['skills']); $a++) {
                                                        $temp = $get_applicants[$tyr]['skills'][$a]['creative_skill'];
                                                        echo ($a > 0)?", ":"";
                                                        if(!empty($temp)){
                                                            echo $temp;
                                                        }

                                                    }*/
                                                    echo rtrim($get_applicants[$tyr]['industry_name'] . ', ' . $get_applicants[$tyr]['extra_title'], ', ');
                                                    ?></span>
                                                        <span
                                                            class="tags"><?php echo $get_applicants[$tyr]['city'] . ", " . $get_applicants[$tyr]['country']; ?></span>
                                                    </div>
                                                    <?php if ($opacity_decline_class == '') { ?>
                                                        <a href="javascript:void(0)" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="btn-flat blue add_candidate"
                                                           id="<?= $get_applicants[$tyr]['user_id'] ?>"
                                                           data-id="<?php echo $get_applicants[$tyr]['job_detail_id']; ?>"
                                                           data-value="add_candidate" >+ Candidate</a>
                                                        <a href="javascript:void(0)" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?>
                                                           class="btn-flat gray decline_applicaint"
                                                           id="<?= $get_applicants[$tyr]['user_id'] ?>"
                                                           data-id="<?php echo $get_applicants[$tyr]['job_detail_id']; ?>"
                                                           data-name="<?= $get_applicants[$tyr]['firstname'] . ' ' . $get_applicants[$tyr]['lastname'] ?>"
                                                           data-value="decline_applicaint">Decline</a>
                                                    <?php } else { ?>
                                                        <a class="btn-flat danger undo_decline_tyroe rt-undo-btn" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?>
                                                           data-type="applicaint_undo"
                                                           data-value="<?php echo $get_applicants[$tyr]['job_detail_id']; ?>"
                                                           data-name="<?= $get_applicants[$tyr]['firstname'] . ' ' . $get_applicants[$tyr]['lastname'] ?>">Undo</a>
                                                    <?php } ?>
                                                    <br clear="all">
                                                </div>
                                                <div class="editors_showcase-container opacity-tr" id="editors-showcase">
                                                    <div class="editors_showcase_holder">
                                                        <div
                                                            class="full-profile_overlay black_overlay_separate<?= $get_applicants[$tyr]['user_id'] ?>"
                                                            onclick="getsingle_user(<?php echo $pos_id; ?>,<?php echo $get_applicants[$tyr]['user_id']; ?>)">
                                                            <div class="full-profile_overlay_holder">
                                                                <p>View Full Profile</p>
                                                                <i class="icon-picture"></i>
                                                            </div>
                                                        </div>
                                                        <div class="row-fluid">
                                                            <?php
                                                            for ($a = 0; $a < 3; $a++) {
                                                                if($get_applicants[$tyr]['portfolio'][$a]['media_name']){
                                                                   $portfolio = 'assets/uploads/portfolio1/300x300_' . $get_applicants[$tyr]['portfolio'][$a]['media_name'];
                                                                   if (!file_exists($portfolio)) {
                                                                       $portfolio = ASSETS_PATH . '/img/image-icon.png';
                                                                   }else{
                                                                       $portfolio = ASSETS_PATH_PORTFOLIO1_THUMB . '300x300_' . $get_applicants[$tyr]['portfolio'][$a]['media_name'];
                                                                   }
                                                                }else{
                                                                    $portfolio = ASSETS_PATH.'/img/image-icon.png';
                                                                }
                                                                ?>
                                                                <div class="showcase_thumb pull-right">
                                                                    <a href="javascript:void(0);"
                                                                       class="score_img_anchor<?= $a ?>"
                                                                       data-id="<?= $get_applicants[$tyr]['user_id'] ?>">
                                                                        <?= (($a == 2) && ($get_applicants[$tyr]['is_user_featured'] == 1)) ? '<span class="editors-badge"><p></p></span>' : ''; ?>
                                                                        <?php if ($a == 0) { ?>
                                                                            <?php if ($get_applicants[$tyr]['get_tyroes_total_score'] != 0) { ?>
                                                                                <div
                                                                                    class="tyroe_score_over_img tyroeScoreHolder<?= $get_applicants[$tyr]['user_id'] ?>">
                                                                                    <div
                                                                                        class="tyroe_score_holder"><?= $get_applicants[$tyr]['get_tyroes_total_score'] ?></div>
                                                                                </div>
                                                                            <?php } ?>
                                                                        <?php } ?>
                                                                        <img
                                                                            src="<?php echo $portfolio ?>"
                                                                            alt="editor-picks-<?= $get_applicants[$tyr]['is_user_featured'] ?>">
                                                                    </a>
                                                                </div>
                                                            <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="editor-seperator editor-seperator-row<?= $get_applicants[$tyr]['user_id'] ?>"></div>
            </div>
            <?php
            $jquery_page_items_count++;
            $pos_id++;
            }
            ?>
        </div>
        <div class="span11 pagination  jquery_p" style="display:none;margin-left: 0px;">
            <ul class="pull-right jquery_pagination_container"></ul>
        </div>
        <!-- START PAGINATION -->

        <?php } else { ?>
            <div class="container">
                <div class="col-md-12">
                    <div class="alert alert-warning error_adjsut">
                        <i class="icon-warning-sign"></i>
                        <span>No applicants found</span>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
