<?php
$option_value="0";
if(is_array($get_states) >0){
    $option_value="";
}
       ?><div class="ui-select span9 h-select">
    <select class="span5 inline-input" name="<?=$ddname?>" id="<?=$ddname?>">
        <option value="<?=$option_value?>">Select State</option>
        <?php
        foreach ($get_states as $key => $states) {
            ?>
            <option <?php if ($states['states_id'] == $get_user['state']) {
                echo "selected='selected'";
            } else {
            } ?>
                    value="<?php echo $states['states_id'] ?>"><?php echo $states['states_name'] ?></option>
            <?php
        }
        ?>
    </select>
</div>