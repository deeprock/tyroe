<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    jQuery(".btn-save").click(function(){
        jQuery("#speciality_form").submit();
    })


    function Add(table_name, coloum_name) {
        if ($('#skill').val() == "") {
            $('#skill').css({
                border: '1px solid red'
            });
            $('#skill').focus();
            return false;
        }

        if ($('#skill').val() != "") {
            $('#skill').css({
                border: ''
            });


            var skill = $("#skill").val();
            var data = "data=" + skill + "&table_name=" + table_name + "&coloum_name=" + coloum_name;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("resume/save")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".field-box").hide();
                        $("#message").html(data.success_message);
                    }
                    else {
                        $("#error_msg").html(data.success_message);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }
</script>
<!-- START LEFT COLUMN -->
<?php
//$this->load->view('left_coloumn');
?>

<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="row-fluid form-wrapper">
    <div class="span12">
        <div class="container-fluid">
            <?php
            if (!is_array($speciality_data)) {
                echo "There is no skill given for speciality";
            } else {
                ?>
                <div class="span4 default-header headerdiv">
                    <h4>
                        <?php
                        echo LABEL_SET_SKILLS;
                        ?>
                    </h4>
                </div>

                <form class="new_user_form inline-input" _lpchecked="1"
                      enctype="multipart/form-data" action="<?= $vObj->getURL("resume/save"); ?>" name="speciality_form"
                      method="post" id="speciality_form">
                    <div class="field-box">
                        <table style="border-collapse: separate;border-spacing: 20px;">
                            <tr>
                                <?php
                                foreach ($speciality_data as $kk => $vv) {
                                    $count++;
                                    ?>
                                    <div class="span6 tab-margin"><input class="<?= $vv['skill_id'] ?> rd-inp" type="radio"
                                               name="speciality" id="speciality"
                                               value="<?= $vv['skill_id']; ?>"
                                            <?php
                                            if ($vv['speciality'] == 1) {
                                                echo "checked";
                                            }
                                            ?>
                                            >
                                        &nbsp;&nbsp;<span class="rd-lab"><?=
                                        $vv['skill_name'];
                                        ?></span></div>
                                    <?php if ($count == 4) {
                                        echo "</tr><tr>";
                                        $count = 0;
                                    }
                                } ?>
                            </tr>
                        </table>
                    </div>
                    <div class="span11 field-box actions pull-right">
                        <input type="submit" class="pop-btn btn-glow primary" value="<?= LABEL_SAVE_CHANGES ?>">
                        <!--<span>OR</span>
                        <input type="reset" value="Cancel" class="reset" onclick="window.history.back()">-->
                    </div>
                    <input type="hidden" name="formname" value="speciality">
                </form>
            <?php
            }
            ?>
        </div>
        <span id="message"></span>
    </div>
</div>


</div>
<!-- END RIGHT COLUMN -->




