<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>
<script type="text/javascript">
function DeleteExperience(job_id) {

    var data = "job_id=" + job_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteExperience")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".experience" + job_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //location.reload();
                    //$(".msg-box").hide();
                }, 3000);
            }
            else {
                //$(".notification-box-message").css("color", "#b81900")
                /*$(".msg-box").addClass("alert alert-danger");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    //location.reload();
                    //$(".msg-box").hide();
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                }, 3000);
            }
        },
        failure: function (errMsg) {
        }
    });

}
function DeleteEducation(education_id) {
    //$(".education" + education_id).hide();


    var data = "education_id=" + education_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteEducation")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".education" + education_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                   // $(".msg-box").hide();
                    //location.reload();
                }, 3000);
            }
            else {
                //$(".msg-box").css("color", "#b81900")
                /*$(".msg-box").addClass("alert alert-danger");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //$(".msg-box").hide();
                    //location.reload();
                }, 3000);
            }
        },
        failure: function (errMsg) {
        }
    });

}
function DeleteSkills(skill_id) {
    //$(".education" + education_id).hide();


    var data = "skill_id=" + skill_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteSkill")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".skill" + skill_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/

                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    //$(".msg-box").hide();
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //location.reload();
                }, 3000);
            }
            else {
                /* $(".msg-box").addClass("alert alert-danger");
                 //$(".alert").css("color", "#b81900")
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //location.reload();
                    //$(".msg-box").hide();
                }, 3000);

            }
        },
        failure: function (errMsg) {
        }
    });

}
function DeleteAward(award_id) {
    //$(".education" + education_id).hide();


    var data = "award_id=" + award_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteAward")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".award" + award_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //$(".msg-box").hide();
                    //location.reload();
                }, 3000);
            }
            else {
                //$(".msg-box").css("color", "#b81900")
                /* $(".msg-box").addClass("alert alert-danger");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //$(".msg-box").hide();
                    //location.reload();
                }, 3000);
            }
        },
        failure: function (errMsg) {
        }
    });

}
function DeleteRefrence(refrence_id) {
    //$(".education" + education_id).hide();


    var data = "refrence_id=" + refrence_id;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/DeleteRefrence")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".refrence" + refrence_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //$(".msg-box").hide();
                    //location.reload();
                }, 3000);
            }
            else {
                //$(".msg-box").css("color", "#b81900")
                /* $(".msg-box").addClass("alert alert-danger");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $("msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    //$(".msg-box").hide();
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //location.reload();
                }, 3000);
            }
        },
        failure: function (errMsg) {
        }
    });

}
function EditForm(table, coloum, row) {
    var data = "table=" + table + "coloum=" + coloum + "row=" + row;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("resume/EditForm")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".refrence" + refrence_id).hide();
                /*$(".msg-box").addClass("alert alert-success");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    //$(".msg-box").hide();
                    //location.reload();
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                }, 3000);
            }
            else {
                // $(".msg-box").css("color", "#b81900")
                /*$(".msg-box").addClass("alert alert-danger");
                 $(".msg-box").html(data.success_message);
                 $(".msg-box").show(100);
                 setTimeout(function () {
                 $(".msg-box").hide();
                 }, 3000);*/
                $(".msg-box").addClass("alert alert-success");
                $(".alert-success").html("<i class='icon-ok-sign'></i>" + data.success_message);
                setTimeout(function () {
                    //$(".msg-box").hide();
                    $(".msg-box").removeClass("alert alert-success");
                    $(".msg-box").html("");
                    $(".msg-box").show();
                    //location.reload();
                }, 3000);
            }
        },
        failure: function (errMsg) {
        }
    });


}


/*function edit_any_form(contandfunc,value){
 var url =  contandfunc;
 var data = value;
 window.open(url+"/"+data,"_self");
 }*/

/* function ShowForm(formname){
 $(".main-experience").hide();
 $(".main-education").hide();
 $(".main-skill").hide();
 $(".main-award").hide();
 $(".main-refrence").hide();

 $(".form-"+formname).show();
 }*/

</script>

<!-- POP-UP -->
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {

        // DOM Ready
        $(function () {

            // Binding a click event
            // From jQuery v.1.7.0 use .on() instead of .bind()
            /*$('#invite').bind('click', function (e) {
             // Prevents the default action to be triggered.
             e.preventDefault();

             // Triggering bPopup when click event is fired
             $('#element_to_pop_up').bPopup();

             });*/
            $('.btn_pop').bind('click', function (e) {
                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired
                var identifier = this.id;
                var data = "";
                var param = "";
                var classes = "";
                if (identifier == '') {
                    classes = $(this).attr("class").split(' ');
                    identifier = classes[1];
                    param = classes[2];
                }
                if (identifier == 'skill') {
                    var url = '<?=$vObj->getURL("resume/GetForm/skill_form")?>';
                } else if (identifier == 'experience') {
                    var url = '<?=$vObj->getURL("resume/ExperienceForm/")?>' + param;
                } else if (identifier == 'education') {
                    var url = '<?=$vObj->getURL("resume/GetForm/education_form/")?>' + param;
                } else if (identifier == 'award') {
                    var url = '<?=$vObj->getURL("resume/GetForm/award_form/")?>' + param;
                } else if (identifier == 'refrence') {
                    var url = '<?=$vObj->getURL("resume/GetForm/refrence_form/")?>' + param;
                } else if (identifier == 'recommendation') {
                    var url = '<?=$vObj->getURL("resume/GetForm/recommendation_form/")?>' + param;
                } else if (identifier == 'speciality') {
                    var url = '<?=$vObj->getURL("resume/GetForm/speciality_form/")?>' + param;
                }
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data,
                    success: function (data) {
                        var jqObj = jQuery(data);
                        $('.modal-title').html(jqObj.find(".headerdiv").html());
                        jqObj.find(".headerdiv").remove();
                        var getfunc = jqObj.find(".btn-glow").attr("onclick");
                        $('.btn-save').attr("onclick", getfunc);
                        jqObj.find(".btn-glow").remove();
                        //var hidden_field = jqObj.find("#formname");
                        if (identifier == 'speciality' || identifier == 'skill') {
                            //jqObj.find(".modal-footer").after("</form>");
                        }
                        $('.modal-body').html(jqObj);
                        /*var x_axis = Math.floor(window.innerWidth / 2);
                         var popup_width = $('#element_to_pop_up').css('width');
                         var actual_width = x_axis - (parseInt(popup_width.replace('px', '')) / 2) - 40;
                         //$('#element_to_pop_up').bPopup({position: [actual_width, 10]});
                         $('#element_to_pop_up').bPopup({position: [actual_width, 10]});
                         $('#element_to_pop_up').append('<a href="javascript:void(0);" class="close-popup bClose">Close</a>');*/

                    }
                })


            });

        });

    })(jQuery);
</script>
<!-- POP-UP -->

<!-- main container -->
<div class="content">

<div class="container-fluid">


<div id="pad-wrapper" id="new-user">

<div class="grid-wrapper">

<div class="row-fluid show-grid">

<!-- START LEFT COLUMN -->
<?php
$this->load->view('left_coloumn');
?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->

<!--Default-->
<div class="span8 sidebar view-resume">
<div class="right-column">

<div class="span4 default-header">
    <h4><?= LABEL_EDIT_RESUME ?></h4>
</div>
<div class="clearfix"></div>
<div class="msg-box" id="msg-box"></div>
<script type="text/javascript">
    setTimeout(function(){
        $('.alert-success').hide();
    }, 2000);
</script>
<?php $msg = $this->session->userdata('exp_suc');
$this->session->unset_userdata('exp_suc');
if (isset($msg) && $msg != '') {
    ?>
    <div class="alert alert-success">
        <i class="icon-ok-sign"></i><?= MESSAGE_ADD_SUCCESS; ?>
    </div>
<?php } else{
    $this->session->unset_userdata('exp_suc');
}?>

<div class="clearfix"></div>
<fieldset>
<div class="panel-group" id="accordion">
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <?= LABEL_EXPERIENCE_TITLE ?>
            </a>
              <span class="pull-right button-center span3">
             <!-- <a class="btn-flat success btn_pop" id="experience" href="javascript:void (0);">+ Add Experience</a>-->
                  <a class="btn-flat success btn_pop span12" data-toggle="modal" data-target="#myModal" id="experience"
                     href="javascript:void (0);">+ Add Experience</a>

              </span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseOne" class="panel-collapse collapse in">

        <div class="panel-body">
            <div class="row-fluid header element main-experience">

                <div class="row-fluid experience_data">
                    <div class="panel">
                        <?php
                        if (!is_array($get_experience)) {
                            ?>
                            <div class="span12" id="side-list">
                                <div class="span11 job-info">
                                    <!--<a href="javascript:void(0);" class="job-header">-->
                                    <?php//= LABEL_NO_DETAILS ?><!--</a>-->
                                </div>
                            </div>
                        <?php
                        } else {
                            foreach ($get_experience as $experience) {
                                $end;
                                if ($experience['current_job'] == '1') {
                                    $end = "Present";
                                } else {
                                    $end = date("Y", $experience['job_end']);
                                }
                                ?>
                                <div class="row-fluid line-tgap experience<?php echo $experience['exp_id'] ?>">
                                    <div class="span12 people" id="top">
                                        <div class="row-fluid">
                                            <div class="span8">
                                                <div class="span12 job-info">
                                                    <a href="#"
                                                       class="job-header"><?php echo $experience['job_title']; ?></a>

                                                    <p>
                                                        <strong>Company:</strong> <?php echo $experience['company_name']; ?>
                                                    </p>

                                                    <p>
                                                        <strong>Date:</strong> <?php echo date("Y", $experience['job_start']) . " - " . $end; ?>
                                                    </p>
                                                </div>
                                            </div>
                                            <div class="span4">
                                                <ul class="actions pull-right">
                                                    <li>
                                                        <!--<a href="<?php/*= $vObj->getURL('resume/ExperienceForm/' . $experience['exp_id']); */?>"><i
                                                                  class="table-edit"></i></a>-->
                                                        <a href="javascript: void(0);" data-toggle="modal"
                                                           data-target="#myModal"
                                                           class="btn_pop experience <?= $experience['exp_id'] ?>"><i
                                                                class="table-edit"></i></a>
                                                    </li>
                                                    <!--<li><i class="table-edit" onclick="edit_any_form('<?php/*=$vObj->getURL("resume/ExperienceForm")*/?>','<?php/*=$experience['exp_id'];*/?>')"></i></li>-->
                                                    <li class="last"><i class="table-delete"
                                                                        onclick="DeleteExperience('<?= $experience['exp_id'] ?>');"></i>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row-fluid">
                                            <p><?php echo $experience['job_description']; ?>
                                            </p>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                <?= LABEL_EDUCATION_TITLE ?>
            </a>
            <span class="pull-right button-center span3"> <a class="btn-flat success btn_pop span12" data-toggle="modal"
                                                             data-target="#myModal" id="education"
                                                             href="javascript:void (0);">+ Add Eduation</a></span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row-fluid header element-show main-education">
                <div class="row-fluid panel education_data">
                    <?php

                    if (!is_array($get_education)) {
                        ?>
                        <div class="span8">
                            <div class="span12 job-info">
                                <!--<a href="javascript:void(0);" class="job-header"> -->
                                <?php//= LABEL_NO_DETAILS ?><!--</a>-->

                            </div>
                        </div>
                    <?php
                    } else {
                        foreach ($get_education as $education) {
                            ?>
                            <div class="row-fluid line-tgap education<?php echo $education['education_id'] ?>">
                                <div class="span12 people" id="top">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <div class="span12 job-info">
                                                <a href="#"
                                                   class="job-header"><?php echo $education['edu_organization']; ?></a>

                                                <!--<p><strong>Institute:</strong> <?php /*echo $education['institute_name']; */?></p>-->

                                                <p>
                                                    <strong>Date:</strong> <?php echo date("Y", $education['start_year']) . "-" . date("Y", $education['end_year']); ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <ul class="actions pull-right">
                                                <li>
                                                    <a href="javascript: void(0);" data-toggle="modal"
                                                       data-target="#myModal"
                                                       class="btn_pop education <?= $education['education_id'] ?>"><i
                                                            class="table-edit"></i></a></li>
                                                <li class="last"><i class="table-delete"
                                                                    onclick="DeleteEducation('<?= $education['education_id'] ?>');"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="row-fluid">
                                        <p><?php echo $education['education_description']; ?>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                <?= LABEL_SKILL_TITLE ?>
            </a>
            <span class="pull-right button-center span3"> <a class="btn-flat success btn_pop span12" data-toggle="modal"
                                                             data-target="#myModal" id="skill"
                                                             href="javascript:void (0);" <?= $skills['skill'] ?> >+ <?= LABEL_ADD_SKILLS ?></a></span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseThree" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row-fluid header element-show main-skill">

                <div class="row-fluid panel skill_data">
                    <?php
                    if (!is_array($get_skills)) {
                        ?>
                        <div class="span8">
                            <div class="span12 job-info">
                                <!--<a href="javascript:void(0);" class="job-header">-->
                                <?php//= LABEL_NO_DETAILS ?><!--</a>-->

                            </div>
                        </div>
                    <?php
                    } else {
                        foreach ($get_skills as $skills) {
                            ?>
                            <div class="row-fluid line-tgap skill<?php echo $skills['skill_id']; ?>">
                                <div class="span12 people" id="top">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <div class="span12 job-info">
                                                <span class="span11"> <?php echo $skills['skill_name']; ?></span>

                                            </div>
                                        </div>
                                        <div class="span4">
                                            <ul class="actions pull-right">
                                                <!--<li>
                                                      <a href="<?php/*= $vObj->getURL('resume/GetForm/education_form/' . $education['education_id']); */?>"><i
                                                              class="table-edit"></i></a></li>-->
                                                <li class="last"><i class="table-delete"
                                                                    onclick="DeleteSkills('<?= $skills['skill_id'] ?>');"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                <?= LABEL_SPECIALITY_TITLE ?>
            </a>
            <span class="pull-right button-center span3"> <a class="btn-flat success btn_pop span12" data-toggle="modal"
                                                             data-target="#myModal" id="speciality"
                                                             href="javascript:void (0);">+ <?= LABEL_SET_SKILLS ?></a></span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseFour" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row-fluid header element-show main-speciality">
                <div class="row-fluid panel speciality_data">
                    <?php
                    if (!is_array($get_speciality)) {
                        ?>
                        <div class="span8">
                            <div class="span12 job-info">
                                <!-- <a href="javascript:void(0);" class="job-header">-->
                                <?php//= LABEL_NO_DETAILS ?><!--</a>-->

                            </div>
                        </div>
                    <?php
                    } else {
                        ?>
                        <div class="row-fluid line-tgap skill<?php echo $get_speciality['skill']; ?>">
                            <div class="span12 people" id="top">
                                <div class="row-fluid">
                                    <div class="span8">
                                        <div class="span12 job-info">
                                            <span class="span11"> <?= $get_speciality['skill_name']; ?></span>

                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseFive">
                <?= LABEL_AWARD_TITLE ?>
            </a>
            <span class="pull-right button-center span3"> <a class="btn-flat success btn_pop span12" data-toggle="modal"
                                                             data-target="#myModal" id="award"
                                                             href="javascript:void (0);">+ <?= LABEL_ADD_AWARDS ?></a></span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseFive" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row-fluid header element-show main-award">
                <div class="row-fluid panel award_data">
                    <?php

                    if (!is_array($get_awards)) {
                        ?>
                        <div class="row-fluid experience">
                            <div class="span8">
                                <div class="span12 job-info">
                                    <!--<a href="javascript:void(0);" class="job-header">-->
                                    <?php//= LABEL_NO_DETAILS ?><!--</a>-->

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    <?php
                    } else {
                        foreach ($get_awards as $awards) {
                            ?>
                            <div class="row-fluid line-tgap award<?php echo $awards['award_id'] ?>">
                                <div class="span12 people" id="top">
                                    <div class="row-fluid">
                                        <div class="span8">
                                            <!--<div class="span12 job-info">
                                                  <?php /*echo $awards['award']; */?>

                                                </div>-->
                                            <div class="span12 job-info">
                                                <a href="#" class="job-header"><?php echo $awards['award']; ?></a>

                                                <p>
                                                    <strong>Organization:</strong> <?php echo $awards['award_organization']; ?>
                                                </p>

                                                <p>
                                                    <strong>Year:</strong> <?php echo $awards['award_year']; ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="span4">
                                            <ul class="actions pull-right">
                                                <li>
                                                    <a href="javascript: void(0);" data-toggle="modal"
                                                       data-target="#myModal"
                                                       class="btn_pop award <?= $awards['award_id'] ?>"><i
                                                            class="table-edit"></i></a></li>
                                                <li class="last"><i class="table-delete"
                                                                    onclick="DeleteAward('<?= $awards['award_id'] ?>');"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        <?php
                        }
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="panel bottom-margin panel-default want-bdr">
    <div class="panel-heading">
        <h4 class="panel-title">
            <a class="detail-click" data-toggle="collapse" data-parent="#accordion" href="#collapseSix">
                <?= LABEL_RECOMMENDATION_TITLE ?>
            </a>
            <span class="pull-right button-center span3"> <a class="btn-flat success btn_pop span12" data-toggle="modal"
                                                             data-target="#myModal" id="recommendation"
                                                             href="javascript:void (0);">+ <?= LABEL_REQUEST_REFERENCES ?></a></span>

            <div class="clearfix"></div>
        </h4>
        <div class="clearfix"></div>
    </div>
    <div id="collapseSix" class="panel-collapse collapse in">
        <div class="panel-body">
            <div class="row-fluid header element-show main-recommendation">
                <div class="row-fluid panel recommend_data alert alert-warning t-magic">
                    <?php
                    if (!is_array($get_recommendation)) {
                        ?>

                        <div class="span8">
                            <div class="span12 job-info">
                                <!-- <a href="javascript:void(0);" class="job-header">-->
                                <?= LABEL_NO_RECOMMENDATION ?><!--</a>-->

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    <?php
                    } else {
                        foreach ($get_recommendation as $recommend) {
                            if ($recommend['recommend_status'] == -1) {
                                ?>
                                <div class="row-fluid line-tgap refrence<?php echo $recommend['recommend_id'] ?>">
                                    <div class="span12 people" id="top">
                                        <div class="row-fluid">
                                            <div class="span8">
                                                <div class="span12 job-info">
                                                    <h5>
                                                        <b><?= $recommend['recommend_name'] . LABEL_RECOMMENDATION_TEXT2 . date("nS M Y", $recommend['created_timestamp']); ?>
                                                            :</b></h5>
                                                </div>
                                                <div class="">
                                                    <?= '"' . $recommend['recommendation'] . '"'; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php
                            }
                        }
                        ?>
                        <div class="span12 job-info">
                            <h5><b><?= "Pending Request"; ?>:</b></h5>
                        </div>
                        <div class="content-box-data">
                            <ul class="lower-n-list">
                                <?php
                                foreach ($get_recommendation as $recommend) {
                                    if ($recommend['recommend_status'] == 0) {
                                        ?>
                                        <li><?= $recommend['recommend_email'] . LABEL_RECOMMENDATION_TEXT3; ?></li>
                                    <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                    <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</fieldset>












<?php /*
<div class="row-fluid header element-show main-skill">
    <div class="span12">
        <h4><?= LABEL_SKILL_TITLE ?></h4>
              <span class="pull-right button-center">
        <a class="btn-flat success" id="invite"
           href="<?= $vObj->getURL('resume/GetForm/skill_form'); ?>">+
            <?= LABEL_ADD_SKILLS ?></a></span>
    </div>
    <div class="row-fluid panel skill_data">
        <?php

        if (!is_array($get_skills)) {
            ?>
            <div class="row-fluid skill">
                <div class="span12 people" id="top">
                    <?= LABEL_NO_DETAILS ?>
                </div>
                <div class="clearfix"></div>
            </div>
        <?php
        } else {
            foreach ($get_skills as $skills) {
                ?>
                <div class="row-fluid ">
                    <div class="span12 people skill<?php echo $skills['skill'] ?>"
                    " id="top">
                    <div class="row-fluid">
                        <div class="span8">
                            <div class="span12 job-info">
                                <a href="#" class="job-header"><?php echo $skills['skill_name']; ?></a>

                            </div>
                        </div>
                        <div class="span4">
                            <ul class="actions pull-right">
                                <li class="last"><i class="table-delete"
                                                    onclick="DeleteSkills('<?= $skills['skill'] ?>');"></i>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>


            <?php
            }
        }
        ?>
    </div>
</div>
*/
?>


</div>
</div>
</div>
<!--Default-->


</div>
</div>
</div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <!--<h4 class="modal-title">Modal title</h4>-->
                <!--<h4 class="modal-title"><?php/* if ($flag == "edit") {
                    echo LABEL_EDIT_EXPERIENCE;
                } else {
                    echo LABEL_ADD_EXPERIENCE;
                }
                    */?>
                </h4>-->
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <p>Loading...&hellip;</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-save" value="<?= LABEL_SAVE_CHANGES ?>">Save changes
                </button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- end main container -->
