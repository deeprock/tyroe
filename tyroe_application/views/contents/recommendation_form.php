<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    function validEmail(v) {
        var r = new RegExp("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?");
        return (v.match(r) == null) ? false : true;
    }

    function Add(table_name, coloum_name) {
        if ($('#recommend').val() == "") {
            $('#recommend').css({
                border: '1px solid red'
            });
            $('#recommend').focus();
            return false;
        }

        if ($('#recommend').val() != "") {
            if(validEmail($('#recommend').val())){
                $('#recommend').css({
                    border: ''
                });
            } else {
                $('#recommend').css({
                    border: '1px solid red'
                });
                $('#recommend').focus();
                $('#recommend').val('').attr('placeholder','Please enter right form');
                return false;
            }
            var recommend = $("#recommend").val();
            var ref_id = $("#recom_id").val();
            var data = "data=" + recommend + "&table_name=" + table_name + "&coloum_name=" + coloum_name + "&id=" + ref_id;
            $('#big_loader').css('display','');
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("resume/save")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".field-box").hide();
                        $("#message").html(data.success_message).delay(5000);
                        window.location = "<?=$vObj->getURL("resume")?>";
                    }
                    else {
                        $("#error_msg").html(data.success_message);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }
</script>

<div class="row-fluid form-wrapper">
    <div class="span12">
        <div class="container-fluid">
            <div class="span8 default-header headerdiv">
                <h4>
                    <?php
                    echo LABEL_REQUEST_REFERENCES;
                    ?>
                </h4>
            </div>
            <form class="new_user_form inline-input" _lpchecked="1"
                  enctype="multipart/form-data" name="profile_form" method="post">
                <div class="field-box span11 tab-margin">
                    <div class="span2 tab-margin"><label><?= LABEL_EMAIL ?>:</label></div>
                        <div class="span9 tab-margin"><input class="span9" type="text"
                           name="recommend" id="recommend"
                           placeholder="<?=LABEL_RECOMMENDATION_TEXT?>"
                           value="<?php echo $refrence_data['refrence_detail']; ?>"></div>
                    <div style="display: none" id="big_loader" ><img src="<?=ASSETS_PATH?>img/big_loader.gif" width="50px" /> Sending...</div>
                </div>
                <div class="clearfix"></div>
                <div class="span11 field-box actions pull-right">
                    <input type="hidden" name="recom_id" id="recom_id"
                           value="<?= $refrence_data['refrence_id']; ?>"/>
                    <input type="button" class="btn-glow primary pop-btn"
                           onclick="Add('<?= TABLE_RECOMMENDATION ?>','recommend_email');"
                           value="<?= LABEL_SEND_INVITATION_BUTTON ?>">
                   <!-- <span>OR</span>
                    <input type="reset" value="Cancel" class="reset"
                           onclick="window.history.back()">-->
                </div>
            </form>
        </div>
        <span id="message" class="success_message"></span>
    </div>
</div>
