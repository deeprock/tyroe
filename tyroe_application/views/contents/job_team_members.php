<script src="http://192.168.10.136/wtyroenew/assets/js/select2.min.js"></script>
<script>
 $(document).ready(function(){
//     job_level_role_actions();
//     $("#invitation_emails").select2({
//            tags:[""],
//            tokenSeparators: [","]
//     });
 });
 
 function job_level_role_actions(){
        $('.reviewer_check_option').unbind('click').click(function(){
            var obj = $(this);
            var arr = $(this).parent().attr('id').split('_');
            var job_id = arr[3];
            var reviewer_id = arr[4];
            var user_role = $(this).attr('value');
            var change_value = 1;
            if(obj.hasClass('active')){
                change_value = 0;
            }
            
            $.ajax({
                type:'POST',
                data:{
                    job_id : job_id,
                    reviewer_id : reviewer_id,
                    user_role : user_role,
                    change_value : change_value
                },
                url:'<?=$vObj->getURL("jobopening/change_job_team_member_role")?>',
                datatype : 'json',
                success: function(responce){
                    if(obj.hasClass('active')){
                        obj.removeClass('active');
                    }else{
                        obj.addClass('active');
                    }
                }
            });
            
        });
        
        $('.delet-job-reviewer').unbind('click').click(function(){
            var obj = $(this);
            var arr = $(this).attr('id').split('_');
            var job_id = arr[3];
            var reviewer_id = arr[4];
            var rev_id = arr[5];
            $.ajax({
                type:'POST',
                data:{
                    job_id : job_id,
                    reviewer_id : reviewer_id,
                    rev_id : rev_id
                },
                url:'<?=$vObj->getURL("jobopening/delete_job_team_member")?>',
                datatype : 'json',
                success: function(responce){
                   obj.parent().parent().remove();
                   if($('#studio_reviewers tr').length == 0){
                       var obj_html = $("<tr class='no_reviewer_record'><td colspan='6'><div class='alert alert-warning'><i class='icon-warning-sign'></i>You Don't have any team member yet.</div></td></tr>");
                       $('#studio_reviewers').append(obj_html);
                   }
                }
            });
            
       });
}
    
    //existing reviewer searching
$(document).on('keyup','#search_existing_reviewer',function(a){
    $('.searching_reviewer_result').css({'display':'block','float':'none'});
    var job_id = $('#edit_id').val();
    var reviewers =  $('input[name="reviewer_ids[]"]').map(function(){
        return this.value
    }).get();
    var searching_value = $("#search_existing_reviewer").val()
    $.ajax({
        type: 'POST',
        data: 'searching_value='+searching_value+'&reviewers='+reviewers+'&job_id='+job_id+'&search_type=existing_job',
        url: "<?= $vObj->getURL("jobopening/search_existing_reviewer"); ?>",
        success : function(html){
            if($('#search_existing_reviewer').val() == ''){
                $('.searching_reviewer_result').html('');
            }else{
                $('.searching_reviewer_result').html(html);
            }

        }
    });

});
$(document).on('click','.select_reviewer',function(){

    var user_value = $(this).data('value');
    var job_id = $('#edit_id').val();
    
    //$('.display_list'+user_value).remove();
    //$('.parent_row' + user_value).remove();
    //$('#reviewer_container').prepend('<li style="text-align:center"><img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" style="width:40px;height:40px;" /></li>')
    $.ajax({
        type: 'POST',
        data: 'reviewer_id='+user_value+'&job_id='+job_id,
        url: "<?= $vObj->getURL("jobopening/add_reviewers_from_job"); ?>",
        success : function(html){
            
            if($('.no_reviewer_record').length > 0){
                $('#studio_reviewers').empty();
                $('#studio_reviewers').html(html);
            }else{
                var obj = $(html);
                $('#studio_reviewers').append(obj);
            }
            job_level_role_actions();
//            var previous_html = $('#reviewer_container').html();
//                previous_html += '<li class="parent_row' + user_value + '">';
//                previous_html += $('.list_parent_row' + user_value).html();
//                previous_html += '</li>';
//                $('#reviewer_container').html(previous_html);
//                $('.list_parent_row' + user_value).remove();
//                $('#reviewer_container .small_loader:eq(0)').parent('li').remove();
        }
    });
});


$(document).on('click','#send_invitaion',function(){
    $('#invitation_error').hide();
    var invitation_emails = $("#invitation_emails").val();
    if(invitation_emails == ''){
        $('#invitation_error').removeClass('alert-success').addClass('alert-error').fadeIn().find('span').text('<?=MESSAGE_SEND_REVIEWER_INVITATION_ERROR?>');
        setTimeout(function(){ $('#invitation_error').fadeOut();},2000);
        return false;
    }
    $(this).css('display','none');
    var job_id = $('#edit_id').val();
    var loader_image = "<img id='send_invitaion_loader' src='<?=ASSETS_PATH_SMALL_LOADER_IMG?>' width='32px'>";
    $(this).after(loader_image);
    data = 'sending_action=invitaion&emails='+invitation_emails+'&job_id='+job_id;

    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/send_email");?>',
        success: function (result) {
            if(result){
                /* Creating emails tags */
                $(".select2-input").val('');
                $("#send_invitaion_loader").remove();
                $("#send_invitaion").css('display','');
                $('#invitation_error').addClass('alert-success').removeClass('alert-error').fadeIn().find('span').text('<?=MESSAGE_SEND_REVIEWER_INVITATION?>');
                setTimeout(function(){ $('#invitation_error').fadeOut();},2000);
            }
        }
    });
});
</script>
<div id="main-stats" style="margin-top: 0;border: none;">
    <div class="stats-row">
        <div class="span5 stat-large text-center box-show">
            <div class="no-images" style="margin-bottom: 0">
                <i class="fa fa-plus-circle"></i>
                <div class="clearfix"></div>
                <h5 class="rt-text-15">Add team member</h5>
                <p>Select existing team member to help you discover the best talent for this job opening.</p>
                <div class="field-box">
                    <input type="text" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="search_existing_reviewer radius-search-input" id="search_existing_reviewer" placeholder="Search team members">
                    <ul class="searching_reviewer_result ext-rvw-list-1"></ul>
                </div>
           </div>
        </div>
        <div class="vertical-sep"></div>
        <div class="span5 stat-large text-center box-show">
            <div class="no-images" style="margin-bottom: 0">
                <i class="fa fa-envelope-o"></i>
                <div class="clearfix"></div>
                <h5 class="rt-text-15">Invite new team member</h5>
                <p>Looking for someone new to help out? Send them an email invitation and get them involved today.</p>
                <div class="alert alert-success" id="invitation_error" style="display:none">
                    <span></span>
                </div>
                <div class="field-box">
                    <div class="alert alert-success" id="invitation_error" style="display:none">
                       <span></span>
                    </div>
                    <div class="row-fluid invite-form">
                        <input type="text" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> class="invite-form-input" name="invitation" placeholder="Emails" id="invitation_emails">
                        <button class="btn-flat success invite-form-btn" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> id="send_invitaion">Send invite</button>
                    </div>
<!--                                            <div class="btn-group">
                        <input type="text" <?php if($get_jobs['archived_status'] == 1){ ?> disabled="disabled" <?php } ?> placeholder="Emails" id="invitation_emails" class="invite-email-text">
                        <button type="button" class="btn btn-success right rt-search-btn send_invitaion_btn" id="send_invitaion">Invite</button>
                    </div>-->
                 </div>
             </div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div>
</div>
    <div class="show-grid margin-adjust">
        <div class="clearfix"></div>
    </div>

    <table class="table table-hover tr-thead">
    <thead>
    <tr>
        <th><div class="position-relative"><a href="javascript:void(0)">Name</a></div></th>
        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)">Job Administrator</a></div></th>
        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)">Job Reviewer</a></div></th>
        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)">Email</a></div></th>
        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)">Status</a></div></th>
        <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)"></a></div></th>
    </tr>
    </thead>
    <tbody id="studio_reviewers" class="team-tr">
    <?php if ($get_reviewers != '') { ?>
    <?php foreach ($get_reviewers as $get_reviewer) { ?>
        <tr class="table_rows row_reviewer_no<?= $get_reviewer['user_id'] ?> job-team-tr">
            <td><p class="rt-name"><?= stripslashes($get_reviewer['firstname']) . ' ' . stripslashes($get_reviewer['lastname']) ?></p>
            <span class="rt-title"><?= stripslashes($get_reviewer['job_title']); ?></span></td>
            <?php
                $is_admin = '';
                $is_reviewer = '';
                if($get_reviewer['is_admin'] == 1){
                    $is_admin = 'active';
                }

                if($get_reviewer['is_reviewer'] == 1){
                    $is_reviewer = 'active';
                }
            ?>

            <td class="role-check-td" id="role_check_td_<?=$get_reviewer['job_id']?>_<?=$get_reviewer['user_id']?>"><a class="reviewer_check_option <?=$is_admin?>" value="1"><i class="fa fa-check-circle"></i></a></td>
            <td class="role-check-td" id="role_check_td_<?=$get_reviewer['job_id']?>_<?=$get_reviewer['user_id']?>"><a class="reviewer_check_option <?=$is_reviewer?>" value="2"><i class="fa fa-check-circle"></i></a></td>

            <td><a href="mailto:<?= $get_reviewer['email'] ?>"><?= $get_reviewer['email'] ?></a></td>
            <td class="status_td">
                <?php if ($get_reviewer['status_sl'] == 1) { ?>
                    <a href="javascript:void(0)" class="reviewer_status_sl"><label class="label label-success">Active</label></a>
                <?php } else if ($get_reviewer['status_sl'] == 0) { ?>
                    <a href="javascript:void(0)"><label class="label label-info rt-pending">Pending</label></a>
                <?php } ?>
            </td>
            <td><a href="javascript:void(0)" class="delet-job-reviewer" id="delet_job_reviewer_<?=$get_reviewer['job_id']?>_<?=$get_reviewer['user_id']?>_0"><i class="fa fa-times"></i></a></td>
        </tr>
    <?php } ?>
    <?php } else { ?>
        <tr class="no_reviewer_record">
            <td colspan="6">
                <div class="alert alert-warning">
                    <i class="icon-warning-sign"></i>
                    You Don't have any team member yet.
                </div>
            </td>
        </tr>
    <?php } ?>
    </tbody>
</table>
<!--<script src="//code.jquery.com/ui/1.10.4/jquery-ui.js"></script>-->
