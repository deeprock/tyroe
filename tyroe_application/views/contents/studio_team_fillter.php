<?php if($get_reviewers != ''){?>
    <div class="show-grid margin-adjust">
        <div class="clearfix"></div>
    </div>

    <table class="table table-hover tr-thead">
        <thead>
        <tr>
            <th><div class="position-relative"><a href="javascript:void(0)" class="sorter" data-column="<?=TABLE_USERS?>.firstname" data-stype="<?= $td_sort ?>">Name</a></div></th>
            <th ><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?=TABLE_USERS?>.created_timestamp" data-stype="<?= $td_sort ?>">Signed up</a></div></th>
            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?= TABLE_USERS ?>.user_id" data-stype="<?= $td_sort ?>">Openings</a></div></th>
            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?=TABLE_USERS?>.email" data-stype="<?= $td_sort ?>">Email</a></div></th>
            <th><div class="position-relative"><span class="line"></span><a href="javascript:void(0)" class="sorter" data-column="<?=TABLE_USERS?>.status_sl" data-stype="<?= $td_sort ?>">Status</a></div></th>
            <th class="width74px"><div class="position-relative"><span class="line"></span></th>
        </tr>
        </thead>
        <tbody id="studio_reviewers" class="team-tr">
        <?php foreach($get_reviewers as $get_reviewer){ ?>
            <tr>
                <td><p class="rt-name"><?= $get_reviewer['firstname'].' '.$get_reviewer['lastname'] ?></p>
                    <span class="rt-title"><?= $get_reviewer['job_title']; ?></span>
                </td>
                <td><?php echo date('M d, Y',$get_reviewer['created_timestamp']); ?></td>
                <td style="width: 350px">
                    <?php if($get_reviewer['get_reviewer_openings'] != ''){ ?>
                        <?php
                        $total_openings = '';
                        foreach($get_reviewer['get_reviewer_openings'] as $reviewer_opening){?>
                            <?php $total_openings.=$reviewer_opening['job_title'].','; ?>
                        <?php } ?>
                        <?php echo rtrim($total_openings,','); ?>
                    <?php } ?>
                </td>
                <td><a href="mailto:<?= $get_reviewer['email'] ?>"><?= $get_reviewer['email'] ?></a></td>
                <td>
                    <?php if($get_reviewer['status_sl'] == 1){?>
                        <label class="label label-success">Active</label>
                    <?php }else if($get_reviewer['status_sl'] == 0){?>
                        <label class="label label-info rt-pending">Pending</label>
                    <?php } ?>
                </td>
                <td><a href="javascript:void(0)" data-toggle="modal" data-target="#myModal" class="edit_reviewer margin-right-2" data-value="<?= $get_reviewer['user_id'] ?>" >Edit</a> <a href="javascript:void(0)" class="delete_reviewer" data-value="<?= $get_reviewer['user_id'] ?>" >Delete</a></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <!-- START PAGINATION -->
    <form class="inline-input" _lpchecked="1" enctype="multipart/form-data" name="search"
          method="post" action="<?= $vObj->getURL('team') ?>">
        <div class="span12 pagination"><?= $pagination ?></div>
        <input type="hidden" value="<?= $fillter_input_val ?>" name="fillter_text"/>
        <input type="hidden" value="<?= $page ?>" name="page">
        <input type="hidden" value="<?=$sorting?>" name="sorting">
        <input type="hidden" value="<?=$sort_column?>" name="sort_column">
        <input type="hidden" value="<?=$sort_type?>" name="sort_type">
    </form>
<?php }else{ ?>
    <div class="alert alert-warning">
        <i class="icon-warning-sign"></i>
        '<?= $fillter_input_val ?>' not found
    </div>
<?php } ?>