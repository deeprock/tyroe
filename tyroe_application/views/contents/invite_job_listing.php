<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    function rejectInvitaion(job_id) {
        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("JobOpening/RejectInvitation")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".status"+job_id).remove();
                    $(".result"+job_id).css("display","");
                    $(".result"+job_id).html("Rejeted");
                }
            },
            failure: function (errMsg) {
            }
        });

    }

    function acceptInvitaion(job_id) {
        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("JobOpening/AcceptInvitation")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".status"+job_id).remove();
                    $(".result"+job_id).css("display","");
                    $(".result"+job_id).html("Accepted");
                }
            },
            failure: function (errMsg) {
            }
        });

    }
</script>
<div class="content">

<div class="container-fluid">

<div id="pad-wrapper">

<div class="grid-wrapper">

<div class="row-fluid show-grid">

<!-- START LEFT COLUMN -->
    <?php
                            $this->load->view('left_coloum_job');
                            ?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar">
    <div class="right-column">

        <div class="row-fluid header">
            <div class="span4">
                <h3 class="name">All Job Invitations</h3>
            </div>
        </div>
        <div  class="filter-result"></div>
        <!-- START OPENING DETAILS -->
        <?php
        if (is_array($get_invite_jobs)) {

            foreach ($get_invite_jobs as $jobs) {
                ?>
                <div class="row-fluid no-filter job-opening<?php echo $jobs['job_id']; ?>">
                    <div class="span12 group" id="top">

                        <!-- TITLE -->
                        <div class="row-fluid">
                            <div class="span8">
                                <h4>
                                    <a href="<?= $vObj->getURL("openingoverview/JobOverview/" . $jobs['job_id']) ?>">
                                        <?php echo $jobs['job_title']; ?>
                                    </a>
                                </h4>
                            </div>
                            <?php if($jobs['invitation_status'] == 0){ ?>
                            <div class="span4 status<?php echo $jobs['job_id']; ?>">
                                <ul class="actions pull-right">
                                    <li><i class="table-accept" onclick="acceptInvitaion('<?= $jobs['job_id'] ?>');"></i>Accept</li>
                                    <li><i class="table-delete" onclick="rejectInvitaion('<?= $jobs['job_id'] ?>');"></i>Reject</li>
                                </ul>
                            </div>
                            <div class=" span4 span_result result<?= $jobs['job_id']; ?>" style="display: none"></div>
                                <?php } else { ?>
                            <div class="span4 span_result">
                                <?php if($jobs['invitation_status']==1){echo "Accepted";}elseif($jobs['invitation_status']== -1){echo "Rejected";}?>
                            </div>
                                <?php } ?>
                        </div>

                        <!-- INFO -->
                        <div class="row-fluid" id="opening-stats">
                            <div class="span4">
                                <div class="row-fluid">
                                    <div class="span12">
                                        <div class="label label-success">Reviews Completed</div>
                                        <p><strong>Location:</strong> <?php echo $jobs['job_location']; ?></p>
                                        <p>
                                            <strong>Date:</strong> <?php echo date('d M', $jobs['start_date']) . " - " . date('d M', $jobs['end_date']); ?>
                                        </p>

                                        <p><strong>Level:</strong> <?php echo $jobs['job_skill']; ?></p>

                                        <p><strong>Quantity:</strong> <?php echo $jobs['job_quantity']; ?></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>


        <!-- END OPENING DETAILS -->

    </div>


</div>
<!-- END RIGHT COLUMN -->

<div>


</div>


</div>
</div>
</div>
</div>
