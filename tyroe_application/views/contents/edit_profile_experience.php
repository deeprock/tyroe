<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<section id="latest-work">

    <div class="container">
        <div class="row-fluid">
            <div class="span12">
                <h3>Resume</h3>
            </div>
        </div>

        <?php
        $param = "";

        if (is_array($get_experience)) {
            $param = "style='color: #96BF48'";
        }

        if ($get_visibility[0]['visible_section'] == "exp" && $get_visibility[0]['visibility'] == "1") {
            $visibile_param = "style='color: #F97E76'";
        }
        ?>

        <section class="work-exp-container exp">
            <div class="row-fluid">
                <div class="span4"></div><!-- Keep this div empty to align -->
                <div class="span8">
                    <a name="education" class="division"></a><h2>Work Experience
                    <!--<i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark" <?php/*=$param*/?>></i>
                    <i class="btn-dis icon-minus-sign pull-right done-completed visibility_section_icon" data-visibile-sec="exp" data-visibile-val="<?php/*= $get_visibility[0]['visibility'] */?>" <?php/*=$visibile_param*/?>></i>--></h2>
                </div>
            </div>
            <div style="display: none;" class="row-fluid work-experience-data-backup resume-save-main-element">
                <div class="span4">
                    <input type="hidden" name="item_id" value="0">
                    <input type="hidden" name="callfrom" value="experience">
                    <h4 data-name="job_title" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_JOBTITLE?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_EXPERIENCE_JOBTITLE?></h4>
                    <h4 data-name="company_name" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_COMPANY?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_EXPERIENCE_COMPANY?></h4>
                    <h4 class="custom_profile_editor"><span data-name="job_start" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_STARTDATE?>" class=" experience-startdate custom-datepicker-backup" data-date="" data-date-format="dd-mm-yyyy"><?=LABEL_PROFILE_RESUME_EXPERIENCE_STARTDATE?></span> - <span data-name="job_end" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_ENDDATE?>" custom-attribute="enddate"  class="experience-enddate custom-datepicker-backup"><?=LABEL_PROFILE_RESUME_EXPERIENCE_ENDDATE?></span></h4>
                    </div>
                <div data-name="job_description" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                   <?=LABEL_PROFILE_RESUME_EXPERIENCE_DESCRIPTION?>
                </div>
            </div>

            <div style="display: none;" class="row work-experience-delete-btn-backup">
                <div class="span3">
                </div>
                <div class="span8">
                    <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                        <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                    </div>
                </div>
            </div>

<?php
            foreach($get_experience as $k=>$v){
                if($v['current_job']>0){
                    $current_job_index=$k;
                }
            ?>
            <div class="row-fluid work-experience-data resume-save-main-element">
                <div class="span4">
                    <input type="hidden" name="item_id" value="<?=$v['exp_id']?>">
                    <input type="hidden" name="callfrom" value="experience">
                    <h4 data-name="job_title" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_JOBTITLE?>" contenteditable="true"><?=($v['job_title']!=""?$v['job_title']:LABEL_PROFILE_RESUME_EXPERIENCE_JOBTITLE)?></h4>
                    <h4 data-name="company_name" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_COMPANY?>" contenteditable="true"><?=($v['company_name']!=""?$v['company_name']:LABEL_PROFILE_RESUME_EXPERIENCE_COMPANY)?></h4>
                    <h4><span data-name="job_start" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_STARTDATE?>" class="custom_profile_editor experience-startdate custom-datepicker" data-date="<?=date('m/d/Y',$v['job_start'])?>" data-date-format="dd-mm-yyyy"><?=date('m/d/Y',$v['job_start'])?></span> - <span data-date="<?=date('m/d/Y',$v['job_end'])?>" data-date-format="dd-mm-yyyy"  data-name="job_end" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_ENDDATE?>" custom-attribute="enddate"  class="experience-enddate custom-datepicker"><?=($v['current_job']>0)?"Present":date('m/d/Y',$v['job_end'])?></span></h4>
                    </div>
                <div data-name="job_description" data-placeholder="<?=LABEL_PROFILE_RESUME_EXPERIENCE_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                    <?=($v['job_description']!=""?stripslashes($v['job_description']):LABEL_PROFILE_RESUME_EXPERIENCE_DESCRIPTION)?>
                </div>
            </div>

            <div class="row work-experience-delete-btn">
                <div class="span3">
                </div>
                <div class="span8">
                    <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                        <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                    </div>
                </div>
            </div>

            <?php } ?>
            <div class="row-fluid">
                <div class="span12 accordion-heading">
                    <a class="accordion-toggle add-key-button add-key-button-resume work-experience-add-more-btn">Add experience <span class="font-icon-plus pull-right"></span></a>
                </div><!-- span12 -->
            </div><!-- row -->
        </section>




        <div class="row-fluid">
            <hr class="span12" style="border-color: rgb(165, 179, 185);">
        </div>
        <?php
        $param = "";
        $visibile_param = "";
        if (is_array($get_education)) {
            $param = "style='color: #96BF48'";
        }
        if ($get_visibility[1]['visible_section'] == "edu" && $get_visibility[1]['visibility'] == "1") {
            $visibile_param = "style='color: #F97E76'";
        }
        ?>
        <section class="edu-container edu">
            <div class="row-fluid">
                <div class="span4"></div><!-- Keep this div empty to align -->
                <div class="span8">
                    <a name="education" class="division"></a><h2>Education  <!--<i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark" <?php/*=$param*/?>></i><i class="btn-dis icon-minus-sign pull-right done-completed visibility_section_icon" data-visibile-sec="edu" data-visibile-val="<?php/*= $get_visibility[1]['visibility'] */?>"<?php/*=$visibile_param*/?>></i>--></h2>
                </div><!-- span8 -->
            </div><!-- row -->


            <div style="display: none;" class="row-fluid education-detail-data-backup resume-save-main-element">
                           <div class="span4">
                               <input type="hidden" name="item_id" value="0">
                               <input type="hidden" name="callfrom" value="education">
                               <h4 data-name="edu_position" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_DEGREE?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_EDUCATION_DEGREE?></h4>
                               <h4 data-name="edu_organization" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_SCHOOL?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_EDUCATION_SCHOOL?></h4>
                               <h4 class="custom_profile_editor"><span data-name="start_year" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_STARTDATE?>" class=" experience-startdate custom-datepicker-backup" data-date="" data-date-format="dd-mm-yyyy"><?=LABEL_PROFILE_RESUME_EDUCATION_STARTDATE?></span> - <span data-name="end_year" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_ENDDATE?>" custom-attribute="enddate"  class="experience-enddate custom-datepicker-backup"><?=LABEL_PROFILE_RESUME_EDUCATION_ENDDATE?></span></h4>
                               </div>
                           <div data-name="education_description" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                              <?=LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION?>
                           </div>
                       </div>

                       <div style="display: none;" class="row work-experience-delete-btn-backup">
                           <div class="span3">
                           </div>
                           <div class="span8">
                               <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                                   <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                               </div>
                           </div>
                       </div>

           <?php
                       foreach($get_education as $k=>$v){
                           if($v['current_job']>0){
                               $current_job_index=$k;
                           }
                       ?>
                       <div class="row-fluid education-detail-data resume-save-main-element">
                           <div class="span4">
                               <input type="hidden" name="item_id" value="<?=$v['education_id']?>">
                               <input type="hidden" name="callfrom" value="education">
                               <h4 data-name="edu_position" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_DEGREE?>" contenteditable="true"><?=($v['edu_position']==""?LABEL_PROFILE_RESUME_EDUCATION_DEGREE:$v['edu_position'])?></h4>
                               <h4 data-name="edu_organization" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_SCHOOL?>" contenteditable="true"><?=($v['edu_organization']==""?LABEL_PROFILE_RESUME_EDUCATION_SCHOOL:$v['edu_organization'])?></h4>
                               <h4><span data-name="start_year" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_STARTDATE?>" class="custom_profile_editor experience-startdate custom-datepicker" data-date="<?=date('m/d/Y',$v['start_year'])?>" data-date-format="dd-mm-yyyy"><?=date('m/d/Y',$v['start_year'])?></span> - <span data-date="<?=date('m/d/Y',$v['end_year'])?>" data-date-format="dd-mm-yyyy"  data-name="end_year" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_ENDDATE?>" custom-attribute="enddate"  class="experience-enddate custom-datepicker"><?=($v['current_job']>0)?"Present":date('m/d/Y',$v['end_year'])?></span></h4>
                               </div>
                           <div data-name="education_description" data-placeholder="<?=LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                               <?=($v['education_description']!=""?$v['education_description']:LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION)?>
                           </div>
                       </div>

                       <div class="row work-experience-delete-btn">
                           <div class="span3">
                           </div>
                           <div class="span8">
                               <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                                   <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                               </div>
                           </div>
                       </div>

                       <?php } ?>



            <div class="row-fluid">
                <!--<div class="span12">
                    <button class="btn-new-anb add-key-button add-key-button-resume education-detail-add-more-btn">Add Education <i class="pull-right icon-chevron-down" <?php/*=$visibile_param*/?>></i></button>
                </div>-->
                <div class="span12 accordion-heading">
                    <a class="accordion-toggle add-key-button add-key-button-resume education-detail-add-more-btn">Add Education <span class="font-icon-plus pull-right"></span></a>
                </div><!-- span12 -->
            </div><!-- row -->
        </section>

        <div class="row-fluid">
            <hr class="span12" style="border-color: rgb(165, 179, 185);">
        </div>
        <?php
        $param = "";
        $visibile_param = "";
        if (is_array($get_awards)) {
            $param = "style='color: #96BF48'";
        }
        if ($get_visibility[2]['visible_section'] == "award" && $get_visibility[2]['visibility'] == "1") {
            $visibile_param = "style='color: #F97E76'";
        }
        ?>

        <section class="awards-container award">
            <div class="row-fluid">
                <div class="span4"></div><!-- Keep this div empty to align -->
                <div class="span8">
                    <a name="education" class="division"></a><h2>Awards <!--<i class="btn-dis icon-ok-sign pull-right ico-done ico-done-latest done-completed complete-tick-mark" <?php/*=$param*/?>></i><i class="btn-dis icon-minus-sign pull-right done-completed visibility_section_icon" data-visibile-sec="award" data-visibile-val="<?php/*= $get_visibility[2]['visibility'] */?>" <?php/*=$visibile_param*/?>></i>--></h2>
                </div><!-- span8 -->
            </div><!-- row -->
            <div style="display: none;" class="row-fluid award-detail-data-backup resume-save-main-element">
                            <div class="span4">
                                <input type="hidden" name="item_id" value="0">
                                <input type="hidden" name="callfrom" value="awards">
                                <h4 data-name="award_organization" class="resume-save-class custom_profile_editor" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_ORGANIZATION?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_AWARD_ORGANIZATION?></h4>
                                <h4 data-name="award" class="resume-save-class custom_profile_editor" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD?>" contenteditable="true"><?=LABEL_PROFILE_RESUME_AWARD?></h4>
                                <h4><span custom-attribute="years" data-name="award_year" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_YEAR?>" class="experience-startdate custom-datepicker-backup custom_profile_editor" data-date="" data-date-format="dd-mm-yyyy"><?=LABEL_PROFILE_RESUME_AWARD_YEAR?></span></h4>
                                </div>
                            <div data-name="award_description" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                               <?=LABEL_PROFILE_RESUME_AWARD_DESCRIPTION?>
                            </div>
                        </div>

                        <div style="display: none;" class="row work-experience-delete-btn-backup">
                            <div class="span3">
                            </div>
                            <div class="span8">
                                <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                                    <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                                </div>
                            </div>
                        </div>

            <?php
                        foreach($get_awards as $k=>$v){
                         ?>
                        <div class="row-fluid award-detail-data resume-save-main-element">
                            <div class="span4">
                                <input type="hidden" name="item_id" value="<?=$v['award_id']?>">
                                <input type="hidden" name="callfrom" value="awards">
                                <h4 data-name="award_organization" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_ORGANIZATION?>" contenteditable="true"><?=($v['award_organization']!=""?$v['award_organization']:LABEL_PROFILE_RESUME_AWARD_ORGANIZATION)?></h4>
                                <h4 data-name="award" class="custom_profile_editor resume-save-class" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD?>" contenteditable="true"><?=($v['award']!=""?$v['award']:LABEL_PROFILE_RESUME_AWARD)?></h4>
                                <h4><span custom-attribute="years" data-name="award_year" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_YEAR?>" class="custom_profile_editor experience-startdate custom-datepicker" data-date="01/01/<?=$v['award_year']?>" data-date-format="dd-mm-yyyy"><?=($v['award_year']!=""?$v['award_year']:LABEL_PROFILE_RESUME_AWARD_YEAR)?></span> </h4>
                                </div>
                            <div data-name="award_description" data-placeholder="<?=LABEL_PROFILE_RESUME_AWARD_DESCRIPTION?>" contenteditable="true" class="custom_profile_editor span8 resume-save-class">
                                <?=($v['award_description']!=""?$v['award_description']:LABEL_PROFILE_RESUME_AWARD_DESCRIPTION)?>
                            </div>
                        </div>

                        <div class="row work-experience-delete-btn">
                            <div class="span3">
                            </div>
                            <div class="span8">
                                <div class="span2 cus-image com-right key-details-section-visibility pull-right">
                                    <a class="key-details-section-delete pull-right"><i class="icon-remove recommendation_pending_approval_remove_btn  pull-left"></i></a>
                                </div>
                            </div>
                        </div>

                        <?php } ?>
            <div class="row-fluid">
                <!--<div class="span12">
                    <button class="btn-new-anb add-key-button add-key-button-resume award-detail-add-more-btn">Add Awards <i class="pull-right icon-chevron-down"></i></button>
                </div>-->
                <div class="span12 accordion-heading">
                    <a class="accordion-toggle add-key-button add-key-button-resume award-detail-add-more-btn">Add Awards <span class="font-icon-plus pull-right"></span></a>
                </div><!-- span12 -->
            </div><!-- row -->
        </section>

        <div class="row-fluid">
            <hr class="span12" style="border-color: rgb(165, 179, 185);">
        </div>
    <script type="text/javascript">
        function clone_data(obj,btnObj){
            var clone_data=$(obj).clone();
            var clone_button=$(obj).next().clone();
            $(clone_data).addClass(obj.replace('-backup','')).removeClass(obj.replace('.',''));
            $(clone_data).show();
            $(clone_button).show();
            $(clone_button).addClass('work-experience-delete-btn').removeClass('work-experience-delete-btn-backup');
            $(clone_data).find('.custom-datepicker-backup').addClass('custom-datepicker').removeClass('custom-datepicker-backup');
            custom_datepicker($(clone_data).find('.custom-datepicker'));
            $(clone_data).insertBefore($(btnObj).closest('.row-fluid'));
            $(clone_button).insertBefore($(btnObj).closest('.row-fluid'));
        }
        $(document).on('click','.work-experience-add-more-btn',function(){
            clone_data('.work-experience-data-backup',this);
       });
        $(document).on('click','.education-detail-add-more-btn',function(){
            clone_data('.education-detail-data-backup',this);
       });
        $(document).on('click','.award-detail-add-more-btn',function(){

            clone_data('.award-detail-data-backup',this);
        });
        $(document).on('blur','.resume-save-class',function(){
            save_resume_data(this)
        });
        function customSetDate(ev,obj){
            var date=ev.date;
            var month=date.getMonth()+1;
            var day=date.getDate();
            if(month<10)
                month='0'+month;
            if(day<10)
                day='0'+day;
            var strDate='';
            if($(obj).attr('custom-attribute')=="years"){
                strDate= date.getFullYear();
            }else{
                strDate= (month)+"/"+day+"/"+ date.getFullYear();
            }
            $(obj).html(strDate);
            $(obj).datepicker('hide');
            save_resume_data(obj)
        }

        function save_resume_data(obj){
           var main_div=$(obj).closest('.resume-save-main-element');
           var data={};
           var value= $.trim($(obj).html());
           var placeholder=$.trim($(obj).attr('data-placeholder'));
           var obj=obj;
           var obj_itemid='';
           if(value.toLowerCase()!=placeholder.toLowerCase()){
               $(main_div).find('input').each(function(){
                   var attr_name=$(this).attr('name');
                   if(attr_name=="item_id")
                       obj_itemid=this;
                   data[attr_name]=$(this).val();
               });
               if(value.toLowerCase()=="present"){
                   data["endpresent"]=true;
               }else{
                   data["endpresent"]=false;
               }
               data[$(obj).attr('data-name')]=value;
               $.ajax({
                   type: 'POST',
                   data: data,
                   url: '<?=$vObj->getURL("profile/save_profile");?>',
                   dataType: 'json',
                   success: function (responseData) {
                     if(responseData.status){
                         $(obj_itemid).val(responseData.item_id);
                         if(responseData.total_rows > 0){
                             $("."+responseData.clas).find($(".complete-tick-mark")).css("color","#96BF48");
                             /*$(".complete-tick-mark").css("color","#96BF48");*/
                         }
                         if(responseData.scorer.resume_scorer+responseData.scorer.portfolio_scorer+responseData.scorer.profile_scorer > "74")
                         {
                           $('.custom-p-color').html('Congratulations, you can publish your profile!');
                         }else
                         {
                           $('.custom-p-color').html('Reach 75% and get published!');
                         }
                         if(responseData.scorer.resume_scorer == "" || responseData.scorer.resume_scorer == "0")
                         {
                            var resume_color = "#F9785B";
                         }
                         else
                         {
                            var resume_color = "#76bdee";
                         }
                         if(responseData.scorer.portfolio_scorer=="" || responseData.scorer.portfolio_scorerr == "0")
                         {
                            var portfolio_color ="#F9785B";
                         }
                         else
                         {
                            var portfolio_color = "#c4dafe";
                         }
//                         new Morris.Donut({
//                            element: 'hero-donut',
//                            width:300,
//                            data: [
//                                {label: 'Profile', value: responseData.scorer.profile_scorer },
//                                {label: 'Resume', value: responseData.scorer.resume_scorer },
//                                {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                                {label: 'Incomplete', value: responseData.scorer.incomplete }
//                            ],
//                             colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                            formatter: function (y) {
//                                return y + "%"
//                            }
//                         });
//                       
//                           
                         //update_knob(responseData.scorer.fullstatus);
                         
                         var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                         update_profile_completness_top_chart(prof_comp);
                         publish_unpublish_btn(responseData.scorer.fullstatus);
                     }
                   }
               });
           }
        }

        function customCheckbox(ev,obj){
            if(ev.checked){
                $(obj).html('present');
            }else{
                $(obj).html($(obj).attr('data-placeholder'));
            }
            $(obj).datepicker('hide');
            save_resume_data(obj)
        }

        function custom_datepicker(obj){
            $(obj).each(function(i,v){
                var attribute=$(this).attr("custom-attribute")
               if(attribute=="enddate"){
                   var default_check=false;
                   if($.trim($(this).text()).toLowerCase()=="present"){
                       default_check=true;
                   }
                   $(this).datepicker({'showCheckbox':true,'showCheckboxSelected':default_check}).on('changeDate',function(ev){customSetDate(ev,this)}).on('customCheckbox',function(ev){customCheckbox(ev,this)});
               }else if(attribute=="years"){
                   $(this).datepicker({'showCheckbox':false,format: " yyyy",viewMode: "years",minViewMode: "years"}).on('changeDate',function(ev){customSetDate(ev,this)});
               }else{
                   $(this).datepicker({'showCheckbox':false}).on('changeDate',function(ev){customSetDate(ev,this)});
               }
           });
        }

        $(document).on('click','.key-details-section-delete',function(){

                var obj_detail_section=$(this).closest('.work-experience-delete-btn').prev('div');
                var current_div=$(this).closest('.work-experience-delete-btn');
                var item_id=$(obj_detail_section).find('input[name=item_id]').val();
                var callfrom=$(obj_detail_section).find('input[name=callfrom]').val();
                if(item_id>0){
                    $.ajax({
                         type: 'POST',
                         data: {'item_id':item_id,'callfrom':'remove'+callfrom},
                         url: '<?=$vObj->getURL("profile/save_profile");?>',
                         dataType: 'json',
                         success: function (responseData) {
                           if(responseData.status){
                               $(obj_detail_section).remove();
                               $(current_div).remove();
                               if(responseData.total_rows == 0){
                               $("."+responseData.clas).find($(".complete-tick-mark")).css("color","");
                                                        }
                               if(responseData.scorer.resume_scorer+responseData.scorer.portfolio_scorer+responseData.scorer.profile_scorer > "74")
                                {
                                  $('.custom-p-color').html('Congratulations, you can publish your profile!');
                                }else
                                {
                                  $('.custom-p-color').html('Reach 75% and get published!');
                                }
                                if(responseData.scorer.resume_scorer == "" || responseData.scorer.resume_scorer == "0")
                                {
                                   var resume_color = "#F9785B";
                                }
                                else
                                {
                                   var resume_color = "#76bdee";
                                }
                                if(responseData.scorer.portfolio_scorer=="" || responseData.scorer.portfolio_scorerr == "0")
                                {
                                   var portfolio_color ="#F9785B";
                                }
                                else
                                {
                                   var portfolio_color = "#c4dafe";
                                }
//                               new Morris.Donut({
//                                   element: 'hero-donut',
//                                   width: 300,
//                                   data: [
//                                       {label: 'Profile', value: responseData.scorer.profile_scorer },
//                                       {label: 'Resume', value: responseData.scorer.resume_scorer },
//                                       {label: 'Portfolio', value: responseData.scorer.portfolio_scorer },
//                                       {label: 'Incomplete', value: responseData.scorer.incomplete }
//                                   ],
//                                   colors: ["#30a1ec", resume_color, portfolio_color, "#F9785B"],
//                                   formatter: function (y) {
//                                       return y + "%"
//                                   }
//                               });
                               //update_knob(responseData.scorer.fullstatus);
                               var prof_comp = parseInt(responseData.scorer.profile_scorer) + parseInt(responseData.scorer.resume_scorer) + parseInt(responseData.scorer.portfolio_scorer);
                               update_profile_completness_top_chart(prof_comp);
                               publish_unpublish_btn(responseData.scorer.fullstatus);
                           }
                         }
                    });
                }else{
                    $(obj_detail_section).remove();
                    $(current_div).remove();
                }
            })
        $(document).ready(function(){
            custom_datepicker('.custom-datepicker');
        });

    </script>
