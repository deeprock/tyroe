<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>rating/jquery.js"></script>
<script type="text/javascript">
    $(document).keypress(function (e) {
        if (e.keyCode == 13) {
            valid_login();
            return false;
        }
    });

    function valid_login() {
        $(".login_btn").hide();
        $("#big_loader").show();
        var user_email = $("#user_email").val();
        var login_password = $("#login_password").val();
        var remember_me = 0;
        if ($('#remember_me').is(":checked")) {
            remember_me = 1;
        }
        //var remember_me = $("#remember_me").check;
        if (user_email != "" && login_password != "") {
//            var data = "user_email=" + user_email + "&pass=" + login_password + "&rem=" + remember_me;
//            $.ajax({
//                type: "POST",
//                data: data,
//                url: '<?=$vObj->getURL("login/login_auth")?>',
//                dataType: "json",
//                success: function (data) {
//                    //session redirect_to_job_overview &&  redirect_to_job_opening from api buttons redirection
//                    if (data.success) {
//                        <?php if($this->session->userdata('redirect_to_job_overview') == 'yes' ){?>
//                            window.location = '<?=$vObj->getURL("openings")?>';
//                        <?php }else if($this->session->userdata('redirect_to_job_opening') == 'yes'){ ?>
//                            window.location = '<?=$vObj->getURL("openings")?>';
//                        <?php }else{ ?>
//                            window.location = '<?=$vObj->getURL("home")?>';
//                        <?php } ?>
//                    }
//                    else {
//                        $(".login_btn").show();
//                        $("#big_loader").hide();
//                        $("#error_msg").html("Invalid Email/Password");
//                    }
//                },
//                failure: function (errMsg) {
//                }
//            });
            $('#login_form').submit();
              
        }
        else {
            $(".login_btn").show();
            $("#big_loader").hide();
            $("#error_msg").html("Provide Authentication Details");
        }

    }


</script>

<!-- main container -->


<div class="row-fluid login-wrapper">
    <a href="<?= LIVE_SITE_URL ?>">
        <img class="logo" src="<?= ASSETS_PATH ?>img/logo-dark.png">
    </a>

    <div class="span4 box">
        <form action="<?= $vObj->getURL('login') ?>" enctype="multipart/form-data" name="login_form" method="post" id="login_form">
            <div class="content-wrap">
                <h6><?= LABEL_LOGIN ?></h6>
                <span id="error_msg"><?=$error_already_invite?></span>
                <input class="span12" type="email" name="user_email" id="user_email" placeholder="E-mail address" value="<?php echo $_COOKIE['remember_email']; ?>">
                <input class="span12" type="password" name="pass" id="login_password" placeholder="Your password" value="<?php echo $_COOKIE['remember_key']; ?>">
                <a href="<?= $vObj->getURL('login/PasswordForgot') ?>" class="forgot"><?= LABEL_PASSWORD_FORGOT ?></a>
                
                <div class="remember">
                    <input id="remember_me" name="rem" type="checkbox"
                           value="1" <?php if ($_COOKIE['remember_chk'] == "1") echo "checked=''" ?>>
                    <label for="remember_me"><?php echo LABEL_REMMEMBER_ME ?></label>
                </div>
                <div style="display: none" id="big_loader" ><img src="<?=ASSETS_PATH?>img/big_loader.gif" width="50px" /> <!--Signing...--></div>
               <div class="login_btn"> <a class="btn-glow primary login" onclick="valid_login();"><?= LABEL_SIGNIN ?></a></div>
            </div>  
        </form>
        
    </div>

    <div class="span4 no-account">
        <p><?= LABEL_NO_ACCOUNT ?></p>
        <a href="<?= $vObj->getURL('register') ?>"><?= LABEL_REGISTERATION ?></a>
    </div>
</div>
<!-- pre load bg imgs -->
<script type="text/javascript">
    $(function () {
        // bg switcher
        var $btns = $(".bg-switch .bg");
        $btns.click(function (e) {
            e.preventDefault();
            $btns.removeClass("active");
            $(this).addClass("active");
            var bg = $(this).data("img");

            $("html").css("background-image", "url('img/bgs/" + bg + "')");
        });
    });
</script>
