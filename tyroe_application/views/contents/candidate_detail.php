<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?=ASSETS_PATH?>rating/jquery.js"></script>
<link href="<?=ASSETS_PATH?>rating/rating_simple.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<?=ASSETS_PATH?>rating/rating_simple.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
                $("#rating_simple3").webwidget_rating_simple({
                    rating_star_length: '5',
                    rating_initial_value: '<?php echo $get_rating?>',
                    rating_function_name: 'InsertRating',//this is function name for click
                    directory: '<?=ASSETS_PATH?>rating/'
                });
            });
    function InsertRating(value) {
        var data = "rating=" + value + "&uid=" +<?php echo $candidate_detail['user_id'];?>;
               $.ajax({
                   type: "POST",
                   data: data,
                   url: '<?=$vObj->getURL("findtalent/SaveRating")?>',
                   dataType: "json",
                   success: function (data) {
                       if (data.success) {
                           $(".job-opening" + job_id).hide();
                           $(".notification-box-message").html(data.success_message);
                           $(".notification-box").show(100);
                           setTimeout(function () {
                               $(".notification-box").hide();
                           }, 5000);
                       }
                       else {
                           $(".notification-box-message").css("color", "#b81900")
                           $(".notification-box-message").html(data.success_message);
                           $(".notification-box").show(100);
                           setTimeout(function () {
                               $(".notification-box").hide();
                           }, 5000);
                       }
                   },
                   failure: function (errMsg) {
                   }
               });

        }


    function InsertComment() {
        var comment = $("#comment_text").val();

        if (comment == "") {
            $("#comment_text").focus();
            $("#comment_text").css("border", '1px solid red');
        } else {
            var uid = $("#uid").val();
            var studio = $("#studio").val();
            var data = "comment=" + comment + "&uid=" + uid + "&studio=" + studio;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("findtalent/SaveComment")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $("#comment_text").val("");
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                    else {
                        $(".notification-box-message").css("color", "#b81900")
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                        //$("#error_msg").html(data.success_message);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }
    function favouriteCandidate() {
        var uid = $("#uid").val();

        var data = "uid=" + uid;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    }

</script>
<!-- main container -->
<div class="content">

<div class="container-fluid">

<!-- upper main stats -->
<?php
//$this->load->view('navigation_top');
?>
<!-- end upper main stats -->

<div id="pad-wrapper">

<div class="grid-wrapper">

<div class="row-fluid show-grid">

<!-- START LEFT COLUMN -->
<?php
$this->load->view('left_coloumn');
?>
<!-- END LEFT COLUMN -->

<!-- START RIGHT COLUMN -->
<div class="span8 sidebar">
<div class="right-column" style="position:relative;">

<!-- margin pointer -->
<div class="left-pointer hidden-phone" id="candidate">
    <div class="left-arrow"></div>
    <div class="left-arrow-border"></div>
</div>


<div class="row-fluid">
    <div class="span12">
        <div class="btn-group large-audio" id="prev-next">
            <a href="javascript:history.back();">
                <button class="btn glow"><i class="icon-caret-left"> Back to Candidates</i>
                </button>
            </a>

        </div>
        <div class="btn-group large-audio pull-right" id="prev-next">
            <button class="btn glow left"><a href="__opening-candidate-01.html"><i
                        class="icon-caret-left"></i>
                </a></button>
            <button class="btn glow right"><a href="__opening-candidate-02.html"><i
                        class="icon-caret-right"></i></a>
            </button>

        </div>

    </div>

</div>

<!-- START CANDIDATE ROW -->
<div class="row-fluid">
    <div class="span12 group" id="top" style="background-color: #f5fafc;">
        <div class="row-fluid" id="user_info">

            <div class="span8 push-down-10">
                <div class="span12">
                    <div class="span3" id="avatar">
                        <input type="hidden" name="uid" id="uid" value="<?php echo $candidate_detail['user_id']; ?>">

                        <?php

                        $image;

                        if ($candidate_detail['media_name'] != "") {
                            // $image=ASSETS_PATH_NO_IMAGE;
                            $image = ASSETS_PATH_PROFILE_THUMB . $candidate_detail['media_name'];
                        } else {
                            $image = ASSETS_PATH_NO_IMAGE;
                        }
                        ?>
                        <img src="<?php echo $image; ?>"
                             class="img-circle avatar_search_large">

                        <!-- settings button -->
                        <div class="btn-group settings span12" id="under-avatar">
                            <button class="btn glow"><i class="icon-cog"></i></button>
                            <button class="btn glow dropdown-toggle" data-toggle="dropdown">
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" style="text-align: left;">
                                <li><a href="javascript:void(0);" onclick="favouriteCandidate();">Favourite</a></li>
                                <li><a href="#">Set As Candidate</a></li>
                                <li>
                                    <a href="<?= $vObj->getURL("findtalent/ViewJobs/" . $candidate_detail['user_id']) ?>">Shortlist</a>
                                </li>
                                <li><a href="#">Hide</a></li>
                            </ul>
                        </div>

                    </div>

                    <!-- user information -->
                    <div class="span8">
                        <a class="name"
                           style="margin:0px;"><?php echo $candidate_detail['username']; ?></a>
                                                    <span class="location"
                                                          style="display: block;"><?php echo $candidate_detail['state'] . " " . $candidate_detail['country']; ?></span>
                        <span class="location"
                               style="display: block;"> <input name="my_input" value="7" id="rating_simple3" type="hidden"> </span>

                        <!-- sliders -->

                    </div>
                </div>
            </div>


            <div class="span4 phone-offset-search">
                                        <div class="row-fluid show-grid" style="margin-bottom: 4px;">

                                            <?php
                                            for ($a = 0;$a < 3;$a++) {
                                            $portfolio = $get_portfolio;
                                            ?>
                                            <div class="span6 img-grid">
                                                <img src="<?php echo ASSETS_PATH_PORTFOLIO_THUMB . $portfolio[$a]['media_name']; ?>">
                                            </div>
                                            <?php
                                            if ($a == 0) {
                                            ?>
                                            <div class="span6 img-grid">
                                                <div class="img-grid-score">
                                                    <div class="thumb-score">
                                                        <span><?php
                                                            echo $vObj->get_profile_score($portfolio[$a]['user_id']);
                                                            ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row-fluid show-grid" style="margin-bottom: 4px;">
                                            <?php
                                            }

                                            }
                                            ?>

                                        </div>

                                    </div>
        </div>
    </div>
</div>

<!-- REVIEW HEADING -->
<div class="row-fluid head sections">
    <div class="span12">
        <h4>Background</h4>
    </div>
</div>


<!-- START BACKGROUND -->
<div class="row-fluid">
    <div class="span12 people" id="top">
        <div class="span12">
            <h4> Experience</h4></div>
        <?php
        if (!is_array($get_experience)) {
            echo "No Details";
        } else {


            foreach ($get_experience as $experience) {
                ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span12">
                            <div class="span8">
                                <a href="javascript:void(0);" class="name"
                                   style="margin:0px;"><?php echo $experience['job_title']; ?></a>
                                <span class="location"
                                      style="display: block;"><?php echo $experience['company_name']; ?></span>

                                            <span class="tags" style="display: block;"><?php
                                                $job_end;
                                                if ($experience['current_job'] == "1") {
                                                    $job_end = "present";
                                                } else {
                                                    $job_end = $experience['job_end'];
                                                }
                                                echo $experience['job_start'] . " - " . $job_end;?></span>


                            </div>
                        </div>
                    </div>
                    <div class="row-fluid ui-elements">
                        <div class="span10">
                            <p><?php echo $experience['job_description']; ?> </p>
                        </div>
                    </div>

                </div>
            <?php
            }
        }
        ?>
    </div>
</div>
<!-- EDUCATION -->
<div class="row-fluid">
    <div class="span12 people" id="top">
        <div class="span12">
            <h4> Education</h4></div>
        <?php
        if (!is_array($get_education)) {
            echo "No Details";
        } else {
            foreach ($get_education as $education) {
                ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span12">
                            <div class="span8">
                                <a href="javascript:void(0);" class="name"
                                   style="margin:0px;"><?php echo $education['degree_title']; ?></a>
                                <span class="location"
                                      style="display: block;"><?php echo $education['institute_name']; ?></span>

                                                               <span class="tags" style="display: block;"><?php
                                                                   $end_year;
                                                                   if ($education['current_job'] == "1") {
                                                                       $job_end = "present";
                                                                   } else {
                                                                       $end_year = $education['end_year'];
                                                                   }
                                                                   echo $education['start_year'] . " - " . $end_year;?></span>

                            </div>
                        </div>
                    </div>
                    <div class="row-fluid ui-elements">
                        <div class="span10">
                            <p><?php echo $education['education_description']; ?> </p>
                        </div>
                    </div>

                </div>
            <?php
            }
        }
        ?>
    </div>
</div>
<!-- Skills -->
<div class="row-fluid">
    <div class="span12 people" id="top">
        <div class="span12">
            <h4> Skills/Experties</h4></div>
        <?php
        if (!is_array($get_skills)) {
            echo "No Details";
        } else {
            foreach ($get_skills as $skills) {
                ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span12">
                            <div class="span8">
                                <a href="javascript:void(0);" class="name"
                                   style="margin:0px;"><?php echo $skills['skill']; ?></a>
                            </div>
                        </div>
                    </div>

                </div>
            <?php
            }
        }
        ?>
    </div>
</div>
<!-- Awards -->
<div class="row-fluid">
    <div class="span12 people" id="top">
        <div class="span12">
            <h4> Awards</h4></div>
        <?php
        if (!is_array($get_awards)) {
            echo "No Details";
        } else {
            foreach ($get_awards as $awards) {
                ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span12">
                            <div class="span8">
                                <a href="javascript:void(0);" class="name"
                                   style="margin:0px;"><?php echo $awards['award']; ?></a>

                            </div>
                        </div>
                    </div>
                </div>
            <?php
            }
        }
        ?>
    </div>
</div>
<!-- Refrences -->
<div class="row-fluid">
    <div class="span12 people" id="top">
        <div class="span12">
            <h4> Refrences</h4></div>
        <?php
        if (!is_array($get_refrences)) {
            echo "No Details";
        } else {
            foreach ($get_refrences as $refrences) {
                ?>
                <div class="row-fluid">
                    <div class="span8">
                        <div class="span12">
                            <div class="span8">
                                <a href="user-profile.html" class="name"
                                   style="margin:0px;"><?php echo $refrences['refrence_detail']; ?></a>

                            </div>
                        </div>
                    </div>


                </div>
            <?php
            }
        }
        ?>
    </div>
</div>


<!-- Comment Section -->
<div class="row-fluid">
    <div class="header section field-box comment-box">
        <div class="span12">
            <h4> Comment</h4></div>
        <input type="hidden" id="uid" name="uid" value="<?php echo $candidate_detail['user_id']; ?>">
        <textarea class="span9 inline-input"
                  name="comment_text" id="comment_text" placeholder="Type Comment Here"></textarea> <br>
        <a class="btn-flat primary" id="insert_comment" onclick="InsertComment();">Save</a>OR
        <input type="reset" value="Cancel" class="reset" onclick="hideComment();"></span>


    </div>

</div>


</div>
</div>
</div>
</div>
<!-- END RIGHT COLUMN -->
<div>


</div>


</div>
</div>
</div>
</div>
<!-- end main container -->
