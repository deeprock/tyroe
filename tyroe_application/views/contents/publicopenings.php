<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<div class="content" style="margin-left: 0px;">
   <div class="container-fluid">
        <div id="pad-wrapper">
            <img src="<?= ASSETS_PATH ?>img/logo-dark.png">
            <!-- END LEFT COLUMN -->

            <div class="grid-wrapper tyroes_content_result">
                <form action="" id="applyForm" method="post">
                    <input type="hidden" name="apply_job_id" value="<?=$get_jobs['job_id']?>">
                    <input type="hidden" name="apply_job_title" value="<?=$get_jobs['job_title']?>">
                    <button type="button" value="signup" class="btn-flat signupbtn primary pull-right margin-left-2">SignUp for apply with <b>Tyroe</b></button>
                    <button type="button" value="signin" class="btn-flat signinbtn primary pull-right">SignIn for apply with <b>Tyroe</b></button>
                    <input type="hidden" name="submitType" value="yes">
                </form>
                <div class="row-fluid">
                    <div class="span12" id="opening-overview">
                        <div class="right-column" style="position:relative;padding-left: 0;">
                            <div class="row-fluid header">
                                <div class="span12">
                                    <h4><?= $get_jobs['job_title'] ?></h4>
                                </div>
                            </div>
                            <div class="row-fluid">
                                <div class="span12 chart">
                                    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/custom.css">
                                    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/layout.css">
                                    <div class="span6 right_overview margin-left-default">
                                        <!--<h3 class="dt-1"><?php /*echo stripslashes($get_jobs['job_title']); */?></h3><br>-->
                                        <h5 class="dt-1 overview_location"><?php echo $get_jobs['city'] . "," . $get_jobs['job_location']; ?></h5><br>
                                        <p class="text-justify dt-2 jQueryClassReadmoreParagraphFunc" style="overflow: hidden"><span><?php echo stripslashes($get_jobs['job_description']); ?></span></p>
                                        <div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">
                                            <div class="span4 text-left st-1"><span class="opening-date"><i class="icon-calendar"></i> <?php echo date('d M Y', $get_jobs['start_date']); ?> </span></div>
                                            <div class="span4 text-center st-1"><span class="opening-month"><i class="icon-time"></i> <?php echo $datedifference ?></span></div>
                                            <div class="span4 text-right st-1"><span class="opening-time"><i class="icon-legal"></i> <?php echo $get_jobs['job_type']; ?></span></div>
                                        </div>
                                        <p class="dt-label pull-left"><?php foreach ($job_skills as $skill){?><span class="label"><?=$skill['creative_skill']?></span> <?php } ?></p>
                                    </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   </div>
</div>
<script>
    $(function(){
        $('.signupbtn').click(function(){
            $('#applyForm').attr('action','<?= $vObj->getUrl();?>register');
            $('#applyForm').submit();
        })
        $('.signinbtn').click(function(){
            $('#applyForm').attr('action','<?= $vObj->getUrl();?>login');
            $('#applyForm').submit();
        })
    })
</script>