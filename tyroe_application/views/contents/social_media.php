<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

?>
<!-- main container -->
<div class="content">

    <div class="container-fluid">
        <div id="pad-wrapper">
            <div class="grid-wrapper">
                <div class="row-fluid show-grid">
                    <!-- START LEFT COLUMN -->
                    <?php
                    $this->load->view('left_coloumn');

                    ?>
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar personal-info">
                        <div class="row-fluid form-wrapper">


                            <div class="span12">
                                <div class="container-fluid">


                                    <div class="span4 default-header">
                                        <h4><?=LABEL_SOCIAL_TITLE?></h4>
                                    </div>

                                    <div class="span12">
                                        <span class="">You can connect your tyroe account to your social media account(s) to easily keep your networks up to date with your activity in Tyroe.</span>

                                        <p>Once you link your profiles, Tyroe will automatically make a post to your
                                            social account whenever you:
                                        </p>

                                        <p>
                                        <ul class="content-box-data">
                                            <li> update your resume</li>
                                            <li> gain recommendations</li>
                                            <li> change your showreel</li>
                                            <li> add or edit your portfolio items
                                            </li>
                                        </ul>
                                        </p>
                                    </div>
                                    <div class="row-fluid header element main-experience">
                                        <div class="span12">
                                            <h4>Currently Linked Profiles</h4>
                                        </div>
                                        <div class="row-fluid experience_data">
                                            <div class="panel">
                                                <div class="row-fluid">
                                                    <div class="span12 people" id="top">
                                                        <div class="row-fluid">
                                                            <div class="span8">
                                                                <div class="span12 job-info">

                                                                    <?php if($in_user){ ?>
                                                                        <p>
                                                                            <img src="<?=ASSETS_PATH?>img/linkedin_logo.png" width="80px"/>
                                                                        <span class="span1" style="float: right" id="in">
                                                                            <a href="javascript:void(0);" onclick="media_unlink(<?=$in_user['user_id'];?>,'<?=$in_user['socialmedia_type'];?>')" style="cursor: default; text-decoration: none"> unlink</a>
                                                                        </span>
                                                                        </p>
                                                                    <?php } if($fb_user){
                                                                        $fb_user_data = unserialize($fb_user['user_data']);
                                                                        $fb_username =$fb_user_data[1]['name'];

                                                                        ?>
                                                                    <p>
                                                                            <?php  //echo $fb_username!=NULL?'<strong>'.$fb_username.'</strong>':""; ?>
                                                                        <img src="<?=ASSETS_PATH?>img/facebook-icon.png" width="80px"/>
                                                                        <span class="span1" style="float: right" id="fb">
                                                                            <a href="javascript:void(0);" onclick="media_unlink(<?=$fb_user['user_id'];?>,'<?=$fb_user['socialmedia_type'];?>')" style="cursor: default; text-decoration: none"> unlink</a>
                                                                        </span>
                                                                    </p>
                                                                    <?php } if($tw_user){ ?>
                                                                    <p>
                                                                        <img src="<?=ASSETS_PATH?>img/twitter-icon.png" width="80px"/>
                                                                        <span class="span1" style="float: right" id="tw">
                                                                            <a href="javascript:void(0);" onclick="media_unlink(<?=$tw_user['user_id'];?>,'<?=$tw_user['socialmedia_type'];?>')" style="cursor: default; text-decoration: none"> unlink</a>
                                                                        </span>
                                                                    </p>
                                                                    <?php }
                                                                    if($gp_user){ ?>
                                                                        <p>
                                                                            <img src="<?=ASSETS_PATH?>img/gplus-icon.png" width="80px"/>
                                                                            <?php

                                                                            $gplus_userdata = @json_decode($gp_user['user_data'],true);
                                                                            $google_profile_name = $gplus_userdata['displayName'];
                                                                            //echo $google_profile_name!=NULL?'<strong>'.$google_profile_name.'</strong>':"";

                                                                            ?>
                                                                        <span class="span1" style="float: right" id="tw">
                                                                            <a href="javascript:void(0);" onclick="media_unlink(<?=$gp_user['user_id'];?>,'<?=$gp_user['socialmedia_type'];?>')" style="cursor: default; text-decoration: none"> unlink</a>
                                                                        </span>
                                                                        </p>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <!--<div class="span4">
                                                                <a href="javascript:void(0);" style="cursor: default; text-decoration: none"> unlink</a>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="row-fluid header element main-experience">
                                        <div class="span12">
                                            <h4>What about?</h4>
                                        </div>
                                        <div class="row-fluid experience_data">
                                            <div class="panel">
                                                <div class="row-fluid">
                                                    <div class="span12 people" id="top">
                                                        <div class="row-fluid">
                                                            <div class="span8">
                                                                <div class="span12 job-info">
                                                                    <?php if(empty($fb_user)){ ?>

                                                                            <a href="javascript:void(0)" onclick="window.open('<?=$vObj->getURL("fbclient/index/$id");?>','facebook','width=400,height=400');">
                                                                                <img src="<?=ASSETS_PATH?>img/facebook-icon.png" width="80px"/>
                                                                            </a>


                                                                        <?php }
                                                                        if(empty($tw_user)){ ?>


                                                                        <a href="javascript:void(0)" onclick="window.open('<?=$vObj->getURL("twitterclient/index/$id");?>','twitter','width=400,height=400,scrollbars=auto');">
                                                                            <!--<a href="javascript:void(0)" onclick="">-->
                                                                            <img src="<?=ASSETS_PATH?>img/twitter-icon.png" width="80px"/>
                                                                        </a>

                                                                    <?php }
                                                                        if(empty($in_user)) {
                                                                        ?>
                                                                            <a href="javascript:void(0)" onclick="window.open('<?=$vObj->getURL("linkedinclient/linkedin/$id");?>','linkedin','width=400,height=400,scrollbars=auto');">
                                                                                <!--<a href="javascript:void(0)" onclick="">-->
                                                                                <img src="<?=ASSETS_PATH?>img/linkedin_logo.png" width="80px"/>
                                                                            </a>
                                                                        <?php
                                                                        }
                                                                        if(empty($gp_user) ) {
                                                                            ?>
                                                                            <a href="javascript:void(0)"  onclick="window.open(' <?=$vObj->getURL("googleclient");?>','googleplus','width=400,height=400,scrollbars=auto');">

                                                                                <!--<a href="javascript:void(0)" onclick="">-->
                                                                                <img src="<?=ASSETS_PATH?>img/gplus-icon.png" width="80px"/>
                                                                            </a>
                                                                        <?php
                                                                        }
                                                                        ?>

                                                                   </div>
                                                            </div>
                                                            <!--<div class="span4">
                                                                <a href="javascript:void(0);" style="cursor: default; text-decoration: none"> link</a>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->
                    <div>
                    </div>
                </div>
            </div>
        </div>
    </div>