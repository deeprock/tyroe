<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    function DeleteOpening(job_id) {

        var data = "job_id=" + job_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("job/DeleteOpening")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".job-opening" + job_id).hide();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });

    }
    function shortlist(job_id,user_id) {

        var data = "jid=" + job_id + "&user_id=" +user_id;

        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/shortlisting")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
</script>
<div class="content">

    <div class="container-fluid">

        <div id="pad-wrapper">

            <div class="grid-wrapper">

                <div class="row-fluid show-grid">

                    <!-- START LEFT COLUMN -->
                    <!--<div class="span4">

                        <div class="row-fluid header-side">
                            <div class="span8">
                                <h4>Filters</h4>
                            </div>
                            <div class="span4">
                                <a class="btn-flat gray">CLEAR</a>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="container span12">
                                    <form class="new_user_form inline-input">
                                        <div class="field-box">
                                            <div class="ui-select find">
                                                <select>
                                                    <option selected="">Status</option>
                                                    <option>Option 1</option>
                                                    <option>Option 2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="container span12">
                                    <form class="new_user_form inline-input">
                                        <div class="field-box">
                                            <div class="ui-select find">
                                                <select>
                                                    <option selected="">Department</option>
                                                    <option>Custom selects</option>
                                                    <option>Pure css styles</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="container span12">
                                    <form class="new_user_form inline-input">
                                        <div class="field-box">
                                            <div class="ui-select find">
                                                <select>
                                                    <option selected="">Project</option>
                                                    <option>Computer Animation</option>
                                                    <option>Fashion</option>
                                                    <option>Graphic Design</option>
                                                    <option>Illustration</option>
                                                    <option>Industrial Design</option>
                                                    <option>Interaction Design</option>
                                                    <option>Motion Graphics</option>
                                                    <option>Photography</option>
                                                    <option>UI/UX</option>
                                                    <option>Web Design</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        <div class="row-fluid">
                            <div class="span12">
                                <div class="container span12">
                                    <form class="new_user_form inline-input">
                                        <div class="field-box">
                                            <div class="ui-select find">
                                                <select>
                                                    <option selected="">Location</option>
                                                    <option>Computer Animation</option>

                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>-->
                    <!-- END LEFT COLUMN -->

                    <!-- START RIGHT COLUMN -->
                    <div class="span8 sidebar">
                        <div class="right-column">

                            <div class="row-fluid header">
                                <div class="span4">
                                    <h3 class="name">Job Openings</h3>
                                </div>
                            </div>
                            <!-- START OPENING DETAILS -->
                            <?php
                            if (is_array($job_detail)) {
                                foreach ($job_detail as $jobs) {
                                    ?>
                                    <div class="row-fluid job-opening<?php echo $jobs['job_id']; ?>">
                                        <div class="span12 group" id="top">

                                            <!-- TITLE -->
                                            <div class="row-fluid">
                                                <div class="span8">
                                                    <h4>
                                                        <a href="<?= $vObj->getURL("openingoverview/JobOverview/" . $jobs['job_id']) ?>"><?php echo $jobs['job_title']; ?></a>
                                                    </h4>
                                                </div>
                                                <div class="span4">
                                                    <ul class="actions pull-right">
                                                        <li><a class="btn-flat primary" href="javascript:void(0);"
                                                               onclick="shortlist('<?php echo $jobs['job_id']; ?>','<?php echo $candidate_id; ?>');">Shortlist for this Job</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>

                                            <!-- INFO -->
                                            <div class="row-fluid" id="opening-stats">
                                                <div class="span4">
                                                    <div class="row-fluid">
                                                        <div class="span12">
                                                            <p>
                                                                <strong>Location:</strong> <?php echo $jobs['job_location']; ?>
                                                            </p>

                                                            <p>
                                                                <strong>Date:</strong> <?php echo date('d M', $jobs['start_date']) . " - " . date('d M', $jobs['end_date']); ?>
                                                            </p>

                                                            <!--<p><strong>Level:</strong> <?php /*echo $jobs['job_skill']; */?>
                                                            </p>-->

                                                            <p>
                                                                <strong>Quantity:</strong> <?php echo $jobs['job_quantity']; ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php
                                }
                            }
                            ?>


                            <!-- END OPENING DETAILS -->

                        </div>


                    </div>
                    <!-- END RIGHT COLUMN -->

                    <div>


                    </div>


                </div>
            </div>
        </div>
    </div>
