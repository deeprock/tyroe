<table style="border:0px;width: 100%;"  cellspacing="0">
    <tr>
        <td valign="middle" colspan="2">
            <img src="<?= base_url() ?>assets/img/logo-dark.png"/>
        </td>
        <td style="text-align: right;">
            <div style="font-weight: bold;font-size: 30px;margin-bottom: 8px;">Invoice</div>
            <div style="color: rgb(132,130,131);;margin-bottom: 8px;"><?= $date ?></div>
            <div style="color: rgb(132,130,131);;margin-bottom: 15px;">ID: <?= $transaction_id ?></div>
        </td>
    </tr>
    <tr>
        <td style="border-top:1px solid #ccc;width: 33.33%;margin-top: 10px;">
            <div style="margin: 10px;border: 1px solid #E7E7E7;border-radius: 5px;margin-right: 20px;margin-left: 0px;height: 200px;margin-top: 20px;">
                <div style="background-color: rgb(245,245,245);padding: 10px;border-bottom: 1px solid #E7E7E7">
                    Invoice From:
                </div>
                <div style="padding: 10px;line-height: 22px;padding-bottom: 40px;">
                    <span style="font-weight: bold">Tyroe Pty Ltd</span><br/>
                    60 Victoria Avenue<br/>
                    Chatswood, NSW<br/>
                    Australia<br/>
                    2067
                </div>
            </div>
        </td>
        <td style="border-top:1px solid #ccc;width: 33.33%;margin-top: 10px;">
            <div style="margin: 10px;border: 1px solid #E7E7E7;border-radius: 5px;margin-right: 10px;margin-left: 5px;height: 200px;margin-top: 20px;">
                <div style="background-color: rgb(245,245,245);padding: 10px;border-bottom: 1px solid #E7E7E7">
                    Payment Information
                </div>
                <div style="padding: 10px;line-height: 22px;padding-bottom: 40px;">
                    <span style="font-weight: bold">Card Name:</span> <?= $card_type ?><br/>
                    <span style="font-weight: bold">Card Number:</span> **** <?= $cc_last_4 ?><br/>
                    <span style="font-weight: bold">Exp Date:</span> <?= $card_exp ?><br/>
                </div>
            </div>
        </td>
        <td style="border-top:1px solid #ccc;width: 33.33%;margin-top: 10px;">
            <div style="margin: 10px;border: 1px solid #E7E7E7;border-radius: 5px;margin-right: 0px;margin-left: 10px;height: 200px;margin-top: 20px;">
                <div style="background-color: rgb(245,245,245);padding: 10px;border-bottom: 1px solid #E7E7E7">
                    Billing To:
                </div>
                <div style="padding: 10px;line-height: 22px;padding-bottom: 40px;">
                    <span style="font-weight: bold"><?= $company_name ?></span><br/>
                    <?= $billing_address ?>
                </div>
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <div style="margin: 10px;border: 1px solid #E7E7E7;border-radius: 5px;margin-right: 0px;margin-left: 0px;margin-top: 20px;">
                <div style="background-color: rgb(245,245,245);padding: 1px;padding-left: 10px;border-bottom: 1px solid #E7E7E7">
                    <h3 style="font-size: 30px">Order Summary</h3>
                </div>
                <div style="padding: 10px;line-height: 22px;padding-bottom: 40px;padding-top:20px">
                    <table style="width: 100%;border:0px;" cellspacing="0" cellpadding="0">
                        <tr>
                            <td style="width: 25%">
                                <span style="font-weight: bold;text-align: left">&nbsp;&nbsp;&nbsp;Item name</span>
                            </td>
                            <td style="width: 25%">

                            </td>
                            <td style="width: 25%">

                            </td>
                            <td style="width: 25%;text-align: right">
                                <span style="font-weight: bold;">Total&nbsp;&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr style="color: #ccc;border:0px;border-top:1px solid #ccc;"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;&nbsp;<?= $transaction_description ?>
                            </td>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td style="width: 25%;text-align: right">
                                <span>$<?= number_format($orig_amt,2) ?>&nbsp;&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr style="color: black;border:0px;border-top:4px solid black;"/>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <span style="font-weight: bold;">Coupon Discount</span>
                            </td>
                            <td style="width: 25%;text-align: right">
                                <span>$<?= number_format($discount_amt,2) ?>&nbsp;&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <span style="font-weight: bold;">GST (10%)</span>
                            </td>
                            <td style="width: 25%;text-align: right">
                                <span>$<?= number_format($tax_amt,2) ?>&nbsp;&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td>

                            </td>
                            <td>
                                <span style="font-weight: bold;">Total</span>
                            </td>
                            <td style="width: 25%;text-align: right">
                                <span style="font-weight: bold;">$<?= number_format($amt,2) ?>&nbsp;&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </td>
    </tr>
</table>