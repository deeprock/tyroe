<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script type="text/javascript">
    $(document).on('click','.get_widgets_color',function(){
        var btntheme = $(this).val();
        var api_code  = '&lt;div id="widgets-btn"&gt;&lt;script type="text/javascript"&gt;var jobid  = "<?=$job_id?>";';
        api_code += 'var comapnyname ="<?=$this->session->userdata('firstname')." ".$this->session->userdata('lastname');?>";';
        api_code += 'var job_url= "<?= $vObj->getURL("openings"); ?>"; var btn_theme = "'+btntheme+'"  ;&lt;/script&gt;&lt;';
        api_code += 'script type="text/javascript" src="<?= ASSETS_PATH ?>js/api_tyroe.js"&gt;&lt;/script&gt;&lt;/div&gt;';
        $('#wideget_code_area').html(api_code);
    });
</script>
<!--<script src="<?php/*= ASSETS_PATH */?>js/social_share/jquery.share.js"></script>-->
<script type="text/javascript">
    /*$(function(){
        $('#social_widgets_share_icons').share({
            networks: ['facebook','twitter','linkedin','pinterest','googleplus','email'],
            theme: 'square',
            urlToShare: '<?= $vObj->getURL("openings"); ?>',
            title: '<?= $get_jobs['job_title']; ?>'
        });
    })*/
</script>
<script src="<?= ASSETS_PATH ?>js/fuelux.wizard.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        <?php if( $jumptoaction == 'candidate'){?>
         $('.get_candidate').trigger('click');
        <?php }else if($jumptoaction == 'applicant') {?>
        $('.get_applicants').trigger('click');
        <?php }else if($jumptoaction == 'shortlist') {?>
        $('.get_shortlist').trigger('click');
        <?php }else if($jumptoaction == 'declined'){?>
        $('#totrigger').val('decline');
        $('.get_candidate').trigger('click');
        //$("button[data-value='Decline']").trigger('click');
        <?php } else if($jumptoaction == 'review'){?>
        $('#totrigger').val('review');
        $('.get_candidate').trigger('click');
        <?php } ?>


        <?php if($this->input->post('applicaint_url_trigger') == 'yes'){ ?>
        $('.get_applicants').trigger('click');
        $('.loader-save-holder').show();
        <?php }else if($this->input->post('shortlist_url_trigger') == 'yes'){ ?>
        $('.loader-save-holder').show();
        $('.get_shortlist').trigger('click');
        <?php }else if($this->input->post('candidate_url_trigger') == 'yes'){ ?>
        $('.loader-save-holder').show();
        $('.get_candidate').trigger('click');
        <?php } ?>
        $('.loader-save-holder').hide();
        $("#startdate").datepicker({'showCheckbox':false});
        $("#enddate").datepicker({'showCheckbox':false});
        $('#editmodal').modal('hide');
        $('#team_member_modal').modal('hide');
        $('#widgets_and_sharing').modal('hide');

        $('#edit_job').click(function () {
            $('#editmodal').modal('show');
            rescale();
        });
        $('#btn_team_members').click(function () {
            $('#team_member_modal').modal('show');
            rescale();
        });
        $('#btn_widgets_and_sharing').click(function () {
            $('#widgets_and_sharing').modal('show');
            rescale();
        });
        $("#job_tags").select2({
            tags:[""],
            tokenSeparators: [","]
        });

        $('#btn-next').on('click',function(){
            job_title = $('#job_title').val();
            job_description = $('#job_description').val();
            job_country = $('#country').val();
            job_city = $('#city').val();
            job_startdate = $('#startdate').val();
            job_enddate = $('#enddate').val();
            job_tags = $('#job_tags').val();
            var date1 = new Date(job_startdate);
            var date2 = new Date(job_enddate);
            var timeDiff = dateDiff(date1,date2);
            var studio_name = $("#hidden_username").val();
            var job_country_name = $("#country option:selected").text();
            var job_city_name = $("#city option:selected").text();
            var job_type_name = $('.job_type').attr('data-id');
            var job_level_name = $('.job_level').attr("data-id");
            var date_my_diff = "";
            if(timeDiff.years > 0){
                date_my_diff = timeDiff.years+" Years(s) "+timeDiff.months+"  Month(s) "+timeDiff.days+" Day(s) ";
            } else {
                date_my_diff = timeDiff.months+"  Month(s) "+timeDiff.days+" Day(s) ";
            }
            var content = "<h3>"+studio_name+"</h3><br>"+
                "<h4>"+job_title+"</h4><br>"+
                "<h5>"+job_city_name+","+job_country_name+"</h5><br>"+
                "<span>"+job_description+"</span><br>"+
                "<span>"+job_startdate+"</span><br>"+
                "<span>"+date_my_diff+"</span><br>"+
                "<span>"+job_type_name+"</span><br>"+
                "<span>"+job_level_name+"</span><br>";
            "<span>"+job_tags+"</span>";
            $(".preview_job").html(content);
        });
        $(window).bind("resize", rescale);

        /* Creating emails tags */
        $("#invitation_emails").select2({
            tags:[""],
            tokenSeparators: [","]
        });
    });
    function rescale() {
        var size = {width: $(window).width(), height: $(window).height() }
        var offset = 20;
        var offsetBody = 150;
        //$('#myModal').css('height', size.height - offset );
        $('.modal-body').css('height', size.height - (offset + offsetBody));
        $('#myModal').css('top', '3%');
    }
    function postjob(){
        var error_empty = '<strong>Error!</strong> empty fields found, all fields are required';
        var error = 0;
        var team_moderation_val = $('.team_moderation').text();
        if(team_moderation_val == 'OFF'){
            team_moderation_val = 0;
        }else if(team_moderation_val == 'ON'){
            team_moderation_val = 1;
        }
        var data={
            'job_title' : $('#job_title_new').val(),
            'job_description' : $('#job_description').val(),
            'job_country' : $('#country').val(),
            'job_city' : $("#city option:selected").text(), //don't take id of city .. use string
            'job_startdate' : $('#startdate').val(),
            'job_enddate' : $('#enddate').val(),
            'job_type' : $('.job_type:checked').val(),
            'job_level' : $('.job_level:checked').val(),
            'job_tags' : $('#job_tags').val(),
            'job_industry' : $('#job_industry').val(),
            'edit_id' : $('#edit_id').val(),
            'team_moderation':team_moderation_val
        };
        if($.trim(data['job_title']) === "")
        {
            $('#job_title_new').css({ border: '1px solid red'});
            error++;
        }else{
            $('#job_title_new').css("border","");
        }
        if($.trim(data['job_description']) === "")
        {
            $('#job_description').css({ border: '1px solid red'});
            error++;
        }else{
            $('#job_description').css("border","");
        }
        if($.trim(data['job_industry']) === "")
        {
            $('.job_industry').css({ border: '1px solid red'});
            error++;
        }else{
            $('.job_industry').css("border","");
        }
        if($.trim(data['job_country']) === "")
        {
            $('.country').css({ border: '1px solid red'});
            error++;
        }else{
            $('.country').css("border","");
        }
        if($.trim(data['job_city']) === "")
        {
            $('.city').css({ border: '1px solid red'});
            error++;
        }else{
            $('.city').css("border","");
        }
        if($.trim(data['job_startdate']) === "")
        {
            $('#startdate').css({ border: '1px solid red'});
            error++;
        }else{
            $('#startdate').css("border","");
        }
        if($.trim(data['job_enddate']) === "")
        {
            $('#enddate').css({ border: '1px solid red'});
            error++;
        }else{
            $('#enddate').css("border","");
        }
        if($.trim(data['job_type']) === "")
        {
            $('.type-extra-padding').css({ border: '1px solid red'});
            error++;
        }else{
            $('.type-extra-padding').css("border","");
        }
        if($.trim(data['job_level']) === "")
        {
            $('.job-level').css({ border: '1px solid red'});
            error++;
        }else{
            $('.job-level').css("border","");
        }
        if($.trim(data['job_tags']) === "")
        {
            $('#s2id_job_tags').css({ border: '1px solid red'});
            error++;
        }else{
            $('#s2id_job_tags').css("border","");
        }
        if(error > 0)
        {
            $('.modal_error_box').show();
            $('.modal_error_box').html(error_empty);
        }
        else{
            $('#post_job').after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
            $.ajax({
                type:'POST',
                data:data,
                url:'<?=$vObj->getURL("jobopening/SaveOpening")?>',
                datatype : 'json',
                success: function(job_id){
                    $('.small_loader').remove();
                    /*var job_name = $('#job_title').val().toLowerCase();
                    job_name = job_name.replace(/ /gi,'-');
                    var redirect_url = "<?=$vObj->getURL("openings")?>/"+job_name+'-'+generate_number_id(eval(job_id));
                    window.location = redirect_url;*/
                    window.location.reload();
                }
            });
        }
    }
    function getcities(country_id){
        var data ="country_id=" + country_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("jobopening/getcities")?>',
            dataType: "json",
            success :function(response){
                $('#citybox').html(response.dropdown);
                $('#city').select2();
            }

        });
    }


    $(function () {
        var $wizard = $('#fuelux-wizard'),
            $btnPrev = $('.wizard-actions .btn-prev'),
            $btnNext = $('.wizard-actions .btn-next'),
            $btnFinish = $(".wizard-actions .btn-finish");
        $wizard.wizard().on('finished',function (e) {
            // wizard complete code
        }).on("changed", function (e) {
                var step = $wizard.wizard("selectedItem");
                // reset states
                $btnNext.removeAttr("disabled");
                $btnPrev.removeAttr("disabled");
                $btnNext.show();
                $btnFinish.hide();

                if (step.step === 1) {
                    $btnPrev.attr("disabled", "disabled");
                } else if (step.step === 3) {
                    $btnNext.hide();
                    $btnFinish.show();
                }
            });

        $btnPrev.on('click', function () {
            $wizard.wizard('previous');
        });
        $btnNext.on('click', function () {
            $wizard.wizard('next');
        });
    });
</script>
<script type="text/javascript">
$(document).ready(function () {
    $("#opening-overview").show();
    $("#review-team").hide();

    $(".comment-box").hide();

});


function dateDiff(dateFrom, dateTo) {
    var from = {
        d: dateFrom.getDate(),
        m: dateFrom.getMonth() + 1,
        y: dateFrom.getFullYear()
    };

    var to = {
        d: dateTo.getDate(),
        m: dateTo.getMonth() + 1,
        y: dateTo.getFullYear()
    };

    var daysFebruary = to.y % 4 != 0 || (to.y % 100 == 0 && to.y % 400 != 0)? 28 : 29;
    var daysInMonths = [0, 31, daysFebruary, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    if (to.d < from.d) {
        to.d   += daysInMonths[parseInt(to.m)];
        from.m += 1;
    }
    if (to.m < from.m) {
        to.m   += 12;
        from.y += 1;
    }

    return {
        days:   to.d - from.d,
        months: to.m - from.m,
        years:  to.y - from.y
    };
}

function jobOverview() {
    $("#opening-overview").show();
    $("#review-team").hide();
}
function hideComment() {
    $(".comment-box").hide();
    $(".activity-stream").show();
}
function commentbox() {
    $(".comment-box").show();
    $(".activity-stream").hide();
}
function InsertComment() {
    var comment = $("#comment_text").val();
    if (comment == "") {
        $("#comment_text").focus();
        $("#comment_text").css("border", '1px solid red');
    } else {
        var jid = $("#jid").val();

        var data = "comment=" + comment + "&jid=" + jid;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/SaveComment")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $("#comment_text").val("");
                    $(".comment-box").hide(200);
                    $(".activity-stream").show();
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
}
function setarchived() {
    var jid = $("#jid").val();
    var data = "jid=" + jid;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("openingoverview/SetArchive")?>',
        dataType: "json",
        success: function (data) {
            if (data.success) {
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(data.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                //$("#error_msg").html(data.success_message);
            }
        },
        failure: function (errMsg) {
        }
    });
}
//existing reviewer searching
$(document).on('keyup','#search_existing_reviewer',function(a){
    var job_id = $('#edit_id').val();
    var reviewers =  $('input[name="reviewer_ids[]"]').map(function(){
        return this.value
    }).get();
    var searching_value = $("#search_existing_reviewer").val()
    $.ajax({
        type: 'POST',
        data: 'searching_value='+searching_value+'&reviewers='+reviewers+'&job_id='+job_id+'&search_type=existing_job',
        url: "<?= $vObj->getURL("jobopening/search_existing_reviewer"); ?>",
        success : function(html){
            if($('#search_existing_reviewer').val() == ''){
                $('.searching_reviewer_result').html('');
            }else{
                $('.searching_reviewer_result').html(html);
            }

        }
    });

});
$(document).on('click','.select_reviewer',function(){
    var user_value = $(this).data('value');
    var job_id = $('#edit_id').val();
    $('.display_list'+user_value).remove();
    $.ajax({
        type: 'POST',
        data: 'reviewer_id='+user_value+'&job_id='+job_id,
        url: "<?= $vObj->getURL("jobopening/add_reviewers_from_job"); ?>",
        success : function(html){
            var previous_html = $('#reviewer_container').html();
                previous_html += '<li class="parent_row' + user_value + '">';
                previous_html += $('.list_parent_row' + user_value).html();
                previous_html += '</li>';
                $('#reviewer_container').html(previous_html);
                $('.list_parent_row' + user_value).remove();
        }
    });
});
$(document).on('click','#send_invitaion',function(){
    var invitation_emails = $("#invitation_emails").val();
    data = 'sending_action=invitaion&emails='+invitation_emails;
    $.ajax({
        type: 'POST',
        data: data,
        url: '<?=$vObj->getURL("search/send_email");?>',
        success: function (result) {
            if(result){
                /* Creating emails tags */
                $(".select2-input").val('');
                alert('invitaion send successfully');
            }
        }
    });
});
//activity searching funhctions
$(document).on("click",'.search_by', function(){
    if ($(this).attr("data-value") == "2") {
        $("#stream_duration").val("week");
    }
    else if ($(this).attr("data-value") == "3") {
        $("#stream_duration").val("month");
    }
    else if ($(this).attr("data-value") == "1") {
        $("#stream_duration").val("all");
    }
    FilterStreams();
});

$(document).on("change",'select',function(){
    var str = "";
    $("select option:selected").each(function () {
        str += $(this).text() + " ";
    });
    if ($(this).val() != "") {
        $(".activity_type_label").text(str);
    }
});
//.change();

/*Clear button */
$("#clear_btn").click(function () {
    $(this).closest('form').find("input[type=text], textarea,select").val("");
    FilterStreams();
});

function FilterStreams(){
    $('.loader-save-holder').show();
    var activity_type = $("#activity_type").val();
    var stream_keyword = $("#stream_keyword").val();
    var stream_duration = $("#stream_duration").val();
    var data = "activity_type=" + activity_type + "&stream_keyword=" + stream_keyword + "&stream_duration=" + stream_duration;
    $.ajax({
        type: "POST",
        data: data,
        url: '<?=$vObj->getURL("stream/FilterStream")?>',
        success: function (data) {
            $('.loader-save-holder').hide();
            $(".no-filter").hide();
            $(".filter-result").html(data);
        }

    });
}

$(document).on('click','.add_to_archive',function(){
    var strconfirm = confirm("Are you sure you want to add this job to archive?");
    if(strconfirm){
        var job_id = $(this).data('value');
        $.ajax({
            type: 'POST',
            data: 'action=add_to_archive&job_id='+job_id,
            url: '<?=$vObj->getURL("openingoverview/applicaint_to_candidate");?>',
            success: function (response) {
                $(".notification-box-message").html('success');
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                    window.location = '<?=$vObj->getURL("openings");?>'
                }, 2000);
            }
        });
    }
})
//anonymous feedback actions functions
var total_feedback = <?= count($get_anonymous); ?>;
$(document).on('click','.approve_anonymous_feedbeck',function(){
    $('.loader-save-holder').show();
    total_feedback--;
    var review_id = $(this).data('value');
    var rev_tyroe_id  = $(this).data('id');
    $.ajax({
        type: 'POST',
        data: 'review_id='+review_id+'&action=approve_feedback&tyroe_id='+rev_tyroe_id,
        url: '<?=$vObj->getURL("openingoverview/anonymous_feedback_action");?>',
        success: function (response) {
            $(".notification-box-message").html('success');
            $(".notification-box").show(100);
            setTimeout(function () {
                if(total_feedback == 0){
                    $('.feedback_main_content').html('<p>Feedback not found</p>');
                }
                $(".feedback_row"+review_id).fadeOut();
            }, 1000);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 3000);
            $('.loader-save-holder').hide();
        }
    });
});
$(document).on('click','.delete_anonymous_feedbeck',function(){
    total_feedback--;
    var review_id = $(this).data('value');
    $.ajax({
        type: 'POST',
        data: 'review_id='+review_id+'&action=delete_feedback',
        url: '<?=$vObj->getURL("openingoverview/anonymous_feedback_action");?>',
        success: function (response) {
            $(".notification-box-message").html('success');
            $(".notification-box").show(100);
            setTimeout(function () {
                if(total_feedback == 0){
                    $('.feedback_main_content').html('<p>Feedback not found</p>');
                }
                $(".feedback_row"+review_id).fadeOut();
            }, 1000);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 3000);
        }
    });
});
//end anonymous feedback actions functions

//applicant area filter function
$(document).on('click','.decline_users',function(){
    var decline_users = $(this).data('value');
    $(this).addClass('active_fillters');
    $('.loader-save-holder').show();
    $.ajax({
        type : 'POST',
        data: 'decline_users='+decline_users,
        url: '<?=$vObj->getURL("openingoverview/applicants/".$job_id)?>',
        success: function(response){
            $('.loader-save-holder').hide();
            $('.tyroes_content_result').html(response);
            $('.tyroes_content_result').hide();
            $('.tyroes_content_result').fadeIn();
            /*$('.active_fillters').trigger('click');*/
            if(check_view_type == 'profile_view'){
                $('.filter_editor #editors_pick_info .name:eq(0)').trigger('click');
            }
        }
    });
});
//end applicant area filter function
//open tyroe reviewers reviews
var feed_up_down = 1;
$(document).on('click','.reviews_toggle',function(){

        if(feed_up_down == 1){
            $(this).removeClass('ar-down')
            $(this).addClass('ar-up');
            feed_up_down = 2;
        }
        else if(feed_up_down == 2){
            $(this).removeClass('ar-up')
            $(this).addClass('ar-down');
            feed_up_down = 1;
        }
    var job_detail_id = $(this).data('id');
    var tyroe_id = $(this).data('value');
    var job_id  = '<?php echo $job_id; ?>';
    var data = 'tyroe_id='+tyroe_id+'&job_id='+job_id;

    $.ajax({
        type: 'POST',
        data : data,
        url: '<?=$vObj->getURL("openingoverview/getreviewers")?>',
        success : function(html){
            $('.reviews_box'+job_detail_id).html(html);
            $('.reviews_box'+job_detail_id).slideToggle();
        }
    });
});
//End open tyroe reviewers reviews

//End search fillter advance
//selecting single user
var click_id = 0;
var search_list_inner_html = '';
var page_position = 0;
var check_view_type = 'list_view';
function getsingle_user(page_number, search_user_id){
    $('.fillters_controllers .container').addClass('rt-pagination-sec');
    check_view_type = 'profile_view';
    var page_position_text = $('#user_page_postion').val();
    var active_pages_tab   = $('.active_fillters').data('value');
    $('#Search_filters').show();
    $('.candidate_no_image_section').hide();
    <?php if($this->session->userdata('role_id')==4){ ?>
    $('.singleprofile_controllers').removeClass('hidden');
    <?php } ?>
    page_position = page_number;
    $('.loader-save-holder').show();
    $('.pagination_controllers').removeClass('hidden');
    if(click_id == 0){
        search_list_inner_html = $('.filter_editor').html();
        click_id = 1;
    }
    //this code for profile view review
    var data_user = 'current_userid='+search_user_id;
    $.ajax({
        type: 'POST',
        data: data_user,
        url: "<?= $vObj->getURL("searchpublicprofile/index"); ?>",
        success : function(html){
            $('.filter_editor').html(html);
            $('.single_num_of_records').text(page_position+' of '+total_person);
            var ifrm = document.getElementById('profile_iframe');
            var html_content = document.getElementById('profile_content').innerHTML;
            //var stylehtml  = '<script src="<?= ASSETS_PATH ?>js/latest-jquery-min.js"><\/script>';
            var stylehtml = '<link href="<?= ASSETS_PATH ?>profile/anibus/bootstrap.min.css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/settings.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/main.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/jquery.fancybox.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/fonts.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/shortcodes.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/responsive.css" type="text/css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/custom.css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/chechkbox.css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/viewport.css" rel="stylesheet"/><link href="<?= ASSETS_PATH ?>profile/anibus/js/modernizr.js" rel="stylesheet"/>';
            ifrm = (ifrm.contentWindow) ? ifrm.contentWindow : (ifrm.contentDocument.document) ? ifrm.contentDocument.document : ifrm.contentDocument;
            ifrm.document.open();
            ifrm.document.write(stylehtml+html_content);
            ifrm.document.close();
            setTimeout(function () {
                $('.loader-save-holder').hide();
                $('#profile_iframe').show();
                $('.filter_editor').show();
                var profile_height = $('#profile_iframe').contents().find('html').height();
                $('#profile_iframe').height(profile_height+50+'px');
                $('#profile_iframe').css({"border":"0"});
                $('#profile_iframe').contents().find('.btn_mail_n_remove').remove();
            }, 3000);

            <?php if($this->session->userdata("role_id") == 4){ ?>
            if(page_position_text == 'candidate' && active_pages_tab == 'Pending'){
                var controllers_html  =  '<div class="row-fluid stats-row rt-rev-btn-sec">';
                controllers_html +=  '<div class="container-fluid">';
                controllers_html +=  '<div class="pull-right">';
                controllers_html +=  '<a class="btn-flat success review_tyroe" data-value="" data-id="'+search_user_id+'">Review</a> ';
                controllers_html +=  '<a class="btn-flat inverse decline_tyroe" data-id="'+search_user_id+'">Decline</a>';
                controllers_html +=  '</div>';
                controllers_html +=  '</div>';
                controllers_html +=  '</div>';
                $('.singleprofile_controllers').html(controllers_html);
            }else if(page_position_text == 'candidate' && active_pages_tab == 'Review'){
                $.ajax({
                    type: 'POST',
                    data: 'tyroe_id='+search_user_id+'&job_id=<?php echo $job_id; ?>&action=review_by_reviewer',
                    url: "<?= $vObj->getURL("openingoverview/getreviewers"); ?>",
                    success : function(response){
                        $('.singleprofile_controllers').html(response);
                    }
                });
            }
            <?php } ?>
            var active_fillter = $('.active_fillters').data('value');
            if(active_fillter == 'Decline'){
                $('.singleprofile_controllers').addClass('hidden');
            }
        }
    });

}
//creating single user pagination
$(document).on("click",".listview_btn",function(){
    $('.fillters_controllers .container').removeClass('rt-pagination-sec');
    if(check_view_type == 'profile_view'){
        $('.filter_editor').show();
    }
    check_view_type = 'list_view';
    $('.singleprofile_controllers').addClass('hidden');
    $('.loader-save-holder').show();
    $('.filter_editor').html('');
    $('.filter_editor').html(search_list_inner_html);
    <?php if($this->session->userdata("role_id") == 4){ ?>
    var fillter_action = $('.active_fillters').data('value');
    if(fillter_action == 'Review'){
        $('.review_tyroe').hide();
        $('.decline_tyroe').hide();
        $('.view_reviews').show();
    }
    <?php } ?>
    $('.pagination_controllers').addClass('hidden');
    setTimeout(function () {
        $('.loader-save-holder').hide();
    }, 3000);
    click_id = 0;
});
$(document).on('click','.single_next',function(){
    if(page_position == total_person){
        $('.single_next').attr("disable",true);
    }else{
        page_position++;
        page_found = $('#user_unique_id'+page_position).attr('data-id');
        page_position = page_found;
        page_user_id =  $('#user_unique_id'+page_position).val();
        getsingle_user(page_position,page_user_id);
    }

});
$(document).on('click','.single_back',function(){
    if(page_position > 1){
        page_position--;
        page_found = $('#user_unique_id'+page_position).attr('data-id');
        page_position = page_found;
        page_user_id =  $('#user_unique_id'+page_position).val();
        getsingle_user(page_position,page_user_id);
    }
});
//End creating single user pagination
//End selecting single user
//candidate fillters function
$(document).on('click','.fillter_action',function(){
    $('#pad-wrapper').css({"padding":"0"});
    $('select[name="fillter_drop"]')[0].selectedIndex = 0;
    $('#Search_filters').hide();
    var fillter_action = $(this).data('value');
    $('.fillter_action').removeClass('active_fillters');
    $(this).addClass('active_fillters');
    $('.loader-save-holder').show();
    $('.listview_btn').removeClass('active_fillters');
    $.ajax({
        type : 'POST',
        data: 'fillter_action='+fillter_action,
        url: '<?= $vObj->getURL("openingoverview/Candidates/" . $job_id) ?>',
        success: function(response){

            $('.loader-save-holder').hide();
            $('.tyroes_content_result').html(response);
            search_list_inner_html = response;
            $('.tyroes_content_result').hide();
            $('.tyroes_content_result').fadeIn();
            $('.singleprofile_controllers').addClass('hidden');
            <?php if($this->session->userdata("role_id") == 4){ ?>
            if(fillter_action == 'Review'){
                $('.review_tyroe').hide();
                $('.decline_tyroe').hide();
                $('.view_reviews').show();
            }
            <?php } ?>
            if(fillter_action == 'Review'){
                if($('.candidate_area_filters select[name="fillter_drop"] option').size() < 4){
                   $('.candidate_area_filters select[name="fillter_drop"]').append('<option value="highiest_score">Highest Score</option>');
                    if($('#totrigger').val()=='admin_see_review'){
                        $(".reviews_toggle").trigger('click');
                        $('#totrigger').val('');
                    }
                }
            }else{
                $('.candidate_area_filters select[name="fillter_drop"] option:eq(3)').remove();
            }
            if(fillter_action == 'Decline'){
                $('.review_tyroe').hide();
                $('.decline_tyroe').hide();
                $('.undo_decline_tyroe').show();
                $('.group_icons').hide();
                $('.under-avatar-status').hide();
                $('.add_to_shortlist_controls').hide();
            }
            if(check_view_type == 'profile_view'){
                //$('.filter_editor').hide();
            }
            if(check_view_type == 'profile_view'){
                $('.filter_editor #editors_pick_info .name').eq(0).trigger('click');

            }
        }
    });

});
//undo decline users
$(document).on('click','.undo_decline_tyroe',function(){
    var data_type = $(this).data('type');
    var tyroe_id  = $(this).data('id');
    if(data_type == 'applicaint_undo'){
        var job_detail_id = $(this).data('value');
        var reveiwer_reviews_id = 0;
    }else if(data_type == 'candidate_undo'){
        var ids_data = $(this).data('value');
        ids_data = ids_data.split(',');
        var reveiwer_reviews_id = ids_data[0];
        var job_detail_id = ids_data[1];
    }
    $.ajax({
        type: 'POST',
        data: 'action=undo_decline&reveiwer_reviews_id='+reveiwer_reviews_id+'&job_detail_id='+job_detail_id,
        url: '<?=$vObj->getURL("openingoverview/applicaint_to_candidate");?>',
        success: function (response) {
            $('.hiderow'+job_detail_id).remove();
            $(".notification-box-message").html('Success');
            $(".notification-box").show(100);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 5000);
            $('.active_fillters').trigger('click');
        }
    });
});


//end candidate filter function
//dropdown fillter function for candidate,applicaint,shortlist
$(document).on('change','select[name="fillter_drop"]',function(){
    if(check_view_type == 'profile_view'){
        $('.filter_editor').hide();
    }
    $('.loader-save-holder').show();
    var user_page_position = $('#user_page_postion').val();
    var fillter_action = $('.active_fillters').data('value');
    if(user_page_position == 'applicaint'){
        var ajax_url = '<?= $vObj->getURL("openingoverview/applicants/" . $job_id) ?>';
    }else if(user_page_position == 'candidate'){
        var ajax_url = '<?= $vObj->getURL("openingoverview/Candidates/" . $job_id) ?>';
    }else if(user_page_position == 'shortlist'){
        var ajax_url = '<?= $vObj->getURL("openingoverview/Shortlisted/" . $job_id) ?>';
    }
    if(typeof fillter_action == 'undefined'){
        var data = 'fillter_drop='+$(this).val();
    }else{
        var data = 'fillter_drop='+$(this).val()+'&fillter_action='+fillter_action;
    }
    $.ajax({
        type: 'post',
        data: data,
        url: ajax_url,
        success: function(response){
            $('.tyroes_content_result').html(response);
            search_list_inner_html = response;
            setTimeout(function () {
                $('.loader-save-holder').hide();
            }, 3000);
            <?php if($this->session->userdata("role_id") == 4){ ?>
            if(fillter_action == 'Review'){
                $('.review_tyroe').hide();
                $('.decline_tyroe').hide();
                $('.view_reviews').show();
            }
            <?php } ?>
            if(fillter_action == 'Decline'){
                $('.review_tyroe').hide();
                $('.decline_tyroe').hide();
                $('.undo_decline_tyroe').show();
                $('.group_icons').hide();
                $('.under-avatar-status').hide();
                $('.add_to_shortlist_controls').hide();
            }
            if(check_view_type == 'profile_view'){
                $('.filter_editor #editors_pick_info .name:eq(0)').trigger('click');
            }
        }
    });
});
//end dropdown filter function for candidate, applicaint, shortlist


<?php if($this->session->userdata("role_id") == 4){ ?>
//declear variables for reviewers review form
var  technical_value    = 50;
var  creative_value     = 50;
var  impression_value   = 50;
var  tyroe_id           = 0;


//view review
var count_up_down = 1;
$(document).on('click','.view_reviews',function(){
    if(count_up_down == 1){
        $(this).removeClass('ed-down')
        $(this).addClass('ed-up');
        count_up_down = 2;
    }
    else if(count_up_down == 2){
        $(this).removeClass('ed-up')
        $(this).addClass('ed-down');
        count_up_down = 1;
    }

    var skillsvalues = $(this).data('value');
    var tyroe_id     = $(this).data('id');
    skillsvalues     = skillsvalues.split(",");
    $('#reviews_edit_section'+tyroe_id).slideToggle();
    $('.slider-technical').slider({
        value:skillsvalues[0]
    })
    $('.slider-creative').slider({
        value:skillsvalues[1]
    })
    $('.slider-impression').slider({
        value:skillsvalues[2]
    })
});
//decline tyroe for review
$(document).on('click','.decline_tyroe',function(){
    tyroe_id                = $(this).data('id');
    var job_id              = '<?= $job_id; ?>';
    $.ajax({
        type:'post',
        data:'reviewed_action=decline_cadidate&tyroe_id='+tyroe_id+'&job_id='+job_id,
        url: '<?=$vObj->getURL("openingoverview/add_reviewer_review")?>',
        success: function(result){
            $('#myModal_give_review').modal('hide');
            $(".notification-box-message").html('Successfully decline');
            $('.active_fillters').trigger('click');
            $('.pagination_controllers').addClass('hidden');
            $('.singleprofile_controllers').addClass('hidden');
            $(".notification-box").show(100);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 5000);
            $('.single_person_pagination .candidate_id'+tyroe_id).remove();
            var total_users_in_candidate = $('.single_person_pagination .users_ids').size();
            for(a=0;a<total_users_in_candidate; a++){
                $('.single_person_pagination .users_ids:eq('+a+')').attr('data-id',a+1);
                $('.single_person_pagination .users_ids:eq('+a+')').attr('id','user_unique_id'+(a+1));
            }
            total_person--;
        }
    });
});
//delete tyroe_review
$(document).on('click','.delete_tyroe_review',function(){
    var ids_arra = $(this).data('id');
    ids_arra = ids_arra.split(',');
    var rate_review_id = ids_arra[0];
    var rate_tyroe_id  = ids_arra[1];
    var job_id = '<?= $job_id; ?>';
    var data_type = $(this).data('type');
    $.ajax({
        type:'post',
        data:'reviewed_action=delete_tyroe_review&tyroe_id='+rate_tyroe_id+'&job_id='+job_id+'&review_id='+rate_review_id,
        url: '<?=$vObj->getURL("openingoverview/add_reviewer_review")?>',
        success: function(result){
            $(".notification-box-message").html('Successfully delete review');
            $(".notification-box").show(100);
            setTimeout(function () {
                $(".notification-box").hide();
            }, 5000);
            if(data_type == 'profile_view'){
                $('.single_person_pagination #user_unique_id'+page_position).remove();
                $('.singleprofile_controllers').addClass('hidden');
                $('.listview_btn').addClass('listbtn_handler');
                if(page_position == '1'){
                    $('.listbtn_handler').trigger('click');
                }else{
                    $('.single_back').trigger('click');
                    var total_hidden = $('.single_person_pagination .users_ids').size();
                    var new_hidden_fields = '';
                    for(var a=0; a<total_hidden; a++){
                        var new_tyroe_id  = $('.single_person_pagination .users_ids:eq('+a+')').val();
                        new_hidden_fields += '<input type="hidden" id="user_unique_id'+(a+1)+'" class="users_ids" data-id="'+(a+1)+'" value="'+new_tyroe_id+'">'
                    }
                    $('.single_person_pagination').html(new_hidden_fields);
                }
                total_person--;
                $('.single_num_of_records').text(page_position+' of '+total_person);
            }else{
                $('.row-id'+rate_tyroe_id).fadeOut();
                $('.active_fillters').trigger('click');
            }
        }
    });
})
//reviewers functions
$(document).on('click','.review_tyroe',function(){

    $('#myModal_give_review').modal('show');
    tyroe_id = $(this).data('id');
    var tyroe_name = $(this).data('value');
    if(tyroe_name == ''){
        tyroe_name = $('.tyroe_name'+tyroe_id).text();
    }
    $('#myModal_give_review .modal-title').text('Review Form - '+tyroe_name);
});
$(document).on('click','.review_save',function(){
    var tyroe_comment       = $('.tyroe_comment').val();
    if(tyroe_comment == ''){
        $('.tyroe_comment').css({"border":"1px solid red"})
        $('.tyroe_comment').before('<span class="error_span" style="color:red">Comment is required.</span>')
    }else{
        $('.tyroe_comment').css({"border":"1px solid #ccc"})
        $('.error_span').remove();
        $(this).hide();
        $('.review_close').hide();
        $('.review_back').show();
        $('.review_submit').show();
        $('.modal-step1').hide();
        $('.modal-step2').show();
    }


});
$(document).on('click','.review_back',function(){
    $(this).hide();
    $('.review_submit').hide();
    $('.review_close').show();
    $('.review_save').show();
    $('.modal-step1').show();
    $('.modal-step2').hide();
})
$(document).on('click','.review_submit',function(){
    $(this).after('<img src="<?= ASSETS_PATH ?>img/small_loader.GIF" class="small_loader" />');
    var this_class          = 'review_submit';
    var tyroe_level         = $('input[name="tyroe_level"]:checked').val();
    var tyroe_comment       = $('.tyroe_comment').val();
    var action_shortlist    = $('input[name="action_shortlist"]:checked').val();
    var action_interview    = $('input[name="action_interview"]:checked').val();
    var anonymous_message   = $('.anonymous_message').val();
    $(this).removeClass('review_submit');
    var job_id              = '<?= $job_id; ?>';
    if(typeof action_shortlist == 'undefined'){
        action_shortlist = '';
    }
    if(typeof action_interview == 'undefined'){
        action_interview = '';
    }
    var reviewer_data = 'tyroe_level='+tyroe_level+'&tyroe_comment='+tyroe_comment+'&action_shortlist='+action_shortlist+'&action_interview='+action_interview+'&anonymous_message='+anonymous_message+'&tyroe_id='+tyroe_id+'&technical_value='+technical_value+'&creative_value='+creative_value+'&impression_value='+impression_value+'&job_id='+job_id;
    $.ajax({
        type:'post',
        data:reviewer_data,
        url: '<?=$vObj->getURL("openingoverview/add_reviewer_review")?>',
        dataType: "json",
        success: function(result){
            $('#myModal_give_review').modal('hide');
            if (result.success) {
                $(".notification-box-message").html(result.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
                $('.pagination_controllers').addClass('hidden');
                $('.singleprofile_controllers').addClass('hidden');
                $('.active_fillters').trigger('click');
            }
            else {
                $(".notification-box-message").css("color", "#b81900")
                $(".notification-box-message").html(result.success_message);
                $(".notification-box").show(100);
                setTimeout(function () {
                    $(".notification-box").hide();
                }, 5000);
            }
            $(this).addClass(this_class);
            $(this).after('.small_loader').remove();
        }
    });

});
//edit reviews
var review_id               = 0;
var tyroe_name              = '';
var edit_type               = '';
$(document).on('click','.edit_reviews',function(){
    review_id                   = $(this).data('id');
    var review_data             = $(this).data('value');
    edit_type                   = $(this).data('type');
    review_data                 = review_data.split(",");
    tyroe_id                    = review_data[0];
    tyroe_name                  = review_data[1];
    var tyroe_level             = review_data[2];
    technical_value             = review_data[3];
    creative_value              = review_data[4];
    impression_value            = review_data[5];
    var tyroe_comment           = review_data[6];
    var action_shortlist        = review_data[7];
    var action_interview        = review_data[8];
    $('input[name="tyroe_level"]').each(function() {
        if($(this).val() == tyroe_level){
            $(this).attr("checked", "checked");
        }
    });
    $('.slider-technical').slider({
        value:technical_value
    })
    $('.slider-creative').slider({
        value:creative_value
    })
    $('.slider-impression').slider({
        value:impression_value
    })
    $('.review_score_run_time').text(Math.round((technical_value+creative_value+impression_value)/3));
    $('.tyroe_comment').val(tyroe_comment);
    if($('input[name=action_shortlist]').val() == action_shortlist){
        $('input[name=action_shortlist]').attr("checked", "checked");
    }
    if($('input[name=action_interview]').val() == action_interview){
        $('input[name=action_interview]').attr("checked", "checked");
    }
    $('.review_update').show();
    $('.review_save').hide();
    $('#myModal_give_review').modal('show');
    $('#myModal_give_review .modal-title').text('Review Form - '+tyroe_name);
});
//update tyroe reivew
$(document).on('click','.review_update',function(){
    $('.loader-save-holder').show();
    var tyroe_level         = $('input[name="tyroe_level"]:checked').val();
    var level_title         = $('input[name="tyroe_level"]:checked').data('id');
    var tyroe_comment       = $('.tyroe_comment').val();
    var action_shortlist    = $('input[name="action_shortlist"]:checked').val();
    var action_interview    = $('input[name="action_interview"]:checked').val();
    var anonymous_message   = $('.anonymous_message').val();
    var job_id              = '<?= $job_id; ?>';
    if(tyroe_comment == ''){
        $('.tyroe_comment').css({"border":"1px solid red"})
        $('.tyroe_comment').before('<span class="error_span" style="color:red">Comment is required.</span>');
    }else{
        $('.tyroe_comment').css({"border":"1px solid #ccc"})
        $('.error_span').remove();
        if(typeof action_shortlist == 'undefined'){
            action_shortlist = '';
        }
        if(typeof action_interview == 'undefined'){
            action_interview = '';
        }
        var reviewer_data = 'tyroe_level='+tyroe_level+'&tyroe_comment='+tyroe_comment+'&action_shortlist='+action_shortlist+'&action_interview='+action_interview+'&anonymous_message='+anonymous_message+'&tyroe_id='+tyroe_id+'&technical_value='+technical_value+'&creative_value='+creative_value+'&impression_value='+impression_value+'&job_id='+job_id+'&review_id='+review_id+'&reviewed=reviewed';
        $.ajax({
            type:'post',
            data:reviewer_data,
            url: '<?=$vObj->getURL("openingoverview/add_reviewer_review")?>',
            dataType: "json",
            success: function(result){
                $('#myModal_give_review').modal('hide');
                if (result.success) {
                    $(".notification-box-message").html(result.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    if(edit_type == 'single_page_edit'){
                        var reviewhtml  = '<div class="row-fluid stats-row rt-bg-review">';
                        reviewhtml += '<div class="container rt-pd-container">';
                        reviewhtml += '<div class="span1 rt-center margin-left-default">';
                        reviewhtml += '<ul class="actions margin-default">';
                        reviewhtml += '<li><a href="javascript:void(0)" data-type="single_page_edit" class="edit_reviews" data-id="'+review_id+'" data-value="'+tyroe_id+','+tyroe_name+','+tyroe_level+','+technical_value+','+creative_value+','+impression_value+','+tyroe_comment+','+action_shortlist+','+action_interview+'"><i class="table-edit"></i></a></li>';
                        reviewhtml += '<li class="last"><a href="javascript:void(0)" data-type="profile_view" class="delete_tyroe_review" data-id="'+review_id+','+tyroe_id+'"><i class="table-delete"></i></a></li>';
                        reviewhtml += '</ul>';
                        reviewhtml += '</div>';
                        reviewhtml += '<div class="span10">';
                        reviewhtml += '<div class="span3 rt-center rt-st-sec margin-left-default">';
                        if(action_shortlist){
                            reviewhtml += '<span class="label inverse">'+action_shortlist+'</span>';
                        }
                        if(action_interview){
                            reviewhtml += '<span class="label inverse">'+action_interview+'</span>';
                        }
                        reviewhtml += '</div>';
                        reviewhtml += '<div class="span6 rt-text-center">';
                        reviewhtml += '<p><strong>'+level_title+': </strong><span>'+tyroe_comment+'</span></p>';
                        reviewhtml += '</div>';
                        var total_score =  Math.round((technical_value+creative_value+impression_value)/3);
                        reviewhtml += '<div class="span3 rt-center rt-score text-center"><span>'+total_score+'</span></div>';
                        reviewhtml += '<div class="clearfix"></div>';
                        reviewhtml += '</div>';
                        reviewhtml += '</div>';
                        reviewhtml += '</div>';
                        $('.singleprofile_controllers').html(reviewhtml);
                        $('.singleprofile_controllers').removeClass('hidden');
                        $('.listview_btn').addClass('listbtn_handler');
                    }else{
                        var reviewhtml = '<div class="span2">';
                        reviewhtml    += '<ul class="actions edit-ul-sec">';
                        reviewhtml    += '<li><a href="javascript:void(0)" class="edit_reviews" data-id="'+review_id+'" data-value="'+tyroe_id+','+tyroe_name+','+tyroe_level+','+technical_value+','+creative_value+','+impression_value+','+tyroe_comment+','+action_shortlist+','+action_interview+'"><i class="table-edit"></i></a></li>';
                        reviewhtml    += '<li class="last"><a href="javascript:void(0)" class="delete_tyroe_review" data-id="'+review_id+','+tyroe_id+'" ><i class="table-delete"></i></a></li>';
                        reviewhtml    += '</ul>';
                        reviewhtml    += '</div>';
                        reviewhtml    += '<div class="span10">';
                        reviewhtml    += '<div class="span4 cn-label">';
                        if(action_shortlist){
                            reviewhtml    += '<span class="label">'+action_shortlist+'</span>';
                        }
                        if(action_interview){
                            reviewhtml    += '<span class="label">'+action_interview+'</span>';
                        }
                        reviewhtml    += '</div>';
                        $('.review_range_sliders'+tyroe_id+' .slider-technical').slider({
                            value:technical_value
                        })
                        $('.review_range_sliders'+tyroe_id+'  .slider-creative').slider({
                            value:creative_value
                        })
                        $('.review_range_sliders'+tyroe_id+'  .slider-impression').slider({
                            value:impression_value
                        })
                        var total_score =  Math.round((technical_value+creative_value+impression_value)/3);
                        reviewhtml    += '<div class="span5">';
                        reviewhtml    += '<p><strong>'+level_title+': </strong>';
                        reviewhtml    += '<span>'+tyroe_comment+'</span></p>';
                        reviewhtml    += '</div>';
                        reviewhtml    += '<div class="span3 rt-range review_range_sliders'+review_id+'">';
                        reviewhtml    += $('.review_range_sliders'+tyroe_id).html();
                        reviewhtml    += '</div>';
                        reviewhtml    += '</div>';
                        $('.row-id'+tyroe_id+' .tyroe_score_over_img .tyroe_score_holder').text(total_score);
                        $('.content_section'+tyroe_id).html(reviewhtml);

                    }
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(result.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                $('.loader-save-holder').hide();
            }
        });
    }

});



//function for profile page review edit
$(document).on('click','.listbtn_handler',function(){
    $('.active_fillters').trigger('click');
    $('.listview_btn').removeClass('listbtn_handler');
});
<?php } ?>
</script>
<script>
    $(document).on("click",".hide_search_single",function(){
        var tyroe_id = $(this).data('value');
        $.ajax({
            type: "POST",
            data: {tyroe_id:tyroe_id},
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".row-id"+tyroe_id).hide(500);
                    $(".editor-seperator-row"+tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.listview_btn').trigger('click');
                $('.hiderow'+tyroe_id).remove();
            },
            failure: function (errMsg) {
            }
        });
    });

    //add to shortlist
    $(document).on('click','.add_shortlist',function(){
        var job_detail_id     = $(this).data('value');
        var tyroe_id          = $(this).data('id');
        var action            = 'candidate_to_shortlist';
        var job_id            = '<?php echo $job_id; ?>';
        var candidate_count   = $('.candidate_counter').text();
        var shortlist_count   = $('.shortlist_counter').text();
        candidate_count       = parseInt(candidate_count)-1;
        shortlist_count       = parseInt(shortlist_count)+1;
        $.ajax({
            type: 'POST',
            data: 'action='+action+'&job_detail_id='+job_detail_id+'&job_id='+job_id+"&tyroe_id="+tyroe_id,
            url: '<?=$vObj->getURL("openingoverview/applicaint_to_candidate")?>',
            success: function(result){
                $('.candidate_counter').text(candidate_count);
                $('.shortlist_counter').text(shortlist_count);
                if(total_candidate == 1){
                    $('.filter_editor').html('<div class="container"><div class="col-md-12"><div class="alert alert-warning "><i class="icon-warning-sign"></i><span>No candidate found</span></div></div></div>');
                }else{
                    $('.hiderow'+job_detail_id).fadeOut();
                    $('.active_fillters').trigger('click');
                }

            }
        });
    });
    $(document).on('click','.add_candidate_from_shortlist',function(){
        var job_detail_id     = $(this).data('value');
        var tyroe_id          = $(this).data('id');
        var action            = 'shortlist_to_candidate';
        var job_id            = '<?php echo $job_id; ?>';
        var candidate_count   = $('.candidate_counter').text();
        var shortlist_count   = $('.shortlist_counter').text();
        candidate_count  = parseInt(candidate_count)+1;
        shortlist_count = parseInt(shortlist_count)-1;
        $.ajax({
            type: 'POST',
            data: 'action='+action+'&job_detail_id='+job_detail_id+'&job_id='+job_id+"&tyroe_id="+tyroe_id,
            url: '<?=$vObj->getURL("openingoverview/applicaint_to_candidate")?>',
            success: function(){
                $('.candidate_counter').text(candidate_count);
                $('.shortlist_counter').text(shortlist_count);
                if(total_shortlisted == 1){
                    $('.filter_editor').html('<div class="container"><div class="col-md-12"><div class="alert alert-warning"><i class="icon-warning-sign"></i><span>Shortlisted not found</span></div></div></div>');
                }else{
                    $('.hiderow'+job_detail_id).fadeOut();
                    $('.jobopening_tyroes_tabs_active').trigger('click');
                }

            }
        });
    });

    $(document).on('click','.add_candidate, .decline_applicaint',function(){
        var job_detail_id     = $(this).data('id');
        var tyroe_id          = $(this).attr('id');
        var action            = $(this).data('value');
        var job_id            = '<?php echo $job_id; ?>';
        var applicaint_count  = $('.applicaint_counter').text();
        var candidate_count   = $('.candidate_counter').text();
        applicaint_count = parseInt(applicaint_count)-1;
        candidate_count  = parseInt(candidate_count)+1;
        $.ajax({
            type: 'POST',
            data: 'action='+action+'&job_detail_id='+job_detail_id+'&job_id='+job_id+"&tyroe_id="+tyroe_id,
            url: '<?=$vObj->getURL("openingoverview/applicaint_to_candidate")?>',
            success: function(){
                if(action == 'add_candidate'){
                    $('.applicaint_counter').text(applicaint_count);
                    $('.candidate_counter').text(candidate_count);
                    $('.single_person_pagination .applicaint_id'+tyroe_id).remove();
                    $('.jobopening_tyroes_tabs_active').trigger('click');
                }else{
                    $('.jobopening_tyroes_tabs_active').trigger('click');
                }
                if(total_applicaint == 1){
                    $('.filter_editor').html('<div class="container"><div class="col-md-12"><div class="alert alert-warning"><i class="icon-warning-sign"></i><span>No applicaint found</span></div></div> </div>');
                }else{
                    $('.hiderow'+job_detail_id).fadeOut();
                }

            }
        });
    });
    //create pagination
    var jquery_total_page = $('.filter_editor .jquery_pagination').size();
    var start_pages = 1;
    if(jquery_total_page > 5){
        var end_pages = 5;
    }else{
        var end_pages = jquery_total_page;
    }
    //create pages
    var jquery_pagination_html = '';
    var page_number = 1;
    function pagination_genrator(){
        if(page_number == 1){
            jquery_pagination_html += '<li class="active"><a href="javascript:void(0)">&lt;&lt;</a></li>';
            jquery_pagination_html += '<li class="active"><a href="javascript:void(0)">&lt;</a></li>';
        }else{
            jquery_pagination_html += '<li><a  href="javascript:void(0)" onclick="change_page(this)" data-id="1">&lt;&lt;</a></li>';
            jquery_pagination_html += '<li><a  href="javascript:void(0)" onclick="change_page(this)" data-id="'+(page_number-1)+'">&lt;</a></li>';
        }
        for(var jpage=start_pages;jpage<=end_pages;jpage++){
                if(jpage == page_number){
                    jquery_pagination_html += '<li class="active" ><a  href="javascript:void(0)">'+jpage+'</a></li>';
                }else{
                    jquery_pagination_html += '<li><a  href="javascript:void(0)" onclick="change_page(this)" data-id="'+jpage+'">'+jpage+'</a></li>';
                }
         }
        if(page_number == jquery_total_page){
            jquery_pagination_html += '<li class="active"><a href="javascript:void(0)">&gt;</a></li>';
            jquery_pagination_html += '<li class="active"><a href="javascript:void(0)">&gt;&gt;</a></li>';
        }else{
            jquery_pagination_html += '<li><a  href="javascript:void(0)" onclick="change_page(this)" data-id="'+(page_number+1)+'">&gt;</a></li>';
            jquery_pagination_html += '<li><a  href="javascript:void(0)" onclick="change_page(this)" data-id="'+jquery_total_page+'">&gt;&gt;</a></li>';
        }
        if(jquery_total_page > 1){
            $('.jquery_p').show();
        }
        $('.jquery_p .jquery_pagination_container').html(jquery_pagination_html);

    }
    var pre_page = 0;
    function change_page(this_ele){
        jquery_pagination_html = '';
        page_number = $(this_ele).data('id');
        if(jquery_total_page > 5){
            if(page_number > (jquery_total_page-5)){
                start_pages = jquery_total_page-5;
                end_pages = jquery_total_page;
            }else if(page_number < 5){
                end_pages = 5;
                start_pages = 1;
            }else {
                if(page_number > 3){
                    start_pages = page_number-3;
                }
                if(page_number < jquery_total_page-3){
                    if(pre_page < page_number){
                        end_pages++;
                    }else if(pre_page > page_number){
                        end_pages--;
                    }
                }
            }
        }else{
            end_pages = jquery_total_page;
            start_pages = 1
        }

        $('.filter_editor .jquery_pagination').hide();
        $('.filter_editor .jquery_pagination:eq('+(page_number-1)+')').show();
        $('body').animate({scrollTop:0},800);
        pagination_genrator();
        pre_page = page_number;
    }
</script>

<!--This input type hidden field for get postion of page-->
<input type="hidden" id="user_page_postion">
<!--to get the declined users from activity stream?-->
<input type="hidden" id="totrigger" value="">
<!--/-->
<div  class="loader-save-holder"><div class="loader-save"></div></div>
<div class="content">
   <div class="container-fluid opening_container padding-default">
<!-- upper main stats -->
        <?php
        $this->load->view('navigation_top');
        ?>
        <div class="clearfix"></div>
        <div id="main-stats" class="margin-top-4 hidden fillters_controllers st-sec">
            <div class="row stats-row">
                <div class="container-fluid">
                    <div class="row-fluid">
                <div class="container span11 ">
                    <div class="pagination_controllers hidden span4">
                        <div class="container-fluid">
                            <div class="row-fluid">
                                <div id="Search_filters" class="span11">
                                    <div class="row">
                                        <div id="editors_pick" class="rt-edit-list">
                                            <div id="editors_pick_info">
                                                <div class="span12 offset1">
                                                <button class="btn btn-primary listview_btn list-rt-btn">LIST VIEW</button>
                                                <div class="btn-group">
                                                    <button class="btn btn-default single_back glow left" type="button"> <i class="icon-angle-left"></i> </button>
                                                    <button class="btn btn-default single_next glow right" type="button"> <i class="icon-angle-right"></i> </button>
                                                </div>
                                                <span class="single_num_of_records"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <div class="fillter_applicaint pull-right candidate_area_filters btn-sp" style="display: none;">
                <form>
                    <div class="btn-group large">
                        <input type="button" class="btn-default btn left fillter_action" data-value="Pending"  value="Pending">
                        <input type="button" class="btn-default btn middle fillter_action" data-value="Review"  value="Reviewed">
                        <input type="button" class="btn-default btn right fillter_action" data-value="Decline" value="Declined">
                    </div>
                    <select class="btn btn-default slect-btn" name="fillter_drop">
                        <option value="">Filter user</option>
                        <option value="aphabetical">Alphabetical</option>
                        <option value="most_recent">Most Recent</option>
                    </select>
                </form>
            </div>
                <div class="fillter_applicaint pull-right applicant_area_filters btn-sp" style="display: none">
                <form>
                    <input type="button" class="btn-default btn decline_users" data-value="Decline"  value="Declined">
                    <select class="btn btn-default slect-btn" name="fillter_drop">
                        <option value="">Filter user</option>
                        <option value="aphabetical">Alphabetical</option>
                        <option value="most_recent">Most Recent</option>
                        <option value="highiest_score">Highest Score</option>
                    </select>
                </form>
            </div>
                <div class="fillter_applicaint pull-right shortlist_area_filters btn-sp" style="display: none">
                <select class="btn btn-default slect-btn" name="fillter_drop">
                    <option value="">Filter user</option>
                    <option value="aphabetical">Alphabetical</option>
                    <option value="most_recent">Most Recent</option>
                </select>
            </div>
                <div class="clearfix"></div>
            </div>
                </div>
                </div>
            </div>
        </div>
        <div id="main-stats" class="margin-default hidden singleprofile_controllers"></div>
    <!-- end upper main stats -->
        <div id="pad-wrapper">
    <!-- END LEFT COLUMN -->
            <div class="grid-wrapper tyroes_content_result">
                <div class="row-fluid show-grid">
                    <!-- START RIGHT COLUMN -->
                    <!-- START JOB OPENING -->
                    <div class="span12" id="opening-overview">
                        <div class="right-column" style="position:relative;padding-left: 0;">
                            <div class="row-fluid header">
                                <div class="span12">
                                    <h4></h4>
                                </div>
                            </div>
                            <input type="hidden" id="edit_id" value="<?=$job_id?>">
                        <!-- morris bar & donut charts -->
                            <div class="row-fluid">
                                <div class="span5 chart">
                                    <?php if( ($candidate_count['total_applicants'] > 0) || ($candidate_count['total_candidates'] > 0) || ($candidate_count['total_shortlist'] > 0) ){?>
                                    <div id="reviewer-donut" style="height: 200px;">
                                    </div>
                                    <?php } ?>
                                    <div class="text-center gr-sec">
                                        <!--<h5 style="text-align: center;">Reviewer Status</h5>-->
                                        <a href="javascript:void(0)" id="btn_team_members" class="btn-flat blue team_modal gr-btn-1"> Team Members</a>
                                        <?php if($role_id == STUDIO_ROLE){ ?>
                                            <br><br>
                                            <a href="javascript:void(0)" id="btn_widgets_and_sharing" class="btn-flat gray gr-btn-2">Widgets + Sharing</a>
                                            <br><br>
                                            <a class="edit-anc" href="javascript:void(0)" id="edit_job">edit</a>  <a href="javascript:void(0)" class="add_to_archive edit-anc" data-value="<?=$job_id?>">Archive</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- placing job details here -->
                                <div class="span7 margin-left-default">
                                    <h3 class="dt-1"><?php echo $get_jobs['job_title']; ?></h3><br>
                                    <h5 class="dt-2"><?php echo $get_jobs['job_location'] . "," . $get_jobs['job_city']; ?></h5><br>
                                    <p class="text-justify dt-2"> <?php echo $get_jobs['job_description']; ?></p>
                                    <div class="job-lower-part pull-left width-100 margin-top-3 margin-bottom-4">
                                        <div class="span4 text-left st-1"><span class="opening-date"><i class="icon-calendar"></i> <?php echo date('d M Y', $get_jobs['start_date']); ?> </span></div>
                                        <div class="span4 text-center st-1"><span class="opening-month"><i class="icon-time"></i> <?php echo $datedifference ?></span></div>
                                        <div class="span4 text-right st-1"><span class="opening-time"><i class="icon-legal"></i> <?php echo $get_jobs['job_type']; ?></span></div>
                                    </div>
                                    <p class="dt-label pull-left"><?php foreach ($job_skills as $skill){?><span class="label"><?=$skill['creative_skill']?></span> <?php } ?></p>
                                </div>
                                <div class="dt-row margin-top-12 margin-bottom-12"></div>
                            </div>
                            <!-- START ACTIVITY STREAM -->
                              <?php if($role_id == STUDIO_ROLE){ ?>
                            <!--<div class="row-fluid header section activity-stream">
                                <div class="span12">
                                                <span class="pull-right button-center">
                                                 <a class="btn-flat primary" id="comment" onclick="commentbox();">Add
                                                     Comment</a></span>
                                </div>
                                <span id="message"></span>
                            </div>-->
                            <input type="hidden" id="jid" name="jid" value="<?php echo $get_jobs['job_id']; ?>">

                            <div class="header section field-box comment-box" style="display: none">

                                <textarea class="span9 inline-input"
                                          name="comment_text" id="comment_text" placeholder="Type Comment Here"></textarea> <br>
                                <a class="btn-flat primary" id="insert_comment" onclick="InsertComment();">Save</a>OR
                                <input type="reset" value="Cancel" class="reset" onclick="hideComment();"></span>
                            </div>
                            <div class="span11 margin-bottom-7">
                                <h4 class="rt-1">Anonymous Feedback</h4>
                                <div class="row-fluid">
                                    <p class="dt-2">
                                        Your team would like to share following feedback with a Tyroe. Please help them by approving this incredibly valuable feedback.
                                        Comments will be sent to Tyroe's on behalf of your company eg: <i>An employee from <?=  $team_admin['company_name']; ?> has reviewed your profile and provided the following feedback.</i>
                                    </p>
                                </div>
                            </div>
                            <div class="span12">
                                <?php if($get_anonymous != ''){ ?>
                                    <div class="row-fluid feedback_main_content">
                                        <table class="table table-br">
                                            <tr class="tr-head">
                                                <th>Team members</th>
                                                <th><span class="tabel-th-br"></span>Message</th>
                                                <th class="text-right"><span class="tabel-th-br"></span>Action</th>
                                            </tr>
                                            <?php foreach($get_anonymous as $anonymous){
                                                $image;
                                                if ($anonymous['media_name'] != "") {
                                                    $image = ASSETS_PATH_PROFILE_THUMB . $anonymous['media_name'];
                                                } else {
                                                    $image = ASSETS_PATH_NO_IMAGE;
                                                }
                                                if($anonymous['anonymous_feedback_message'] != ""){ ?>
                                                    <tr class="feedback_row<?=$anonymous['review_id']?>">
                                                        <td class="span3">
                                                            <div class="span3">
                                                                <img src="<?= $image ?>" class="img-circle avatar-55">
                                                            </div>
                                                            <div class="span9 td-user-info margin-top-2">
                                                                <a href="#" class="name_sidebar"><?php echo $anonymous['firstname'].' '.$anonymous['lastname']; ?></a>
                                                                <div class="clearfix"></div>
                                                                <span class="location"><?php echo $anonymous['user_occupation']; ?> Designer</span>
                                                            </div>
                                                        </td>
                                                        <td class="span7">
                                                            <p><?php echo $anonymous['anonymous_feedback_message']; ?> <a href="javascript:void(0);">read more...</a> </p>
                                                        </td>
                                                        <td class="span2 text-right td-btn-sec">
                                                            <a href="javascript:void(0)" class="label-success label approve_anonymous_feedbeck" data-value="<?=$anonymous['review_id']?>" data-id="<?=$anonymous['user_id']?>">Approve</a>
                                                            <a href="javascript:void(0)" class="delete_anonymous_feedbeck margin-left-2 margin-right-2" data-value="<?=$anonymous['review_id']?>">Delete</a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            <?php } ?>
                                        </table>
                                    </div>
                                <?php }else{ ?>
                                    <p>No feedback message to approved</p>
                                <?php } ?>
                            </div>
                            <div class="dt-row margin-top-12 margin-bottom-12"></div>
                        <?php } ?>
                                        <div class="span12 margin-bottom-6">
                                            <h4 class="pull-left">Activity Stream</h4>
                                            <div class="row-fluid span10 pull-right">
                                                <div class="container span4 pull-right activity-last" >
                                                    <div class="field-box">
                                                        <div class="btn-group pull-left stream-view">
                                                            <button class="glow left search_by" data-value="1">All</button>
                                                            <button class="glow search_by" data-value="2">This Week</button>
                                                            <button class="glow right search_by" data-value="3">This Month</button>
                                                            <input type="hidden" name="stream_duration" id="stream_duration" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="field-box span4 pull-right activity-keyword tab-margin">
                                                    <div class="span12 ctrls">
                                                        <input type="text" name="stream_keyword" id="stream_keyword"
                                                               value="<?= $stream_keyword ?>"
                                                               placeholder="Keywords" onkeypress="FilterStreams()"
                                                               class="search span12 padding-1">
                                                    </div>
                                                </div>
                                                <div class="span4 pull-right activity-search tab-margin">
                                                    <div class="container span12">
                                                        <div class="field-box">
                                                            <div class="ui-select find rt-slect">
                                                                <select class="span5 inline-input" name="activity_type"
                                                                        id="activity_type" onchange="FilterStreams()">
                                                                    <option value="">Select Type</option>
                                                                    <?php
                                                                    foreach ($get_activity_type as $key => $activity_type) {
                                                                        ?>
                                                                        <option
                                                                            value="<?php echo $activity_type['activity_type_id'] ?>"><?php echo $activity_type['activity_title'] ?></option>
                                                                    <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <form method="post" action="" id="jumptoform">
                                            <input type="hidden" name="job_id" id="job_id" value="">
                                            <input type="hidden" name="job_title" id="job_title" value="">
                                            <input type="hidden" name="action" id="action" value="">
                                        </form>
                                        <form enctype="multipart/form-data" id="form_stream" class="form_stream" name="stream"
                              method="post" action="<?php//= $vObj->getURL('exposure/feedback_rating') ?>">
                            <div class="row-fluid">
                                <div class="span12">
                                    <div class="navbar navbar-inverse">
                                        <ul style="margin: 0 10px 10px 10px; list-style:none;">
                                            <li class="stream-container">
                                                <div class="stream-dialog">
                                                    <div class="filter-result"></div>
                                                    <div class="notes no-filter">
                                                        <?php
                                                        if (is_array($job_activity) == "") {
                                                            ?>
                                                            <div class="alert alert-info">
                                                                <i class="icon-exclamation-sign"></i>
                                                                There is no activity
                                                            </div><?php

                                                        } else {
                                                            foreach ($job_activity as $job_activity_log) {
                                                                $icon;
                                                                $text;
                                                                $link = "";
                                                                $href = "javascript:void(0)";
                                                                $onclick = "";
                                                                if ($job_activity_log['activity_type_id'] == LABEL_APPLY_JOB_LOG) {
                                                                    /*JOB APPLICANT SECTION*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_FAVOURITED_PROFILE_ACTIVITY) {
                                                                    /*Profile Favourited Section*/
                                                                    $icon = LABEL_ICON_STAR;
                                                                    $activity_data_email = unserialize(stripslashes($job_activity_log['activity_data']));
                                                                    $text = "You added " . $activity_data_email['tyroe_name'] . " to your Favourites";
                                                                    $href = $vObj->getURL('favourite');

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_JOB_CANDIDATE_ACTIVITY) {
                                                                    /*JOB CANDIDATE Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_SHORTLIST_JOB_LOG) {
                                                                    /*JOB SHORTLIST Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_NEW_JOB_LOG) {
                                                                    /*OPENINGS Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];

                                                                } else if ($job_activity_log['activity_type_id'] == LABEL_FEEDBACK_ACTIVITY) {
                                                                    /*Feedback Section*/
                                                                    $return_activity_stream = $vObj->studio_reviewer_activity_sub_type_text($job_activity_log); // Get the text...
                                                                    if(count($return_activity_stream) == 0)continue;
                                                                    $icon = $return_activity_stream['icon'];
                                                                    $text = $return_activity_stream['text'];
                                                                    $href = $return_activity_stream['href'];
                                                                    $onclick = $return_activity_stream['onclick'];
                                                                }

                                                                ?>
                                                                <a href="<?= $href ?>" class="activity_val" data-id="<?= $link ?>"
                                                                   data-value="<?= $id ?>" onclick="<?= $onclick ?>">
                                                                                <span class="item">
                                                                                <i class="<?php echo $icon; ?>"></i> <?php //echo $job_activity_log['username'] ?>
                                                                                    <?= $text ?>
                                                                                    <span class="time"><i
                                                                                            class="icon-time"></i> <?php echo get_activity_time($job_activity_log['created_timestamp']); ?></span>
                                                                </a></span>
                                                            <?php
                                                            }

                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </div>
                    </div>
                    <!-- END JOB OPENING -->
                    <!-- END RIGHT COLUMN -->
                </div>
            </div>
        </div>
   </div>
</div>
<!-- end main container -->
<!--Edit job modal start-->

<div style="display: none;" class="modal fade" id="editmodal" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close2 reset_borders" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"> Update Job </h4>
                </div>
                <div class="global-popup_body">
                    <div class="step-content">
                        <div id="step1" class="step-pane active">
                            <div class="row-fluid form-wrapper">
                                <div class="span10 center-block">
                                    <form>
                                        <div class="team-mod-setting-container">
                                            <h3>Settings</h3>
                                            <div class="field-box" id="rt_description">
                                                <div class="span10">
                                                    <h6>Team moderation</h6>
                                                    <p>Allow adminstrators to moderate all feedback shared with artists. If turned off, team members can share thier helpful feedback directly to artists on behalf of your company.</p>
                                                </div>
                                                <div class="span2">
                                                    <div class="slider-frame pull-right">
                                                        <?php if($get_jobs['team_moderation'] == 1){ ?>
                                                            <span data-on-text="ON" data-off-text="OFF" class="slider-button team_moderation on">ON</span>
                                                        <?php }else{ ?>
                                                            <span data-on-text="ON" data-off-text="OFF" class="slider-button team_moderation">OFF</span>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="modal_error_box alert alert-danger"></div>
                                        <div class="field-box">
                                            <input type="text" class="form-control" placeholder="Job Title" name="job_title"
                                                   id="job_title_new" value="<?=$get_jobs['job_title']?>">
                                            <input type="hidden" name="hidden_username" id="hidden_username"
                                                   value="<?= $studio_name ?>">
                                        </div>
                                        <div class="field-box">
                                            <textarea name="" class="form-control" id="job_description" cols="30"
                                                      rows="10" placeholder="Description"><?php
                                                $description = str_replace('<br />','&#13;',$get_jobs['job_description']);
                                                echo $description;
                                                ?></textarea>
                                        </div>
                                        <div  class="field-box" >
                                            <div class="row-fluid">
                                                <div class="span12">
                                                    <select name="job_industry" id="job_industry" class="job_industry">
                                                        <option value="">Industry</option>
                                                        <?php
                                                        foreach ($job_industries as $industry) {
                                                            if($industry['industry_id']==$get_jobs['industry_id'])
                                                                $selected ="selected=selected";
                                                            else
                                                                $selected=null;
                                                            ?>
                                                            <option <?=$selected?>
                                                                value="<?= $industry['industry_id'] ?>" ><?= $industry['industry_name'] ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="field-box" style="margin-bottom: 25px;">
                                            <div class="row-fluid">
                                                <div class="span6">
                                                    <select name="country" id="country" onchange="getcities(this.value)">
                                                        <option value="">Country</option>
                                                        <?php
                                                        foreach ($get_countries as $country) {
                                                            if($country['id']==$get_jobs['country_id'])
                                                                $selected ="selected=selected";
                                                            else
                                                                $selected=null;
                                                            ?>
                                                            <option <?=$selected?>
                                                                value="<?php echo $country['id'] ?>"><?php echo $country['name'] ?></option>
                                                        <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="span6" id="citybox">
                                                    <select name="city" id="city" class="city">
                                                        <option value="">city</option>
                                                        <?php
                                                        $cities=$vObj->getAll("SELECT * FROM ".TABLE_CITIES." WHERE country_id=".$get_jobs['country_id']);
                                                        foreach($cities as $city){
                                                            echo $city['city'] .'=='. $get_jobs['job_city'];
                                                            if($city['city_id'] == $city['city_id']){
                                                                $selected = 'selected = selected';
                                                            }else{
                                                                $selected = '';
                                                            }

                                                            ?> <option value="<?php echo $city['city_id']; ?>" <?php echo $selected; ?>><?php echo $city['city']; ?></option><?php
                                                        }

                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field-box">
                                            <div class="row-fluid">
                                               <div class="span6 date-period">
                                                    <input type="text" name="startdate" id="startdate" placeholder="Start-Date" value="<?=date('d/m/Y', $get_jobs['start_date']);?>">
                                               </div>
                                                <div class="span6 date-period">
                                                    <input type="text" name="enddate" id="enddate" placeholder="End-Date" value="<?=date('d/m/Y', $get_jobs['end_date']);?>">
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" field-box lined margin-30 job-type sr-sec">
                                            <div class="row-fluid type-extra-padding">
                                                <?php
                                                foreach ($get_job_type as $type) {
                                                    ?>
                                                    <div class="span4">
                                                        <div class="job-type-holder">
                                                            <input type="radio" class="job_type" value="<?= $type['job_type_id'] ?>" data-id="<?= $type['job_type'] ?>" name="job_type">
                                                            <span><?= $type['job_type'] ?></span>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="field-box  job-level sr-sec-2">
                                            <div class="row-fluid">
                                                <?php
                                                foreach ($get_job_level as $level) {
                                                    ?>
                                                    <div class="span3">
                                                        <div class="job-level-holder">
                                                            <input type="radio" class="job_level" value="<?= $level['job_level_id'] ?>" data-id="<?= $level['level_title'] ?>" name="job_level">
                                                            <span><?= $level['level_title'] ?></span>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <?php

                                        foreach ($job_skills as $skill)
                                        {
                                            $skillnames[]= $skill['creative_skill'];
                                        }

                                        $skillnamesval=implode(",",$skillnames);
                                        ?>
                                        <br clear="all">

                                        <div id="field-box">
                                            <div class="row-fluid">
                                                <input type="text" name="job_tags" placeholder="tags" id="job_tags" value="<?=$skillnamesval?>" class="span12 form-control" style="padding-left:20px;height: 45px">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer job_create_footer">
                   <div class="margin-right-49">
                        <button class="btn-glow success" type="button" id="post_job"
                                onclick="postjob()">
                            Update
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--edit job modal end-->
<!-- Team Members Modal start-->
<div style="display: none;" class="modal fade" id="team_member_modal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="job_create_container">
        <div class="job_create_details">
            <div class="modal-content job_create_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="top: 23px;">&times;</button>
                </div>
                <div class="job_create_body">
                   <div id="step2" class="step-pane">
                       <div class=" form-wrapper">
                           <?php if($role_id == STUDIO_ROLE){ ?>
                               <div class="span4 center-block">
                                   <h3 class="label-head">Add existing reviewers</h3>
                                   <div class="field-box">
                                       <input type="text" id="search_existing_reviewer" placeholder="Search 24 Collaborators" class="radius-search">

                                       <ul class="searching_reviewer_result ext-rvw-list">

                                       </ul>
                                   </div>
                                   <div class="clearfix"></div>
                               </div>
                               <div class="span5 center-block">
                                   <div class="legend-sep"></div>
                               </div>
                               <div class="span5 center-block">
                                   <h3 class="label-head">Invite new reviewers</h3>
                                   <div class="field-box">
                                       <div class="row-fluid">
                                           <div class="span9">
                                               <input type="text" name="invitation" placeholder="Emails" id="invitation_emails">
                                           </div>
                                           <div class="span3">
                                               <button class="btn-flat success" id="send_invitaion">Send invite</button>
                                           </div>
                                       </div>

                                   </div>
                               </div>
                               <div class="clearfix"></div>
                           <?php } ?>
                           <div class="form_users_list">
                               <ul class="form-list-holder" id="reviewer_container">
                                   <li>
                                       <div class="span5 center-block">
                                           <div class="row-fluid">
                                               <div class="span8">
                                                   <div class="span2 user-display">
                                                       <?php
                                                       $image;
                                                       if ($team_admin['media_name'] != "") {
                                                           $image = ASSETS_PATH_PROFILE_THUMB . $team_admin['media_name'];
                                                       } else {
                                                           $image = ASSETS_PATH_NO_IMAGE;
                                                       }
                                                       ?>
                                                       <img src="<?php echo $image; ?>" alt="avatar" class="img-circle avatar hidden-phone">
                                                   </div>
                                                   <div class="span10">
                                                       <a href="javascript:void(0);" class="name"><?php echo $team_admin['firstname'] . ' ' . $team_admin['lastname']; ?></a>
                                                       <span class="subtext"><?php echo $team_admin['user_occupation']; ?> <span class="tag">Admin</span></span>
                                                   </div>
                                               </div>
                                               <div class="span4">
                                                   <a href="#" class="mail-user"><?php echo $team_admin['email']; ?></a>
                                               </div>
                                           </div>
                                       </div>
                                   </li>
                                   <?php  foreach($get_reviewers as $get_reviewer){
                                      $image;
                                      if ($get_reviewer['media_name'] != "") {
                                          $image = ASSETS_PATH_PROFILE_THUMB . $get_reviewer['media_name'];
                                      } else {
                                          $image = ASSETS_PATH_NO_IMAGE;
                                      }
                                  ?>
                                   <li class="list_parent_row<?php echo $get_reviewer['user_id']; ?>" >
                                       <div class="span5 center-block">
                                           <div class="row-fluid">
                                               <div class="span8">
                                                   <div class="span2 user-display">
                                                       <?php
                                                       $image;
                                                       if ($get_reviewer['media_name'] != "") {
                                                           $image = ASSETS_PATH_PROFILE_THUMB . $get_reviewer['media_name'];
                                                       } else {
                                                           $image = ASSETS_PATH_NO_IMAGE;
                                                       }
                                                       ?>
                                                       <img src="<?php echo $image; ?>" alt="avatar" class="img-circle avatar hidden-phone">
                                                   </div>
                                                   <div class="span10">
                                                       <a href="javascript:void(0);" class="name"><?php echo $get_reviewer['firstname'] . ' ' . $get_reviewer['lastname']; ?></a>
                                                       <span class="subtext"><?php echo $get_reviewer['user_occupation']; ?><span class="tag">Reviewer</span></span>
                                                   </div>
                                               </div>
                                               <div class="span4">
                                                   <a href="#" class="mail-user"><?php echo $get_record['email']; ?></a>
                                               </div>
                                           </div>
                                       </div>
                                   </li>
                                   <?php } ?>
                               </ul>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
        <!-- Team Members Modal End-->

<!--Widgets & sharing Modal start-->
<div style="display: none;" class="modal fade" id="widgets_and_sharing" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="global-popup_container">
        <div class="global-popup_details">
            <div class="global-popup_holder global-popup social-modal">
                <div class="modal-header job_create_header st-header">
                    <div class="container-fluid">
                        <div class="row-fluid">
                    <div class="global-popup_body">
                        <div class="md-supporter position-relative">
                            <div>
                                <button type="button" class="close tr-close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h3>Social Share</h3>
                                <p class="st-text">Click here to quickly notify your socical media networks about this job</p>
                            </div>
                            <!--<div id="social_widgets_share_icons" class="margin-bottom-10"></div>-->
                            <span class='st_facebook_large' displayText='Facebook'></span>
                            <span class='st_twitter_large' displayText='Tweet'></span>
                            <span class='st_linkedin_large' displayText='LinkedIn'></span>
                            <span class='st_pinterest_large' displayText='Pinterest'></span>
                            <span class='st_googleplus_large' displayText='Google +'></span>
                            <span class='st_email_large' displayText='Email'></span>
                            <h3>Buttons</h3>
                            <p class="ft-text margin-top-1">Add buttons to your website job boards to link them directly to this job</p>
                            <form>
                                <div class="span3 text-center" style="margin-left: 0">
                                    <div class="radio-wrap">
                                        <input type="radio" name="btn_widgets" class="get_widgets_color" value="blue-btn" checked="checked">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn-flat primary">Apply with <b>Tyroe</b></button>
                                </div>
                                <div class="span3 text-center">
                                    <div class="radio-wrap">
                                        <input type="radio" name="btn_widgets" class="get_widgets_color" value="grey-btn" >
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn-flat gray">Apply with <b>Tyroe</b></button>
                                </div>
                                <div class="span3 text-center">
                                    <div class="radio-wrap">
                                        <input type="radio" name="btn_widgets" class="get_widgets_color" value="white-btn" >
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn-flat white">Apply with <b>Tyroe</b></button>
                                </div>
                                <div class="span3 text-center">
                                    <div class="radio-wrap">
                                        <input type="radio" name="btn_widgets" class="get_widgets_color" value="black-btn">
                                    </div>
                                    <div class="clearfix"></div>
                                    <button type="button" class="btn-flat inverse">Apply with <b>Tyroe</b></button>
                                </div>
                                <div class="clearfix"></div>
                                <p class="ft-text-2 margin-top-4 margin-bottom-2">Copy & paste this code</p>
                                <div id="wideget_code_area" contenteditable="false" style="margin-left: 0;min-height:120px;box-shadow: 1px 1px 1px #ccc,-1px -1px 1px #ccc;padding-left:10px; padding-top:10px;">
                                    &lt;div id="widgets-btn"&gt;
                                    &lt;script type="text/javascript"&gt;
                                    var jobid       = '<?=$job_id?>';
                                    var comapnyname = '<?=$this->session->userdata('firstname').' '.$this->session->userdata('lastname');?>';
                                    var job_url     = '<?= $vObj->getURL("openings"); ?>';
                                    var btn_theme   = 'blue-btn';
                                    &lt;/script&gt;
                                    &lt;script type="text/javascript" src="<?= ASSETS_PATH ?>js/api_tyroe.js"&gt;
                                    &lt;/script&gt;
                                    &lt;/div&gt;
                                </div>
                            </form>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Widgets & sharing Modal End-->


<!-- CANDIDATE SECTION EMAIL CONTAINER -->
<div style="display: none;" class="modal fade" id="myModal_candidate" role="dialog"
     aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="global-popup_container">
         <div class="global-popup_details">
            <div class="global-popup_holder global-popup ">
                <div class="modal-header job_create_header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Send Mail</h4>

                </div>
                <div class="span8 email_content_body">
                    <div class="global-popup_body ">
                        <div class="center prof-dtl">
                            <div class="tab-margin">
                               <div class="sugg-msg alert alert-info" style="display: none"></div>
                               <div class="clearfix"></div>
                               <div class="field-box">
                                   <input type="text" name="email-subject" id="email-subject" placeholder="Subject"/>
                               </div>
                               <div class="field-box">
                                    <textarea name="" id="" class="email-message" placeholder="Message"></textarea>
                               </div>
                           </div>
                       </div>
                    </div>
                    <div class="global-popup_footer">
                        <button type="button" class="btn btn-flat white email_close"  style="display: none" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-flat btn-save btn-send-email" value="<?= LABEL_SUBMIT ?>">
                            <?= LABEL_SUBMIT ?>
                        </button>
                   </div>
               </div>
               <div class="clearfix"></div>
           </div>
       </div>
    </div>
</div>
<!-- /CANDIDATE SECTION EMAIL CONTAINER -->


<!-- morrisjs -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/morris.min.js"></script>
<!-- build the charts -->
<script type="text/javascript">

    // Morris Donut Chart
    /*Morris.Donut({
     element: 'candidate-donut',
     data: [
     {label: 'Interested', value: 75 },
     {label: 'Not Interested', value: 10 },
     {label: 'Pending', value: 15 },

     ],
     colors: ["#81bd82", "#f8d2e0", "#76bdee"],
     formatter: function (y) {
     return y + "%"
     }
     });*/

    // Morris Donut Chart
    Morris.Donut({
        element: 'reviewer-donut',
        data: [
            {label: 'Reviewed', value: <?php echo $get_total_reviewed; ?> },
            {label: 'Pending', value: <?php echo $get_total_pending; ?> }
        ],
        colors: ["#1BA0EF", "#BB5D81" ],
        formatter: function (y) {
            return y + "%"
        }
    });

</script>
<script>
    $(function () {
        $('#country').select2({
            placeholder: 'Country'
        });
        $('#city').select2({
            placeholder: 'City'
        });
        $('#job_industry').select2({
            placeholder: 'Specialities'
        });
    })


    $(document).on('click','.close2',function(){
        $('#job_title_new').css("border","");
        $('#job_description').css("border","");
        $('.job_industry').css("border","");
        $('.country').css("border","");
        $('.city').css("border","");
        $('#startdate').css("border","");
        $('#enddate').css("border","");
        $('.job-level').css("border","");
        $('#s2id_job_tags').css("border","");
        $('.type-extra-padding').css("border","");
        $('.modal_error_box').hide();
    });
</script>
