<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]>
<html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]>
<html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]>
<html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="en-US"> <!--<![endif]-->
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if($controller_name == "publicprofile"){ ?>
    <meta property="og:title" content="<?= $page_title ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="<?= ASSETS_PATH ?>img/tyroe-logo-200x-200x.jpg" />
    <meta property="og:url" content="<?= $page_url ?>" />
    <meta property="og:description" content="<?= $page_type ?>" />
    <meta property="og:site_name" content="<?=LIVE_SITE_URL?>" />
<?php } ?>
<title><?= $page_title ?></title>
<meta name="description" content="<?php echo $this->session->userdata("sv_metatag_desc") ?>">
<meta name="keywords" content="<?php echo $this->session->userdata("sv_metatag_keyword") ?>">
<meta name="author" content="<?php echo $this->session->userdata("sv_metatag_author") ?>">

<link rel="shortcut icon" href="<?= ASSETS_PATH ?>static-site/images/favicon.ico">
<link rel="apple-touch-icon" href="<?= ASSETS_PATH ?>static-site/images/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?= ASSETS_PATH ?>static-site/images/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?= ASSETS_PATH ?>static-site/images/apple-touch-icon-114x114.png">

<?php if (strtolower(ENVIRONMENT) == 'development') { ?>
<meta http-equiv="cache-control" content="max-age=0"/>
<meta http-equiv="cache-control" content="no-cache"/>
<meta http-equiv="expires" content="0"/>
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT"/>
<meta http-equiv="pragma" content="no-cache"/>
<?php } ?>

<?php
if (true === $load_masonry_file) {
    ?>
    <script src="<?= ASSETS_PATH ?>/js/masonry.pkgd.min.js"></script>
<?php
}
?>
<?php if($controller_name == "openingoverview"){ 
    if($controller_name == "openingoverview"){
        
         $keywords = array();
        foreach($skills as $val){
            $keywords[] = $val['creative_skill'];
        }
        $description = strip_tags(substr($job_detail['job_description'], 0, 255));

        if($company_detail['logo_image'] !=''){
            $logo_url = LIVE_SITE_URL.'assets/uploads/company_logo/'.$company_detail['logo_image'];
        }else{
            $logo_url = LIVE_SITE_URL.'assets/img/default-avatar.png';
        }
    ?>
    <meta property="og:title" content="Tyroe Jobs - <?=$job_detail['job_title'].' | '.trim($job_location)?>" />
    <meta property="og:type" content="Tyroe Jobs" />
    <meta property="og:image" content="<?=$logo_url?>" />
    <meta property="og:url" content="<?=$public_job_url?>" />
    <meta property="og:description" content="<?=$description?>" />
    <meta property="og:site_name" content="<?=LIVE_SITE_URL?>" />
    
    <?php    
    }else{
    ?>
    
    <?php if ($get_jobs['job_title']) { ?>
        <meta property="og:type" content="Tyroe Jobs"/>
        <meta property="og:title" content="<?php echo $get_jobs['job_title']; ?>"/>

    <?php } else { ?>
        <meta property="og:type" content="jobs"/>
        <meta property="og:title"
              content="<?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname') . ' Jobs'; ?>"/>

    <?php } ?>

    <?php if ($get_jobs['job_description']) { ?>
        <meta property="og:description" content="<?php echo $get_jobs['job_description']; ?>"/>
    <?php } else { ?>
        <meta property="og:description"
              content="Apply with <?php echo $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname') . ' Jobs'; ?>"/>
    <?php } ?>
    <meta property="og:image" content="<?= ASSETS_PATH ?>img/tyroe-logo-200x-200x.jpg"/>
    <meta property="og:url" content="<?= $vObj->getURL("").'tyroeJobs/'.$vObj->cleanString($get_jobs['job_title']).'-'.$vObj->generate_job_number($get_jobs['job_id']); ?>"/>
<?php }} ?>

<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">-->

<!-- Mobile Specifics -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="HandheldFriendly" content="true"/>
<meta name="MobileOptimized" content="320"/>

<!-- Mobile Internet Explorer ClearType Technology -->
<!--[if IEMobile]>
<meta http-equiv="cleartype" content="on">  <![endif]-->

<style>
    ::selection {
        background: #f97e76;
        color: #fff;
    }

    ::-moz-selection {
        background: #f97e76;
        color: #fff;
    }

    ::-webkit-selection {
        background: #f97e76;
        color: #fff;
    }
</style>

<?php
//$controller_name
//
if ($controller_name == "profile" || $controller_name == "publicprofile" || $controller_name == "profile2" || $controller_name == "search_publicprofile") {
    ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>

    <link href="<?= ASSETS_PATH ?>profile/anibus/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/settings.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/main.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/jquery.fancybox.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/fonts.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/shortcodes.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/responsive.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/custom.css?v=0.02" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/chechkbox.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/viewport.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/js/modernizr.js" rel="stylesheet"/>
<?php
} else if($controller_name == "account"){
?>
    <link href="<?= ASSETS_PATH ?>profile/anibus/custom.css?v=0.02" rel="stylesheet"/>
    <!-- bootstrap -->
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-responsive.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet"/>

    <!-- custom styles -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/layout.css">
<?php
} else {
    ?>
    <!-- bootstrap -->
    <link href="<?= ASSETS_PATH ?>profile/anibus/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-responsive.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet"/>

    <!-- custom styles -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/layout.css">
<?php
}
?>

<!-- libraries -->
<link href="<?= ASSETS_PATH ?>css/lib/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css"/>
<link href="<?= ASSETS_PATH ?>css/lib/bootstrap-wysihtml5.css" type="text/css" rel="stylesheet">
<link href="<?= ASSETS_PATH ?>css/lib/uniform.default.css" type="text/css" rel="stylesheet">
<link href="<?= ASSETS_PATH ?>css/lib/select2.css" type="text/css" rel="stylesheet">
<?php
if(isset($create_job) && $create_job == 1){    
?>
<script src="<?= ASSETS_PATH ?>css/lib/bootstrap-datetimepicker.css"></script>
<?php
}else{
?>
<link href="<?= ASSETS_PATH ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet">
<?php
}
?>
<link href="<?= ASSETS_PATH ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet">
<link href="<?= ASSETS_PATH ?>css/lib/font-awesome.css" type="text/css" rel="stylesheet"/>
<link href="<?= ASSETS_PATH ?>css/lib/jquery.circliful.css" type="text/css" rel="stylesheet">
<!-- global styles -->
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/elements.css">
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/icons.css">
<link rel="stylesheet" href="<?= ASSETS_PATH ?>profile/colorpicker.css" type="text/css"/>
<link rel="stylesheet" media="screen" type="text/css" href="<?= ASSETS_PATH ?>profile/layout.css"/>

<!-- this page specific styles -->
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/form-wizard.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/index.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/reponsive-touches.css" type="text/css" media="screen"/>

<!--Social share styles -->
<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/social_share/jquery.share.css" type="text/css" media="screen"/>
<!--/End Social share styles -->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
<!-- open sans font -->
<link
    href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
    rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>


<![endif]-->

<?php
    if (isset($stylesheets)) {
        foreach ($stylesheets as $stylesheet) {
            ?>
            <link href="<?= ASSETS_PATH ?>css/<?= $stylesheet ?><?= $css_version ?>" rel="stylesheet" type="text/css" >
            <?php
        }
    }
?>

<script src="<?= ASSETS_PATH ?>js/latest-jquery-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/date.format.js"></script>

<script src="<?= ASSETS_PATH ?>profile/modernizr.js"></script>

<!--Social share script-->
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "8ec9d9ef-e855-403c-8073-cabf2bde9edd", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<!--/End Social share script-->



<style type="text/css">
    .icon-chevron-left {
        color: #96C134 !important;
    }

    .icon-chevron-right {
        color: #96C134 !important;
    }
</style>
<?php
if($controller_name != 'publicprofile' && !isset($public_recommendation))
{
?>
<script>
    setInterval(function(){
        $.ajax({
            url:'<?=$vObj->getURL("login/check_login")?>',
            success:function(data){
                if(data != true){
                    window.location.replace("<?=$vObj->getURL("login")?>");
                }
            }
        });
    },5000);
</script>
<?php
}
?>

<script>
    /*.team-first div - row or row-fluid change on Window Resize*/
    $(document).ready(function () {
        var three_images_height = $(".full-profile_overlay").next('.row-fluid').height();
        $(".full-profile_overlay").css('height',three_images_height);
        var three_images_html = $(".full-profile_overlay").next('.row-fluid').html();
        $(".tyroe_score_over_img").css('height',$(".showcase_thumb").height());
        $(window).resize(function(){
            var three_images_height = $(".full-profile_overlay").next('.row-fluid').height();
            $(".full-profile_overlay").css('height',three_images_height);
            var three_images_html = $(".full-profile_overlay").next('.row-fluid').html();
            $(".tyroe_score_over_img").css('height',$(".showcase_thumb").height());
        });
        $.ajax({

        });
       // Switch slide buttons
        $('.slider-button').click(function () {
            if ($(this).hasClass("on")) {
                $(this).removeClass('on').html($(this).data("off-text"));
            } else {
                $(this).addClass('on').html($(this).data("on-text"));
            }
        });
        window.row_fluid_row = "";
        var row_fluid_row_func = function () {
            var default_window_size = $(window).width();
            if (($(window).width() >= 0) && (default_window_size <= 767)) {
                window.row_fluid_row = "row";
                $(".team-first").removeClass('row-fluid').addClass('row');
            } else if (default_window_size > 767) {
                window.row_fluid_row = "row-fluid";
                $(".team-first").removeClass('row').addClass('row-fluid');
            }
        }
        row_fluid_row_func();
        $(window).resize(function () {
            row_fluid_row_func();
        });


        /*Mobile Menu Toggler - Start*/
        var $menu = $("#sidebar-nav");
        $("body").click(function () {
            if ($menu.hasClass("display")) {
                $menu.removeClass("display");
            }
        });
        $menu.click(function (e) {
            e.stopPropagation();
        });
        $("#menu-toggler").click(function (e) {
            e.stopPropagation();
            $menu.toggleClass("display");
        });
        /*Mobile Menu Toggler - End*/
    });
</script>
<script>
    function multiplechecks(master_check, checkboxes) {
        var is_checked = null;
        var checked_elez = $(checkboxes);
        var is_checked = master_check.is(':checked');

        if (is_checked == true) {
            checked_elez.prop('checked', true);
        } else {
            checked_elez.prop('checked', false);
        }
        return;
        if (master_check.checked) { // check select status
            $(checkboxes).each(function () { //loop through each checkbox
                var each_ele = $(this);
                each_ele.attr('checked', 'checked');
                checkboxes.checked = true;  //select all checkboxes with class "param2"
            });
        } else {
            $(checkboxes).each(function () { //loop through each checkbox
                checkboxes.checked = false; //deselect all checkboxes with class "param2"
            });
        }
    }
</script>
<script>
    //function for profile iframes
    var callback = 1;
    function open_dropdown() {
        if (callback == 1) {
            $('.email_sending_btns .dropdown-menu').show();
            callback = 0;
        } else {
            $('.email_sending_btns .dropdown-menu').hide();
            callback = 1;
        }
    }
    function add_in_opening_jobs(iframe_job_id, iframe_tyroe_id) {
        /*First Param =  JOB ID*/
        /*Second Param =  Tyroe ID*/
        $('.email_sending_btns .dropdown-menu .add_job_anch' + iframe_job_id).addClass('disable_opening');
        $('.email_sending_btns .dropdown-menu .add_job_anch' + iframe_job_id).removeClass('add_in_opening_jobs');

        var job_id = iframe_job_id;
        var tyroe_id = iframe_tyroe_id;
        $.ajax({
            type: "POST",
            url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
            dataType: "json",
            data: {job_id: job_id, tyroe_id: tyroe_id, search_param: 'add_by_search'},
            success: function (data) {
                if (data.success) {
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    }
    function favourite_profile(iframe_tyroe_id) {
        var uid = iframe_tyroe_id;
        var data = "uid=" + uid;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".fav_profile").css("background","#2388d6");
                    $(".fav_profile_star").css("color","#ffffff");
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".fav_profile").css("background","#ffffff");
                    $(".fav_profile_star").css("color","#2388d6");
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
            },
            failure: function (errMsg) {
            }
        });
    }

    function generate_number_id(number){
        var user_num = number;
        var new_number = user_num;
        var number_zeroes = '0000';
        var job_id_length = user_num.length;
        if(job_id_length <= 4){
            new_number = number_zeroes.substr(0,4-job_id_length);
            new_number = new_number+user_num;
         }else{
            new_number = number;
         }
        return new_number;
    }


    function hide_search_single(iframe_tyroe_id) {
        var tyroe_id = iframe_tyroe_id;
        $.ajax({
            type: "POST",
            data: {tyroe_id: tyroe_id},
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".row-id" + tyroe_id).hide(500);
                    $(".editor-seperator-row" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                    //$("#error_msg").html(data.success_message);
                }
                $('.listview_btn').trigger('click');
                $('.hiderow' + tyroe_id).remove();
            }
        });
    }

    //profile view adjustment in search controller , favourite controller , job applicaint , candidate , shortlist
    <?php if ($controller_name == "search" || $controller_name == "openingoverview"  || $controller_name == "favourite"   || $controller_name == "featured" ){?>
    jQuery(window).bind('resize', function () {
        var profile_height = jQuery('#profile_iframe').contents().find('html body .iframe-body').height();
        jQuery('#profile_iframe').height(profile_height + 'px');
        jQuery('#profile_iframe').css({"border": "0"});
        jQuery('.loader-save-holder').hide();
    });
    jQuery(window).scroll(function () {
        var profile_height = jQuery('#profile_iframe').contents().find('html body .iframe-body').height();
        jQuery('#profile_iframe').height(profile_height + 'px');
        jQuery('#profile_iframe').css({"border": "0"});
    });
    jQuery(document).on('mouseover','#profile_iframe',function () {
        var profile_height = jQuery('#profile_iframe').contents().find('html body .iframe-body').height();
        jQuery('#profile_iframe').height(profile_height + 'px');
        jQuery('#profile_iframe').css({"border": "0"});
    });
    function initFrame() {
        resizeAppFrame();
        if (navigator.userAgent.indexOf("MSIE") > 0)
            $(function () {
                $(appFrame.document.body).resize(resizeAppFrame)
            }) //IE
        else{
            document.getElementById("profile_iframe").contentWindow.addEventListener("overflow", resizeAppFrame, false);  //FireFox
        }

    }
    function resizeAppFrame() {
        $("#profile_iframe").height($("#profile_iframe").contents().find("body").outerHeight(true));
    }
    <?php } ?>
    function resetPopupForm(){
        $('div.modal').find('input[type="text"]').val('').css({border:""});
        $('div.modal').find('select option:first-child').attr("selected", true);
        $('div.modal').find('textarea').val('').css({border:""});
        $('div.modal').find('input[type="checkbox"]').attr({checked:false});
        $('div.modal').find('input[type="radio"]').first().prop("checked", true);
        $('div.modal').find('.sugg-msg').hide();
        $('div.modal').find('.email_close').hide();
        $('div.modal').find('.btn-send-email').show();
        $('div.modal').find('.email-message').css({border:""});
        $('div.modal').find('#email-subject').css({border:""});
        $('div.modal').find('.error_modal_div').addClass('hidden');
        $('div.modal').find('input[name="job_tags"]').select2("val", "");
        $('div.modal').find('.review_score_run_time').text('50');
        $('div.modal').find('.slider-technical').slider({value:50});
        $('div.modal').find('.slider-creative').slider({ value:50 });
        $('div.modal').find('.slider-impression').slider({ value:50});
        $('div.modal').find('.review_back').trigger('click');
        $('div.modal').find('#reviewer_container .hideOnclose').remove();
        $('body').removeClass('modal-open');
        $('body').css({"overflow":"auto"});
    }
    $(document).on('click','button.close, a.close, .job_close, .email_close, .review_close',function(){
        resetPopupForm();
    });
    $(document).on('click','.close2,.white-close',function(){
        $('body').removeClass('modal-open');
        $('body').css({"overflow":"auto"});
    })
    $(document).on('click','#btn_widgets_and_sharing,#btn_team_members,#edit_job,.job_post,.btn-create-job,.btn-msg,.open_modal,.edit_reviewer',function(){
        $('body').css({"overflow":"hidden"});
        $('body').addClass('modal-open');
    });
    //these commtented function for score hovering
    /*$(document).on('mouseover','.score_img_anchor0',function(){
        var tyroeDataId = $(this).data('id');
       $(this).find('.tyroeScoreHolder'+tyroeDataId).fadeIn();
    });
    $(document).on('mouseleave','.score_img_anchor0',function(){
        var tyroeDataId = $(this).data('id');
        $(this).find('.tyroeScoreHolder'+tyroeDataId).fadeOut();
    });
    $(document).on('mouseover','.score_img_anchor1, .score_img_anchor2',function(){
        var tyroeDataId = $(this).data('id');
        $('.black_overlay_separate'+tyroeDataId).css({"display":"table","opacity":"1"})
    });
    $(document).on('mouseleave','.editors_showcase_holder',function(){
        $('.full-profile_overlay').css({"display":"none","opacity":"0"})
    });*/
    function jQueryCleanString(string){
        return string.replace(/[^\w\s]/gi, '');
    }
    //replace text
    function replaceAll(find, replace, str){
        return str.split(find).join(replace);
     // return str.replace(new RegExp(find, 'g'), replace);
    }
    //for profile view next previous button stucking color
    $(document).on('click mouseout','.single_back,.single_next',function(){
        $(this).css({"background":"#ffffff"}).find('i').css({"color":"#2388D6"});
    });
    $(document).on('mouseover','.single_back,.single_next',function(){
        $(this).css({"background":"#2388D6"}).find('i').css({"color":"#ffffff"});
    });
    $(document).on('mouseleave','.searchReviewArea',function(){
        $('.searching_reviewer_result').hide();
    })
</script>
<script type="text/javascript">

    jQuery(document).ready(function () {
        $.fn.extend({
            donetyping: function (callback, timeout) {
                timeout = timeout || 1e5; // 1 second default timeout
                var timeoutReference,
                        doneTyping = function (el) {
                            if (!timeoutReference) return;
                            timeoutReference = null;
                            callback.call(el);
                        };
                return this.each(function (i, el) {
                    var $el = $(el);
                    $el.is(':input') && $el.keydown(function () {
                        if (timeoutReference) clearTimeout(timeoutReference);
                        timeoutReference = setTimeout(function () {
                            doneTyping(el);
                        }, timeout);
                    }).blur(function () {
                                doneTyping(el);
                    });
                });
            }
        });
    });
</script>


</head>
<body>
<input type="hidden" id="site_base_url" value="<?=LIVE_SITE_URL?>"/>
<input type="hidden" id="full_width_image_path" value="<?=ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE?>"/>
<!--<div class="notification-box">
    <div class="notification-box-message"></div>
</div>-->
<div class="row-fluid notification-box" id="publish_notify">
    <div class="span12">
        <div class="alert alert-info text-center">
            <i class="icon-exclamation-sign"></i>
            <span class="notification-box-message"></span>
        </div>
    </div>
</div>
<?php
if($pending_moderation_review_count > 0 && ($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4)) { ?>
<div class="row-fluid" id="pending_review_alert">
    <div class="span12">
        <div class="alert alert-error text-center" style="margin-bottom:0px">
            <i class="icon-exclamation-sign"></i>
            <span class="text-center text-danger">
                You have important candidate feedback that requires your approval.&nbsp;&nbsp;
                <a href="<?= base_url() ?>moderation" class="btn btn-default btn-danger">Review Now</a>
            </span>
        </div>
    </div>
</div>
<?php } ?>
<?php
//If admin see a user profile and it is not publish, this message will be show
if( ($controller_name == 'publicprofile') && ($is_message_show == 1) ){
    ?>
    <div class="row-fluid" id="publish_notify" style="z-index: 9999999; display: block; position: fixed; top: 0px;">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                <?= UNPUBLISH_PROFILE_MESSAGE_FOR_ADMIN ?>
            </div>
        </div>
    </div>
    <?php
}
?>

<!-- navbar -->
<?php
if ($controller_name != "publicprofile") {
    if (($controller_name != "profile") && ($vObj->publish_or_not() == 0) && $this->session->userdata('role_id') == TYROE_FREE_ROLE) {
        ?>
        <div class="row-fluid" id="publish_notify">
            <div class="span12">
                <div class="alert alert-info text-center">
                    <i class="icon-exclamation-sign"></i>
                    <?= UNPUBLISH_PROFILE_MESSAGE ?>
                    <a href="javascript:void(0);" class="remove-publish-notify"><i
                            class="icon-remove icon-remove-profilemessage"></i> </a>
                </div>
            </div>
        </div>
    <?php
    }
    if (($controller_name != "profile") && ($this->session->userdata('role_id') == TYROE_FREE_ROLE) && ($this->session->userdata('recommendation_notification') > 0)) {
        ?>
        <div class="row-fluid" id="recommendation_notify">
            <div class="span12">
                <div class="alert alert-info text-center">
                    <i class="icon-exclamation-sign"></i>
                    <?= RECOMMNENDATION_RECIEVED_MESSAGE ?>
                    <a href="javascript:void(0);" class="remove-publish-notify"><i
                            class="icon-remove icon-remove-recommendation"></i> </a>
                </div>
            </div>
        </div>
    <?php }
    if (($controller_name != "profile") && ($this->session->userdata('role_id') == TYROE_FREE_ROLE) && ($this->session->userdata('latest_jobs') > 0)) { ?>
        <div class="row-fluid" id="latestjobs_notify">
            <div class="span12">
                <div class="alert alert-info text-center">
                    <i class="icon-exclamation-sign"></i>
                    <?= str_replace(0, $this->session->userdata('latest_jobs'), LATEST_JOB_MESSAGE) ?>
                    <a href="javascript:void(0);" class="btn-flat btn-remove-newjob-messages">View</a>
                    <a href="javascript:void(0);" class="remove-publish-notify"><i
                            class="icon-remove icon-remove-newjob-messages"></i> </a>
                </div>
            </div>
        </div>
    <?php }
    if (($controller_name != "profile") && ($this->session->userdata('role_id') == TYROE_FREE_ROLE) && ($this->session->userdata('latest_shortlist_jobs') > 0)) { ?>
    <div class="row-fluid" id="latestshortlistjobs_notify">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                <?= str_replace(0, $this->session->userdata('latest_shortlist_jobs'), LATEST_SHORTLIST_JOB_MESSAGE) ?>
                <a href="javascript:void(0);" class="btn-flat btn-remove-shortlisjob-messages">View</a>
                <a href="javascript:void(0);" class="remove-publish-notify"><i
                        class="icon-remove icon-remove-shortlistjob-messages"></i> </a>
            </div>
        </div>
    </div>
    <?php 
    
    }
    if ($this->session->userdata('role_id') == STUDIO_ROLE && $this->session->userdata('pending_reviewer') != 0 && $controller_name != "team") { ?>
        <!-- <div class="row-fluid" id="latestjobs_notify">
            <div class="span12">
                <div class="alert alert-info text-center">
                    <i class="icon-exclamation-sign"></i>
                    You have <?= $this->session->userdata('pending_reviewer') ?> reviewer in pending
                    <a href="javascript:void(0);" class="active_reviewer_notification"><i
                            class="icon-remove remove-blue-msg"></i> </a>
                </div>
            </div>
        </div> -->
    <?php } ?>
    <?php if($unWantedApplier != ''){ ?>
        <div class="row-fluid" style="z-index: 9999999; display: block; position: fixed; top: 0px;">
                <div class="span12">
                    <div class="alert alert-info text-center">
                        <i class="icon-exclamation-sign"></i>
                        <?= $unWantedApplier ?>
                    </div>
                </div>
            </div>
    <?php } ?>
    <?php if ($controller_name == "jobopening") { ?>
    <div class="row-fluid" id="hire-notify">
        <div class="span12">
            <div class="alert alert-info text-center hirejobs_message">
            </div>
        </div>

    </div>
    <?php } ?>
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <button type="button" class="btn btn-navbar visible-phone custom-hide" id="menu-toggler">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="<?= LIVE_SITE_URL ?>"><img class="logo-main"
                                                              src="<?= ASSETS_PATH ?>img/logo_tyroe.png"></a>
            <ul class="nav pull-right">
                <?php
                if ($this->session->userdata('logged_in')) {
                    ?>
                    <li class="hidden-phone">

                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle hidden-phone" data-toggle="dropdown">
                            <span id="fn_ln">
                            <?php
                                if( $this->session->userdata('firstname') == '' && $this->session->userdata('lastname') == '' ){
                                    if($this->session->userdata("user_name")!=""){
                                        echo $this->session->userdata("user_name");
                                    } else {
                                        echo "Name Not Available";
                                    }
                                } else {
                                    echo $this->session->userdata('firstname') . " " . $this->session->userdata('lastname');
                                }
                            #display username or email
                            ?>
                            </span>
                            <b class="caret"></b>
                        </a>
                        <?php
                        if ($this->session->userdata('role_id') == TYROE_FREE_ROLE) {
                            ?>
                            <ul class="dropdown-menu">
                                <li><a href="<?= $vObj->getURL('account') ?>">Account settings</a></li>
                                <li><a href="<?= $vObj->getURL('profile') ?>">Edit Profile</a></li>
                            </ul>
                        <?php
                        } else if( ($this->session->userdata('role_id') == REVIEWER_ROLE) || ($this->session->userdata('role_id') == STUDIO_ROLE) ) {
                        ?>
                            <ul class="dropdown-menu">
                                <li><a href="<?= $vObj->getURL('account') ?>">Account settings</a></li>
                            </ul>
                        <?php } ?>
                    </li>
                    <li class="settings hidden-phone">
                        <a target="_blank" role="button" href="http://support.tyroe.com">
                            <i class="icon-question-sign"></i>
                        </a>
                    </li>
                    <?php
                    if (isset($system_modules['top_menu'])) {

                        foreach ($system_modules['top_menu'] as $k => $v) {

                            ?>
                           <!-- <li class="settings hidden-phone">
                                <a href="<?php/*= $vObj->getURL($v['link']) */?>" role="button" target="_blank">
                                    <i class="<?php/*= $v['module_img'] */?>"></i>-->

                                </a>

                            </li>

                        <?php
                        }
                    }
                    ?>
                    <li class="settings hidden-phone">
                        <a href="<?= $vObj->getURL('logout') ?>" role="button">
                            <?php
                            if ($controller_name == "profile") {
                                ?>
                                <i class="icon-chevron-right"></i>
                            <?php
                            } else {
                                ?>
                                <i class="icon-share-alt"></i>
                            <?php
                            }

                            ?>

                        </a>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <!-- end navbar -->
<?php
}
?>
<?php
if ($this->session->userdata('logged_in') && $controller_name != "publicprofile") {
    ?>
    <!-- sidebar -->
    <div id="sidebar-nav">
        <button type="button" class="btn-navbar btn-toggler bth-hide" id="menu-toggler">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <ul id="dashboard-menu">
            <?php
            if($controller_name == 'moderation')$controller_name = 'home';
            if (isset($system_modules['side_menu'])) {
                foreach ($system_modules['side_menu'] as $k => $v) {
                    if ($controller_name == "resume" || $controller_name == "portfolio" || $controller_name == "social" || $controller_name == "profile" ) {
                        $controller_name = "profile";
                    } else if ($controller_name == "user") {
                        $controller_name = "user/listing";
                    } else if ($controller_name == "studios") {
                        $controller_name = "studios/listing";
                    } else if ($controller_name == "configuration") {
                        $controller_name = "configuration/system_variables";
                    } else if ($controller_name == "stream") {
                        $controller_name = "activity-stream";
                    } else if ($controller_name == "openingoverview") {
                        $controller_name = "jobopening";
                    } else if ($controller_name == 'activity-stream') {
                        if($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4)
                        {
                            $controller_name = "activity-stream";
                        } else {
                            $controller_name = "feedback";
                        }
                    }

                    if ($v['link'] == "jobopening") {
                        $v['link'] = "openings";
                    }
                    ?>
                    
                    <?php
                    //echo $v['link'] . "<br>" .  $controller_name;
                    if($controller_name == "jobopening") {
                    ?>
                        <li <?php if ($v['link'] == "openings") { ?>class="active"<?php } ?>>
                            <?php if ($v['link'] == "openings") { ?>
                                <div class="pointer">
                                    <div class="arrow" id="left-arrow-pointer"></div>
                                    <div class="arrow_border" id="left-arrow-pointer-back"></div>
                                </div>
                            <?php
                                if ($v['link'] == "jobopening") {
                                    $v['link'] = "openings";
                                }
                            }
                            ?>
                            <?php
                            if ($v['link'] == "payment") {
                               $v['module_title'] = "Payment";
                            }
                            ?>
                            <a href="<?= $vObj->getURL($v['link']) ?>">
                                <i class="<?php echo $v['module_img'] ?>"></i>
                                <span><?php echo $v['module_title']; ?></span>
                            </a>
                        </li>
                    <?php } else { ?>
                        <li <?php if ($v['link'] == $controller_name) { ?>class="active"<?php } ?>>
                            <?php if ($v['link'] == $controller_name) { ?>
                            <div class="pointer">
                                <div class="arrow" id="left-arrow-pointer"></div>
                                <div class="arrow_border" id="left-arrow-pointer-back"></div>
                            </div>
                                <?php
                                if ($v['link'] == "jobopening") {
                                    $v['link'] = "openings";
                                }
                                
                            }
                            ?>
                            
                            <?php
                            if ($v['link'] == "payment") {
                               $v['module_title'] = "Payment";
                            }
                            ?>
                            <a href="<?= $vObj->getURL($v['link']) ?>">
                                <i class="<?php echo $v['module_img'] ?>"></i>
                                <span><?php echo $v['module_title']; ?></span>
                            </a>
                        </li>
                    <?php } ?>
                <?php
                } }
            ?>
        </ul>
    </div>
    <!-- end sidebar -->
<?php
}

if ($controller_name == "publicprofile" and $hide_header == '') {
    ?>

    <header>
        <div class="container">
            <div class="row">
                <div class="span7">
                </div>

                <div class="span5">
                    <!-- Mobile Menu -->
                    <!--<a href="javascript:void(0);" class="contact-top-ico pull-right"><i class="icon-envelope-alt"></i></a>-->
                    <a href="#menu-nav" class="menu-nav" id="mobile-nav"></a>

                    <!-- Standard Menu -->
                    <nav id="menu">
                        <ul id="menu-nav" class="sf-js-enabled sf-arrows">
                            <li class=""><a href="#intro-box">Portfolio</a></li>
                            <li class=""><a href="#latest-work">Resume</a></li>
                            <li class=""><a href="javascript:void(0);" class=""><i class="icon-envelope-alt"></i></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
<?php
}
?>