<div id="main-stats">
       <div class="row-fluid stats-row" id="stats-padding">
           <div class="span12">
               <div class="span3 stat first">

                   <div class="data">
                       <span class="number"><?php echo $reviewer_viewed['cnt']; ?></span>
                       VISITS
                   </div>
                   <span class="date">This month</span>
               </div>
               <div class="span3 stat">
                   <div class="data">

                       <span class="number"><?php echo $studio_viewed['cnt']; ?></span>
                       COMPANIES
                   </div>
                   <span class="date">Viewed your work</span>
               </div>
               <div class="span3 stat">
                   <div class="data">
                       <span class="number"><?php echo $invited_openings['cnt']; ?></span>
                       JOB OPENINGS
                   </div>
                   <span class="date">in Progress</span>
               </div>
               <div class="span3 stat last">
                   <div class="data">
                       <span class="number"><?php echo $reviews['cnt']; ?></span>
                       REVIEWS
                   </div>
                   <span class="date">This month</span>
               </div>
           </div>

       </div>
   </div>