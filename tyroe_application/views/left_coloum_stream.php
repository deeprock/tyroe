<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<div class="span4">
    <form action="">
    <div class="row-fluid header-side">
        <div class="span8">
            <h4>Filters</h4>
        </div>
        <div class="span4">
            <!--<a class="btn-flat gray">CLEAR</a>-->
            <a value="CLEAR" class="btn-flat gray" id="clear_btn">CLEAR</a>
           <!-- <input type="reset" value="CLEAR" class="reset btn-flat gray" >-->
        </div>
    </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">

                    <div class="field-box">
                        <div class="ui-select find">
                            <select class="span5 inline-input" name="activity_type"
                                    id="activity_type" onchange="FilterStreams();">
                                <option value="">Select Type</option>
                                <?php
                                foreach ($get_activity_type as $key => $activity_type) {
                                    ?>
                                    <option
                                        value="<?php echo $activity_type['activity_type_id'] ?>"><?php echo $activity_type['activity_title'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

            </div>
        </div>
    </div>
<br>
    <div class="row-fluid">
           <div class="span12">
               <div class="container span12">
                       <div class="field-box">
                                <div class="keyword-textbox">
                               <p><input class="span11" type="text" name="stream_keyword" id="stream_keyword" placeholder="Keywords" onkeypress="FilterStreams();"></p>
                                </div>
                       </div>
               </div>
           </div>
       </div>

    <div class="row-fluid">
        <div class="span12">
            <div class="container span12">
                    <div class="field-box">
                        <div class="span9" style="margin-top: 10px;">
                            <a href="javascript:void(0);" class="btn-flat gray search_by">This Week</a>
                            <a href="javascript:void(0);" class="btn-flat gray search_by">This Month</a>
                            <a href="javascript:void(0);" class="btn-flat gray search_by">All</a>
                            <input type="hidden" name="stream_duration" id="stream_duration" value="">
                        </div>
                    </div>
            </div>
        </div>
    </div>
    </form>

</div>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $(".search_by").on("click", function () {
         if (this.text == "This Week") {
            $("#stream_duration").val("week");
         }
            else if(this.text == "This Month"){
             $("#stream_duration").val("month");
         }
         else if(this.text == "All"){
             $("#stream_duration").val("all");
         }
            FilterStreams();

            });

        $( "select" ).on("change",function () {
            var str = "";
            $( "select option:selected" ).each(function() {
              str += $( this ).text() + " ";
            });
                if($( this ).val()!=""){
            $( ".activity_type_label" ).text( str );
                }
          })
//.change();

          /*Clear button */
        $("#clear_btn").click(function(){
            $(this).closest('form').find("input[type=text], textarea,select").val("");
            FilterStreams();
            });

          });

    function FilterStreams() {

        var activity_type=$("#activity_type").val();
        var stream_keyword=$("#stream_keyword").val();
        var stream_duration=$("#stream_duration").val();
        var data = "activity_type=" + activity_type + "&stream_keyword=" +stream_keyword+ "&stream_duration=" +stream_duration;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("stream/FilterStream")?>',

            success: function (data) {
                $(".no-filter").hide();
                $(".filter-result").html(data);
                /*if (data.success) {

                    $(".no-filter").hide();
                    $(".filter-result").html(data.success_message);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }*/
            }

        });
    }
</script>