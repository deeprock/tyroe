<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>

<div class="span4 job_form">
    <!--<form action="" class="job_form">-->
        <div class="row-fluid header-side">

            <div class="span8">
                <h4>Filters</h4>
            </div>
            <div class="span4">
                <!--<a class="btn-flat gray">CLEAR</a>-->
                <a value="CLEAR" class="btn-flat gray pull-right" id="clear_btn">CLEAR</a>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12">

                    <div class="btn-group opening-btn-group span12 tab-margin">
                        <button onchange="FilterJobs();" class="glow left search_by" data-value="0">All</button>
                        <button class="glow search_by candidate-filter-btn" data-value="1">Candidate</button>
                        <button class="glow right search_by" data-value="2">Shortlisted</button>

                        <input type="hidden" name="job_filter" id="job_filter" value="">
                    </div>
            </div>
        </div>
        <div class="separator t-magic"></div>
        <div class="row-fluid">
            <div class="span12 margin-adjust">
                <div class="container span12 ">

                    <div class="field-box">

                        <div class="ui-select find">
                            <select class="span5 inline-input" name="country_id"
                                    id="country_id" onchange="GetCities('country_id'); FilterJobs('','empty');" >
                                <option value="">Select Country</option>
                                <?php
                                foreach ($get_countries as $key => $countries) {
                                    $selected ="";
                                    if($countries['id'] == $country_id)
                                    {
                                        $selected = 'selected="selected"';
                                    }
                                    ?>
                                    <option <?=$selected?>
                                        value="<?php echo $countries['id'] ?>"><?php echo $countries['name'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>

                    </div>

                </div>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12 margin-adjust">
                <div class="container span12">

                    <div class="field-box">
                        <div class="result-cities"></div>
                        <div class="ui-select find all-cities">
                            <select class="span5 inline-input" name="job_city"
                                    id="job_city" onchange="FilterJobs();">
                                <option value="">Select City</option>
                                <?php
/*                                foreach ($get_job_cities as $key => $job_cities) {
                                    */?><!--
                                    <option
                                        value="<?php /*echo $job_cities['job_city'] */?>"><?php /*echo $job_cities['job_city'] */?></option>
                                --><?php
/*                                }
                                */?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 margin-adjust">
                <div class="container span12">

                    <div class="field-box">
                        <div class="ui-select find">
                            <select class="span5 inline-input" name="creative_skill"
                                    id="creative_skill" onchange="FilterJobs();">
                                <option value="">Select Skills</option>
                                <?php
                                foreach ($get_creative_skills as $key => $creative_skills) {
                                    $selected ="";
                                    if($creative_skills['creative_skill_id'] == $creative_skill)
                                    {
                                        $selected = 'selected="selected"';
                                    }
                                    ?>
                                    <option <?=$selected?>
                                        value="<?php echo $creative_skills['creative_skill_id'] ?>"><?php echo $creative_skills['creative_skill'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>


                </div>
            </div>
        </div>

        <div class="row-fluid">
            <div class="span12 margin-adjust">
                <div class="container span12">

                    <div class="field-box">
                        <div class="ui-select find">
                            <select class="span5 inline-input" name="job_level_id"
                                    id="job_level_id" onchange="FilterJobs ();">
                                <option value="">Select Level</option>
                                <?php
                                foreach ($get_job_level as $key => $job_level) {
                                    $selected ="";
                                    if($job_level['job_level_id'] == $skill_level_id)
                                    {
                                        $selected = 'selected="selected"';
                                    }
                                    ?>
                                    <option
                                        value="<?php echo $job_level['job_level_id'] ?>"><?php echo $job_level['level_title'] ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row-fluid">
                   <div class="span12 margin-adjust">
                       <div class="container span12">

                           <div class="field-box">
                               <div class="ui-select find">
                                   <select class="span5 inline-input" name="job_type"
                                           id="job_type" onchange="FilterJobs();">
                                       <option value="">Contract Type</option>

                                       <?php
                                       foreach ($get_job_type as $key => $job_types) {
                                           $selected ="";
                                           if($job_types['job_type_id'] == $job_type)
                                           {
                                               $selected = 'selected="selected"';
                                           }
                                           ?>
                                           <option <?=$selected?>
                                               value="<?php echo $job_types['job_type_id'] ?>"><?php echo $job_types['job_type'] ?></option>
                                       <?php
                                       }
                                       ?>
                                   </select>
                               </div>
                           </div>


                       </div>
                   </div>
               </div>

        <div class="row-fluid">
            <div class="span12">
                <div class="container span12">

                    <div class="field-box opening-left-support-box">

                        <input type="text" id="start_date" name="start_date" placeholder="Start Date" class="span12" value="<?=$start_date?>"> <!--<img id="img_date"
                                                                                                 src="<?php /*echo ASSETS_PATH */?>img/date-picker-icon.png"/>-->

                       <!-- <p>
                            <span class="span3"> Finish Before</span>
                            <input type="text" id="end_date" name="end_date" class="span6"> <img id="img_date"
                                                                                                 src="<?php /*echo ASSETS_PATH */?>img/date-picker-icon.png"/>
                        </p>-->

                        <p>
                            <input type="text" id="opening_keyword" name="opening_keyword" value="<?=$keyword?>"
                                   placeholder="Keywords" class="span12" onkeyup="FilterJobs ();">
                        </p>
                    </div>

                </div>
            </div>
            <!--Modules of Opening-->


            <!--Modules of Opening-->
        </div>
      <!--  <div class="row-fluid" id="side-list">
            <?php
/*            /*                               if (isset($system_modules['navigation_hotspot'])) {
                                               foreach ($system_modules['navigation_hotspot'] as $k => $v) {*/
            ?>

            <div class="row-fluid  exposure_filter"> <?php /*//if ($v['link'] == $controller_name . $method_name) {
            // echo "id='active'";
            // } else '';
            */?>
            <div class="span12">
                <a onclick="FilterJobs('intresting_filter')"
                   class="name"><?php/*= LABEL_INTERESTING_ROLES_MODULE */?></a>
                <span class="subtext"><?php/*= LABEL_INTERESTING_ROLES_SUB_TEXT */?></span>
            </div>
            </div>
            <div class="row-fluid  exposure_filter">
        <div class="span12">
            <a class="name" onclick="FilterJobs('candidate_filter')"><?php/*= LABEL_CANDIDATE_ROLES_MODULE */?></a>
            <span class="subtext"><?php/*= LABEL_CANDIDATE_ROLES_SUB_TEXT */?></span>
        </div>
    </div>
            <div class="row-fluid  exposure_filter"> <?php /*//if ($v['link'] == $controller_name . $method_name) {
// echo "id='active'";
// } else '';
*/?>
<div class="span12">
    <a onclick="FilterJobs('shortlist_filter')"
       class="name"><?php/*= LABEL_SHORTLISTED_ROLES_MODULE */?></a>
    <span class="subtext"><?php/*= LABEL_SHORTLISTED_ROLES_SUB_TEXT */?></span>


</div>
</div>
<?php
/*   }
}*/
?>

</div>-->

<!--</form>-->
</div>

<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function(){
        $(".search_by").on("click", function () {
                    if ($(this).attr("data-value") == "1") {
                        $("#job_filter").val("invite");
                    }
                    else if ($(this).attr("data-value") == "2") {
                        $("#job_filter").val("shortlist");

                    }
                    else if ($(this).attr("data-value") == "0") {
                        $("#job_filter").val("");
                    }
            FilterJobs();

                });
        //activate selected link
        $('.exposure_filter').on('click',function(){
            $(".exposure_filter").removeAttr('id', 'active');
            $(this).attr('id', 'active');
        });
        /*Clear button */
        $("#clear_btn").click(function(){
            $(this).closest('.job_form').find("input[type=text], textarea,select").val("");
            FilterJobs();
            });
          });
    /*$('#opening_keyword').keyup(function(e){if(e.keyCode == 8)
        FilterJobs();
        })*/

    function GetCities(ddname) {
           $('#job_city').html('<option value="">Select City</option>');
            var country_name = $("#country_id option:selected").val();
            var data = "country=" + country_name + "&ddname=" + ddname;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("profile/get_cities_by_country")?>',
                success: function (data) {
                    //$(".all-states").hide();
                    if(data != "")
                    {
                        $(".all-cities").remove();
                        $(".result-cities").html("");
                        $(".result-cities").html(data);
                    }
                    else
                    {
                        $('#job_city').html('<option value="">Select City</option>');
                    }
                }
            });
        }

    function FilterJobs(click_type,city) {
        var click_type = click_type;

        if(typeof click_type === "undefined"){
            /*This is for filter dd*/
            click_type = $("#for_filter").val();
            /*This is for filter dd*/
        }
        var query_var = click_type;
        var url = '';
        if(click_type == 'candidate_filter'){
            query_var = click_type;
            url = '<?=$vObj->getURL("jobopening/FilterExposure")?>';
        } else if(click_type == 'intresting_filter') {
            query_var = click_type;
            url = '<?=$vObj->getURL("jobopening/FilterExposure")?>';
        } else if(click_type == 'shortlist_filter'){
            query_var = click_type;
            url = '<?=$vObj->getURL("jobopening/FilterExposure")?>';
        } else {
            url = '<?=$vObj->getURL("jobopening/FilterJobs")?>';
        }
        var orderby = $("#orderby").val();
        var skill_level_id = $("#job_level_id").val();
        var job_city = $("#job_city option:selected").text();
        var creative_skill = $("#creative_skill").val();
        var country_id = $("#country_id").val();
        var keyword = $("#opening_keyword").val();
        var end_date = $("#end_date").val();
        var start_date = $("#start_date").val();
        var job_type = $("#job_type").val();
        var job_filter = $("#job_filter").val();

        if(city == 'city'){
            job_city = '';
        }
        var data = "creative_skill=" + creative_skill + "&country_id=" + country_id + "&skill_level_id="
                + skill_level_id+"&orderby="+orderby+"&job_city="+job_city+"&keyword="+keyword+"&start_date="
                +start_date+"&end_date="+end_date+"&query_var="+query_var+"&job_type="+job_type+"&job_filter="+job_filter;
        $.ajax({
            type: "POST",
            data: data,
            url: url,
            success: function (data) {
                $(".no-filter").hide();
                var data_arr = data.split('<!--|||-->');
                $(".filter-result").html(data_arr[0]);
                $('#footer_pagination').html(data_arr[1]);
                init_read_more_job_description();
            }

        });
    }
</script>
<?php if($this->session->userdata('comapnyname') != ''){ ?>
    <script>
        jQuery(document).ready(function(){
            jQuery("#opening_keyword").val('<?= $this->session->userdata('comapnyname') ?>');
            FilterJobs ();
            //unset redirecting data
            <?php if($this->session->userdata('share_jobid') != ''){ ?>
                var jobid  =   '<?=$this->session->userdata('share_jobid')?>';
                var scrollpos =   jQuery('.job-opening'+jobid).position().top;
                jQuery('html,body').animate({scrollTop:scrollpos+'px'},'slow');
                jQuery('.job-opening'+jobid).fadeOut(400);
                jQuery('.job-opening'+jobid).fadeIn(1400);
            <?php } ?>
            <?php
                  $this->session->unset_userdata('redirect_to_job_overview');
                  $this->session->unset_userdata('comapnyname');
                  $this->session->unset_userdata('share_jobid');
             ?>
        });
    </script>
<?php } ?>