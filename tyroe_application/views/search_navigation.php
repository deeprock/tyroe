<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<script src="<?= ASSETS_PATH ?>js/jquery-1.9.1.min.js"></script>
<script src="<?= ASSETS_PATH ?>js/popup/popup.js"></script>


<script type="text/javascript">
    $(document).ready(function () {

        $(".batch-selection").on("click", function () {
            if (this.text == "Batch Contact") {

            } else if (this.text == "Batch Add to Job") {
                /* var chk_count = $("input[type=selected_tyroe_chk]").length;
                 if (chk_count <= 0) {
                 alert("Haven't select any user ");

                 }*/

            }
            else if (this.text == "Batch Remove") {

                var id_s = $('input[name="selected_tyroe_chk"]:checked');
                $.each(id_s, function (index, item) {
                    var tyroes_val = item.value;
                    $(".detailed-tyroe" + tyroes_val).show();
                    $(".tyroe" + tyroes_val).hide();
                });


            }
        })

    });
    function AddToJob() {
        var job_id = $("#job_id").val();
        var tyreo_arr = [];
        var id_s = $('input[name="selected_tyroe_chk"]:checked');
        $.each(id_s, function (index, item) {
            var tyroes_val = item.value;
            tyreo_arr.push(tyroes_val);
        });
        var tyroe_id = tyreo_arr.join(",");
        if (tyroe_id == "") {

        }
        if (job_id == "") {
            alert("Job Selection Required");
        } else {


            var data = "tyroe_id=" + tyroe_id + "&job_id=" + job_id;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
                dataType: "json",
                success: function (data) {
                    if (data.success) {
                        $(".detailed-tyroe" + tyroe_id).hide(500);
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                    else {
                        $(".notification-box-message").css("color", "#b81900")
                        $(".notification-box-message").html(data.success_message);
                        $(".notification-box").show(100);
                        setTimeout(function () {
                            $(".notification-box").hide();
                        }, 5000);
                    }
                },
                failure: function (errMsg) {
                }
            });
        }
    }
    function AddSelection(tyro_id) {
        $(".detailed-tyroe" + tyro_id).hide();
        $(".tyroe" + tyro_id).show();
    }

    function HideTyroe(tyroe_id) {
        var data = "tyroe_id=" + tyroe_id;
        $.ajax({
            type: "POST",
            data: data,
            url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
            dataType: "json",
            success: function (data) {
                if (data.success) {
                    $(".detailed-tyroe" + tyroe_id).hide(500);
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
                else {
                    $(".notification-box-message").css("color", "#b81900")
                    $(".notification-box-message").html(data.success_message);
                    $(".notification-box").show(100);
                    setTimeout(function () {
                        $(".notification-box").hide();
                    }, 5000);
                }
            },
            failure: function (errMsg) {
            }
        });


    }
    function ContactTyroe(tyro_id) {

    }

    function filter_tyroe(orderby){
    var country_id=$("#country_id").val();

   // var skill_id=$("#job_level_id").val();
    var creative_skill_id=$("#creative_skill").val();
        //var orderby='alphabet';
        var data = "orderby=" + orderby+"&country_id=" + country_id+"&creative_skill_id=" + creative_skill_id;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?=$vObj->getURL("search/FilterTyroes")?>',
                    /*dataType: "json",*/
                    success: function (data) {
                        /*$(".no-filter-tyroe").hide();
                                               $(".filter-result" ).show();*/
                                               $(".no-filter-tyroe").html(data);




                    }

                });

    }
</script>
<script type="text/javascript">
    // Semicolon (;) to ensure closing of earlier scripting
    // Encapsulation
    // $ is assigned to jQuery
    ;
    (function ($) {

        // DOM Ready
        $(function () {

            // Binding a click event
            // From jQuery v.1.7.0 use .on() instead of .bind()
            $('#my-button').bind('click', function (e) {

                // Prevents the default action to be triggered.
                e.preventDefault();

                // Triggering bPopup when click event is fired
                $('#element_to_pop_up').bPopup();

            });

        });

    })(jQuery);
</script>


<!-- main container -->
<!-- Element to pop up -->
<div id="element_to_pop_up">
    <div class="job-selection">
        <div class="field-box">
            <label>Select Job:</label>

            <div class="ui-select find">
                <select name="job_id" id="job_id">
                    <option value="">Select Job</option>
                    <?php
                    foreach ($get_jobs as $jobs) {
                        ?>
                        <option value="<?php echo $jobs['job_id']; ?>"><?php echo $jobs['job_title']; ?></option>
                    <?php
                    }
                    ?>
                </select></div>
            <div class="span9">
                <a class="btn-flat user-search" onclick="AddToJob();">SUBMIT</a>
            </div>
        </div>
    </div>

    <div class="contact-form" style="display: none">
        <div class="field-box">
            <label>Contact Form</label>


        </div>
    </div>
</div>
<div class="content">

<div class="container-fluid">


<div id="pad-wrapper">

<div class="grid-wrapper">

<div class="row-fluid head">
    <form class="new_user_form inline-input" >
        <div class="span12">
            <div class="container span3">
                <div class="field-box">
                    <div class="ui-select find">
                        <select class="span5 inline-input" name="job_level_id"
                                id="job_level_id">
                            <option value="">Select Level</option>
                            <?php
                            foreach ($get_job_level as $key => $job_level) {
                                ?>
                                <option
                                    value="<?php echo $job_level['job_level_id'] ?>"><?php echo $job_level['level_title'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>

            </div>

            <div class="container span3">

                <div class="field-box">
                    <div class="ui-select find">
                        <select class="span5 inline-input" name="country_id"
                                id="country_id" >
                            <option value="">Select Country</option>
                            <?php
                            foreach ($get_countries as $key => $countries) {
                                ?>
                                <option
                                    value="<?php echo $countries['country_id'] ?>"><?php echo $countries['country'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>
            <div class="container span3">


                <div class="field-box">
                    <div class="ui-select find">
                        <select class="span5 inline-input" name="creative_skill"
                                id="creative_skill">
                            <option value="">Select Skills</option>
                            <?php
                            foreach ($get_creative_skills as $key => $creative_skills) {
                                ?>
                                <option
                                    value="<?php echo $creative_skills['creative_skill_id'] ?>"><?php echo $creative_skills['creative_skill'] ?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>


            </div>
            <div class="container span3">
                <div class="span3 adv-btn">
                    <a class="btn-flat inverse user-advanced"><i class="icon-cog" style="font-size:20px;"></i></a>
                </div>
                <div class="span9">
                    <a class="btn-flat user-search" onclick="filter_tyroe('');">SEARCH</a>
                </div>
            </div>
        </div>
    </form>
</div>
    <div class="row-fluid show-grid push-down-20">

    <!-- START LEFT COLUMN -->
    <div class="span4 hidden-phone">
        <div>

            <div class="row-fluid header">
                <div class="span12">
                    <h4>Selection</h4>

                    <div class="btn-group settings pull-right left-pad-20">
                        <button class="btn glow" id="batch"><i class="icon-cog"></i></button>
                        <button class="btn glow dropdown-toggle" id="batch" data-toggle="dropdown">
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="javascript:void(0);" class="batch-selection">Batch Contact</a></li>
                            <li><a href="javascript:void(0);" class="batch-selection" id="my-button">Batch Add to Job</a>
                            </li>
                            <li><a href="javascript:void(0);" class="batch-selection">Batch Remove</a></li>
                        </ul>

                    </div>
                </div>

            </div>
            <?php   for ($tyr = 0; $tyr < count($get_tyroes); $tyr++) {
                $image;

                if ($get_tyroes[$tyr]['media_name'] != "") {
                    // $image=ASSETS_PATH_NO_IMAGE;
                    $image = ASSETS_PATH_PROFILE_THUMB . $get_tyroes[$tyr]['media_name'];
                } else {
                    $image = ASSETS_PATH_NO_IMAGE;
                }
                ?>
                <div class="row-fluid selected tyroe<?= $get_tyroes[$tyr]['user_id']; ?>" style="display: none">
                    <div class="span12 people" id="top">
                        <div class="row-fluid" id="opening-stats">
                            <div class="span12">
                                <input type="checkbox" class="user" id="tyroe_chk" name="selected_tyroe_chk"
                                       value="<?= $get_tyroes[$tyr]['user_id']; ?>">
                                <img src="<?= $image ?>" class="img-circle avatar-55">
                                <a href="#" class="name_sidebar"><?= $get_tyroes[$tyr]['username']; ?></a>
                            </div>
                        </div>
                    </div>
                </div>


            <?php
            }
            ?>
        </div>
    </div>
    <!-- END LEFT COLUMN -->
        <!-- START RIGHT COLUMN -->
        <div class="span8 sidebar">
            <div class="right-column" style="position:relative;">

                <!-- margin pointer -->
                <div class="left-pointer hidden-phone" id="candidate">
                    <div class="left-arrow"></div>
                    <div class="left-arrow-border"></div>
                </div>

                <!-- Start Header -->
                <div class="pull-right">
                    <div class="btn-group pull-right">
                        <button class="glow left large" onclick="filter_tyroe('alphabet');">Alphabetical</button>
                        <button class="glow middle large" onclick="filter_tyroe('popular');">Popular</button>
                        <button class="glow middle large" onclick="filter_tyroe('recent');">Most Recent</button>
                        <button class="glow right large" onclick="filter_tyroe('featured');">Featured</button>
                    </div>
                </div>

