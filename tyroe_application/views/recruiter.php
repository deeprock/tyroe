<!doctype html>
<!--[if IE 8 ]><html class="ie no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if IE 9]> <html class="ie9 no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html  class="no-js" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
<head>
	<meta charset="utf-8">
	<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>Tyroe - Recruitment Simplified.</title>
	<meta name="description" content="Tyroe - Recruitment Simplified.">
	<meta name="keywords" content="online portfolio, resume, recruitment, jobs, creative jobs, freelancers, vfx, animation, games, junior, entry level, internships, mid level">
	<meta name="author" content="Tyroe">

	<link rel="shortcut icon" href="assets/static-site/images/favicon.ico">
	<link rel="apple-touch-icon" href="assets/static-site/images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="assets/static-site/images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="assets/static-site/images/apple-touch-icon-114x114.png">

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- jQuery -->
	<script src="assets/static-site/js/jquery.js" type="text/javascript" charset="utf-8"></script>

	<!-- Fonts -->
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400|Montserrat' rel='stylesheet' type='text/css'>

	<!-- Stylesheets -->
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/framework.css">
	<link rel="stylesheet" type="text/css" href="assets/static-site/css/style.css">

</head>

<body>
	<!-- Begin Navigation -->
	<nav class="mobile">

		<!-- Menu -->
		<ul class="nav-content clearfix">
			<li id="magic-line"></li>
			<li class="upper"><a href="<?= base_url() ?>">For Talent</a></li>
			<li class="current-page upper"><a href="#">For Recruiters</a></li>
		</ul><!-- END -->


	</nav>
	<header class="mobile">

		<!-- Logo -->
		<a href="<?= base_url() ?>"><img class="logo" src="assets/static-site/images/logo.png" alt="Form Logo" width="96" height="35" /></a>

		<!-- Menu Button -->
		<button type="button" class="nav-button">
		  <div class="button-bars"></div>
	    </button><!-- END -->

	</header>

	<div class="sticky-head"></div>
	<!-- End Navigation -->

	<!-- Begin Large Hero Block -->
	<section class="hero accent parallax" style="background-image: url(assets/static-site/images/hero-sketch.jpg);">

		<!-- Heading -->
		<div class="hero-content container">
			<h1>Recruitment Simplified.</h1>
			<p class="herop">Manage your recruitment pipeline for up-and-coming creatives,<br>and effortlessly hire talent as a team.</p>
		</div><!-- END -->
		<div class="sub-hero container">
		
			<div class="center col-sm-12">
				<form action="http://register.tyroe.com/" method="post" name="requestFormTop">
					<input id="requestFieldTop" class="signup" onblur="blurField('Top');" onclick="focusField('Top');" value="Email address..." name="requestEmail">
					<a class="submit-btn input-width-b" onclick="validate('Top');" href="#">Request Invite</a>
				</form>

			</div>

		</div>



	</section>
	<!-- End Large Hero Block -->


<section class="center-mobile content container" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
	<div class="row">
		<div class="col-sm-4">
			<div class="pe-block  pe-view-layout-block pe-view-layout-block-4">
				<div class="pe-container">
				<h2>Powerful Dashboard</h2>
				<p>Every team member is setup with their own powerful dashboard for full collaboration and transparency in the recruitment process. They will know exactly what job they are helping to hire for, which candidates they need to review, and have full access to search the creative database. </p>
				</div>
				<a href="#sign" class="button gray" style="margin-top: 40px;
">Sign up</a>
			</div>
		</div>
		<div class="col-sm-8" style="text-align:right;">
			<img src="assets/static-site/images/dashboard.jpg" class="img-responsive" alt="" title="">
		</div>
	</div>	
</section>

<section class="center-mobile content container" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
	<div class="row">
		
		<div class="col-sm-8">
			<img src="assets/static-site/images/global_search_list.jpg" class="img-responsive" alt="" title="">
		</div>
		<div class="col-sm-4">
			<div class="pe-block  pe-view-layout-block pe-view-layout-block-4">
				<div class="pe-container">
				<h2>Search Visually</h2>
				<p>Gone are the days of trawling websites, reviewing job applications from your network of job boards and company website. Tyroe filters all your job applications through one single system and gives you full control to search on-demand.</p>
				</div>
				<a href="#sign" class="button gray" style="margin-top: 40px;
">Sign up</a>
			</div>
		</div>
	</div>	
</section>







<section class="center-mobile content container" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
	<div class="row">
		<div class="col-sm-4">
			<div class="pe-block  pe-view-layout-block pe-view-layout-block-4">
				<div class="pe-container">
				<h2>Manage Applications</h2>
				<p>Tyroe has been designed to streamline and simplify your recruitment process. Tyroe automatically tracks and monitors every job opening you are trying to fill and you will receive instant notifications to monitor your team and all the job candidates.</p>
				</div>
				<a href="#sign" class="button gray" style="margin-top: 40px;
">Sign up</a>
			</div>
		</div>
		<div class="col-sm-8" style="text-align:right;">
			<img src="assets/static-site/images/job-openings.jpg" class="img-responsive" alt="" title="">
		</div>
	</div>	
</section>

<section class="feature-box light">
		<div class="feature-image" style="background-image: url(assets/static-site/images/feature-image.jpg);"></div>
		<div class="content container center-mobile">

			<!-- Title -->
			<div class="row">
				<div class="title col-sm-6 col-sm-push-6">
					<h2>More than just a great looking portfolio site</h2>
					<p>Tyroe is a complete recruitment platform specifically designed to help studios find entry level to mid level creatives.</p>
				</div>
			</div><!-- END-->

			<!-- Features -->
			<div class="row">
				<div class="feature col-sm-6 col-sm-offset-6 col-md-3 clearfix">
					<h3>Vetted talent</h3>
					<p class="small">We help you find the best talent by hand selecting premium artists before you start.</p>
					
				</div>
				<div class="feature col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-0 clearfix">
					<h3>Industry Ranked</h3>
					<p class="small">Each Tyroe receives an industry score based on feedback from studio reviews.</p>
					
				</div>
			</div><!-- END -->

			<!-- Features -->
			<div class="row">
				<div class="feature last col-sm-6 col-sm-offset-6 col-md-3 clearfix">
					<h3>Standardised Applications</h3>
					<p class="small">Reduce time by searching and reviewing standardised portfolios and resumes.</p>
					
				</div>
				<div class="feature last col-sm-6 col-sm-offset-6 col-md-3 col-md-offset-0 clearfix">
					<h3>Export Data</h3>
					<p class="small">Tyroe exports data about Tyroes to all your existing recruitment tools.</p>
					
				</div>

			</div><!-- END -->
			
		
		</div>
	</section>

<section class="center-mobile content container" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
	<div class="row">
		
		<div class="col-sm-8">
			<img src="assets/static-site/images/reviews.jpg" class="img-responsive" alt="" title="">
		</div>
		<div class="col-sm-4">
			<div class="pe-block  pe-view-layout-block pe-view-layout-block-4">
				<div class="pe-container">
				<h2>Team Collaboration.</h2>
				<p>Hand pick your recruitment dream team from staff at your company and collaborate with them to find the best candidate for the job. Your team will love using the tools, and you will have complete control and visibility every step of the way.</p>
				</div>
				<a href="#sign" class="button gray" style="margin-top: 40px;
">Sign up</a>
			</div>
		</div>
	</div>	
</section>

<section class="center-mobile content container" style="border-bottom: 1px solid rgba(0,0,0,0.1);">
	<div class="row">
		<div class="col-sm-4">
			<div class="pe-block  pe-view-layout-block pe-view-layout-block-4">
				<div class="pe-container">
				<h2>Track every activity</h2>
				<p>At a moment’s notice you can know the status of your jobs using the Tyroe Activity Stream. Every action completed by you and your team is logged for reference. This includes notifications about reviews, new candidates being added, applicants showing interest in your job, new featured artists, and many others.</p>
				</div>
				<a href="#sign" class="button gray" style="margin-top: 40px;
">Sign up</a>
			</div>
		</div>
		<div class="col-sm-8" style="text-align:right;">
			<img src="assets/static-site/images/activity-stream.jpg" class="img-responsive" alt="" title="">
		</div>
	</div>	
</section>
	
</div>	
</section>


	<section class="content grey"  id="sign">
	<div class="container">
		<!-- Title -->
		<div class="row">
			<div class="center title col-sm-12">
				<h1 class="white">Anticipated by 87 companies who make cool stuff!</h1>
				<p style="color:white;font-size:20px">Make sure to secure your spot in our beta test starting soon.</p>

				<form action="https://register.tyroe.com/" method="post" name="requestFormBottom">
					<input id="requestFieldBottom" class="signup" onblur="blurField('Bottom');" onclick="focusField('Bottom');" value="Email address..." name="requestEmail">
					<a class="submit-btn input-width-b" onclick="validate('Bottom');" href="#">Request Invite</a>
				</form>

				</div>
				

		</div><!-- END-->

		<!-- Form -->
		
	</section>
	<!-- End Contact Form Block -->

	<!-- Begin Footer -->
	<footer>
		<div class="container">
			<div class="row">

				<!-- Contact List -->
				<ul class="contact-list col-md-8">
					<li><i class="fa fa-envelope-o"></i><a href="mailto:hello@tyroe.com">hello@tyroe.com</a></li>
					<li><i class="fa fa-phone"></i><a href="#">+ 61 (2) 9882 1835</a></li>
					<li><i class="fa fa-map-marker"></i><a href="#">Sydney, Australia.</a></li>
				</ul><!-- END -->

				<!-- Social List -->
				<ul class="social-list col-md-4">
					<li><a href="https://www.twitter.com/tyroeapp"><i class="fa fa-twitter"></i></a></li>
					<li><a href="https://www.facebook.com/tyroeapp"><i class="fa fa-facebook"></i></a></li>
					<li class="copyright">© 2014 Tyroe Pty Ltd.</li>
				</ul><!-- END -->

			</div>
		</div>
	</footer>
	<!-- End Footer -->

<!-- Update Google Analytics -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53597013-1', 'auto');
  ga('send', 'pageview');

</script>
	

	<!-- Javascript -->
	<script src="assets/static-site/js/retina.js"></script>
	<script src="assets/static-site/js/backgroundcheck.js"></script>
	<script src="assets/static-site/js/plugins.js"></script>

	<script src="assets/static-site/js/main.js"></script>

	<!-- AP Registration form -->
	<script>
		function validate(form){
		valid = true;
		validtxt = '';
		if(!/\S+@\S+\.\S+/.test(document.getElementById('requestField'+form).value)){
		valid = false;
		validtxt += 'Please provide a valid email address';
		}
		if(!valid){
		alert(validtxt);
		}else{
		document.forms['requestForm'+form].submit();
		}
		}
		function focusField(form){
		if(document.getElementById('requestField'+form).value=='Email address...'){
		document.getElementById('requestField'+form).value='';
		}
		}
		function blurField(form){
		if(document.getElementById('requestField'+form).value==''){
		document.getElementById('requestField'+form).value='Email address...';
		}
		}
	</script>

</body>

</html>