<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!--Script for Tyroe Home Page-->
<script src="<?= ASSETS_PATH ?>js/validation.js?v=0.01"></script>
<script src="<?= ASSETS_PATH ?>js/jquery.session.js"></script>
<script src="<?= ASSETS_PATH ?>js/jquery.uniform.min.js"></script>



<!--Menu toggler-->

<!-- Admin Dashboard DIV -->
<?php
if($this->controller_name == "login"){
?>
    <script src="<?= ASSETS_PATH ?>js/bootstrap.js"></script>
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-save">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>


        <div class="registration_options_link hide">
            <h3>Registration Options</h3>
            <a class="btn-glow primary">Tyroe</a>
            <a class="btn-glow primary">Studio</a>
        </div>
<?php
}
?>
<script type="text/javascript">

        /*var three_images_height = $(".full-profile_overlay").next('.row-fluid').height();
        alert(three_images_height);
        $(".full-profile_overlay").css('height',three_images_height);
        var three_images_html = $(".full-profile_overlay").next('.row-fluid').html();
        $(".tyroe_score_over_img").css('height',$(".showcase_thumb").height());*/

</script>
<script type="text/javascript">
    jQuery(document).ready(function () {
         <?php
        if($this->controller_name == "login"){
        ?>
        $(".open_accounttpe_modal").click(function (){
            $(".login_div").hide();
            $(".reg_optiondiv").fadeIn(700);
            return false;
        });

        $(".backto_login").click(function (){
            $(".reg_optiondiv").hide();
            $(".login_div").fadeIn(700);
            return false;
        });

        <?php
        }
        ?>
        $(".icon-remove-profilemessage").click(function(){
            $("#publish_notify").remove();
            var data = 'callfrom=set_publish_message&status=-1';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
        });

        $(".remove-blue-msg").click(function(){
            $("#latestjobs_notify").remove();
        });

        $(".icon-remove-recommendation").click(function(){
            $("#recommendation_notify").remove();
            var data = 'callfrom=recommendation_message_remove';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
        });

        $("#recommendation_notify").click(function(){
            $.session.set("myScrollValue", "recommendation_approved_list");
            $(".icon-remove-recommendation").trigger("click");
            window.location.href = "<?=$vObj->getURL("profile");?>";
        });
        if($.session.get("myScrollValue") == 'recommendation_approved_list'){
            $('html, body').animate({
                scrollTop: $('.'+$.session.get("myScrollValue")).offset().top
            });
            $.session.set("myScrollValue", "");
        }

        /*New JOB notification bar Remove*/
        $(".icon-remove-newjob-messages").click(function(){
            $("#latestjobs_notify").remove();
            var data = 'callfrom=latestjob_message_remove';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
        });
        $(".btn-remove-newjob-messages").click(function(){
            var newjob_message_form = '<form id="form_latest_jobs_ids" name="form_latest_jobs_ids" method="post" action="<?= $vObj->getURL('jobopening') ?>">'+
                                        '<input type="hidden" value="<?= $this->session->userdata('latest_jobs_ids') ?>" name="latest_jobs_ids">'+
                                      '</form>';
            $('body').append(newjob_message_form);
            $("#latestjobs_notify").remove();
            var data = 'callfrom=latestjob_message_remove';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
            $( "#form_latest_jobs_ids" ).submit();
        });
        /*New JOB notification bar Remove*/


        /*New SHORTLIST JOB notification bar Remove*/
        $(".icon-remove-shortlistjob-messages").click(function(){
            $("#latestshortlistjobs_notify").remove();
            var data = 'callfrom=latestshortlistjob_message_remove';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
        });
        $(".btn-remove-shortlisjob-messages").click(function(){
            var shortlist_message_form = '<form id="form_latest_shortlistjobs_ids" name="form_latest_shortlistjobs_ids" method="post" action="<?= $vObj->getURL('jobopening') ?>">'+
                                            '<input type="hidden" value="<?= $this->session->userdata('latest_shortlist_jobs_ids') ?>" name="latest_shortlist_jobs_ids">'+
                                         '</form>';
            $('body').append(shortlist_message_form);
            $("#latestshortlistjobs_notify").remove();
            var data = 'callfrom=latestshortlistjob_message_remove';
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("profile/save_profile");?>',
                dataType: 'json'
            });
            $( "#form_latest_shortlistjobs_ids" ).submit();
        });
        /*New SHORTLIST JOB notification bar Remove*/

        $('.btn-toggler').on('click', function(){
            $( 'body' ).toggleClass( "change" );

            if($('#navbar-sticky').hasClass('sticky')){
                if($('body').hasClass('change')){
                    var old_width = $(".sticky").css("width");
                    $(".sticky").css("width",parseInt(old_width)+112);
                } else {
                    var old_width = $(".sticky").css("width");
                    $(".sticky").css("width",parseInt(old_width)-112);
                }
            } else {
                $("#navbar-sticky").css("width","100%");
            }
        });

        $(".content-box-dashboard").click(function () {
            window.location.href = $(this).find("a").attr("href");
        });


    });
</script>
<!-- Admin Dashboard DIV -->

<!-- scripts for this page -->

<script>
    function accountcancel(){
        window.location.replace("home");
    }

    function checkspecialchars(evt){
        var code = evt.which || evt.keyCode;
        // 65 - 90 for A-Z and 97 - 122 for a-z 95 for _ 45 for - 46 for . - 32 for spacebar
        if (!((code >= 65 && code <= 90) || (code >= 97 && code <= 122) || code == 95 || code == 46 || code == 45 || code == 32 || code == 8 || code == 46 || (code >= 48 && code <= 57) ))
        {
            $('.specialcharserror').show();
            setTimeout(function(){
                $('.specialcharserror').hide();
            },3000);
            return false;
        }
        return true;
    }

    function jumpto(job_id,job_title,action_to)
    {
        $('#job_id').val(job_id);
        $('#job_title').val(job_title);
        $('#action').val(action_to);
        var title = job_title.replace(/ /gi, '-');
        var redirect_url = "<?=$vObj->getURL("openings")?>/" + title + '-' + generate_number_id(job_id);
        $('#jumptoform').attr('action',redirect_url);
        $('#jumptoform').submit();
    }

    function multiplechecks(master_check,checkboxes)
          {
              var is_checked = null;
              var checked_elez = $(checkboxes);
              var is_checked = master_check.is(':checked');

              if(is_checked==true){
                  checked_elez.prop('checked',true);
              } else {
                  checked_elez.prop('checked',false);
              }
              return;
              if(master_check.checked) { // check select status
                  $(checkboxes).each(function() { //loop through each checkbox
                      var each_ele  = $(this);
                      each_ele.attr('checked','checked');
                      checkboxes.checked = true;  //select all checkboxes with class "param2"
                  });
              }else{
                  $(checkboxes).each(function() { //loop through each checkbox
                      checkboxes.checked = false; //deselect all checkboxes with class "param2"
                  });
              }
          }
</script>

<?php if($this->controller_name == "findtalent"){
?>

<script>
$.noConflict();
</script>


<?php
}
?>

<!-- knob -->
<script src="<?= ASSETS_PATH ?>js/jquery.knob.js"></script>
<script>
    $(window).load(function(){
       $('.knob').knob();
           setTimeout(function () {
            $('.home_knob').css('margin-top','52px');
            $('.activity_knob').css('margin-top','49px');
            $(".knob").css('visibility','visible');
        }, 500);
    })
</script>
<script>
    function FilterFeedback() {
           $('.loader-save-holder').show();
           var feedback_type = $("#feedback_type").val();
            var feedback_keyword = $("#feedback_keyword").val();
            var feedback_duration = $("#feedback_duration").val();
            var data = "feedback_type=" + feedback_type + "&feedback_keyword=" + feedback_keyword + "&feedback_duration=" + feedback_duration;
            $.ajax({
                type: "POST",
                data: data,
                url: '<?=$vObj->getURL("feedback/filter_feedback")?>',

                success: function (data) {
                    $(".no-filter").hide();
                    $(".filter-result").html(data);
                    $("#footer_pagination").remove();
                    $(".knob").knob();
                    setTimeout(function () {
                        $('.filter-result .activity_knob').css('margin-top','49px');
                        $(".knob").css('visibility','visible');
                      },500);
                    $('.loader-save-holder').hide();
                }

            });
        }
</script>
<!--VIDEO-->
<script>
    $(document).ready(function(){
        var lst_dc_wdth = null;
        window.onresize = function (){
            if(this.resizeTO) clearTimeout(this.resizeTO);
            this.resizeTO = setTimeout(function() {
                $(this).trigger('resizeEnd');
            }, 100);

        };

        $(window).bind('resizeEnd', function() {
            //
            <?php
            if($this->session->userdata('role_id')==1){
            ?>
            var chart_id = $('.admin_dashboard_graph_btns > glow.active').attr('id');
            <?php
            } else {
            ?>
            var week_ele = $('#opening-overview').find('#week');
            var month_ele = $('#opening-overview').find('#month');
                if(week_ele.hasClass('active')){
                    var func_param='week';
                } else {
                    var func_param='month';
                }

                if(typeof chart ==='function'){
                    chart(func_param);
                }
            <?php
            }
            ?>
        });


    });


    function ytVidId(url) {
        //var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
        var p = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=|S+))((\w|-){11})(?:\S+)?$/;
        return (url.match(p)) ? RegExp.$1 : false;
    }

    function vimeoVidId(url)
    {
        var regExp = /http:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
        var match = url.match(regExp);
        if(match=="" || match==null){
            regExp = /https:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/;
            match = url.match(regExp);
        }
        var result;
        if (match){ result = match[2]; }else{ result = false; }
        return result;
    }

    jQuery("#embd_vid").click(function(){
        jQuery("#vid_url_div").fadeIn("slow");
    })
    jQuery("#save_vid").click(function(){
        var url = jQuery("#vid_url").val();
        var vid_desc = jQuery("#vid_desc").val();
        if(ytVidId(url) != false)
        {
            jQuery(".no-video").html("");
            jQuery(".no-video").html('<img id="loader" src="<?= ASSETS_PATH ?>img/loader.gif"  width="20px"/>');
            var data = "media_type=video&video_category=yt&video_url="+url+"&vid_desc="+vid_desc;
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("portfolio/upload_image");?>',
                dataType: 'json',
                success: function(data){
                    //jQuery("#loader").css("diplay","none");
                    jQuery(".no-video").html('<div class="no-video"><iframe id="frame_url" width="420" height="345" src=""></iframe></div>');
                    jQuery("#frame_url").attr("src",url);
                    jQuery("#video_status").html("Video (1/1)");
                }
            })
        } else if(vimeoVidId(url) != false){
            var playerurl = "//player.vimeo.com/video/"+vimeoVidId(url);
            jQuery(".no-video").html("");
            jQuery(".no-video").html('<img id="loader" src="<?= ASSETS_PATH ?>img/loader.gif"  width="20px"/>');
            var data = "media_type=video&video_url="+playerurl+"&video_desc="+vid_desc;
            jQuery.ajax({
                type: 'POST',
                data: data,
                url: '<?=$vObj->getURL("portfolio/upload_image");?>',
                dataType: 'json',
                success: function(data){
                    jQuery(".no-video").html('<div class="no-video"><iframe id="frame_url" width="420" height="345" src=""></iframe></div>');
                    jQuery("#frame_url").attr("src",playerurl);
                    jQuery("#video_status").html("Video (1/1)");
                }
            })
        } else {
            alert("Your URL is not valid");
        }
    });
</script>
<!--VIDEO-->


<!-- SOCIAL MEDIA -->
<script type="text/javascript">
    function media_unlink(id,type)
    {
        jQuery.ajax({
            type: 'POST',
            data: {m_id:id, m_type:type},
            url: '<?=$vObj->getURL("social/media_unlink");?>',
            dataType: 'json',
            success: function(data){
                if(data.ok)
                {
                    jQuery("#"+type).css('display','none');
                    window.location.href = '<?=$vObj->getURL("social")?>';
                }
            }
        })
    }
</script>
<!-- SOCIAL MEDIA -->


<?php
if(!isset($create_job)){    
?>
<!-- Date Picker -->
<script type="text/javascript">
    $(function () {


        $("#job_start").datepicker({dateFormat: 'dd-mm-yy' });
        $("#job_end").datepicker();



        $("#start_date").datepicker().on('changeDate',function(){
            FilterJobs();
        }).on('changeDate', function(ev){
                    $(this).datepicker('hide');
        });
        $("#end_date").datepicker().on('changeDate',function(){
                    FilterJobs();

        });
    });
</script>
<!-- Date Picker -->
<?php
}   
?>

<!-- TOOLTIP -->
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery.tipTip.js"></script>
<link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/tipTip.css"/>
<script type='text/javascript'>
    $(function () {
        $(".someClass").tipTip();
    });
</script>
<!-- TOOLTIP -->


<!-- FILE UPLOADER JS -->
<script src="<?= ASSETS_PATH ?>jQueryFileUploader/js/vendor/jquery.ui.widget.js"></script>
<script src="<?= ASSETS_PATH ?>jQueryFileUploader/js/jquery.iframe-transport.js"></script>
<script src="<?= ASSETS_PATH ?>jQueryFileUploader/js/jquery.fileupload.js?v=0.03"></script>
<!-- FILE UPLOADER JS -->



<!--color picker-->
<?php if($controller_name != "search"){  //This condition done for remove colorpicker error.?>
<script src="<?= ASSETS_PATH ?>profile/colorpicker.js"></script>
<script src="<?= ASSETS_PATH ?>profile/eye.js"></script>
<script src="<?= ASSETS_PATH ?>profile/utils.js"></script>
<script src="<?= ASSETS_PATH ?>profile/layout.js?ver=1.0.2"></script>
<?php } ?>
<!--color picker-->

<?php
if($controller_name == "publicprofile"){
    ?>
    <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.min.js"></script>
    <script>
        $(document).ready(function(){

            var nav_html = '<nav id="navigation-mobile" style="display: none;">'+
                                '<div class="container"><div class="row"><div class="span12"><ul class="sf-js-enabled sf-arrows" id="menu-nav-mobile">'+
                                    '<li class=""><a href="#intro-box">Portfolio</a></li>'+
                                    '<li class=""><a href="#latest-work">Resume</a></li>'+
                                    /*'<li class=""><a href="javascript:void(0);" class=""><i class="icon-envelope-alt"> Contact</i></a></li>'+*/
                                    /*'<li class="" style="display: none"><a href="javascript:void(0)" class="alpha_contact">Contact</a></li>'+*/
                                '</ul></div></div></div>'+
                            '</nav>';

            $(window).resize(function(){
                if( $(window).width() <= 979 ){
                    if($("#navigation-mobile").length == 0) {
                        $('header').after(nav_html);
                        $("#menu-nav-mobile").append('<li class=""><a href="javascript:void(0)" class="alpha_contact">Contact</a></li>');
                    }
                } else {
                    $("#navigation-mobile").css("display","none");
                    $("ul#menu-nav > li:last-child").remove();
                    $("ul#menu-nav").append('<li class=""><a href="javascript:void(0)" class=""><i class="icon-envelope-alt"></a></li>');
                }
            });

            if( $(window).width() <= 979 ){
                if($("nav#navigation-mobile").css('display','none'))
                {
                    $("ul.sf-js-enabled > li:last-child").remove();
                    $("ul.sf-js-enabled").append('<li class=""><a href="javascript:void(0)" class="alpha_contact">Contact</a></li>');
                }
            }

        });



    </script>
<?php
}
    if($controller_name == "profile" || $controller_name == "profile2" || $controller_name == "publicprofile"){
    ?>


         <!--commented by shafqat(21-apr-2014) issue(Conflict)-->
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.themepunch.revolution.min.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.themepunch.plugins.min.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.superfish.min.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.supersubs.min.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.isotope.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.fancybox.pack.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.fancybox-media.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.tweet.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/jquery.flickr.min.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/plugins.js"></script>
        <script src="<?= ASSETS_PATH ?>profile/anibus/js/main.js"></script>

        <script>
            $(function() {
                $(window).scroll(function() {
                    if($(this).scrollTop() > 1000) {
                        $('#back-to-top').fadeIn();
                    } else {
                        $('#back-to-top').fadeOut();
                    }
                });

                $('#back-to-top').click(function() {
                    $('body,html').animate({scrollTop:0},1600);
                });
            });
        </script>

<?php
}
?>
<script src="<?= ASSETS_PATH ?>js/jquery-ui.min.js"></script>
<script src="<?= ASSETS_PATH ?>js/bootstrap.min.js"></script>
<?php
if(isset($create_job) && $create_job == 1){    
?>
<script src="<?= ASSETS_PATH ?>js/moment.js"></script>
<script src="<?= ASSETS_PATH ?>js/bootstrap-datetimepicker.js"></script>
<?php
}else{
?>
<script src="<?= ASSETS_PATH ?>js/bootstrap-datepicker.js"></script>
<?php
}
?>



<script src="<?= ASSETS_PATH ?>js/raphael-min.js"></script>
<script src="<?= ASSETS_PATH ?>js/morris-0.4.1.min.js"></script>
<!-- call all plugins -->

<script src="<?= ASSETS_PATH ?>js/theme.js"></script>
<!-- build the charts -->

<!--Table Sorter-->
<script src="<?=ASSETS_PATH?>js/jquery.tablesorter.js"></script>

<!--- TAGS MAKING -->
<script src="<?= ASSETS_PATH ?>js/select2.min.js"></script>


<script type="text/javascript">

    $(".schematrigger").click(function (eve) {
        $schematrigger = $(this);
        eve.stopPropagation();
        eve.preventDefault();
        ///console.log($schematrigger.attr('class'));
        eve.cancelBubble;


        $(".schemaholder").slideUp();
        /////


        //event.stopPropagation();
        //getting the next element
        $schemaholder = $schematrigger.children(".schemaholder");
        //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
        if($schemaholder.is(":visible")){
            $schemaholder.slideUp(500);
        }
        else {
            $schemaholder.slideDown(500);
        }

    });

</script>

<script>
    $(document).ready(function(){

        /*Mobile Menu Toggler - Start*/
        var $menu = $("#sidebar-nav");
        $("body").click(function () {
            if ($menu.hasClass("display")) {
                $menu.removeClass("display");
            }
        });
        $menu.click(function(e) {
            e.stopPropagation();
        });
        $("#menu-toggler").click(function (e) {
            e.stopPropagation();
            $menu.toggleClass("display");
        });
        /*Mobile Menu Toggler - End*/
    })
</script>
<?php
switch ($this->controller_name) {
    case 'portfolio':
        ?>

        <script>
            $(function () {
                'use strict';
                var url = '<?=ASSETS_PATH?>uploads/index.php?dir=portfolio';
                $('#fileupload').click(function () {

                    $('#progress .bar').css(
                        'width',
                        0 + '%'
                    );
                })
                $('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    done: function (e, data) {
                        $.each(data.result.files, function (index, file) {
                            $('<p/>').text(file.name).appendTo('#files');
                        });
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        //var progress = parseInt(data.loaded * 100, 10);
                        $('#progress .bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
            });


        </script>
        <?php
        break;
    case 'profile':
    case 'profile1':
        ?>
        <!-- USERNAME VALIDATION -->
        <script type="text/javascript">
            jQuery("#user_name").blur(function () {
                var user_name = jQuery("#user_name").val();
                if (user_name != '') {
                    jQuery("#loader").css("display", "");
                    var data = "username=" + user_name;
                    jQuery.ajax({
                        type: 'POST',
                        data: data,
                        url: '<?=$vObj->getURL("profile/check_username");?>',
                        dataType: 'json',
                        success: function (data) {
                            jQuery("#loader").css("display", "none");
                            if (data.result == 1) {
                                jQuery("#user_result").attr("src", "<?=ASSETS_PATH?>img/not_ok.png");
                                jQuery("#user_name").val("");
                            } else {
                                jQuery("#user_result").attr("src", "<?=ASSETS_PATH?>img/yes_check.png");
                            }
                            jQuery("#user_result").css("display", "");
                        }
                    });
                }
            })
        </script>
        <!-- USERNAME VALIDATION -->
        <script>
            $(function () {
                'use strict';
                var url = '<?=ASSETS_PATH?>uploads/index.php?dir=profile';
                $('#fileupload').change(function () {
		            $('.loader-save-holder').show();
                })

                $('#fileupload').click(function () {
                    $('#progress .bar').css(
                        'width',
                        0 + '%'
                    );
                });


                $('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    done: function (e, data) {
                        $('#new_image').hide();
                        $('#pro_image_avatar').show();
                        $.each(data.result.files, function (index, file) {
                            jQuery("#imgname").val(file.name);
                            $("#save_profile_img").trigger("click");
                            $('#cropImageUrl').val(file.name);
                            $('#is_image_uploaded').val("yes");
                        });
                    }
                });

                /*$('#fileupload').fileupload({
                    url: url,
                    dataType: 'json',
                    done: function (e, data) {
                        $('#new_image').hide();
                        $('#pro_image_avatar').show();
                        $.each(data.result.files, function (index, file) {
                            jQuery("#imgname").val(file.name);
                            var html='<img id="cropImage"  src="<?=ASSETS_PATH_PROFILE_IMAGE?>'+file.name+'" class="jcrop-preview" alt="Preview" />';
                            jQuery('#cropImageContainer').html(html);
                            jQuery('#cropImageUrl').val(file.name);
                            customCropImage();
                            $("#save_profile_img").trigger("click");

                        });
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');*/

            });
        </script>
        <?php
        break;

    case 'home':
    if($this->session->userdata('role_id') ==1){

        $monthly_users_data = array();
        $months_name = array('jan','feb','mar','apr','may','jun','jul','aug','sep','oct','nov','dec');
        $days_name = array('mon','tue','wed','thu','fri','sat','sun');
        $years_users_data = array();
        $monthly_users_data = array();
        $weekly_users_data = array();
//$weekly_users_data = array();
//2=tyroe, 3=studio, 4=reviewer
        $current_month = date('m');
        $current_year = date('Y');
        foreach($admin_graph_data as $graph_data){
            $created_timestamp = $graph_data['created_timestamp'];
            $user_id = $graph_data['user_id'];
            $role_id = $graph_data['role_id'];
            $the_monthname = strtolower(date('M',$created_timestamp));

            $the_year = strtolower(date('Y',$created_timestamp));
            $the_month = strtolower(date('m',$created_timestamp));
            $the_dayname = strtolower(date('D',$created_timestamp));
            if($role_id==2) { $new_role ='Tyroe'; } else if($role_id==3 || $role_id==4) { $new_role ='Studio_Reviewer'; }
            $years_users_data[$new_role][$the_year][]=1;
            if($current_year ==$the_year) {
                $monthly_users_data[$new_role][$the_monthname][]=1;
            }
            if($created_timestamp<=$week_end_unix && $created_timestamp>=$week_start_unix && $current_month==$the_month) {
                $weekly_users_data[$new_role][$the_dayname][]=1;
            }
        }

        $prev_tenyears = date('Y',$prev_ten_years);
        $tyroe_yearsdata = '';
        $studio_reviewer_yearsdata = '';
        $years_label = '';
        $for_years_inc = 0;
        for($years_inc=$prev_tenyears;$years_inc<=$prev_tenyears+9;$years_inc++) {
            $for_years_inc++;
            if($years_inc==$prev_tenyears+9) {
                $add_comma='';
            } else {
                $add_comma=',';
            }
            $tyroe_year_count = count($years_users_data['Tyroe'][$years_inc]);
            $reviewer_year_count = count($years_users_data['Studio_Reviewer'][$years_inc]);
            if($reviewer_year_count===null) {
                $reviewer_year_count = 0;
            }
            if($tyroe_year_count===null) {
                $tyroe_year_count = 0;
            }

            $years_label .= '['.$for_years_inc.', '.$years_inc.']'.$add_comma;


            $tyroe_years_count .= '['.$for_years_inc.', '.$tyroe_year_count.']'.$add_comma;
            $studio_reviewer_years_count .= '['.$for_years_inc.', '.$reviewer_year_count.']'.$add_comma;
        }

        $month_inc = 0;
        $months_label = null;
        foreach($months_name as $month_key=>$month_name){
            $month_inc++;
            $add_comma_month = $month_name=='dec'?" ":", ";

            $tyroe_month_count = count($monthly_users_data['Tyroe'][$month_name]);
            $reviewer_month_count = count($monthly_users_data['Studio_Reviewer'][$month_name]);

            if($reviewer_month_count===null) {
                $reviewer_month_count = 0;
            }
            if($tyroe_month_count===null) {
                $tyroe_month_count = 0;
            }




            $tyroe_months_count .= '['.$month_inc.', '.$tyroe_month_count.']'.$add_comma_month;
            $studio_reviewer_months_count .= '['.$month_inc.', '.$reviewer_month_count.']'.$add_comma_month;
            $months_label .= '['.$month_inc.', "'.strtoupper($month_name).'"]'.$add_comma_month;
        }

        //$days_name;
        $day_inc = 0;
        foreach($days_name as $day_key=>$day_name){
            $day_inc++;
            $add_comma_days = $day_name=='sun'?" ":", ";

            $tyroe_day_count = count($weekly_users_data['Tyroe'][$day_name]);
            $reviewer_days_count = count($weekly_users_data['Studio_Reviewer'][$day_name]);

            if($tyroe_day_count===null) {
                $tyroe_day_count = 0;
            }
            if($reviewer_days_count===null) {
                $reviewer_days_count = 0;
            }

            $tyroe_days_count .= '['.$day_inc.', '.$tyroe_day_count.']'.$add_comma_days;
            $studio_reviewer_days_count .= '['.$day_inc.', '.$reviewer_days_count.']'.$add_comma_days;
            $days_label .= '['.$day_inc.', "'.strtoupper($day_name).'"]'.$add_comma_days;
        }
     ?>

        <script>
            var years_label = null;
            var tyroe_years_count = null;
            var studio_reviewer_years_count = null;
            var months_label = null;
            var tyroe_months_count = null;
            var studio_reviewer_months_count = null;
            var days_label = null;
            var tyroe_days_count = null;
            var studio_reviewer_days_count = null;

            years_label = [<?= $years_label; ?>];
            tyroe_years_count = [<?= $tyroe_years_count; ?>];
            studio_reviewer_years_count = [<?= $studio_reviewer_years_count; ?>];
            months_label = [<?= $months_label; ?>];
            tyroe_months_count = [<?= $tyroe_months_count; ?>];
            studio_reviewer_months_count = [<?= $studio_reviewer_months_count; ?>];
            days_label = [<?= $days_label; ?>];
            tyroe_days_count = [<?= $tyroe_days_count; ?>];
            studio_reviewer_days_count = [<?= $studio_reviewer_days_count; ?>];
        </script>
        <?php
        }
        ?>

        <!-- PROGRESS BARS JS & CS -->
        <?php if($this->session->userdata('role_id') == '1' || $this->session->userdata('role_id') == '2' || $this->session->userdata('role_id') == '5'){ ?>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/jquery.ui.css">
            <script src="<?= ASSETS_PATH ?>js/jquery.ui.js"></script>
        <?php } ?>
        <script>
            $(function () {
                var value1 = <?=$profile;?>0;
                $("#progressbar_<?=LABEL_MODULE_PROFILE;?>").progressbar({
                    value: value1
                });
                if (value1 < 30) {
                    $("#progressbar_<?=LABEL_MODULE_PROFILE;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value1 >= 30 && value1 < 70) {
                    $("#progressbar_<?=LABEL_MODULE_PROFILE;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value1 >= 70 && value1 <= 100) {
                    $("#progressbar_<?=LABEL_MODULE_PROFILE;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }

                var value2 = <?=$resume;?>0;
                $("#progressbar_<?=LABEL_MODULE_RESUME;?>").progressbar({
                    value: value2
                });
                if (value2 < 30) {
                    $("#progressbar_<?=LABEL_MODULE_RESUME;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value2 >= 30 && value2 < 70) {
                    $("#progressbar_<?=LABEL_MODULE_RESUME;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value2 >= 70 && value2 <= 100) {
                    $("#progressbar_<?=LABEL_MODULE_RESUME;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }

                var value3 = <?=$portfolio;?>0;
                $("#progressbar_<?=LABEL_MODULE_PORTFOLIO;?>").progressbar({
                    value: value3
                });
                if (value3 < 30) {
                    $("#progressbar_<?=LABEL_MODULE_PORTFOLIO;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value3 >= 30 && value3 < 70) {
                    $("#progressbar_<?=LABEL_MODULE_PORTFOLIO;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value3 >= 70 && value3 <= 100) {
                    $("#progressbar_<?=LABEL_MODULE_PORTFOLIO;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }

                var value4 = <?php//=$socialmedia;?>60;
                $("#progressbar_Social").progressbar({
                    value: value4
                });
                if (value4 < 30) {
                    $("#progressbar_Social > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value4 >= 30 && value4 < 70) {
                    $("#progressbar_Social > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value4 >= 70 && value4 <= 100) {
                    $("#progressbar_Social > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }

                var value5 = <?=$subscription;?>0;
                $("#progressbar_<?=LABEL_MODULE_SUBSCRIPTION;?>").progressbar({
                    value: value5
                });
                if (value5 < 30) {
                    $("#progressbar_<?=LABEL_MODULE_SUBSCRIPTION;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value5 >= 30 && value5 < 70) {
                    $("#progressbar_<?=LABEL_MODULE_SUBSCRIPTION;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value5 >= 70 && value5 <= 100) {
                    $("#progressbar_<?=LABEL_MODULE_SUBSCRIPTION;?> > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }

                var value6 = <?=$notification;?>0;
                $("#progressbar_Schedule").progressbar({
                    value: value6
                });
                if (value6 < 30) {
                    $("#progressbar_Schedule > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% red' });
                } else if (value6 >= 30 && value6 < 70) {
                    $("#progressbar_Schedule > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% #ffa500' });
                } else if (value6 >= 70 && value6 <= 100) {
                    $("#progressbar_Schedule > div").css({ 'background': 'url("images/ui-bg_highlight-soft_75_cccccc_1x100.png") repeat-x scroll 50% 50% green' });
                }
            });
        </script>
        <!-- PROGRESS BARS JS & CS -->
        <!-- flot charts -->
        <script src="<?= ASSETS_PATH ?>js/jquery.flot.js"></script>
        <script src="<?= ASSETS_PATH ?>js/jquery.flot.stack.js"></script>
        <script src="<?= ASSETS_PATH ?>js/jquery.flot.resize.js"></script>
        <!-- morrisjs -->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script src="<?= ASSETS_PATH ?>js/morris.min.js"></script>
        <?php if($this->session->userdata('role_id') != '1'){ ?>
        <script type="text/javascript">
        $(window).bind("load", function () {
            chart('month');
        });
        jQuery("#week").click(function () {
            chart('week');
        })
        jQuery("#month").click(function () {
            chart('month');
        })

        function chart(data) {
            switch (data) {
               case 'week':
                    $("#month").removeClass('active');
                    $("#week").addClass('active');
                    $(".knob").knob();

                    // jQuery Flot Chart
                    var visitors_week = [
                        <?php
                        $a="";
                        foreach($week_chart as $k_day => $v_day){
                        $average = ($v_day/$week_chart_others[$k_day])*100;
                            ?>
                        [<?=$k_day?>, <?=$average?>],
                        <?php
                        if($average > $a)
                        {
                            $a=$average;
                        }
                    }

                    ?>
                    ];

                    var others_week = [
                        <?php
                        $b="";
                        foreach($week_chart_others as $ko_day => $vo_day){
                            $average_all_tyroes_week = ceil(( ($vo_day / $count_all_tyroes['cnt']) / $vo_day ) * 100);
                            ?>
                            [<?=$ko_day?>, <?=$average_all_tyroes_week?>],
                            <?php
                            if($average_all_tyroes_week>$b)
                            {
                                $b= $average_all_tyroes_week;
                            }
                        }
                        //getting the greater one from a and b and storing it in c
                         if($a>$b)
                         //getting absolute value of c
                            $c = ceil($a);
                            else
                            $c = ceil($b);


                           //dividing c into 5 for graph ticks
                            $val = $c/5;
                            $val1 = $val;
                            $dataArray = array();
                            for($i=0;$i<5;$i++){
                            $val =ceil($val);
                            $dataArray[] = $val;
                            $val = $val1 + $val;
                            }
                            //imploding the array of ticks so that we can use it in javascript
                            $array = implode(',',$dataArray);
                        ?>
                    ];
                  var plot = $.plot($("#statsChart"),
                        [
                            { data: visitors_week, label: "My Profile" },
                            { data: others_week, label: "Member Profiles" }
                        ], {
                            series: {
                                lines: { show: true,
                                    lineWidth: 1,
                                    fill: true,
                                    fillColor: { colors: [
                                        { opacity: 0.1 },
                                        { opacity: 0.13 }
                                    ] }
                                },
                                points: { show: true,
                                    lineWidth: 2,
                                    radius: 3
                                },
                                shadowSize: 0,
                                stack: false
                            },
                            grid: { hoverable: true,
                                clickable: true,
                                tickColor: "#f9f9f9",
                                borderWidth: 0
                            },
                            legend: {
                                // show: false
                                labelBoxBorderColor: "#fff"
                            },
                            colors: ["#30a0eb", "#a7b5c5"],
                            xaxis: {
                                ticks: [
                                    [1, "MON"],
                                    [2, "TUE"],
                                    [3, "WED"],
                                    [4, "THU"],
                                    [5, "FRI"],
                                    [6, "SAT"],
                                    [7, "SUN"]
                                ],
                                font: {
                                    size: 12,
                                    family: "Open Sans, Arial",
                                    variant: "small-caps",
                                    color: "#697695"
                                }
                            },
                            yaxis: {
                                min: 0,
                                max: <?=$c?>,
                                ticks: [0,<?=$array?>],
                                tickFormatter: function (val, axis) {
                                    return val + "%";
                                },
                                tickDecimals: 0,
                                font: {size: 12, color: "#9da3a9"}
                            }
                        });
                    break;
                case 'month':
                case 'default':
                    $("#month").addClass('active');
                    $("#week").removeClass('active');
                    $(".knob").knob();

                    <?php /*echo '<pre>';print_r($month_chart );echo '</pre>';*/?>

                    // jQuery Flot Chart
                    var visitors_month = [
                        <?php
                        //getting the max value then store in variable a to implement the max value of graph
                        $a="0";
                        foreach($month_chart as $k_month => $v_month){
                        $average = ($v_month/$month_chart_others[$k_month])*100;
                        ?>
                        [<?=$k_month?>, <?=$average?>],
                        <?php
                            if($average > $a)
                            {
                                $a=$average;
                            }
                        }
                        ?>
                    ];
                    var others_month = [
                        <?php
                        //getting the max value then store in variable b to implement the max value of graph
                        $b="0";
                        foreach($month_chart_others as $ko_month => $vo_month){
                            $average_all_tyroes = ( ($vo_month / $count_all_tyroes['cnt']) / $vo_month ) * 100;
                            ?>
                            [<?=$ko_month?>, <?=$average_all_tyroes?>],
                            <?php
                            if($average_all_tyroes>$b)
                            {
                                $b= $average_all_tyroes;
                            }

                        }
                        //getting the greater one from a and b and storing it in c
                         if($a>$b)
                         //getting absolute value of c
                            $c = ceil($a);
                            else
                            $c = ceil($b);
                           //dividing c into 5 for graph ticks
                            $val = $c/5;
                            $val1 = $val;
                            $dataArray = array();
                            for($i=0;$i<5;$i++){
                            $val =ceil($val);
                            $dataArray[] = $val;
                            $val = $val1 + $val;
                            }
                            //imploding the array of ticks so that we can use it in javascript
                            $array = implode(',',$dataArray);

                        ?>
                    ];
                    var plot = $.plot($("#statsChart"),
                        [
                            { data: visitors_month, label: "My Profile" },
                            { data: others_month, label: "Member Profiles" }
                        ], {
                            series: {
                                lines: { show: true,
                                    lineWidth: 1,
                                    fill: true,
                                    fillColor: { colors: [
                                        { opacity: 0.1 },
                                        { opacity: 0.13 }
                                    ] }
                                },
                                points: { show: true,
                                    lineWidth: 2,
                                    radius: 3
                                },
                                shadowSize: 0,
                                stack: false
                            },
                            grid: { hoverable: true,
                                clickable: true,
                                tickColor: "#f9f9f9",
                                borderWidth: 0
                            },
                            legend: {
                                // show: false
                                labelBoxBorderColor: "#fff"
                            },
                            colors: ["#30a0eb", "#a7b5c5"],
                            xaxis: {
                                ticks: [
                                    [1, "JAN"],
                                    [2, "FEB"],
                                    [3, "MAR"],
                                    [4, "APR"],
                                    [5, "MAY"],
                                    [6, "JUN"],
                                    [7, "JUL"],
                                    [8, "AUG"],
                                    [9, "SEP"],
                                    [10, "OCT"],
                                    [11, "NOV"],
                                    [12, "DEC"]
                                ],
                                font: {
                                    size: 12,
                                    family: "Open Sans, Arial",
                                    variant: "small-caps",
                                    color: "#697695"
                                }
                            },
                            yaxis: {
                                min: 0,
                                max:<?=$c ?>,
                                ticks:[0,<?=$array?>],
                                tickFormatter: function (val, axis) {
                                    return val + "%";
                                },
                                tickDecimals: 0,
                                font: {size: 12, color: "#9da3a9"}
                            }
                        });
                    break;
            }
        }
        </script>


    <?php } else { ?>

        <script type="text/javascript">
            function chart5() {
                return false;
                <?php /* currently this function not required */ ?>
                var d1 = [];
                var d1 = [
                <?php
                $key_set=array();
                foreach($accumulate_jobs as $k=>$v){
                    if($accumulate_jobs[$k]['r_month']!=0){
                        //echo "[".$accumulate_jobs[$k]['r_month'].",parseInt(Math.random() * 30)],";
                        echo "[".$accumulate_jobs[$k]['r_month'].",parseInt(".$accumulate_jobs[$k]['r_cnt'].")],";
                        $key_set[$accumulate_jobs[$k]['r_month']]="a";
                    }
                }
                for($i=1;$i<=12;$i++){
                    if(!isset($key_set[$i])){
                        echo "[".$i.",0],";
                    }
                }

                ?>
                ];

                var stack = 0,
                        bars = true,
                        lines = false,
                        steps = false;

                function plotWithOptions() {
                    $.plot($("#chart_5"), [d1], {
                        xaxis: {
                            ticks: [
                                [1, "JAN"],
                                [2, "FEB"],
                                [3, "MAR"],
                                [4, "APR"],
                                [5, "MAY"],
                                [6, "JUN"],
                                [7, "JUL"],
                                [8, "AUG"],
                                [9, "SEP"],
                                [10, "OCT"],
                                [11, "NOV"],
                                [12, "DEC"]
                            ]
                        },
                        series: {
                            stack: stack,
                            lines: {
                                show: lines,
                                fill: true,
                                steps: steps
                            },
                            bars: {
                                show: bars,
                                barWidth: 0.6
                            }
                        }
                    });
                }

                $(".stackControls input").click(function (e) {
                    e.preventDefault();
                    stack = $(this).val() == "With stacking" ? true : null;
                    plotWithOptions();
                });
                $(".graphControls input").click(function (e) {
                    e.preventDefault();
                    bars = $(this).val().indexOf("Bars") != -1;
                    lines = $(this).val().indexOf("Lines") != -1;
                    steps = $(this).val().indexOf("steps") != -1;
                    plotWithOptions();
                });

                plotWithOptions();
            }
            chart5();
        $(window).bind("load", function () {
            chart_newfunc('month');
        });
            $('body').delegate('.admin_dashboard_graph_btns > .glow','click',function (){
                var this_ele = $(this);
                var this_id = this_ele.attr('id');
                $('.admin_dashboard_graph_btns > .glow').removeClass('active');
                this_ele.addClass('active');
                chart_newfunc(this_id);
            });

        function chart_newfunc(data) {

            var studio_reviewer_data=null;
            var tyroes_data=null;
            var graph_label=null;
            switch (data) {
                case 'month':
                case 'default':
                   studio_reviewer_data=studio_reviewer_months_count;
                    tyroes_data=tyroe_months_count;
                    graph_label=months_label;
                    $(".knob").knob();
                    break;
                case 'year':
                    studio_reviewer_data=studio_reviewer_years_count;
                    tyroes_data=tyroe_years_count;
                    graph_label=years_label;
                    break;
                case 'day':
                    studio_reviewer_data=studio_reviewer_days_count;
                    tyroes_data=tyroe_days_count;
                    graph_label=days_label;
                    break;
            }
            var max_val1=0;
            var max_val2=0;
            var max_val;
            var i;
            for(i = 0;i<studio_reviewer_data.length;i++)
            {
            if(studio_reviewer_data[i][1] > max_val1)
                {
                    max_val1 = studio_reviewer_data[i][1];
                }
            }
            for(i = 0;i<tyroes_data.length;i++)
            {
            if(tyroes_data[i][1] > max_val2)
                {
                    max_val2 = tyroes_data[i][1];
                }
            }
            if(max_val1>max_val2)
            {
                max_val=max_val1;
            }
            else
            {
                max_val=max_val2;
            }
            max_val=max_val/3;
            var y_ticks=[];
            y_ticks[0]=0;
            var val=max_val;
            max_val = null;
            for(i=1;i<4;i++)
            {
                max_val=max_val+val;
                max_val=Math.ceil(max_val);
                y_ticks[i]=max_val;

            }
            var plot = $.plot($("#statsChart"),
                    [ { data: studio_reviewer_data, label: "Studio/Reviewer"},
                        { data: tyroes_data, label: "Tyroe" }], {
                        series: {
                            lines: { show: true,
                                lineWidth: 1,
                                fill: true,
                                fillColor: { colors: [ { opacity: 0.05 }, { opacity: 0.09 } ] }
                            },
                            points: { show: true,
                                lineWidth: 2,
                                radius: 3
                            },
                            shadowSize: 0,
                            stack: false
                        },
                        grid: { hoverable: true,
                            clickable: true,
                            tickColor: "#f9f9f9",
                            borderWidth: 0
                        },
                        legend: {
                            //show: false
                            labelBoxBorderColor: "#fff"
                        },
                        colors: ["#a7b5c5", "#30a0eb"],
                        xaxis: {
                            ticks: graph_label,
                            font: {
                                size: 14,
                                family: "Open Sans, Arial",
                                variant: "small-caps",
                                color: "#9da3a9"
                            }
                        },
                        yaxis: {
                            min : 0,
                            max : max_val,
                            ticks:y_ticks,
                            tickDecimals: 0,
                            font: {size:12, color: "#9da3a9"}
                        }
                    });

            $("#statsChart").css({"height":"183px"});
            return false;
            <?php /* */ ?>

            var studio_reviewer_data=null;
            var tyroes_data=null;
            var graph_label=null;
            switch (data) {
                case 'month':
                case 'default':
                    studio_reviewer_data=studio_reviewer_months_count;
                    tyroes_data=tyroe_months_count;
                    graph_label=months_label;
                    $(".knob").knob();
                    break;
                case 'year':
                    studio_reviewer_data=studio_reviewer_years_count;
                    tyroes_data=tyroe_years_count;
                    graph_label=years_label;
                    break;
                case 'day':
                    studio_reviewer_data=studio_reviewer_days_count;
                    tyroes_data=tyroe_days_count;
                    graph_label=days_label;
                    break;
            }


            var plot = $.plot($("#statsChart"),
                    [
                        { data: tyroes_data, label: "Tyroe" },
                        { data: studio_reviewer_data, label: "Studio/Reviewer" }
                    ], {
                        series: {
                            lines: { show: true,
                                lineWidth: 1,
                                fill: true,
                                fillColor: { colors: [
                                    { opacity: 0.1 },
                                    { opacity: 0.13 }
                                ] }
                            },
                            points: { show: true,
                                lineWidth: 2,
                                radius: 3
                            },
                            shadowSize: 0,
                            stack: true
                        },
                        grid: { hoverable: true,
                            clickable: true,
                            tickColor: "#f9f9f9",
                            borderWidth: 0
                        },
                        legend: {
                            // show: false
                            labelBoxBorderColor: "#fff"
                        },
                        colors: ["#A7B5C2", "#30a0eb"],
                        xaxis: {
                            ticks: graph_label,
                            font: {
                                size: 12,
                                family: "Open Sans, Arial",
                                variant: "small-caps",
                                color: "#697695"
                            }
                        },
                        yaxis: {
                            min: 0,
                            ticks: 3,
                            tickDecimals: 0,
                            font: {size: 12, color: "#9da3a9"}
                        }
                    });


        }

        function chart(data) {
            switch (data) {
                case 'month':
                case 'default':
                    $(".knob").knob();


                    // jQuery Flot Chart----------------
                    var tyroes = [
                        <?php
                            $key_set=array();
                            $my_arr = array();


                            foreach($accumulate_tyroes as $k=>$v){
                                if($accumulate_tyroes[$k]['r_month']!=0){
                                    $my_arr[$accumulate_tyroes[$k]['r_month']] = $accumulate_tyroes[$k]['r_cnt'];
                                    $key_set[$accumulate_tyroes[$k]['r_month']]="a";
                                }
                            }
                            $count = 0;
                            for($i=1;$i<=12;$i++){
                                if(!isset($my_arr[$i])){
                                    $my_arr[$i] = 0;
                                }
                                if($i == 1){
                                    echo "[".$i.",".$my_arr[$i]."],";
                                    $count = $my_arr[$i];
                                } else {
                                    $count = $count+$my_arr[$i];
                                    echo "[".$i.",".$count."],";
                                }
                            }
                        ?>
                    ];
                    var studio_reviewer = [
                        <?php
                        $key_set=array();
                        $my_arr = array();


                        foreach($accumulate_studios_reviewers as $k=>$v){
                            if($accumulate_studios_reviewers[$k]['r_month']!=0){
                                $my_arr[$accumulate_studios_reviewers[$k]['r_month']] = $accumulate_studios_reviewers[$k]['r_cnt'];
                                $key_set[$accumulate_studios_reviewers[$k]['r_month']]="a";
                            }
                        }
                        $count = 0;
                        for($i=1;$i<=12;$i++){
                            if(!isset($my_arr[$i])){
                                $my_arr[$i] = 0;
                            }
                            if($i == 1){
                                echo "[".$i.",".$my_arr[$i]."],";
                                $count = $my_arr[$i];
                            } else {
                                $count = $count+$my_arr[$i];
                                echo "[".$i.",".$count."],";
                            }
                        }
                        ?>
                    ];
                    var plot = $.plot($("#statsChart"),
                            [
                                { data: tyroes, label: "Tyroe" },
                                { data: studio_reviewer, label: "Studio/Reviewer" }
                            ], {
                                series: {
                                    lines: { show: true,
                                        lineWidth: 1,
                                        fill: true,
                                        fillColor: { colors: [
                                            { opacity: 0.1 },
                                            { opacity: 0.13 }
                                        ] }
                                    },
                                    points: { show: true,
                                        lineWidth: 2,
                                        radius: 3
                                    },
                                    shadowSize: 0,
                                    stack: true
                                },
                                grid: { hoverable: true,
                                    clickable: true,
                                    tickColor: "#f9f9f9",
                                    borderWidth: 0
                                },
                                legend: {
                                    // show: false
                                    labelBoxBorderColor: "#fff"
                                },
                                colors: ["#A7B5C2", "#30a0eb"],
                                xaxis: {
                                    ticks: [
                                        [1, "JAN"],
                                        [2, "FEB"],
                                        [3, "MAR"],
                                        [4, "APR"],
                                        [5, "MAY"],
                                        [6, "JUN"],
                                        [7, "JUL"],
                                        [8, "AUG"],
                                        [9, "SEP"],
                                        [10, "OCT"],
                                        [11, "NOV"],
                                        [12, "DEC"]
                                    ],
                                    font: {
                                        size: 12,
                                        family: "Open Sans, Arial",
                                        variant: "small-caps",
                                        color: "#697695"
                                    }
                                },
                                yaxis: {
                                    min: 0,
                                    ticks: 3,
                                    tickDecimals: 0,
                                    font: {size: 12, color: "#9da3a9"}
                                }
                            });
                    break;
            }
        }

        </script>
        <?php } ?>
        <script type="text/javascript">
        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                position: 'absolute',
                display: 'none',
                top: y - 30,
                left: x - 50,
                color: "#fff",
                padding: '2px 5px',
                'border-radius': '6px',
                'background-color': '#000',
                opacity: 0.80
            }).appendTo("body").fadeIn(200);
        }
        var previousPoint = null;
        $("#statsChart").bind("plothover", function (event, pos, item) {
            if (item) {
                if (previousPoint != item.dataIndex) {
                    previousPoint = item.dataIndex;

                    $("#tooltip").remove();
                    var x = item.datapoint[0].toFixed(0),
                        y = item.datapoint[1].toFixed(0);

                    var month = item.series.xaxis.ticks[item.dataIndex].label;

                    showTooltip(item.pageX, item.pageY,
                        item.series.label + " of " + month + ": " + y);
                }
            }
            else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
        </script>


        <!--Script for tyro home page End-->
        <script type="text/javascript">
            <?php
            if($profile_completness['resume_scorer'] == "" || $profile_completness['resume_scorer'] == "0")
            {
                $resume_color = "#F9785B";
            }else{
                $resume_color = "#76bdee";
            }
            if($profile_completness['portfolio_scorer'] == "" || $profile_completness['portfolio_scorer'] == "0")
            {
                $portfolio_color = "#F9785B";
            }else{
                $portfolio_color = "#c4dafe";
            }
            ?>
            var resume_color = "<?=$resume_color?>";
            var portfolio_color = "<?=$portfolio_color?>";
            // Morris Donut Chart -- Profile Completeness
            Morris.Donut({
                element: 'candidate-donut',
                data: [
                    {label: 'Profile', value: <?=$profile_completness['profile_scorer'];?> },
                    {label: 'Resume', value: <?=$profile_completness['resume_scorer'];?> },
                    {label: 'Portfolio', value: <?=$profile_completness['portfolio_scorer'];?> },
                    {label: 'Incomplete', value: <?=$profile_completness['incomplete'];?> }
                ],
                colors: ["#30a1ec",resume_color,portfolio_color,"#F9785B"],
                readonly: true,
                formatter: function (y) {
                    return y + "%"
                }
            });
            var last_width =null;



            $(document).ready(function(){



                // Morris Donut Chart
                Morris.Donut({
                    element: 'reviewer-donut',
                    data: [
                        {label: 'Reviewed', value: 45 },
                        {label: 'Pending', value: 55 }
                    ],
                    colors: ["#81bd82", "#76bdee"],
                    readonly: true,
                    formatter: function (y) {
                        return y + "%"
                    }
                });
            });

            function showTooltip(x, y, contents) {
                $('<div id="tooltip">' + contents + '</div>').css({
                    position: 'absolute',
                    display: 'none',
                    top: y - 30,
                    left: x - 50,
                    color: "#fff",
                    padding: '2px 5px',
                    'border-radius': '6px',
                    'background-color': '#000',
                    opacity: 0.80
                }).appendTo("body").fadeIn(200);
            }

            var previousPoint = null;
            $("#statsChart").bind("plothover", function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;

                        $("#tooltip").remove();
                        var x = item.datapoint[0].toFixed(0),
                            y = item.datapoint[1].toFixed(0);

                        var month = item.series.xaxis.ticks[item.dataIndex].label;

                        showTooltip(item.pageX, item.pageY,
                            item.series.label + " of " + month + ": " + y);
                    }
                }
                else {
                    $("#tooltip").remove();
                    previousPoint = null;
                }
            });
        </script>






        <?php
        break;
    case 'exposure':
        ?>

        <?php
        break;
}
?>

<script>
    $(document).ready(function(){

        jQuery('input[name=friend_name]').keyup(function() {
            var name=$(this).val();
            var message=$('#invite_message').text();
            if(message!=""){
                var split_msg=message.split(",");
                split_msg[0]="Hi "+name;
                $('#invite_message').text(split_msg.toString())
                $('#invite_message').val(split_msg.toString())
            }
        });

    });
</script>

<script src="<?= ASSETS_PATH ?>js/jqueryui.mini.touch.punch.js"></script>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-53597013-1', 'auto');
ga('send', 'pageview');
</script>
</body>
</html>