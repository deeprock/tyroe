<?php

require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_coupons_allot_users_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' . EXT);
require_once(APPPATH . 'entities/constants' . EXT);

class Tyr_coupons_allot_users_entity extends Abstract_entity {

    //put your code here
    public $allot_id;
    public $coupon_id;
    public $user_id;
    public $coupon_label;
    public $coupon_value;
    public $coupon_type;
    public $coupon_status;
    public $coupon_issued;
    public $coupon_used;
    public $coupon_used_for;
    public $status_sl;

    public function __construct() {
        $this->allot_id = 0;
        $this->coupon_id = 0;
        $this->user_id = 0;
        $this->coupon_label = '';
        $this->coupon_value = 0;
        $this->coupon_type = '';
        $this->coupon_status = 0;
        $this->coupon_issued = 0;
        $this->coupon_used = 0;
        $this->coupon_used_for = 0;
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }

    public function save_coupons_allot_users() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $tyr_coupons_allot_users_DOA->save_coupons_allot_users($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return true;
    }

    public function get_coupons_allot_users() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $tyr_coupons_allot_users_DOA->get_coupons_allot_users($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }

    public function get_current_coupon_by_user_id() {
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $return_array = $tyr_coupons_allot_users_DOA->get_current_coupon_by_user_id($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_current_coupons_by_user_id() {
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $return_array = $tyr_coupons_allot_users_DOA->get_all_current_coupons_by_user_id($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_all_current_coupons_by_user_id_and_status() {
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $return_array = $tyr_coupons_allot_users_DOA->get_all_current_coupons_by_user_id_and_status($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    public function update_status_sl(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_coupons_allot_users_DOA = new Tyr_coupons_allot_users_dao($db_connection);
            $status = $tyr_coupons_allot_users_DOA->update_status_sl($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }

}