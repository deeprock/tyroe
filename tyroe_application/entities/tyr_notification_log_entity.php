<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_notification_log_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_notification_log_entity extends Abstract_entity{
    //put your code here
    public $notfiy_log_id;
    public $send_to;
    public $notification_id;
    public $notification_status;
    public $status_sl;
    public $created_timestamp;
   
    public function __construct() {
        $this->notfiy_log_id = 0;
        $this->send_to = '';
        $this->notification_id = '';
        $this->notification_status = 0;
        $this->status_sl = 0;
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_notification_log(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_notification_log_DOA = new Tyr_notification_log_dao($db_connection);
            $tyr_notification_log_DOA->save_notification_log($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_notification_log(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_notification_log_DOA = new Tyr_notification_log_dao($db_connection);
            $tyr_notification_log_DOA->get_notification_log($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
