<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_deleted_account_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_deleted_account_entity extends Abstract_entity{
    //put your code here
    public $acc_id;
    public $user_id;
    public $suggestions;
    public $status_sl;
    
    public function __construct() {
        $this->acc_id = 0;
        $this->user_id = 0;
        $this->suggestions = '';
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_deleted_account(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_deleted_account_DOA = new Tyr_deleted_account_dao($db_connection);
            $tyr_deleted_account_DOA->save_deleted_account($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_deleted_account(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_deleted_account_DOA = new Tyr_deleted_account_dao($db_connection);
            $tyr_deleted_account_DOA->get_deleted_account($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
