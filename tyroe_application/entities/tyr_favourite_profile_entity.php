<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_favourite_profile_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_favourite_profile_entity extends Abstract_entity{
    //put your code here
    public $fav_profile_id;
    public $user_id;
    public $studio_id;
    public $status_sl;
    public $created_timestamp = 0;
    
    public function __construct() {
        $this->fav_profile_id = 0;
        $this->user_id = 0;
        $this->studio_id = 0;
        $this->status_sl = 0;
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_favourite_profile(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $tyr_favourite_profile_DOA->save_favourite_profile($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
            return false;
        }
        PDO_utils::close_connection($db_connection);
        return true;
    }
     
    public function get_favourite_profile(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $tyr_favourite_profile_DOA->get_favourite_profile($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function check_user_already_favourite(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $return_array = $tyr_favourite_profile_DOA->check_user_already_favourite($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_favourite_status(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $status = $tyr_favourite_profile_DOA->update_favourite_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
        
    }
    
    public function delete_favourite_status(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $tyr_favourite_profile_DOA->delete_favourite_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        
    }
    
    public function check_user_favourite(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $return_array = $tyr_favourite_profile_DOA->check_user_favourite($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_favourite_profile_id(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_favourite_profile_DOA = new Tyr_favourite_profile_dao($db_connection);
            $return_array = $tyr_favourite_profile_DOA->get_favourite_profile_id($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
