<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_activity_log_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_activity_log_entity extends Abstract_entity{
    //put your code here
    public $activity_id;
    public $data;
    public $user_id;
    public $table_name;
    public $ip_address;
    public $activity_status;
    public $created_timestamp;
    
    public function __construct() {
        $this->activity_id = 0;
        $this->data = '';
        $this->user_id = 0;
        $this->table_name = 0;
        $this->ip_address = '';
        $this->activity_status = '';
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_activity_log(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_activity_log_DOA = new Tyr_activity_log_dao($db_connection);
            $tyr_activity_log_DOA->save_activity_log($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_activity_log(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_activity_log_DOA = new Tyr_activity_log_dao($db_connection);
            $tyr_activity_log_DOA->get_activity_log($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
