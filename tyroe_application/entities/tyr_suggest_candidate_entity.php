<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_suggest_candidate_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_suggest_candidate_entity extends Abstract_entity{
    //put your code here
    public $suggest_id;
    public $user_id;
    public $jobs_id;
    public $studio_id;
    public $reviewer_id;
    public $status_sl;
    
    public function __construct() {
        $this->suggest_id = 0;
        $this->user_id = 0;
        $this->jobs_id = 0;
        $this->studio_id = 0;
        $this->reviewer_id = 0;
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_suggest_candidate(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_suggest_candidate_DOA = new Tyr_suggest_candidate_dao($db_connection);
            $tyr_suggest_candidate_DOA->save_suggest_candidate($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_suggest_candidate(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_suggest_candidate_DOA = new Tyr_suggest_candidate_dao($db_connection);
            $tyr_suggest_candidate_DOA->get_suggest_candidate($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
