<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_pageviews_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_pageviews_entity extends Abstract_entity{
    //put your code here
    public $visit_id;
    public $user_id;
    public $session_id;
    public $user_type;
    public $page_url;
    public $user_info_detail;
    public $user_ip;
    public $visit_time;
    

    public function __construct() {
        $this->visit_id = 0;
        $this->user_id = 0;
        $this->session_id = '';
        $this->user_type = 0;
        $this->page_url = '';
        $this->user_info_detail	= '';
        $this->user_ip	= 0;
        $this->visit_time = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_pageviews(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_pageviews_DOA = new Tyr_pageviews_dao($db_connection);
            $tyr_pageviews_DOA->save_pageviews($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
            var_dump($e->getFile().$e->getLine());
            die();
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_pageviews(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_pageviews_DOA = new Tyr_pageviews_dao($db_connection);
            $tyr_pageviews_DOA->get_pageviews($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_todays_pageview_count($start_day_unixtime,$end_day_unixtime){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_pageviews_DOA = new Tyr_pageviews_dao($db_connection);
            $return_array = $tyr_pageviews_DOA->get_todays_pageview_count($this,$start_day_unixtime,$end_day_unixtime);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
