<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_profile_view_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_profile_view_entity extends Abstract_entity{
    //put your code here
    public $profile_view_id;
    public $viewer_id;
    public $viewer_type;
    public $job_id;
    public $user_id;
    public $created_timestamp;
    
    public function __construct() {
        $this->profile_view_id = 0;
        $this->viewer_id = 0;
        $this->viewer_type = 0;
        $this->job_id = 0;
        $this->user_id = 0;
        $this->created_timestamp = time();
        $this->set_CCUU_to_now(0);
    }
    
    public function save_profile_view(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $tyr_profile_view_DOA->save_profile_view($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return true;
    }
     
    public function get_profile_view(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $tyr_profile_view_DOA->get_profile_view($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_viewer_count_by_viewer_type(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_viewer_count_by_viewer_type($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_viewer_id_count_by_viewer_type(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_viewer_id_count_by_viewer_type($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_viewer_count_by_viewer_type_IN($type_in){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_viewer_count_by_viewer_type_IN($this,$type_in);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_already_view_by_user(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->check_already_view_by_user($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_profile_view_month_chart(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_profile_view_month_chart($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_profile_view_month_chart_other(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_profile_view_month_chart_other($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_profile_view_week_chart(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_profile_view_week_chart($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_profile_view_week_chart_other(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_profile_view_DOA = new Tyr_profile_view_dao($db_connection);
            $return_array = $tyr_profile_view_DOA->get_profile_view_week_chart_other($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
