<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_education_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_education_entity extends Abstract_entity{
    //put your code here
    public $education_id;
    public $user_id;
    public $edu_organization;
    public $edu_url;
    public $edu_position;
    public $edu_country;
    public $edu_city;
    public $institute_name;
    public $degree_title;
    public $field_interest;
    public $grade_obtain;
    public $start_year;
    public $end_year;
    public $activities;
    public $education_description;
    public $created;
    public $modified;
    public $status_sl;
    
    public function __construct() {
        $this->education_id = 0;
        $this->user_id = 0;
        $this->edu_organization = '';
        $this->edu_url = '';
        $this->edu_position = '';
        $this->edu_country = 0;
        $this->edu_city = '';
        $this->institute_name = '';
        $this->degree_title = '';
        $this->field_interest = '';
        $this->grade_obtain = '';
        $this->start_year = 0;
        $this->end_year = 0;
        $this->activities = '';
        $this->education_description = '';
        $this->status_sl = 0;
        $this->modified = 0;
        $this->created = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_education(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $tyr_education_DOA->save_education($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return TRUE;
    }
     
    public function get_education(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $tyr_education_DOA->get_education($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_user_education(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $tyr_education_DOA->get_user_education($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    

    public function get_all_user_education_for_profile(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $return_array = $tyr_education_DOA->get_all_user_education_for_profile($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_user_all_education(){
        $db_connection = PDO_utils::db_connection();
         $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $return_array = $tyr_education_DOA->get_user_all_education($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_education(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $status = $tyr_education_DOA->update_education($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function get_user_education_count(){
        $db_connection = PDO_utils::db_connection();
         $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_education_DOA = new Tyr_education_dao($db_connection);
            $return_array = $tyr_education_DOA->get_user_education_count($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
