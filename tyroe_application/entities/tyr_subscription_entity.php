<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_subscription_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_subscription_entity extends Abstract_entity{
    //put your code here
    public $subscription_id;
    public $tyroe_id ;
    public $subscription_type;
    public $subscription_coupon;
    public $subscription_use_coupon;
    public $subscription_payment_type;
    public $subscription_card_type;
    public $subscription_card_num;
    public $subscription_name;
    public $subscription_expiry_month;
    public $subscription_expiry_year;
    public $subscription_ccv;
    public $status_sl;
    
    public function __construct() {
        $this->subscription_id = 0;
        $this->tyroe_id = 0;
        $this->subscription_type = '';
        $this->subscription_coupon = '';
        $this->subscription_use_coupon = 0;
        $this->subscription_payment_type = '';
        $this->subscription_card_type = '';
        $this->subscription_card_num = '';
        $this->subscription_name = '';
        $this->subscription_expiry_month = 0;
        $this->subscription_expiry_year = 0;
        $this->subscription_ccv = 0;
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_subscription(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_subscription_DOA = new Tyr_subscription_dao($db_connection);
            $tyr_subscription_DOA->save_subscription($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return TRUE;
    }
     
    public function get_subscription(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_subscription_DOA = new Tyr_subscription_dao($db_connection);
            $tyr_subscription_DOA->get_subscription($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function update_subscription(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_subscription_DOA = new Tyr_subscription_dao($db_connection);
            $status = $tyr_subscription_DOA->update_subscription($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
}
