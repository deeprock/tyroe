<?php

class Constants {
    const ERROR_INVALID_INPUT_FORMAT = 'Invalid input format';
    const ERROR_NO_DB_CONNECTION = 'Unable to obtain DB connection';
}
