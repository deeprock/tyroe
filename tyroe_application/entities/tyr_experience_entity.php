<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_experience_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_experience_entity extends Abstract_entity{
    //put your code here
    public $exp_id;
    public $user_id;
    public $company_name;
    public $job_title;
    public $job_location;
    public $job_start;
    public $job_end;
    public $job_description;
    public $job_url;
    public $job_country;
    public $job_city;
    public $current_job;
    public $status_sl;    
    public $created;
    
    public function __construct() {
        $this->exp_id = 0;
        $this->user_id = 0;
        $this->company_name = '';
        $this->job_title = '';
        $this->job_location = '';
        $this->job_start = 0;
        $this->job_end = 0;
        $this->job_description = '';
        $this->job_url = '';
        $this->job_country = 0;
        $this->job_city = '';
        $this->current_job = 0;
        $this->status_sl = 0;
        $this->created = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_experience(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $tyr_experience_DOA->save_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return true;
    }
     
    public function get_experience(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $tyr_experience_DOA->get_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_user_experience(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $return_array = $tyr_experience_DOA->get_user_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_experience_cnt(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $return_array = $tyr_experience_DOA->get_user_experience_cnt($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_all_exp_by_user_id_and_status(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $return_array = $tyr_experience_DOA->get_all_exp_by_user_id_and_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_user_all_experience(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $return_array = $tyr_experience_DOA->get_user_all_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_experience_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $return_array = $tyr_experience_DOA->get_user_experience_count($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_experience(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $status = $tyr_experience_DOA->update_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_user_experience_updated_at(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $status = $tyr_experience_DOA->update_user_experience_updated_at($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_user_all_experience(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_experience_DOA = new Tyr_experience_dao($db_connection);
            $status = $tyr_experience_DOA->update_user_all_experience($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
}
