<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_invite_tyroe_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_invite_tyroe_entity extends Abstract_entity{
    //put your code here
    public $invite_tyr_id;
    public $reviewer_id;
    public $job_id;
    public $tyroe_id;
    public $invitation_status;
    public $status_sl;
    
    public function __construct() {
        $this->invite_tyr_id = 0;
        $this->reviewer_id = 0;
        $this->job_id = 0;
        $this->tyroe_id = 0;
        $this->invitation_status = 0;
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_invite_tyroe(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_invite_tyroe_DOA = new Tyr_invite_tyroe_dao($db_connection);
            $tyr_invite_tyroe_DOA->save_invite_tyroe($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_invite_tyroe(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_tyroe_DOA = new Tyr_invite_tyroe_dao($db_connection);
            $tyr_invite_tyroe_DOA->get_invite_tyroe($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_tyroe_invite_job_ids(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_tyroe_DOA = new Tyr_invite_tyroe_dao($db_connection);
            $return_array = $tyr_invite_tyroe_DOA->get_tyroe_invite_job_ids($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
     public function get_invite_job_count_by_tyroe(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_tyroe_DOA = new Tyr_invite_tyroe_dao($db_connection);
            $return_array = $tyr_invite_tyroe_DOA->get_invite_job_count_by_tyroe($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
