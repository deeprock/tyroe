<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_sessions_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_sessions_entity extends Abstract_entity{
    //put your code here
    public $id;
    public $session_id;
    public $ip_address;
    public $user_agent;
    public $last_activity;
    public $user_data;
    
    public function __construct() {
        $this->id = 0;
        $this->sessions_id = '';
        $this->ip_address = '';
        $this->user_agent = '';
        $this->last_activity = 0;
        $this->user_data = '';
        $this->set_CCUU_to_now(0);
    }
    
    public function save_sessions(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_sessions_DOA = new Tyr_sessions_dao($db_connection);
            $tyr_sessions_DOA->save_sessions($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_sessions(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_sessions_DOA = new Tyr_sessions_dao($db_connection);
            $tyr_sessions_DOA->get_sessions($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
