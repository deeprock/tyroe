<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_visibility_options_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_visibility_options_entity extends Abstract_entity{
    //put your code here
    public $visiblity_id;
    public $user_id ;
    public $visible_section;
    public $visibility;
    public $status_sl;
    
    public function __construct() {
        $this->visiblity_id = 0;
        $this->user_id = 0;
        $this->visible_section = '';
        $this->visibility = 0;
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_visibility_options(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_visibility_options_DOA = new Tyr_visibility_options_dao($db_connection);
            $tyr_visibility_options_DOA->save_visibility_options($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_visibility_options(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_visibility_options_DOA = new Tyr_visibility_options_dao($db_connection);
            $tyr_visibility_options_DOA->get_visibility_options($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    

//    public function get_visibility_by_user_and_status(){
//        $db_connection = PDO_utils::db_connection();
//        $return_array = '';
//        if($db_connection == null) {
//                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
//                return false;
//        }
//        try{
//            $db_connection->beginTransaction();
//            $tyr_visibility_options_DOA = new Tyr_visibility_options_dao($db_connection);
//            $return_array = $tyr_visibility_options_DOA->get_visibility_by_user_and_status($this);
//            $db_connection->commit();
//        }catch(Exception $e){
//            $error = API_message::error_message($e->getMessage());
//            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
//        }
//        PDO_utils::close_connection($db_connection);
//        return $return_array;
//    }

    public function get_all_user_visibility_options(){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_visibility_options_DOA = new Tyr_visibility_options_dao($db_connection);
            $return_array = $tyr_visibility_options_DOA->get_all_user_visibility_options($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_user_visibility_of_section(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_visibility_options_DOA = new Tyr_visibility_options_dao($db_connection);
            $status = $tyr_visibility_options_DOA->update_user_visibility_of_section($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
}
