<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_industry_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_industry_entity extends Abstract_entity{
    //put your code here
    public $industry_id;
    public $industry_name;
    public $status_sl;
    
    public function __construct() {
        $this->industry_id = 0;
        $this->industry_name = '';
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_industry(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_industry_DOA = new Tyr_industry_dao($db_connection);
            $tyr_industry_DOA->save_industry($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_industry(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_industry_DOA = new Tyr_industry_dao($db_connection);
            $tyr_industry_DOA->get_industry($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_all_industries(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_industry_DOA = new Tyr_industry_dao($db_connection);
            $return_array = $tyr_industry_DOA->get_all_industries();
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_industries_by_keyword($search_input){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_industry_DOA = new Tyr_industry_dao($db_connection);
            $return_array = $tyr_industry_DOA->get_all_industries_by_keyword($search_input);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
