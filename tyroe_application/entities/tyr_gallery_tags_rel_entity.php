<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_gallery_tags_rel_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_gallery_tags_rel_entity extends Abstract_entity{
    //put your code here
    public $gallerytag_id;
    public $gallerytag_name_id;
    public $media_image_id;
    public $add_time;
    public $delete_time;
    public $status;
    
    public function __construct() {
        $this->gallerytag_id = 0;
        $this->gallerytag_name_id = 0;
        $this->media_image_id = 0;
        $this->add_time = 0;
        $this->delete_time = 0;
        $this->status = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_gallery_tags_rel(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $tyr_gallery_tags_rel_DOA->save_gallery_tags_rel($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_gallery_tags_rel(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $tyr_gallery_tags_rel_DOA->get_gallery_tags_rel($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_already_tag_ref(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $return_array = $tyr_gallery_tags_rel_DOA->get_already_tag_ref($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    
    public function update_media_tag_image_status(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $status = $tyr_gallery_tags_rel_DOA->update_media_tag_image_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_tag_rel_status(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $status = $tyr_gallery_tags_rel_DOA->update_tag_rel_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_status_name_id_not_in($where_not_in_update){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_rel_DOA = new Tyr_gallery_tags_rel_dao($db_connection);
            $status = $tyr_gallery_tags_rel_DOA->update_status_name_id_not_in($this,$where_not_in_update);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
}
