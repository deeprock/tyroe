<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_job_activity_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_job_activity_entity extends Abstract_entity{
    //put your code here
    public $job_activity_id;
    public $performer_id;
    public $job_id;
    public $object_id;
    public $activity_data;
    public $activity_type;
    public $user_seen;
    public $status_sl;
    public $created_timestamp;
    
    public function __construct() {
        $this->job_activity_id = 0;
        $this->performer_id = 0;
        $this->job_id = 0;
        $this->object_id = 0;
        $this->activity_data = '';
        $this->activity_type = '';
        $this->user_seen = 0;
        $this->status_sl = 0;
        $this->created_timestamp = strtotime("now");
        $this->set_CCUU_to_now(0);
    }
    
    public function save_job_activity(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_job_activity_DOA = new Tyr_job_activity_dao($db_connection);
            $tyr_job_activity_DOA->save_job_activity($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_job_activity(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_activity_DOA = new Tyr_job_activity_dao($db_connection);
            $tyr_job_activity_DOA->get_job_activity($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_job_activity_count_by_user_id(){
        $db_connection = PDO_utils::db_connection();
        $count = array();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_activity_DOA = new Tyr_job_activity_dao($db_connection);
            $count = $tyr_job_activity_DOA->get_job_activity_count_by_user_id($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $count;
    }
    
    public function update_job_activity_user_seen(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_activity_DOA = new Tyr_job_activity_dao($db_connection);
            $status = $tyr_job_activity_DOA->update_job_activity_user_seen($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
}
