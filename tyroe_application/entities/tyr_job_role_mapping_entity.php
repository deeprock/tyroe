<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_job_role_mapping_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_job_role_mapping_entity extends Abstract_entity{
    //put your code here
    public $id;
    public $job_id;
    public $user_id;
    public $is_admin;
    public $is_reviewer;
    
    public function __construct() {
        $this->id = 0;
        $this->job_id = 0;
        $this->user_id = 0;
        $this->is_admin = 0;
        $this->is_reviewer = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_job_role_mapping(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $tyr_job_role_mapping_DOA->save_job_role_mapping($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_job_role_mapping(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $tyr_job_role_mapping_DOA->get_job_role_mapping($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function update_job_role_mapping(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $status = $tyr_job_role_mapping_DOA->update_job_role_mapping($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
        
    }
    
    public function delete_job_role_mapping(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $status = $tyr_job_role_mapping_DOA->delete_job_role_mapping($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
        
    }
    
    public function get_user_job_ids(){
        $db_connection = PDO_utils::db_connection();
        $return_array = array();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $return_array = $tyr_job_role_mapping_DOA->get_user_job_ids($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewers_for_job(){
        $db_connection = PDO_utils::db_connection();
        $return_array = array();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $return_array = $tyr_job_role_mapping_DOA->get_all_reviewers_for_job($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_admins_for_job(){
        $db_connection = PDO_utils::db_connection();
        $return_array = array();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_job_role_mapping_DOA = new Tyr_job_role_mapping_dao($db_connection);
            $return_array = $tyr_job_role_mapping_DOA->get_all_admins_for_job($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
}
?>
