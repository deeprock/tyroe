<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_category_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_category_entity extends Abstract_entity{
    //put your code here
    public $category_id;
    public $category_name;
    
    public function __construct() {
        $this->category_id = 0;
        $this->category_name = '';
        $this->set_CCUU_to_now(0);
    }
    
    public function save_category(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_category_DOA = new Tyr_category_dao($db_connection);
            $tyr_category_DOA->save_category($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_category(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_category_DOA = new Tyr_category_dao($db_connection);
            $tyr_category_DOA->get_category($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
