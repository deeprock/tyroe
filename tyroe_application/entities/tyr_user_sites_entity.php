<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_user_sites_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_user_sites_entity extends Abstract_entity{
    //put your code here
    public $id;
    public $user_id ;
    public $other_site_id;
    public $site_name;
    public $status_sl;
    
    public function __construct() {
        $this->id = 0;
        $this->user_id = 0;
        $this->other_site_id = 0;
        $this->site_name = '';
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_user_sites(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_user_sites_DOA = new Tyr_user_sites_dao($db_connection);
            $tyr_user_sites_DOA->save_user_sites($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_user_sites(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_user_sites_DOA = new Tyr_user_sites_dao($db_connection);
            $tyr_user_sites_DOA->get_user_sites($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_user_sites_by_user_id(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_user_sites_DOA = new Tyr_user_sites_dao($db_connection);
            $return_array = $tyr_user_sites_DOA->get_user_sites_by_user_id($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_all_user_sites(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_user_sites_DOA = new Tyr_user_sites_dao($db_connection);
            $return_array = $tyr_user_sites_DOA->get_all_user_sites($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function delete_user_sites(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_user_sites_DOA = new Tyr_user_sites_dao($db_connection);
            $tyr_user_sites_DOA->delete_user_sites($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
}
