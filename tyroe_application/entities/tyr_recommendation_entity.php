<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_recommendation_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_recommendation_entity extends Abstract_entity{
    //put your code here
    public $recommend_id;
    public $user_id;
    public $studio_id;
    public $job_id;
    public $recommend_email;
    public $recommendation;
    public $recommend_name;
    public $recommend_company;
    public $recommend_dept;
    public $status_sl;
    public $recommend_status;
    public $created_timestamp;
    
    public function __construct() {
        $this->recommend_id = 0;
        $this->user_id = 0;
        $this->studio_id = 0;
        $this->job_id = 0;
        $this->recommend_email = '';
        $this->recommendation = '';
        $this->recommend_name = '';
        $this->recommend_company = '';
        $this->recommend_dept = '';
        $this->status_sl = 0;
        $this->recommend_status = 0;
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_recommendation(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $tyr_recommendation_DOA->save_recommendation($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_recommendation(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $tyr_recommendation_DOA->get_recommendation($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }

    public function get_user_all_recommendation_by_status(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $return_array = $tyr_recommendation_DOA->get_user_all_recommendation_by_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_recommendation_by_id(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $tyr_recommendation_DOA->update_recommendation_by_id($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }

    public function get_user_all_recommendation(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $tyr_recommendation_DOA->get_user_all_recommendation($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_all_recommendation_approvallist(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $return_array = $tyr_recommendation_DOA->get_all_recommendation_approvallist($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_recommendation_exist(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $return_array = $tyr_recommendation_DOA->check_recommendation_exist($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_recommendation_exist_1(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $return_array = $tyr_recommendation_DOA->check_recommendation_exist_1($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_all_recommendation_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $return_array = $tyr_recommendation_DOA->get_user_all_recommendation_count($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_all_data(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendation_DOA = new Tyr_recommendation_dao($db_connection);
            $status = $tyr_recommendation_DOA->update_all_data($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();
            //var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
}
