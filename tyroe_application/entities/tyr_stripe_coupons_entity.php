<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_stripe_coupons_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_stripe_coupons_entity extends Abstract_entity{
    //put your code here
    
    public $id;
    public $coupon_id;
    public $cash_discount;
    public $per_discount;
    public $max_use;
    public $expire_by;
    public $coupon_created_at;
    public $status;
    
    public function __construct() {
        $this->id = 0;
        $this->coupon_id = '';
        $this->cash_discount = 0;
        $this->per_discount = 0;
        $this->max_use = 0;
        $this->expire_by = 0;
        $this->coupon_created_at = 0;
        $this->status = 1;
    }
    
    public function save(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_stripe_coupons_dao = new Tyr_stripe_coupons_dao($db_connection);
            $tyr_stripe_coupons_dao->save($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_stripe_coupons_dao = new Tyr_stripe_coupons_dao($db_connection);
            $tyr_stripe_coupons_dao->get($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_by_coupon_id(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_stripe_coupons_dao = new Tyr_stripe_coupons_dao($db_connection);
            $tyr_stripe_coupons_dao->get_by_coupon_id($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function update(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_stripe_coupons_dao = new Tyr_stripe_coupons_dao($db_connection);
            $tyr_stripe_coupons_dao->update($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function delete(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_stripe_coupons_dao = new Tyr_stripe_coupons_dao($db_connection);
            $tyr_stripe_coupons_dao->delete($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
