<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_joins_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_joins_entity extends Abstract_entity{
    //put your code here
    
   
    public function __construct() {

    }
    
    public function get_all_modules_by_user_id($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_modules_by_user_id($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_company_by_user_id($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_company_by_user_id($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_profile_viewer_type($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_profile_viewer_type($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_shortlisted_users_jobs($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_shortlisted_users_jobs($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_jobs_skill_by_job_id($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_jobs_skill_by_job_id($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_invite_candidate_users_jobs($job_id,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_invite_candidate_users_jobs($job_id,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_review_count_by_user_reviews($user_id,$where_feedback){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_invite_candidate_users_jobs($user_id,$where_feedback);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_feedback_by_reviews($user_id,$where_feedback,$order_by_pagintaion){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_feedback_by_reviews($user_id,$where_feedback,$order_by_pagintaion);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_candidate(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_candidate();
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    
    public function get_candidate_details_by_candidate_id($candidate_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_candidate_details_by_candidate_id($candidate_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_user_skill($candidate_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_user_skill($candidate_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_email_data_by_user_id($candidate_id, $on = ''){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_email_data_by_user_id($candidate_id, $on);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_jobs_details($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_jobs_details($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_jobs_details_1($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_jobs_details_1($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_team_admin($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_team_admin($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_top_candidate_details($hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_top_candidate_details($hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_activity_stream($where,$order="",$pagination_limit=""){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_activity_stream($where,$order,$pagination_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_jobs_openings_listing($studio_id,$reviewer_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_jobs_openings_listing($studio_id,$reviewer_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_jobs_openings_listing_1($studio_id,$reviewer_id,$job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_jobs_openings_listing_1($studio_id,$reviewer_id,$job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_tyroe_featured_approval(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_tyroe_featured_approval();
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_studios_featured_approval($where,$order_by,$limit,$studio_role){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_studios_featured_approval($where,$order_by,$limit,$studio_role);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_personal_info($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_personal_info($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_personal_info_1($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_personal_info_1($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_row_count_jobopening($user_id,$where,$where_job){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_jobopening($user_id,$where,$where_job);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_job_details_paginated($user_id,$where,$where_job,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_job_details_paginated($user_id,$where,$where_job,$order_by,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_skill_data_for_job($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_skill_data_for_job($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_edit_job_details($user_id,$edit_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_edit_job_details($user_id,$edit_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_email_data_by_job_id($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_email_data_by_job_id($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    
    public function get_total_row_count_archive_job($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_archive_job($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_job_details_archive_job($where,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_job_details_archive_job($where,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_invite_jobs($get_invite_jobs,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_invite_jobs($get_invite_jobs,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_studio_details_by_job_id($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_studio_details_by_job_id($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_row_count_filter_job($user_id,$where,$where_values){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_filter_job($user_id,$where,$where_values);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_job_details_filter_job($user_id,$where,$where_values,$orderby,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_job_details_filter_job($user_id,$where,$where_values,$orderby,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_invite_jobs_details($get_invite_jobs,$user_id,$where_values,$orderby){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_invite_jobs_details($get_invite_jobs,$user_id,$where_values,$orderby);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_shortlist_invite_jobs_details($user_id,$where_values,$orderby){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_shortlist_invite_jobs_details($user_id,$where_values,$orderby);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_studio_admin_details($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_studio_admin_details($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function search_existing_reviewers($join,$user_id,$searching_value){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->search_existing_reviewers($join,$user_id,$searching_value);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function search_existing_reviewers_1($join,$user_id,$parent_id,$searching_value){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->search_existing_reviewers_1($join,$user_id,$parent_id,$searching_value);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_added_reviewer_details($parent_id,$job_id,$reviewer_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_added_reviewer_details($parent_id,$job_id,$reviewer_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_job_details_by_job_id($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_job_details_by_job_id($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewers_for_studio_job($job_id,$user_id,$parent_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_reviewers_for_studio_job($job_id,$user_id,$parent_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
     public function get_all_job_team_member($job_id,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_job_team_member($job_id,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_anonymous_feedback_by_job($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_anonymous_feedback_by_job($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewers($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_reviewers($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewers_by_job_id($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_reviewers_by_job_id($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_applicant_for_job($selection,$rate_and_review_join,$job_id,$job_detail_status,$user_id,$hidden_user,$group_by,$order_by){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_applicant_for_job($selection,$rate_and_review_join,$job_id,$job_detail_status,$user_id,$hidden_user,$group_by,$order_by);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_candidate_by_data_query($data_query){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_candidate_by_data_query($data_query);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_review_details($job_id,$candidate_id,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_review_details($job_id,$candidate_id,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_shortlisted_users($job_id,$user_id,$hidden_user,$order_by){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_shortlisted_users($job_id,$user_id,$hidden_user,$order_by);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_hidden_candidate_for_job($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_hidden_candidate_for_job($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_email_data_for_tyroe($on,$job_id,$reviewer_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_email_data_for_tyroe($on,$job_id,$reviewer_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_email_data_for_user_job($user_id,$job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_email_data_for_user_job($user_id,$job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewers_by_studio_job($user_id,$job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_reviewers_by_studio_job($user_id,$job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_job_activity_data($job_id,$user_id,$tyroe_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_job_activity_data($job_id,$user_id,$tyroe_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_rate_review_detail($review_id,$job_id,$tyroe_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_tyroe_rate_review_detail($review_id,$job_id,$tyroe_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_reviewer_detail_by_studio($tyroe_id,$job_id,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_reviewer_detail_by_studio($tyroe_id,$job_id,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_shotlist_for_pdf($job_id,$user_id,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_shotlist_for_pdf($job_id,$user_id,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_csv_info($user_id,$job_id,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_csv_info($user_id,$job_id,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
     public function get_csv_reviews($user_id,$job_id,$ccv_user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_csv_reviews($user_id,$job_id,$ccv_user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_email_data_for_tyroe_1($on,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_email_data_for_tyroe_1($on,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_job_row_count($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_job_row_count($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_job_row_count_1($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_job_row_count_1($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_job_row_paginated($where,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_job_row_paginated($where,$order_by,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_skills_for_job($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_skills_for_job($job_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_data_image($data,$user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_data_image($data,$user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_video_group_img_id($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->check_video_group_img_id($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_with_profile_image($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_with_profile_image($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_search_listing_studio_data($role_id,$where,$orderby,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_search_listing_studio_data($role_id,$where,$orderby,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_search_listing_users_data($role_id,$where,$orderby,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_search_listing_users_data($role_id,$where,$orderby,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_featured_details($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_featured_details($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_studio_details_record($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_studio_details_record($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_subscription_paginated($where,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_subscription_paginated($where,$order_by,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_subscription_id_count($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_subscription_id_count($where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_studio_coupons_paginated($where,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_studio_coupons_paginated($where,$order_by,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_row_count_for_cupons($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_for_cupons($where);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_user_profile($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_profile($user_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_total_row_count_activity_stream($where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_activity_stream($where);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_featured_user_row_count($featured_join,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_featured_user_row_count($featured_join,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_featured_users_detail_page($featured_join,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_featured_users_detail_page($featured_join,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_featured_tyroe_detail_paginated($featured_join,$hidden_user,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_featured_tyroe_detail_paginated($featured_join,$hidden_user,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_tyroe_details_without_limit($query){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_tyroe_details_without_limit($query);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_tyroe_details_with_limit($query){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_tyroe_details_with_limit($query);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_all_experience($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_all_experience($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_all_user_skills_for_profile($user_id, $status){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_user_skills_for_profile($user_id, $status);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_all_skill_by_users($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_skill_by_users($user_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_job_details_for_public_page($job_id, $status){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_job_details_for_public_page($job_id, $status);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
            
    }

    public function get_users_speciality($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction(); 
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_users_speciality($user_id);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_user_profile_score($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_profile_score($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_resume_score($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_resume_score($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_portfolio_score($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_portfolio_score($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_detail_info($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_detail_info($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();
            //var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_users_recommendation_sentlist($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_users_recommendation_sentlist($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_user_all_skill_count($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_user_all_skill_count($user_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_latest_work_media($user_id,$column1_start){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_latest_work_media($user_id,$column1_start);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_latest_work_media_with_limit($user_id,$column2_start,$total_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_latest_work_media_with_limit($user_id,$column2_start,$total_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_already_invites($invite_email,$job_id,$studio_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->check_already_invites($invite_email,$job_id,$studio_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function check_already_invites_1($invite_email,$job_id,$studio_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->check_already_invites_1($invite_email,$job_id,$studio_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_row_count_jobopeining($user_id,$where,$where_job){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_jobopeining($user_id,$where,$where_job);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_row_count_favourite_profile($user_id,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_favourite_profile($user_id,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_favourite_profile($user_id,$hidden_user){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_favourite_profile($user_id,$hidden_user);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_user_skill_with_limit($candidate_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_user_skill_with_limit($candidate_id);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_total_featured_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_featured_count();
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_featured_users_details_with_limit($pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_featured_users_details_with_limit($pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_featured_users_details(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_featured_users_details();
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    public function get_count_by_feedback($user_id,$where_feedback){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_count_by_feedback($user_id,$where_feedback);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    public function get_all_feedback($user_id,$where_feedback,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_feedback($user_id,$where_feedback,$order_by,$pagintaion_limit);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_count_for_filter_feedback($user_id,$where_feedback,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_count_for_filter_feedback($user_id,$where_feedback,$order_by,$pagintaion_limit);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_all_jobs_details_not_for_hidden($hidden_user,$where,$order_by,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = array();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_jobs_details_not_for_hidden($hidden_user,$where,$order_by,$pagintaion_limit);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_job_details_by_job_id_1($job_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_job_details_by_job_id_1($job_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_all_total_gallary_records_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_total_gallary_records_count();
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }


    public function get_search_listing_studio_by_studio_role($where,$orderby,$pagintaion_limit){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_search_listing_studio_by_studio_role($where,$orderby,$pagintaion_limit);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_total_row_count_filter_team($user_id,$parent_id,$fillter_text){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_total_row_count_filter_team($user_id,$parent_id,$fillter_text);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    

    public function get_gallery_records_pagination($limit, $offset){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_gallery_records_pagination($limit, $offset);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_reviewer_filter_team($user_id,$parent_id,$fillter_text,$orderby,$pagintaion_limit){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_reviewer_filter_team($user_id,$parent_id,$fillter_text,$orderby,$pagintaion_limit);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tag_user_ids($search_input){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_tag_user_ids($search_input);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }

    public function get_all_openings_by_user_reviewer($user_id,$reviewer_id){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_openings_by_user_reviewer($user_id,$reviewer_id);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_all_favourite_profile_paginated($user_id, $hidden_user){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_all_favourite_profile_paginated($user_id, $hidden_user);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_studio_locations_list($selected_value){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_studio_locations_list($selected_value);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_studio_locations_city_ids($selected_value){

        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_studio_locations_city_ids($selected_value);
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_search_listing_studio_data_count($role_id,$where){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            
            $tyr_joins_DOA = new Tyr_joins_dao($db_connection);
            $return_array = $tyr_joins_DOA->get_search_listing_studio_data_count($role_id,$where);
            
            $db_connection->commit();
            
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            //
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
}
