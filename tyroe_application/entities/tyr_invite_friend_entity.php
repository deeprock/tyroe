<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_invite_friend_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_invite_friend_entity extends Abstract_entity{
    //put your code here
    public $invite_id;
    public $coupon_id;
    public $user_id;
    public $email;
    public $friend_name;
    public $invite_message;
    public $accpeted_timestamp;
    public $created_timestamp;
    public $status_sl;
    
    public function __construct() {
        $this->invite_id = 0;
        $this->coupon_id = 0;
        $this->user_id = 0;
        $this->email = '';
        $this->friend_name = '';
        $this->invite_message = 0;
        $this->accpeted_timestamp = 0;
        $this->created_timestamp = strtotime("now");
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_invite_friend(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_invite_friend_DOA = new Tyr_invite_friend_dao($db_connection);
            $tyr_invite_friend_DOA->save_invite_friend($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_invite_friend(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_friend_DOA = new Tyr_invite_friend_dao($db_connection);
            $tyr_invite_friend_DOA->get_invite_friend($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function update_status_sl(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_friend_DOA = new Tyr_invite_friend_dao($db_connection);
            $status = $tyr_invite_friend_DOA->update_status_sl($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
}
