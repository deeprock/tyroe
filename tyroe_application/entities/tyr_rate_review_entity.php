<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_rate_review_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_rate_review_entity extends Abstract_entity{
    //put your code here
    public $review_id;
    public $user_id;
    public $user_type;
    public $tyroe_id;
    public $job_id;
    public $level_id;
    public $technical;
    public $creative;
    public $impression;
    public $review_comment;
    public $action_shortlist;
    public $action_interview;
    public $anonymous_feedback_message;
    public $anonymous_feedback_status;
    public $status_sl;
    public $created_timestamp;
    public $moderated;
    
    public function __construct() {
        $this->review_id = 0;
        $this->user_id = 0;
        $this->user_type = 0;
        $this->tyroe_id = 0;
        $this->job_id = 0;
        $this->level_id = 0;
        $this->technical = 0;
        $this->creative = 0;
        $this->impression = 0;
        $this->review_comment = '';
        $this->action_shortlist = '';
        $this->action_interview = '';
        $this->anonymous_feedback_message = '';
        $this->anonymous_feedback_status = 0;
        $this->status_sl = 0;
        $this->moderated = 1;
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_rate_review(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $tyr_rate_review_DOA->save_rate_review($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_rate_review(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $tyr_rate_review_DOA->get_rate_review($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_all_rate_review_by_user_type($user_type,$profile_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_all_rate_review_by_user_type($this,$user_type,$profile_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_sum_all_rate_review_by_user(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_sum_all_rate_review_by_user($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_total_score(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_tyroe_total_score($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_score_total_1(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_tyroe_score_total_1($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_score_total_2(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_tyroe_score_total_2($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_rate_review(){
        $db_connection = PDO_utils::db_connection();
        $status = true;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $status = $tyr_rate_review_DOA->update_rate_review($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function get_anonymous_message(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_anonymous_message($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_rate_review(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_tyroe_rate_review($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_avg_review_score(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_tyroe_avg_review_score($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_pending_moderation_review_count($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_pending_moderation_review_count($user_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_pending_moderation_review($user_id){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->get_pending_moderation_review($user_id);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_pending_moderation(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_rate_review_DOA = new Tyr_rate_review_dao($db_connection);
            $return_array = $tyr_rate_review_DOA->update_pending_moderation($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
