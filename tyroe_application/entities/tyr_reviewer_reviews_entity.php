<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_reviewer_reviews_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_reviewer_reviews_entity extends Abstract_entity{
    //put your code here
    public $reviewer_reviews_id;
    public $studio_id;
    public $job_id;
    public $reviewer_id;
    public $tyroe_id;
    public $reviewed_type;
    public $reviewed_time;
    public $status_sl;
    
    public function __construct() {
        $this->reviewer_reviews_id = 0;
        $this->studio_id = 0;
        $this->job_id = 0;
        $this->reviewer_id = 0;
        $this->tyroe_id = 0;
        $this->reviewed_type = '';
        $this->reviewed_time = '';
        $this->status_sl = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_reviewer_reviews(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $tyr_reviewer_reviews_DOA->save_reviewer_reviews($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_reviewer_reviews(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $tyr_reviewer_reviews_DOA->get_reviewer_reviews($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_reviewer_reviews_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $return_array = $tyr_reviewer_reviews_DOA->get_reviewer_reviews_count($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_tyroe_reviews_count(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $return_array = $tyr_reviewer_reviews_DOA->get_tyroe_reviews_count($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get_reviews_id_count_for_job(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $return_array = $tyr_reviewer_reviews_DOA->get_reviews_id_count_for_job($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_reviewer_reviews(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $status = $tyr_reviewer_reviews_DOA->update_reviewer_reviews($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_reviewer_reviews_status(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $status = $tyr_reviewer_reviews_DOA->update_reviewer_reviews_status($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function get_all_reviews_count_by_job_id($reviewer_ids){
        $db_connection = PDO_utils::db_connection();
        $return_array = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_reviewer_reviews_DOA = new Tyr_reviewer_reviews_dao($db_connection);
            $return_array = $tyr_reviewer_reviews_DOA->get_all_reviews_count_by_job_id($this, $reviewer_ids);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
