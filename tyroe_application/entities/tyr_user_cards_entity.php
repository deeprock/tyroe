<?php

require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_user_cards_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' . EXT);
require_once(APPPATH . 'entities/constants' . EXT);

class Tyr_user_cards_entity extends Abstract_entity {

    //put your code here
    public $user_id;
    public $card_no;
    public $card_id;
    public $id;

    public function __construct() {
        $this->id = 0;
        $this->user_id = 0;
        $this->card_no = '';
        $this->card_id = '';
    }

    public function save() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_user_cards_dao = new Tyr_user_cards_dao($db_connection);
            $tyr_user_cards_dao->save($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }

    public function get() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $tyr_user_cards_dao = new Tyr_user_cards_dao($db_connection);
            $tyr_user_cards_dao->get($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }

}
