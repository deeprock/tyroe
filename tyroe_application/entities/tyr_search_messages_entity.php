<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_search_messages_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_search_messages_entity extends Abstract_entity{
    //put your code here
    public $search_msg_id;
    public $studio_id;
    public $role_id;
    public $tyroe_id;
    public $email_subject;
    public $email_message;
    public $email_status;
    public $status_sl;
    public $created_timestamp;
    
    public function __construct() {
        $this->search_msg_id = 0;
        $this->studio_id = 0;
        $this->role_id = 0;
        $this->tyroe_id = 0;
        $this->email_subject = '';
        $this->email_message = '';
        $this->email_status = '';
        $this->status_sl = 0;
        $this->created_timestamp = strtotime("now");
        $this->set_CCUU_to_now(0);
    }
    
    public function save_search_messages(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_search_messages_DOA = new Tyr_search_messages_dao($db_connection);
            $tyr_search_messages_DOA->save_search_messages($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_search_messages(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_search_messages_DOA = new Tyr_search_messages_dao($db_connection);
            $tyr_search_messages_DOA->get_search_messages($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
