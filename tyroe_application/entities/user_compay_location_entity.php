<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/user_company_location_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class User_company_location_entity extends Abstract_entity{
    //put your code here
    public $user_id ;
    public $parent_id;
    public $country_id;
    public $city_id;
    public $country_city;
    public $country;
    public $city;
    
    public function __construct() {
        $this->user_id = 0;
        $this->parent_id = 0;
        $this->country_id = 0;
        $this->city_id = '';
        $this->country_city = '';
        $this->country = '';
        $this->city = '';
    }
     
    public function get_user_company_location(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $user_company_location_DOA = new User_company_location_dao($db_connection);
            $user_company_location_DOA->get_user_company_location($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}


?>
