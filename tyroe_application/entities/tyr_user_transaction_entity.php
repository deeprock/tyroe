<?php

require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_user_transaction_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' . EXT);
require_once(APPPATH . 'entities/constants' . EXT);

class Tyr_user_transaction_entity extends Abstract_entity {

    //put your code here
    public $user_id;
    public $transaction_id;
    public $type;
    public $job_id;
    public $created_at;
    public $created_by;
    public $updated_at;
    public $updated_by;
    public $transaction_time;
    public $amt;
    public $package;
    public $coupon_id;

    public function __construct() {
        $this->id = 0;
        $this->user_id = 0;
        $this->transaction_id = '';
        $this->type = 0;
        $this->job_id = 0;
        $this->created_at = 0;
        $this->updated_at = 0;
        $this->created_by = '';
        $this->updated_by = '';
        $this->transaction_time = 0;
        $this->amt = 0;
        $this->package = 0;
        $this->coupon_id = '';
    }

    public function save_transaction() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $this->set_CCUU_to_now($this->user_id);
            $tyr_user_transaction_DOA = new Tyr_user_transaction_dao($db_connection);
            $tyr_user_transaction_DOA->save_transaction($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            die();
            
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_all_transactions() {
        $db_connection = PDO_utils::db_connection();
        $return_array = array();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $this->set_CCUU_to_now($this->user_id);
            $tyr_user_transaction_DOA = new Tyr_user_transaction_dao($db_connection);
            $return_array = $tyr_user_transaction_DOA->get_all_transactions($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function get() {
        $db_connection = PDO_utils::db_connection();
        if ($db_connection == null) {
            $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
            return false;
        }
        try {
            $db_connection->beginTransaction();
            $this->set_CCUU_to_now($this->user_id);
            $tyr_user_transaction_DOA = new Tyr_user_transaction_dao($db_connection);
            $tyr_user_transaction_DOA->get($this);
            $db_connection->commit();
        } catch (Exception $e) {
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
            
        }
        PDO_utils::close_connection($db_connection);
    }

}
