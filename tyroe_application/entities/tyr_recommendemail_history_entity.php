<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_recommendemail_history_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_recommendemail_history_entity extends Abstract_entity{
    //put your code here
    public $recommend_history_id;
    public $recommend_id;
    public $message;
    public $created_timestamp;
    
    public function __construct() {
        $this->recommend_history_id = 0;
        $this->recommend_id = 0;
        $this->message = '';
        $this->created_timestamp = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_recommendemail_history(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_recommendemail_history_DOA = new Tyr_recommendemail_history_dao($db_connection);
            $tyr_recommendemail_history_DOA->save_recommendemail_history($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_recommendemail_history(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_recommendemail_history_DOA = new Tyr_recommendemail_history_dao($db_connection);
            $tyr_recommendemail_history_DOA->get_recommendemail_history($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
