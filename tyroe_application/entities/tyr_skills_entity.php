<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_skills_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_skills_entity extends Abstract_entity{
    //put your code here
    public $skill_id;
    public $user_id;
    public $skill;
    public $speciality;
    public $status_sl;
    public $created_timestamp;
    public $modified;
    
    public function __construct() {
        $this->skill_id = 0;
        $this->user_id = 0;
        $this->skill = '';
        $this->speciality = 0;
        $this->status_sl = 0;
        $this->created_timestamp = 0;
        $this->modified = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_skills(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $tyr_skills_DOA->save_skills($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_skills(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $tyr_skills_DOA->get_skills($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_user_skill(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $tyr_skills_DOA->get_user_skill($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function get_users_for_skill_id_in($skill_ids){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $return_array = $tyr_skills_DOA->get_users_for_skill_id_in($skill_ids);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_user_skill(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $status = $tyr_skills_DOA->update_user_skill($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_user_all_speciality_flag(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $status = $tyr_skills_DOA->update_user_all_speciality_flag($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function update_user_skill_speciality_flag(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $status = $tyr_skills_DOA->update_user_skill_speciality_flag($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
    public function delete_user_skills(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $tyr_skills_DOA->delete_user_skills($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function check_user_skill_exist(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $return_array = $tyr_skills_DOA->check_user_skill_exist($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
    public function update_skills(){
        $db_connection = PDO_utils::db_connection();
        $status = false;
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_skills_DOA = new Tyr_skills_dao($db_connection);
            $status = $tyr_skills_DOA->update_skills($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $status;
    }
    
}
