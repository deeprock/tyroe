<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_gallery_tags_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_gallery_tags_entity extends Abstract_entity{
    //put your code here
    public $tag_id;
    public $tag_name;
    public $tag_add_by;
    public $tag_add_time;
    public $tag_status;
    
    
    public function __construct() {
        $this->tag_id = 0;
        $this->tag_name = '';
        $this->tag_add_by = 0;
        $this->tag_add_time = 0;
        $this->tag_status = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_gallery_tags(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_gallery_tags_DOA = new Tyr_gallery_tags_dao($db_connection);
            $tyr_gallery_tags_DOA->save_gallery_tags($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_gallery_tags(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_DOA = new Tyr_gallery_tags_dao($db_connection);
            $tyr_gallery_tags_DOA->get_gallery_tags($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function check_tag_exist(){
        $db_connection = PDO_utils::db_connection();
        $return_array = '';
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_gallery_tags_DOA = new Tyr_gallery_tags_dao($db_connection);
            $return_array = $tyr_gallery_tags_DOA->check_tag_exist($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
        return $return_array;
    }
    
}
