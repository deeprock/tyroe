<?php
require_once(APPPATH . 'third_party/php_utils/abstract_entity' . EXT);
require_once(APPPATH . 'third_party/php_utils/pdo_utils' . EXT);
require_once(APPPATH . 'daos/tyr_invite_reviewer_dao' . EXT);
require_once(APPPATH . 'third_party/php_utils/uuid_utils' .EXT);
require_once(APPPATH . 'entities/constants' .EXT);

class Tyr_invite_reviewer_entity extends Abstract_entity{
    //put your code here
    public $invite_reviewer_id;
    public $invite_email;
    public $invite_job_id;
    public $invite_studio_id;
    public $status_sl;
    public $created_time;
    
    public function __construct() {
        $this->invite_reviewer_id = 0;
        $this->invite_email = '';
        $this->invite_job_id = 0;
        $this->invite_studio_id = 0;
        $this->status_sl = 0;
        $this->created_time = 0;
        $this->set_CCUU_to_now(0);
    }
    
    public function save_invite_reviewer(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $this->generate_validation_code();
            $tyr_invite_reviewer_DOA = new Tyr_invite_reviewer_dao($db_connection);
            $tyr_invite_reviewer_DOA->save_invite_reviewer($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
     
    public function get_invite_reviewer(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_reviewer_DOA = new Tyr_invite_reviewer_dao($db_connection);
            $tyr_invite_reviewer_DOA->get_invite_reviewer($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
    public function check_insert_invite_reviewer(){
        $db_connection = PDO_utils::db_connection();
        if($db_connection == null) {
                $error = API_message::error_message(Constants::ERROR_NO_DB_CONNECTION);
                return false;
        }
        try{
            $db_connection->beginTransaction();
            $tyr_invite_reviewer_DOA = new Tyr_invite_reviewer_dao($db_connection);
            $tyr_invite_reviewer_DOA->check_insert_invite_reviewer($this);
            $db_connection->commit();
        }catch(Exception $e){
            $error = API_message::error_message($e->getMessage());
            $db_connection->rollBack();//var_dump($error);var_dump($e->getFile().$e->getLine());
        }
        PDO_utils::close_connection($db_connection);
    }
    
}
