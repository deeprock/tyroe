<?php

require_once('abstract_entity' . EXT);

class API_message extends Abstract_entity {

    const ERROR = "Error";
    const SUCCESS = "Success";
   
    public $type;
    public $message;
   
    public function __construct($type = '', $message_str = '') {
        $this->type = $type;
        $this->message = $message_str;
    }

	public function as_json() {
		$this->unset_values(array('created_at', 'created_by','updated_at', 'updated_by'));
		
        return parent::as_json($this);
    }
	
    public static function error_message($message){
        return new API_message(API_message::ERROR, $message);
    }

    public static function success_message($message){
        return new API_message(API_message::SUCCESS, $message);
    }
}
