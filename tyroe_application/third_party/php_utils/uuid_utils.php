<?php

require_once(APPPATH . 'third_party/lib.uuid/lib.uuid' . EXT);

function uuid_new() {
    $uuid = UUID::mint();
    $new_uuid = str_replace('-', '', $uuid->string);
    return $new_uuid;
}