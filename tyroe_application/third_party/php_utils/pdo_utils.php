<?php

require_once(APPPATH . 'config/pdo' . EXT);

class PDO_utils {

    public static function db_connection($db_name = '') {

        global $db_props;

        $host = $db_props[ENVIRONMENT]['host'];
        if ($db_name == ''){
            $db_name = $db_props[ENVIRONMENT]['db_name'];
        }else{
            $db_name = isset($db_props[ENVIRONMENT][$db_name]) ? $db_props[ENVIRONMENT][$db_name] : $db_name; 
        }
        $username = $db_props[ENVIRONMENT]['username'];
        $password = $db_props[ENVIRONMENT]['password'];

        //$dsn = 'mysql:host=' . $host . ';dbname=' . $db_name.';charset=utf8';
        $dsn = 'pgsql:host='.$host.';dbname='.$db_name;

        $db_connection = null;
        try {
            $db_connection = new PDO($dsn, $username, $password);
            $db_connection->exec("set names utf8");
            $db_connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            log_message('error', 'Unable to obtain DB connection' . $e->getMessage());
            var_dump($e->getMessage());
        }

        return $db_connection;
    }

    public static function close_connection(&$db_connection) {
        try {
            $db_connection = null;
        } catch (PDOException $e) {
            log_message('error', 'Failed to close DB connection' . $e->getMessage());
        }
    }

}