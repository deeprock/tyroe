<?php

require_once('api_message' . EXT);

class Abstract_entity {

    public $created_at;
    public $updated_at;
    public $created_by = 0;
    public $updated_by = 0;
    public $created_updated_by = 0;

    public function __construct() {
        $this->created_updated_by = 0;
    }
    
    public function as_json() {
        return json_encode($this);
    }

    protected function required_properties_present($obj, $properties_arr, &$error) {
        foreach ($properties_arr as $property) {
            if (!property_exists($obj, $property)) {
                $error = API_message::error_message('Invalid JSON - Property Missing: ' . $property);
                return false;
            }
        }

        return true;
    }

    protected function set_CCUU_to_now($user_id) {
        $format = 'Y-m-d H:i:s';
        $now = date($format);
        $this->created_at = $now;
        $this->updated_at = $now;

        $this->created_by = $user_id;
        $this->updated_by = $user_id;
        $this->created_updated_by = $user_id;
    }

    public function unset_values($properties_arr) {
        foreach ($properties_arr as $property) {
            if (property_exists($this, $property)) {
                unset($this->$property);
            }
        }
    }

    public function generate_validation_code(){

    }
    
}
