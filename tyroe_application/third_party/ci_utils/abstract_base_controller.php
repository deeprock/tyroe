<?php

require_once(APPPATH . 'utilities/login_utils' . EXT);

class Abstract_BASE_controller extends CI_Controller {

    protected $page_data;

    private $deploy_mode_js_mapping;
    
    private $deploy_mode_css_mapping;
    
    protected $login_obj;
    
    function __construct() {
        parent::__construct();

        date_default_timezone_set('GMT');
        $this->page_data = array();
        $this->_set_no_cache();
        $this->_set_common_vars();
        $this->login_obj = new Login_utils();
        $this->check_session();
    }
    
    function check_session(){
        $this->page_data['lang_preference'] = $this->login_obj->check_session();
    }

    function index() {
        wh_redirect('', 'refresh');
    }

    function _set_common_vars() {
        $this->page_data['theme_path'] = $this->config->item('base_url')."themes/";
        $this->page_data['script_path'] = $this->config->item('base_url')."js/";
        
        $this->page_data['base_url'] = $this->config->item('base_url');
        
        $this->page_data['page_title'] = "";
        
        $this->page_data['page_description'] = "";

        $this->page_data['scripts'] = array('default/jquery.nicescroll.js','jquery.hoverIntent.js','common.js');
        
        $this->page_data['stylesheets'] = array();
        
        $this->page_data['css_assets'] = array();
        $this->page_data['js_assets'] = array();
        
        $current_url = current_url();
        $current_url = str_replace("index.php?", "", $current_url);
        $this->page_data['current_url'] = $current_url;
        $this->page_data['show_mail_header'] = false;
        $this->page_data['css_version'] = '?v=0.32';
        $this->page_data['js_version'] = '?v=0.32';
    }
    
    function _css_file_by_deploy_mode($file_path, $deploy_mode) {
        if($deploy_mode == 'production') {
            
            if(array_key_exists($file_path, $this->deploy_mode_css_mapping)) {
                
                $file_to_return = $this->deploy_mode_css_mapping[$file_path];
                
                foreach($this->page_data['stylesheets'] as  $value) {
                    if($file_to_return == $value) {
                      return '';  
                    }
                }
            
                return $file_to_return;
            } else {
                return $file_path;
            }
            
        } else {
            return $file_path;
        }
    }

    function _add_css() {
        $deploy_mode = $this->config->item('deploy_mode');
        
        for ($i = 0;$i < func_num_args();$i++) {
            $css_file = $this->_css_file_by_deploy_mode(func_get_arg($i), $deploy_mode);
            if(strlen($css_file) > 0) {
                $this->page_data['stylesheets'][] =  $css_file;
            }
        }
    }
    
    function _add_css_assets() {
        $deploy_mode = $this->config->item('deploy_mode');
        
        for ($i = 0;$i < func_num_args();$i++) {
            $css_file = $this->_css_file_by_deploy_mode(func_get_arg($i), $deploy_mode);
            if(strlen($css_file) > 0) {
                $this->page_data['css_assets'][] =  $css_file;
            }
        }
    }
    
    function _js_file_by_deploy_mode($file_path, $deploy_mode) {
        if($deploy_mode == 'production') {
            
            if(array_key_exists($file_path, $this->deploy_mode_js_mapping)) {
                
                $file_to_return = $this->deploy_mode_js_mapping[$file_path];
                
                foreach($this->page_data['scripts'] as $value) {
                    if($file_to_return == $value) {
                      return '';  
                    }
                }
            
                return $file_to_return;
            } else {
                return $file_path;
            }
            
        } else {
            return $file_path;
        }
    }

    function _add_js() {
        $deploy_mode = $this->config->item('deploy_mode');
        for ($i = 0;$i < func_num_args();$i++) {
            $js_file = $this->_js_file_by_deploy_mode(func_get_arg($i), $deploy_mode);
            if(strlen($js_file) > 0) {
                $this->page_data['scripts'][] =  $js_file;
            }
        }
    }
    
    function _add_js_assets() {
        $deploy_mode = $this->config->item('deploy_mode');
        for ($i = 0;$i < func_num_args();$i++) {
            $js_file = $this->_js_file_by_deploy_mode(func_get_arg($i), $deploy_mode);
            if(strlen($js_file) > 0) {
                $this->page_data['js_assets'][] =  $js_file;
            }
        }
    }

    function _set_no_cache() {
        $this->output->set_header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        $this->output->set_header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate");
        $this->output->set_header("Cache-Control: post-check=0, pre-check=0", false);
        $this->output->set_header("Pragma: no-cache");
    }

    function _current_page_URL() {
        $isHTTPS = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on");
        $port = (isset($_SERVER["SERVER_PORT"]) && ((!$isHTTPS && $_SERVER["SERVER_PORT"] != "80") || ($isHTTPS && $_SERVER["SERVER_PORT"] != "443")));
        $port = ($port) ? ':'.$_SERVER["SERVER_PORT"] : '';
        $url = ($isHTTPS ? 'https://' : 'http://').$_SERVER["SERVER_NAME"].$port.$_SERVER["REQUEST_URI"];
        return $url;
    }

    function _login_page_data() {
        $current_URL = $this->_current_page_URL();
        $base_URL =  $this->config->item('base_url');
        $current_URL_length = strlen($current_URL);
        $base_URL_length = strlen($base_URL);
        $required_data = substr($current_URL, $base_URL_length, ($current_URL_length - $base_URL_length) );

        return ereg_replace('/', '-', $required_data);
    }
}