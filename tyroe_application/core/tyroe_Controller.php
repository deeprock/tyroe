<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_refrences_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendation_entity' . EXT);
require_once(APPPATH . 'entities/tyr_professional_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_other_site_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_sites_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_social_link_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendemail_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_rel_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_entity' . EXT);
require_once(APPPATH . 'entities/tyr_pageviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_log_entity' . EXT);
require_once(APPPATH . 'entities/tyr_system_variables_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_log_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_job_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_reviewer_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);


class Tyroe_Controller extends CI_Controller
{


    var $data = array();
    var $template_arr = array();
    var $controller_name = '';

    public function addslashesToPost(){
        foreach($_POST as $k=>$v){
            if(is_array($v)){
                foreach($v as $k1=>$v1){
                    $_POST[$k][$k1]=addslashes(stripslashes($v1));
                }
            }else{
                $_POST[$k]=addslashes(stripslashes($v));
            }

        }
    }

    function resizeImage($originalImage,$toWidth,$toHeight,$new_dir_filename){
        $filename = $originalImage;
        $width = $toWidth;
        $height = $toHeight;
        $getimagesize = getimagesize($filename);
        list($width_orig, $height_orig)=getimagesize($filename);
        $ratio_orig = $width_orig/$height_orig;
        if ($width/$height > $ratio_orig) {
            $width = $height*$ratio_orig;
        } else {
            $height = $width/$ratio_orig;
        }
        $image_p = imagecreatetruecolor($width, $height);
        
        if($getimagesize['mime']=='image/png'){
            imagealphablending($image_p, false);
            imagesavealpha($image_p,true);
            $transparent = imagecolorallocatealpha($image_p, 255, 255, 255, 127);
            imagefilledrectangle($image_p, 0, 0, $width, $height, $transparent);
            $image = imagecreatefrompng($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagepng($image_p, $new_dir_filename);
        }elseif($getimagesize['mime']=='image/gif'){
            $image = imagecreatefromgif($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagegif($image_p, $new_dir_filename);
        }elseif($getimagesize['mime']=='image/jpg'){
            $image = imagecreatefromjpeg($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagejpeg($image_p, $new_dir_filename, 100);
        }elseif($getimagesize['mime']=='image/jpeg'){
            $image = imagecreatefromjpeg($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagejpeg($image_p, $new_dir_filename, 100);
        }elseif($getimagesize['mime']=='image/pjpeg'){
            $image = imagecreatefromjpeg($filename);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
            imagejpeg($image_p, $new_dir_filename, 100);
        }
        if($image==null){
            return false;
        }
    }

    public function strip_addslashes($data)
    {

        foreach ($data as $k => $v) {
            $data[$k] = addslashes(stripslashes($v));
        }
        return $data;
    }
    public function dropdown($data=array(),$selected,$options=array()){
        $attributes="";
        if(!isset($options['class'])){
            $options['class']='select2';
        }
            foreach($options as $k=>$v)
                $attributes.=" ".$k."='".$v."'";
        $dropdown=" <select ".$attributes." >";
        $dropdown.="<option value=' ' selected=''>".$options['placeholder']."</option>";
        foreach($data as $k=>$v){
            if($selected!="" && $selected==$v['id']){
                $dropdown.="<option value='".$v['id']."' selected>".$v['name']."</option>";
            }else{
                $dropdown.="<option value='".$v['id']."'>".$v['name']."</option>";
            }
        }
        $dropdown.="</select>";
        return $dropdown;
    }

    public function __construct($controllername = 'logged_in')
    {
        parent::__construct();
        $this->data['scripts'] = array();
        $this->data['stylesheets'] = array();
        $this->data['js_version'] = '?v=0.10';
        $this->data['css_version'] = '?v=0.12';
        //for api button redirection
        if($this->input->post('website_share_post')){
            $this->session->set_userdata('redirect_to_job_overview','yes');
            $this->session->set_userdata('comapnyname',$this->input->post('comapnyname'));
            $this->session->set_userdata('share_jobid',$this->input->post('share_jobid'));
        }

        if($this->input->post('submitType') != ''){
            $this->session->set_userdata('submitType',$this->input->post('apply_job_id'));
            $this->session->set_userdata('apply_job_id',$this->input->post('apply_job_id'));
            $this->session->set_userdata('apply_job_title',$this->input->post('apply_job_title'));
        }
        if($this->input->get(md5('job_invitation'))){
            //check already insert
            
            $invite_email =  base64_decode($this->input->get(md5('invite_email')));
            $job_id =  base64_decode($this->input->get(md5('job_id')));
            $studio_id =  base64_decode($this->input->get(md5('studio_id')));
                    
            $joins_obj = new Tyr_joins_entity();
            $check_alreadyInvite = $joins_obj->check_already_invites($invite_email, $job_id, $studio_id);
            
//            $check_alreadyInvite = $this->getRow('SELECT '.TABLE_INVITE_REVIEWER.'.invite_email FROM '.TABLE_INVITE_REVIEWER.'
//                RIGHT JOIN '.TABLE_USERS.' ON
//                '.TABLE_USERS.'.email = '.TABLE_INVITE_REVIEWER.'.invite_email
//                WHERE '.TABLE_INVITE_REVIEWER.'.invite_email =  "'.base64_decode($this->input->get(md5('invite_email'))).'" AND
//                '.TABLE_INVITE_REVIEWER.'.invite_job_id =  "'.base64_decode($this->input->get(md5('job_id'))).'" AND '.TABLE_USERS.'.status_sl = 1 AND
//                '.TABLE_INVITE_REVIEWER.'.invite_studio_id =  "'.base64_decode($this->input->get(md5('studio_id'))).'"'
//            );

            if($check_alreadyInvite['invite_email'] == ''){
                
                $invite_reviewer_obj = new Tyr_invite_reviewer_entity();
                $invite_reviewer_obj->invite_email =  base64_decode($this->input->get(md5('invite_email')));
                $invite_reviewer_obj->invite_job_id =  base64_decode($this->input->get(md5('job_id')));
                $invite_reviewer_obj->invite_studio_id =  base64_decode($this->input->get(md5('studio_id')));
                $invite_reviewer_obj->check_insert_invite_reviewer();
                
                
//                $checkinsertrow = $this->getRow('SELECT '.TABLE_INVITE_REVIEWER.'.invite_email FROM '.TABLE_INVITE_REVIEWER.'
//                                WHERE '.TABLE_INVITE_REVIEWER.'.invite_email =  "'.base64_decode($this->input->get(md5('invite_email'))).'" AND
//                                '.TABLE_INVITE_REVIEWER.'.invite_job_id =  "'.base64_decode($this->input->get(md5('job_id'))).'" AND
//                                '.TABLE_INVITE_REVIEWER.'.invite_studio_id =  "'.base64_decode($this->input->get(md5('studio_id'))).'"'
//                            );
                if($invite_reviewer_obj->invite_reviewer_id == 0){
                    $this->insert(TABLE_INVITE_REVIEWER,array(
                            'invite_email'      => base64_decode($this->input->get(md5('invite_email'))),
                            'invite_job_id'     => base64_decode($this->input->get(md5('job_id'))),
                            'invite_studio_id'  => base64_decode($this->input->get(md5('studio_id'))),
                            'created_time'      =>time(),
                            'status_sl'         =>1
                        )
                    );
                    $invite_reviewer_obj = new Tyr_invite_reviewer_entity();
                    $invite_reviewer_obj->invite_email =  base64_decode($this->input->get(md5('invite_email')));
                    $invite_reviewer_obj->invite_job_id =  base64_decode($this->input->get(md5('job_id')));
                    $invite_reviewer_obj->invite_studio_id =  base64_decode($this->input->get(md5('studio_id')));
                    $invite_reviewer_obj->created_time = time();
                    $invite_reviewer_obj->status_sl = 1;
                    $invite_reviewer_obj->save_invite_reviewer();
                    
                }
                $this->session->set_userdata('redirect_to_job','yes');
                $this->session->set_userdata('invite_job_id',base64_decode($this->input->get(md5('job_id'))));
                $this->session->set_userdata('invite_studio_id',base64_decode($this->input->get(md5('studio_id'))));
                $this->session->set_userdata('invite_email',base64_decode($this->input->get(md5('invite_email'))));
            }else{
                $this->session->set_userdata('alreadyInvite',MESSAGE_ALREADY_INVITE_REVIEWER);
                redirect($this->getURL('login'));
            }
        }
        $user_type_sess = $this->session->userdata('user_type');
        if(!$this->input->is_ajax_request()){
            $insert_visit_data = array();
            switch (strtolower($user_type_sess)) {
                case "tyroe":
                    $user_type = 1;
                    break;
                case "studio":
                    $user_type = 2;
                    break;
                case "admin":
                    $user_type = 3;
                    break;
                case "":
                    $user_type = 0;
                    default;
            }
            $session_id = $this->session->userdata('session_id');
            $current_url = current_url();
            $have_ext = strtolower(pathinfo($current_url,PATHINFO_EXTENSION));
            $not_allow_ext = array('png','jpg','jpeg','gif');
            $insert_visit_data['user_id'] = $this->session->userdata('user_id');
            $insert_visit_data['user_type'] = $user_type;  //1=USER, 2=STUDIO, NULL=NOT LOGGED
            $insert_visit_data['page_url'] = $current_url;
            $insert_visit_data['user_info_detail'] = json_encode($_SERVER);
            //$insert_visit_data['user_ip'] = $_SERVER['REMOTE_ADDR'];
            $insert_visit_data['user_ip'] = $this->input->server('REMOTE_ADDR');
            $insert_visit_data['visit_time'] = time();
            $insert_visit_data['session_id'] = $session_id;
            //
            if(!in_array($have_ext,$not_allow_ext)){
                
                $pageviews_obj = new Tyr_pageviews_entity();
                $pageviews_obj->user_id = $this->session->userdata('user_id');
                $pageviews_obj->user_type = $user_type;
                $pageviews_obj->page_url = $current_url;
                $pageviews_obj->user_info_detail = json_encode($_SERVER);
                $pageviews_obj->user_ip = $this->input->server('REMOTE_ADDR');
                $pageviews_obj->visit_time = time();
                $pageviews_obj->session_id = $session_id;
                $pageviews_obj->save_pageviews();
                
                //$this->db->insert(TABLE_PAGEVIEWS,$insert_visit_data);
            }
        }


        $this->controller_name = strtolower($this->router->fetch_class());
        $callfrom = strtolower(trim($this->input->post('callfrom')));
        $last_uploaded_user_dispalyimage_id = $this->session->userdata('last_uploaded_user_dispalyimage_id');
        $uploaded_user_img = $this->session->userdata('uploaded_user_img');
        $old_user_dispalyimage_id = $this->session->userdata('old_user_dispalyimage_id');

        if($last_uploaded_user_dispalyimage_id && $uploaded_user_img && $callfrom!='cropimage'){
            $this->session->unset_userdata('old_user_dispalyimage_id');

            //echo '<pre>';print_r($this->session->all_userdata());echo '</pre>';
            //die('reached!!');


//            $old_user_dispalyimage_data = array(
//                'modified' => strtotime("now"),
//                'status_sl' => "1"
//            );
//
//            $old_user_dispalyimage_where = array(
//                'image_id' => $old_user_dispalyimage_id
//            );
//            
//            $this->update(TABLE_MEDIA, $old_user_dispalyimage_data, $old_user_dispalyimage_where);
            
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $old_user_dispalyimage_id;
            $media_obj->get_media();
            $media_obj->status_sl = 1;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            

//            $last_uploaded_user_dispalyimage_data = array(
//                'modified' => strtotime("now"),
//                'status_sl' => "-1"
//            );
//
//            $last_uploaded_user_dispalyimage_where = array(
//                'image_id' => $last_uploaded_user_dispalyimage_id
//            );
//            
//            //echo $this->db->last_query();
//            $this->update(TABLE_MEDIA, $last_uploaded_user_dispalyimage_data, $last_uploaded_user_dispalyimage_where);
            
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $last_uploaded_user_dispalyimage_id;
            $media_obj->get_media();
            $media_obj->status_sl = -1;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            
            
            //echo $this->db->last_query();

            $this->session->unset_userdata('last_uploaded_user_dispalyimage_id');
            $this->session->unset_userdata('uploaded_user_img');
            //echo '<pre>';print_r($this->session->all_userdata());echo '</pre>';
            //die;

        }
        $this->data['controller_name'] = strtolower($this->router->fetch_class());
        $this->output->nocache();
        $this->data['vObj'] = $this;
        $method = strtolower($this->router->fetch_method());
        $bool_checker = false;

        if ($this->data['controller_name'] == "profile" && ($method == "public_recommendation") || ($method == "save_recommendation")) {
            $bool_checker = true;
            $this->data['access_allowed'] = true;
        }
        if ($controllername == 'login' || $controllername == 'register' || $bool_checker || $controllername == 'publicprofile' || $controllername == 'loadmore') {

        } else {
            if ($this->check_login()) {
                //$this->data['get_modules']= $this->getMenus();
                $this->module_validator();
                //$get_countries = $this->getAll("SELECT country_id AS id,country AS name FROM " . TABLE_COUNTRIES);
                $countries_obj = new Tyr_countries_entity();
                $get_countries = $countries_obj->get_all_countries_as();
                $this->data['get_countries'] = $get_countries;

                //$get_creative_skills = $this->getAll("SELECT creative_skill_id,creative_skill FROM " . TABLE_CREATIVE_SKILLS);
                $creative_skills_obj = new Tyr_creative_skills_entity();
                $get_creative_skills = $creative_skills_obj->get_all_creative_skills();
                $this->data['get_creative_skills'] = $get_creative_skills;

                //$get_job_level = $this->getAll("SELECT job_level_id,level_title FROM " . TABLE_JOB_LEVEL);
                $job_level_obj = new Tyr_job_level_entity();
                $get_job_level = $job_level_obj->get_all_job_level();
                $this->data['get_job_level'] = $get_job_level;

                //$get_job_cities = $this->getAll("SELECT DISTINCT(job_city) FROM " . TABLE_JOBS);
                $jobs_obj = new Tyr_jobs_entity();
                $get_job_cities = $jobs_obj->get_distinct_job_cities();
                $this->data['get_job_cities'] = $get_job_cities;

                //$get_job_industries = $this->getAll("SELECT industry_id,industry_name from ".TABLE_INDUSTRY);
                $industry_obj = new Tyr_industry_entity();
                $get_job_industries = $industry_obj->get_all_industries();
                $this->data['job_industries'] = $get_job_industries;
                
                
                //$get_job_type = $this->getAll("SELECT job_type_id,job_type FROM " . TABLE_JOB_TYPE);
                $job_type_obj = new Tyr_job_type_entity();
                $get_job_type = $job_type_obj->get_all_job_types();
                $this->data['get_job_type'] = $get_job_type;
                
                $pending_moderation_review_obj = new Tyr_rate_review_entity();
                $this->data['pending_moderation_review_count'] = $pending_moderation_review_obj->get_pending_moderation_review_count($this->session->userdata("user_id"));
               /* $get_variables = $this->getAll("select * from " . TABLE_SYSTEM_VARIABLES);
                $this->data['get_variables'] = $get_variables;*/

            } else {
                header('location:' . $this->getURL("login"));

                //$this->route($this->getURL("login"));
                exit;
            }

        }


    }

    /*function pagination($params)
    {
        $this->load->library('pagination');
        $config['base_url'] = $this->getURL($params['url']);
        $config['total_rows'] = $params['total_rows'];
        $config['per_page'] = $params['per_page'];
        $config['cur_page'] = $params['current_page'];
        $config['first_link'] = "&lt;&lt; First";
        $config['last_link'] = "Last &gt;&gt;";
        $config['full_tag_open'] = "<div class='pagination pagination-right'>";
        $config['full_tag_close'] = "</div>";
        $config['last_tag_open'] = "";
        $config['first_tag_close'] = "";
        $config['anchor_class'] = "page";
        $this->pagination->initialize($config);
        return $this->pagination->create_links("javascript");
    }*/

    function pagination($params)
    {
        $this->load->library('pagination');
        $config['base_url'] = $this->getURL($params['url']);
        $config['total_rows'] = $params['total_rows'];
        $config['per_page'] = $params['per_page'];
        $config['cur_page'] = $params['current_page'];
        $config['first_link'] = "&lt;&lt;";
        $config['last_link'] = "&gt;&gt;";
        $config['full_tag_open'] = "<ul class='pull-right'>";
        $config['full_tag_close'] = "</ul>";
        $config['last_tag_open'] = "";
        $config['first_tag_close'] = "";
        $config['anchor_class'] = "page";
        $this->pagination->initialize($config);
        return $this->pagination->create_links("javascript");
    }

    //function dateDifference($startDate, $endDate)
    function dateDifference($startDate, $endDate)
    {
        /*if ($startDate === false || $startDate < 0 || $endDate === false || $endDate < 0 || $startDate > $endDate)
            return "0 Day(s)";

        $years = date('Y', $endDate) - date('Y', $startDate);


        // Calculate months
        $endMonth = date('m', $endDate);
        $startMonth = date('m', $startDate);
        $months = $endMonth - $startMonth;

        $days = $endDate - $startDate;
        $days = date('j', $days);

        if( ($days < 30) ){
            $months = 0;
        }

        if($months > 0){
            $differenceFormat = $months.' Month(s) '.$days.' Day(s)';
        } else {
            $differenceFormat = $days.' Day(s)';
        }
        //return $differenceFormat."-->".$check_var;//array($years, $months, $days);
        return $differenceFormat;*/
        $diff = floor($endDate - $startDate);
        $day =  60 * 60 * 24;
        $days = floor($diff/$day);
        $months = floor($days/31);
        $years = floor($months/12);
       /* if($months > 0){
            $totalDaysInMonth = $months * 31;
            $days =   abs($totalDaysInMonth - $days);
        }
        if($years > 0){
            $totalMonthInYear = $years * 12;
            $months =   abs($totalMonthInYear - $months);
        }*/
        $message = '';
        if($days == 1){
            $message .= $days . " Day ";
        }else if($days >= 0){
            $message .= $days . " Day(s) ";
        }
       /* if($months == 1){
            $message .= $months . " Month ";
        }else if($months > 1){
            $message .= $months . " Month(s) ";
        }
        if($years == 1){
            $message .= $years . " Year \n";
        }else if($years > 1){
            $message .= $years . " Year(s) \n";
        }*/
        return $message;
    }
    /*function dateDifference($date_1 , $date_2 , $differenceFormat = '%m %d' )
    {

        //$datetime1 = date_create($date_1);
        $datetime1 = date('Y-d-m', $date_1);
        //$datetime2 = date_create($date_2);
        $datetime2 = date('Y-d-m', $date_2);
        $interval = date_diff($datetime1, $datetime2);
        if($interval->m > 0){
            $differenceFormat = '%m Month(s) %d Day(s)';
        } else {
            $differenceFormat = '%a Day(s)';
        }
        return $interval->format($differenceFormat);

    }*/

    /*function module_validator()
    {

        $method_name = strtolower($this->router->fetch_method());
        if ($method_name == "index")
            $method_name = $this->controller_name;
        if ($method_name == "logout")
            return true;
        $modules = $this->getAll("SELECT module_id,module_file,link FROM tyr_modules WHERE module_file='" . $this->controller_name . "'");

        $boolean_checker = false;

        if (count($modules) && is_array($modules)) {
            foreach ($modules as $k => $v) {
                $link = @explode('/', $v['link']);
                if (!isset($link[1])) {
                    $link[1] = $link[0];
                }

                if ($link[1] == $method_name) {

                    $user_modules = $this->session->userdata('module_id');
                    if (!in_array($v['module_id'], $user_modules)) {
                        echo "Unauthorised Access";
                        exit;
                    } else {
                        //echo $link[1]."<br>";
                        return true;
                    }

                } else {
                    $boolean_checker = true;
                }
            }
        }
        if ($boolean_checker) {
            echo "Unauthorised Access";
            exit;
        }

    }*/

    function module_validator() {

        $method_name = strtolower($this->router->fetch_method());
        if ($method_name == "index")
            $method_name = $this->controller_name;
        if ($method_name == "logout")
            return true;
       
        $where = "";
        if ($this->session->userdata('role_id') == 1) {
            $where .= " AND module_id NOT IN (" . ADMIN_NOT_IN_USER_MODULE . ")";
        }
        
        $modules_obj = new Tyr_modules_entity();
        $modules_obj->module_file = $this->controller_name;
        $modules_obj->status_sl = 1;
        $modules = $modules_obj->get_all_system_modules_where($where);
        //$modules = $this->getAll("SELECT module_id,module_file,link FROM " . TABLE_MODULES . " WHERE module_file='" . $this->controller_name . "' AND status_sl=1 " . $where);
        $boolean_checker = false;

        if (count($modules) && is_array($modules)) {
            foreach ($modules as $k => $v) {
                $link = @explode('/', $v['link']);
                if (!isset($link[1])) {
                    $link[1] = $link[0];
                }

                if ($link[1] == $method_name) {

                    $user_modules = $this->session->userdata('module_id');
                    if (!in_array($v['module_id'], $user_modules)) {
                        echo "Unauthorised Access";
                        exit;
                    } else {
                        //echo $link[1]."<br>";
                        return true;
                    }

                } else {
                    $boolean_checker = true;
                }
            }
        } else {
            $boolean_checker = true;
        }

        if($this->controller_name == 'fbclient'){$boolean_checker = false;
        }elseif($this->controller_name == 'twitterclient'){$boolean_checker = false;
        }elseif($this->controller_name == 'googleclient'){$boolean_checker = false;
        }elseif($this->controller_name == 'linkedinclient'){$boolean_checker = false;}


        if ($boolean_checker) {
            echo "Unauthorised Access";
            exit;
        }

    }

    function updatesession($data = array())
    {
        $this->session->set_userdata($data);
    }

    function getURL($params, $page = "index.php")
    {
        if ($params != "")
            $params = $params;
        //return LIVE_SITE_URL.$page."/". $params;
        return LIVE_SITE_URL. $params;
    }

    function route($url, $message = "", $method = "php", $target = '_self')
    {
        $this->updatesession(array("success_message" => $message));
        header('Location:' . $url);
        //ob_clean();
        //redirect($url,$method,$target);
    }

    function email_sender($to, $subject, $message, $file = 'nofile', $cc = 'nocc', $no_remove = "", $params = array(),$email_from=null,$email_subject=null)
    {
        //print_array($email_from,false,'$email_from');
        //print_array($email_subject,true,'$email_subject');
        $config = unserialize(EMAIL_CONFIG);
        $this->load->library('email');
        $this->email = new CI_Email();
        $this->email->initialize($config);
        $this->email->set_newline("\r\n");
        $this->email->clear(true);
        $this->email->mailtype = "html";
        $new_mail_from = EMAIL_FROM;

//        if($email_from!=null){
//            $new_mail_from = $email_from;
//        } else {
//            $new_mail_from = EMAIL_FROM;
//        }

        if($email_subject!=null){
            $new_subject = $email_subject;

        } else {
            $new_subject= EMAIL_FROM_TITLE;
        }

        $this->email->from($new_mail_from,$new_subject);
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->to($to);
        if ($cc != "nocc" && $cc != '') {
            $this->email->cc($cc);

        }
        if (is_array($file)) {
            foreach ($file as $k => $v) {
                if ($v != 'nofile' && $v != '') {
                    $this->email->attach($v);
                }
            }
        } else {
            if ($file != 'nofile' && $file != '') {
                $this->email->attach($file);
            }
        }

        if ($this->email->send()) {
            if ($no_remove == "yes") {
            } else {
                if (is_array($file)) {
                    foreach ($file as $k => $v) {
                        @unlink($v);
                    }
                } else {
                    @unlink($file);
                }
            }
            return true;
        } else {
            if ($no_remove == "yes") {
            } else {
                if (is_array($file)) {
                    foreach ($file as $k => $v) {
                        @unlink($v);
                    }
                } else {
                    @unlink($file);
                }
            }
            return false;
        }
    }

    public function load_template()
    {
        foreach ($this->template_arr as $k => $v) {
            $this->load->view($v, $this->data);
        }
    }

    function set_post_for_view()
    {
        foreach ($_POST as $k => $v) {
            $this->data[$k] = stripslashes($v);
        }
    }

    function client_condition()
    {

    }

    function orderby()
    {

        $sort = $this->input->post('sort_by');
        $flag = $this->input->post('flag');
        $data = array();
        if ($sort != "") {
            if ($flag == "0") {
                $data['order_by'] = " ORDER BY " . $sort . " DESC";
                $sort_img = '<img src="' . ASSETS_PATH . "img/sort_asc.png" . '" alt=""  />';
                $this->data['sort_img_' . $sort] = $sort_img;
                $this->data['sort_by'] = $sort;
                $this->data['new_flag'] = "1";
            } else {
                $data['order_by'] = " ORDER BY " . $sort . " ASC";

                $sort_img = '<img src="' . ASSETS_PATH . "img/sort_desc.png" . '" alt=""  />';
                $this->data['sort_img_' . $sort] = $sort_img;
                $this->data['sort_by'] = $sort;
                $this->data['new_flag'] = "0";
            }
            $this->data['flag'] = $flag;

        } else {
            $this->data['sort_by'] = "";
            $this->data['flag'] = "0";
        }
        return $data;
    }

    function pagination_limit()
    {
        $page = $this->input->post('page');
        if ($page == '' || $page == 0)
            $page = 1;
        $start_page = ($page - 1) * LIMIT_END;
        $this->data['page'] = $page;
        return array('start_page' => $page, 'limit' => " offset ".$start_page." LIMIT ". LIMIT_END);
    }

    public function check_login()
    {
        if ($this->session->userdata('logged_in')) {
            return true;
        } else {
            return false;
        }
    }


    function getAll($query)
    {
        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            return $result->result_array();
        } else {
            return "";
        }

    }

    function getRow($query)
    {

        $result = $this->db->query($query);
        if ($result->num_rows > 0) {
            return $result->row_array();

        } else {
            return "";
        }
    }

    function insert($table, $data)
    {
        $data = $this->strip_addslashes($data);
        //$this->set_activity_log($table,$data,"Insert");
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    function update($table, $data, $where)
    {
        $data = $this->strip_addslashes($data);
        $this->db->update($table, $data, $where);
        return true;
    }

    function Execute($query)
    {
        return $this->db->query($query);
    }

    function Delete($table, $where)
    {
        if ($where != "" && $table != "") {
            return $this->db->query("DELETE FROM " . $table . " WHERE " . $where);
        }
    }

    function get_ip_address()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    } else
                        return $ip;
                }
            }
        }
    }

    function hashPass($password)
    {
        return md5(trim($password));
    }

    function getMenus()
    {
        $permissions_obj = new Tyr_permissions_entity();
        $permissions_obj->user_id = $this->session->userdata('user_id');
        $permissions_obj->get_all_user_permissions();
        
        //$return_row = $this->Execute("SELECT * FROM " . TABLE_PERMISSIONS . " where user_id = '" . $this->session->userdata('user_id') . "' ");
        if ($permissions_obj->permission_id == 0) {
            $user_id = '1';
        } else {
            $user_id = $this->session->userdata('user_id');
        }
        
        $permissions_obj = new Tyr_permissions_entity();
        $permissions_obj->user_id = $user_id;
        $get_permissions = $permissions_obj->get_all_user_permissions();
        
        //$get_permissions = $this->getRow("SELECT * FROM " . TABLE_PERMISSIONS . " where user_id = '" . $user_id . "' ");
        $menu_id = $get_permissions['module_id'];
        
        $modules_obj = new Tyr_modules_entity();
        $get_modules = $modules_obj->get_all_system_modules_in($menu_id);
        
        //$get_modules = $this->getAll("SELECT * FROM " . TABLE_MODULES . " WHERE module_id IN (" . $menu_id . ")");
        return $get_modules;
    }

    /*function getMenus()
        {
            $return_row = $this->Execute("SELECT * FROM " . TABLE_PERMISSIONS . " where user_id = '" . $this->session->userdata('user_id') . "' ");
            if ($return_row->num_rows <= 0) {
                $user_id = '1';
            } else {
                $user_id = $this->session->userdata('user_id');
            }
            $get_permissions = $this->getRow("SELECT * FROM " . TABLE_PERMISSIONS . " where user_id = '" . $user_id . "' ");
            $menu_id = $get_permissions['module_id'];
            $get_modules = $this->getAll("SELECT * FROM " . TABLE_MODULES . " WHERE module_id IN (" . $menu_id . ")");
            return $get_modules;
        }*/

    function get_system_modules($where = '')
    {
        $modules_obj = new Tyr_modules_entity();
        $modules = $modules_obj->get_system_modules($where, $this->session->userdata('module_id'));
        if (count($modules) && is_array($modules)) {
            foreach ($modules as $k => $v) {
                $subtext = "";
                $moduletitle = "";
                if ($v['module_title'] != "")
                    $moduletitle = defined(($v['module_title'])) ? constant($v['module_title']) : '';
                if ($v['sub_text'] != "")
                    $subtext = defined(($v['sub_text'])) ? constant($v['sub_text']) : '';

                $this->data['system_modules'][$v['module_type']][] = array(
                    'module_id' => $v['module_id'],
                    'module_title' => $moduletitle,
                    'module_file' => $v['module_file'],
                    'sub_text' => $subtext,
                    'link' => $v['link'],
                    'module_img' => $v['module_img']
                );
            }
        }

    }

    function get_profile_score($profile_id,$user_type)
    {
        $rate_review_obj = new Tyr_rate_review_entity();
        $row = $rate_review_obj->get_all_rate_review_by_user_type($user_type,$profile_id);
        
        if(!empty($row)){
            foreach ($row as $k => $val) {
                $rows[$k] = ($val['technical'] + $val['creative'] + $val['impression']) / 3;
            }
            foreach ($rows as $key => $vals) {
                $counter += $vals;
            }
            $final_result = (int)($counter / count($rows));
            return $final_result;
        }
    }

    function publish_or_not(){
        $user_id = $this->session->userdata("user_id");
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        
        //$row = $this->getRow("SELECT publish_account FROM ".TABLE_USERS." WHERE user_id = '" . $user_id . "' ");
        return $users_obj->publish_account;
    }

    function get_profile_completeness()
    {
           /*$row = $this->getRow("SELECT (
               ( CASE WHEN username IS NOT NULL  THEN 20  ELSE 0 END ) +
               ( CASE WHEN company_name IS NOT NULL THEN 20  ELSE 0 END ) +
               ( CASE WHEN edu_position IS NOT NULL THEN 20   ELSE 0 END ) +
               ( CASE WHEN award IS NOT NULL  THEN 20 ELSE 0 END ) +
               ( CASE  WHEN skill IS NOT NULL THEN 20 ELSE 0 END ) ) AS count_acc
                FROM tyr_users
             INNER JOIN tyr_experience ON tyr_experience.user_id=tyr_users.user_id
             INNER JOIN `tyr_education` ON tyr_education.user_id=tyr_users.user_id
             INNER JOIN `tyr_awards` ON tyr_awards.user_id=tyr_users.user_id
             INNER JOIN `tyr_skills` ON tyr_skills.user_id=tyr_users.user_id

           WHERE tyr_users.user_id = '".$profile_id."'
           AND tyr_users.status_sl='1'  AND tyr_experience.status_sl='1' AND tyr_education.status_sl='1'
           AND tyr_awards.status_sl='1' AND tyr_skills.status_sl='1'  GROUP BY tyr_users.user_id");*/
           $user_id = $this->session->userdata("user_id");
           
           $joins_obj = new Tyr_joins_entity();
           $profile_scorer = $joins_obj->get_user_profile_score($user_id);
           $profile_scorer = $profile_scorer['cnt']*1;
           
           $resume_scorer = $joins_obj->get_user_resume_score($user_id);
           $resume_scorer= $resume_scorer['cnt']*10;

                  /* $portfolio_image_scorer=$this->getRow(" SELECT (
                   (CASE WHEN COUNT(media_name) != '0'   THEN 10  ELSE 0 END )) AS cnt
                   FROM " . TABLE_MEDIA . "  WHERE tyr_media.user_id = '" . $user_id . "' AND tyr_media.status_sl='1' AND tyr_media.media_type='image'");

                   $portfolio_video_scorer=$this->getRow(" SELECT (
                           (CASE WHEN COUNT(media_name) != '0'   THEN 10  ELSE 0 END )) AS cnt
                           FROM " . TABLE_MEDIA . "  WHERE tyr_media.user_id = '" . $user_id . "' AND tyr_media.status_sl='1' AND tyr_media.media_type='video'");


                   $portfolio_scorer=$portfolio_image_scorer['cnt']+$portfolio_video_scorer['cnt'];*/


           $portfolio_scorer = $joins_obj->get_user_portfolio_score($user_id);
           $portfolio_scorer = $portfolio_scorer['cnt']*10;

           //$portfolio_scorer=$portfolio_image_scorer['cnt']*10;
                      

                  // $this->data['portfolio_scorer']= $portfolio_scorer;
           $scorer=array("portfolio_scorer"=>$portfolio_scorer,
               "profile_scorer"=>$profile_scorer,
               "resume_scorer"=>$resume_scorer,
               "fullstatus"=>$portfolio_scorer+$profile_scorer+$resume_scorer,
               "incomplete"=> 100 - ($portfolio_scorer+$profile_scorer+$resume_scorer)
           );
          /* echo ($portfolio_scorer+$profile_scorer+$resume_scorer)/3;
echo '<pre>';print_r($scorer );echo '</pre>';*/
           return $scorer;
    }

    function get_profile_review($profile_id)
    {
        $rate_review_obj = new Tyr_rate_review_entity();
        $rate_review_obj->tyroe_id = $profile_id;
        $row = $rate_review_obj->get_sum_all_rate_review_by_user();
        return $row;
    }

    //not in use
    function set_activity_stream($user_id, $tyroe_id = "", $job_id = "", $reviewer_type = '', $activity_data = "", $activity_type = '')
    {
        #JOB ACTIVITY START
//        $job_activity = array(
//            'performer_id' => $user_id,
//            'job_id' => $job_id,
//            'object_id' => $tyroe_id,
//            'activity_data' => serialize($activity_data),
//            'activity_type' => $activity_type,
//            'created_timestamp' => strtotime("now"),
//            'status_sl' => '1'
//        );
//        $return = $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
        
        $job_activity_entity = new Tyr_job_activity_entity();
        $job_activity_entity->performer_id = $user_id;
        $job_activity_entity->job_id = $job_id;
        $job_activity_entity->object_id = $tyroe_id;
        $job_activity_entity->activity_data = serialize($activity_data);
        $job_activity_entity->activity_type = $activity_type;
        $job_activity_entity->status_sl = '1';
        $job_activity_entity->created_timestamp = strtotime("now");
        var_dump($job_activity_entity);
        $job_activity_entity->save_job_activity();

        return $job_activity_entity->job_activity_id;
    }

    // currently in use
    function set_activity_stream_new($performer_id, $job_id = "", $object_id = "", $activity_data = "", $activity_type)
        {
            #JOB ACTIVITY START
//            $job_activity = array(
//                'performer_id' => $performer_id,
//                'job_id' => $job_id,
//                'object_id' => $object_id,
//                'activity_data' => serialize($activity_data),
//                'activity_type' => $activity_type,
//                'created_timestamp' => strtotime("now"),
//                'status_sl' => '1'
//            );
//            $return = $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $performer_id;
            $job_activity_obj->job_id = $job_id;
            $job_activity_obj->object_id = $object_id;
            $job_activity_obj->activity_type = $activity_type;
            $job_activity_obj->activity_data = serialize($activity_data);
            $job_activity_obj->status_sl = 1;
            $job_activity_obj->created_timestamp = strtotime("now");
            $return = $job_activity_obj->save_job_activity();

            return $return;
        }

    function get_activity_stream($where,$order = '',$pagination_limit = '')
    {
        $joins_obj = new Tyr_joins_entity();
        $return = $joins_obj->get_activity_stream($where,$order,$pagination_limit);
        /*echo "SELECT job_activity_id,j_act.object_id,j_act.job_id,j_act.activity_data,job.job_title,j_act.performer_id,act_type.activity_type_id,
                    act_type.activity_title AS activity_type,j_act.created_timestamp,users.username
                    FROM " . TABLE_JOB_ACTIVITY . " j_act
                    INNER JOIN  " . TABLE_USERS . " users ON j_act.performer_id=users.user_id
                    INNER JOIN  " . TABLE_ACTIVITY_TYPE . " act_type ON act_type.activity_type_id=j_act.activity_type
                    LEFT JOIN  " . TABLE_JOBS . " job ON j_act.job_id=job.job_id
                    WHERE j_act." . $where . " AND j_act.status_sl = 1  ".$order.$pagination_limit;die;*/
        return $return;

    }


    function activity_sub_type_text($data)
    { //Only for Tyroe
        $activity_arr = array();
        $activity_log = $data;
        $activity_data = unserialize(stripslashes($activity_log['activity_data']));
        switch($activity_data['activity_sub_type']){
            case LABEL_TYROE_APPLIED_FOR_JOB:
                $activity_arr['icon'] = LABEL_ICON_STAR;
                $activity_arr['text'] = LABEL_USER_NEW_JOB_LOG_TEXT . $activity_data['job_title'] ." at ". $activity_data['company_name'];
                $url = "opening/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                $activity_arr['href'] = $this->getURL($url);
                break;
            case LABEL_TYROE_REJECT_JOB_INVITATION:
                $activity_arr['icon'] = LABEL_ICON_MOVE;
                $activity_arr['text'] = LABEL_TYROE_REJECT_JOB_INVITATION_TEXT . " " . $activity_data['job_title'] ." at ". $activity_data['company_name'];
                $url = "opening/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                $activity_arr['href'] = $this->getURL($url);
                break;
            case LABEL_TYROE_ACCEPT_JOB_INVITATION:
                $activity_arr['icon'] = LABEL_ICON_MOVE;
                $activity_arr['text'] = LABEL_TYROE_ACCEPT_JOB_INVITATION_TEXT . " " . $activity_data['job_title'] ." at ". $activity_data['company_name'];
                $url = "opening/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                $activity_arr['href'] = $this->getURL($url);
                break;
        }
        return $activity_arr;
    }

    function studio_reviewer_activity_sub_type_text($data)
    { //Only for Studio and Reviewer
        $activity_arr = array();
        $activity_log = $data;
        $activity_data = unserialize(stripslashes($activity_log['activity_data']));
        if($this->session->userdata("role_id") == 3){
             switch($activity_data['activity_sub_type']){
                case LABEL_TYROE_APPLIED_FOR_JOB:
                    $activity_arr['icon'] = LABEL_ICON_STAR;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . LABEL_TYROE_JOB_APPLIED_TEXT . $activity_data['job_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','applicant')";
                    break;
                case LABEL_TYROE_REJECT_JOB_INVITATION:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . " rejected your invitation for " . $activity_data['job_title'] . ".";
                    $activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    break;
                case LABEL_TYROE_ACCEPT_JOB_INVITATION:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . " accepted your invitation for " . $activity_data['job_title'] . ".";
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','candidate')";
                    break;
                case LABEL_REVIEWED_ACTIVITY_NAME:
                    if($activity_data['reviewer_name'] != ''){
                        $activity_arr['icon'] = LABEL_ICON_ENVLOPE;
                        $activity_arr['text'] = $activity_data['reviewer_name'] . " has reviewed " . $activity_data['tyroe_name']." for ".$activity_data['job_opening_title'];
                        //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                        $activity_arr['href'] = "javascript:void(0)";
                        $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_opening_title']."','review')";
                    }
                    break;
                case LABEL_STUDIO_CREATED_JOB:
                    $activity_arr['icon'] = LABEL_ICON_ENVLOPE;
                    $activity_arr['text'] = "You created a new job opening for ".$activity_data['job_title'];
                    $activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));;
                    break;
                case LABEL_INVITE_TYROE_JOB_LOG:
                    $activity_arr['icon'] = LABEL_ICON_ENVLOPE;
                    $activity_arr['text'] = $activity_data['performer_name'] . " added " . $activity_data['tyroe_name']." as a candidate for ".$activity_data['job_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','candidate')";
                    break;
                case LABEL_APPLIED_TO_CANDIDATE:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = "You added " . $activity_data['tyroe_name']. " as a candidate for ".$activity_data['job_title'].".";
                    //$activity_arr['text'] = $activity_data['performer_name']." added " . $activity_data['tyroe_name']. " as a candidate for ".$activity_data['job_title'].".";
                    $activity_arr['href'] = $this->getURL("openings");
                    break;
                case LABEL_CANDIDATE_TO_SHORTLIST:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['tyroe_name']." was shortlisted for ".$activity_data['job_title'].".";
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','shortlist')";
                    break;
            }
        } else if($this->session->userdata("role_id") == 4){
            switch($activity_data['activity_sub_type']){
                case LABEL_TYROE_APPLIED_FOR_JOB:
                    $activity_arr['icon'] = LABEL_ICON_STAR;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . LABEL_TYROE_JOB_APPLIED_TEXT . $activity_data['job_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));//$this->getURL("openings/FilterJobs/".$activity_data['job_id']);
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','applicant')";
                    break;
                case LABEL_TYROE_REJECT_JOB_INVITATION:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . " rejected your invitation for " . $activity_data['job_title'] . ".";
                    $activity_arr['href'] = "javascript:void(0)";//$this->getURL("openings/FilterJobs/".$activity_data['job_id']);
                    break;
                case LABEL_TYROE_ACCEPT_JOB_INVITATION:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['tyroe_name'] . " accepted your invitation for " . $activity_data['job_title'] . ".";
                    $activity_arr['href'] = "javascript:void(0)";//$this->getURL("openings/FilterJobs/".$activity_data['job_id']);
                    break;
                case LABEL_REVIEWED_ACTIVITY_NAME:
                    $activity_arr['icon'] = LABEL_ICON_ENVLOPE;
                    $activity_arr['text'] = "You reviewed " . $activity_data['tyroe_name'] . " for ".$activity_data['job_opening_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','review')";
                    break;
                case LABEL_DECLINED_ACTIVITY_NAME:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = "You declined " . $activity_data['tyroe_name'] . " for ".$activity_data['job_opening_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_opening_title']."','declined')";
                    break;
                case LABEL_ADD_REVIEWER_TO_JOB_LOG:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = "You have been added as team member for " . $activity_data['job_opening_title'];
                    $activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    break;
                case LABEL_INVITE_TYROE_JOB_LOG:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['performer_name']." added " . $activity_data['tyroe_name'] . " as a candidate for " . $activity_data['job_title'];
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','candidate')";
                    break;
                case LABEL_APPLIED_TO_CANDIDATE:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = "You added " . $activity_data['tyroe_name']. " as a candidate for ".$activity_data['job_title'].".";
                    $activity_arr['href'] = $this->getURL("openings");
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','candidate')";
                    break;
                case LABEL_CANDIDATE_TO_SHORTLIST:
                    $activity_arr['icon'] = LABEL_ICON_MOVE;
                    $activity_arr['text'] = $activity_data['studio_admin_name']." shortlisted ".$activity_data['tyroe_name']." for ".$activity_data['job_title'].".";
                    //$activity_arr['href'] = "openings/".strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($activity_data['job_id']));
                    $activity_arr['href'] = "javascript:void(0)";
                    $activity_arr['onclick']= "jumpto('".$activity_data['job_id']."','".$activity_data['job_title']."','shortlist')";
                    break;
            }
        }
        return $activity_arr;
    }

    function set_activity_log($table_name, $data, $activity_status)
    {
        $user_id = $this->session->userdata("user_id");
        #JOB ACTIVITY START
//        $main_activity = array(
//            'user_id' => $user_id,
//            'data' => $data,
//            'table_name' => $table_name,
//            'ip_address' => $this->get_ip_address(),
//            'created_timestamp' => strtotime("now"),
//            'activity_status' => $activity_status
//        );
//        $return = $this->insert(TABLE_ACTIVITY_LOG, $main_activity);
        
        $activity_log_obj = new Tyr_activity_log_entity();
        $activity_log_obj->user_id = $user_id;
        $activity_log_obj->data = $data;
        $activity_log_obj->table_name = $table_name;
        $activity_log_obj->ip_address = $this->get_ip_address();
        $activity_log_obj->created_timestamp = strtotime("now");
        $activity_log_obj->activity_status = $activity_status;
        $activity_log_obj->save_activity_log();
        
    }

    function get_system_variables()
    {
        $system_variables_obj = new Tyr_system_variables_entity();
        $get_variables = $system_variables_obj->get_all_system_variables();
        
        //$get_variables = $this->getAll("select * from " . TABLE_SYSTEM_VARIABLES);

    }

    function send_notification($notification_id, $to)
    {
        $this->session->userdata("firstname");
        $this->session->userdata("lastname");
        
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->notification_id = $notification_id;
        $get_notification = $notification_templates_obj->get_notification_templates_by_notification_id();
        
        
        //$get_notification = $this->getRow("SELECT * FROM " . TABLE_NOTIFICATION_TEMPLATE . " where notification_id='" . $notification_id . "'");
        // $message=$this->load->view('email_templates/notification_template',$get_notification['template']);
        $return = $this->email_sender($to, $get_notification['title'], $get_notification['template']);

//        $notification_details = array(
//            "send_to" => $to,
//            "notification_id" => $notification_id,
//            "notification_status" => $return,
//            "created_timestamp" => strtotime("now")
//        );
//        $this->insert(TABLE_NOTIFICATION_LOG, $notification_details);
        
        $notification_log_obj = new Tyr_notification_log_entity();
        $notification_log_obj->send_to = $to;
        $notification_log_obj->notification_id = $notification_id;
        $notification_log_obj->notification_status = $return;
        $notification_log_obj->created_timestamp = strtotime("now");
        $notification_log_obj->save_notification_log();

        return $return;
    }

    function pro_tyroe()
    {
        $pro = array("label" => "Pro Only",
            "link" => "account/subscription",);
        return $pro;
    }

    function limited_words($data = null, $words_limit = 20) {
        $data = strip_tags($data);
        $text_len = strlen($data);

        $array_words = @array_values(array_filter(explode(' ',$data)));
        $array_words_count=count($array_words);
        $foreach_inc = 0;
        $return_data =NULL;
        if(is_objarray($array_words)){
            foreach($array_words as $word_key=>$word_value){
                $foreach_inc++;
                if($words_limit == $foreach_inc) {
                    $return_data .=$word_value.'';
                    break;
                } else {
                    $return_data .=$word_value.' ';
                }
            }
        }
        if($words_limit<$array_words_count-1) {

            $return_data = $return_data.'...';
        }
        return $return_data;
    }

    function generate_job_number($number){
        $number_zeros = '0000';
        $number_length = strlen($number);
         if($number_length <= 4){
        	$your_number = substr($number_zeros,0,4-$number_length);
        	$your_number = $your_number.$number;
         }else{
         	$your_number = $number;
         }
        return $your_number;
    }

    function get_number_from_name($name){
        $get_break_array = explode('-',$name);
        $id = '';
        for($a=0; $a<count($get_break_array); $a++){
        	if(is_numeric($get_break_array[$a])){
        		$id = $get_break_array[$a];
        	}
        }
        return round($id);
    }

    function custom_crop_image($image_source,$image_width,$image_height,$ext){
        $filename = $image_source;
        if($ext=='.png'){
            $image = imagecreatefrompng($filename);
        }elseif($ext=='.gif'){
            $image = imagecreatefromgif($filename);
        }elseif($ext=='.jpg'){
            $image = imagecreatefromjpeg($filename);
        }

        $thumb_width = $image_width;
        $thumb_height = $image_height;

        $width = imagesx($image);
        $height = imagesy($image);

        $original_aspect = $width / $height;
        $thumb_aspect = $thumb_width / $thumb_height;

        if ( $original_aspect >= $thumb_aspect )
        {
           // If image is wider than thumbnail (in aspect ratio sense)
           $new_height = $thumb_height;
           $new_width = $width / ($height / $thumb_height);
        }
        else
        {
           // If the thumbnail is wider than the image
           $new_width = $thumb_width;
           $new_height = $height / ($width / $thumb_width);
        }

        $thumb = imagecreatetruecolor( $thumb_width, $thumb_height );
        if($ext=='.png'){
            imagealphablending($thumb, false);
            imagesavealpha($thumb,true);
            $transparent = imagecolorallocatealpha($thumb, 255, 255, 255, 127);
            imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $transparent);
            imagecopyresampled($thumb,
                                $image,
                                0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                                0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                                0, 0,
                                $new_width, $new_height,
                                $width, $height);
            imagepng($thumb, $filename);
        }elseif($ext=='.gif'){
            imagecopyresampled($thumb,
                $image,
                0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                0, 0,
                $new_width, $new_height,
                $width, $height);
            imagegif($thumb, $filename, 100);
        }elseif($ext=='.jpg'){
           imagecopyresampled($thumb,
                            $image,
                            0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                            0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                            0, 0,
                            $new_width, $new_height,
                            $width, $height);
            imagejpeg($thumb, $filename, 100);
        }

        /*imagecopyresampled($thumb,
                           $image,
                           0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                           0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                           0, 0,
                           $new_width, $new_height,
                           $width, $height);
        imagejpeg($thumb, $filename, 80);*/
    }

    function cleanString($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function send_linkedinpost($user_id,$comment) {
        //$in_details = $this->getRow("SELECT * FROM tyr_fb_users WHERE user_id='".$user_id."' AND socialmedia_type='in' AND status_sl='1'");
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'in';
        $in_details = $social_media_obj->get_user_social_media_details();
        
        if($in_details !=""){
            $access_token = $in_details['access_token'];
            $content ="<share>
                            <comment>".$comment."</comment>
                            <content>
                                <title>The title of the thing</title>
                                <description>the description of the thing</description>
                                <submitted-url>https://dev.tyroe.com/</submitted-url>
                                <submitted-image-url>http://dev.tyroe.com/assets/img/logo_tyroe.png</submitted-image-url>
                            </content>
                            <visibility>
                                <code>anyone</code>
                            </visibility>
                        </share>";
            $handle = curl_init();
            curl_setopt($handle, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($handle, CURLOPT_URL, "https://api.linkedin.com/v1/people/~/shares?format=json&oauth2_access_token={$access_token}");
            curl_setopt($handle, CURLOPT_VERBOSE, TRUE);
            $header[] = 'Content-Type: text/xml; charset=UTF-8';
            curl_setopt($handle, CURLOPT_POSTFIELDS, $content);
            curl_setopt($handle, CURLOPT_HTTPHEADER, $header);
            curl_exec($handle);
        }
    }

    function send_tweet($user_id,$tweet_message){
         $tw_details = $this->getRow("SELECT * FROM tyr_fb_users WHERE user_id='".$user_id."' AND socialmedia_type='tw' AND status_sl='1'");
         if($tw_details !=""){
             $userdata = unserialize($tw_details['user_data']);
             $this->load->library('twitteroauth');
             $consumer_key       = 'cO4pv1U4GvAKdw9e9tpzQ';
             $consumer_secret    = 'rr6OYVCQGraf5ESfSKzItdTUABbkETHgAhYFN8zqY';
             $oauth_token        = $userdata['oauth_token'];
             $oauth_secret       = $userdata['oauth_token_secret'];
             $connection = new TwitterOAuth($consumer_key, $consumer_secret, $oauth_token, $oauth_secret);
             $connection->host = "https://api.twitter.com/1.1/";
             $connection->ssl_verifypeer = TRUE;
             $parameters = array('name'=>'Tyroe','screen_name'=> 'Tyroe','status'=>$tweet_message." ".rand()."  @TyroeApp");
             $status = $connection->post('statuses/update',$parameters);
         }
    }
    
    function _add_css() {
        for ($i = 0; $i < func_num_args(); $i++) {
            $css_file = func_get_arg($i);
            if (strlen($css_file) > 0 && !in_array($css_file, $this->data['stylesheets'])) {
                $this->data['stylesheets'][] = $css_file;
            }
        }
    }
    
    function _add_js() {
        for ($i = 0; $i < func_num_args(); $i++) {
            $js_file = func_get_arg($i);
            if (strlen($js_file) > 0 && !in_array($js_file, $this->data['scripts'])) {
                $this->data['scripts'][] = $js_file;
            }
        }
    }
    
    function get_company_name(){
        $parent_id = $this->session->userdata("parent_id");
        if(intval($parent_id) == 0){
            $parent_id = $this->session->userdata("user_id");
        }
        $company_details_obj = new Tyr_company_detail_entity();
        $company_details_obj->studio_id = $parent_id;
        $company_details_obj->get_company_detail_by_studio();
        $this->data['company_name'] = $company_details_obj->company_name;
    }
    
    function get_extension($filename) {
        $x = explode('.', $filename);
        return '.' . end($x);
    }
}