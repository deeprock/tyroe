<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

#PAGE TITLES
define("LABEL_LOGIN", "WELCOME BACK !");
//define("LABEL_PASSWORD_FORGOT", "Forgotten Your Password?");
define("LABEL_FEATURED_USERS","Featured");
define("LABEL_PASSWORD_FORGOT", "Forgot Password?");
define("LABEL_REGISTERATION", "SIGN UP");
define("LABEL_CREATE_ACCOUNT", "CREATE YOUR ACCOUNT");
define("LABEL_EXISTING_PW", "Existing Password");
define("LABEL_REGISTERATION_SUCCESS", "Registeration Success");
define("LABEL_SUBSCRIPTION_SUCCESS", "Subscription Success");
define("LABEL_TYROE", "Tyroe");
define("LABEL_STUDIO", "Studio");
define("LABEL_PASSWORD_CHANGE", "Change Password");
define("LABEL_EDIT_PROFILE", "Edit Profile");
define("LABEL_EDIT_RESUME", "Edit Resume");
define("LABEL_CANDIDATE_ROLES", "You are a candidate for the following current openings");
define("LABEL_SHORTLISTED_ROLES", "You have been shortlisted for the following current openings");
define("LABEL_EDIT_PORTFOLIO", "Edit Portfolio");
define("LABEL_EXPERIENCE_TITLE", "Experience");
define("LABEL_EDUCATION_TITLE", "Education");
define("LABEL_PASSWORD_CHANGED", "Password Changed");
define("LABEL_ADD_EXPERIENCE", "Add Experience");
define("LABEL_EDIT_EXPERIENCE", "Edit Experience");
define("LABEL_ADD_EDUCATION", "Add Education");
define("LABEL_EDIT_EDUCATION", "Edit Education");
define("LABEL_ADD_SKILLS", "Add Skills");
define("LABEL_SET_SKILLS", "Set Speciality");
define("LABEL_EDIT_SKILLS", "Edit Skills");
define("LABEL_SKILL_TITLE", "Skill");
define("LABEL_SPECIALITY_TITLE", "Speciality");
define("LABEL_ADD_AWARDS", "Add Awards");
define("LABEL_EDIT_AWARDS", "Edit Awards");
define("LABEL_AWARD_TITLE", "Award");
define("LABEL_ADD_REFERENCES", "Add References");
define("LABEL_REQUEST_REFERENCES", "Request Recommendation");
define("LABEL_EDIT_REFERENCES", "Edit References");
define("LABEL_CREATE_OPENING", "Create Opening");
define("LABEL_REFRENCE_TITLE", "Reference");
define("LABEL_RECOMMENDATION_TITLE", "Recommendations");
define("LABEL_RECOMMENDATION_TEXT", "Enter your friend email.");
define("LABEL_ADD_PORTFOLIO", "Add Portfolio");
define("LABEL_MEDIA_TITLE", "Title");
define("LABEL_IMAGE_DESCRIPTION", "Description");
define("LABEL_FEATURED", "Set as feature item");
define("LABEL_IMAGE", "Image");
define("LABEL_JOB_OPENING", "Job Opening");
define("LABEL_JOB_LOCATION", "Location");
define("LABEL_JOB_SKILL_LEVEL", "Skill Level");
define("LABEL_JOB_QUANTITY", "Quantity");
define("LABEL_FIND_TALENT", "FIND TALENT");
define("LABEL_INVITE_TEAM", "INVITE TEAM");
define("LABEL_SEARCH", "Search");
define("LABEL_OPENING_OVERVIEW", "Opening Overview");
define("LABEL_JOB_OVERVIEW", "Job Overview");
define("LABEL_JOB_OVERVIEW_SUBTITLE", "General information and activity stream");

define("LABEL_REVIEW_TEAM", "Review Team");
define("LABEL_REVIEW_TEAM_SUBTITLE", "Invite and manage your Reviewers");
define("LABEL_CANDIDATES", "Candidates");
define("LABEL_CANDIDATE_SUBTITLE", "Review all artists information and scores");
define("LABEL_SHORTLISTED", "Shortlisted");
define("LABEL_SHORTLISTED_SUBTITLE", "Candidates worth interviewing for job");
define("LABEL_HIDDEN", "Hidden");
define("LABEL_HIDDEN_SUBTITLE", "Candidates hidden from job listing");
define("LABEL_REVIEWER", "REVIEWER");
define("LABEL_CREATE_REVIEWER", "Create Reviewer");
define("LABEL_ACCOUNT", "ACCOUNT");

#Award Form
define("LABEL_AWARD_ORGANIZATION", "Organization");
define("LABEL_YEAR", "Year");
define("LABEL_DESCRIPTION", "Description");

#SUB TITLES
define("LABEL_PROFILE_SUBTITLE", "Edit your information and settings");
define("LABEL_RESUME_SUBTITLE", "Showcase your experience and skills");
define("LABEL_PORTFOLIO_SUBTITLE", "Upload your best images and videos");
define("LABEL_SUBSCRIPTION_SUBTITLE", "Account and billing information");

#MODULES
define("LABEL_MODULE_HOME", "Home");
define("LABEL_MODULE_ACCOUNT", "Account");
define("LABLE_STUDIO_TEAM", "Team");
define("LABEL_MODULE_EXPOSURE", "Activity");
define("LABEL_MODULE_ACTIVITY_SUB_TEXT", "Real-Time notifications about your profile");
define("LABEL_MODULE_PROFILE_SIDE_MENU", "Profile");
define("LABEL_MODULE_OPENINGS", "Openings");
define("LABEL_MODULE_FAVOURITES", "Favourites");
define("LABEL_MODULE_SEARCH", "Search");
define("LABEL_MODULE_STREAM", "Activity Stream");
define("LABEL_STUDIO_ACTIVITY", "Activity");
define("LABEL_MODULE_SUBSCRIPTION", "Subscription");
define("LABEL_MODULE_PROFILE", "Profile");
define("LABEL_MODULE_RESUME", "Resume");
define("LABEL_MODULE_PORTFOLIO", "Portfolio");
define("LABEL_MODULE_JOB", "Job");
define("LABEL_MODULE_USER", "Users");
define("LABEL_MODULE_CONFIGURATION", "Configuration");
define("LABEL_SAVE", "Save");
define("LABEL_DELETE", "Delete");
define("LABEL_UPLOAD", "Upload");
define("LABEL_EDIT", "Edit");
define("LABEL_MODULE_SOCIAL", "Linked Accounts");
define("LABEL_SOCIAL_TITLE", "Linked Accounts");
define("LABEL_MODULE_SOCIAL_SUB_TEXT", "Link your social accounts to update them directly from Tyroe.");
define("LABEL_PROFILE_VIEWS_MODULE", "Profile Views");
define("LABEL_PROFILE_VIEWS_SUB_TEXT", "Wonder who has viewed your profile");
define("LABEL_FEEDBACK_RATING_MODULE", "Feedback & Ratings");
define("LABEL_FEEDBACK_RATING_SUB_TEXT", "What the industry players think of your profile");
define("LABEL_CANDIDATE_ROLES_MODULE", "Candidate Roles");
define("LABEL_CANDIDATE_ROLES_SUB_TEXT", "The roles that you are currently a candidate for");
define("LABEL_SHORTLISTED_ROLES_MODULE", "Shortlisted Roles");
define("LABEL_SHORTLISTED_ROLES_SUB_TEXT", "The roles that you are currently shorlisted for");
define("LABEL_ACCOUNT_DETAILS_TEXT", "Account Details");
define("LABEL_ACCOUNT_SUB_TEXT", "Maintain your system credentials access");
define("LABEL_ACCOUNT_SUB_TEXT_STUDIO", "Maintain your system credentials access");
define("LABEL_MODULE_SUBSCRIPTION_ADMIN", "Subscription");
define("LABEL_COUPONS_MODULE", "Coupons");
define("LABEL_COUPONS_SUB_TEXT", "Checkout your current coupons and earn new ones");
define("LABEL_INTERESTING_ROLES_MODULE", "Interesting Roles");
define("LABEL_INTERESTING_ROLES_SUB_TEXT", "The roles that you have extressed interest in.");
define("LABEL_INTERESTING_OPENINGS", "Interesting Openings");
define("LABEL_MODULE_SEARCH_PUBLICPROFILE", "Searchpublicprofile");
define("LABEL_GET_CITIES_STATES","get_cities_states");
define("LABEL_ADMIN_DELETE_STUDIO","delete_studio");
define("LABEL_ADMIN_SAVE_OPENING","saveopening");
define("LABEL_ADMIN_STUDIO_BULKS","studios/bulkaction");
define("LABEL_ADMIN_FILTER_STUDIO","studios/filter_studio");
define("LABEL_ADMIN_FILTER_TYROE","user/filter_tyroe");
define("LABEL_ADMIN_GALLERY","Gallery");
define("LABEL_SEND_EMAIL","search/send_email");
define("LABEL_MODULE_PAYMENT", "Payment");
define("LABEL_COMPANY_ACCOUNT_DETAILS_TEXT", "Company Details");
define("LABEL_COMPANY_ACCOUNT_SUB_TEXT", "Edit global settings for your company");


#FORM INPUT FIELDS
define("LABEL_USER_NAME", "Username");
define("LABEL_PASSWORD", "Password");
define("LABEL_NEW_PASSWORD", "New Password");
define("LABEL_CONFIRM_PASSWORD", "Confirm Password");
define("LABEL_FIRST_NAME", "First Name");
define("LABEL_LAST_NAME", "Last Name");
define("LABEL_OCCUPATION", "Occupation");
define("LABEL_WEBSITE", "Website");
define("LABEL_BIOGRAPHY", "Biography");
define("LABEL_OTHER_SITES", "Other Sites");
define("LABEL_EMAIL", "Email");
define("LABEL_FIRST_LAST_NAME", "First & Last Name");
define("LABEL_NAME_TEXT", "Name");
define("LABEL_FROM_TEXT", "From");
define("LABEL_MESSAGE_TEXT", "Message");
define("LABEL_INVITATION_MESSAGE", "Message");
define("LABEL_PHONE", "Phone");
define("LABEL_FAX", "Fax");
define("LABEL_COUNTRY", "Country");
define("LABEL_STATE", "State");
define("LABEL_TYROE_LEVEL", "Level & Experience");
define("LABEL_CITY", "City");
define("LABEL_ZIPCODE", "Zip Code");
define("LABEL_SUBURB", "Suburb");
define("LABEL_ADDRESS", "Address");
define("LABEL_CELLPHONE", "Phone");
define("LABEL_REVIEWER_TASK", "Reviewer Task");
define("LABEL_ROLE", "Role");
define("LABEL_REMMEMBER_ME", "Remember Me");
define("LABEL_NO_ACCOUNT", "Don't have an account?");
define("LABEL_NEED_ACCOUNT_FOR_COMPANY", "Don't have an account for your company?");
define("LABEL_ALREADY_ACCOUNT", "Already have an account?");

define("LABEL_LOCATION", "Location");
define("LABEL_JOB_TITLE", "Job Title");
define("LABEL_COMPANY_NAME", "Company Name");
define("LABEL_START_DATE", "Start Date");
define("LABEL_END_DATE", "End Date");
define("LABEL_CURRENT_JOB", "Current Job");
define("LABEL_JOB_DESCRIPTION", "Job Description");
define("LABEL_JOB_TAGS", "Job Skills");
define("LABEL_WEBSITE_URL", "Website URL");
define("LABEL_JOB_INDUSTRY","Industry");

define("LABEL_DEGREE_TITLE", "Degree Title");
define("LABEL_ORGANIZATION", "Organization");
define("LABEL_POSITION", "Position");
define("LABEL_INSTITUTE_NAME", "Institute Name");
define("LABEL_FIELD", "Field Interest");
define("LABEL_GRADE", "Grade");
define("LABEL_START_YEAR", "Start Year");
define("LABEL_END_YEAR", "End Year");
define("LABEL_EDUCATION_DESCRIPTION", "Education Description");
define("LABEL_ACTIVITIES", "Activities");
define("LABEL_APPLY_JOB", "Apply Job");
define("LABEL_APPLICANTS", "Applicants");
define("LABEL_APPLICANT_SUBTITLE", "Candidates applied for job");
define("LABEL_PUBLIC_PROFILE", "Profile URL");
#FORM BUTTONS
define("LABEL_SIGNUP", "Sign up");
define("LABEL_CANCEL_BUTTON", "CANCEL");
define("LABEL_SIGNUP_BUTTON", "SIGN UP & GET STARTED");
define("LABEL_SAVE_CHANGES", "Save changes");
define("LABEL_DELETE_MY_ACCOUNT", "Please delete my account");
define("LABEL_SEND_ANNONYMOUS", "Send Annoynmous Message");
define("LABEL_SAVE_CHANGES_NOTIFICATION", "Change Notification Details");
define("LABEL_SIGNIN", "Sign In");
define("LABEL_SUBMIT", "Submit");
define("LABEL_BACK", "Back");
define("LABEL_NOT_MEMBER", "Not a Member Sign up Now");
define("LABEL_SEND_INVITATION_BUTTON", "Send Invite");
define("MESSAGE_SEND_INVITATION", "Success Invitation");
define("MESSAGE_SEND_REVIEWER_INVITATION_ERROR", "Please write some emails before sending invitation.");
define("MESSAGE_SEND_REVIEWER_INVITATION", "Successfully invite reviewers");
define("MESSAGE_SEND_INVITATION_FAILED", "Failure Invitation");
define("REG_IAMTALENT","I'm talent");
define("REG_IAMCOMPANY","I'm a company");


#Email Messages
define("LABEL_EMAIL_ALREADY_REGISTERED", "Email Already Registered");
define("LABEL_USERNAME_ALREADY_REGISTERED", "Username Already Registered");
define("LABEL_NO_DETAILS", "No details supplied");
define("LABEL_NO_RECOMMENDATION", "Send Recommendation by click on Request Recommendation.");
define("LABEL_RECOMMENDATION_TEXT2", " is Recommended you on the ");
define("LABEL_RECOMMENDATION_TEXT3", " has not supplied your requested recommendation.");
define("MESSAGE_PROFILE_UPDATED", "Profile Updated Successfully");
define("MESSAGE_PROFILE_NOT_UPDATED", "Profile Not Updated Successfully");
define("MESSAGE_PROFILE_IMAGE_UPDATE", "Profile Image Updated");
define("MESSAGE_PROFILE_IMAGE_DELETED", "Profile Image Removed");
define("MESSAGE_JOB_OPENING", "New Opening Inserted Succeffully ,check Openings");
define("MESSAGE_ADD_EXPERIENCE_SUCCESS", "Experience Added");
define("MESSAGE_ADD_EDUCATION_SUCCESS", "Education Added");
define("MESSAGE_ADD_SKILL_SUCCESS", "Skills Added");
define("MESSAGE_DELETE_SUCCESS", "Delete Success");
define("MESSAGE_ALREADY_INVITE_REVIEWER", "Invitation expire!");
define("MESSAGE_ARCHIVED_JOB", "Job archived successfully");
define("MESSAGE_UNWANTEDAPPLIER", "You have'nt rights to apply on jobs");
define("MESSAGE_APPROVED_FEEDBACK", "Feedback approved successfully");
define("SUCCESS_MESSAGE_APPLY_JOB", "You have successfully Applied for the Job");
define("ERROR_MESSAGE_APPLY_JOB", "Error, failed to apply for job");
define("ERROR_JOBAPPLY_EXCEED","Sorry! Your today job apply limit is over now.");
define("ALREADY_APPLY_FOR_JOB", "You have already Applied for this Job");
define("PUBLISH_PROFILE_BEFORE_APPLY_FOR_JOB", "Woh, not so fast! You need to publish your profile before you can apply for any of these cool jobs.");
define("MESSAGE_SHORTLISTED_SUCCESS", "Shortlisted Success");
define("ALREADY_FAVOURITE_CANDIDATE", "This candidate is already Favourite");
define("UN_FAVOURITE_CANDIDATE", "is not your favourite anymore hey? OK, fair enough.");
define("MESSAGE_ADD_SUCCESS", "Add Successfully");
define("MESSAGE_UPDATE_SUCCESS", "Update Successfully");
define("MESSAGE_REVIEWER_UPDATE_SUCCESS", "Reviewer update Successfully");
define("MESSAGE_SUCCESS", "Success");
define("MESSAGE_REVIEW_UPDATE", "Review update successfully");
define("MESSAGE_REVIEW_ADD", "Reviewed successfully");
define("MESSAGE_DELETE_REVIEW_SUCCESSFULLY", "Review delete successfully");
define("MESSAGE_ALREADY_REVIEWD", "You already review this person");
define("MESSAGE_DECLINE_TYROE","OK, fair enough. [TYROE NAME] has been declined.");
define("MESSAGE_UNDODECLINE_TYROE","OK, fair enough. [TYROE NAME] has been undo.");
define("MESSAGE_INVITE_SUCCESS", "Nice one! You invited [TYROE NAME] to [JOB NAME].");
define("HIDE_MESSAGE_SUCCESS", "Fair enough! We have hidden this profile for you.");
define("UN_HIDE_MESSAGE_SUCCESS", "Fair enough! We have unhidden this profile for you.");
define("MESSAGE_ERROR", "Error");
define("MESSAGE_ALREADY_COPY_FOR_JOB", "This Candidate is already invited for this job");
define("MESSAGE_ALREADY_RATED", "This User is already rated by you");
define("MESSAGE_ALREADY_SHORTLISTED", "This User is already shortlisted for this job by you");
define("MESSAGE_EXPERIENCE_DELETE", "Experience Deleted");
define("MESSAGE_EDUCATION_DELETE", "Education Deleted");
define("MESSAGE_PASSWORD_CHANGED", "Password Changed Successfully,Now use your new password");
define("MESSAGE_PASSWORD_NOT_CHANGED", "Password Not Changed");
define("ERROR_IMAGE_UPLOAD", "Image Not Uploaded");
define("MESSAGE_REGISTERATION_SUCCESS", "Thank you, Now you are able to use the Dashboard to manage all your account, activities and experiences.");
define("MESSAGE_SUBSCRIPTION_SUCCESS", "Thank you, After 24 Hours you are able to access your PRO account as Tyroe PRO.");
define("LABEL_FORGOT_EMAIL_REQUEST_MESSAGE", "A link has been sent to your email address,please click on the link to reset your password.");
define("LABEL_FORGOT_EMAIL_NOT_FOUND", "Given Email address is not in our database, please provide corretct Email address");
define("LABEL_MODULE_DASHBOARD_UPLOAD_MEDIA","UPLOAD MEDIA");
define("LABEL_MODULE_DASHBOARD_EDIT_RESUME","EDIT RESUME");
define("LABEL_MODULE_DASHBOARD_REVIEW_FEEDBACK","REVIEW FEEDBACK");
define("LABEL_MODULE_DASHBOARD_CREATE_JOB", "CREATE JOB OPENING");
define("LABEL_MODULE_DASHBOARD_JOB", "Openings");
define("TITLE_ACTION_EDIT", "Edit");
define("TITLE_ACTION_REMOVE", "Remove");
define("LABEL_TOOLBAR_GO", "GO");
define("LABEL_TOOLBAR_NEW", "New");
define("LABEL_VIEW_PUBLIC_PROFILE", "View public profile");
define("LABEL_MODULE_STUDIOS", "Studios");
define("LABEL_SHORTLIST", "Shortlist");
define("LABEL_HIDE", "Hidden");
define("LABEL_DASHBOARD", "Dashboard");

define("LABEL_ADMIN_CREATE_USER", "Create User");
define("LABEL_ADMIN_EDIT_USER", "Edit User");
define("LABEL_ADMIN_PROFILE_USER", "Profile");
define("LABEL_ADMIN_PROFILE_SUBTEXT", "Create user information and settings");


define("LABEL_MONTHLY_ACCOUNT", "Monthly Account");
define("LABEL_QUARTERLY_ACCOUNT", "Quarterly Account");
define("LABEL_ANNUAL_ACCOUNT", "Annual Account");
define("LABEL_UPGRADE_TYROE", "Upgrade to Pro Now!");


#JOB NOTIFICATION
define("EMAIL_SUBJECT_SHORTLIST", "Shortlisted for this job");
define("EMAIL_SUBJECT_REJECTED", "Rejected for this job");
define("EMAIL_MESSAGE_SHORTLISTED", "Thank you for your application you are selected to fill this position");

define("LABEL_ADMIN_RECORD_INSERT", "Row Inserted");
define("LABEL_NOTIFICATION_TYPE", "Where should we send your notifications?");
define("LABEL_NOTIFICATION_PROCESS", "How offten do you want notifications?");
define("LABEL_SCHEDULE_NOTIFICATION", "Email Notifications");
define("LABEL_SUBSCRIPTION", "Subscription");
define("LABEL_SCHEDULE_NOTIFICATION_SUB_TITLE", "Schedule Notification Information");

# JOB ACTIVITY
define("LABEL_JOB_TITLE_CHANGE","job_title_changed");
define("LABEL_NEW_JOB_LOG", "2");
define("LABEL_APPLIED_TO_CANDIDATE", "apply_to_candidate");
define("LABEL_CANDIDATE_TO_SHORTLIST", "candidate_to_shortlist");
define("LABEL_EDIT_JOB_LOG", "editjob");
define("LABEL_DELETE_JOB_LOG", "deljob");
define("LABEL_HIDE_JOB_LOG", "hidejob");
define("LABEL_SHORTLIST_JOB_LOG", "6");
define("LABEL_SHORTLIST_JOB_LOG_TEXT","You have been shortlisted for");
define("LABEL_INVITE_FRIEND_JOB_LOG", "13");
define("LABEL_RATE_JOB_LOG", "3");
define("LABEL_COMMENT_JOB_LOG", "3");
define("LABEL_COMMENT_PROFILE_LOG", "3");
define("LABEL_APPLY_JOB_LOG", "19");
define("LABEL_TYROE_APPLIED_FOR_JOB", "jobapply");
define("LABEL_TYROE_REJECT_JOB_INVITATION", "rejected_invitation");
define("LABEL_TYROE_REJECT_JOB_INVITATION_TEXT", "You declined an invitation for");
define("LABEL_TYROE_ACCEPT_JOB_INVITATION", "accepted_invitation");
define("LABEL_TYROE_ACCEPT_JOB_INVITATION_TEXT", "You accepted an invitation for");
define("LABEL_ARCHIVED_JOB_LOG", "archivejob");
define("LABEL_ADD_CANDIDATE_JOB_LOG", "addcandidate");
define("LABEL_DECLINE_CANDIDATE", "decline tyroe");
define("LABEL_APPLICAINT_MOVE_TO_CANDIDATE", "16");
define("LABEL_MOVE_TO_CANDIDATE_TEXT", "move to candidate");
define("LABEL_CANDIDATE_MOVE_TO_SHORTLIST", "15");
define("LABEL_MOVE_TO_SHORTLIST_TEXT", "move to shortlist");
define("LABEL_SHORTLIST_MOVE_TO_CANDIDATE", "16");
define("LABEL_ADD_REVIEWER_TO_JOB_LOG", "add reviewer");
define("LABEL_PROFILE_VIEW_JOB_LOG", "7");
define("LABEL_INVITE_REVIEWER_JOB_LOG", "reviewerjob");
define("LABEL_INVITE_JOIN_TYROE_JOB_LOG", "13");
define("LABEL_INVITE_JOIN_ACCEPT_JOB_LOG", "invitationaccept");
define("LABEL_ACCEPT_JOB_INVITATION", "17");
define("LABEL_ACCEPT_JOB_INVITATION_TEXT", "accept job invitation");
define("LABEL_REJECT_JOB_INVITATION_TEXT", "reject job invitation");
define("LABEL_ICON_ENVLOPE", "icon-envelope-alt");
define("LABEL_ICON_STAR", "icon-star");
define("LABEL_ICON_MOVE", "icon-move");
define("LABEL_ICON_VIEW", "icon-eye-open");
define("LABEL_ICON_DECLINED", "icon-remove");
define("LABEL_ICON_APPLIED_JOB", "icon-upload");
define("LABEL_ICON_ADDED_AS_CANDIDATE", "icon-plus-sign");
define("LABEL_RECOMMEND_COMPLETE", "12");
define("LABEL_RECOMMEND_COMPLETE_TEXT", "You received a recommendation from");
define("LABEL_REVIEWED_ACTIVITY", "18");
define("LABEL_REVIEWED_ACTIVITY_NAME", "Reviewed");
define("LABEL_REVIEWED_ACTIVITY_TEXT", "is Reviewed your profile.");
define("LABEL_DECLINED_ACTIVITY_NAME","declined");
define("LABEL_VIEWED_PROFILE_TEXT", "Your profile was viewed by a ");
define("LABEL_FAVOURITED_PROFILE_ACTIVITY", "20");
define("LABEL_FAVOURITED_PROFILE_TEXT", "Your profile has been favourited");
define("LABEL_UNFAVOURITED_PROFILE_TEXT", "Your profile has been un favourited");
define("LABEL_FEEDBACK_ACTIVITY","3");
define("LABEL_FEEDBACK_ACTIVITY_TEXT","You have been reviewed by an employee at ");
define("LABEL_JOB_CANDIDATE_ACTIVITY","5");
define("LABEL_JOB_CANDIDATE_ACTIVITY_TEXT","You have been selected as a candidate for");
define("LABEL_STUDIO_CREATED_JOB","created job");

define("LABEL_COMMENT_JOB_TEXT_LOG", "Feedback added by");
define("LABEL_HIDE_JOB_TEXT_LOG", "moved to hidden");
define("LABEL_VIEW_PROFILE_TEXT_LOG", "Profile viewed by");
define("LABEL_NEW_JOB_TEXT_LOG", "added new job");
define("LABEL_USER_NEW_JOB_LOG_TEXT", "You applied for ");
define("LABEL_COMMENT_PROFILE_TEXT_LOG", "commented on profile");
define("LABEL_SHORTLIST_JOB_TEXT_LOG", "You have been shortlisted for");
define("LABEL_CANDIDATE_CONFIRM_TEXT", "You are a candidate for");
define("LABEL_EDIT_JOB_TEXT_LOG", "update job");
define("LABEL_DELETE_JOB_TEXT_LOG", "delete job");
define("LABEL_INVITE_TYROE_JOB_LOG", "invitetyroe");
define("LABEL_ADD_CANDIDATE_TEXT_LOG", "has added you as a candidate");
define("LABEL_INVITE_TYRO_TEXT_LOG", "has invite you to join tyroe");
define("LABEL_INVITE_TYRO_ACCEPT_TEXT_LOG", "has accept your invitation to signup");
define("LABEL_PROFILE_VIEW_TOOLTIP_TEXT", "upgrade your subscription to pro to see who and what company");
define("LABEL_ALL_STREAM_TOOLTIP_TEXT", "upgrade to a pro account to get the most out of your feed!");
define("LABEL_RECOMMENDATION_RECEIVED", "Recommendation received from");

#STUDIO ACTIVITY TEXT
define("LABEL_TYROE_JOB_APPLIED_TEXT", " has applied for ");



#TOOLTIP PROGRESS BAR LABELS
define("LABEL_PORTFOLIO_BAR_TOOLTIP", "Complete Your Portfolio");
define("LABEL_RESUME_BAR_TOOLTIP", "Complete Your Resume");
define("LABEL_PROFILE_BAR_TOOLTIP", "Complete Your Profile");
define("LABEL_NOTIFICATION_BAR_TOOLTIP", "Complete Your Notification");
define("LABEL_SOCIAL_BAR_TOOLTIP", "Complete Your Social media");
define("LABEL_SUBSCRIPTION_BAR_TOOLTIP", "Subscription");

define("LABEL_INVITATION_REQUEST", "Tyroe Invitation for signup.");

#SUBSCRIPTION
define("LABEL_NEW_SUBSCRIPTION", "New Subscription");
define("LABEL_EDIT_SUBSCRIPTION", "Edit Subscription");
define("LABEL_TYPE", "Type");
define("LABEL_USE_COUPONS", "Use Coupons?");
define("LABEL_PAYMENT_METHOD", "Payment Method");
define("LABEL_JOB_LEVEL","Job_Level");

define("LABEL_MANAGE_TYROES", "Manage Tyroes");
define("LABEL_MANAGE_STUDIO_REVIEWER", "Team members");
define("LABEL_FAVOURITE_TYROES", "Favourite tyroes");
define("LABEL_USER_MANAGER","User Manager");
define("LABEL_MANAGE_STUDIOS", "Studio Manager");
define("MESSAGE_ADD_STUDIO_SUCCESS", "Studio Added");

#SYSTEM VARIABLES
define("SYSTEM_VARIABLE_PORTFOLIO_VIDEO_QTY", "Video Quantity (Portfolio)");
define("SYSTEM_VARIABLE_PORTFOLIO_IMAGE_QTY", "Image Quantity (Portfolio)");
define("SYSTEM_VARIABLE_COUPONS_QTY", "Coupons Quantity");
define("SYSTEM_VARIABLE_AUTO_COUPON_VAL", "Coupons");
define("SYSTEM_VARIABLE_AUTO_COUPON_STATUS", "Coupons Status");
define("SYSTEM_VARIABLE_CURRENCY", "Currency");
define("SYSTEM_VARIABLE_TIMEZONE", "Time Zone");
define("SYSTEM_VARIABLE_META_TAG_KEYWORD", "Meta Keywords");
define("SYSTEM_VARIABLE_META_TAG_DESCRIPTION", "Meta Description");
define("SYSTEM_VARIABLE_META_TAG_AUTHOR", "Meta Author");
define("SYSTEM_VARIABLE_PERDAY_HIRE", "Hire button access limit per day");

define("LABEL_MANAGE_SYSTEM_VARIABLE", "System Variable");

define("LABEL_ADD_COUPON", "Add Coupon");
define("LABEL_EDIT_COUPON", "Edit Coupon");


#Notification Templates
define("LABEL_EDIT_NOTIFICATION_TEMPLATE", "Edit Notification Template");
/*define("LABEL_EDIT_NOTIFICATION_TEMPLATE", "Edit Notification Template");*/
define("LABEL_NOTIFICATION_TITLE", "Title");
define("LABEL_NOTIFICATION_DESCRIPTION", "Description");
define("LABEL_NOTIFICATION_EMAILTEMPLATE", "Email Template");
define("LABEL_NOTIFICATION_SOCIALTEMPLATE", "Social Template");
define("LABEL_MANAGE_SYSTEM_CONFIGRUARTION", "System Configuration");

define("LABEL_DETAILS","Details");

#EMAIL TEMPLATES
//define("LABEL_TO_NAME",$this->session->userdata("firstname"));
#EMAIL NOTIFICATION
define("LABEL_NOTIFICATION_TEXT_JOB","Job Opening Status");
define("LABEL_NOTIFICATION_SUB_TEXT_JOB","Notifications about your status as a candidate for jobs.");
define("LABEL_NOTIFICATION_TEXT_FEEDBACK","Feedback & Ratings");
define("LABEL_NOTIFICATION_SUB_TEXT_FEEDBACK","Receive emails when feedback and ratings are added.");
define("LABEL_NOTIFICATION_TEXT_PROFILE_VIEW","Profile Views");
define("LABEL_EMAIL_NOTIFY_SUBJECT_PROFILE_VIEW","Profile Viewed");
define("LABEL_NOTIFICATION_SUB_TEXT_PROFILE_VIEW","Notifications when a comapny views your profile.");
define("LABEL_NOTIFICATION_TEXT_STAFF_NEWSLETTER","Staff Pick Newsletter");
define("LABEL_NOTIFICATION_SUB_TEXT_STAFF_NEWSLETTER","The best Tyroe artist curated in a stunning email.");
define("LABEL_NOTIFICATION_TEXT_TYROE_NEWSLETTER","Tyroe Newsletter");
define("LABEL_NOTIFICATION_SUB_TEXT_TYROE_NEWSLETTER","Notifications about new features, updates and special offers.");

define("LABEL_PROFILE_RESUME_AWARD","+ awards name");
define("LABEL_PROFILE_RESUME_AWARD_YEAR","+ Year");
define("LABEL_PROFILE_RESUME_AWARD_ORGANIZATION","+ organisation");
define("LABEL_PROFILE_RESUME_AWARD_DESCRIPTION","+ Please type a brief description about your Award.");

define("LABEL_PROFILE_RESUME_EDUCATION_DEGREE","+ course name");
define("LABEL_PROFILE_RESUME_EDUCATION_SCHOOL","+ school name");
define("LABEL_PROFILE_RESUME_EDUCATION_STARTDATE","start date");
define("LABEL_PROFILE_RESUME_EDUCATION_ENDDATE","End date");
define("LABEL_PROFILE_RESUME_EDUCATION_DESCRIPTION","+ Please type a brief description of the course.");


define("LABEL_PROFILE_RESUME_EXPERIENCE_JOBTITLE","+ job title");
define("LABEL_PROFILE_RESUME_EXPERIENCE_COMPANY","+ company");
define("LABEL_PROFILE_RESUME_EXPERIENCE_STARTDATE","Start date");
define("LABEL_PROFILE_RESUME_EXPERIENCE_ENDDATE","End date");
define("LABEL_PROFILE_RESUME_EXPERIENCE_DESCRIPTION","+ Please type a brief description about this job.");

define("LABEL_RECOMMENDATION_TEXTFIELD_PLACEHOLDER","Email Address");
define("LABEL_RECOMMENDATION_TEXTFIELD_ERROR_PLACEHOLDER","Please Provide Email Format Eg: test@test.com");

#Modal Text
define("LABEL_RECOMMENDATION_SENT_HEADER","Nice one! Your request has been sent.");
define("LABEL_RECOMMENDATION_SENT_CONTENT","You will be notified once [EMAIL] has replied, you can then approve their glowing recommendation and display it on your profile.");
define("LABEL_REMOVE_SELECTED","Are you sure, you want to remove it?");


#Notification Messages
define("UNPUBLISH_PROFILE_MESSAGE","Woh! Your profile is currently unpublished. Just say'n!");
define("UNPUBLISH_PROFILE_MESSAGE_FOR_ADMIN","Woh! This profile is currently unpublished. Just say'n!");
define("RECOMMNENDATION_RECIEVED_MESSAGE","Woohoo, you have a new notification! Approve it here!");
define("LATEST_JOB_MESSAGE","You have 0 job openings that require your attention.");
define("LATEST_SHORTLIST_JOB_MESSAGE","You have been shortlisted for 0 jobs.");
define("RECOMMENDATION_EMAIL_EXIST","Sorry! This Email already exist.");
