<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_shortlist_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_comment_job_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);

class Openingoverview extends Tyroe_Controller {

    private $job_roles_mapping_obj;

    public function Openingoverview() {
        parent::__construct();
        $user_id = $this->session->userdata("user_id");
        $this->data['page_title'] = 'Opening-Overview';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_OPENINGOVERVIEW_MODULE . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
        #GEt jobid from url
        $job_id = $this->uri->segment(3);
        if (intval($job_id) == 0) {
            $job_name = $this->uri->segment(2);
            $job_id = $this->get_number_from_name($job_name);
        }

        $this->job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
        $this->job_roles_mapping_obj->job_id = intval($job_id);
        $this->job_roles_mapping_obj->user_id = $user_id;
        $this->job_roles_mapping_obj->get_job_role_mapping();
        $this->data['job_role'] = (array) $this->job_roles_mapping_obj;

        $joins = new Tyr_joins_entity();
        $get_jobs = $joins->get_job_details_by_job_id($job_id);

        $jobs_obj = new tyr_jobs_entity();
        $jobs_obj->job_id = $job_id;
        $jobs_obj->get_jobs();
        $this->data['job_detail'] = (array) $jobs_obj;
//        $get_jobs = $this->getRow("SELECT job_id,user_id,archived_status,category_id,job_title,job_description,job_city,team_moderation," . TABLE_CITIES . ".city,".TABLE_JOB_TYPE.".job_type_id,".TABLE_JOB_TYPE.".job_type,".TABLE_COUNTRIES.".country
//                AS job_location,".TABLE_COUNTRIES.".country_id,".TABLE_JOB_LEVEL.".job_level_id,".TABLE_JOB_LEVEL.".level_title AS job_skill,start_date,end_date,".TABLE_INDUSTRY.".industry_name,".TABLE_INDUSTRY.".industry_id,job_quantity," . TABLE_JOBS . ".created_timestamp,"
//                . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl FROM " . TABLE_JOBS . "
//                LEFT JOIN " . TABLE_COUNTRIES . " ON "
//                . TABLE_JOBS . ".country_id = " . TABLE_COUNTRIES . ".country_id
//                LEFT JOIN " . TABLE_CITIES . " ON "
//                                . TABLE_JOBS . ".job_city = " . TABLE_CITIES . ".city_id
//                LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id = "
//                . TABLE_JOB_LEVEL . ".job_level_id
//                LEFT JOIN ".TABLE_JOB_TYPE." ON " . TABLE_JOBS . ".job_type=".TABLE_JOB_TYPE.".job_type_id
//                LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id= ".TABLE_INDUSTRY.".industry_id WHERE job_id = '"
//                . $job_id . "' AND " . TABLE_JOBS . ".status_sl = '1' ");

        $this->data['datedifference'] = $this->dateDifference($get_jobs['start_date'], $get_jobs['end_date']);

        $job_skills_obj = new Tyr_job_skills_entity();
        $job_skills_obj->job_id = $job_id;
        $getskills = $job_skills_obj->get_all_creative_skill_id_by_job();
        //$getskills=$this->getAll("SELECT creative_skill_id FROM ".TABLE_JOB_SKILLS." where job_id='".$job_id."'");

        $job_skills = array();
        foreach ($getskills as $skill) {
            //$job_skills[]=$this->getRow("SELECT creative_skill FROM ".TABLE_CREATIVE_SKILLS." where creative_skill_id='".$skill['creative_skill_id']."'");
            $creative_skills_obj = new Tyr_creative_skills_entity();
            $creative_skills_obj->creative_skill_id = $skill['creative_skill_id'];
            $creative_skills_obj->get_creative_skills();
            $job_skills[] = array('creative_skill' => $creative_skills_obj->creative_skill);
        }

        $this->get_company_name();

        $this->data['role_id'] = $this->session->userdata("role_id");
        $this->data['job_id'] = $job_id;
        $this->data['job_skills'] = $job_skills;
        $this->data['get_jobs'] = $get_jobs;

        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->studio_id = $user_id;
        $hidden_obj->job_id = $job_id;
        $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

        //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['user_id'];
        }
        //total collaborators
        $users_obj = new Tyr_users_entity();
        $users_obj->parent_id = $user_id;
        $users_obj->status_sl = 1;
        //$this->data['collaborators'] = $users_obj->get_count_of_collaborators();
        //$this->data['collaborators'] = $this->getRow("SELECT COUNT(user_id) AS collaborators FROM ".TABLE_USERS." WHERE parent_id = '".$user_id."' AND status_sl = '1'");
        //$this->data['applicant_count'] = $this->getRow("SELECT COUNT(job_status_id) AS total_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 2 AND invitation_status = 0 AND   tyroe_id NOT IN (" . $hidden_user . ")");
        //$this->data['candidate_count'] = $this->getRow("SELECT COUNT(job_status_id) AS total_candidates FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 1 AND invitation_status != -1 AND  tyroe_id NOT IN (" . $hidden_user . ")");
        //$this->data['shortlist_count'] = $this->getRow("SELECT COUNT(job_status_id) AS total_shortlist FROM  " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 3 AND invitation_status = 1 AND    tyroe_id NOT IN (" . $hidden_user . ")");
        //$this->data['reviewer_count'] = $this->getRow("SELECT   COUNT(*) AS total_reviewer FROM " . TABLE_REVIEWER_JOBS . " WHERE job_id='" . $job_id . "'");
        //$this->data['hidden_count'] = $this->getRow("SELECT  COUNT(*) AS total_hidden FROM " . TABLE_HIDDEN . " WHERE job_id='" . $job_id . "'");

        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = 1;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = -1;
        $this->data['candidate_count'] = $job_details_obj->get_candidate_count_except_hidden($hidden_user);


        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = 2;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = 0;
        $this->data['applicant_count'] = $job_details_obj->get_applicant_count_except_hidden($hidden_user);


        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = 3;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = 1;
        $this->data['shortlist_count'] = $job_details_obj->get_shortlist_count_except_hidden($hidden_user);


        $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
        $reviewer_jobs_obj->job_id = $job_id;
        $reviewer_jobs_obj->status_sl = 1;
        $this->data['reviewer_count'] = $reviewer_jobs_obj->get_total_reviewers_count_for_job();

        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->job_id = $job_id;
        $this->data['hidden_count'] = $hidden_obj->get_total_hidden_count_by_job();

        $this->data['total_candidates_to_be_reviewed'] = intval($this->data['candidate_count']['total_candidates']) + intval($this->data['shortlist_count']['total_shortlist']);
        //Count number of total newest candidate , applicaint, and shortlist
//        $this->data['newest_candidate_count'] = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_candidates FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 1 AND invitation_status != -1 AND   tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");
//        $this->data['newest_applicant_count'] = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 2 AND invitation_status = 0 AND   tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");
//        $this->data['newest_shortlist_count'] = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_shortlist FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND job_status_id = 3 AND invitation_status = 1 AND   tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");
        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = -1;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = 1;
        $job_details_obj->newest_candidate_status = 0;
        $this->data['newest_candidate_count'] = $job_details_obj->get_new_candidate_count_except_hidden($hidden_user);


        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = 2;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = 0;
        $job_details_obj->newest_candidate_status = 0;
        $this->data['newest_applicant_count'] = $job_details_obj->get_new_applicant_count_except_hidden($hidden_user);


        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->job_status_id = 3;
        $job_details_obj->status_sl = 1;
        $job_details_obj->invitation_status = 1;
        $job_details_obj->newest_candidate_status = 0;
        $this->data['newest_shortlist_count'] = $job_details_obj->get_new_shortlist_count_except_hidden($hidden_user);
        //End count number of total newest candidate , applicaint, and shortlist
        //get team members admin
        $company_details_obj = new Tyr_company_detail_entity();
        $company_details_obj->studio_id = $user_id;
        $company_details_obj->get_company_detail_by_studio();
        $get_team_admin_company = array('company_name' => $company_details_obj->company_name);

        // $get_team_admin_company = $this->getRow("SELECT company_name FROM ".TABLE_COMPANY." WHERE studio_id = '".$user_id."'");
//        $get_team_admin = $this->getRow('SELECT
//        '. TABLE_USERS .'.user_id,
//        '. TABLE_USERS .'.firstname,
//        '. TABLE_USERS .'.lastname,
//        '. TABLE_USERS .'.user_occupation,
//        '. TABLE_USERS .'.email,
//        '. TABLE_MEDIA .'.media_type,
//        '. TABLE_MEDIA .'.media_name
//        FROM '. TABLE_USERS . '
//        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
//        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
//        WHERE '. TABLE_USERS . '.user_id = '.$user_id.' AND
//        '. TABLE_USERS . '.status_sl = "1"');
        $joins_obj = new Tyr_joins_entity();
        $get_team_admin = $joins_obj->get_team_admin($user_id);
        $get_team_admin['company_name'] = $get_team_admin_company['company_name'];
        $this->data['team_admin'] = $get_team_admin;
        //end get team admin
        //get job reviewers for team lightbox ...
//        $get_reviewers = $this->getAll('SELECT
//        '. TABLE_USERS .'.user_id,
//        '. TABLE_USERS .'.firstname,
//        '. TABLE_USERS .'.lastname,
//        '. TABLE_USERS .'.job_title,
//        '. TABLE_USERS .'.email,
//        '. TABLE_MEDIA .'.media_type,
//        '. TABLE_MEDIA .'.media_name,
//        '. TABLE_REVIEWER_JOBS .'.rev_id,
//        '. TABLE_REVIEWER_JOBS .'.job_id
//        FROM '. TABLE_USERS . '
//        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
//        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
//        RIGHT JOIN '. TABLE_REVIEWER_JOBS .' ON ' . TABLE_USERS . '.user_id = '. TABLE_REVIEWER_JOBS .'.reviewer_id
//        AND '. TABLE_REVIEWER_JOBS .'.job_id = "'.$job_id.'" AND '. TABLE_REVIEWER_JOBS .'.studio_id = "'.$user_id.'"
//        WHERE '. TABLE_USERS . '.role_id = "4" AND
//        '. TABLE_USERS . '.status_sl = "1"');
        $parent_id = $user_id;
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        if ($users_obj->parent_id != 0) {
            $parent_id = $users_obj->parent_id;
        }

        //$joins_obj = new Tyr_joins_entity();
        //$get_reviewers = $joins_obj->get_all_reviewers_for_studio_job($job_id, $user_id, $parent_id);

        $joins_obj = new Tyr_joins_entity();
        $get_reviewers = $joins_obj->get_all_job_team_member($job_id, $this->session->userdata("user_id"));

//        print_r($get_reviewers);
//        die();
//        if($get_reviewers != ''){
//            foreach($get_reviewers as $index => $val){
//                $reviewer_id = $val['user_id'];
//                $job_role_mapping = new Tyr_job_role_mapping_entity();
//                $job_role_mapping->job_id = $job_id;
//                $job_role_mapping->user_id = $reviewer_id;
//                $job_role_mapping->get_job_role_mapping();
//                if($job_role_mapping->id > 0){
//                    $get_reviewers[$index]['is_admin'] = $job_role_mapping->is_admin;
//                    $get_reviewers[$index]['is_reviewer'] = $job_role_mapping->is_reviewer;
//                    $get_reviewers[$index]['job_role_id'] = $job_role_mapping->id;
//                }else{
//                    $get_reviewers[$index]['is_admin'] = 0;
//                    $get_reviewers[$index]['is_reviewer'] = 0;
//                    $get_reviewers[$index]['job_role_id'] = 0;
//                }
//
//            }
//        }

        $this->data['get_reviewers'] = $get_reviewers;




        //end getting job reviewers for team lightbox...
        //get anonymous  feedback
//        $this->data['get_anonymous'] = $this->getAll("SELECT
//                              ".TABLE_RATE_REVIEW.".review_id,
//                              ".TABLE_RATE_REVIEW.".user_id,
//                              ".TABLE_RATE_REVIEW.".job_id,
//                              ".TABLE_USERS.".firstname,
//                              ".TABLE_USERS.".lastname,
//                              ".TABLE_USERS.".user_occupation,
//                              ".TABLE_RATE_REVIEW.".anonymous_feedback_message,
//                              ".TABLE_RATE_REVIEW.".anonymous_feedback_status,
//                              ".TABLE_MEDIA.".media_type,
//                              ".TABLE_MEDIA.".media_name
//                            FROM
//                              ".TABLE_RATE_REVIEW."
//                              LEFT JOIN tyr_users
//                                ON ".TABLE_RATE_REVIEW.".user_id = ".TABLE_USERS.".user_id
//                              LEFT JOIN tyr_media
//                                ON ".TABLE_USERS.".user_id = ".TABLE_MEDIA.".user_id
//                                AND ".TABLE_MEDIA.".profile_image = '1'
//                                AND ".TABLE_MEDIA.".status_sl = '1'
//                            WHERE ".TABLE_RATE_REVIEW.".job_id = '".$job_id."' AND ".TABLE_RATE_REVIEW.".status_sl = '1' AND ".TABLE_RATE_REVIEW.".anonymous_feedback_status = 0");
        $joins_obj = new Tyr_joins_entity();
        $this->data['get_anonymous'] = $joins_obj->get_all_anonymous_feedback_by_job($job_id);
        //End anonymous feedback




        if ($this->session->userdata("role_id") == '2') {
            //$where_activity = "object_id='" . $user_id . "'";
            $where_activity = "performer_id='" . $user_id . "'";
        } else {
            $where_activity = "performer_id='" . $user_id . "'";
        }
        $where_activity .=" AND j_act.job_id ='" . $job_id . "' ";
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY job_activity_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();

        $this->data['job_activity'] = $this->get_activity_stream($where_activity, "ORDER BY job_activity_id DESC ", "LIMIT 10 OFFSET 0");

        //get job activity type
        //$activity_type = $this->getAll("SELECT * FROM " . TABLE_ACTIVITY_TYPE . " WHERE status_sl = 1");
//        $activity_type = $this->getAll("SELECT * FROM " . TABLE_ACTIVITY_TYPE . " WHERE status_sl = '1' AND show_in_menu = '1' ORDER BY sort_order ASC");
//        $this->data['get_activity_type'] = $activity_type;

        $activity_type_obj = new Tyr_activity_type_entity();
        $activity_type_obj->status_sl = 1;
        $activity_type_obj->show_in_menu = 1;
        $activity_type = $activity_type_obj->get_all_activity_type_by_menu();
        $this->data['get_activity_type'] = $activity_type;
    }

    public function index() {

    }

    public function JobOverview($job_id, $jumptoaction = '') {
        $public_job_url = LIVE_SITE_URL . 'jobs/' . $job_id;
        $this->data['public_job_url'] = $public_job_url;
        if ($this->input->post('action') != "") {
            $this->data['jumptoaction'] = $this->input->post('action');
        } elseif ($this->input->get('job') == md5('shortlist')) {
            $this->data['jumptoaction'] = 'shortlist';
        } else if ($jumptoaction != '') {
            $this->data['jumptoaction'] = $jumptoaction;
        }
        
        $user_id = $this->session->userdata("user_id");
        if (!is_numeric($job_id)) {
            $job_id = $this->get_number_from_name($job_id);
        }

        if ($this->session->flashdata('job_payment_done') == 1) {
            $this->data['job_payment_done'] = true;
        }

        if (intval($job_id) > 0) {

            /* Meta Data */

            $jobs_obj = new tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->get_jobs();
            $this->data['job_detail'] = (array) $jobs_obj;

            $users_details_obj = new Tyr_users_entity();
            $users_details_obj->user_id = $jobs_obj->user_id;
            $users_details_obj->get_user();
            $parent_id = $users_details_obj->parent_id;
            if ($parent_id == 0) {
                $parent_id = $users_details_obj->user_id;
            }

            $company_detail_studio_obj = new Tyr_company_detail_entity();
            $company_detail_studio_obj->studio_id = $parent_id;
            $company_detail_studio_obj->get_company_detail_by_studio();
            if ($company_detail_studio_obj->parent_id != 0) {
                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->company_id = $company_detail_studio_obj->parent_id;
                $company_detail_obj->get_company_detail();
                $this->data['job_company_detail'] = (array) $company_detail_obj;
            } else {
                $this->data['job_company_detail'] = (array) $company_detail_studio_obj;
            }

            $country_obj = new Tyr_countries_entity();
            $country_obj->country_id = $jobs_obj->country_id;
            $country_obj->get_countries();

            $city_obj = new Tyr_cities_entity();
            $city_obj->city_id = $jobs_obj->job_city;
            $city_obj->get_cities();

            $joins_obj = new Tyr_joins_entity();
            $this->data['job_skills'] = $joins_obj->get_all_jobs_skill_by_job_id($job_id);

            $this->data['job_site_url'] = LIVE_SITE_URL;

            $this->data['job_location'] = $city_obj->city . ', ' . trim($country_obj->country);

            //getting donute chart pending and reviewed
            if (intval($this->job_roles_mapping_obj->is_admin) == 1) {
                $parent_id = $this->session->userdata('parent_id');
                if (intval($parent_id) == 0) {
                    $parent_id = $user_id;
                }

                $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs_obj->studio_id = $parent_id;
                $reviewer_jobs_obj->job_id = $job_id;
                $total_reviewer = $reviewer_jobs_obj->get_reviewers_count_for_studio_job();

                if ($total_reviewer['reviewer_ids'] == '') {
                    $total_reviewer['reviewer_ids'] = '-1';
                }

                $total_reviews_needed = intval($total_reviewer['total_reviewers']) * intval($this->data['total_candidates_to_be_reviewed']);

                $total_reviews_obj = new Tyr_reviewer_reviews_entity();
                $total_reviews_obj->job_id = $job_id;
                $total_reviews_obj->studio_id = $parent_id;
                $total_reviews_obj->status_sl = 1;
                $total_reviews_arr = $total_reviews_obj->get_all_reviews_count_by_job_id($total_reviewer['reviewer_ids']);

                $total_reviews_done = intval($total_reviews_arr['review_count']);
                $reviews_pending_percentage = 0;
                if ($total_reviews_done != 0) {
                    $reviews_pending_percentage = $total_reviews_needed - $total_reviews_done;
                    if ($reviews_pending_percentage < 0) {
                        $reviews_pending_percentage = 100;
                    } else {
                        $reviews_pending_percentage = round(100 - (($reviews_pending_percentage / $total_reviews_needed) * 100));
                    }
                }
                $total_review_candidate_percent = $reviews_pending_percentage;

                $this->data['get_total_reviewed'] = $reviews_pending_percentage;
                $this->data['get_total_pending'] = 100 - $reviews_pending_percentage;
            } else if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
                $parent_id = $this->session->userdata('parent_id');
                if (intval($parent_id) == 0) {
                    $parent_id = $user_id;
                }
                //$where_reveiw = " AND studio_id = '".$parent_id."' AND reviewer_id = '".$user_id."'";
                $total_reviews_needed = intval($this->data['total_candidates_to_be_reviewed']);
                $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                $reviewer_reviews_obj->studio_id = $parent_id;
                $reviewer_reviews_obj->reviewer_id = $this->session->userdata('user_id');
                $reviewer_reviews_obj->job_id = $job_id;
                ;
                $reviewer_reviews_obj->status_sl = 1;
                $total_reviews = $reviewer_reviews_obj->get_reviews_id_count_for_job();


                //total REviewed candidate
                $total_review_candidate = intval($total_reviews['total_reviews']);

                $reviewed_percentage = round((($total_reviews_needed - $total_review_candidate) / $total_reviews_needed) * 100);
                $total_review_candidate_percent = 100 - $reviewed_percentage;
                $this->data['get_total_reviewed'] = $total_review_candidate_percent;
                $this->data['get_total_pending'] = $reviewed_percentage;
            }
            //End getting donute chart pending and reviewed
            $this->template_arr = array('header', 'contents/opening_overview', 'footer');
            $this->load_template();
        } else {
            header('location:' . $this->getURL("openings"));
        }
    }

    public function ReviewTeam($job_id) {
        if ($job_id != "") {
            $user_id = $this->session->userdata("user_id");
//            $get_user = $this->getAll("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                                   username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                                   state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                                    " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                                 LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                                 LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                                 WHERE " . TABLE_USERS . ".parent_id='" . $user_id . "'and " . TABLE_USERS . ".status_sl='1'");
            $joins_obj = new Tyr_joins_entity();
            $get_user = $joins_obj->get_all_reviewers_by_job_id($job_id);

            $this->data['get_reviewer'] = $get_user;
            $this->data['job_id'] = $job_id;
            $this->template_arr = array('header', 'contents/review_team', 'footer');
            $this->load_template();
        } else {
            //header('location:' . $this->getURL("jobopening"));
        }
    }

    public function applicants($job_id) {
        //filltering query for applicaint fillters
        if ($this->input->post('decline_users') != '' && $this->input->post('fillter_drop') == '') {
            $job_detail_status = -1;
            $this->data['opacity_decline_class'] = 'dull_decline_tyroes';
        } else {
            $job_detail_status = 1;
            $this->data['opacity_decline_class'] = '';
        }
        if ($this->input->post('fillter_drop') == 'most_recent') {
            $order_by = ' ORDER BY ' . TABLE_JOB_DETAIL . '.job_detail_id DESC ';
        } else if ($this->input->post('fillter_drop') == 'aphabetical') {
            $order_by = ' ORDER BY ' . TABLE_USERS . '.firstname ASC ';
        } else if ($this->input->post('fillter_drop') == 'highiest_score') {
            $selection = 'AVG(
                                (
                                  tyr_rate_review.technical + tyr_rate_review.impression + tyr_rate_review.creative
                                ) / 3
                              ) AS tyroe_score,';
            $selection = '((AVG(' . TABLE_RATE_REVIEW . '.technical))+(AVG(' . TABLE_RATE_REVIEW . '.creative))+(AVG(' . TABLE_RATE_REVIEW . '.impression)))/3 AS tyroe_score,';
            $rate_and_review_join = "LEFT JOIN " . TABLE_RATE_REVIEW . " ON " . TABLE_USERS . ".user_id = " . TABLE_RATE_REVIEW . ".tyroe_id" . $check_by_review;
            $order_by = " ORDER BY tyroe_score DESC ";
            $group_by = " GROUP BY " . TABLE_USERS . ".user_id";
        }

        $data['job_id'] = $job_id;
        if ($job_id != "") {
//            $user_id = $this->session->userdata("parent_id");
//            if(intval($user_id) != 0){
//                $user_id = $this->session->userdata("user_id");
//            }
            $user_id = $this->session->userdata("user_id");
            $parent_id = intval($this->session->userdata("parent_id"));
            if ($parent_id == 0) {
                $parent_id = intval($user_id);
            }

            //$this->update(TABLE_JOB_DETAIL,array('newest_candidate_status' => '1'),"job_id = '".$job_id."' AND job_status_id = 2 AND reviewer_id = '".$user_id."'");
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 2;
            $job_detail_obj->reviewer_id = $user_id;
            $job_detail_obj->newest_candidate_status = 1;
            $job_detail_obj->update_newest_candidate_status();

            //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
            $hidden_obj = new Tyr_hidden_entity();
            $hidden_obj->studio_id = $user_id;
            $hidden_obj->job_id = $job_id;
            $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

            $hidden_user = 0;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            }


//            $get_applicants = $this->getAll("SELECT  ".$selection." ".TABLE_USERS.".user_id,firstname,extra_title,lastname,availability," . TABLE_MEDIA . ".media_type,
//                        " . TABLE_MEDIA . ".media_name,".TABLE_COUNTRIES.".country," . TABLE_INDUSTRY . ".industry_name,
//                        " . TABLE_CITIES . ".city,".TABLE_JOB_DETAIL.".job_detail_id from ".TABLE_USERS."
//                        LEFT JOIN ".TABLE_COUNTRIES." ON ".TABLE_COUNTRIES.".country_id=".TABLE_USERS.".country_id
//                        LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
//                LEFT JOIN " . TABLE_MEDIA . "
//                ON " . TABLE_MEDIA . ".user_id = ". TABLE_USERS .".user_id
//                AND " . TABLE_MEDIA . ".profile_image = '1'
//                AND " . TABLE_MEDIA . ".status_sl = '1'
//                LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
//            LEFT JOIN ".TABLE_JOB_DETAIL." ON ".TABLE_USERS.".user_id=".TABLE_JOB_DETAIL.
//            ".tyroe_id AND ".TABLE_JOB_DETAIL.".job_status_id = 2
//            ".$rate_and_review_join."
//            WHERE ".TABLE_JOB_DETAIL.".job_id='".$job_id."'  AND ".TABLE_USERS.".publish_account = '1' AND ".TABLE_JOB_DETAIL.".invitation_status = 0  AND ".TABLE_JOB_DETAIL.".status_sl=".$job_detail_status." AND ".TABLE_JOB_DETAIL.".reviewer_id = '".$user_id."' AND ".TABLE_JOB_DETAIL.".tyroe_id NOT IN (" . $hidden_user . ") ".$group_by." ".$order_by." ");

            $joins_obj = new Tyr_joins_entity();
            $get_applicants = $joins_obj->get_all_applicant_for_job($selection, $rate_and_review_join, $job_id, $job_detail_status, $parent_id, $hidden_user, $group_by, $order_by);

            if ($get_applicants != NULL) {
                for ($i = 0; $i < count($get_applicants); $i++) {

//                    $get_applicants[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                                     FROM " . TABLE_SKILLS . " LEFT JOIN
//                                     " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                                     WHERE user_id='" . $get_applicants[$i]['user_id'] . "' and status_sl=1");
//                        #Portfolio/Media
//                    $get_applicants[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_applicants[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
//                        /*FEATURED TYROE - TRIANGLE*/
//                        $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '".$get_applicants[$i]['user_id']."'");
//                    $get_applicants[$i]['is_user_featured'] = $is_user_featured['featured_status'];
//                    /*FEATURED TYROE - TRIANGLE*/
//                    /**Getting tyroe jobs**/
//                    $tyroe_jobs = $this->getAll("SELECt job_id FROM ".TABLE_JOB_DETAIL." WHERE tyroe_id = '".$get_applicants[$i]['user_id']."'");
//                    $get_applicants[$i]['tyroe_jobs'] = $tyroe_jobs;
//                    /**End getting tyroe jobs**/
//                    /**get tyroe total score**/
//                    $get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_applicants[$i]['user_id']."'");
//                    if($get_tyroes_total_score['tyroe_score'] == NULL){
//                        $get_tyroes_total_score['tyroe_score'] = 0;
//                    }
//                    $get_applicants[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
//                    /**end get tyroe total score**/

                    $joins_obj = new Tyr_joins_entity();
                    $get_applicants[$i]['skills'] = $joins_obj->get_all_user_skill($get_applicants[$i]['user_id']);

                    #Portfolio/Media
                    $media_obj = new Tyr_media_entity();
                    $media_obj->user_id = $get_applicants[$i]['user_id'];
                    $get_applicants[$i]['portfolio'] = $media_obj->get_user_short_portfolio();

                    /* FEATURED TYROE - TRIANGLE */
                    $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                    $featured_tyroes_obj->featured_tyroe_id = $get_applicants[$i]['user_id'];
                    $featured_tyroes_obj->get_user_featured_status();
                    $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                    $get_applicants[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    /* FEATURED TYROE - TRIANGLE */

                    /*                     * Getting tyroe jobs* */
                    $job_details_obj = new Tyr_job_detail_entity();
                    $job_details_obj->tyroe_id = $get_applicants[$i]['user_id'];
                    $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                    $get_applicants[$i]['tyroe_jobs'] = $tyroe_jobs;
                    /*                     * End getting tyroe jobs* */

                    /*                     * get tyroe total score* */
                    $rate_review_obj = new Tyr_rate_review_entity();
                    $rate_review_obj->tyroe_id = $get_applicants[$i]['user_id'];
                    $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();

                    if ($get_tyroes_total_score['tyroe_score'] == NULL) {
                        $get_tyroes_total_score['tyroe_score'] = 0;
                    }
                    $get_applicants[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    /*                     * end get tyroe total score* */
                }
            }
            $this->data['get_applicants'] = $get_applicants;
            $this->template_arr = array('contents/job_applicants');
            $this->load_template();
        }
    }

    public function Candidates($job_id) {
        $user_id = $this->session->userdata("user_id");
        $parent_id = intval($this->session->userdata("parent_id"));
        if ($parent_id == 0) {
            $parent_id = intval($user_id);
        }


        $this->job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
        $this->job_roles_mapping_obj->job_id = intval($job_id);
        $this->job_roles_mapping_obj->user_id = $user_id;
        $this->job_roles_mapping_obj->get_job_role_mapping();
        $this->data['job_role'] = (array) $this->job_roles_mapping_obj;

        if (intval($this->job_roles_mapping_obj->is_admin) == 1) {
            $reviewer_where = '';
        }
        if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
            //geeting levels
            //$get_levels = $this->getAll("SELECT * FROM " . TABLE_JOB_LEVEL);
            $job_level_obj = new Tyr_job_level_entity();
            $get_levels = $job_level_obj->get_all_job_level();
            $this->data['all_levels'] = $get_levels;
            $reviewer_where = " AND " . TABLE_JOB_DETAIL . ".reviewer_id = '" . $user_id . "' ";
        }
        if ($this->input->post('fillter_action') == 'Decline') {
            $this->data['opacity_decline_class1'] = 'dull_decline_tyroes';
        } else {
            $this->data['opacity_decline_class1'] = '';
        }
        //Getting total tyroes
        //$this->data['total_tyroes'] = $this->getRow("SELECT COUNT(*) AS total_tyroe   FROM " .TABLE_USERS. " WHERE role_id = 2 AND status_sl = 1");
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 2;
        $users_obj->status_sl = 1;
        $this->data['total_tyroes'] = $users_obj->get_total_tyroe_count_by_role();

        $this->data['job_id'] = $job_id;
        if ($job_id != "") {
            $user_id = $this->session->userdata("user_id");
            if (intval($this->job_roles_mapping_obj->is_admin) == 1) {
                $reviewer_check = '';
            }
            if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
                $user_id = $parent_id;
                $reviewer_id = $this->session->userdata("user_id");
                $reviewer_check = " AND " . TABLE_REVIEWER_REVIEWS . ".reviewer_id = '" . $reviewer_id . "'";
            }

            //$this->update(TABLE_JOB_DETAIL,array('newest_candidate_status' => '1'),"job_id = '".$job_id."' AND job_status_id = 1 AND reviewer_id = '".$user_id."'");
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 1;
            $job_detail_obj->reviewer_id = $parent_id;
            $job_detail_obj->newest_candidate_status = 1;
            $job_detail_obj->update_newest_candidate_status();


            //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
            $hidden_obj = new Tyr_hidden_entity();
            $hidden_obj->studio_id = $parent_id;
            $hidden_obj->job_id = $job_id;
            $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

            $hidden_user = 0;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            }

            $group_by = '';
            if ($this->input->post('fillter_drop') == 'most_recent') {
                $order_by = ' ORDER BY ' . TABLE_JOB_DETAIL . '.job_detail_id DESC ';
            } else if ($this->input->post('fillter_drop') == 'aphabetical') {
                $order_by = ' ORDER BY ' . TABLE_USERS . '.firstname ASC ';
            } else if ($this->input->post('fillter_drop') == 'highiest_score') {
                $selection = '((AVG(' . TABLE_RATE_REVIEW . '.technical))+(AVG(' . TABLE_RATE_REVIEW . '.creative))+(AVG(' . TABLE_RATE_REVIEW . '.impression)))/3 AS tyroe_score,';
                if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
                    $check_by_review = " AND " . TABLE_RATE_REVIEW . ".user_id = " . $reviewer_id . " AND " . TABLE_RATE_REVIEW . ".status_sl = 1";
                    $group_by = 'group by tyr_users.user_id,tyr_media.media_type,tyr_media.media_name,tyr_industry.industry_name,tyr_countries.country,
tyr_cities.city,tyr_job_detail.job_detail_id,tyr_reviewer_reviews.reviewer_reviews_id';
                } else if (intval($this->job_roles_mapping_obj->is_admin) == 1) {
                    $check_by_review = " AND " . TABLE_RATE_REVIEW . ".job_id = " . $job_id . " AND " . TABLE_RATE_REVIEW . ".status_sl = 1";
                }
                $rate_and_review_join = "LEFT JOIN " . TABLE_RATE_REVIEW . " ON " . TABLE_USERS . ".user_id = " . TABLE_RATE_REVIEW . ".tyroe_id" . $check_by_review;
                $order_by = " ORDER BY tyroe_score DESC ";
                //$group_by = " GROUP BY " . TABLE_USERS . ".user_id";
            }
            if (intval($this->job_roles_mapping_obj->is_admin) == 1) {

                $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs_obj->studio_id = $parent_id;
                $reviewer_jobs_obj->job_id = $job_id;
                $total_reviewer = $reviewer_jobs_obj->get_reviewers_count_for_studio_job();

//                $total_reviewer = $this->getRow("SELECT COUNT(*) AS total_reviewers,job_id FROM ".TABLE_REVIEWER_JOBS." WHERE studio_id = '".$user_id."' AND job_id = '".$job_id."'");
//                $total_reviewer['total_reviewers'];

                $job_detail_obj = new Tyr_job_detail_entity();
                $job_detail_obj->reviewer_id = $parent_id;
                $job_detail_obj->job_id = $job_id;
                $job_detail_obj->job_status_id = 1;
                $total_tyroes = $job_detail_obj->get_total_tyroes_by_reviewer_job_id();
                //$total_tyroes = $this->getAll("SELECT * FROM ".TABLE_JOB_DETAIL." WHERE reviewer_id = ".$user_id." AND job_id = ".$job_id." AND job_status_id = 1");


                $pending_tyroes = 0;
                $reviewed_tyroes = 0;
                foreach ($total_tyroes as $total_tyroe) {

                    $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                    $reviewer_reviews_obj->job_id = $job_id;
                    $reviewer_reviews_obj->status_sl = 1;
                    $reviewer_reviews_obj->studio_id = $parent_id;
                    $reviewer_reviews_obj->tyroe_id = $total_tyroe['tyroe_id'];
                    $get_riview = $reviewer_reviews_obj->get_tyroe_reviews_count();

                    //$get_riview = $this->getRow('SELECT COUNT(*) AS tyr_review, tyroe_id FROM '.TABLE_REVIEWER_REVIEWS.' WHERE job_id = '.$job_id.' AND status_sl = 1 AND studio_id = '.$user_id.' AND tyroe_id = '.$total_tyroe['tyroe_id']);

                    if ($get_riview['tyr_review'] == $total_reviewer['total_reviewers']) {
                        if ($get_riview['tyroe_id'] == '') {
                            $get_riview['tyroe_id'] = -1;
                        }
                        $reviewed_tyroes .= ',' . $get_riview['tyroe_id'];
                    } else {
                        if ($get_riview['tyroe_id'] == '') {
                            $get_riview['tyroe_id'] = -1;
                        }
                        $pending_tyroes .= ',' . $get_riview['tyroe_id'];
                    }
                }
                $where_pending = " AND " . TABLE_JOB_DETAIL . ".tyroe_id NOT IN  (" . $reviewed_tyroes . ") ";
                $where_review = " AND " . TABLE_JOB_DETAIL . ".tyroe_id NOT IN  (" . $pending_tyroes . ")
                                  AND " . TABLE_REVIEWER_REVIEWS . ".reviewer_id IS NOT NULL
                                 ";
                //$group_by = " GROUP BY " . TABLE_USERS . ".user_id ";
            }
            if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
                $where_pending = " AND " . TABLE_REVIEWER_REVIEWS . ".reviewer_id IS NULL ";
                $where_review = " AND " . TABLE_REVIEWER_REVIEWS . ".reviewer_id IS NOT NULL ";
            }
            //pending query
//            if($group_by == ''){
//                $group_by = 'GROUP by tyr_users.user_id, tyr_media.media_type, tyr_media.media_name, tyr_industry.industry_name, tyr_countries.country, tyr_cities.city,'
//                        . ' tyr_job_detail.job_detail_id, tyr_reviewer_reviews.reviewer_reviews_id, tyr_reviewer_reviews.tyroe_id ';
//            }else{
//                $group_by .= ' , tyr_media.media_type, tyr_media.media_name, tyr_industry.industry_name, tyr_countries.country, tyr_cities.city,'
//                        . ' tyr_job_detail.job_detail_id, tyr_reviewer_reviews.reviewer_reviews_id, tyr_reviewer_reviews.tyroe_id ';
//            }
            $data_query = "SELECT
                                      " . TABLE_USERS . ".user_id,
                                      firstname,
                                      lastname,
                                      availability,
                                      extra_title,
                                      email,
                                      " . TABLE_MEDIA . ".media_type,
                                      " . TABLE_MEDIA . ".media_name,
                                      " . TABLE_INDUSTRY . ".industry_name,
                                      " . TABLE_COUNTRIES . ".country,
                                      " . TABLE_CITIES . ".city,
                                      " . TABLE_JOB_DETAIL . ".job_detail_id,
                                      " . TABLE_JOB_DETAIL . ".job_id,
                                      " . TABLE_JOB_DETAIL . ".invitation_status,
                                      " . TABLE_JOB_DETAIL . ".invite_notification_status,
                                       " . $selection . "
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_reviews_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewed_type
                                    FROM
                                      " . TABLE_JOB_DETAIL . "
                                      LEFT JOIN " . TABLE_REVIEWER_REVIEWS . "
                                        ON " . TABLE_REVIEWER_REVIEWS . ".job_id = " . TABLE_JOB_DETAIL . ".job_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".tyroe_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".reviewed_type = 'review'
                                        " . $reviewer_check . "
                                        AND " . TABLE_REVIEWER_REVIEWS . ".status_sl = 1
                                      LEFT JOIN " . TABLE_USERS . "
                                        ON " . TABLE_JOB_DETAIL . ".tyroe_id = " . TABLE_USERS . ".user_id
                                      LEFT JOIN " . TABLE_MEDIA . "
                                        ON " . TABLE_MEDIA . ".user_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_MEDIA . ".profile_image = 1
                                        AND " . TABLE_MEDIA . ".status_sl = 1
                                      " . $rate_and_review_join . "
                                      LEFT JOIN " . TABLE_COUNTRIES . "
                                        ON " . TABLE_COUNTRIES . ".country_id = " . TABLE_USERS . ".country_id
                                      LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
                                      LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id = " . TABLE_USERS . ".industry_id
                                    WHERE " . TABLE_JOB_DETAIL . ".job_id = " . $job_id . "
                                      AND " . TABLE_JOB_DETAIL . ".reviewer_id = " . $parent_id . "
                                      AND " . TABLE_JOB_DETAIL . ".job_status_id = 1
                                      AND " . TABLE_JOB_DETAIL . ".status_sl = 1
                                      AND " . TABLE_USERS . ".publish_account = 1
                                      " . $where_pending . "
                                      AND " . TABLE_JOB_DETAIL . ".tyroe_id NOT IN (" . $hidden_user . ") " . $group_by . " " . $order_by . " ";

            //filltering query for applicaint fillters
            if ($this->input->post('fillter_action') == 'Review') {
                $data_query = "SELECT
                                      " . TABLE_USERS . ".user_id,
                                      firstname,
                                      lastname,
                                      availability,
                                      extra_title,
                                      email,
                                      " . TABLE_MEDIA . ".media_type,
                                      " . TABLE_MEDIA . ".media_name,
                                      " . TABLE_COUNTRIES . ".country,
                                      " . TABLE_INDUSTRY . ".industry_name,
                                      " . TABLE_CITIES . ".city,
                                      " . TABLE_JOB_DETAIL . ".job_detail_id,
                                      " . TABLE_JOB_DETAIL . ".invitation_status,
                                      " . TABLE_JOB_DETAIL . ".invite_notification_status,
                                      " . TABLE_JOB_DETAIL . ".job_id,
                                      " . $selection . "
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_reviews_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewed_type
                                    FROM
                                      " . TABLE_JOB_DETAIL . "
                                      LEFT JOIN " . TABLE_REVIEWER_REVIEWS . "
                                        ON " . TABLE_REVIEWER_REVIEWS . ".job_id = " . TABLE_JOB_DETAIL . ".job_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".tyroe_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".reviewed_type = 'review'
                                        " . $reviewer_check . "
                                        AND " . TABLE_REVIEWER_REVIEWS . ".status_sl = 1
                                      LEFT JOIN " . TABLE_USERS . "
                                        ON " . TABLE_JOB_DETAIL . ".tyroe_id = " . TABLE_USERS . ".user_id
                                      LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
                                      LEFT JOIN " . TABLE_MEDIA . "
                                        ON " . TABLE_MEDIA . ".user_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_MEDIA . ".profile_image = 1
                                        AND " . TABLE_MEDIA . ".status_sl = 1
                                      " . $rate_and_review_join . "
                                      LEFT JOIN " . TABLE_COUNTRIES . "
                                        ON " . TABLE_COUNTRIES . ".country_id = " . TABLE_USERS . ".country_id
                                        LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
                                    WHERE " . TABLE_JOB_DETAIL . ".job_id = " . $job_id . "
                                      AND " . TABLE_JOB_DETAIL . ".reviewer_id = " . $parent_id . "
                                      AND " . TABLE_JOB_DETAIL . ".job_status_id = 1
                                      AND " . TABLE_JOB_DETAIL . ".status_sl = 1
                                      AND " . TABLE_USERS . ".publish_account = 1
                                      " . $where_review . "
                                      AND " . TABLE_JOB_DETAIL . ".tyroe_id NOT IN (" . $hidden_user . ") " . $group_by . " " . $order_by . " ";
            } else if ($this->input->post('fillter_action') == 'Decline') {
                $data_query = "SELECT
                                      " . TABLE_USERS . ".user_id,
                                      firstname,
                                      lastname,
                                      availability,
                                      extra_title,
                                      email,
                                      " . TABLE_MEDIA . ".media_type,
                                      " . TABLE_MEDIA . ".media_name,
                                      " . TABLE_COUNTRIES . ".country,
                                       " . TABLE_INDUSTRY . ".industry_name,
                                      " . TABLE_CITIES . ".city,
                                      " . TABLE_JOB_DETAIL . ".job_detail_id,
                                      " . TABLE_JOB_DETAIL . ".job_id,
                                      " . TABLE_JOB_DETAIL . ".invitation_status,
                                      " . TABLE_JOB_DETAIL . ".invite_notification_status,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_reviews_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewer_id,
                                      " . TABLE_REVIEWER_REVIEWS . ".reviewed_type
                                    FROM
                                      " . TABLE_JOB_DETAIL . "
                                      LEFT JOIN " . TABLE_REVIEWER_REVIEWS . "
                                        ON " . TABLE_REVIEWER_REVIEWS . ".job_id = " . TABLE_JOB_DETAIL . ".job_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".tyroe_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_REVIEWER_REVIEWS . ".reviewed_type = 'decline'
                                        " . $reviewer_check . "
                                        AND " . TABLE_REVIEWER_REVIEWS . ".status_sl = 1

                                      LEFT JOIN " . TABLE_USERS . "
                                        ON " . TABLE_JOB_DETAIL . ".tyroe_id = " . TABLE_USERS . ".user_id
                                      LEFT JOIN " . TABLE_MEDIA . "
                                        ON " . TABLE_MEDIA . ".user_id = " . TABLE_JOB_DETAIL . ".tyroe_id
                                        AND " . TABLE_MEDIA . ".profile_image = 1
                                        AND " . TABLE_MEDIA . ".status_sl = 1
                                      LEFT JOIN " . TABLE_COUNTRIES . "
                                        ON " . TABLE_COUNTRIES . ".country_id = " . TABLE_USERS . ".country_id
                                        LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
                                        LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
                                    WHERE " . TABLE_JOB_DETAIL . ".job_id = " . $job_id . "
                                      AND " . TABLE_JOB_DETAIL . ".reviewer_id = " . $parent_id . "
                                      AND " . TABLE_JOB_DETAIL . ".job_status_id = 1
                                      AND " . TABLE_JOB_DETAIL . ".status_sl = -1
                                      AND " . TABLE_USERS . ".publish_account = 1
                                      AND " . TABLE_REVIEWER_REVIEWS . ".reviewer_id IS NOT NULL
                                      AND " . TABLE_JOB_DETAIL . ".tyroe_id NOT IN (" . $hidden_user . ") " . $order_by . "";
            }
            //$get_candidates = $this->getAll($data_query);
            $joins_obj = new Tyr_joins_entity();
            $get_candidates = $joins_obj->get_all_candidate_by_data_query($data_query);

            /* Add to opening dropdown */
            if ($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4) {
                //$this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '".$parent_id."' and status_sl = 1 AND archived_status = 0 AND job_id != '".$job_id."'");
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->user_id = $parent_id;
                $jobs_obj->status_sl = 1;
                $jobs_obj->archived_status = 0;
                $jobs_obj->job_id = $job_id;
                $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
            }
//            else if ($this->session->userdata("role_id") == 4) {
//
//                $studio_id = $parent_id;
//                $reviewer_id = $this->session->userdata('user_id');
//                $joins_obj = new Tyr_joins_entity();
//                $this->data['get_openings_dropdown'] = $joins_obj->get_jobs_openings_listing_1($studio_id, $reviewer_id, $job_id);
//
//                //$this->data['get_openings_dropdown'] = $this->getAll("SELECT ".TABLE_JOBS.".job_id, ".TABLE_JOBS.".job_title FROM ".TABLE_REVIEWER_JOBS." LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".`job_id` = ".TABLE_REVIEWER_JOBS.".`job_id` WHERE ".TABLE_REVIEWER_JOBS.".`reviewer_id` = '".$this->session->userdata('user_id')."' AND ".TABLE_REVIEWER_JOBS.".studio_id = '".$this->session->userdata('parent_id')."' AND ".TABLE_REVIEWER_JOBS.".job_id != '".$job_id."'");
//            }
            /* Add to opening dropdown */
            $candidate_id_array = array();
            $candidate_array = array();
            if ($get_candidates != NULL) {
                for ($i = 0; $i < count($get_candidates); $i++) {
//                        $get_candidates[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                                     FROM " . TABLE_SKILLS . " LEFT JOIN
//                                     " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                                     WHERE user_id='" . $get_candidates[$i]['user_id'] . "' and status_sl=1");
                    $joins_obj = new Tyr_joins_entity();
                    $get_candidates[$i]['skills'] = $joins_obj->get_all_user_skill($get_candidates[$i]['user_id']);

                    #Portfolio/Media
                    //$get_candidates[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_candidates[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                    $media_obj = new Tyr_media_entity();
                    $media_obj->user_id = $get_candidates[$i]['user_id'];
                    $get_candidates[$i]['portfolio'] = $media_obj->get_user_short_portfolio();

                    /* FEATURED TYROE - TRIANGLE */
//                        $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '".$get_candidates[$i]['user_id']."'");
//                        $get_candidates[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                    $featured_tyroes_obj->featured_tyroe_id = $get_candidates[$i]['user_id'];
                    $featured_tyroes_obj->get_user_featured_status();
                    $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                    $get_candidates[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    /* FEATURED TYROE - TRIANGLE */

                    /* Check is favourite or not */
                    //$is_favourite = $this->getRow("SELECT fav_profile_id FROM ".TABLE_FAVOURITE_PROFILE." WHERE user_id = '".$get_candidates[$i]['user_id']."' AND studio_id = '".$user_id."' AND status_sl = 1");
                    //$get_candidates[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                    $favourite_profile_obj = new Tyr_favourite_profile_entity();
                    $favourite_profile_obj->user_id = $get_candidates[$i]['user_id'];
                    $favourite_profile_obj->studio_id = $parent_id;
                    $favourite_profile_obj->status_sl = 1;
                    $is_favourite = $favourite_profile_obj->get_favourite_profile_id();
                    $get_candidates[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                    /* End Checking favourite */

                    /*                     * Getting tyroe jobs* */
                    //$tyroe_jobs = $this->getAll("SELECt job_id FROM ".TABLE_JOB_DETAIL." WHERE tyroe_id = '".$get_candidates[$i]['user_id']."'");
                    //$get_candidates[$i]['tyroe_jobs'] = $tyroe_jobs;
                    $job_details_obj = new Tyr_job_detail_entity();
                    $job_details_obj->tyroe_id = $get_candidates[$i]['user_id'];
                    $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                    $get_candidates[$i]['tyroe_jobs'] = $tyroe_jobs;
                    /*                     * End getting tyroe jobs* */


                    /*                     * get tyroe total score* */
                    if ($this->input->post('fillter_action') == 'Review' && intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
                        //$get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_candidates[$i]['user_id']."' AND job_id = '".$job_id."' AND user_id = '".$this->session->userdata("user_id")."' AND status_sl = 1");
                        $rate_review_obj = new Tyr_rate_review_entity();
                        $rate_review_obj->tyroe_id = $get_candidates[$i]['user_id'];
                        $rate_review_obj->job_id = $job_id;
                        $rate_review_obj->user_id = $this->session->userdata("user_id");
                        $rate_review_obj->status_sl = 1;
                        $get_tyroes_total_score = $rate_review_obj->get_tyroe_score_total_1();

                        if ($get_tyroes_total_score['tyroe_score'] == NULL) {
                            $get_tyroes_total_score['tyroe_score'] = 0;
                        }
                    } else if ($this->session->userdata("role_id") == 3) {
                        //$get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_candidates[$i]['user_id']."' AND job_id = '".$job_id."' AND status_sl = 1");
                        if ($get_tyroes_total_score['tyroe_score'] == NULL) {
                            $get_tyroes_total_score['tyroe_score'] = 0;
                        }
                    }

                    $get_candidates[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    /*                     * end get tyroe total score* */

                    //getting reviewer review
                    if ($this->input->post('fillter_action') == 'Review' && intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
//                            $get_review = $this->getRow("SELECT * FROM ".TABLE_RATE_REVIEW."
//                             LEFT JOIN ".TABLE_JOB_LEVEL." ON ".TABLE_JOB_LEVEL.".job_level_id = ".TABLE_RATE_REVIEW.".level_id
//                             WHERE ".TABLE_RATE_REVIEW.".job_id = '".$job_id."' AND ".TABLE_RATE_REVIEW.".tyroe_id = '".$get_candidates[$i]['user_id']."' AND ".TABLE_RATE_REVIEW.".user_id = '".$this->session->userdata("user_id")."' AND ".TABLE_RATE_REVIEW.".status_sl = 1");
//                            $get_candidates[$i]['reviewer_review'] =$get_review;
                        $candidate_id = $get_candidates[$i]['user_id'];
                        $user_id = $this->session->userdata("user_id");
                        $joins_obj = new Tyr_joins_entity();
                        $get_review = $joins_obj->get_review_details($job_id, $candidate_id, $user_id);
                        $get_candidates[$i]['reviewer_review'] = $get_review;
                    }

                    if (in_array($get_candidates[$i]['user_id'], $candidate_id_array)) {
                        continue;
                    } else {
                        $candidate_id_array[] = $get_candidates[$i]['user_id'];
                        $candidate_array[] = $get_candidates[$i];
                    }
                }
            }

            $this->data['get_candidates'] = $candidate_array;
            $this->template_arr = array('contents/candidate');
            $this->load_template();
        }
    }

    public function Shortlisted($job_id) {
        if ($job_id != "") {

            $user_id = $this->session->userdata("user_id");
            $parent_id = intval($this->session->userdata("parent_id"));
            if ($parent_id == 0) {
                $parent_id = intval($user_id);
            }

//            $user_id = $this->session->userdata("parent_id");
//            if (intval($user_id) == 0) {
//                $user_id = $this->session->userdata("user_id");
//            }
            //$this->update(TABLE_JOB_DETAIL,array('newest_candidate_status' => '1'),"job_id = '".$job_id."' AND job_status_id = 3 AND reviewer_id = '".$user_id."'");
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 3;
            $job_detail_obj->reviewer_id = $user_id;
            $job_detail_obj->newest_candidate_status = 1;
            $job_detail_obj->update_newest_candidate_status();


            //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
            $hidden_obj = new Tyr_hidden_entity();
            $hidden_obj->studio_id = $parent_id;
            $hidden_obj->job_id = $job_id;
            $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

            $hidden_user = 0;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            }

            if ($this->input->post('fillter_drop') == 'most_recent') {
                $order_by = ' ORDER BY ' . TABLE_JOB_DETAIL . '.job_detail_id DESC ';
            } else if ($this->input->post('fillter_drop') == 'aphabetical') {
                $order_by = ' ORDER BY ' . TABLE_USERS . '.firstname ASC ';
            }

//            if (intval($this->job_roles_mapping_obj->is_admin) == 1) {
//                //$this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '".$user_id."' and status_sl = 1 AND archived_status = 0 AND job_id != '".$job_id."'");
//                $jobs_obj = new Tyr_jobs_entity();
//                $jobs_obj->user_id = $user_id;
//                $jobs_obj->status_sl = 1;
//                $jobs_obj->job_id = $job_id;
//                $jobs_obj->archived_status = 0;
//                $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user_1();
//            } else if (intval($this->job_roles_mapping_obj->is_reviewer) == 1) {
//                //$this->data['get_openings_dropdown'] = $this->getAll("SELECT ".TABLE_JOBS.".job_id, ".TABLE_JOBS.".job_title FROM ".TABLE_REVIEWER_JOBS." LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".`job_id` = ".TABLE_REVIEWER_JOBS.".`job_id` WHERE ".TABLE_REVIEWER_JOBS.".`reviewer_id` = '".$this->session->userdata('user_id')."' AND ".TABLE_REVIEWER_JOBS.".studio_id = '".$this->session->userdata('parent_id')."'  AND ".TABLE_REVIEWER_JOBS.".job_id != '".$job_id."'");
//                $studio_id = $this->session->userdata('parent_id');
//                $reviewer_id = $this->session->userdata('user_id');
//                $joins_obj = new Tyr_joins_entity();
//                $this->data['get_openings_dropdown'] = $joins_obj->get_jobs_openings_listing_1($studio_id, $reviewer_id, $job_id);
//            }

            if ($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4) {
                //$this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '".$parent_id."' and status_sl = 1 AND archived_status = 0 AND job_id != '".$job_id."'");
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->user_id = $parent_id;
                $jobs_obj->status_sl = 1;
                $jobs_obj->archived_status = 0;
                $jobs_obj->job_id = $job_id;
                $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
            }


            /* Add to opening dropdown */

//            $get_shortlist = $this->getAll("SELECT ".TABLE_USERS.".user_id,firstname,extra_title,lastname,availability,email,".TABLE_COUNTRIES.".country," . TABLE_CITIES . ".city,".TABLE_JOB_DETAIL.".job_detail_id,".TABLE_JOB_DETAIL.".invitation_status," . TABLE_MEDIA . ".media_type,
//                        " . TABLE_MEDIA . ".media_name," . TABLE_INDUSTRY . ".industry_name from ".TABLE_USERS."
//                        LEFT JOIN ".TABLE_COUNTRIES.
//                " ON ".TABLE_COUNTRIES.".country_id=".TABLE_USERS.".country_id
//                LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
//                LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
//                LEFT JOIN " . TABLE_MEDIA . "
//                ON " . TABLE_MEDIA . ".user_id = ". TABLE_USERS .".user_id
//                AND " . TABLE_MEDIA . ".profile_image = '1'
//                AND " . TABLE_MEDIA . ".status_sl = '1'
//                LEFT JOIN ".TABLE_JOB_DETAIL." ON ".TABLE_USERS.".user_id=".TABLE_JOB_DETAIL.
//                ".tyroe_id AND ".TABLE_JOB_DETAIL.".job_status_id = 3  WHERE ".TABLE_JOB_DETAIL.".job_id='".$job_id."'  AND ".TABLE_USERS.".publish_account = '1' AND ".TABLE_JOB_DETAIL.".status_sl=1 AND ".TABLE_JOB_DETAIL.".reviewer_id = '".$user_id."' AND ".TABLE_JOB_DETAIL.".tyroe_id NOT IN (" . $hidden_user . ") ".$order_by." ");

            $joins_obj = new Tyr_joins_entity();
            $get_shortlist = $joins_obj->get_all_shortlisted_users($job_id, $parent_id, $hidden_user, $order_by);

            if ($get_shortlist != NULL) {
                for ($i = 0; $i < count($get_shortlist); $i++) {

//                    $get_shortlist[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                                 FROM " . TABLE_SKILLS . " LEFT JOIN
//                                 " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                                 WHERE user_id='" . $get_shortlist[$i]['user_id'] . "' and status_sl=1");
                    $joins_obj = new Tyr_joins_entity();
                    $get_shortlist[$i]['skills'] = $joins_obj->get_all_user_skill($get_shortlist[$i]['user_id']);

                    #Portfolio/Media
                    //$get_shortlist[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_shortlist[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                    $media_obj = new Tyr_media_entity();
                    $media_obj->user_id = $get_shortlist[$i]['user_id'];
                    $get_shortlist[$i]['portfolio'] = $media_obj->get_user_short_portfolio();

                    /* FEATURED TYROE - TRIANGLE */
//                    $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '".$get_shortlist[$i]['user_id']."'");
//                    $get_shortlist[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                    $featured_tyroes_obj->featured_tyroe_id = $get_shortlist[$i]['user_id'];
                    $featured_tyroes_obj->get_user_featured_status();
                    $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                    $get_shortlist[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    /* FEATURED TYROE - TRIANGLE */

                    /* Check is favourite or not */
//                    $is_favourite = $this->getRow("SELECT fav_profile_id FROM ".TABLE_FAVOURITE_PROFILE." WHERE user_id = '".$get_shortlist[$i]['user_id']."' AND studio_id = '".$user_id."' AND status_sl = 1");
//                    $get_shortlist[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                    $favourite_profile_obj = new Tyr_favourite_profile_entity();
                    $favourite_profile_obj->user_id = $get_shortlist[$i]['user_id'];
                    $favourite_profile_obj->studio_id = $parent_id;
                    $favourite_profile_obj->status_sl = 1;
                    $is_favourite = $favourite_profile_obj->get_favourite_profile_id();
                    $get_shortlist[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                    /* End Checking favourite */


                    /*                     * Getting tyroe jobs* */
                    //$tyroe_jobs = $this->getAll("SELECt job_id FROM ".TABLE_JOB_DETAIL." WHERE tyroe_id = '".$get_shortlist[$i]['user_id']."'");
                    //$get_shortlist[$i]['tyroe_jobs'] = $tyroe_jobs;
                    $job_details_obj = new Tyr_job_detail_entity();
                    $job_details_obj->tyroe_id = $get_shortlist[$i]['user_id'];
                    $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                    $get_shortlist[$i]['tyroe_jobs'] = $tyroe_jobs;
                    /*                     * End getting tyroe jobs* */

                    /*                     * get tyroe total score* */
//                    $get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_shortlist[$i]['user_id']."'");
//                    if($get_tyroes_total_score['tyroe_score'] == NULL){
//                        $get_tyroes_total_score['tyroe_score'] = 0;
//                    }
//                    $get_shortlist[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    $rate_review_obj = new Tyr_rate_review_entity();
                    $rate_review_obj->tyroe_id = $get_shortlist[$i]['user_id'];
                    $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();
                    if ($get_tyroes_total_score['tyroe_score'] == NULL) {
                        $get_tyroes_total_score['tyroe_score'] = 0;
                    }
                    $get_shortlist[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    /*                     * end get tyroe total score* */
                }
            }
            $this->data['get_shortlist'] = $get_shortlist;
            $this->template_arr = array('contents/shortlisted');
            $this->load_template();
        } else {
            header('location:' . $this->getURL("jobopening"));
        }
    }

    public function Hidden($job_id) {
        if ($job_id != "") {
//            $hidden_candidates = $this->getAll("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                                                username,firstname,lastname,phone,fax,cellphone,address,city,
//                                                state,display_name, " . TABLE_USERS . ".status_sl,
//                                                " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                                                LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                                                LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                                                AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//                                                INNER JOIN " . TABLE_HIDDEN . " ON tyr_users.user_id=" . TABLE_HIDDEN . ".user_id
//                                                and job_id='" . $job_id . "'
//                                                WHERE " . TABLE_USERS . ".role_id='2' and " . TABLE_USERS . ".status_sl='1'");
//            $this->data['hidden_candidates'] = $hidden_candidates;

            $joins_obj = new Tyr_joins_entity();
            $hidden_candidates = $joins_obj->get_all_hidden_candidate_for_job($job_id);
            $this->data['hidden_candidates'] = $hidden_candidates;

            $this->template_arr = array('header', 'contents/hidden', 'footer');
            $this->load_template();
        } else {
            header('location:' . $this->getURL("jobopening"));
        }
    }

    public function shortlisting() { //depricated
        $studio_id = $this->session->userdata("user_id");
        $job_id = addslashes($this->input->post("jid"));
        $user_id = addslashes($this->input->post("user_id"));

//        $check_shortlist=$this->getRow("SELECT COUNT(*) as cnt FROM tyroe.tyr_shortlist
//        WHERE job_id='".$job_id."' AND user_id='".$user_id."' AND status_sl='1'");
        $shortlist_obj = new Tyr_shortlist_entity();
        $shortlist_obj->job_id = $job_id;
        $shortlist_obj->user_id = $user_id;
        $shortlist_obj->check_shortlist();

        if ($shortlist_obj->id > 0) {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ALREADY_SHORTLISTED));
        } else {


//        $data = array("job_id" => $job_id,
//            "user_id" => $user_id,
//            "studio_id" => $studio_id,
//            "created_timestamp" => strtotime("now"),
//            "status_sl" => "1"
//        );
//        $return = $this->insert(TABLE_SHORTLIST, $data);
            $shortlist_obj->job_id = $job_id;
            $shortlist_obj->user_id = $user_id;
            $shortlist_obj->studio_id = $studio_id;
            $shortlist_obj->status_sl = 1;
            $return = $shortlist_obj->save_shortlist();

            if ($return) {

                $this->set_activity_stream($studio_id, $user_id, $job_id, $this->session->userdata("role_id"), $data, LABEL_SHORTLIST_JOB_LOG);


                # Send job shortlisting Notification
                $notification_type = "email";
                if ($notification_type == "email") {
                    $this->email_sender('notify@tyroe.com', 'Shortlisted for job', 'Thank you for your application you are selected to fill this position');
                }
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_SHORTLISTED_SUCCESS));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        }
    }

    public function HideUser() { //depricated
        //script for candidate jobs
        //$user_id = $this->session->userdata("user_id");
        $job_id = 0;
        $action = '';
        if ($this->input->post('action') != '') {
            $action = $this->input->post('action');
            $job_id = $this->input->post('job_id');
        }
        $hide_tip = $this->input->post('hide_tip');
        $studio_id = $this->session->userdata("user_id");
        $tyroe_id = addslashes($this->input->post("tyroe_id"));
        if ($hide_tip == "unhide") {
//            $table = TABLE_HIDE_USER;
//            $where = "tyroe_id = '".$tyroe_id."' AND studio_id = '".$studio_id."'";
//            $this->Delete($table,$where);
            $hide_user_obj = new Tyr_hide_user_entity();
            $hide_user_obj->tyroe_id = $tyroe_id;
            $hide_user_obj->studio_id = $studio_id;
            $hide_user_obj->delete_hide_users_by_studio();
            echo json_encode(array('success' => 'unhidden', 'success_message' => UN_HIDE_MESSAGE_SUCCESS));
        } else {
            if ($action == 'candidate_hide') {
                $table = TABLE_HIDDEN;
                $data = array(
                    "job_id" => $job_id,
                    "user_id" => $tyroe_id,
                    "studio_id" => $studio_id,
                    "created_timestamp" => strtotime("now"),
                    "status_sl" => '1'
                );

                $hidden_obj = new Tyr_hidden_entity();
                $hidden_obj->user_id = $tyroe_id;
                $hidden_obj->job_id = $job_id;
                $hidden_obj->studio_id = $studio_id;
                $hidden_obj->status_sl = 1;
                $hidden_obj->created_timestamp = strtotime("now");
                $return = $hidden_obj->save_hidden();
            } else {
                $table = TABLE_HIDE_USER;
                $data = array(
                    "tyroe_id" => $tyroe_id,
                    "studio_id" => $studio_id,
                    "created_timestamp" => strtotime("now")
                );

                $hide_user_obj = new Tyr_hide_user_entity();
                $hide_user_obj->tyroe_id = $tyroe_id;
                $hide_user_obj->status_sl = 1;
                $hide_user_obj->studio_id = $studio_id;
                $hide_user_obj->created_timestamp = strtotime("now");
                $return = $hide_user_obj->save_hide_user();
            }

            //$return = $this->insert($table, $data);

            if ($return) {
                #JOB ACTIVITY START
//                $job_activity = array(
//                    'performer_id' => $user_id,
//                    'job_id' => $job_id,
//                    'activity_type' => LABEL_HIDE_JOB_LOG,
//                    'activity_data' => serialize($data),
//                    'created_timestamp' => strtotime("now"),
//                    'status_sl' => '1'
//                );
//                $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

                $job_activity_obj = new Tyr_job_activity_entity();
                $job_activity_obj->performer_id = $studio_id;
                $job_activity_obj->job_id = $job_id;
                $job_activity_obj->object_id = $tyroe_id;
                $job_activity_obj->activity_type = LABEL_HIDE_JOB_LOG;
                $job_activity_obj->activity_data = serialize($data);
                $job_activity_obj->status_sl = 1;
                $job_activity_obj->created_timestamp = strtotime("now");
                $job_activity_obj->save_job_activity();

                #JOB ACTIVITY END
                echo json_encode(array('success' => true, 'success_message' => HIDE_MESSAGE_SUCCESS));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        }
    }

    public function Allcandidates($job_id) { //depricated
        $this->data['job_id'] = $job_id;
//        $get_candidates = $this->getAll("SELECT  tyr_users.user_id,   tyr_users.country_id,  email,  username,     cellphone,
//                  address,  city,  state,     " . TABLE_COUNTRIES . ".country,  " . TABLE_MEDIA . ".media_type,  " . TABLE_MEDIA . ".media_name
//                FROM
//                  " . TABLE_USERS . "
//                  LEFT JOIN " . TABLE_COUNTRIES . "
//                    ON " . TABLE_USERS . ".country_id = " . TABLE_COUNTRIES . ".country_id
//                  LEFT JOIN " . TABLE_MEDIA . "
//                    ON tyr_users.user_id = " . TABLE_MEDIA . ".user_id
//                    AND " . TABLE_MEDIA . ".profile_image = '1'
//                    AND " . TABLE_MEDIA . ".status_sl = '1'
//                WHERE " . TABLE_USERS . ".role_id = '2'
//                  AND " . TABLE_USERS . ".status_sl = '1' ");
        $joins_obj = new Tyr_joins_entity();
        $get_candidates = $joins_obj->get_all_candidate();
        $this->data['get_candidate'] = $get_candidates;
        $this->template_arr = array('header', 'contents/all_candidates', 'footer');
        $this->load_template();
    }

    public function SaveComment() {
        $user_id = $this->session->userdata("user_id");
        $job_id = addslashes($this->input->post("jid"));
        $comment = addslashes($this->input->post("comment"));

        $data = array("job_id" => $job_id,
            "studio_id" => $user_id,
            "comment_description" => $comment,
            "created_timestamp" => strtotime("now"),
            "status_sl" => "1"
        );
//        $return = $this->insert(TABLE_COMMENT_JOB, $data);

        $comment_job = new Tyr_comment_job_entity();
        $comment_job->job_id = $job_id;
        $comment_job->studio_id = $user_id;
        $comment_job->comment_description = $comment;
        $comment_job->status_sl = 1;
        $comment_job->created_timestamp = strtotime("now");
        $comment_job->save_comment_job();
        $return = true;

        if ($return) {
            #JOB ACTIVITY START
            $this->set_activity_stream($user_id, "", $job_id, $this->session->userdata("role_id"), $data, LABEL_COMMENT_JOB_LOG);

            /* $job_activity = array(
              'user_id' => $user_id,
              'job_id' => $job_id,
              'activity_type' => LABEL_COMMENT_JOB_LOG,
              'activity_data' => serialize($data),
              'created_timestamp' => strtotime("now"),
              'status_sl' => '1'
              );
              $this->insert(TABLE_JOB_ACTIVITY, $job_activity); */
            #JOB ACTIVITY END
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
        /* $this->template_arr = array('header', 'contents/candidate_detail', 'footer');
          $this->load_template(); */
    }

    public function SetArchive() {
        $user_id = $this->session->userdata("user_id");
//        $where = "job_id = '" . addslashes($this->input->post("jid")) . "'";
//        $studio_id = addslashes($this->input->post("studio"));
        $job_id = $this->input->post("jid");

        $data = array(
            "modified_timestamp" => strtotime("now"),
            "archived_status" => "1"
        );
        //$return = $this->update(TABLE_JOBS, $data, $where);
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->archived_status = 1;
        $jobs_obj->job_id = $job_id;
        $return = $jobs_obj->update_job_archived_status_by_id();


        if ($return) {
            #JOB ACTIVITY START
            $this->set_activity_stream($user_id, "", $job_id, $this->session->userdata("role_id"), $data, LABEL_ARCHIVED_JOB_LOG);
            /* $job_activity = array(
              'user_id' => $user_id,
              'job_id' => $job_id,
              'activity_type' => LABEL_ARCHIVED_JOB_LOG,
              'activity_data' => serialize($data),
              'created_timestamp' => strtotime("now"),
              'status_sl' => '1'
              );
              $this->insert(TABLE_JOB_ACTIVITY, $job_activity); */
            #JOB ACTIVITY END
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function InviteReviewer() {
        $studio_id = $this->session->userdata("user_id");
        $reviewer_id = addslashes($this->input->post("reviewer_id"));
        $job_id = addslashes($this->input->post("job_id"));

        $data = array(
            "reviewer_id" => $reviewer_id,
            "studio_id" => $studio_id,
            "job_id" => $job_id,
            "created_timestamp" => strtotime("now"),
        );
        //$return = $this->insert(TABLE_REVIEWER_JOBS, $data);

        $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
        $reviewer_jobs_obj->reviewer_id = $reviewer_id;
        $reviewer_jobs_obj->studio_id = $studio_id;
        $reviewer_jobs_obj->job_id = $job_id;
        $return = $reviewer_jobs_obj->save_reviewer_jobs();


        if ($return) {
            $modules = @explode(',', REVIEWER_USER_MODULES);
            foreach ($modules as $k => $v) {
//                $user_modules = array(
//                    'user_id' => $reviewer_id,
//                    'module_id' => $v);
                //$this->insert(TABLE_PERMISSIONS, $user_modules);
                $permissions_obj = new Tyr_permissions_entity();
                $permissions_obj->user_id = $reviewer_id;
                $permissions_obj->module_id = $v;
                $permissions_obj->save_permissions();
            }
            #JOB ACTIVITY START
            $data["search_keywords"] = "invite reviewer job opening openings";
//            $job_activity = array(
//                'performer_id' => $user_id,
//                'job_id' => $job_id,
//                'activity_type' => LABEL_INVITE_REVIEWER_JOB_LOG,
//                'activity_data' => serialize($data),
//                'created_timestamp' => strtotime("now"),
//                'status_sl' => '1'
//            );
//            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
//
            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $studio_id;
            $job_activity_obj->job_id = $job_id;
            $job_activity_obj->object_id = $studio_id;
            $job_activity_obj->activity_type = LABEL_INVITE_REVIEWER_JOB_LOG;
            $job_activity_obj->activity_data = serialize($data);
            $job_activity_obj->status_sl = 1;
            $job_activity_obj->save_job_activity();
            #JOB ACTIVITY END
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function InviteTyroe() {
        $reviewer_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $reviewer_id = $this->session->userdata("parent_id");
        }
        $tyroe_id = addslashes($this->input->post("tyroe_id"));
        $job_id = addslashes($this->input->post("job_id"));
        $search_param = addslashes($this->input->post("search_param"));

        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->reviewer_id = $reviewer_id;
        $job_details_obj->job_id = $job_id;
        $job_details_obj->tyroe_id = $tyroe_id;
        $job_details_obj->status_sl = 1;
        $duplicate_entry = $job_details_obj->check_duplicate_entry_count();

        //$duplicate_entry = $this->getRow("SELECT COUNT(*)as cnt FROM " . TABLE_JOB_DETAIL . " WHERE reviewer_id='" . $reviewer_id . "' AND job_id='" . $job_id . "' AND tyroe_id='" . $tyroe_id . "' and status_sl='1'");


        if ($duplicate_entry['cnt'] > 0) {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ALREADY_COPY_FOR_JOB));
        } else {
            if ($this->session->userdata('parent_id') == '0') {
                $on = "ON u.user_id = c.studio_id";
            } else if ($this->session->userdata('parent_id') != '0') {
                $on = "ON u.parent_id = c.studio_id";
            }

//            $email_data = $this->getRow("SELECT u.firstname, u.lastname, j.job_title, c.company_id, c.company_name, j.job_title as opening_title
//                                                FROM tyr_users u
//                                                LEFT JOIN tyr_company_detail c ".$on."
//                                                LEFT JOIN tyr_jobs j ON c.studio_id = j.user_id
//                                                WHERE u.user_id = '".$this->session->userdata('user_id')."' AND j.job_id = '".$job_id."'");
            $joins_obj = new Tyr_joins_entity();
            $email_data = $joins_obj->get_email_data_for_tyroe($on, $job_id, $this->session->userdata("user_id"));

            #Add JOB
            $arr = explode(",", $tyroe_id);
            for ($i = 0; $i < count(explode(",", $tyroe_id)); $i++) {

//                $data = array(
//                    "reviewer_id"       => $reviewer_id,
//                    "tyroe_id"          => (explode(",",$tyroe_id)[$i]),
//                    "job_id"            => $job_id,
//                    "invitation_status" => 0,
//                    "reviewer_type"     => $this->session->userdata("role_id"),
//                    "job_status_id"     => 1,
//                    "status_sl"         => 1
//                );
//                $table = TABLE_JOB_DETAIL;
                //$return = $this->insert($table, $data);

                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->reviewer_id = $reviewer_id;
                $job_details_obj->tyroe_id = $arr[$i];
                $job_details_obj->job_id = $job_id;
                $job_details_obj->invitation_status = 0;
                $job_details_obj->reviewer_type = $this->session->userdata("role_id");
                $job_details_obj->job_status_id = 1;
                $job_details_obj->status_sl = 1;
                $return = $job_details_obj->save_job_detail();
            }

            if ($return) {

                $schedule_notification_obj = new Tyr_schedule_notification_entity();
                $schedule_notification_obj->user_id = $tyroe_id;
                $schedule_notification_obj->option_id = 15;
                $check_notification_job_status = $schedule_notification_obj->get_schedule_notification_by_user_option();

                //$check_notification_job_status = $this->getRow("SELECT * FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$tyroe_id."' AND option_id = 15");

                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $tyroe_id;
                $users_obj->get_user();
                $current_tyroe_data = (array) $users_obj;

                //$current_tyroe_data = $this->getRow("SELECT firstname, lastname, email FROM ".TABLE_USERS." WHERE user_id  = '".$tyroe_id."'");

                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->get_jobs();
                $job_data = (array) $job_data;
                $job_data['opening_title'] = $jobs_obj->job_title;

                //$job_data = $this->getRow("SELECT job_title AS opening_title FROM ".TABLE_JOBS." WHERE job_id  = '".$job_id."'");


                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $this->session->userdata('user_id');
                $users_obj->get_user();
                $performer_name = array('username' => $users_obj->username);
                //$performer_name = $this->getRow("SELECT username FROM ".TABLE_USERS." WHERE user_id=".$this->session->userdata('user_id'));
                #Performer_Studio - Object_ReviewerAndTyroe Stream
                /* if($this->session->userdata('role_id') == 3){
                  $get_all_reviewers = $this->getAll("SELECT * FROM ".TABLE_REVIEWER_JOBS." WHERE studio_id = '".$this->session->userdata('user_id')."'");
                  if(!empty($get_all_reviewers)){
                  for($rev=0; $rev<count($get_all_reviewers); $rev++){
                  $activity_data = array(
                  "reviewer_name" => $reviewer_data['firstname'] . " " . $reviewer_data['lastname'],
                  "job_title" => $reviewer_data['opening_title'],
                  "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                  "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG
                  );
                  $job_activity = array(
                  'performer_id' => $this->session->userdata('user_id'),
                  'job_id' => $job_id,
                  'object_id' => $tyroe_id,
                  'activity_type' => LABEL_JOB_CANDIDATE_ACTIVITY,
                  'activity_data' => serialize($activity_data),
                  'created_timestamp' => strtotime("now"),
                  'status_sl' => '1'
                  );
                  $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
                  }
                  }
                  } */
                #Performer_Reviewer - Object_StudioAndTyroe Stream
                #Tyroe - Stream
                $tyroe_activity_data = array(
                    "job_title" => $email_data['opening_title'],
                    "company_name" => $email_data['company_name'],
                    "performer_name" => $performer_name['username'],
                    "job_id" => $job_id
                );

//                $tyroe_job_activity = array(
//                    'performer_id' => $tyroe_id,
//                    'job_id' => $job_id,
//                    'object_id' => $tyroe_id,
//                    'activity_type' => LABEL_JOB_CANDIDATE_ACTIVITY,
//                    'activity_data' => serialize($tyroe_activity_data),
//                    'created_timestamp' => strtotime("now"),
//                    'status_sl' => '1'
//                );
//                $this->insert(TABLE_JOB_ACTIVITY, $tyroe_job_activity);
                $job_activity_obj = new Tyr_job_activity_entity();
                $job_activity_obj->performer_id = $tyroe_id;
                $job_activity_obj->job_id = $job_id;
                $job_activity_obj->object_id = $tyroe_id;
                $job_activity_obj->activity_type = LABEL_JOB_CANDIDATE_ACTIVITY;
                $job_activity_obj->activity_data = serialize($tyroe_activity_data);
                $job_activity_obj->status_sl = 1;
                $job_activity_obj->created_timestamp = strtotime("now");
                $job_activity_obj->save_job_activity();

                if ($this->session->userdata('role_id') == 3) {
                    $studio_activity_data = array(
                        "performer_name" => "You",
                        "job_title" => $job_data['opening_title'],
                        "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG,
                        "job_id" => $job_id
                    );
//                    $studio_job_activity = array(
//                        'performer_id' => $this->session->userdata("user_id"),
//                        'job_id' => $job_id,
//                        'object_id' => $tyroe_id,
//                        'activity_type' => LABEL_JOB_CANDIDATE_ACTIVITY,
//                        'activity_data' => serialize($studio_activity_data),
//                        'created_timestamp' => strtotime("now"),
//                        'status_sl' => '1'
//                    );
//                    $this->insert(TABLE_JOB_ACTIVITY, $studio_job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $this->session->userdata("user_id");
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = LABEL_JOB_CANDIDATE_ACTIVITY;
                    $job_activity_obj->activity_data = serialize($studio_activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();


//                    $other_reviewers = $this->getAll("SELECT user_id FROM ".TABLE_USERS." WHERE
//                     parent_id='".$this->session->userdata("user_id")."' and status_sl=1");

                    $users_obj = new Tyr_users_entity();
                    $users_obj->parent_id = $this->session->userdata("user_id");
                    $users_obj->status_sl = 1;
                    $other_reviewers = $users_obj->get_all_reviewer_for_studio();

                    foreach ($other_reviewers as $reviewer) {
                        $rev_activity_data = array(
                            "performer_name" => $performer_name['username'],
                            "job_title" => $job_data['opening_title'],
                            "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                            "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG,
                            "job_id" => $job_id
                        );
//                        $rev_job_activity = array(
//                            'performer_id' => $reviewer['user_id'],
//                            'job_id' => $job_id,
//                            'object_id' => $tyroe_id,
//                            'activity_type' => LABEL_JOB_CANDIDATE_ACTIVITY,
//                            'activity_data' => serialize($rev_activity_data),
//                            'created_timestamp' => strtotime("now"),
//                            'status_sl' => '1'
//                        );
//                        $this->insert(TABLE_JOB_ACTIVITY, $rev_job_activity);

                        $job_activity_obj = new Tyr_job_activity_entity();
                        $job_activity_obj->performer_id = $reviewer['user_id'];
                        $job_activity_obj->job_id = $job_id;
                        $job_activity_obj->object_id = $tyroe_id;
                        $job_activity_obj->activity_type = LABEL_JOB_CANDIDATE_ACTIVITY;
                        $job_activity_obj->activity_data = serialize($rev_activity_data);
                        $job_activity_obj->status_sl = 1;
                        $job_activity_obj->created_timestamp = strtotime("now");
                        $job_activity_obj->save_job_activity();
                    }
                } else if ($this->session->userdata('role_id') == 4) {

                    $activity_data = array(
                        "performer_name" => "You",
                        "job_title" => $job_data['opening_title'],
                        "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG,
                        "job_id" => $job_id
                    );
                    $this->set_activity_stream_new($this->session->userdata("user_id"), $job_id, $tyroe_id, $activity_data, LABEL_JOB_CANDIDATE_ACTIVITY);

                    $activity_data = array(
                        "performer_name" => $performer_name['username'],
                        "job_title" => $job_data['opening_title'],
                        "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG,
                        "job_id" => $job_id
                    );
                    $this->set_activity_stream_new($this->session->userdata("parent_id"), $job_id, $tyroe_id, serialize($activity_data), LABEL_JOB_CANDIDATE_ACTIVITY);

                    $users_obj = new Tyr_users_entity();
                    $users_obj->parent_id = $this->session->userdata("parent_id");
                    $users_obj->user_id = $this->session->userdata('user_id');
                    $other_reviewers = $users_obj->get_all_reviewer_for_studio_1();

//                    $other_reviewers = $this->getAll("SELECT user_id FROM ".TABLE_USERS." WHERE
//                                         parent_id='".$this->session->userdata("parent_id")."' AND user_id != '".$this->session->userdata('user_id')."'");

                    foreach ($other_reviewers as $reviewer) {

                        $activity_data = array(
                            "performer_name" => "A team member",
                            "job_title" => $job_data['opening_title'],
                            "tyroe_name" => $current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'],
                            "activity_sub_type" => LABEL_INVITE_TYROE_JOB_LOG,
                            "job_id" => $job_id
                        );
                        $this->set_activity_stream_new($reviewer['user_id'], $job_id, $tyroe_id, serialize($activity_data), LABEL_JOB_CANDIDATE_ACTIVITY);
                    }
                }


                if ($check_notification_job_status['status_sl'] == 1) {
                    $email_to = $tyroe_id;
                    $get_details = $this->getRow("SELECT email FROM " . TABLE_USERS . " WHERE " . TABLE_USERS . ".user_id='" . $email_to . "' AND " . TABLE_USERS . ".status_sl='1'");
                    /* Email to User */
                    $job_name = $job_data['opening_title'];
                    $company_name = $email_data['company_name'];

                    $notification_templates_obj = new Tyr_notification_templates_entity();
                    $notification_templates_obj->notification_id = 'n02';
                    $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                    //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n02'");

                    $url = $this->getURL("opening/" . strtolower(str_replace(' ', '-', $job_name) . "-" . $this->generate_job_number($job_id)));
                    $subject = $get_message['subject'];
                    $subject = str_replace(array('[$job_title]', '[$company_name]', '[$url_viewinvitation]', '[$url_tyroe]'), array($job_name, $company_name, $url, LIVE_SITE_URL), $subject);
                    $message = htmlspecialchars_decode($get_message['template']);
                    $message = str_replace(array('[$job_title]', '[$company_name]', '[$url_viewinvitation]', '[$url_tyroe]'), array($job_name, $company_name, $url, LIVE_SITE_URL), $message);

                    /* $job_name = $job_data['opening_title'];
                      $company_name = $email_data['company_name'];
                      $subject = LABEL_JOB_CANDIDATE_ACTIVITY_TEXT . " " . $job_name." at ".$company_name;
                      $message = "Woohoo! You just applied for a job using Tyroe. You are one step closer to landing this awesome job. To increase your chances make sure your profile looks the bomb and is completely up-to-date.";
                      $message .= "<br><br>";
                      $message .= '<a href="' . $this->getURL("opening/".strtolower(str_replace(' ','-',$job_name)."-".$this->generate_job_number($job_id))) . '" class="" style=""><img src="'.ASSETS_PATH.'img/view-job-detail-btn.png"></a>';
                      $message .= "<br><br>";
                      $message .= "Good luck with your job application.";
                      $message .= "<br><br>";
                      $message .= "<i>-Team Tyroe</i>"; */
                    $invited_candidate_email = $get_details['email'];
                    $this->email_sender($invited_candidate_email, $subject, $message);
                    /* Email to User */
                }
                $replace_this = array('[TYROE NAME]', '[JOB NAME]');
                $by_this = array($current_tyroe_data['firstname'] . " " . $current_tyroe_data['lastname'], $email_data['opening_title']);
                $invite_success_message = str_replace($replace_this, $by_this, MESSAGE_INVITE_SUCCESS);
                echo json_encode(array('success' => true, 'success_message' => $invite_success_message));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        }
    }

    public function applicaint_to_candidate() {
        try {
            $job_detail_id = $this->input->post('job_detail_id');
            $action = $this->input->post('action');
            $user_id = $this->session->userdata("user_id");
            if ($this->session->userdata("role_id") == 4) {
                $user_id = $this->session->userdata("parent_id");
            }
            $job_id = $this->input->post('job_id');
            $tyroe_id = $this->input->post('tyroe_id');

            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_detail_id = $job_detail_id;
            $job_details_obj->get_job_detail();

            //add into candidate section
            if ($action == 'decline_applicaint') {
                $records = array(
                    'status_sl' => -1
                );
                $job_details_obj->status_sl = -1;
                $activity_type_data = LABEL_DECLINE_CANDIDATE;
            } else if ($action == 'add_candidate') {
                $records = array(
                    'job_status_id' => 1,
                    'invitation_status' => 1,
                    'newest_candidate_status' => 0,
                    'status_sl' => 1
                );

                $job_details_obj->job_status_id = 1;
                $job_details_obj->invitation_status = 1;
                $job_details_obj->invite_notification_status = 1;
                $job_details_obj->newest_candidate_status = 0;
                $job_details_obj->status_sl = 1;

                $activity_type_key = LABEL_JOB_CANDIDATE_ACTIVITY;
                $activity_type_data = LABEL_APPLICAINT_MOVE_TO_CANDIDATE;
                $email_activity_data = LABEL_MOVE_TO_CANDIDATE_TEXT;
            } else if ($action == 'candidate_to_shortlist') {
                $records = array(
                    'job_status_id' => 3,
                    'invitation_status' => 1,
                    'newest_candidate_status' => 0,
                    'modified_timestamp' => strtotime("now"),
                    'status_sl' => 1
                );

                $job_details_obj->job_status_id = 3;
                $job_details_obj->invitation_status = 1;
                $job_details_obj->newest_candidate_status = 0;
                $job_details_obj->modified_timestamp = strtotime("now");
                $job_details_obj->status_sl = 1;

                $activity_type_key = LABEL_SHORTLIST_JOB_LOG;
            } else if ($action == 'shortlist_to_candidate') {
                $records = array(
                    'job_status_id' => 1,
                    'invitation_status' => 1,
                    'newest_candidate_status' => 0,
                    'status_sl' => 1
                );

                $job_details_obj->job_status_id = 1;
                $job_details_obj->invitation_status = 1;
                $job_details_obj->newest_candidate_status = 0;
                $job_details_obj->status_sl = 1;

                $activity_type_data = LABEL_APPLICAINT_MOVE_TO_CANDIDATE;
                $email_activity_data = LABEL_MOVE_TO_CANDIDATE_TEXT;
            }

            if ($action == 'add_to_archive') {
                $records = array(
                    'archived_status' => 1
                );
                //$this->update(TABLE_JOBS,$records,"job_id = '".$job_id."'");

                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->get_jobs();
                $jobs_obj->archived_status = 1;
                $jobs_obj->updated_by = $user_id;
                $jobs_obj->update_jobs();
            } else if ($action == 'undo_decline') {
                $reveiwer_reviews_id = $this->input->post('reveiwer_reviews_id');
                $records = array(
                    'status_sl' => 1
                );
                //$this->update(TABLE_JOB_DETAIL,$records,"job_detail_id = '".$job_detail_id."'");

                $job_details_obj_1 = new Tyr_job_detail_entity();
                $job_details_obj_1->job_detail_id = $job_detail_id;
                $job_details_obj_1->get_job_detail();
                $job_details_obj_1->status_sl = 1;
                $job_details_obj_1->update_job_detail();

                if ($reveiwer_reviews_id != 0) {
                    $records = array(
                        'status_sl' => -1
                    );
                    $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                    $reviewer_reviews_obj->reviewer_reviews_id = $reveiwer_reviews_id;
                    $reviewer_reviews_obj->get_reviewer_reviews();
                    $reviewer_reviews_obj->status_sl = -1;
                    $reviewer_reviews_obj->update_reviewer_reviews();

                    //$this->update(TABLE_REVIEWER_REVIEWS,$records,"reviewer_reviews_id = '".$reveiwer_reviews_id."'");
                }
            } else {
                $job_details_obj->job_detail_id = $job_detail_id;
                $job_details_obj->update_job_detail();
                //$this->update(TABLE_JOB_DETAIL,$records,"job_detail_id = '".$job_detail_id."'");

                if (($action == 'candidate_to_shortlist')) {
                    #Stream
                    $joins_obj = new Tyr_joins_entity();
                    $email_data = $joins_obj->get_email_data_for_user_job($this->session->userdata('user_id'), $job_id);

//                $email_data = $this->getRow("SELECT u.firstname, u.lastname, j.job_title, c.company_id, c.company_name, j.job_title as opening_title
//                                                FROM tyr_users u
//                                                LEFT JOIN tyr_company_detail c ON u.user_id = c.studio_id
//                                                LEFT JOIN tyr_jobs j ON c.studio_id = j.user_id
//                                                WHERE u.user_id = '".$this->session->userdata('user_id')."' AND j.job_id = '".$job_id."'");
                    //$tyroe_data = $this->getRow("SELECT firstname, lastname FROM ".TABLE_USERS." WHERE user_id = '".$tyroe_id."'");
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $tyroe_id;
                    $users_obj->get_user();
                    $tyroe_data = (array) $users_obj;

                    $jobs_obj = new Tyr_jobs_entity();
                    $jobs_obj->job_id = $job_id;
                    $jobs_obj->get_jobs();

                    $get_all_reviewers = $joins_obj->get_all_reviewers_by_studio_job($this->session->userdata('user_id'), $job_id);

//                $get_all_reviewers = $this->getAll("SELECT rj.reviewer_id, u.email FROM ".TABLE_REVIEWER_JOBS." rj
//                                                        LEFT JOIN ".TABLE_USERS." u ON rj.reviewer_id = u.user_id
//                                                        WHERE rj.studio_id = '".$this->session->userdata('user_id')."' AND rj.job_id = '".$job_id."'");
#Studio
                    $studio_activity_data = array(
                        "job_title" => $jobs_obj->job_title,
                        "tyroe_name" => $tyroe_data['firstname'] . " " . $tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_CANDIDATE_TO_SHORTLIST,
                        "job_id" => $job_id,
                        "search_keywords" => "job shortlist shortlisted"
                    );
//                $studio_job_activity = array(
//                    'performer_id' => $this->session->userdata('user_id'),
//                    'job_id' => $job_id,
//                    'object_id' => $tyroe_id,
//                    'activity_type' => $activity_type_key,
//                    'activity_data' => serialize($studio_activity_data),
//                    'created_timestamp' => strtotime("now"),
//                    'status_sl' => '1'
//                );
//                $this->insert(TABLE_JOB_ACTIVITY, $studio_job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $this->session->userdata('user_id');
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = $activity_type_key;
                    $job_activity_obj->activity_data = serialize($studio_activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();

                    #Reviewer
                    $reviewer_activity_data = array(
                        "job_title" => $email_data['opening_title'],
                        "studio_admin_name" => $email_data['firstname'] . " " . $email_data['lastname'],
                        "tyroe_name" => $tyroe_data['firstname'] . " " . $tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_CANDIDATE_TO_SHORTLIST,
                        "job_id" => $job_id,
                        "search_keywords" => "job shortlist shortlisted"
                    );
                    if (!empty($get_all_reviewers)) {
                        for ($rev = 0; $rev < count($get_all_reviewers); $rev++) {

//                        $reviewer_job_activity = array(
//                            'performer_id' => $get_all_reviewers[$rev]['reviewer_id'],
//                            'job_id' => $job_id,
//                            'object_id' => $tyroe_id,
//                            'activity_type' => $activity_type_key,
//                            'activity_data' => serialize($reviewer_activity_data),
//                            'created_timestamp' => strtotime("now"),
//                            'status_sl' => '1'
//                        );
//                        $this->insert(TABLE_JOB_ACTIVITY, $reviewer_job_activity);

                            $job_activity_obj = new Tyr_job_activity_entity();
                            $job_activity_obj->performer_id = $get_all_reviewers[$rev]['reviewer_id'];
                            $job_activity_obj->job_id = $job_id;
                            $job_activity_obj->object_id = $tyroe_id;
                            $job_activity_obj->activity_type = $activity_type_key;
                            $job_activity_obj->activity_data = serialize($reviewer_activity_data);
                            $job_activity_obj->status_sl = 1;
                            $job_activity_obj->created_timestamp = strtotime("now");
                            $job_activity_obj->save_job_activity();

                            #Email
                            $job_title = $email_data['opening_title'];
                            $studio_admin_name = $reviewer_activity_data['studio_admin_name'];
                            $tyroe_name = $reviewer_activity_data['tyroe_name'];
                            $url = $this->getURL("openings/" . strtolower(str_replace(' ', '-', $email_data['opening_title']) . "-" . $this->generate_job_number($job_id))) . '?job=' . md5('shortlist');
                            $company_name = $email_data['company_name'];

                            $notification_templates_obj = new Tyr_notification_templates_entity();
                            $notification_templates_obj->notification_id = 'n12';
                            $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                            //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n12'");

                            $subject = $get_message['subject'];
                            $subject = str_replace(array('[$job_title]', '[$company_name]', '[$studio_admin_name]', '[$tyroe_name]', '[$url_viewshortlisted]', '[$url_tyroe]'), array($job_title, $company_name, $studio_admin_name, $tyroe_name, $url, LIVE_SITE_URL), $subject);
                            $message = htmlspecialchars_decode($get_message['template']);
                            $message = str_replace(array('[$job_title]', '[$company_name]', '[$studio_admin_name]', '[$tyroe_name]', '[$url_viewshortlisted]', '[$url_tyroe]'), array($job_title, $company_name, $studio_admin_name, $tyroe_name, $url, LIVE_SITE_URL), $message);
                            $current_user_email = $get_all_reviewers[$rev]['email'];
                            $this->email_sender($current_user_email, $subject, $message);
                        }
                    }


                    #Tyroe
                    $tyroe_activity_data = array(
                        "job_title" => $email_data['opening_title'],
                        "company_name" => $email_data['company_name'],
                        "job_id" => $job_id,
                        "search_keywords" => "job shortlist shortlisted"
                    );

//                $tyroe_job_activity = array(
//                    'performer_id' => $tyroe_id,
//                    'job_id' => $job_id,
//                    'object_id' => $tyroe_id,
//                    'activity_type' => $activity_type_key,
//                    'activity_data' => serialize($tyroe_activity_data),
//                    'created_timestamp' => strtotime("now"),
//                    'status_sl' => '1'
//                );
//                $this->insert(TABLE_JOB_ACTIVITY, $tyroe_job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $tyroe_id;
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = $activity_type_key;
                    $job_activity_obj->activity_data = serialize($tyroe_activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();
                } elseif ($action == 'add_candidate') {
                    #Stream
                    if ($this->session->userdata('role_id') == '3') {
                        $on = "ON u.user_id = c.studio_id";
                    } else if ($this->session->userdata('role_id') == '4') {
                        $on = "ON u.parent_id = c.studio_id";
                    }

//                $email_data = $this->getRow("SELECT u.firstname, u.lastname, u.job_title, c.company_id, c.company_name, j.job_title as opening_title
//                                                FROM tyr_users u
//                                                LEFT JOIN tyr_company_detail c ".$on."
//                                                LEFT JOIN tyr_jobs j ON c.studio_id = j.user_id
//                                                WHERE u.user_id = '".$this->session->userdata('user_id')."' AND j.job_id = '".$job_id."'");
                    $joins_obj = new Tyr_joins_entity();
                    $email_data = $joins_obj->get_email_data_for_tyroe($on, $job_id, $this->session->userdata('user_id'));

                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $tyroe_id;
                    $users_obj->get_user();
                    $tyroe_data = (array) $users_obj;
                    //$tyroe_data = $this->getRow("SELECT firstname, lastname FROM ".TABLE_USERS." WHERE user_id = '".$tyroe_id."'");

                    $jobs_obj = new Tyr_jobs_entity();
                    $jobs_obj->job_id = $job_id;
                    $jobs_obj->get_jobs();
                    $job_data = (array) $job_data;
                    $job_data['opening_title'] = $jobs_obj->job_title;
                    //$job_data = $this->getRow("SELECT job_title AS opening_title FROM ".TABLE_JOBS." WHERE job_id  = '".$job_id."'");

                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $this->session->userdata('user_id');
                    $users_obj->get_user();
                    $performer_name = array('username' => $users_obj->username);
                    //$performer_name = $this->getRow("SELECT username FROM ".TABLE_USERS." WHERE user_id=".$this->session->userdata('user_id'));

                    $activity_data = array(
                        "job_title" => $email_data['opening_title'],
                        "company_name" => $email_data['company_name'],
                        #Studio Name
                        "performer_name" => 'You',
                        "tyroe_name" => $tyroe_data['firstname'] . " " . $tyroe_data['lastname'],
                        "activity_sub_type" => LABEL_APPLIED_TO_CANDIDATE,
                        "search_keywords" => "added add invite invited job opening"
                    );

//                $job_activity = array(
//                    'performer_id' => $this->session->userdata('user_id'),
//                    'job_id' => $job_id,
//                    'object_id' => $tyroe_id,
//                    'activity_type' => $activity_type_key,
//                    'activity_data' => serialize($activity_data),
//                    'created_timestamp' => strtotime("now"),
//                    'status_sl' => '1'
//                );
//                $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $this->session->userdata('user_id');
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = $activity_type_key;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();
                } else {
                    #$activity_type_data ACTIVITY START
//                $query_activity = "SELECT
//                                ".TABLE_JOBS.".job_title,
//                                ".TABLE_USERS.".firstname,
//                                ".TABLE_USERS.".lastname,
//                                ".TABLE_USERS.".username,
//                                ".TABLE_USERS.".email
//                                FROM ".TABLE_JOB_DETAIL."
//                                LEFT JOIN " . TABLE_JOBS . "
//                                ON
//                                " . TABLE_JOBS . ".job_id = ".TABLE_JOB_DETAIL.".job_id
//                                LEFT JOIN ".TABLE_USERS."
//                                ON
//                                ".TABLE_USERS.".user_id = ".TABLE_JOB_DETAIL.".tyroe_id
//                                WHERE ".TABLE_JOB_DETAIL.".job_id=".$job_id." AND ".TABLE_JOB_DETAIL.".tyroe_id=".$tyroe_id." AND ".TABLE_JOB_DETAIL.".reviewer_id = ".$user_id;
//                $activity_data = $this->getRow($query_activity);

                    $joins_obj = new Tyr_joins_entity();
                    $activity_data = $joins_obj->get_job_activity_data($job_id, $user_id, $tyroe_id);

                    $this->set_activity_stream($user_id, $tyroe_id, $job_id, $this->session->userdata("role_id"), $activity_data, $activity_type_data);
                    #JOB ACTIVITY END
                }


                //email actvity
                //condition fo add review and update review
                //$check_notification_feedback_status = $this->getRow("SELECT * FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$tyroe_id."' AND option_id = 15");
                $schedule_notification_obj = new Tyr_schedule_notification_entity();
                $schedule_notification_obj->user_id = $tyroe_id;
                $schedule_notification_obj->option_id = 15;
                $check_notification_feedback_status = $schedule_notification_obj->get_schedule_notification_by_user_option();

                if ($check_notification_feedback_status['status_sl'] == 1) {
                    //System enter, If tyroe allow.
                    $email_to = $tyroe_id;
                    $job_title = $activity_data['job_title'];
//                $get_details = $this->getRow("SELECT email FROM " . TABLE_USERS . "
//                  WHERE " . TABLE_USERS . ".user_id='".$email_to."' AND " . TABLE_USERS . ".status_sl='1'");

                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $email_to;
                    $users_obj->get_user();
                    $get_details = array('email' => $users_obj->email);

                    if ($action == 'candidate_to_shortlist') {
                        $job_title = $email_data['job_title'];
                        $company_name = $email_data['company_name'];
                        $url = $this->getURL("opening/" . strtolower(str_replace(' ', '-', $job_title) . "-" . $this->generate_job_number($job_id)));

                        $notification_templates_obj = new Tyr_notification_templates_entity();
                        $notification_templates_obj->notification_id = 'n03';
                        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();
                        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n03'");

                        $subject = $get_message['subject'];
                        $subject = str_replace(array('[$job_title]', '[$company_name]', '[$url_viewshortlistjob]', '[$url_tyroe]'), array($job_title, $company_name, $url, LIVE_SITE_URL), $subject);
                        $message = htmlspecialchars_decode($get_message['template']);
                        $message = str_replace(array('[$job_title]', '[$company_name]', '[$url_viewshortlistjob]', '[$url_tyroe]'), array($job_title, $company_name, $url, LIVE_SITE_URL), $message);
                        #Email
                        /* $subject = LABEL_SHORTLIST_JOB_LOG_TEXT . " " . $email_data['job_title'] ." at ". $email_data['company_name'];
                          $message = "Hold on for the ride!  You have been shortlisted for a job. This is big news as you have been hand picked from a huge list of job candidates and are now in the final stage of the recruitment process.";
                          $message .= "<br>Tip: Why don’t you check out the companies website and think about some clever question you could ask if you land an interview.";
                          $message .= "<br><br>";
                          $message .= '<a href="' . $this->getURL("opening/".strtolower(str_replace(' ', '-', $email_data['job_title']) . "-" . $this->generate_job_number($job_id))) . '" class="" style=""><img src="'.ASSETS_PATH.'img/view-job-detail-btn.png"></a>';
                          $message .= "<br><br>";
                          $message .= "Keep up the great work!";
                          $message .= "<br><br>";
                          $message .= "<b>-Team Tyroe</b>"; */
                        $current_user_email = $get_details['email'];
                        $this->email_sender($current_user_email, $subject, $message);
                    } else {
                        /* $email_subject='You  '. $email_activity_data .' in ' . $job_title .' by  '.$this->session->userdata('firstname').' '.$this->session->userdata('lastname');
                          $email_message='You  '. $email_activity_data .' in ' . $job_title .' by  '.$this->session->userdata('firstname').' '.$this->session->userdata('lastname');
                          $to_email=$get_details['email'];
                          $email_success=$this->email_sender($to_email,$email_subject, $email_message); */
                    }
                }
            }
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
        return true;
    }

    //function for approve and delete anonymous feedback
    public function anonymous_feedback_action() {
        $review_id = $this->input->post('review_id');
        $action = $this->input->post('action');
        $tyroe_id = $this->input->post('tyroe_id');
        if ($action == 'approve_feedback') {
            $records = array(
                'anonymous_feedback_status' => 1
            );
            //$this->update(TABLE_RATE_REVIEW,$records,"review_id = '".$review_id."'");

            $rate_review_obj = new Tyr_rate_review_entity();
            $rate_review_obj->review_id = $review_id;
            $rate_review_obj->get_rate_review();
            $rate_review_obj->anonymous_feedback_status = 1;
            $rate_review_obj->update_rate_review();

            $email_to = $tyroe_id;
//            $get_details = $this->getRow("SELECT email FROM " . TABLE_USERS . "
//                      WHERE " . TABLE_USERS . ".user_id='".$email_to."' AND " . TABLE_USERS . ".status_sl='1'");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $email_to;
            $users_obj->get_user();
            $get_details = array('email' => $users_obj->email);

//            $get_anonymous_message = $this->getRow("SELECT " . TABLE_RATE_REVIEW . ".anonymous_feedback_message FROM " . TABLE_RATE_REVIEW . "
//                      WHERE " . TABLE_RATE_REVIEW . ".review_id='".$review_id."' AND " . TABLE_RATE_REVIEW . ".status_sl='1' AND " . TABLE_RATE_REVIEW . ".anonymous_feedback_status = 1");

            $rate_review_obj = new Tyr_rate_review_entity();
            $rate_review_obj->review_id = $review_id;
            $get_anonymous_message = $rate_review_obj->get_anonymous_message();

            $email_subject = 'from tyroe';
            $email_message = $get_anonymous_message['anonymous_feedback_message'];
            $to_email = $get_details['email'];
            $email_success = $this->email_sender($to_email, $email_subject, $email_message);
        } else if ($action == 'delete_feedback') {
//            $records = array(
//                'anonymous_feedback_status' => -1
//            );
//            $this->update(TABLE_RATE_REVIEW,$records,"review_id = '".$review_id."'");
            $rate_review_obj = new Tyr_rate_review_entity();
            $rate_review_obj->review_id = $review_id;
            $rate_review_obj->get_rate_review();
            $rate_review_obj->anonymous_feedback_status = -1;
            $rate_review_obj->update_rate_review();
        }
    }

    public function getreviewers() {
        if ($this->input->post('action') == 'review_by_reviewer') {
            //this for get one review by reviewer in tyror profile view
            $job_id = $this->input->post('job_id');
            $tyroe_id = $this->input->post('tyroe_id');
            $review_id = $this->session->userdata('user_id');

//            $tyroe_review = $this->getRow(
//                "SELECT * , ((SUM(" . TABLE_RATE_REVIEW . ".technical)+SUM(" . TABLE_RATE_REVIEW . ".creative)+SUM(" . TABLE_RATE_REVIEW . ".impression))/3) AS review_score
//            FROM " . TABLE_RATE_REVIEW . "
//            LEFT JOIN ".TABLE_JOB_LEVEL." ON ".TABLE_JOB_LEVEL.".job_level_id = ".TABLE_RATE_REVIEW.".level_id
//            LEFT JOIN ".TABLE_USERS." ON ".TABLE_USERS.".user_id = " . TABLE_RATE_REVIEW . ".tyroe_id
//            WHERE
//            " . TABLE_RATE_REVIEW . ".user_id = '".$review_id."' AND " . TABLE_RATE_REVIEW . ".job_id = '".$job_id."' AND " . TABLE_RATE_REVIEW . ".tyroe_id = '".$tyroe_id."' AND " . TABLE_RATE_REVIEW . ".status_sl = 1");

            $joins_obj = new Tyr_joins_entity();
            $tyroe_review = $joins_obj->get_tyroe_rate_review_detail($review_id, $job_id, $tyroe_id);

            $review_html = '<div class="row-fluid stats-row rt-bg-review">';
            $review_html .= '<div class="container rt-pd-container">';
            $review_html .= '<div class="span1 rt-center">';
            $review_html .= '<ul class="actions margin-default">';
            $review_html .= '<li><a href="javascript:void(0)" data-type="single_page_edit" class="edit_reviews" data-id="' . $tyroe_review['review_id'] . '" data-value="' . $tyroe_id . ',' . $tyroe_review['firstname'] . ' ' . $tyroe_review['lastname'] . ',' . $tyroe_review['level_id'] . ',' . $tyroe_review['technical'] . ',' . $tyroe_review['creative'] . ',' . $tyroe_review['impression'] . ',' . $tyroe_review['review_comment'] . ',' . $tyroe_review['action_shortlist'] . ',' . $tyroe_review['action_interview'] . '"><i class="table-edit"></i></a></li>';
            $review_html .= '<li class="last"><a href="javascript:void(0)" class="delete_tyroe_review" data-type="profile_view" data-id="' . $tyroe_review['review_id'] . ',' . $tyroe_id . '"><i class="table-delete"></i></a></li>';
            $review_html .= '</ul>';
            $review_html .= '</div>';
            $review_html .= '<div class="span10">';
            $review_html .= '<div class="span3 rt-center rt-st-sec">';
            if ($tyroe_review['action_shortlist'] != '') {
                $review_html .= '<span class="label inverse">' . $tyroe_review['action_shortlist'] . '</span>';
            }
            if ($tyroe_review['action_interview'] != '') {
                $review_html .= '<span class="label inverse">' . $tyroe_review['action_interview'] . '</span>';
            }
            $review_html .= '</div>';
            $review_html .= '<div class="span6 rt-text-center">';
            $review_html .= '<p><strong>' . ( ($tyroe_review['level_title'] == 'Intern') ? 'Entry Level' : $tyroe_review['level_title']) . ': </strong><span>' . $tyroe_review['review_comment'] . '</span></p>';
            $review_html .= '</div>';
            $review_html .= '<div class="span3 rt-center rt-score text-center"><span>' . round($tyroe_review['review_score']) . '</span></div>';
            $review_html .= '<div class="clearfix"></div>';
            $review_html .= '</div>';
            $review_html .= '</div>';
            $review_html .= '</div>';
        } else {
            //this for get all reviewer by studio
            // reiveiwers query
            $user_id = $this->session->userdata('user_id');
            $parent_id = $this->session->userdata('parent_id');
            if ($parent_id == 0) {
                $parent_id = intval($user_id);
            }

            $job_id = $this->input->post('job_id');
            $tyroe_id = $this->input->post('tyroe_id');
            
            $jobs_obj = new tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->get_jobs();
            $this->data['job_detail'] = (array) $jobs_obj;
            
            $this->job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
            $this->job_roles_mapping_obj->job_id = intval($job_id);
            $this->job_roles_mapping_obj->user_id = $user_id;
            $this->job_roles_mapping_obj->get_job_role_mapping();
            $this->data['job_role'] = (array) $this->job_roles_mapping_obj;

//            $query = "SELECT
//                              ".TABLE_USERS.".user_id,
//                              ".TABLE_USERS.".firstname,
//                              ".TABLE_USERS.".lastname,
//                              ".TABLE_USERS.".parent_id,
//                              ".TABLE_USERS.".user_occupation,
//                              ".TABLE_USERS.".job_title,
//                              ".TABLE_RATE_REVIEW.".job_id,
//                              ".TABLE_RATE_REVIEW.".action_shortlist,
//                              ".TABLE_RATE_REVIEW.".action_interview,
//                              ".TABLE_RATE_REVIEW.".level_id,
//                              ".TABLE_RATE_REVIEW.".review_comment,
//                              ".TABLE_JOB_LEVEL.".level_title,
//                              ".TABLE_MEDIA.".media_type,
//                              ".TABLE_MEDIA.".media_name,
//                              AVG(
//                                (
//                                  ".TABLE_RATE_REVIEW.".technical + ".TABLE_RATE_REVIEW.".impression + ".TABLE_RATE_REVIEW.".creative
//                                ) / 3
//                              ) AS review_average
//                            FROM
//                              tyr_reviewer_jobs
//                              LEFT JOIN ".TABLE_USERS."
//                                ON ".TABLE_USERS.".user_id = ".TABLE_REVIEWER_JOBS.".reviewer_id
//                              LEFT JOIN ".TABLE_RATE_REVIEW."
//                                ON ".TABLE_RATE_REVIEW.".user_id = ".TABLE_USERS.".user_id
//                                AND ".TABLE_RATE_REVIEW.".job_id = ".TABLE_REVIEWER_JOBS.".job_id
//                                AND ".TABLE_RATE_REVIEW.".tyroe_id = '".$tyroe_id."'
//                                AND ".TABLE_RATE_REVIEW.".status_sl = 1
//                                LEFT JOIN ".TABLE_JOB_LEVEL."
//                               ON ".TABLE_JOB_LEVEL.".job_level_id = ".TABLE_RATE_REVIEW.".level_id
//                              LEFT JOIN tyr_media
//                                ON ".TABLE_MEDIA.".user_id = ".TABLE_USERS.".user_id
//                                AND ".TABLE_MEDIA.".profile_image = '1'
//                                AND ".TABLE_MEDIA.".status_sl = '1'
//                            WHERE ".TABLE_REVIEWER_JOBS.".studio_id = '".$user_id."'
//                               AND ".TABLE_REVIEWER_JOBS.".job_id = '".$job_id."' AND ".TABLE_REVIEWER_JOBS.".status_sl = 1
//                            GROUP BY ".TABLE_REVIEWER_JOBS.".reviewer_id ";
            // $get_data   = $this->getAll($query);

            $joins_obj = new Tyr_joins_entity();
            $get_data = $joins_obj->get_all_reviewer_detail_by_studio($tyroe_id, $job_id, $user_id);

            foreach ($get_data as $i => $value) {
                if ($value['user_id'] == $this->session->userdata('user_id')) {
                    $joins_obj = new Tyr_joins_entity();
                    $get_review = $joins_obj->get_review_details($job_id, $tyroe_id, $user_id);
                    $get_data[$i]['reviewer_review'] = $get_review;
                    $get_data[$i]['reviewer_review']['candidate_id'] = $tyroe_id;
                }
            }

            $this->data['get_reviewers'] = $get_data;
            $this->template_arr = array('contents/getreviewrs');
            $this->load_template();
        }
    }

    public function generate_pdf_file() {
        //condition for delete file
        if ($this->input->post('file_path') != '') {
            $file = $this->input->post('file_path');
            $file_len = strlen($file);
            $file = substr($file, 1, $file_len);
            echo $file;
            unlink($file);
        } else {
            $user_id = $this->session->userdata("user_id");
            $job_id = $this->input->post('job_id');
            //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
            $job_id = 10;
            $hidden_obj = new Tyr_hidden_entity();
            $hidden_obj->job_id = $job_id;
            $hidden_obj->studio_id = $user_id;
            $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

            $hidden_user = 0;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            }

//            $get_shortlist = $this->getAll("SELECT ".TABLE_USERS.".user_id,firstname,lastname,availability,email,".TABLE_COUNTRIES.".country," . TABLE_CITIES . ".city,".TABLE_JOB_DETAIL.".job_detail_id,".TABLE_JOB_DETAIL.".invitation_status," . TABLE_MEDIA . ".media_type,
//                        " . TABLE_MEDIA . ".media_name from ".TABLE_USERS."
//                        LEFT JOIN ".TABLE_COUNTRIES.
//                " ON ".TABLE_COUNTRIES.".country_id=".TABLE_USERS.".country_id
//                LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
//                LEFT JOIN " . TABLE_MEDIA . "
//                ON " . TABLE_MEDIA . ".user_id = ". TABLE_USERS .".user_id
//                AND " . TABLE_MEDIA . ".profile_image = '1'
//                AND " . TABLE_MEDIA . ".status_sl = '1'
//                LEFT JOIN ".TABLE_JOB_DETAIL." ON ".TABLE_USERS.".user_id=".TABLE_JOB_DETAIL.
//                ".tyroe_id AND ".TABLE_JOB_DETAIL.".job_status_id = 3  WHERE ".TABLE_JOB_DETAIL.".job_id='".$job_id."'  AND ".TABLE_USERS.".publish_account = '1' AND ".TABLE_JOB_DETAIL.".status_sl=1 AND ".TABLE_JOB_DETAIL.".reviewer_id = '".$user_id."' AND ".TABLE_JOB_DETAIL.".tyroe_id NOT IN (" . $hidden_user . ")");

            $joins_obj = new Tyr_joins_entity();
            $get_shortlist = $joins_obj->get_all_shotlist_for_pdf($job_id, $user_id, $hidden_user);

            if ($get_shortlist != NULL) {
                for ($i = 0; $i < count($get_shortlist); $i++) {

                    $joins_obj = new Tyr_joins_entity();
                    $get_shortlist[$i]['skills'] = $joins_obj->get_all_user_skill($get_shortlist[$i]['user_id']);

//                    $get_shortlist[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                                 FROM " . TABLE_SKILLS . " LEFT JOIN
//                                 " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                                 WHERE user_id='" . $get_shortlist[$i]['user_id'] . "'");
                    #Portfolio/Media
                    //$get_shortlist[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_shortlist[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                    $media_obj = new Tyr_media_entity();
                    $media_obj->user_id = $get_shortlist[$i]['user_id'];
                    $get_shortlist[$i]['portfolio'] = $media_obj->get_user_short_portfolio();


                    #get reviewers
                    $joins_obj = new Tyr_joins_entity();
                    $get_shortlist[$i]['reviewers'] = $joins_obj->get_all_reviewer_detail_by_studio($get_shortlist[$i]['user_id'], $job_id, $user_id);

//                    $get_shortlist[$i]['reviewers'] = $this->getAll("SELECT
//                              ".TABLE_USERS.".user_id,
//                              ".TABLE_USERS.".firstname,
//                              ".TABLE_USERS.".lastname,
//                              ".TABLE_USERS.".parent_id,
//                              ".TABLE_USERS.".user_occupation,
//                              ".TABLE_RATE_REVIEW.".job_id,
//                              ".TABLE_RATE_REVIEW.".action_shortlist,
//                              ".TABLE_RATE_REVIEW.".action_interview,
//                              ".TABLE_RATE_REVIEW.".level_id,
//                              ".TABLE_RATE_REVIEW.".review_comment,
//                              ".TABLE_JOB_LEVEL.".level_title,
//                              ".TABLE_MEDIA.".media_type,
//                              ".TABLE_MEDIA.".media_name,
//                              AVG(
//                                (
//                                  ".TABLE_RATE_REVIEW.".technical + ".TABLE_RATE_REVIEW.".impression + ".TABLE_RATE_REVIEW.".creative
//                                ) / 3
//                              ) AS review_average
//                            FROM
//                              tyr_reviewer_jobs
//                              LEFT JOIN ".TABLE_USERS."
//                                ON ".TABLE_USERS.".user_id = ".TABLE_REVIEWER_JOBS.".reviewer_id
//                              LEFT JOIN ".TABLE_RATE_REVIEW."
//                                ON ".TABLE_RATE_REVIEW.".user_id = ".TABLE_USERS.".user_id
//                                AND ".TABLE_RATE_REVIEW.".job_id = ".TABLE_REVIEWER_JOBS.".job_id
//                                AND ".TABLE_RATE_REVIEW.".tyroe_id = '".$get_shortlist[$i]['user_id']."'
//                                AND ".TABLE_RATE_REVIEW.".status_sl = 1
//                                LEFT JOIN ".TABLE_JOB_LEVEL."
//                               ON ".TABLE_JOB_LEVEL.".Candidates_id = ".TABLE_RATE_REVIEW.".level_id
//                              LEFT JOIN tyr_media
//                                ON ".TABLE_MEDIA.".user_id = ".TABLE_USERS.".user_id
//                                AND ".TABLE_MEDIA.".profile_image = '1'
//                                AND ".TABLE_MEDIA.".status_sl = '1'
//                            WHERE ".TABLE_REVIEWER_JOBS.".studio_id = '".$user_id."'
//                               AND ".TABLE_REVIEWER_JOBS.".job_id = '".$job_id."'
//                            GROUP BY ".TABLE_REVIEWER_JOBS.".reviewer_id ");
                    //$is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '".$get_shortlist[$i]['user_id']."'");
                    //$get_shortlist[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                    $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                    $featured_tyroes_obj->featured_tyroe_id = $get_shortlist[$i]['user_id'];
                    $featured_tyroes_obj->get_user_featured_status();
                    $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                    $get_shortlist[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                }
            }

            $css_array = array(
                ASSETS_PATH . 'css/bootstrap/bootstrap.css',
                ASSETS_PATH . 'css/bootstrap/bootstrap-responsive.css',
                ASSETS_PATH . 'css/bootstrap/bootstrap-overrides.css',
                ASSETS_PATH . 'css/custom.css',
                ASSETS_PATH . 'css/layout.css',
                ASSETS_PATH . 'css/smart-grid.css'
            );
            $pdf_data['get_shortlist'] = $get_shortlist;
            $pdf_data['css_files'] = $css_array;
            $pdf_html = $this->load->view('contents/get_shortlistpdfhtml', $pdf_data, true);
            $filename = $job_id . "_shortlist_pdf.pdf";
            $file_directory = "/uploads/" . $filename;
            $pdfFilePath = FCPATH . $file_directory;
            if (file_exists($pdfFilePath) == FALSE) {
                try {
                    ini_set('memory_limit', '32M');
                    $this->load->library('pdf');
                    $pdf = $this->pdf->load();
                    $pdf->SetFooter($_SERVER['HTTP_HOST'] . '|{PAGENO}|' . date(DATE_RFC822));
                    $pdf->WriteHTML($pdf_html);
                    $pdf->Output($pdfFilePath, 'F');
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
            echo $file_directory;
        }
    }

    public function get_csvfile() {
        $job_id = $this->input->post('job_id');
        $user_id = $this->session->userdata("user_id");
        $filename = $job_id . ' shortlist_csv' . date('D / M / Y', time());
//        $hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$job_id."' GROUP BY studio_id");
//        $hidden_user;
//        if (is_array($hiden_tyroes)) {
//            $hidden_user = $hiden_tyroes['user_id'];
//        } else {
//            $hidden_user = 0;
//        }

        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->job_id = $job_id;
        $hidden_obj->studio_id = $user_id;
        $hiden_tyroes = $hidden_obj->get_hidden_tyroes();
        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['user_id'];
        }

//       $query = "SELECT *
//        from ".TABLE_USERS."
//            LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".user_id = '".$user_id."'
//            AND ".TABLE_JOBS.".job_id = '".$job_id."'
//            LEFT JOIN ".TABLE_JOB_DETAIL." ON ".TABLE_USERS.".user_id=".TABLE_JOB_DETAIL.
//            ".tyroe_id AND ".TABLE_JOB_DETAIL.".job_status_id = 3
//            WHERE
//            ".TABLE_JOB_DETAIL.".job_id='".$job_id."'
//            AND ".TABLE_JOB_DETAIL.".status_sl=1
//            AND ".TABLE_JOB_DETAIL.".reviewer_id = '".$user_id."'
//            AND ".TABLE_JOB_DETAIL.".tyroe_id NOT IN (" . $hidden_user . ")";

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename . '.csv');
        $output = fopen('php://output', 'w');
        // $query_result = $this->db->query($query);

        $columns = array();
        //$get_csv_info = $this->getAll($query);
        $joins_obj = new Tyr_joins_entity();
        $get_csv_info = $joins_obj->get_csv_info($user_id, $job_id, $hidden_user);


        for ($i = 0; $i < count($get_csv_info); $i++) {
//            $query_inner = "SELECT
//                        ". TABLE_RATE_REVIEW .".review_id,
//                        ". TABLE_RATE_REVIEW .".user_id,
//                        ". TABLE_RATE_REVIEW .".user_type,
//                        ". TABLE_RATE_REVIEW .".tyroe_id,
//                        ". TABLE_RATE_REVIEW .".job_id,
//                        ". TABLE_RATE_REVIEW .".level_id,
//                        ". TABLE_RATE_REVIEW .".technical,
//                        ". TABLE_RATE_REVIEW .".creative,
//                        ". TABLE_RATE_REVIEW .".impression,
//                        ". TABLE_RATE_REVIEW .".review_comment,
//                        ". TABLE_RATE_REVIEW .".action_shortlist,
//                        ". TABLE_RATE_REVIEW .".action_interview,
//                        ". TABLE_RATE_REVIEW .".created_timestamp,
//                        ". TABLE_RATE_REVIEW .".status_sl
//                        FROM
//                        ". TABLE_USERS ."
//                        LEFT JOIN ". TABLE_RATE_REVIEW ." ON
//                        ". TABLE_RATE_REVIEW .".user_id = ". TABLE_USERS .".user_id
//                            AND
//                            ". TABLE_RATE_REVIEW .".job_id = ".$job_id."
//                            AND
//                            ". TABLE_RATE_REVIEW .".user_id = ".$get_csv_info[$i]['user_id']."
//                        WHERE
//                          (". TABLE_USERS .".parent_id = ".$user_id." OR ". TABLE_USERS .".user_id = ".$user_id.")
//                          AND ". TABLE_USERS .".status_sl = 1  AND ".TABLE_USERS.".publish_account = '1'
//                          GROUP BY ". TABLE_USERS .".user_id";
//            $csv_reviews = $this->getAll($query_inner);

            $ccv_user_id = $get_csv_info[$i]['user_id'];
            $joins_obj = new Tyr_joins_entity();
            $csv_reviews = $joins_obj->get_csv_reviews($user_id, $job_id, $ccv_user_id);

            $count = 0;
            for ($j = 0; $j < count($csv_reviews); $j++) {
                $count++;
                foreach ($csv_reviews[$j] as $r_key => $r_val) {
                    $get_csv_info[$i][$r_key . $count] = $r_val;
                }
            }
            foreach ($get_csv_info[$i] as $get_csv => $val) {
                if ($val == '') {
                    $val = 'NULL';
                }
                $columns[] = $get_csv;
                $get_csv_info[$i][$get_csv] = $val;
            }
            if ($i == 0) {
                fputcsv($output, $columns);
            }
            fputcsv($output, $get_csv_info[$i]);
        }
    }

    //reviewer function
    public function add_reviewer_review() {
        $studio_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $studio_id = $this->session->userdata("parent_id");
        }

        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $this->session->userdata('user_id');
        $users_obj->get_user();
        $reviewer_name = $tyroe_detail = (array) $users_obj;

        if ($this->input->post('reviewed_action') == 'decline_cadidate') {

            $tyroe_id = $this->input->post('tyroe_id');
            $job_id = $this->input->post('job_id');
            $user_id = $this->session->userdata("user_id");

            $records = array(
                'status_sl' => -1
            );
            $activity_type_data = LABEL_DECLINE_CANDIDATE;

            $job_details = new Tyr_job_detail_entity();
            $job_details->tyroe_id = $tyroe_id;
            $job_details->job_id = $job_id;
            $job_details->status_sl = -1;
            $job_details->update_job_detail_status();

            //$this->update(TABLE_JOB_DETAIL,$records,"tyroe_id = '".$tyroe_id."' AND job_id = '".$job_id."'");
            //creating reviewed status array
            $review_status_arr = array(
                'studio_id' => $studio_id,
                'job_id' => $job_id,
                'reviewer_id' => $user_id,
                'tyroe_id' => $tyroe_id,
                'reviewed_type' => 'decline',
                'reviewed_time' => time(),
                'status_sl' => '1'
            );
            //$this->insert(TABLE_REVIEWER_REVIEWS,$review_status_arr);

            $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
            $reviewer_reviews_obj->studio_id = $studio_id;
            $reviewer_reviews_obj->job_id = $job_id;
            $reviewer_reviews_obj->reviewer_id = $user_id;
            $reviewer_reviews_obj->tyroe_id = $tyroe_id;
            $reviewer_reviews_obj->reviewed_type = 'decline';
            $reviewer_reviews_obj->reviewed_time = time();
            $reviewer_reviews_obj->status_sl = 1;
            $reviewer_reviews_obj->save_reviewer_reviews();

            /*             * *****Set Activity Stream --  START ****** */
//            $query_activity = "SELECT
//                                ".TABLE_JOBS.".job_title,
//                                ".TABLE_USERS.".firstname,
//                                ".TABLE_USERS.".lastname,
//                                ".TABLE_USERS.".username,
//                                ".TABLE_USERS.".email
//                                FROM ".TABLE_JOB_DETAIL."
//                                LEFT JOIN " . TABLE_JOBS . "
//                                ON
//                                " . TABLE_JOBS . ".job_id = ".TABLE_JOB_DETAIL.".job_id
//                                LEFT JOIN ".TABLE_USERS."
//                                ON
//                                ".TABLE_USERS.".user_id = ".TABLE_JOB_DETAIL.".tyroe_id
//                                WHERE ".TABLE_JOB_DETAIL.".job_id=".$job_id." AND ".TABLE_JOB_DETAIL.".tyroe_id=".$tyroe_id." AND ".TABLE_JOB_DETAIL.".reviewer_id = ".$user_id;
//            $activity_data = $this->getRow($query_activity);
            #Reviewer
            $activity_type = LABEL_APPLY_JOB_LOG;

            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $tyroe_id;
            $users_obj->get_user();
            $tyroe_detail = (array) $users_obj;

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->get_jobs();
            $job_detail = (array) $jobs_obj;

            //$tyroe_detail = $this->getRow("SELECT firstname,lastname,email FROM ".TABLE_USERS." WHERE user_id = '".$tyroe_id."'");
            //$job_detail = $this->getRow("SELECT job_title FROM ".TABLE_JOBS." WHERE job_id = '".$job_id."'");


            $activity_data_reviewer = array(
                "tyroe_name" => $tyroe_detail['firstname'] . " " . $tyroe_detail['lastname'],
                "job_opening_title" => $job_detail['job_title'],
                "activity_sub_type" => LABEL_DECLINED_ACTIVITY_NAME,
                "job_id" => $job_id,
                "search_keywords" => "decline declined reject rejected"
            );
//            $job_activity_reviewer = array(
//                'performer_id' => $user_id,
//                'job_id' => $job_id,
//                'object_id' => $tyroe_id,
//                'activity_type' => $activity_type,
//                'activity_data' => serialize($activity_data_reviewer),
//                'created_timestamp' => strtotime("now"),
//                'status_sl' => '1'
//            );
//            $this->insert(TABLE_JOB_ACTIVITY, $job_activity_reviewer);

            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $user_id;
            $job_activity_obj->job_id = $job_id;
            $job_activity_obj->object_id = $tyroe_id;
            $job_activity_obj->activity_type = $activity_type;
            $job_activity_obj->activity_data = serialize($activity_data_reviewer);
            $job_activity_obj->status_sl = 1;
            $job_activity_obj->created_timestamp = strtotime("now");
            $job_activity_obj->save_job_activity();

            /*             * *****Set Activity Stream --  END ****** */

            //$this->set_activity_stream($user_id, $tyroe_id, $job_id, $this->session->userdata("role_id"), $activity_data, $activity_type_data);
        } else if ($this->input->post('reviewed_action') == 'delete_tyroe_review') {
            $tyroe_id = $this->input->post('tyroe_id');
            $job_id = $this->input->post('job_id');
            $review_id = $this->input->post('review_id');
            $user_id = $this->session->userdata("user_id");
            //$studio_id = $this->session->userdata("parent_id");
            $records = array(
                'status_sl' => -1
            );
            //$this->update(TABLE_REVIEWER_REVIEWS,$records,"tyroe_id = '".$tyroe_id."' AND job_id = '".$job_id."' AND studio_id = '".$studio_id."' AND reviewer_id = '".$user_id."'");
            //$this->update(TABLE_RATE_REVIEW,$records,array('review_id' => $review_id));

            $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
            $reviewer_reviews_obj->tyroe_id = $tyroe_id;
            $reviewer_reviews_obj->job_id = $job_id;
            $reviewer_reviews_obj->studio_id = $studio_id;
            $reviewer_reviews_obj->reviewer_id = $user_id;
            $reviewer_reviews_obj->status_sl = -1;
            $reviewer_reviews_obj->update_reviewer_reviews_status();

            $rate_review_obj = new Tyr_rate_review_entity();
            $rate_review_obj->review_id = $review_id;
            $rate_review_obj->get_rate_review();
            $rate_review_obj->status_sl = -1;
            $rate_review_obj->update_rate_review();
        } else {
            $tyroe_level = $this->input->post('tyroe_level');
            $tyroe_comment = $this->input->post('tyroe_comment');
            $action_shortlist = $this->input->post('action_shortlist');
            $action_interview = $this->input->post('action_interview');
            $anonymous_message = $this->input->post('anonymous_message');
            $tyroe_id = $this->input->post('tyroe_id');
            $technical_value = $this->input->post('technical_value');
            $creative_value = $this->input->post('creative_value');
            $impression_value = $this->input->post('impression_value');
            //$studio_id = $this->session->userdata("parent_id");
            $job_id = $this->input->post('job_id');
            $reviewer_id = $this->session->userdata("user_id");
            $reviewer_role = $this->session->userdata("role_id");
            $time = time();
            $table = TABLE_RATE_REVIEW;
            $status_sl = 1;
            $review_moderated = 1;
            if ($this->input->post('reviewed') == 'reviewed') {
                $table_data = array(
                    'user_id' => $reviewer_id,
                    'user_type' => $reviewer_role,
                    'tyroe_id' => $tyroe_id,
                    'job_id' => $job_id,
                    'level_id' => $tyroe_level,
                    'technical' => $technical_value,
                    'creative' => $creative_value,
                    'impression' => $impression_value,
                    'review_comment' => $tyroe_comment,
                    'action_shortlist' => $action_shortlist,
                    'action_interview' => $action_interview,
                    'status_sl' => $status_sl
                );
            } else {
                //check moderation
                //$check_moderation_status = $this->getRow("SELECT team_moderation FROM ".TABLE_JOBS." WHERE job_id = '".$job_id."' AND status_sl = 1");

                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->status_sl = 1;
                $check_moderation_status = $jobs_obj->check_moderation_status();
                if ($check_moderation_status['team_moderation'] == 1) {
                    $anonymous_feedback_status = 1;
                    $review_moderated = 0;
                } else {
                    $anonymous_feedback_status = 1;
                    $email_to = $tyroe_id;

//                    $get_details = $this->getRow("SELECT user_id, email FROM " . TABLE_USERS . "
//                      WHERE " . TABLE_USERS . ".user_id='".$email_to."' AND " . TABLE_USERS . ".status_sl='1'");
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $email_to;
                    $users_obj->status_sl = 1;
                    $users_obj->get_user_by_status();
                    $get_details = (array) $users_obj;

                    $email_subject = 'from tyroe';
                    $email_message = $anonymous_message;
                    $to_email = $get_details['email'];
                    if ($anonymous_message != '') {
                        //$email_success = $this->email_sender($to_email, $email_subject, $email_message);
                    }
                }

                $table_data = array(
                    'user_id' => $reviewer_id,
                    'user_type' => $reviewer_role,
                    'tyroe_id' => $tyroe_id,
                    'job_id' => $job_id,
                    'level_id' => $tyroe_level,
                    'technical' => $technical_value,
                    'creative' => $creative_value,
                    'impression' => $impression_value,
                    'review_comment' => $tyroe_comment,
                    'action_shortlist' => $action_shortlist,
                    'action_interview' => $action_interview,
                    'anonymous_feedback_message' => $anonymous_message,
                    'anonymous_feedback_status' => $anonymous_feedback_status,
                    'moderated' => $review_moderated,
                    'created_timestamp' => $time,
                    'status_sl' => $status_sl
                );
                //creating reviewed status array
                $review_status_arr = array(
                    'studio_id' => $studio_id,
                    'job_id' => $job_id,
                    'reviewer_id' => $reviewer_id,
                    'tyroe_id' => $tyroe_id,
                    'reviewed_type' => 'review',
                    'reviewed_time' => time(),
                    'status_sl' => '1'
                );

                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->studio_id = $studio_id;
                $company_detail_obj->get_company_detail_by_studio();
                $company_name = $company_detail_obj->company_name;

                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $tyroe_id;
                $users_obj->get_user();
                $tyroe_detail = (array) $users_obj;

                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->get_jobs();
                $job_detail = (array) $jobs_obj;
                $job_opening_title = $job_detail['job_title'];
            }

            //condition fo add review and update review
            //$check_notification_feedback_status = $this->getRow("SELECT * FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$tyroe_id."' AND option_id = 16");

            $schedule_notification_obj = new Tyr_schedule_notification_entity();
            $schedule_notification_obj->user_id = $tyroe_id;
            $schedule_notification_obj->option_id = 16;
            $check_notification_feedback_status = $schedule_notification_obj->get_schedule_notification_by_user_option();

            //$get_job_title = $this->getRow("SELECT * FROM ".TABLE_JOBS." WHERE job_id = '".$job_id."'");

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->get_jobs();
            $get_job_title = (array) $jobs_obj;

            if ($this->input->post('reviewed') == 'reviewed') {
                if ($check_notification_feedback_status['status_sl'] == 1) {
                    $email_to = $tyroe_id;
                    $job_title = $get_job_title['job_title'];

//                    $get_details = $this->getRow("SELECT email FROM " . TABLE_USERS . "
//                      WHERE " . TABLE_USERS . ".user_id='".$email_to."' AND " . TABLE_USERS . ".status_sl='1'");

                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $email_to;
                    $users_obj->status_sl = 1;
                    $users_obj->get_user_by_status();
                    $get_details = (array) $users_obj;

                    $email_subject = 'review update in ' . $job_title . ' by  ' . $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname');
                    $email_message = 'You have reviewed update in ' . $job_title . ' by ' . $this->session->userdata('firstname') . ' ' . $this->session->userdata('lastname');
                    $to_email = $get_details['email'];
                    $email_success = $this->email_sender($to_email, $email_subject, $email_message);
                }

                //$this->update(TABLE_RATE_REVIEW,$table_data,"review_id = '".$this->input->post('review_id')."'");
                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->review_id = $this->input->post('review_id');
                $rate_review_obj->get_rate_review();

                $rate_review_obj->user_id = $reviewer_id;
                $rate_review_obj->user_type = $reviewer_role;
                $rate_review_obj->tyroe_id = $tyroe_id;
                $rate_review_obj->job_id = $job_id;
                $rate_review_obj->level_id = $tyroe_level;
                $rate_review_obj->technical = $technical_value;
                $rate_review_obj->creative = $creative_value;
                $rate_review_obj->impression = $impression_value;
                $rate_review_obj->review_comment = $tyroe_comment;
                $rate_review_obj->action_shortlist = $action_shortlist;
                $rate_review_obj->action_interview = $action_interview;
                $rate_review_obj->status_sl = 1;
                $rate_review_obj->created_timestamp = $time;
                $rate_review_obj->update_rate_review();

                echo json_encode(array('success' => true, 'success_message' => MESSAGE_REVIEW_UPDATE));
            } else {
                //check review already exists

                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->tyroe_id = $tyroe_id;
                $rate_review_obj->user_id = $reviewer_id;
                $rate_review_obj->job_id = $job_id;
                $rate_review_obj->status_sl = 1;
                $check = $rate_review_obj->get_tyroe_rate_review();

                if (empty($check)) {
                    if ($check_notification_feedback_status['status_sl'] == 1) {
                        $email_to = $tyroe_id;
                        $job_opening_title = $get_job_title['job_title'];

                        $users_obj = new Tyr_users_entity();
                        $users_obj->user_id = $email_to;
                        $users_obj->status_sl = 1;
                        $users_obj->get_user_by_status();
                        $get_details = (array) $users_obj;

                        /* Email To Tyroe and Insert Stream on behalf of Tyore */
                        if ($this->session->userdata('role_id') == '3') {
                            $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                        } else if ($this->session->userdata('role_id') == '4') {
                            $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                        }

                        $joins_obj = new Tyr_joins_entity();
                        $email_data = $joins_obj->get_email_data_for_tyroe_1($on, $this->session->userdata('user_id'));
                    }

                    //$tyroe_detail = $this->getRow("SELECT firstname,lastname,email FROM ".TABLE_USERS." WHERE user_id = '".$tyroe_id."'");
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $tyroe_id;
                    $users_obj->get_user();
                    $tyroe_detail = (array) $users_obj;


                    //$current_rev_id = $this->insert($table, $table_data);
                    $rate_review_obj = new Tyr_rate_review_entity();
                    $rate_review_obj->user_id = $reviewer_id;
                    $rate_review_obj->user_type = $reviewer_role;
                    $rate_review_obj->tyroe_id = $tyroe_id;
                    $rate_review_obj->job_id = $job_id;
                    $rate_review_obj->level_id = $tyroe_level;
                    $rate_review_obj->technical = $technical_value;
                    $rate_review_obj->creative = $creative_value;
                    $rate_review_obj->impression = $impression_value;
                    $rate_review_obj->review_comment = $tyroe_comment;
                    $rate_review_obj->action_shortlist = $action_shortlist;
                    $rate_review_obj->action_interview = $action_interview;
                    $rate_review_obj->anonymous_feedback_message = $anonymous_message;
                    $rate_review_obj->anonymous_feedback_status = $anonymous_feedback_status;
                    $rate_review_obj->status_sl = 1;
                    $rate_review_obj->created_timestamp = $time;
                    $rate_review_obj->moderated = $review_moderated;
                    $rate_review_obj->save_rate_review();
                    $current_rev_id = $rate_review_obj->review_id;

                    //$this->insert(TABLE_REVIEWER_REVIEWS,$review_status_arr);
                    $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                    $reviewer_reviews_obj->studio_id = $studio_id;
                    $reviewer_reviews_obj->job_id = $job_id;
                    $reviewer_reviews_obj->reviewer_id = $reviewer_id;
                    $reviewer_reviews_obj->tyroe_id = $tyroe_id;
                    $reviewer_reviews_obj->reviewed_type = 'review';
                    $reviewer_reviews_obj->reviewed_time = time();
                    $reviewer_reviews_obj->status_sl = 1;
                    $reviewer_reviews_obj->save_reviewer_reviews();


                    #Tyroe
                    if ($review_moderated == 1) {
                        $activity_type = 3; // Feedback
                        $activity_data_tyroe = array(
                            "reviewer_job_title" => $reviewer_name['firstname'] . " " . $reviewer_name['lastname'],
                            "reviewer_company_name" => $company_name,
                            "rev_id" => $current_rev_id,
                            "rev_name" => $reviewer_name['firstname'] . " " . $reviewer_name['lastname'],
                            "search_keywords" => "feedback comment reviewed"
                        );

                        $job_activity_obj1 = new Tyr_job_activity_entity();
                        $job_activity_obj1->performer_id = $tyroe_id;
                        $job_activity_obj1->job_id = $job_id;
                        $job_activity_obj1->object_id = $tyroe_id;
                        $job_activity_obj1->activity_type = $activity_type;
                        $job_activity_obj1->activity_data = serialize($activity_data_tyroe);
                        $job_activity_obj1->status_sl = 1;
                        $job_activity_obj1->created_timestamp = strtotime("now");
                        $job_activity_obj1->save_job_activity();
                    }

                    #Studio
                    $activity_data_studio = array(
                        "reviewer_name" => $reviewer_name['firstname'] . " " . $reviewer_name['lastname'],
                        "tyroe_name" => $tyroe_detail['firstname'] . " " . $tyroe_detail['lastname'],
                        "job_opening_title" => $job_opening_title,
                        "activity_sub_type" => LABEL_REVIEWED_ACTIVITY_NAME,
                        "rev_id" => $current_rev_id,
                        "job_id" => $job_id,
                        "search_keywords" => "review reviewed"
                    );

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $studio_id;
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = $activity_type;
                    $job_activity_obj->activity_data = serialize($activity_data_studio);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();

                    #Reviewer
                    $activity_data_reviewer = array(
                        "tyroe_name" => $tyroe_detail['firstname'] . " " . $tyroe_detail['lastname'],
                        "job_opening_title" => $job_opening_title,
                        "activity_sub_type" => LABEL_REVIEWED_ACTIVITY_NAME,
                        "rev_id" => $current_rev_id,
                        "job_id" => $job_id,
                        "search_keywords" => "review reviewed"
                    );

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $this->session->userdata("user_id");
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $tyroe_id;
                    $job_activity_obj->activity_type = $activity_type;
                    $job_activity_obj->activity_data = serialize($activity_data_reviewer);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();

                    /*                     * *****Set Activity Stream --  END ****** */

                    //$check_notification_feedback_status = $this->getRow("SELECT * FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$tyroe_id."' AND option_id = 16");
                    $schedule_notification_obj = new Tyr_schedule_notification_entity();
                    $schedule_notification_obj->user_id = $tyroe_id;
                    $schedule_notification_obj->option_id = 16;
                    $check_notification_feedback_status = $schedule_notification_obj->get_schedule_notification_by_user_option();


                    if ($check_notification_feedback_status['status_sl'] == 1 && $review_moderated == 1) {
                        #Email
                        $job_title = $job_opening_title;
                        $reviewer_job_title = $reviewer_name['job_title'];
                        $reviewer_full_name = $reviewer_name['firstname'] . " " . $reviewer_name['lastname'];
                        $company_name = $email_data['company_name'];
                        $url = $this->getURL("feedback/" . strtolower(str_replace(' ', '-', $reviewer_full_name . "-" . $this->generate_job_number($current_rev_id))));

                        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n04'");
                        $notification_templates_obj = new Tyr_notification_templates_entity();
                        $notification_templates_obj->notification_id = 'n04';
                        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                        $subject = $get_message['subject'];
                        $subject = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $subject);
                        $message = htmlspecialchars_decode($get_message['template']);
                        $message = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $message);
                        $current_user_email = $tyroe_detail['email'];
                        $this->email_sender($current_user_email, $subject, $message);
                    }

                    if (true) {
                        //get all admins for this jobs and email them
                        $job_admins_obj = new Tyr_job_role_mapping_entity();
                        $job_admins_obj->job_id = $job_id;
                        $job_admins = $job_admins_obj->get_all_admins_for_job();

                        $tyr_users_obj = new Tyr_users_entity();
                        $admins_details = $tyr_users_obj->get_users_details($job_admins['user_id']);

                        foreach ($admins_details as $admin_detail) {
                            #Email
                            $job_title = $job_opening_title;
                            $reviewer_job_title = $reviewer_name['job_title'];
                            $reviewer_full_name = $reviewer_name['firstname'] . " " . $reviewer_name['lastname'];
                            $company_name = $email_data['company_name'];
                            $job_title_url = $this->cleanString($job_opening_title) . '-' . $this->generate_job_number($job_id);
                            $url = $this->getURL("openings/" . $job_title_url . '/review');

                            //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n04'");
                            $notification_templates_obj = new Tyr_notification_templates_entity();
                            $notification_templates_obj->notification_id = 'n22';
                            $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                            $subject = $get_message['subject'];
                            $subject = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $subject);
                            $message = htmlspecialchars_decode($get_message['template']);
                            $message = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $message);
                            $current_user_email = $admin_detail['email'];
                            $this->email_sender($current_user_email, $subject, $message);
                        }
                    }

                    /* Email To Tyroe and Insert Stream on behalf of Tyore */

                    echo json_encode(array('success' => true, 'success_message' => MESSAGE_REVIEW_ADD));
                } else {
                    echo json_encode(array('success' => false, 'success_message' => MESSAGE_ALREADY_REVIEWD));
                }
            }
        }
    }

    public function update_invite_notification_status() {
        $job_detail_id = $this->input->post('job_detail_id');
        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_detail_id = $job_detail_id;
        $job_details_obj->get_job_detail();

        if ($job_details_obj->invitation_status == -1) {
            //$job_details_obj->status_sl = -1;
            $job_details_obj->delete_job_details();
            echo json_encode(array('success' => true, 'details' => 'removed', 'user_id' => $job_details_obj->tyroe_id));
        } else {
            $job_details_obj->invite_notification_status = 1;
            $job_details_obj->update_invite_notification_status();
            echo json_encode(array('success' => true, 'details' => 'updated', 'user_id' => $job_details_obj->tyroe_id));
        }
    }

}
