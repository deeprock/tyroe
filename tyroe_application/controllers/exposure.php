<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
class Exposure extends Tyroe_Controller
{
    public function Exposure()
    {

        parent::__construct();
        $this->data['page_title'] = 'Exposure';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . EXPOSURE_SUB_MENU . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACTIVITY . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . EXPOSURE_SUB_MENU . ') ');
        }
    
    }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
        /*$viewers_data = $this->getAll("SELECT pv.viewer_id, pv.viewer_type, u.username, u.lastname, u.city, u.state, m.media_name,pv.created_timestamp
                                        FROM ".TABLE_PROFILE_VIEW." pv
                                        LEFT JOIN tyr_users u ON pv.viewer_id = u.user_id
                                        LEFT JOIN tyr_media m ON u.user_id = m.user_id AND m.profile_image = 1
                                        WHERE pv.user_id = '{$user_id}'");*/
        $joins_obj = new Tyr_joins_entity();
        $viewers_data = $joins_obj->get_all_profile_viewer_type($user_id);
        
        foreach ($viewers_data as $key => $viewer) {
            if ($viewer['job_title'] == "") {
                $job_title = "N/A";
            } else {
                $job_title = $viewer['job_title'];
            }

            if ($viewer['media_name'] == "") {
                $image = ASSETS_PATH_NO_IMAGE;
            } else {
                $image = ASSETS_PATH_PROFILE_THUMB . $viewer['media_name'];
            }
            $all_viewers[] = array(
                "viewer_id" => $viewer['viewer_id'],
                "viewer_type" => $viewer['viewer_type'],
                "reviewer" => $viewer['reviewer'],
                "studio" => $viewer['studio'],
                "media_name" => $image,
                "created_timestamp" => $viewer['created_timestamp'],
                "job_title" => $job_title
            );
        }
        $this->data['viewers_data'] = $all_viewers;
        $this->template_arr = array('header', 'contents/profile_views', 'footer');
        $this->load_template();
    }

    public function interesting_roles()
    {
        $this->template_arr = array('header', 'contents/interesting_roles', 'footer');
        $this->load_template();
    }

    public function shortlisted_roles()
    {
        $user_id = $this->session->userdata("user_id");
        /* $get_invite_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id FROM " . TABLE_INVITE_TYROE . " WHERE tyroe_id = '{$user_id}' ");
         if (empty($get_invite_jobs['job_id'])) {
             $get_invite_jobs['job_id'] = -1;
         };*/
        
        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_shortlisted_users_jobs($user_id);
        
        for ($i = 0; $i < count($get_jobs); $i++) {
            $job_id = $get_jobs[$i]['job_id'];
            $get_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($job_id);
        }

        $this->data['get_shorlisted_jobs'] = $get_jobs;
        $this->template_arr = array('header', 'contents/shortlisted_roles', 'footer');
        $this->load_template();
    }

    public function candidate_roles()
    {
        $user_id = $this->session->userdata("user_id");
        $invite_tyroe_obj = new Tyr_invite_tyroe_entity();
        $invite_tyroe_obj->tyroe_id = $user_id;
        $get_invite_jobs = $invite_tyroe_obj->get_tyroe_invite_job_ids();
        
        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_invite_candidate_users_jobs($get_invite_jobs['job_id'],$user_id);

        for ($i = 0; $i < count($get_jobs); $i++) {
            $job_id = $get_jobs[$i]['job_id'];
            $get_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($job_id);
        }
        
        $this->data['get_invite_jobs'] = $get_jobs;
        $this->template_arr = array('header', 'contents/candidate_roles', 'footer');
        $this->load_template();
    }

    public function feedback_rating()
    {
        $feedback_id=$this->input->post("id");
        $user_id = $this->session->userdata("user_id");
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->status_sl = 1;
        $get_job_openings = $jobs_obj->get_all_jobs_by_status();
        $this->data['get_job_openings'] = $get_job_openings;
        
        #TYROE TOP SECTION
        //        $this->data['profile_viewed'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_PROFILE_VIEW . " WHERE FROM_UNIXTIME(created_timestamp, '%Y-%m-%d') = CURRENT_DATE() AND user_id='" . $user_id . "'");
               
        $profile_view_obj1 = new Tyr_profile_view_entity();
        $profile_view_obj1->viewer_type = 4;
        $profile_view_obj1->user_id = $user_id;
        $this->data['reviewer_viewed'] = $profile_view_obj1->get_viewer_count_by_viewer_type();
        
        $profile_view_obj2 = new Tyr_profile_view_entity();
        $profile_view_obj2->viewer_type = 3;
        $profile_view_obj2->user_id = $user_id;
        $this->data['studio_viewed'] = $profile_view_obj2->get_viewer_count_by_viewer_type();
        
        $invite_tyroe_obj = new Tyr_invite_tyroe_entity();
        $invite_tyroe_obj->tyroe_id = $user_id;
        $invite_tyroe_obj->status_sl = 1;
        $this->data['invited_openings'] = $invite_tyroe_obj->get_invite_job_count_by_tyroe();
        
        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $user_id;
        $this->data['reviews'] = $rating_profile_obj->get_rating_count_by_user_id();

        if($feedback_id!=""){
            $where_feedback="AND review_id=".$feedback_id;
        }


        $bool = $this->input->post('bool');
        
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_review_count_by_user_reviews($user_id,$where_feedback);
                 
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY review_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();

        $get_feebacks = $joins_obj->get_all_feedback_by_reviews($user_id,$where_feedback,$order_by.$pagintaion_limit['limit']);
 

        foreach ($get_feebacks as $feedback) {
            $profile_score = $this->get_profile_score($feedback['company_id'],"user_id");

            if ($feedback['media_name'] == "") {
                $image = ASSETS_PATH_NO_IMAGE;
            } else {
                $image = ASSETS_PATH_PROFILE_THUMB . $feedback['media_name'];
            }
            if ($feedback['job_title'] == "") {
                $job_title = "N/A";
            } else {
                $job_title = $feedback['job_title'];
            }
            $all_feedbacks[] = array(
                "review_id" => $feedback['review_id'],
                "technical" => $feedback['technical'],
                "creative" => $feedback['creative'],
                "impression" => $feedback['impression'],
                "review_comment" => $feedback['review_comment'],
                "created_timestamp" => $feedback['created_timestamp'],
                "username" => $feedback['username'],
                "job_title" => $job_title,
                "media_name" => $image,
                "country" => $feedback['country'],
                "city" => $feedback['city'],
                "profile_score" => $profile_score

            );
        }
        /*for ($i = 0; $i < count($get_feebacks); $i++) {

            $all_feedbacks[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
                                                           FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
                                                           " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
                                                           WHERE job_id='" . $get_feebacks[$i]['job_id'] . "'");
                }*/
        $this->data['get_feedback'] = $all_feedbacks;
        $this->data['pagination'] = $this->pagination(array('url' => 'exposure/feedback_rating', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('header', 'contents/feedback_rating', 'footer');
        $this->load_template();
    }

    public function filter_feedback()
       {
           $user_id = $this->session->userdata("user_id");
           $feedback_type = $this->input->post("feedback_type");
           $feedback_keyword = $this->input->post("feedback_keyword");
           $feedback_duration = $this->input->post("feedback_duration");
           $where_feedback_filter;

           /*if ($this->session->userdata("role_id") == '2') {
               $where_feedback_filter = "tyroe_id='" . $user_id . "'";
           } else {
               $where_feedback_filter = "user_id='" . $user_id . "'";
           }*/
           if (!empty($feedback_type)) {

                   $where_feedback_filter .= " AND review.job_id='" . $feedback_type . "'";

           }
           if (!empty($feedback_keyword)) {
               $where_feedback_filter .= " AND review_comment LIKE '%" . $feedback_keyword . "%'";
           }
           if (!empty($feedback_duration) && $feedback_duration == "week") {
               $where_feedback_filter .= " AND FROM_UNIXTIME(review.created_timestamp) >= DATE_SUB(CURDATE(), INTERVAL 6 DAY)";
           } else if (!empty($feedback_duration) && $feedback_duration == "month") {
               $where_feedback_filter .= " AND MONTH(FROM_UNIXTIME(review.created_timestamp)) = MONTH(CURDATE())";
           }


           $bool = $this->input->post('bool');

           $joins_obj = new Tyr_joins_entity();
           $total_rows = $joins_obj->get_total_review_count_by_user_reviews($user_id, $where_feedback_filter);
           
           $this->set_post_for_view();
           $order_by = $this->orderby();
           if (isset($order_by['order_by'])) {
               $order_by = $order_by['order_by'];
           } else {
               $order_by = " ORDER BY review_id DESC ";
           }
           $pagintaion_limit = $this->pagination_limit();


           $feedback_result = $joins_obj->get_all_feedback_by_reviews($user_id,$where_feedback_filter,$order_by.$pagintaion_limit['limit']);

           //echo '<pre>';print_r($this->get_activity_stream($where_activity) );die('Call');echo '</pre>';$this->get_activity_stream($where_activity);

           foreach ($feedback_result as $feedback) {
                       $profile_score = $this->get_profile_score($feedback['company_id'],"user_id");

                       if ($feedback['media_name'] == "") {
                           $image = ASSETS_PATH_NO_IMAGE;
                       } else {
                           $image = ASSETS_PATH_PROFILE_THUMB . $feedback['media_name'];
                       }
                       if ($feedback['job_title'] == "") {
                           $job_title = "N/A";
                       } else {
                           $job_title = $feedback['job_title'];
                       }
                       $all_feedbacks[] = array(
                           "review_id" => $feedback['review_id'],
                           "technical" => $feedback['technical'],
                           "creative" => $feedback['creative'],
                           "impression" => $feedback['impression'],
                           "review_comment" => $feedback['review_comment'],
                           "created_timestamp" => $feedback['created_timestamp'],
                           "username" => $feedback['username'],
                           "job_title" => $job_title,
                           "media_name" => $image,
                           "country" => $feedback['country'],
                           "city" => $feedback['city'],
                           "profile_score" => $profile_score

                       );

           }

           


           $this->data['get_feedback'] = $all_feedbacks;
           $this->data['pagination'] = $this->pagination(array('url' => 'exposure/filter_feedback', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
           $this->template_arr = array('contents/search/search_feedback');
           $this->load_template();
       }
}
