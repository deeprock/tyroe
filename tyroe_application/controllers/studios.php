<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);

class Studios extends Tyroe_Controller {

    public function Studios() {
        parent::__construct();
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_USER_STUDIO . ') ');
        } else {
            $this->get_system_modules();
        }
    }

    public function form() {
        $item_id = $this->input->post("item_id");
        if (!empty($item_id)) {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $item_id;
            $users_obj->get_user();
            $studio_details = (array) $users_obj;

            //var_dump($studio_details);

            $company_detail_obj = new Tyr_company_detail_entity();
            $company_detail_obj->studio_id = $item_id;
            $company_detail_obj->get_company_detail_by_studio();
            $company_details = (array) $company_detail_obj;

            //var_dump($company_details);

            $this->data['studio_id'] = $studio_details['user_id'];
            if ($studio_details['status_sl'] == 1) {
                $studio_status_text = 'Active';
                $this->data['class'] = "on success";
                $this->data['class_span'] = "on";
            } elseif (
                    $studio_details['status_sl'] == 0) {
                $studio_status_text = 'Pending';
            }
            $this->data['studio_status'] = $studio_details['status_sl'];
            $this->data['studio_status_text'] = $studio_status_text;
            $this->data['firstname'] = $studio_details['firstname'];
            $this->data['lastname'] = $studio_details['lastname'];
            $this->data['email'] = $studio_details['email'];
            $this->data['username'] = $studio_details['username'];
            $this->data['password'] = $studio_details['password'];
            $this->data['studio_country'] = $studio_details['country_id'];
            $this->data['studio_state'] = $studio_details['state'];
            $this->data['studio_city'] = $studio_details['city'];
            $this->data['industry_id'] = $studio_details['industry_id'];
            $cellphone = explode("-", $studio_details['cellphone']);
            $this->data['code_cellphone'] = $cellphone[0];
            $this->data['cellphone'] = $cellphone[1];
            $this->data['company_id'] = $company_details['company_id'];
            $this->data['company_name'] = $company_details['company_name'];
            $this->data['company_country'] = $company_details['company_country'];
            $this->data['company_state'] = $company_details['company_state'];
            $this->data['company_city'] = $company_details['company_city'];
            $this->data['flag'] = "edit";

            $states_obj = new Tyr_states_entity();
            $states_obj->country_id = $studio_details['country_id'];
            $get_states_studio = $states_obj->get_all_states_by_country();

            $states_obj_1 = new Tyr_states_entity();
            $states_obj_1->country_id = $company_details['company_country'];
            $get_states_company = $states_obj_1->get_all_states_by_country();

            $this->data['get_states_studio'] = $get_states_studio;
            $this->data['get_states_company'] = $get_states_company;

            $cities_obj = new Tyr_cities_entity();
            $cities_obj->country_id = $studio_details['country_id'];
            $get_cities_studio = $cities_obj->get_all_cities_by_country_id();

            $cities_obj_1 = new Tyr_cities_entity();
            $cities_obj_1->country_id = $company_details['company_country'];
            $get_cities_company = $cities_obj_1->get_all_cities_by_country_id();

            $this->data['get_cities_studio'] = $get_cities_studio;
            $this->data['get_cities_company'] = $get_cities_company;
        }
        $content = "contents/studios/form";
        $this->template_arr = array($content);
        $this->load_template();
    }

    public function listing() {
        //$this->_add_css('bootstrap/bootstrap.min.css');
        $this->_add_css('admin_studio_listing.css');
       
        $this->data['page_title'] = 'Studios Listing';
        $studio_name = $this->input->post('studio_name');
        $filter_type = $this->input->post('filter_type');
        $bool = $this->input->post('bool');
        $sorting = $this->input->post('sorting');
        $where = '';
        if (!empty($studio_name)) {
            $where .= " and (c.company_name  LIKE('%" . $studio_name . "%'))";
            $this->data['studio_name'] = $studio_name;
        }
        $this->set_post_for_view();
        $order_by = "";
        if ($sorting == true) {
            $sort_column = $this->input->post('sort_column');
            $sort_type = $this->input->post('sort_type');
            $order_by = "ORDER BY " . $sort_column . " " . $sort_type;
            $this->data['sorting'] = $sorting;
            $this->data['sort_column'] = $sort_column;
            $this->data['sort_type'] = $sort_type;
        } else if (!empty($filter_type)) {
            if ($filter_type == "mostrecent") {
                $order_by = " ORDER BY st.user_id DESC ";
            } elseif ($filter_type == "alphabetical") {
                $order_by = " ORDER BY c.company_name ASC ";
            } else {
                $order_by = "";
            }
            $this->data['filter_type'] = $filter_type;
        } else {
            $order_by = " ORDER BY st.user_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();


        /* $this->data['data'] = $this->getAll('SELECT user.user_id,user.email,user.firstname,user.lastname,role.role AS role_id FROM ' . TABLE_USERS . ' user , ' . TABLE_ROLES . ' role  WHERE 1=1 AND role.role_id=user.role_id AND user.parent_id="0" AND user.role_id=' . STUDIO_ROLE . ' ' . $where . ' ' . $order_by . $pagintaion_limit['limit']); */
//        $this->data['data'] = $this->getAll("SELECT st.user_id AS id,co.company_name,st.firstname AS admin,stat.status,st.email,c.country AS country,i.industry_name AS industry,st.created_timestamp
//          AS signed_up,(SELECT COUNT(user_id) FROM ".TABLE_USERS." WHERE parent_id = st.user_id) AS reviewers,(SELECT COUNT(job_id) FROM ".TABLE_JOBS." WHERE
//          user_id = st.user_id) AS openings,tpv.visit_time FROM ".TABLE_USERS." st LEFT OUTER JOIN ".TABLE_PAGEVIEWS." tpv ON st.user_id=tpv.user_id
//          LEFT JOIN ".TABLE_STATUSES." stat ON st.status_sl = stat.status_id LEFT JOIN ".TABLE_INDUSTRY." i
//          ON st.industry_id=i.industry_id LEFT JOIN ".TABLE_COUNTRIES." c ON st.country_id = c.country_id LEFT JOIN ".TABLE_COMPANY." co ON st.user_id = co.studio_id
//          WHERE role_id ='" . STUDIO_ROLE . "' " . $where . ' AND (st.status_sl=1 OR st.status_sl=0) GROUP BY st.user_id ' . $order_by . $pagintaion_limit['limit']); 
//         $total_rows = $this->getRow('SELECT COUNT(*) AS cnt FROM ' . TABLE_USERS . ' st
//          LEFT JOIN  tyr_company_detail co ON st.user_id = co.studio_id
//          LEFT JOIN  tyr_countries c ON st.country_id = c.country_id
//          WHERE 1=1 AND role_id=' . STUDIO_ROLE . ' AND parent_id="0" ' . $where . ' AND (status_sl=1 OR status_sl=0) ORDER BY user_id ASC'); 

//        $users_obj = new Tyr_users_entity();
//        $users_obj->role_id = STUDIO_ROLE;
//        $total_rows = $users_obj->get_search_listing_studio_count($where);
        
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_search_listing_studio_data_count(STUDIO_ROLE,$where);

        $joins_obj = new Tyr_joins_entity();
        $this->data['data'] = $joins_obj->get_search_listing_studio_data(STUDIO_ROLE, $where, $order_by, $pagintaion_limit);
        $this->data['pagination'] = $this->pagination(array('url' => 'user/index/page', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        /* Country Drop Down */
        if ($bool == 'true') {
            $this->template_arr = array('contents/studios/get_studios_filter');
        } else {
            $this->template_arr = array('actions', 'header', 'contents/studios/view', 'footer');
        }
        //echo '<pre>';print_r($this->template_arr);echo '</pre>';
        $this->load_template();
    }

    public function save() {
        $studio_status = $this->input->post("studio_status");
        $company_name = $this->input->post("company_name");
        $company_country = $this->input->post("company_country");
        //$company_state = $this->input->post("company_state");
        $company_city = $this->input->post("company_city");
        $firstname = $this->input->post("firstname");
        $lastname = $this->input->post("lastname");
        $username = $this->input->post("username");
        $email = $this->input->post("email");
        $password = $this->input->post("password");
        $confirmpass = $this->input->post("confirmpass");
        $user_country = $this->input->post("user_country");
        //$user_state = $this->input->post("user_state");
        $user_city = $this->input->post("user_city");
        $cellphone = $this->input->post("cellphone");
        $industry = $this->input->post("industry");
        $flag = $this->input->post("flag");
        $where = "";
        if ($flag == 'edit') {
            $where = " AND user_id!='" . $this->input->post("studio_id") . "'";
        }

        $users_obj_check_email = new Tyr_users_entity();
        $users_obj_check_email->email = $email;
        if ($users_obj_check_email->check_duplicate_user_email($where) == true) {
            echo json_encode(array("duplicate_email" => true));
            exit;
        } else {
            $users_obj_check_username = new Tyr_users_entity();
            $users_obj_check_username->username = $username;
            if ($users_obj_check_username->check_duplicate_user_username($where) == true) {
                echo json_encode(array("duplicate_username" => true));
                exit;
            } else {
                $where_company = "";
                if ($flag == 'edit') {
                    $where_company = " AND company_id!='" . $this->input->post("company_id") . "'";
                }

                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->company_name = $company_name;
                $company_detail_obj->check_duplicate_company_name($where_company);

                if ($company_detail_obj->company_id > 0) {
                    echo json_encode(array("duplicate_company" => true));
                    exit;
                }
            }
        }

        if ($flag == 'edit') {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $this->input->post("studio_id");
            $users_obj->get_user();
            $users_obj->email = $email;
            $users_obj->firstname = $firstname;
            $users_obj->lastname = $lastname;
            $users_obj->username = $username;
            $users_obj->country_id = $user_country;
            $users_obj->city = $user_city;
            $users_obj->cellphone = $cellphone;
            $users_obj->role_id = 3;
            $users_obj->industry_id = $industry;
            $users_obj->status_sl = $studio_status;
            $users_obj->modified_timestamp = time();
            $users_obj->update_users();
        } else {
            $users_obj = new Tyr_users_entity();
            $users_obj->email = $email;
            $users_obj->firstname = $firstname;
            $users_obj->lastname = $lastname;
            $users_obj->username = $username;
            $users_obj->country_id = $user_country;
            $users_obj->city = $user_city;
            $users_obj->cellphone = $cellphone;
            $users_obj->role_id = 3;
            $users_obj->industry_id = $industry;
            $users_obj->status_sl = $studio_status;
            $users_obj->password = $this->hashPass($password);
            $users_obj->password_len = strlen($password);
            $users_obj->save_user();
        }

        $id = $users_obj->user_id;
        $modules = @explode(',', STUDIO_USER_MODULES);
        $return = 0;
       
        if ($flag == 'edit') {
//            $company_id = $this->input->post("company_id");
//            $this->update(TABLE_COMPANY,$data_studio_company,array('company_id'=>$company_id));
            $company_detail_obj = new Tyr_company_detail_entity();
            $company_detail_obj->company_id = $this->input->post("company_id");
            $company_detail_obj->get_company_detail();
            $company_detail_obj->studio_id = $id;
            $company_detail_obj->company_name = $company_name;
            $company_detail_obj->company_country = $company_country;
            $company_detail_obj->company_city = $company_city;
            $company_detail_obj->update_company_all_details();
            $return = $company_detail_obj->company_id;
        } else {
            $company_detail_obj = new Tyr_company_detail_entity();
            $company_detail_obj->company_id = $this->input->post("company_id");
            $company_detail_obj->studio_id = $id;
            $company_detail_obj->company_name = $company_name;
            $company_detail_obj->company_country = $company_country;
            $company_detail_obj->company_city = $company_city;
            $company_detail_obj->save_company_detail();
            $return = $company_detail_obj->company_id;
            foreach ($modules as $v) {
                $user_modeul_obj = new Tyr_permissions_entity();
                $user_modeul_obj->user_id = $id;
                $user_modeul_obj->module_id = $v;
                $user_modeul_obj->save_permissions();
            }
        }

        $joins_obj = new Tyr_joins_entity();
        $record = $joins_obj->get_studio_details_record($id);
//        $where = ' AND st.user_id='.$id;
//        $record = $this->getRow("SELECT st.user_id,st.email,st.created_timestamp,i.industry_name,stat.status,c.company_name,cont.country,
//                              tpv.visit_time FROM ".TABLE_USERS." st LEFT JOIN ".TABLE_COMPANY." c ON st.user_id = c.studio_id
//                              LEFT JOIN ".TABLE_STATUSES." stat ON st.status_sl = stat.status_id
//                              LEFT JOIN ".TABLE_COUNTRIES." cont ON c.company_country = cont.country_id
//                              LEFT JOIN ".TABLE_INDUSTRY." i ON st.industry_id = i.industry_id
//                              LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv ON st.user_id = tpv.user_id
//                              WHERE st.user_id='".$id."'");
        $html = '';
        if ($record['status'] == 'pending') {
            $status = '<span class="label label-info rt-pending">' . $record['status'] . '</span>';
        } else {
            $status = '<span class="label label-success">' . $record['status'] . '</span>';
        }
        if ($record['country'] == "") {
            $country = "Not Available";
        } else {
            $country = $record['country'];
        }
        if ($record['visit_time'] == "") {
            $last_visit = "Not Available";
        } else {
            $last_visit = date('M j,Y', $v['visit_time']);
        }
        $signed_up = date('M j,Y', $v['signed_up']);
        if ($flag != 'edit') {
            $html .= '<tr id="Studio-id-' . $record['user_id'] . '" >';
        }
        $html.= '<td align="center"><input type="checkbox" name="studio_radio" class="studio_check"
                                              value="' . $record['user_id'] . '"></td>
                    <td>' . $record['user_id'] . '</td>
                    <td><p class="rt-name">' . $record['company_name'] . '</p>
                    <span class="rt-title">' . $record['industry'] . '</span></td>
                    <td>' . $status . '</td>
                    <td>' . $country . '</td>
                    <td><a href="mailto:' . $record['email'] . '">' . $record['email'] . '</a></td>
                    <td>' . $signed_up . '</td>
                    <td>' . $last_visit . '</td>
                    <td><a href="javascript:void(0)" class="btn_pop" id="' . $record['user_id'] . '">Edit</a> &nbsp;&nbsp;<a href="javascript:void(0)" class="btn_delete" id="' . $record['user_id'] . '">Delete</a></td>';
        if ($flag != 'edit') {
            $html.='</tr>';
        }

        if ($flag == 'edit') {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_UPDATE_SUCCESS, 'html' => $html, 'flag' => '0', 'id' => $id));
        } else {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS, 'html' => $html, 'flag' => '1'));
        }
        //echo json_encode(array('success' => true, 'success_message' => $msg));
    }

    public function get_cities_states() {
        $country_id = $this->input->post('country_id');

        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_all_cities_by_country_id();

        $states_obj = new Tyr_states_entity();
        $states_obj->country_id = $country_id;
        $states = $states_obj->get_all_states_by_country();
        ?>
        <div id="StatesDiv">
            <option value="">Select State</option>
        <?php
        foreach ($states as $state) {
            ?>
                <option value="<?= $state['states_id'] ?>"><?= $state['states_name'] ?></option>
            <?php
        }
        ?>
        </div>
        <div id="CitiesDiv">
            <option value="">Select City</option>
        <?php
        foreach ($cities as $city) {
            ?>
                <option value="<?= $city['city_id'] ?>"><?= $city['city'] ?></option>
                <?php
            }
            ?>
        </div>
            <?php
        }

        public function delete_studio() {
            $delete_id = $this->input->post('delete_id');
            
            /* Check Main Parent */
            $company_details_obj = new Tyr_company_detail_entity();
            $company_details_obj->studio_id = $delete_id;
            $company_details_obj->get_company_detail_by_studio();
            
            $joins_obj  = new Tyr_joins_entity();
            $query_studio = $joins_obj->get_studio_locations_list($company_details_obj->company_id);
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $delete_id;
            $users_obj->get_user();
            $users_obj->email = 'Remove';
            $users_obj->profile_url = 'Remove';
            $users_obj->username = 'Remove';
            if(intval($users_obj->role_id) == 2){
                $users_obj->status_sl = -1;
            }
            
            if($company_details_obj->parent_id != 0){
                $users_obj->status_sl = -1;
            }
            
            if(count($query_studio) == 1){
                $users_obj->status_sl = -1;
            }
            
            $result = $users_obj->update_users();
            
            /* Remove All Team Member*/
            $tema_member_obj = new Tyr_users_entity();
            $tema_member_obj->parent_id = $delete_id;
            $tema_member = $tema_member_obj->get_all_team_member_of_studio();
            if($tema_member != ''){
            foreach($tema_member as $member_id){
                $remove_team_obj = new Tyr_users_entity();
                $remove_team_obj->user_id = $member_id;
                $remove_team_obj->get_user();
                $remove_team_obj->email = 'Remove';
                $remove_team_obj->profile_url = 'Remove';
                $remove_team_obj->username = 'Remove';
                $remove_team_obj->status_sl = -1;
                $remove_team_obj->update_users();
                }
            }
            
            
            /* Add Jobs To Archive*/
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $delete_id;
            $jobs_obj->archive_job_by_user();
            if($tema_member != ''){
            foreach($tema_member as $member_id){
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->user_id = $member_id;
                $jobs_obj->archive_job_by_user();
            }
            
            }
            
            
            if ($result) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
            } else {
                echo json_encode(array('success' => false));
            }
        }

        public function bulkaction() {
            $delete_ids = $this->input->post('bulk_check');
//        $status = array(
//                    'status_sl' => -1
//        );
            foreach ($delete_ids as $id) {
                /* Check Main Parent */
                $company_details_obj = new Tyr_company_detail_entity();
                $company_details_obj->studio_id = $id;
                $company_details_obj->get_company_detail_by_studio();

                $joins_obj  = new Tyr_joins_entity();
                $query_studio = $joins_obj->get_studio_locations_list($company_details_obj->company_id);

                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $id;
                $users_obj->get_user();
                $users_obj->email = 'Remove';
                $users_obj->profile_url = 'Remove';
                $users_obj->username = 'Remove';
                
                if($users_obj->role_id != 0){
                    $users_obj->status_sl = -1;
                }
                
                if($company_details_obj->parent_id != 0){
                    $users_obj->status_sl = -1;
                }

                if(count($query_studio) == 1){
                    $users_obj->status_sl = -1;
                }

                $result = $users_obj->update_users();

                /* Remove All Team Member*/
                $tema_member_obj = new Tyr_users_entity();
                $tema_member_obj->parent_id = $id;
                $tema_member = $tema_member_obj->get_all_team_member_of_studio();
                if($tema_member != ''){
                foreach($tema_member as $member_id){
                    $remove_team_obj = new Tyr_users_entity();
                    $remove_team_obj->user_id = $member_id;
                    $remove_team_obj->get_user();
                    $remove_team_obj->email = 'Remove';
                    $remove_team_obj->profile_url = 'Remove';
                    $remove_team_obj->username = 'Remove';
                    $remove_team_obj->status_sl = -1;
                    $remove_team_obj->update_users();
                    }
                }
                

                /* Add Jobs To Archive*/
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->user_id = $id;
                $jobs_obj->archive_job_by_user();

                if($tema_member != ''){
                    foreach($tema_member as $member_id){
                        $jobs_obj = new Tyr_jobs_entity();
                        $jobs_obj->user_id = $member_id;
                        $jobs_obj->archive_job_by_user();
                    }

                }
            }
            if ($result) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS, 'json_idz' => $delete_ids));
            } else {
                echo json_encode(array('success' => false));
            }
        }

//not needed filter_studio method anymore....
        public function filter_studio() {
            $filter_type = $this->input->post('filter_type');
            if ($filter_type == "mostrecent") {
                $orderby = "ORDER BY st.user_id DESC";
            } elseif ($filter_type == "alphabetical") {
                $orderby = "ORDER BY co.company_name ASC";
            } else {
                $orderby = "";
            }
            $pagintaion_limit = $this->pagination_limit();
            $joins_obj = new Tyr_joins_entity();
            $this->data['data'] = $joins_obj->get_search_listing_studio_by_studio_role($where, $orderby, $pagintaion_limit);
//            $this->data['data'] = $this->getAll("SELECT st.user_id AS id,co.company_name,st.firstname AS admin,stat.status,st.email,c.country AS country,i.industry_name AS industry,FROM_UNIXTIME(st.created_timestamp,'%m-%d-%Y')
//            AS signed_up,(SELECT COUNT(user_id) FROM " . TABLE_USERS . " WHERE parent_id = st.user_id) AS reviewers,(SELECT COUNT(job_id) FROM " . TABLE_JOBS . " WHERE
//            user_id = st.user_id) AS openings FROM " . TABLE_USERS . " st LEFT JOIN " . TABLE_STATUSES . " stat ON st.status_sl = stat.status_id LEFT JOIN " . TABLE_INDUSTRY . " i
//             ON st.industry_id=i.industry_id LEFT JOIN " . TABLE_COUNTRIES . " c ON st.country_id = c.country_id LEFT JOIN " . TABLE_COMPANY . " co ON st.user_id = co.studio_id
//             WHERE role_id ='" . STUDIO_ROLE . "' " . $where . ' AND st.status_sl=1 ' . $orderby . $pagintaion_limit['limit']);
            $this->data['pagination'] = $this->pagination(array('url' => 'user/index/page', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
            $this->template_arr = array('contents/studios/get_studios_filter');
            $this->load_template();
        }

        public function approval() {
            $stuidio_id = preg_replace('/[^\d]/', '', $this->input->get('stuidio_id'));
            $action = preg_replace('/[^\d]/', '', $this->input->get('action'));
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $stuidio_id;
            $users_obj->status_sl = 0;
            $users_obj->role_id = 4;
            $count = $users_obj->get_remaining_studio_reviewer_count();
            $remaining_studio_reviewers = $count['cnt'];
        
        
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $stuidio_id;
            $users_obj->role_id = 3;
            $studio_data = $users_obj->get_studio_details();
            
//            $rem_studios_where = array('parent_id' => $stuidio_id, 'status_sl ' => '0', 'role_id' => '4');
//            $remaining_studio_reviewers = $this->db->select()->from(TABLE_USERS)->where($rem_studios_where)->count_all_results();
//            $studio_data = $this->db->select()->from(TABLE_USERS)->where(array('user_id' => $stuidio_id, 'role_id' => '3'))->get()->result_array();
//            
            
            $firstname = $studio_data[0]['firstname'];
            $lastname = $studio_data[0]['lastname'];
            if (!is_numeric($remaining_studio_reviewers)) {
                $remaining_studio_reviewers = 0;
                $email_msg = "<b>" . ucfirst($firstname) . " " . ucfirst($lastname) . "</b> studio account has been approved <br><br><br>";
            } else if ($remaining_studio_reviewers >= 2) {
                $email_msg = "<b>" . ucfirst($firstname) . " " . ucfirst($lastname) . "</b> studio account has been approved and $remaining_studio_reviewers reviewers waiting for your approval <br><br><br>";
            } else {
                $email_msg = "<b>" . ucfirst($firstname) . " " . ucfirst($lastname) . "</b> studio account has been approved and $remaining_studio_reviewers reviewer waiting for your approval <br><br><br>";
            }


            $rec_email = $studio_data[0]['email'];

            $message = "<link
            href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
            rel='stylesheet' type='text/css'>";
            $message .= "<style>html { font-family: 'Open Sans'}</style>";
            //$message .= "<b>".ucfirst($firstname)." ".ucfirst($lastname)."</b> studio account has been approved and $remaining_studio_reviewers reviewers waiting for approval <br><br><br>";
            $message .= $email_msg;
            $message .= '<a href="' . LIVE_SITE_URL . 'team" style="text-decoration: none; color: #fff; background: #2E9DF6; padding: 9px 13px; border-radius: 3px; font-family: verdana; font-size: 12px; font-weight: bold;">Manage Reviewers</a><br /> <br />';
            $message .= "___________________________________________________";
            $message .= "<br><br>";
            $message .= "Andrew & Alwyn, co-creators of Tyroe";
            $message .= "<br>";
            $message .= "<a href='" . LIVE_SITE_URL . "'><img src='" . ASSETS_PATH . "img/logo_tyroe_new1.png'></a>";
            /*
              <a href="#" style="
              text-decoration: none;
              color: #fff;
              background: #2E9DF6;
              padding: 9px 13px;
              border-radius: 3px;
              font-family: verdana;
              font-size: 12px;
              font-weight: bold;
              ">Manage Reviewers</a>

             *
             * */

            $subject = ucfirst($firstname) . " " . ucfirst($lastname) . " Studio has been approved!";


            if (is_numeric($stuidio_id) && is_numeric($action)) {
                $mail_from = EMAIL_FROM;
                //$stuidio_id;
                if ($action == 2) {
                    $studio_new_status = '-1';
                } else {
                    $studio_new_status = '1';

                    $config = unserialize(EMAIL_CONFIG);
                    //print_array($config ,true,'MAIL DEBUG');
                    $this->load->library('email');
                    $this->email = new CI_Email();
                    $this->email->initialize($config);
                    $this->email->clear(true);
                    $this->email->mailtype = "html";
                    $this->email->from($mail_from);
                    $this->email->subject($subject);
                    $this->email->message($message);
                    $this->email->to($rec_email);
                    $this->email->send();
                }

                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $stuidio_id;
                $users_obj->get_user();
                $users_obj->status_sl = $studio_new_status;
                $users_obj->update_users();

                echo 1;
                //print_array($this->db->affected_rows());
                //print_array($this->db->last_query(),false,'>>LAST UPDATE QUERY');
            } else {
                echo 0;
            }
        }
        
        function get_studio_team_member(){
            $delete_id = $this->input->post('delete_id');
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $delete_id;
            $users_obj->status_sl = 1;
            $result = $users_obj->get_studio_team_count();
            $count = $result['cnt'];
            echo json_encode(array("status" => true,'reviewer_count' => $count));
        }

    }
