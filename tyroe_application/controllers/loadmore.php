<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);

class Loadmore extends Tyroe_Controller {

    public function __construct() {
        parent::__construct('loadmore');
    }

    public function index() {

    }

    public function loadmore_images() {
        $page_no = $this->input->get('page_no');
        $profile_user_id = $this->input->get('profile_user_id');
        $start = 20 + ((intval($page_no) - 1) * 10);

        $media_entity_obj = new Tyr_media_entity();
        $latestwork_images = $media_entity_obj->get_latest_image_for_load_more_new($profile_user_id, $start);
        $foreach_array_inc = 0;
        foreach ($latestwork_images as $add_html_latestwork) {
            $foreach_array_inc++;
            $latest_work_tag = $add_html_latestwork['latest_work_tag'];
            //print_array($latest_work_tag);
            if (trim($latest_work_tag) != null) {
                $tags = explode(',', $latest_work_tag);
                $tags = array_filter($tags);

                $add_html = '';
                if (is_array($tags) && !empty($tags)) {
                    $add_html .= '<ul class="media_tag media-img-tag">';
                    foreach ($tags as $tag_index => $tag_val) {
                        $add_html .='<li><a href="#">' . $tag_val . '</a></li>';
                    }
                    $add_html .='</ul>';
                }
                $add_html_latestwork['latest_work_tag_html'] = $add_html;
            }

            $new_latestwork[] = $add_html_latestwork;
        }

        if (is_array($new_latestwork) && !empty($new_latestwork)) {
            echo json_encode($new_latestwork);
        } else {
            die(0);
        }
        exit;
    }

    public function fill_images() {
        $target_col = $this->input->get('target_col');
        $result_target = $this->input->get('result_target');
        $divs_diff = $this->input->get('divs_diff');
        $last_img_width = $this->input->get('last_img_width');
        $last_image_id = $this->input->get('last_image_id');

        $last_image_info = explode('-', $last_image_id);
        $last_image_id = preg_replace('/[^\d]/', '', $last_image_info[1]);
        $last_image_order = $last_image_info[2];
        if (!is_numeric($last_image_id)) {
            die;
        }

        $media_entity_obj = new Tyr_media_entity();
        $filled_col_images = $media_entity_obj->get_filled_cols_image_for_load_more($last_image_id, $last_image_order, $result_target);

        ///echo $this->db->last_query();
        $new_json = null;
        foreach ($filled_col_images as $filled_col_image) {
            $latest_work_image_src = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $filled_col_image['latest_work_image_src'];
            list($width_orig, $height_orig) = getimagesize($latest_work_image_src);
            $ratio_orig = $width_orig / $height_orig;
            $height = $last_img_width / $ratio_orig;
            $new_height = round($height);
            //echo $new_height.' __ <br />';
            $divs_diff = max($new_height, $divs_diff) - min($new_height, $divs_diff);
            $new_json[] = $filled_col_image;
            if ($divs_diff <= 200) {
                break;
            }
        }

        $foreach_array_inc = 0;
        $new_latestwork = null;
        foreach ($new_json as $add_html_latestwork) {
            $foreach_array_inc++;
            $latest_work_tag = $add_html_latestwork['latest_work_tag'];
            //print_array($latest_work_tag);
            if (trim($latest_work_tag) != null) {
                $tags = explode(',', $latest_work_tag);
                $tags = array_filter($tags);

                $add_html = '';
                if (is_array($tags) && !empty($tags)) {
                    $add_html .= '<ul class="media_tag media-img-tag">';
                    foreach ($tags as $tag_index => $tag_val) {
                        $add_html .='<li><a href="#">' . $tag_val . '</a></li>';
                    }
                    $add_html .='</ul>';
                }
                $add_html_latestwork['latest_work_tag_html'] = $add_html;
            }

            $new_latestwork[] = $add_html_latestwork;
        }

        echo json_encode($new_latestwork);
    }

}
