<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_friend_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);

class Register extends Tyroe_Controller
{
    public function Register()
    {
        parent::__construct('login');
        $this->data['page_title'] = 'Registration';
        $this->load->library('form_validation');
        $country_obj = new Tyr_countries_entity();
        $this->data['get_countries'] = $country_obj->get_all_countries_as();
    }

    public function index($is_company = '')
    {
        
            //For invite reviewer
            if($this->session->userdata('invite_job_id')){
                $this->data['talent_active']    = '';
                $this->data['company_active']   = 'active';
                $this->data['is_company_invite_active']   = true;
                $this->data['invite_job_id']   = $this->session->userdata('invite_job_id');
                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->studio_id = $this->session->userdata('invite_studio_id');
                $company_detail_obj->get_company_detail_by_studio();
                $this->data['company_country_id']   = $company_detail_obj->company_country;
                $this->data['company_city_id']   = $company_detail_obj->company_city;
                $this->data['invite_studio_id']   = $this->session->userdata('invite_studio_id');
                
                $this->data['company_name']  = $company_detail_obj->company_name;
                $this->data['user_email']  =$this->session->userdata('invite_email');
            }else if($is_company != '' && $is_company == 'company'){
                $this->data['talent_active']    = 'active';
                $this->data['company_active']   = '';
                $this->data['company_name']  = '';
                $this->data['user_email'] = '';
                $this->data['is_company_active']   = true;
            }else{
                $this->data['talent_active']    = 'active';
                $this->data['company_active']   = '';
                $this->data['company_name']  = '';
                $this->data['user_email'] = '';
            }
            
            $this->data['msg'] = "";
            $this->data['error_id'] = 0;
            $this->data['page_title'] = 'Registration';
            $this->template_arr = array('header_login', 'contents/register', 'footer');
            $this->load_template();
    }

    public function invite_register($data="")
    {
        $this->data['msg'] = "";
        $this->data['error_id'] = 0;
        $this->data['page_title'] = 'Registration';
        if(!empty($data)){
            $data_arr = explode("_",$data);
            $this->data['invite_user_id'] = $data_arr['0'];
            $this->data['invite_coupon_id'] = $data_arr['1'];
        }
        $this->template_arr = array('header_login', 'contents/register', 'footer');
        $this->load_template();
    }

    function get_existing_studios()
    {
        $searching_value = $this->input->post("searching_value");
        $company_detail_obj = new Tyr_company_detail_entity();
        //$query_studio = $company_detail_obj->get_group_concat_company_name($searching_value);
        $query_studio = $company_detail_obj->get_company_name_list($searching_value);
        echo json_encode($query_studio);
    }
    
    function get_studios_locations()
    {
        $selected_value = $this->input->post("selected_value");
        
        $company_detail_obj = new Tyr_company_detail_entity();
        $company_detail_obj->studio_id = $selected_value;
        $company_detail_obj->get_company_detail_by_studio();
        
        $joins_obj  = new Tyr_joins_entity();
        $query_studio = $joins_obj->get_studio_locations_list($company_detail_obj->company_id);
        //$query_studio = array(0 => array('name' => "Adelaide,Austrelia",'company_id' => 1));
        echo json_encode(array('success' => true, 'locations' => $query_studio));
    }

    public function save_user(){
        $new_location = $this->input->post("new_location");
        $this->data['error_id'] = 0;
        if(isset($_POST['user_type']) && $_POST['user_type'] == 'company'){
            
            if($new_location == "" or $new_location == 'new'){
                $config_validation = array(
                    array(
                        'field' => 'user_firstname',
                        'label' => LABEL_FIRST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_lastname',
                        'label' => LABEL_LAST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_email',
                        'label' => LABEL_EMAIL,
                        'rules' => 'trim|required|valid_email'
                    ),
                    array(
                        'field' => 'user_password',
                        'label' => LABEL_PASSWORD,
                        'rules' => 'required'
                    ),
//                    array(
//                        'field' => 'user_name',
//                        'label' => LABEL_USER_NAME,
//                        'rules' => 'required'
//                    ),
                    array(
                        'field' => 'company_name',
                        'label' => LABEL_COMPANY_NAME,
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'job_title',
                        'label' => LABEL_JOB_TITLE,
                        'rules' => 'trim|required'
                    )
                    ,
                    array(
                        'field' => 'country',
                        'label' => LABEL_COUNTRY,
                        'rules' => 'trim|required'
                    )
                    ,
                    array(
                        'field' => 'city',
                        'label' => LABEL_CITY,
                        'rules' => 'trim|required'
                    )
                );
            }else{
                $config_validation = array(
                    array(
                        'field' => 'user_firstname',
                        'label' => LABEL_FIRST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_lastname',
                        'label' => LABEL_LAST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_email',
                        'label' => LABEL_EMAIL,
                        'rules' => 'trim|required|valid_email'
                    ),
                    array(
                        'field' => 'user_password',
                        'label' => LABEL_PASSWORD,
                        'rules' => 'required'
                    ),
//                    array(
//                        'field' => 'user_name',
//                        'label' => LABEL_USER_NAME,
//                        'rules' => 'required'
//                    ),
                    array(
                        'field' => 'company_name',
                        'label' => LABEL_COMPANY_NAME,
                        'rules' => 'trim|required'
                    ),
                    array(
                        'field' => 'job_title',
                        'label' => LABEL_JOB_TITLE,
                        'rules' => 'trim|required'
                    )
                );
            }
                
        }else{
            $config_validation = array(
                    array(
                        'field' => 'user_firstname',
                        'label' => LABEL_FIRST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_lastname',
                        'label' => LABEL_LAST_NAME,
                        'rules' => 'required'
                    ),array(
                        'field' => 'user_email',
                        'label' => LABEL_EMAIL,
                        'rules' => 'trim|required|valid_email'
                    ),
                    array(
                        'field' => 'user_password',
                        'label' => LABEL_PASSWORD,
                        'rules' => 'required'
                    ),
//                    array(
//                        'field' => 'user_name',
//                        'label' => LABEL_USER_NAME,
//                        'rules' => 'required'
//                    ),
                );
        }
        
        
        $this->form_validation->set_rules($config_validation);
        
        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = "";
            $this->template_arr = array('header_login', 'contents/register', 'footer');
            $this->load_template();
        }else {
//            echo '<pre>';
//            var_dump($_POST);
//            echo '</pre>';
//            echo "<br/>";
            
            $country_id = $this->input->post("country");
            $city_id = $this->input->post("city");
            $user_type = $this->input->post("user_type");
            $parent_id = $this->input->post("parent_id");
            
            if($user_type == "company"){
                if($new_location != 'new' && $new_location != ""){
                    $arr = explode('_', $new_location);
                    $country_id = $arr[1];
                    $city_id = $arr[0];
                }
            }
            
//            $cities_obj = new Tyr_cities_entity();
//            $cities_obj->city_id = $city_id;
//            $cities_obj->get_cities();
//            $city_name = $cities_obj->city;
            
//            echo "City Name : ".$city_name;
//            echo "City ID : ".$city_id;
//            echo "Country ID : ".$country_id;
//            echo "User Type : ".$user_type;
//            echo "New Location : ".$new_location;
//            exit;
                        
            $user_email = $this->input->post("user_email");
            $user_firstname = $this->input->post("user_firstname");
            $user_lastname = $this->input->post("user_lastname");
            
            $user_handle_obj = new Tyr_users_entity();
            $user_handle_obj->firstname = $user_firstname;
            $user_handle_obj->lastname = $user_lastname;
            $profile_handle = $user_handle_obj->get_user_handle();
          
            $user_name = $profile_handle;
            
            $company_name = $this->input->post("company_name");
            $job_title = $this->input->post("job_title");

            #Checking Email Duplication
            $dup_users_obj = new Tyr_users_entity();
            $dup_users_obj->email = $user_email;
            
            $dup_user_name_obj = new Tyr_users_entity();
            $dup_user_name_obj->username = $user_name;
            
//            $duplicate_email = $this->Execute("Select email from " . TABLE_USERS . " where email='" . $user_email . "'");
//            $duplicate_user_name = $this->Execute("Select email from " . TABLE_USERS . " where username='" . $user_name . "'");
            
            if ($dup_users_obj->check_duplicate_user_email() && !(isset($_POST['is_company_invite_active']))) {
                $this->data['msg'] = LABEL_EMAIL_ALREADY_REGISTERED;
                $this->data['error_id'] = 1;
                $this->data['company_name'] = $company_name;
                $this->data['job_title'] = $job_title;
                $this->template_arr = array('header_login', 'contents/register', 'footer');
                $this->load_template();
            }else if ($dup_user_name_obj->check_duplicate_user_username() && !(isset($_POST['is_company_invite_active']))) {
                $this->data['msg'] = LABEL_USERNAME_ALREADY_REGISTERED;
                $this->data['error_id'] = 1;
                $this->data['company_name'] = $company_name;
                $this->data['job_title'] = $job_title;
                $this->template_arr = array('header_login', 'contents/register', 'footer');
                $this->load_template();
            }else {
                $role_id = "2";
                $password = $this->hashPass($this->input->post("user_password"));
                $company_insert_status = 0;
                $company_studio_id = $this->input->post('parent_company_studio_id');
                
                $new_users_obj = new Tyr_users_entity();
                if(isset($_POST['is_company_invite_active']) && $_POST['is_company_invite_active'] == 1){
                    
                    $new_users_obj->email = $user_email;
                    $new_users_obj->get_user_by_email();
                    $new_users_obj->firstname = $user_firstname;
                    $new_users_obj->lastname = $user_lastname;
                    $new_users_obj->username = $user_name;
                    $new_users_obj->profile_url = $user_name;
                    $new_users_obj->password = $password;
                    $new_users_obj->password_len = strlen($this->input->post("user_password"));
                    $new_users_obj->created_timestamp = strtotime("now");
                    $new_users_obj->modified_timestamp = strtotime("now");
                    $new_users_obj->country_id = $country_id;
                    $new_users_obj->city = $city_id;
                    $new_users_obj->status_sl = 1;
                    
                    if(intval($company_studio_id) != 0){
                        $first_company_detail_obj = new Tyr_company_detail_entity();
                        $first_company_detail_obj->studio_id = $company_studio_id;
                        $first_company_detail_obj->get_company_detail_by_studio();
                        $company_parent_id = $first_company_detail_obj->company_id;

                        $company_detail_obj = new Tyr_company_detail_entity();
                        $company_detail_obj->parent_id = $first_company_detail_obj->company_id;
                        $company_detail_obj->company_country = $country_id;
                        $company_detail_obj->company_city = $city_id;
                        $is_company_exist = $company_detail_obj->get_company_count_by_parent_id_and_location();
                    }
                    
                    $new_users_obj->role_id = 4;
                    $new_users_obj->job_title = $job_title;
                    $new_users_obj->parent_id = $is_company_exist['company_owner'];
                    
                    #Update Invite User
                    $new_users_obj->update_users();
                    
                    $modules = @explode(',', REVIEWER_USER_MODULES);
                    
                }else{
                    //create USERS Entity Obj to add new user
                    $new_users_obj->role_id = $role_id;
                    $new_users_obj->email = $user_email;
                    $new_users_obj->firstname = $user_firstname;
                    $new_users_obj->lastname = $user_lastname;
                    $new_users_obj->username = $user_name;
                    $new_users_obj->profile_url = $user_name;
                    $new_users_obj->password = $password;
                    $new_users_obj->password_len = strlen($this->input->post("user_password"));
                    $new_users_obj->created_timestamp = strtotime("now");
                    $new_users_obj->modified_timestamp = strtotime("now");
                    $new_users_obj->country_id = $country_id;
                    $new_users_obj->city = $city_id;
                    $new_users_obj->status_sl = 1;

                    
                    if($company_name != ""){
                        // create Company_Detail Entity Obj to Check if Company Exist
                        $company_parent_id = 0;
                        $is_company_exist = array('co_cnt' => 0);
                        if(intval($company_studio_id) != 0){

                            $first_company_detail_obj = new Tyr_company_detail_entity();
                            $first_company_detail_obj->studio_id = $company_studio_id;
                            $first_company_detail_obj->get_company_detail_by_studio();
                            $company_parent_id = $first_company_detail_obj->company_id;

                            $company_detail_obj = new Tyr_company_detail_entity();
                            $company_detail_obj->parent_id = $first_company_detail_obj->company_id;
                            $company_detail_obj->company_country = $country_id;
                            $company_detail_obj->company_city = $city_id;
                            $is_company_exist = $company_detail_obj->get_company_count_by_parent_id_and_location();
                        }

                        if($is_company_exist['co_cnt'] == 0){
                            $new_users_obj->role_id = 3;
                            $new_users_obj->job_title = $job_title;
                            $company_insert_status = 1;
                            $per = '';
                            $modules = @explode(',', STUDIO_USER_MODULES);
                        } else {
                            $new_users_obj->role_id = 4;
                            $new_users_obj->job_title = $job_title;
                            $new_users_obj->parent_id = $is_company_exist['company_owner'];
                            $per = '';
                            $modules = @explode(',', REVIEWER_USER_MODULES);
                        }
                    } else {
                        //$new_users_obj->status_sl = 1;
                        $modules = @explode(',', TYROE_FREE_USER_MODULES);
                    }

                    #Insert new User
                    $new_users_obj->save_user();
                }
                $user_details = (array)$new_users_obj;
                
                //getting just registered user and checking if role id is 2 then add this user in the featured table
                $role_users_obj = new Tyr_users_entity();
                $role_users_obj->user_id = $new_users_obj->user_id;
                $role_users_obj->get_role_id_by_user_id();
                $reg_user = (array) $role_users_obj;
                
                if($reg_user['role_id'] == 2){
                    $featured_obj = new Tyr_featured_tyroes_entity(); 
                    $featured_obj->featured_tyroe_id = $new_users_obj->user_id;
                    $featured_obj->featured_status = 0;
                    $featured_obj->save_featured_tyroes();
                }

                if ($new_users_obj->user_id > 0) {

                    //Update invitation status
                    $invite_user_id = $this->input->post("invite_user_id");
                    $coupon_allot_id = $this->input->post("invite_coupon_id");
                    if(!empty($invite_user_id) && !empty($coupon_allot_id)){
                       //Update invite friend table
                        $invite_friend_obj = new Tyr_invite_friend_entity();
                        $invite_friend_obj->user_id = $invite_user_id;
                        $invite_friend_obj->coupon_id = $coupon_allot_id;
                        $invite_friend_obj->status_sl = 1;
                        $invite_friend_obj->update_status_sl();

                        //Update Coupon Allot table
                        $coupons_allot_obj = new Tyr_coupons_allot_users_entity();
                        $coupons_allot_obj->allot_id = $coupon_allot_id;
                        $coupons_allot_obj->user_id = $invite_user_id;
                        $coupons_allot_obj->coupon_status = 1;
                        $coupons_allot_obj->update_status_sl();
                    }


                    if($company_insert_status == 1){
                        $company_detail_new_obj = new Tyr_company_detail_entity();
                        $company_detail_new_obj->studio_id = $new_users_obj->user_id;
                        $company_detail_new_obj->company_name = $company_name;
                        $company_detail_new_obj->company_job_title = $job_title;
                        $company_detail_new_obj->parent_id = $company_parent_id;
                        $company_detail_new_obj->company_country = $country_id;
                        $company_detail_new_obj->company_city = $city_id;
                        $company_detail_new_obj->created_by = $new_users_obj->user_id;
                        $company_detail_new_obj->updated_by = $new_users_obj->user_id;
                        $company_detail_new_obj->save_company_detail();
                    }

                    //Add modules
                    //$modules = @explode(',', TYROE_FREE_USER_MODULES);
                   
                    foreach ($modules as $k => $v) {
                        $table_per_obj = new Tyr_permissions_entity();
                        $table_per_obj->user_id = $new_users_obj->user_id;
                        $table_per_obj->module_id = $v;
                        $table_per_obj->created_by = $new_users_obj->user_id;
                        $table_per_obj->updated_by = $new_users_obj->user_id;
                        $table_per_obj->save_permissions();
                    }
                    
                    //ADD NOTIFICATION DETAILS
                    $schedule_notification = @explode(',', SCHEDULE_NOTIFICATIONS);
                    foreach ($schedule_notification as $k => $v) {
                        $schedule_notification_obj = new Tyr_schedule_notification_entity();
                        $schedule_notification_obj->user_id = $new_users_obj->user_id;
                        $schedule_notification_obj->option_id = $v;
                        $schedule_notification_obj->created_by = $new_users_obj->user_id;;
                        $schedule_notification_obj->updated_by = $new_users_obj->user_id;
                        $schedule_notification_obj->save_schedule_notification();
                    }
                    //Visiblity Section DETAILS


                    $visiblity = @explode(',', VISIBILITY_SECTION);
                    foreach ($visiblity as $k => $v) {
                        $visibility_options_obj = new Tyr_visibility_options_entity();
                        $visibility_options_obj->user_id = $new_users_obj->user_id;
                        $visibility_options_obj->visible_section = $v;
                        $visibility_options_obj->visibility = "0";
                        $visibility_options_obj->created_at = strtotime("now");
                        $visibility_options_obj->created_by = $new_users_obj->user_id;
                        $visibility_options_obj->updated_by = $new_users_obj->user_id;
                        $visibility_options_obj->status_sl = "1";
                        $visibility_options_obj->save_visibility_options();
                    }

                    //Theme Section Details
                    $profile_theme_arr = array('text'=>'#323A45', 'background'=>'', 'icons'=>'#D0DBE2', 'icons_rollover'=>'#F97E76');
                    $featured_theme_arr = array('text'=>'#323A45', 'backgroundA'=>'#F97E76', 'backgroundB'=>'#E9F0F4');
                    $gallery_theme_arr = array('text'=>'#323A45', 'background'=>'#FFFFFF', 'overlay'=>'rgba(255, 255 ,255, 0.5)', 'button'=>'#F97E76');
                    $resume_theme_arr = array('text'=>'#323A45', 'background'=>'#E9F0F4');
                    $footer_theme_arr = array('icon'=>'#D0DBE2', 'icon_rollover'=>'#F97E76', 'backgroundA'=>'#323A45');
                    
                    $custome_theme_entity_obj = new Tyr_custome_theme_entity();
                    $custome_theme_entity_obj->user_id = $new_users_obj->user_id;
                    $custome_theme_entity_obj->profile_theme = serialize($profile_theme_arr);
                    $custome_theme_entity_obj->featured_theme = serialize($featured_theme_arr);
                    $custome_theme_entity_obj->gallery_theme = serialize($gallery_theme_arr);
                    $custome_theme_entity_obj->resume_theme = serialize($resume_theme_arr);
                    $custome_theme_entity_obj->footer_theme = serialize($footer_theme_arr);
                    $custome_theme_entity_obj->status_sl = '1';
                    $custome_theme_entity_obj->created_by = $new_users_obj->user_id;
                    $custome_theme_entity_obj->updated_by = $new_users_obj->user_id;
                    $custome_theme_entity_obj->save_custome_theme();
                    
                    $this->data['page_title'] = 'Thanks For Registering';
                    if($user_details['role_id'] == 3){
                        $this->data['role_id'] = 3;
                    } else if ($user_details['role_id'] == 4){
                        $this->data['role_id'] = 4;
                    }
                    
//                    if(isset($_POST['is_company_invite_active']) && $_POST['is_company_invite_active'] == 1){
//                        $invite_job_id = $this->input->post('invite_job_id');
//                        $job_role_mapping = new Tyr_job_role_mapping_entity();
//                        $job_role_mapping->job_id = $invite_job_id;
//                        $job_role_mapping->user_id = $user_details['user_id'];
//                        $job_role_mapping->is_admin = 0;
//                        $job_role_mapping->is_reviewer = 1;
//                        $job_role_mapping->save_job_role_mapping();
//                    }
//                    
                    //$this->template_arr = array('header_login', 'contents/register', 'footer');
                    //$this->load_template();
                    $this->session->set_userdata('reg_user_email', $this->input->post("user_email"));
                    $this->session->set_userdata('reg_user_password', $this->input->post("user_password"));
                    //redirect(LIVE_SITE_URL.'login/login_auth/'.str_rot13($this->input->post("user_email")).'/'.str_rot13($this->input->post("user_password")));
                    redirect(LIVE_SITE_URL.'login/login_auth');
                }
            }
        }
    }
    
    public function getcities() {
        $country_id = $this->input->post('country_id');
        $cities_not_in = 0;
        $cities = array();
        if(isset($_REQUEST['source']) && $_REQUEST['source'] == 'register'){
            $parent_company_id = $this->input->post('parent_company_id');
            if($parent_company_id > 0){
                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->studio_id = $parent_company_id;
                $company_detail_obj->get_company_detail_by_studio();

                $joins_obj  = new Tyr_joins_entity();
                $query_studio_city_ids = $joins_obj->get_studio_locations_city_ids($company_detail_obj->company_id);
                $cities_not_in = $query_studio_city_ids['city_id'];
            }
        }
        
        //echo $cities_not_in;

        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $country_id . "'");
        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $country_id . "' ORDER BY city ASC");

        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_cities_by_country_where($cities_not_in);

        $cities = $this->dropdown($cities, '', array('name' => 'city', 'placeholder' => 'City', 'id' => 'city', 'class' => 'city'));
        echo json_encode(array('dropdown' => $cities));
    }
}
