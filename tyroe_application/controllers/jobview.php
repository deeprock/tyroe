<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);

class Jobview extends CI_Controller{
    
    public function Jobview() {
        parent::__construct();
    }
    
    public function index($job_id){
        if($this->session->userdata('logged_in')){
            if($this->session->userdata('role_id') == 3 || $this->session->userdata('role_id') == 4){
                redirect(LIVE_SITE_URL.'openings/'.$job_id);
            }else if($this->session->userdata('role_id') == 2){
                redirect(LIVE_SITE_URL.'openings');
            }else{
                redirect(LIVE_SITE_URL.'register');
            }
        }else{
             redirect(LIVE_SITE_URL.'register');
        }
    }
    
}


