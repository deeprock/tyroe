<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'entities/tyr_fb_users_entity' . EXT);
class Googleclient extends Tyroe_Controller
{
    public function Linkedinclient()
    {
       parent::__construct();
      /// $this->load->library('Google_Client'); NOT WORKING!

    }
    public function index()
    {
        $user_ip = $this->input->ip_address();
        $user_id = $this->session->userdata("user_id");

        $this->load->library('Google_Client');
        $client = new Google_Client();
        $plus = new Google_PlusService($client);



        $code =  $this->input->get('code');
        if($code) {
            $client->authenticate($code);
            $access_toke = $this->session->set_userdata('access_token',$client->getAccessToken());

        }
        $access_token = $this->session->userdata('access_token');

        if(isset($access_token) && !empty($access_token) ) {
            $client->setAccessToken($access_token);
        }

        if($access_token){

            $token_data = json_decode($access_token,true);
            $me = $plus->people->get('me');
            $gplus_userid = $me['id'];
            $gplus_displayName = $me['displayName'];
            $gplus_token = $token_data['access_token'];

            $save_tooken_profiledata = json_encode(array_merge($token_data,$me));


//            $this->db->select('*');
//            $this->db->from(TABLE_FB_USERS);
//            $multi_where = array( 'user_id' => $user_id, 'socialmedia_type' => 'gp');
//            $this->db->where($multi_where);
//            $exists_record = $this->db->get()->row_array();
            
            $social_media_obj = new Tyr_fb_users_entity();
            $social_media_obj->user_id = $user_id;
            $social_media_obj->socialmedia_type = 'gp';
            $social_media_obj->get_user_social_media_details();


            //  if($this->db->now_rows > 0) {
            if($social_media_obj->id > 0) {
                
	         if($social_media_obj->status_sl == 1){
			$update_status = 0;
		  }else{
		  	$update_status = 1;
		  }
  		  $update_data = array();
//                $update_data['socialmedia_id'] = $gplus_userid;
//                $update_data['user_id'] = $user_id;
//                $update_data['access_token'] = $gplus_token;
//                $update_data['ip'] = $user_ip;
//                $update_data['user_data'] = $save_tooken_profiledata;
//                $update_data['socialmedia_type'] = 'gp';
//                $update_data['status_sl'] = $update_status ;
//                $update_data['access_date'] = date("Y-m-d H:i:s");
//                $this->db->where($multi_where);
//                $this->db->update(TABLE_FB_USERS,$update_data);
                  
                $social_media_obj->socialmedia_id = $gplus_userid;
                $social_media_obj->access_token = $gplus_token;
                $social_media_obj->ip = $user_ip;
                $social_media_obj->user_data = $save_tooken_profiledata;
                $social_media_obj->status_sl = 1;
                $social_media_obj->access_date = date("Y-m-d H:i:s");
                $social_media_obj->update_user_social_media_details();
            } else {
//                $insert_data = array();
//                $insert_data['socialmedia_id'] = $gplus_userid;
//                $insert_data['user_id'] = $user_id;
//                $insert_data['access_token'] = $gplus_token;
//                $insert_data['ip'] = $user_ip;
//                $update_data['user_data'] = $save_tooken_profiledata;
//                $insert_data['socialmedia_type'] = 'gp';
//                $insert_data['status_sl'] = '1';
//                $insert_data['access_date'] = date("Y-m-d H:i:s");
//                $this->db->insert(TABLE_FB_USERS,$insert_data);
                $social_media_obj = new Tyr_fb_users_entity();
                $social_media_obj->user_id = $user_id;
                $social_media_obj->socialmedia_type = 'gp';
                $social_media_obj->socialmedia_id = $gplus_userid;
                $social_media_obj->access_token = $gplus_token;
                $social_media_obj->user_data = $save_tooken_profiledata;
                $social_media_obj->ip = $user_ip;
                $social_media_obj->status_sl = 1;
                $social_media_obj->access_date = date("Y-m-d H:i:s");
                $social_media_obj->save_fb_users();
            }
            $this->db->last_query();
            echo '<script type="text/javascript">  window.opener.location.reload();  setTimeout(function () {  window.close(); } , 100);    </script>';


        } else {
            $authUrl = $client->createAuthUrl();
            redirect($authUrl);

        }


    }

}
