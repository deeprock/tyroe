<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);

class Reviewer extends Tyroe_Controller
{
    public function Reviewer()
    {

        parent::__construct();
        $user_id = $this->session->userdata("user_id");
        $this->data['page_title'] = 'Reviewer';
        if ($this->session->userdata('role_id') != ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
        $get_countries = $this->getAll("SELECT country_id,country FROM " . TABLE_COUNTRIES);
        $this->data['get_countries'] = $get_countries;
        $this->load->library('form_validation');

        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $this->data['total_reviewers'] = $users_obj->get_total_reviewer_count();
        
        //$this->data['total_reviewers'] = $this->getRow("SELECT COUNT(*) AS total   FROM " . TABLE_USERS . " a, " . TABLE_USERS . " b WHERE a.parent_id = b.user_id AND  b.user_id='" . $user_id . "' and a.status_sl='1' ");

    }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
//        $get_user = $this->getAll("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                         username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                         state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                          " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                       LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                       LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                       WHERE " . TABLE_USERS . ".parent_id='" . $user_id . "'and " . TABLE_USERS . ".status_sl='1'");
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_all_reviewers($user_id);
        $this->data['get_reviewer'] = $get_user;

        $this->template_arr = array('header', 'contents/get_reviewers', 'footer');
        $this->load_template();
    }

    public function CreateReviewer()
    {

        $this->template_arr = array('header', 'contents/reviewer_form', 'footer');
        $this->load_template();
    }

    public function EditReviewer($edit_id)
    {
        $user_id = $this->session->userdata("user_id");
        if ($edit_id != '') {
//            $get_user = $this->getRow("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                         username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                         state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                          " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".image_id, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                       LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                       LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                       WHERE " . TABLE_USERS . ".user_id='" . $edit_id . "'");
            $joins_obj = new Tyr_joins_entity();
            $get_user = $joins_obj->get_user_personal_info_1($edit_id);
            

            $image;
            if ($get_user['media_name']) {
                $image = ASSETS_PATH_REVIEWER_THUMB . $get_user['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }


            $new_pass = "";
            for ($i = 1; $i <= $get_user['password_len']; $i++) {
                $new_pass .= " ";
            }

            $user_data = array(
                "user_id" => $get_user['user_id'],
                "created_timestamp" => $get_user['created_timestamp'],
                "email" => $get_user['email'],
                "username" => $get_user['username'],
                "firstname" => $get_user['firstname'],
                "lastname" => $get_user['lastname'],
                "password" => $new_pass,
                "image" => $image,
                "password_len" => $get_user['password_len'],
                "state" => $get_user['state'],
                "city" => $get_user['city'],
                "zip" => $get_user['zip'],
                "address" => $get_user['address'],
                "phone" => $get_user['phone'],
                "cellphone" => $get_user['cellphone'],
                "country" => $get_user['country'],
                "country_id" => $get_user['country_id'],
                "image_id" => $get_user['image_id']
            );


            $this->data['get_reviewer'] = $user_data;
        }
        $this->template_arr = array('header', 'contents/reviewer_formedit', 'footer');
        $this->load_template();
    }

    public function SaveReviewer()
    {
        echo "d";die;
        #Validation
        $config_validation = array(
            array(
                'field' => 'user_name',
                'label' => LABEL_USER_NAME,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_country',
                'label' => LABEL_COUNTRY,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_address',
                'label' => LABEL_ADDRESS,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_phone',
                'label' => LABEL_PHONE,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_cellphone',
                'label' => LABEL_CELLPHONE,
                'rules' => 'trim|required'
            ),

            array(
                'field' => 'user_firstname',
                'label' => LABEL_FIRST_NAME,
                'rules' => 'required'
            ),
            array(
                'field' => 'user_lastname',
                'label' => LABEL_LAST_NAME,
                'rules' => 'required'
            ),
            array(
                'field' => 'user_password',
                'label' => LABEL_PASSWORD,
                'rules' => 'required'
            ),
            array(
                'field' => 'user_confirmpass',
                'label' => LABEL_CONFIRM_PASSWORD,
                'rules' => 'required,required|matches[user_confirmpass]'
            )
        );

        $this->form_validation->set_rules($config_validation);
        #Not Validate
        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = "";
            $this->template_arr = array('header', 'contents/reviewer_form', 'footer');
            $this->load_template();
        } #Validate
        else {
            $user_id = $this->session->userdata("user_id");
            $username = $this->input->post("user_name");
            $useremail = $this->input->post("user_email");
            //$duplicate_username=$this->Execute("Select username from ". TABLE_USERS ." where username='".$username."' and email='".$useremail."'");
            //$duplicate_email = $this->Execute("Select username from " . TABLE_USERS . " where email='" . $useremail . "'");
            
            $users_obj = new Tyr_users_entity();
            $users_obj->email = $useremail;
            $duplicate_email = $users_obj->check_duplicate_user_email();
            
            if ($duplicate_email->num_rows() > 0) {
                $this->data['msg'] = LABEL_EMAIL_ALREADY_REGISTERED;
                $this->template_arr = array('header', 'contents/reviewer_form', 'footer');
                $this->load_template();
            } else {
//                $user_details = array(
//                    'parent_id' => $user_id,
//                    'role_id' => "4",
//                    'username' => $this->input->post("user_name"),
//                    'email' => $this->input->post("user_email"),
//                    'firstname' => $this->input->post("user_firstname"),
//                    'lastname' => $this->input->post("user_lastname"),
//                    'password' => $this->hashPass($this->input->post("user_confirmpass")),
//                    'password_len' => strlen($this->input->post("user_confirmpass")),
//                    'country_id' => $this->input->post("user_country"),
//                    'state' => $this->input->post("user_state"),
//                    'city' => $this->input->post("user_city"),
//                    'zip' => $this->input->post("user_zipcode"),
//                    'address' => $this->input->post("user_address"),
//                    'phone' => $this->input->post("user_phone"),
//                    'cellphone' => $this->input->post("user_cellphone"),
//                    'created_timestamp' => strtotime("now")
//                );
//
//                $return_user = $this->insert(TABLE_USERS, $user_details);
                
                $users_obj = new Tyr_users_entity();
                $users_obj->parent_id = $user_id;
                $users_obj->role_id = 4;
                $users_obj->username = $this->input->post("user_name");
                $users_obj->email = $this->input->post("user_email");
                $users_obj->firstname = $this->input->post("user_firstname");
                $users_obj->lastname = $this->input->post("user_lasttname");
                $users_obj->country_id = $this->input->post("user_country");
                $users_obj->city = $this->input->post("user_city");
                $users_obj->phone = $this->input->post("user_phone");
                $users_obj->cellphone = $this->input->post("user_cellphone");
                $users_obj->address = $this->input->post("user_address");
                $users_obj->password = $this->hashPass($this->input->post("user_confirmpass"));
                $users_obj->password_len = strlen($this->input->post("user_confirmpass"));
                $users_obj->created_timestamp = strtotime("now");
                $users_obj->modified_timestamp = strtotime("now");
                $users_obj->save_user();
                $return_user = $users_obj->user_id;
                
                
                #permission for Reviewers
               /* $modules = @explode(',', REVIEWER_USER_MODULES);
                foreach ($modules as $k => $v) {
                    $user_modules = array(
                        'user_id' => $return_user,
                        'module_id' => $v);
                    $this->insert(TABLE_PERMISSIONS, $user_modules);
                }*/

                $image_name = $this->input->post("image");

//                $image_detail = array(
//                    "user_id" => $return_user,
//                    "profile_image" => '1',
//                    "media_type" => 'image',
//                    "media_name" => $image_name,
//                    'created' => strtotime("now")
//                );
//                $return_user_image = $this->insert(TABLE_MEDIA, $image_detail);
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $return_user;
                $media_obj->profile_image = 1;
                $media_obj->media_type = 'image';
                $media_obj->media_name = $image_name;
                $media_obj->save_media();
                $return_user_image = $media_obj->image_id;
                
                if ($return_user > 0) {
                    header("location:" . $this->getURL("reviewer"));
                }
            }
        }
    }

    public function UpdateReviewer()
    {

        #Validation
        $config_validation = array(
            array(
                'field' => 'user_name',
                'label' => LABEL_USER_NAME,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_country',
                'label' => LABEL_COUNTRY,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_address',
                'label' => LABEL_ADDRESS,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_phone',
                'label' => LABEL_PHONE,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_cellphone',
                'label' => LABEL_CELLPHONE,
                'rules' => 'trim|required'
            ),

            array(
                'field' => 'user_firstname',
                'label' => LABEL_FIRST_NAME,
                'rules' => 'required'
            ),
            array(
                'field' => 'user_lastname',
                'label' => LABEL_LAST_NAME,
                'rules' => 'required'
            ),

        );

        $this->form_validation->set_rules($config_validation);
        #Not Validate
        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = "";
            $this->template_arr = array('header', 'contents/reviewer_form', 'footer');
            $this->load_template();
        } #Validate
        else {
            $user_id = $this->session->userdata("user_id");
            $edit_id = $this->input->post("edit_id");
//            
//            $user_details = array(
//                'parent_id' => $user_id,
//                'role_id' => "4",
//                'username' => $this->input->post("user_name"),
//                'email' => $this->input->post("user_email"),
//                'firstname' => $this->input->post("user_firstname"),
//                'lastname' => $this->input->post("user_lastname"),
//                'password' => $this->hashPass($this->input->post("user_confirmpass")),
//                'password_len' => strlen($this->input->post("user_confirmpass")),
//                'country_id' => $this->input->post("user_country"),
//                'state' => $this->input->post("user_state"),
//                'city' => $this->input->post("user_city"),
//                'zip' => $this->input->post("user_zipcode"),
//                'address' => $this->input->post("user_address"),
//                'phone' => $this->input->post("user_phone"),
//                'cellphone' => $this->input->post("user_cellphone"),
//                'created_timestamp' => strtotime("now")
//            );
//            $return_user;
//            
//            $where = "user_id ='" . $edit_id . "'";
//            $return_user = $this->update(TABLE_USERS, $user_details, $where);
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $edit_id;
            $users_obj->get_user();
            
            $users_obj->parent_id = $user_id;
            $users_obj->role_id = 4;
            $users_obj->username = $this->input->post("user_name");
            $users_obj->email = $this->input->post("user_email");
            $users_obj->firstname = $this->input->post("user_firstname");
            $users_obj->lastname = $this->input->post("user_lasttname");
            $users_obj->country_id = $this->input->post("user_country");
            $users_obj->city = $this->input->post("user_city");
            $users_obj->phone = $this->input->post("user_phone");
            $users_obj->cellphone = $this->input->post("user_cellphone");
            $users_obj->address = $this->input->post("user_address");
            $users_obj->password = $this->hashPass($this->input->post("user_confirmpass"));
            $users_obj->password_len = strlen($this->input->post("user_confirmpass"));
            $users_obj->modified_timestamp = strtotime("now");
            $return_user = $users_obj->update_users();
            

            $image_name = $this->input->post("image");
            $image_id = $this->input->post("image_id");
            $image_detail = array(
                "user_id" => $edit_id,
                "profile_image" => '1',
                "media_type" => 'image',
                "media_name" => $image_name,
                'modified' => strtotime("now")
            );
            $where = "user_id ='" . $edit_id . "'";
            if ($image_id == "") {
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $edit_id;
                $media_obj->profile_image = 1;
                $media_obj->media_type = 'image';
                $media_obj->media_name = $image_name;
                $media_obj->save_media();
                $return_user_image = $media_obj->image_id;
                //$return_user_image = $this->insert(TABLE_MEDIA, $image_detail);
            } else {
                
                $media_obj = new Tyr_media_entity();
                $media_obj->image_id = $image_id;
                $media_obj->get_media();
                $media_obj->user_id = $edit_id;
                $media_obj->profile_image = 1;
                $media_obj->media_type = 'image';
                $media_obj->media_name = $image_name;
                $media_obj->update_media();
                $return_user_image = $media_obj->image_id;
                //$return_user_image = $this->update(TABLE_MEDIA, $image_detail, $where);
            }
            
            if ($return_user) {
                header("location:" . $this->getURL("reviewer"));
            }
        }
    }

    public function DeleteReviewer()
    {
//        $where = "user_id ='" . $this->input->post("user_id") . "'";
//        $update = array(
//            "status_sl" => "-1",
//            "modified_timestamp" => strtotime("now")
//        );
//        $return = $this->update(TABLE_USERS, $update, $where);
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $this->input->post("user_id");
        $users_obj->get_user();
        $users_obj->status_sl = -1;
        $users_obj->modified_timestamp = strtotime("now");
        $return = $users_obj->update_users();

        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function upload_image()
    {
        $video_ext_array = array('avi', 'mpg', 'mpeg', 'wmv', 'mp4', '3gp', 'flv', 'mp3', 'AVI', 'MPG', 'MPEG', 'WMV', 'MP4', '3GP', 'FLV', 'MP3');
        $pathinfo = pathinfo($_FILES['Filedata']['name']);
        $pathinfo_ext = $pathinfo['extension'];
        $verifyToken = md5('unique_salt' . $this->input->post('timestamp'));

        $image_upload_folder = 'assets/uploads/reviewer';
        $image_upload_folder_thumb = 'assets/uploads/reviewer/thumb';

        if (!file_exists($image_upload_folder)) {
            mkdir($image_upload_folder, DIR_WRITE_MODE, true);
        }
        $this->upload_config = array(
            'upload_path' => $image_upload_folder,
            'allowed_types' => '*',
            'max_size' => 0,
            'remove_space' => TRUE,
            'encrypt_name' => TRUE,
        );
        $this->upload->initialize($this->upload_config);

        if (!empty($_FILES) && $this->input->post('token') == $verifyToken) {
            if (!$this->upload->do_upload('Filedata')) {
                $upload_error = $this->upload->display_errors();
                echo json_encode($upload_error);
            } else {
                $file_info = $this->upload->data();
                $resp[] = $file_info;

                /*=======  Making Thumbnail of Image  ========*/
                $config = array(
                    'source_image' => $file_info['full_path'], //path to the uploaded image
                    'new_image' => $image_upload_folder_thumb, //path to
                    'maintain_ratio' => true,
                    'width' => 273,
                    'height' => 206,
                    'create_thumb' => TRUE
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();

            }

            echo json_encode($resp);
        }
    }
}
