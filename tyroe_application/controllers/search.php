<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);

class Search extends Tyroe_Controller {

    public function Search() {

        parent::__construct();
        $this->data['page_title'] = 'Search';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_SEARCH . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_SEARCH . ') ');
        }
    }

    public function index() {
        //check if the user has full profile access
        $parent_id = $this->session->userdata("user_id");
        if(intval($this->session->userdata("parent_id")) != 0){
            $parent_id = $this->session->userdata("parent_id");
        }
        
        $this->_add_css('payment.css');
        
        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $this->data['has_profile_access'] = $profile_access_obj->id == 0 ? 0 : 1;
        
        $this->data['page_number'] = $this->input->post('page');
        $user_id = $this->session->userdata("user_id");
        //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(tyroe_id ORDER BY tyroe_id ASC SEPARATOR ',')
        //AS tyroe_id,studio_id FROM " . TABLE_HIDE_USER . " WHERE studio_id='" . $user_id . "'  AND status_sl = 1  GROUP BY studio_id");
        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->studio_id = $user_id;
        $hidden_obj->status_sl = 1;
        $hiden_tyroes = $hidden_obj->get_hidden_tyroes_by_studio_id();
        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['tyroe_id'];
        }

        //join featured usered from home
        if ($this->input->post('home_featured') == 'featured') {
            $featured_join = "RIGHT JOIN " . TABLE_FEATURED_TYROE . " ON "
                    . TABLE_USERS . ".user_id = " . TABLE_FEATURED_TYROE . ".featured_tyroe_id  AND " . TABLE_FEATURED_TYROE . ".featured_status = 1";
        }
        //End join featured usered from home
//        $total_rows = $this->getRow
//                ("SELECT COUNT(*)AS cnt FROM " . TABLE_USERS . " " . $featured_join . "
//                      LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                      LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                      AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//                      WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND "
//                       . TABLE_USERS . ".publish_account = '1' AND tyr_users.user_id NOT IN (" . $hidden_user . ")");

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_featured_user_row_count($featured_join, $hidden_user);

        $pagintaion_limit = $this->pagination_limit();
        //$this->data['pagintaion_limit'] = $pagintaion_limit;
        /* $per_page = 5;
          $pages = ceil($total_rows['cnt'] / $per_page);
          $this->data['pages'] = $pages; */

        //forsingle pagination
//        $get_rows_for_pagination = $this->getAll("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//              username,firstname,lastname,address,city,availability,
//              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . " " . $featured_join . "
//              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' AND tyr_users.user_id NOT IN (" . $hidden_user . ") ORDER BY user_id DESC ");
        $get_rows_for_pagination = $joins_obj->get_all_featured_users_detail_page($featured_join, $hidden_user);
        $this->data['get_rows_for_pagination'] = $get_rows_for_pagination;
        //end for single page pagination
//        $get_tyroes = $this->getAll
//                ("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//              username,firstname,lastname,address,availability,extra_title,
//              state,zip," . TABLE_CITIES . ".city," . TABLE_INDUSTRY . ".industry_name," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . " " . $featured_join . "
//              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
//              LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
//              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' AND tyr_users.user_id NOT IN (" . $hidden_user . ") ORDER BY user_id DESC " . $pagintaion_limit['limit']);
        $get_tyroes = $joins_obj->get_all_featured_tyroe_detail_paginated($featured_join, $hidden_user, $pagintaion_limit);
        // foreach ($get_tyroes as $tyroes) {
        
        if($get_tyroes != ''){
            for ($i = 0; $i < count($get_tyroes); $i++) {

    //            $get_tyroes[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
    //                         FROM " . TABLE_SKILLS . " LEFT JOIN
    //                         " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
    //                         WHERE user_id='" . $get_tyroes[$i]['user_id'] . "' AND status_sl=1");

                $get_tyroes[$i]['skills'] = $joins_obj->get_all_user_skill($get_tyroes[$i]['user_id']);

                #Portfolio/Media
    //            $get_tyroes[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_tyroes[$i]['user_id'] . "' AND status_sl='1'"
    //                    . " AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $get_tyroes[$i]['user_id'];
                $get_tyroes[$i]['portfolio'] = $media_obj->get_user_short_portfolio();


                /* FEATURED TYROE - TRIANGLE */
    //            $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '" . $get_tyroes[$i]['user_id'] . "'");
    //            $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                $featured_tyroes_obj->featured_tyroe_id = $get_tyroes[$i]['user_id'];
                $featured_tyroes_obj->get_user_featured_status();
                $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];

                /* FEATURED TYROE - TRIANGLE */
                /* Check is favourite or not */
    //            $is_favourite = $this->getRow("SELECT fav_profile_id FROM " . TABLE_FAVOURITE_PROFILE . " WHERE user_id = '" . $get_tyroes[$i]['user_id'] . "' AND"
    //                    . " studio_id = '" . $user_id . "' AND status_sl = 1");
    //            $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $get_tyroes[$i]['user_id'];
                $favourite_profile_obj->studio_id = $user_id;
                $favourite_profile_obj->status_sl = 1;
                $is_favourite = $favourite_profile_obj->get_favourite_profile_id();
                $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];

                /* End Checking favourite */
                /*             * Getting tyroe jobs* */
    //            $tyroe_jobs = $this->getAll("SELECt job_id FROM " . TABLE_JOB_DETAIL . " WHERE tyroe_id = '" . $get_tyroes[$i]['user_id'] . "'");
    //            $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;
                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;
                /*             * End getting tyroe jobs* */

                /*             * get tyroe total score* */
                //$get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM "
                //. TABLE_RATE_REVIEW . " WHERE tyroe_id = '" . $get_tyroes[$i]['user_id'] . "'");
                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();
                if (is_array($get_tyroes_total_score)) {
                    if ($get_tyroes_total_score['tyroe_score'] == NULL) {
                        $get_tyroes_total_score['tyroe_score'] = 0;
                    } else {
                        $get_tyroes[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    }
                } else {
                    $get_tyroes_total_score['tyroe_score'] = 0;
                }

                /*             * end get tyroe total score* */
            }
        }
        
        
        $where = "";
        if ($this->session->userdata("role_id") == '4') {
//            $get_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id,reviewer_id FROM "
//                    . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $user_id . "' GROUP BY reviewer_id");

            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
            $reviewer_jobs_obj->reviewer_id = $user_id;
            $get_jobs = $reviewer_jobs_obj->get_job_ids_by_reviewer_id();

            //if(empty($get_jobs['job_id'])){$get_jobs['job_id'] = -1;}
            if ($get_jobs['job_id'] == '') {
                $get_jobs['job_id'] = -1;
            }
            $where = " and job_id IN(" . $get_jobs['job_id'] . ")";
            //$where = " and user_id='" . $this->session->userdata("parent_id") . "'";
        } else if ($this->session->userdata("role_id") == '3') {
            $where = " and user_id='" . $user_id . "'";
        }

//        $jobs = $this->getAll("SELECT job_id,user_id,category_id,job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
//                         " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date,
//                           end_date,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl  FROM " . TABLE_JOBS . "
//                 LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                 LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//                           where " . TABLE_JOBS . ".status_sl='1'
//                           and archived_status='0'" . $where);

        $jobs = $joins_obj->get_all_jobs_details_1($where);

        $this->data['get_countries_dropdown'] = $this->dropdown($this->data['get_countries'], $this->data['get_user']["country_id"], array('name' => 'country', 'placeholder' => 'Country', 'id' => 'search_countries', 'onchange' => 'getCities(this)'));

        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $this->data['get_user']["country_id"] . "'");
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $this->data['get_user']["country_id"];
        $cities = $cities_obj->get_all_cities_by_country_id();
        $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City', 'id' => 'city_search'));

        //$get_industry = $this->getAll("SELECT industry_id AS id,industry_name AS name FROM " . TABLE_INDUSTRY);
        $industry_obj = new Tyr_industry_entity();
        $get_industry = $industry_obj->get_all_industries();
        $this->data['get_industry'] = $get_industry;

        //$get_levels = $this->getAll("SELECT job_level_id  AS id,level_title AS name FROM " . TABLE_JOB_LEVEL);
        $job_level_obj = new Tyr_job_level_entity();
        $get_levels = $job_level_obj->get_all_job_level();
        $this->data['get_levels'] = $get_levels;

        //$get_experience = $this->getAll("SELECT experienceyear_id  AS id,experienceyear AS expyear FROM " . TABLE_EXPERIENCE_YEARS);
        $experience_years_obj = new Tyr_experience_years_entity();
        $get_experience = $experience_years_obj->get_all_experience_years();
        $this->data['get_experience'] = $get_experience;


        $this->data['get_jobs'] = $jobs;
        $this->data['get_tyroes'] = $get_tyroes;
        // $this->data['get_portfolio'] = $portfolio;
        // $this->data['get_skills'] = $skills;

        $skill_obj = new Tyr_creative_skills_entity();
        $this->data['skills'] = $skill_obj->get_creative_skills_for_search();
        
        
        if ($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4) {
//            $this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '" . $user_id . "' and status_sl = 1"
//                    . " AND archived_status = 0");

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->status_sl = 1;
            $jobs_obj->archived_status = 0;
            $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
        }
        /* Add to opening dropdown */

        /* PAGINATION */
        $this->data['pagination'] = $this->pagination(array('url' => 'search/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->data['ha'] = $total_rows['cnt'];
        /* $this->template_arr = array('contents/search/search_tyr');
          $this->load_template(); */

        $this->template_arr = array('header', 'contents/search/search_tyroes', 'footer');
        $this->load_template();
    }

    public function getcities() {
        $country_id = $this->input->post('country_id');
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_all_cities_by_country_id();
        $cities = $this->dropdown($cities, '', array('name' => 'city', 'placeholder' => 'City', 'onchange' => 'citiesvalue(this.value)'));
        echo $cities;
        exit;
    }

    public function FilterTyroes() {
        // check if user has full profile access
        $parent_id = $this->session->userdata("user_id");
        if(intval($this->session->userdata("parent_id")) != 0){
            $parent_id = $this->session->userdata("parent_id");
        }
        
        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $this->data['has_profile_access'] = $profile_access_obj->id == 0 ? 0 : 1;
        
        $user_id = $this->session->userdata("user_id");
        $country_id = $this->input->post('country_id');
        $creative_skill_id = $this->input->post('creative_skill_id');
        $orderby = $this->input->post('orderby');


        if ($this->input->post('search_page_index') != '') {
            $page_start = $this->input->post('search_page_index');
            $page_end = 12;
            $search_limit = 'OFFSET '.$page_start.' LIMIT ' .$page_end;
        } else {
            $page_start = 0;
            $search_limit = '';
        }
        /* $page = $this->input->post('page');
          if ($page) {
          $page = $page;
          } else {
          $page = 1;
          } */

        $order_by;
        if (!empty($country_id)) {
            $where_country = " AND " . TABLE_USERS . ".country_id LIKE '%" . $country_id . "%'";
        }
        if (!empty($creative_skill_id)) {
            $where_skill = " AND skill IN(" . $creative_skill_id . ")";
        }
        if ($orderby == 'alphabet') {
            $order_by = " ORDER BY " . TABLE_USERS . ".username ASC";
        } else if ($orderby == 'recent') {
            $order_by = " ORDER BY " . TABLE_USERS . ".user_id DESC";
        }



        /* for studio post come from search tryoes */
        $view_request = $this->input->post('view_request');
        //creating where clause
        $industry_value = $this->input->post('industry_value');
        $industry_array = explode(',', $industry_value);
        if (count($industry_array) > 1) {
            $industry_where = " AND (";
            $a = 1;
            foreach ($industry_array as $industry) {
                if ($a > 1) {
                    $industry_where .= "   OR   " . TABLE_USERS . ".industry_id = '" . $industry . "'";
                } else {
                    $industry_where .= TABLE_USERS . ".industry_id = '" . $industry . "'";
                    $a++;
                }
            }
            $industry_where .= ") ";
        } else if (count($industry_array) == 1) {
            if ($industry_array[0] != '') {
                $industry_where = " AND (" . TABLE_USERS . ".industry_id = '" . $industry_array[0] . "') ";
            }
        }
        $experience = $this->input->post('experience');
        $experience_array = explode(',', $experience);
        if (count($experience_array) > 1) {
            $experience_where = " AND (";
            $a = 1;
            foreach ($experience_array as $experience) {
                if ($a > 1) {
                    $experience_where .= "   OR   " . TABLE_USERS . ".experienceyear_id = '" . $experience . "'";
                } else {
                    $experience_where .= TABLE_USERS . ".experienceyear_id = '" . $experience . "'";
                    $a++;
                }
            }
            $experience_where .= ") ";
        } else if (count($experience_array) == 1) {
            if ($experience[0] != '') {
                $experience_where = " AND (" . TABLE_USERS . ".experienceyear_id = '" . $experience[0] . "') ";
            }
        }
        $level = $this->input->post('level');
        $level_array = explode(',', $level);
        if (count($level_array) > 1) {
            $level_where = " AND (";
            $a = 1;
            foreach ($level_array as $level) {
                if ($a > 1) {
                    $level_where .= "   OR   " . TABLE_USERS . ".job_level_id = '" . $level . "'";
                } else {
                    $level_where .= TABLE_USERS . ".job_level_id = '" . $level . "'";
                    $a++;
                }
            }
            $level_where .= ") ";
        } else if (count($level_array) == 1) {
            if ($level[0] != '') {
                $level_where = " AND (" . TABLE_USERS . ".job_level_id = '" . $level[0] . "') ";
            }
        }


        $city_id = $this->input->post('city_id');
        if ($city_id != 'City' and $city_id != '') {
            $where_city = " AND " . TABLE_USERS . ".city = '" . $city_id . "'";
        }
        $periorty = $this->input->post('periorty');
        $world_id = $this->input->post('world_wide_value');
        $search_input = strtolower($this->input->post('search_input'));
        $availability = $this->input->post('availability');
        $availability_later = $this->input->post('availability_later');
        $not_availability = $this->input->post('not_availability');
        $show_hidden_tyroe_value = $this->input->post('show_hidden_tyroe_value');
        if ($periorty == 'featured') {
            $featured_join = "RIGHT JOIN " . TABLE_FEATURED_TYROE . " ON "
                    . TABLE_USERS . ".user_id = " . TABLE_FEATURED_TYROE . ".featured_tyroe_id  AND " . TABLE_FEATURED_TYROE . ".featured_status = 1";
        } else if ($periorty == 'most_recent') {
            $order_by = " ORDER BY " . TABLE_USERS . ".user_id DESC ";
        } else if ($periorty == 'alphabet') {
            $order_by = " ORDER BY " . TABLE_USERS . ".username ASC";
        } else if ($periorty == 'recent_updated') {
            $order_by = " ORDER BY " . TABLE_USERS . ".modified_timestamp DESC";
        } else if ($periorty == 'highest_score') {
            $selection = '((AVG(' . TABLE_RATE_REVIEW . '.technical))+(AVG(' . TABLE_RATE_REVIEW . '.creative))+(AVG(' . TABLE_RATE_REVIEW . '.impression)))/3 AS tyroe_score,';
            $rate_and_review_join = "LEFT JOIN " . TABLE_RATE_REVIEW . " ON " . TABLE_USERS . ".user_id = " . TABLE_RATE_REVIEW . ".tyroe_id";
            $order_by = " ORDER BY tyroe_score DESC";
            $group_by = " GROUP BY " . TABLE_USERS . ".user_id";
        }
        if ($world_id != 'Country' and $world_id != '') {
            $where_country = " AND " . TABLE_USERS . ".country_id = '" . $world_id . "' ";
        }
        
        $where_availability = '';
        if ($availability != '') {
            $where_availability = TABLE_USERS . ".availability = '1' ";
        }
        
        if($availability_later != ''){
            $where_availability = $where_availability == '' ? TABLE_USERS . ".availability = '2' " : $where_availability." or ".TABLE_USERS . ".availability = '2' ";
        }
        
        if($not_availability != ''){
            $where_availability = $where_availability == '' ? TABLE_USERS . ".availability = '0' " : $where_availability." or ".TABLE_USERS . ".availability = '0' ";
        }
        
        $where_availability = $where_availability == '' ? '' : " AND (".$where_availability.')';
        
        $skills = array();
        $skills_search = '';
        $skills_search_main = '';
        $skill_searched = $this->input->post('skills');
        if($skill_searched == '')$skill_searched = trim($search_input);
        if($skill_searched != ''){
            $skills_obj1 = new Tyr_creative_skills_entity();
            $skills = $skills_obj1->get_user_id_for_skill_search($skill_searched);
        }
        if(count($skills)  > 0 && $skills['user_id'] != ''){
            $skills_search = ' AND '.TABLE_USERS.'.user_id in ('.$skills['user_id'].') ';
            $skills_search_main = ' OR '.TABLE_USERS.'.user_id in ('.$skills['user_id'].') ';
        }

//        $hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(tyroe_id ORDER BY tyroe_id ASC SEPARATOR ',')AS tyroe_id,studio_id FROM " . TABLE_HIDE_USER
//                . " WHERE studio_id='" . $user_id . "'  AND status_sl = 1  GROUP BY studio_id");
        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->studio_id = $user_id;
        $hidden_obj->status_sl = 1;
        $hiden_tyroes = $hidden_obj->get_hidden_tyroes_by_studio_id();

        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['tyroe_id'];
        }
        $custom_where = '';
        $join_obj = new Tyr_joins_entity();
        if ($search_input != '') {
            $search_input = trim($search_input);
            /*             * For Search By Skill keyword* */
            //getting skills of user
//            $skill_ids = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_CREATIVE_SKILLS . ".creative_skill_id ORDER BY " . TABLE_CREATIVE_SKILLS
//                    . ".creative_skill_id ASC SEPARATOR ',')AS creative_skill_id  FROM " . TABLE_CREATIVE_SKILLS . " WHERE creative_skill LIKE '%"
//                    . $search_input . "%'");
            $creative_skills = new Tyr_creative_skills_entity();
            $skill_ids = $creative_skills->get_relative_creative_skill_ids_1($search_input);
            if ($skill_ids == '' || $skill_ids['creative_skill_id'] == '') {
                $skill_ids = array();
                $skill_ids['creative_skill_id'] = -1;
            }
            //getting skill users
//            $tyroe_id_by_skills = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_SKILLS . ".user_id  ORDER BY " . TABLE_SKILLS . ".user_id ASC SEPARATOR ',')"
//                    . " AS tyroe_id_by_skills FROM " . TABLE_SKILLS . " WHERE " . TABLE_SKILLS . ".skill IN(" . $skill_ids['creative_skill_id'] . ")");

            $skills_entity = new Tyr_skills_entity();
            $tyroe_id_by_skills = $skills_entity->get_users_for_skill_id_in($skill_ids);
            if ($tyroe_id_by_skills == '' || $tyroe_id_by_skills['tyroe_id_by_skills'] == '') {
                $tyroe_id_by_skills = array();
                $tyroe_id_by_skills['tyroe_id_by_skills'] = -1;
            }
            /*             * End  Search By Skill keyword* */
            /*             * For Search By Location  keyword* */
//            $country_ids_by_keywords = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_COUNTRIES . ".country_id ORDER BY " . TABLE_COUNTRIES
//                    . ".country_id ASC SEPARATOR ',') AS country_id  FROM " . TABLE_COUNTRIES . " WHERE country LIKE '%" . $search_input . "%'");

            $countries_obj = new Tyr_countries_entity();
            $country_ids_by_keywords = $countries_obj->get_all_country_id_by_keyword($search_input);
            if ($country_ids_by_keywords == '' || $country_ids_by_keywords['country_id'] == '') {
                $country_ids_by_keywords = array();
                $country_ids_by_keywords['country_id'] = -1;
            }
//            $city_ids_by_keywords = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_CITIES . ".city_id ORDER BY " . TABLE_CITIES . ".city_id"
//                    . " ASC SEPARATOR ',') AS city_id  FROM " . TABLE_CITIES . " WHERE city LIKE '%" . $search_input . "%'");
            $cities_obj = new Tyr_cities_entity();
            $city_ids_by_keywords = $cities_obj->get_all_cities_by_keyword($search_input);
            if ($city_ids_by_keywords == '' || $city_ids_by_keywords['city_id'] == '') {
                $city_ids_by_keywords = array();
                $city_ids_by_keywords['city_id'] = -1;
            }

//            $states_ids_by_keywords = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_STATES . ".states_id ORDER BY " . TABLE_STATES . ".states_id ASC"
//                    . " SEPARATOR ',') AS states_id  FROM " . TABLE_STATES . " WHERE states_name LIKE '%" . $search_input . "%'");
            $states_obj = new Tyr_states_entity();
            $states_ids_by_keywords = $states_obj->get_all_states_by_keyword($search_input);
            if ($states_ids_by_keywords == '' || $states_ids_by_keywords['states_id'] == '') {
                $states_ids_by_keywords = array();
                $states_ids_by_keywords['states_id'] = -1;
            }
            /*             * End  Search By Location  keyword* */
            /*             * For Search By Industry  keyword* */
//            $industry_ids_by_keywords = $this->getRow("SELECT GROUP_CONCAT(" . TABLE_INDUSTRY . ".industry_id ORDER BY " . TABLE_INDUSTRY . ".industry_id"
//                    . " ASC SEPARATOR ',') AS industry_id  FROM " . TABLE_INDUSTRY . " WHERE industry_name LIKE '%" . $search_input . "%'");

            $industry_obj = new Tyr_industry_entity();
            $industry_ids_by_keywords = $industry_obj->get_all_industries_by_keyword($search_input);
            if ($industry_ids_by_keywords == '' || $industry_ids_by_keywords['industry_id'] == '') {
                $industry_ids_by_keywords = array();
                $industry_ids_by_keywords['industry_id'] = -1;
            }
            /*             * End Search By Industry  keyword* */
            /*             * For Search By Tags  keyword* */

//            $tag_users_id = $this->getRow("SELECT
//                  GROUP_CONCAT(
//                    " . TABLE_MEDIA . ".user_id
//                    ORDER BY " . TABLE_MEDIA . ".user_id ASC SEPARATOR ','
//                  ) AS tags_user_id
//                FROM
//                  " . TABLE_GALLERY_TAGS . "
//                  LEFT JOIN " . TABLE_GALLERY_TAGS_REL . " ON
//                  " . TABLE_GALLERY_TAGS_REL . ".gallerytag_name_id =  " . TABLE_GALLERY_TAGS . ".tag_id
//                  LEFT JOIN " . TABLE_MEDIA . " ON
//                  " . TABLE_MEDIA . ".image_id = " . TABLE_GALLERY_TAGS_REL . ".media_image_id
//                WHERE " . TABLE_GALLERY_TAGS . ".tag_name LIKE '%" . $search_input . "%'
//                AND " . TABLE_GALLERY_TAGS_REL . ".status=1
//                ");
            $tag_users_id = $join_obj->get_tag_user_ids($search_input);
            if ($tag_users_id == '' || $tag_users_id['tags_user_id'] == '') {
                $tag_users_id['tags_user_id'] = -1;
            } else {
                $tag_users_id['tags_user_id'] = rtrim($tag_users_id['tags_user_id']);
            }
            $get_tag_id = $tag_users_id;

            /*             * End Search By Tags  keyword* */
            $custom_where = " AND (lower(" . TABLE_USERS . ".email) LIKE '%" . $search_input . "%'
                OR lower(" . TABLE_USERS . ".username) LIKE '%" . $search_input . "%'
                OR lower(CONCAT(" . TABLE_USERS . ".firstname, ' ', " . TABLE_USERS . ".lastname)) LIKE '%" . $search_input . "%'
                OR lower(" . TABLE_USERS . ".firstname) LIKE '%" . $search_input . "%'
                OR lower(" . TABLE_USERS . ".lastname) LIKE '%" . $search_input . "%'
                OR " . TABLE_USERS . ".user_id IN (" . $tyroe_id_by_skills['tyroe_id_by_skills'] . ")
                OR " . TABLE_USERS . ".country_id IN (" . $country_ids_by_keywords['country_id'] . ")
                OR " . TABLE_USERS . ".state IN (" . $states_ids_by_keywords['states_id'] . ")
                OR " . TABLE_USERS . ".city IN (" . $city_ids_by_keywords['city_id'] . ")
                OR " . TABLE_USERS . ".industry_id IN (" . $industry_ids_by_keywords['industry_id'] . ")
                OR " . TABLE_USERS . ".user_id IN (" . $get_tag_id['tags_user_id'] . ") ".
                    $skills_search_main . "
                )";
        }

//        $hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(tyroe_id ORDER BY tyroe_id ASC SEPARATOR ',')AS tyroe_id,studio_id FROM "
//                . TABLE_HIDE_USER . " WHERE studio_id='" . $user_id . "' AND status_sl = 1 GROUP BY studio_id");
        $hide_user_obj = new Tyr_hide_user_entity();
        $hide_user_obj->studio_id = $user_id;
        $hide_user_obj->status_sl = 1;
        $hiden_tyroes = $hide_user_obj->get_hidden_tyroes_by_studio_id();

        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['tyroe_id'];
        }
        if ($show_hidden_tyroe_value != '') {
            $tyroes_hidden_status = " AND " . TABLE_USERS . ".user_id IN (" . $hidden_user . ")";
        } else {
            $tyroes_hidden_status = " AND " . TABLE_USERS . ".user_id NOT IN (" . $hidden_user . ")";
        }

        //getting all number of tyroes without limit
        $total_tyroes_query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
                  username,firstname,lastname,address,availability,extra_title,
                  state,zip," . TABLE_CITIES . ".city ," . TABLE_INDUSTRY . ".industry_name," . TABLE_STATES . ".states_name," . TABLE_COUNTRIES . ".country, "
                . $selection . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
                  " . $featured_join . "
                  LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                  LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
                  LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
                  LEFT JOIN " . TABLE_STATES . " ON " . TABLE_USERS . ".state =" . TABLE_STATES . ".states_id
                  LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
                  AND " . TABLE_MEDIA . ".profile_image = 1 AND " . TABLE_MEDIA . ".status_sl= 1
                  " . $rate_and_review_join . "
                  WHERE " . TABLE_USERS . ".role_id = 2 AND " . TABLE_USERS . ".status_sl= 1 AND " . TABLE_USERS . ".publish_account = 1 "
                . $custom_where . $where_country . $where_city . $industry_where . $experience_where . $level_where . $where_availability .
                "
            " . $tyroes_hidden_status . $skills_search ." " . $group_by . " " . $order_by . " ";
        
        //var_dump($total_tyroes_query);
        $total_tyroes = $join_obj->get_all_tyroe_details_without_limit($total_tyroes_query);
        //end all number of tyroes without limit

        $get_tyroes_query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
                  username,firstname,lastname,address,availability,extra_title,
                  state,zip," . TABLE_CITIES . ".city ," . TABLE_INDUSTRY . ".industry_name," . TABLE_STATES . ".states_name," . TABLE_COUNTRIES . ".country, " . $selection . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
                  " . $featured_join . "
                  LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                  LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
                  LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
                  LEFT JOIN " . TABLE_STATES . " ON " . TABLE_USERS . ".state=" . TABLE_STATES . ".states_id
                  LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
                  AND " . TABLE_MEDIA . ".profile_image = 1 AND " . TABLE_MEDIA . ".status_sl= 1
                  " . $rate_and_review_join . "
                  WHERE " . TABLE_USERS . ".role_id= 2 AND " . TABLE_USERS . ".status_sl= 1 AND " . TABLE_USERS . ".publish_account = 1"
                . $custom_where . $where_country . $where_city . $industry_where . $experience_where . $level_where . $where_availability .
                "
                  " . $tyroes_hidden_status . $skills_search. " " . $group_by . " " . $order_by . " " . $search_limit . " ";
        
        //echo $get_tyroes_query;
        
        $get_tyroes = $join_obj->get_all_tyroe_details_with_limit($get_tyroes_query);
        // foreach ($get_tyroes as $tyroes) {
        if ($get_tyroes != '') {
            for ($i = 0; $i < count($get_tyroes); $i++) {
//                $get_tyroes[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                                  FROM " . TABLE_SKILLS . " LEFT JOIN
//                                  " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                                  WHERE user_id='" . $get_tyroes[$i]['user_id'] . "'" . $where_skill . " AND status_sl=1");
                $get_tyroes[$i]['skills'] = $join_obj->get_all_user_skill($get_tyroes[$i]['user_id']);
                
                #Portfolio/Media
                //$get_tyroes[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_tyroes[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' ORDER BY image_id DESC  LIMIT 0,3");
//                $get_tyroes[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_tyroes[$i]['user_id'] .
//                        "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND"
//                        . " media_type='image' ORDER BY image_id DESC LIMIT 3 ");

                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $get_tyroes[$i]['user_id'];
                $get_tyroes[$i]['portfolio'] = $media_obj->get_user_short_portfolio();
                /* FEATURED TYROE - TRIANGLE */

//                $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '" .
//                        $get_tyroes[$i]['user_id'] . "'");
//                $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];

                $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                $featured_tyroes_obj->featured_tyroe_id = $get_tyroes[$i]['user_id'];
                $featured_tyroes_obj->get_user_featured_status();
                $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];

                /* FEATURED TYROE - TRIANGLE */


                /* Check is favourite or not */
//                $is_favourite = $this->getRow("SELECT fav_profile_id FROM " . TABLE_FAVOURITE_PROFILE . " WHERE user_id = '" .
//                        $get_tyroes[$i]['user_id'] . "' AND studio_id = '" . $user_id . "' AND status_sl = 1");
//                $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];

                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $get_tyroes[$i]['user_id'];
                $favourite_profile_obj->studio_id = $user_id;
                $favourite_profile_obj->status_sl = 1;
                $is_favourite = $favourite_profile_obj->get_favourite_profile_id();
                $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];

                /* End Checking favourite */
                /*                 * Getting tyroe jobs* */
//                $tyroe_jobs = $this->getAll("SELECt job_id FROM " . TABLE_JOB_DETAIL . " WHERE tyroe_id = '" . $get_tyroes[$i]['user_id'] . "'");
//                $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;

                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;

                /*                 * End getting tyroe jobs* */
                /*                 * get tyroe total score* */
//                $get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS"
//                        . " tyroe_score  FROM " . TABLE_RATE_REVIEW . " WHERE tyroe_id = '" . $get_tyroes[$i]['user_id'] . "'");

                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();
                if ($get_tyroes_total_score == '' || $get_tyroes_total_score['tyroe_score'] == NULL) {
                    $get_tyroes_total_score['tyroe_score'] = 0;
                }
                $get_tyroes[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                /*                 * end get tyroe total score* */
            }
        }
//        if ($this->session->userdata("role_id") == 3) {
//            $this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '" . $user_id . "' and status_sl = 1 AND archived_status = 0");
//        } else if ($this->session->userdata("role_id") == 4) {
//            $this->data['get_openings_dropdown'] = $this->getAll("SELECT " . TABLE_JOBS . ".job_id, " . TABLE_JOBS . ".job_title FROM " . TABLE_REVIEWER_JOBS . " LEFT JOIN " . TABLE_JOBS . " ON " . TABLE_JOBS . ".`job_id` = " . TABLE_REVIEWER_JOBS . ".`job_id` WHERE " . TABLE_REVIEWER_JOBS . ".`reviewer_id` = '" . $this->session->userdata('user_id') . "' AND " . TABLE_REVIEWER_JOBS . ".studio_id = '" . $this->session->userdata('parent_id') . "'");
//        }

        if ($this->session->userdata("role_id") == 3) {
//            $this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '" . $user_id . "' and status_sl = 1"
//                    . " AND archived_status = 0");

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->status_sl = 1;
            $jobs_obj->archived_status = 0;
            $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
        } else if ($this->session->userdata("role_id") == 4) {
            $studio_id = $this->session->userdata('parent_id');
            $reviewer_id = $this->session->userdata('user_id');

//            $this->data['get_openings_dropdown'] = $this->getAll("SELECT " . TABLE_JOBS . ".job_id, " . TABLE_JOBS . ".job_title FROM "
//                    . TABLE_REVIEWER_JOBS . " LEFT JOIN " . TABLE_JOBS . " ON " . TABLE_JOBS . ".`job_id` = " . TABLE_REVIEWER_JOBS
//                    . ".`job_id` WHERE " . TABLE_REVIEWER_JOBS . ".`reviewer_id` = '" . $this->session->userdata('user_id')
//                    . "' AND " . TABLE_REVIEWER_JOBS . ".studio_id = '" . $this->session->userdata('parent_id') . "'");
            $this->data['get_openings_dropdown'] = $join_obj->get_jobs_openings_listing($studio_id, $reviewer_id);
        }
        /* Add to opening dropdown */
        /* PAGINATION */
        $this->data['page_start'] = $page_start;
        $this->data['search_page_number'] = $this->input->post('search_page_number');
        if ($this->session->userdata('pre_page')) {
            $this->data['pre_page'] = $this->session->userdata('pre_page');
        } else {
            $this->data['pre_page'] = -1;
        }

        $this->data['total_tyroes'] = $total_tyroes;
        $this->data['get_tyroes'] = $get_tyroes;



        if ($view_request == 'studio_search') {
            $this->template_arr = array('contents/search/filtered_tyroes_by_studio');
        } else {
            $this->template_arr = array('contents/search/filtered_tyroes');
        }
        $this->load_template();
    }

    public function send_email() {
        if ($this->input->post('sending_action') == 'invitaion' || $this->input->post('sending_action') == 'bulk_email') {
            $emails = $this->input->post('emails');
            $job_id = $this->input->post('job_id');
            $break_mails = explode(',', $emails);
            if ($this->input->post('email_subject') != '') {
                $email_subject = $this->input->post('email_subject');
            } else {
                $email_subject = 'Invitation';
            }
            if ($this->input->post('email_message') != '') {
                $email_message = $this->input->post('email_message');
            } else {
                $studio_admin_name = $this->session->userdata('firstname') . " " . $this->session->userdata('lastname');
                //getting job title
                //$getjob_title = $this->getRow('SELECT job_title FROM ' . TABLE_JOBS . ' WHERE job_id = "' . $job_id . '"');
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->get_jobs();
                $getjob_title = (array) $jobs_obj;

                $job_name = $getjob_title['job_title'];
                //getting email template from super admin
                //$get_message = $this->getRow("SELECT subject,template from " . TABLE_NOTIFICATION_TEMPLATE . " where notification_id='n13'");

                $notification_templates_obj = new Tyr_notification_templates_entity();
                $notification_templates_obj->notification_id = 'n13';
                $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                $subject_temp = $get_message['subject'];
                $message_temp = htmlspecialchars_decode($get_message['template']);
            }
            
            $i = 0;
            $err_emails = array();
            foreach ($break_mails as $email) {
                $users_obj = new Tyr_users_entity();
                $users_obj->email = $email;
                $users_obj->get_user_by_email();
                if($users_obj->user_id == 0){
                    if ($this->input->post('email_message') == '') {
                        $url = $this->getURL('register') . "/?" . md5('job_invitation') . "=1&" . md5('invite_email') . "=" . base64_encode($email) . "&" . md5('job_id') . "=" . base64_encode($job_id) . "&" . md5('studio_id') . "=" . base64_encode($this->session->userdata('user_id'));
                        $email_subject = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$url_invitereviewer]', '[$url_tyroe]'), array($job_name, $studio_admin_name, $url, LIVE_SITE_URL), $subject_temp);
                        $email_message = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$url_invitereviewer]', '[$url_tyroe]'), array($job_name, $studio_admin_name, $url, LIVE_SITE_URL), $message_temp);
                    }
                    $email_success = $this->email_sender($email, $email_subject, $email_message, '', '', '', '', '', 'Tyroe');
                }else{
                    $err_emails[$i] = $email;
                    $i++;
                }
            }
            
            $row = '';
            $error_message = false;
           
            if(isset($_POST['invite_type']) && $_POST['invite_type'] == 'job_invites'){
                foreach ($break_mails as $email) {
                    $email_present = false;
                    $users_obj = new Tyr_users_entity();
                    $users_obj->email = $email;
                    $users_obj->get_user_by_email();
                    
                    if($users_obj->user_id == 0){
                        $users_obj->status_sl = 0;
                        $users_obj->role_id = 4;
                        $users_obj->created_timestamp = strtotime("now");
                        $users_obj->modified_timestamp = strtotime("now");
                        $users_obj->save_user();
                    }else{
                        $email_present = true;
                    }
                    
                    if($users_obj->role_id != 2 && $users_obj->role_id != 1){
                        $profile_id = $users_obj->user_id;

                        $job_role_mapping = new Tyr_job_role_mapping_entity();
                        $job_role_mapping->job_id = $job_id;
                        $job_role_mapping->user_id = $profile_id;
                        $job_role_mapping->get_job_role_mapping();
                        if($job_role_mapping->id == 0){
                            $job_role_mapping->is_admin = 0;
                            $job_role_mapping->is_reviewer = 1;
                            $job_role_mapping->save_job_role_mapping();
                            $row.= '<tr class="table_rows job-team-tr" value="'.$profile_id.'">
                                    <td><p class="rt-name">Name not available</p>
                                    <span class="rt-title">no title</span></td>
                                    <td class="role-check-td" style="text-align: center" id="role_check_td_'.$job_id.'_'.$profile_id.'"><a class="reviewer_check_option" value="1"><i class="fa fa-check-circle"></i></a></td>
                                    <td class="role-check-td" style="text-align: center" id="role_check_td_'.$job_id.'_'.$profile_id.'"><a class="reviewer_check_option active" value="2"><i class="fa fa-check-circle"></i></a></td>
                                    <td style="text-align: right;padding-right: 13px"><a href="mailto:'.$email.'">'.$email.'</a></td>
                                    <td class="status_td">
                                        <a href="javascript:void(0)"><label class="label label-info rt-pending">Pending</label></a>
                                    </td>
                                    <td>
                                    <a href="javascript:void(0)" class="delet-job-reviewer" id="delet_job_reviewer_'.$job_id.'_'.$profile_id.'_0"><i class="fa fa-times"></i></a>
                                    </td>
                                </tr>';
                        }else{
                            $error_message = true;
                        }
                        
                   }else{
                       if(count($break_mails) == 1){
                           $error_message = true;
                       }else{
                           $error_message = true;
                       }
                   }
                }
            }
            
            if ($email_success) {
                echo json_encode(array('success' => true, 'msg' => $email_message,'row' => $row,'error_message' => $error_message,'err_emails' => $err_emails));
            } else {
                echo json_encode(array('success' => false,'row' => $row,'error_message' => $error_message,'err_emails' => $err_emails));
            }
        } else {
            $email_to = $this->input->post("email_to");
            $get_details = $this->getRow("SELECT email FROM " . TABLE_USERS . "
                      WHERE " . TABLE_USERS . ".user_id='" . $email_to . "' AND " . TABLE_USERS . ".status_sl='1'");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $email_to;
            $users_obj->status_sl = 1;
            $users_obj->get_user_by_email_and_status();
            $get_details = (array) $users_obj;

            $user_id = $this->session->userdata('user_id');
            $role_id = $this->session->userdata('role_id');
            $email_subject = $this->input->post("email_subject");
            $email_message = $this->input->post("email_message");
            //$email_to="shafqat@wiztech.pk";
            $to_email = $get_details['email'];
            $email_success = $this->email_sender($to_email, $email_subject, $email_message);

            $search_messages_obj = new Tyr_search_messages_entity();
            $search_messages_obj->studio_id = $user_id;
            $search_messages_obj->tyroe_id = $email_to;
            $search_messages_obj->role_id = $role_id;
            $search_messages_obj->email_message = $email_message;
            $search_messages_obj->email_subject = $email_subject;
            $search_messages_obj->email_status = $email_success;
            $search_messages_obj->status_sl = 1;
            $search_messages_obj->save_search_messages();

            if ($email_success) {
                echo json_encode(array('success' => true));
            } else {
                echo json_encode(array('success' => false));
            }
//            $data = array("studio_id" => $user_id,
//                "role_id" => $role_id,
//                "tyroe_id" => $email_to,
//                "email_subject" => $email_subject,
//                "email_message" => $email_message,
//                "email_status" => $email_success,
//                "created_timestamp" => strtotime("now"),
//                "status_sl" => "1",
//            );
//            $this->insert(TABLE_SEARCH_MESSAGES, $data);
        }
    }

    public function access_module() {
        $callfrom = $this->input->post("callfrom");
        if ($callfrom == "") {
            $url_params = $this->uri->uri_to_assoc('3');
            if (isset($url_params['callfrom'])) {
                $callfrom = $url_params['callfrom'];
            }
        }
        switch ($callfrom) {
            case 'send_email':
                $this->send_email();
                exit;
                break;
        }
    }
    
    public function get_skills(){
        $skill_obj = new Tyr_creative_skills_entity();
        $result = $skill_obj->get_creative_skills_for_profile($this->input->get('q'));
        echo json_encode(array('items' => $result,"incomplete_results" => false, "total_count" => count($result)));
    }

}
