<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_fb_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_publish_notificaion_entity' . EXT);
require_once(APPPATH . 'entities/tyr_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_entity' . EXT);
require_once(APPPATH . 'entities/tyr_deleted_account_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);



class Account extends Tyroe_Controller {

    public function Account() {
        parent::__construct();
        $this->data['page_title'] = 'Account';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ACCOUNT_SUB_MENU . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACCOUNT . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . ACCOUNT_SUB_MENU . ') ');
            //$this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
    }

    public function index() {
        $this->_add_css('account_details.css');
        $user_id = $this->session->userdata("user_id");
        #Profile Details
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_company_by_user_id($user_id);
        
        $new_pass = "";
        for ($i = 1; $i <= $get_user['password_len']; $i++) {
            $new_pass .= " ";
        }
        
        $city_obj = new Tyr_cities_entity();
        $city_obj->city_id = $get_user['city'] == '' ? 0 : $get_user['city'];
        $city_obj->get_cities();
                
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "password_len" => $new_pass,
            "profile_url" => $get_user['profile_url'],
            "role_id" => $get_user['role_id'],
            "company_name" => $get_user['company_name'],
            "company_job_title" => $get_user['job_title'],
            "city" => $city_obj->city
        );

        //correction required
        $fb_social_obj = new Tyr_fb_users_entity();
        $fb_social_obj->user_id = $user_id;
        $fb_social_obj->socialmedia_type = 'fb';
        $getfb_user_data = $fb_social_obj->get_social_details_by_user_and_type();
        
        $tw_social_obj = new Tyr_fb_users_entity();
        $tw_social_obj->user_id = $user_id;
        $tw_social_obj->socialmedia_type = 'tw';
        $gettw_user_data = $tw_social_obj->get_social_details_by_user_and_type();
        
        $gp_social_obj = new Tyr_fb_users_entity();
        $gp_social_obj->user_id = $user_id;
        $gp_social_obj->socialmedia_type = 'gp';
        $getgp_user_data = $gp_social_obj->get_social_details_by_user_and_type();
        
        $in_social_obj = new Tyr_fb_users_entity();
        $in_social_obj->user_id = $user_id;
        $in_social_obj->socialmedia_type = 'in';
        $getin_user_data = $in_social_obj->get_social_details_by_user_and_type();

        $this->data['get_user'] = $user_data;
        $this->data['social_fbdata'] = $getfb_user_data;
        $this->data['social_twdata'] = $gettw_user_data;
        $this->data['social_gpdata'] = $getgp_user_data;
        $this->data['social_indata'] = $getin_user_data;
        $this->template_arr = array('header', 'contents/acc_details', 'footer');
        $this->load_template();
    }

    public function schedule_notification() {
        $user_id = $this->session->userdata("user_id");

        $schedule_notification_obj = new Tyr_schedule_notification_entity();
        $schedule_notification_obj->user_id = $user_id;
        $this->data['selected_notification'] = $schedule_notification_obj->get_all_schedule_notification_by_user_id();

        $publish_notificaion_obj = new Tyr_publish_notificaion_entity();
        $publish_notificaion_obj->user_id = $user_id;
        $this->data['selected_systemcomms'] = $publish_notificaion_obj->get_selected_systemcomms_by_user_id();

        $options_notifications = new Tyr_options_entity();
        $options_notifications->option_type = 'notifications';
        $this->data['get_notifications'] = $options_notifications->get_all_options_by_option_type();

        $options_systemcomms = new Tyr_options_entity();
        $options_systemcomms->option_type = 'systemcomms';
        $this->data['get_systemcomms'] = $options_systemcomms->get_all_options_by_option_type();

        $this->data['pro_only'] = $this->pro_tyroe();
        $this->template_arr = array('header', 'contents/schedule_notification', 'footer');
        $this->load_template();
    }
    
    public function company_details() {
        $user_id = $this->session->userdata("user_id");
        $parent_id = $this->session->userdata("parent_id");

        if(intval($parent_id) == 0){
            $parent_id = intval($user_id);
        }
        
        $company_detail_studio_obj = new Tyr_company_detail_entity();
        $company_detail_studio_obj->studio_id = $parent_id;
        $company_detail_studio_obj->get_company_detail_by_studio();
        
        //var_dump($company_detail_studio_obj);
        
        $company_id = intval($company_detail_studio_obj->parent_id) == 0 ? $company_detail_studio_obj->company_id : $company_detail_studio_obj->parent_id;
        
        //var_dump($company_id);
        
        $company_detail_obj = new Tyr_company_detail_entity();
        $company_detail_obj->company_id = $company_id;
        $company_detail_obj->get_company_detail();
        $this->data['company_detail'] = (array)$company_detail_obj;
        
        if($company_detail_obj->updated_by > 0){
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $company_detail_obj->updated_by;
            $users_obj->get_user();
            
            $user_name = $users_obj->firstname.' '.$users_obj->lastname;
            $city_id = $users_obj->city;
            $city_obj = new Tyr_cities_entity();
            $city_obj->city_id = $city_id;
            $city_obj->get_cities(); 
            $city_name = $city_obj->city;
            
            $time = strtotime($company_detail_obj->updated_at);
            $user_time = date('l, j F Y g:ia',$time);
            
            $record_update_message = 'Last modified by '.$user_name.'-'.$city_name.', on '.$user_time;
            $this->data['record_update_message'] = $record_update_message;
        }
      
        $joins_obj  = new Tyr_joins_entity();
        $company_locations = $joins_obj->get_studio_locations_list($company_id);
        $this->data['company_locations'] = $company_locations;
        
        $industries_obj = new Tyr_industry_entity();
        $industries = $industries_obj->get_all_industries();
        $this->data['industries'] = $industries;

        
        if($this->session->flashdata('company_detail_saved') == 1){
            $this->data['company_detail_saved'] = true;
        }
            
        
        $this->_add_css('company_acc_details.css');

        $this->template_arr = array('header', 'contents/company_acc_details', 'footer');
        $this->load_template();
    }
    
    public function save_company_account_details(){
        
        $description = $this->input->post('description');
        $industry_id = $this->input->post('industry');
        $website = $this->input->post('website');
        
        $user_id = $this->session->userdata("user_id");
        $parent_id = $this->session->userdata("parent_id");
        
        if(intval($parent_id) == 0){
            $parent_id = intval($user_id);
        }
        
        $company_detail_studio_obj = new Tyr_company_detail_entity();
        $company_detail_studio_obj->studio_id = $parent_id;
        $company_detail_studio_obj->get_company_detail_by_studio();
        
        $company_id = $company_detail_studio_obj->company_id;
        if($company_detail_studio_obj->parent_id != 0)
            $company_id = $company_detail_studio_obj->parent_id;
        
        $company_detail_obj = new Tyr_company_detail_entity();
        $company_detail_obj->company_id = $company_id;
        $company_detail_obj->get_company_detail();
        $logo_image = '';
        if (!empty($_FILES)) {
            //$path = $_SERVER['DOCUMENT_ROOT'] . '/uploads/';
            $path = UPLOAD_PATH.'company_logo/';
            
            $file_temp = $_FILES['image']['tmp_name'];

            $file_ext = $this->get_extension($_FILES['image']['name']);
            $file_size = round($_FILES['image']['size'] / 1024, 2);
            
            $file_types = array('jpg','jpeg','gif','png','JPG','JPEG','GIF','PNG'); // File extensions
            $file_parts = pathinfo($_FILES['image']['name']);
            if (in_array($file_parts['extension'],$file_types)) {
                if ($file_size != 0) {

                    $file_name_temp = $company_detail_obj->company_id;
                    $file_name_normal = 'logo_'.$file_name_temp.$file_ext;
                    
                    $target_file = str_replace('//', '/', $path) . $file_name_normal;
                    $logo_image = $file_name_normal;
                    if(is_file($target_file)){
                        unlink($target_file);
                    }
                    
                    
                    if(move_uploaded_file($file_temp, $target_file) == false){
                        //echo json_encode(array('status' => 'upload ERROR'));
                        //return;
                    }else{
                        list($width,$height)=getimagesize($target_file);
                        $new_width = 400;
                        $new_height = 400;
                        $change_size = false;
                        if($width > $new_width){
                            $new_height = ($new_width * $height)/$width;
                            $change_size = true;
                        }else if($height > $new_height){
                            $new_width = ($width * $new_height)/$height;
                            $change_size = true;
                        }
                        if($change_size){
                            $this->resizeImage($target_file,$new_width,$new_height,$target_file);
                        }
                    }
                    
                }else{
                    //echo json_encode(array('status' => 'size ERROR'));
                }
            }else{
                //echo json_encode(array('status' => 'extension ERROR'));
            }
        }
        
        $company_detail_obj->company_description = $description;
        $company_detail_obj->industry_id = $industry_id;
        $company_detail_obj->website = $website;
        if($logo_image != ''){
            $company_detail_obj->logo_image = $logo_image;
        }
        $company_detail_obj->updated_by = $user_id;
        $company_detail_obj->update_company_all_details();
        $this->session->set_flashdata('company_detail_saved', '1');
        redirect(LIVE_SITE_URL.'account/company_details');
    }

    public function save_notification() {

        //$notification_process = $this->input->post('notification_process');
        $option_type = $this->input->post('option_type');
        $status_sl = $this->input->post('option_value');
        if ($status_sl == "OFF") {
            $status_sl = "0";
        } else {
            $status_sl = "1";
        }
        $user_id = $this->session->userdata("user_id");
        $notification_type = $this->input->post('notification_type');
        /* $return_notification = $this->getRow("SELECT count(*) cnt ,schedule_id  FROM " . TABLE_SCHEDULE_NOTIFICATION . " WHERE user_id = '" . $user_id . "'");

          if ($return_notification['cnt'] > 0) {
          $where = "schedule_id='" . $return_notification['schedule_id'] . "'";
          $del = $this->Delete(TABLE_SCHEDULE_NOTIFICATION, $where);
          } */
        /* $schedule_data = array("status_sl" => $status_sl,
          "user_id" => $user_id);
          //$return= $this->insert(TABLE_SCHEDULE_NOTIFICATION, $schedule_data);
          $where = "schedule_id='" . $option_type . "'"; */

        $schedule_notification_obj = new Tyr_schedule_notification_entity();
        $schedule_notification_obj->schedule_id = $option_type;
        $schedule_notification_obj->get_schedule_notification();

        $schedule_notification_obj->user_id = $user_id;
        $schedule_notification_obj->status_sl = $status_sl;
        $return = $schedule_notification_obj->update_schedule_notification();

        /* Set Session Values of TABLE_SCHEDULE_NOTIFICATION */

        $enotify_key = "enotify" . $schedule_notification_obj->option_id;
        $this->session->set_userdata($enotify_key, $schedule_notification_obj->status_sl);

        //$e_notify_updates = $this->getRow("SELECT option_id, status_sl FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE schedule_id = '".$option_type."'");
        //$enotify_key = "enotify".$e_notify_updates['option_id'];
        //$this->session->set_userdata($enotify_key,$e_notify_updates['status_sl']);
        /* Set Session Values of TABLE_SCHEDULE_NOTIFICATION */

        /* $publish_data = array("option_id" => $notification_type,
          "user_id" => $user_id);

          $return_publish = $this->getRow("SELECT count(*) cnt ,publish_id  FROM " . TABLE_PUBLISH_NOTIFICATION . " WHERE user_id = '" . $user_id . "'");

          if ($return_publish['cnt'] > 0) {
          $where = "publish_id='" . $return_publish['publish_id'] . "'";
          $del = $this->Delete(TABLE_PUBLISH_NOTIFICATION, $where);
          }

          $return = $this->insert(TABLE_PUBLISH_NOTIFICATION, $publish_data); */
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function subscription() {
        $user_id = $this->session->userdata("user_id");
        $post = $this->input->post("flag");
        if ($post == 'flag') {
            $subscription_obj = new Tyr_subscription_entity();
            $subscription_obj->tyroe_id = $user_id;
            $subscription_obj->subscription_type = $this->input->post("subsc_type");
            $subscription_obj->subscription_coupon = serialize($this->input->post("coupons"));
            $subscription_obj->subscription_use_coupon = $this->input->post("allow_use_copon");
            $subscription_obj->subscription_payment_type = $this->input->post("payment_type");
            $subscription_obj->subscription_card_type = $this->input->post("card_type");
            $subscription_obj->subscription_card_num = $this->input->post("credit_card_num");
            $subscription_obj->subscription_name = $this->input->post("name_card");
            $subscription_obj->subscription_expiry_month = $this->input->post("expiry_1");
            $subscription_obj->subscription_expiry_year = $this->input->post("expiry_2");
            $subscription_obj->subscription_ccv = $this->input->post("ccv");
            $subscription_obj->save_subscription();
            $this->template_arr = array('header_login', 'contents/subscription_success', 'footer');
            $this->load_template();
        } else {
            // need Correction with status in database

            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->user_id = $user_id;
            $coupons_allot_users_obj->coupon_status = 1;
            $get_current_coupons = $coupons_allot_users_obj->get_all_current_coupons_by_user_id_and_status();
            $this->data['current_coupons'] = $get_current_coupons;
            $this->template_arr = array('header', 'contents/subscription_form', 'footer');
            $this->load_template();
//            $get_current_coupons = $this->getAll("SELECT * FROM " . TABLE_COUPONS_ALLOT . " WHERE user_id = '{$user_id}' AND coupon_status = '1'");
//            $this->data['current_coupons'] = $get_current_coupons;
//            $this->template_arr = array('header', 'contents/subscription_form', 'footer');
//            $this->load_template();
        }
    }

    public function save_account_details() {

        $user_id = $this->session->userdata("user_id");
        $email = $this->input->post('email');
        $first_name = $this->input->post('first_name');
        $last_name = $this->input->post('last_name');
        $username = $this->input->post('username');
        $company_name = $this->input->post('company_name');
        $company_job_title = $this->input->post('company_job_title');

        if ($username == "tyroeJobs") {
            echo json_encode(array('false' => true, 'success_message' => "You can not set this ProfileURL"));
            exit();
        }

        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();

        if (($users_obj->email == $email) && ($users_obj->user_id == $user_id)) {

            $users_obj->firstname = $first_name;
            $users_obj->lastname = $last_name;
            $users_obj->profile_url = $username;
            $users_obj->modified_timestamp = time();
            $user_confirmpass = trim($this->input->post("cp"));
            #password edit
            if ($user_confirmpass != "") {
                $users_obj->password = $this->hashPass($user_confirmpass);
                $users_obj->password_len = strlen($user_confirmpass);
            }
            $users_obj->update_users();

            if ($this->session->userdata("role_id") == 3) {

                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->studio_id = $user_id;
                $company_exist = $company_detail_obj->get_company_count();

                if ($company_exist['cnt'] > 0) {
                    $company_detail_obj = new Tyr_company_detail_entity();
                    //$company_detail_obj->company_name = $company_name;
                    $company_detail_obj->company_job_title = $company_job_title;
                    $company_detail_obj->studio_id = $user_id;
                    $status = $company_detail_obj->update_company_details();
                    if ($status) {
                        $this->session->set_userdata('company_name', $company_name);
                    }
                } else {
                    $company_detail_obj = new Tyr_company_detail_entity();
                    $company_detail_obj->company_name = $company_name;
                    $company_detail_obj->company_job_title = $company_job_title;
                    $company_detail_obj->studio_id = $user_id;
                    $company_detail_obj->save_company_detail();
                }
            }
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->job_title = $company_job_title;
            $users_obj->update_users();
            
            $this->session->set_userdata('firstname', $first_name);
            $this->session->set_userdata('lastname', $last_name);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_PROFILE_UPDATED, 'type' => '1', 'firstname' => $first_name, 'lastname' => $last_name, 'user_name' => $this->session->userdata('user_name')));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_PROFILE_NOT_UPDATED, 'type' => '2'));
        }
        //Notifications
    }

#for tyroe Friend Invitation form to get coupon

    public function coupons() {
        $user_id = $this->session->userdata("user_id");
        $this->data['email'] = $this->session->userdata("user_email");
        $this->data['name'] = $this->session->userdata("firstname") . " " . $this->session->userdata("lastname");

        $coupons_obj = new Tyr_coupons_entity();
        $coupons_obj->coupon_type = 'g';
        $coupons_obj->get_coupons_by_coupons_type();

        $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
        $coupons_allot_users_obj->user_id = $user_id;
        $get_coupons = $coupons_allot_users_obj->get_current_coupon_by_user_id();
        if (count($get_coupons) > 0) {
            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->coupon_id = $coupons_obj->coupon_id;
            $coupons_allot_users_obj->user_id = $user_id;
            $coupons_allot_users_obj->coupon_label = $coupons_obj->coupon_label;
            $coupons_allot_users_obj->coupon_value = $coupons_obj->coupon_value;
            $coupons_allot_users_obj->coupon_type = $coupons_obj->coupon_type;
            $coupons_allot_users_obj->coupon_status = 0;
            $coupons_allot_users_obj->coupon_issued = strtotime("now");
            $coupons_allot_users_obj->save_coupons_allot_users();
        }

        $coupons_allot_users_obj_1 = new Tyr_coupons_allot_users_entity();
        $coupons_allot_users_obj_1->user_id = $user_id;
        $coupons = $coupons_allot_users_obj_1->get_all_current_coupons_by_user_id();

        $this->data['coupons'] = $coupons;
        $this->template_arr = array('header', 'contents/invite_friend_form', 'footer');
        $this->load_template();
    }

    public function invite_friend() {
        $user_id = $this->session->userdata("user_id");
        $coupons_obj = new Tyr_coupons_entity();
        $coupons_obj->coupon_type = 'r';
        $coupons_obj->tyroe_id = $user_id;
        $coupons_obj->get_coupons_by_coupons_type_and_tyroe();

        if ($coupons_obj->coupon_id == 0) {
            $coupons_obj = new Tyr_coupons_entity();
            $coupons_obj->coupon_type = 'r';
            $coupons_obj->tyroe_id = 0;
            $coupons_obj->get_coupons_by_coupons_type_and_tyroe_zero();
        }

        $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
        $coupons_allot_users_obj->coupon_id = $coupons_obj->coupon_id;
        $coupons_allot_users_obj->user_id = $user_id;
        $coupons_allot_users_obj->coupon_id = $coupons_obj->coupon_label;
        $coupons_allot_users_obj->coupon_id = $coupons_obj->coupon_value;
        $coupons_allot_users_obj->coupon_id = $coupons_obj->coupon_type;
        $coupons_allot_users_obj->status_sl = 0;
        $coupons_allot_users_obj->coupon_issued = strtotime("now");
        $coupons_allot_users_obj->save_coupons_allot_users();
        $get_id = $coupons_allot_users_obj->allot_id;

        $invite_friend_obj = new Tyr_invite_friend_entity();
        $invite_friend_obj->coupon_id = $get_id;
        $invite_friend_obj->user_id = $user_id;
        $invite_friend_obj->friend_name = $this->input->post("friend_name");
        $invite_friend_obj->invite_message = $this->input->post("invite_message");
        $invite_friend_obj->email = $this->input->post("friend_email");
        $invite_friend_obj->status_sl = 0;
        $invite_friend_obj->save_invite_friend();

        //$invite_friend_obj->get_invite_friend();
        $coupon_detail = (array) $invite_friend_obj;

        if ($invite_friend_obj->invite_id != 0) {
            $this->set_activity_stream($user_id, $invite_friend_obj->invite_id, '0', $this->session->userdata("role_id"), $coupon_detail, LABEL_INVITE_FRIEND_JOB_LOG);

//        if ($return) {
//            $this->set_activity_stream($user_id, $return, '0', $this->session->userdata("role_id"), $coupon_detail, LABEL_INVITE_FRIEND_JOB_LOG);
            # Send job Invitation Notification
            $notification_type = "email";
            if ($notification_type == "email") {
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $user_id;
                $users_obj->get_user();

                $to = $this->input->post("friend_email");
                $from = $users_obj->firstname . " " . $users_obj->lastname;
                $msg = $this->input->post("invite_message") . "<br>Join Tyroe.com by <a href='" . $this->getURL("register/invite_register/" . $user_id . "_" . $get_id . "_" . strtotime("now")) . "'>Click here. </a>
                                       <br>This message sent from '{$from}'.";
                $sendemail = $this->email_sender($to, LABEL_INVITATION_REQUEST, $msg);
                //$this->send_notification("n08",$to);
                if (!$sendemail) {
                    echo json_encode(array('success' => false, 'success_message' => MESSAGE_SEND_INVITATION_FAILED));
                } else {
                    echo json_encode(array('success' => true, 'success_message' => MESSAGE_SEND_INVITATION));
                }
            }
        }

        /* if ($return) {
          echo json_encode(array('success' => true, 'success_message' => MESSAGE_SEND_INVITATION));
          } else {
          echo json_encode(array('success' => false, 'success_message' => MESSAGE_SEND_INVITATION_FAILED));
          } */
    }

    #for admin

    public function getcoupons() {
        $this->template_arr = array('header', 'contents/coupon_form', 'footer');
        $this->load_template();
    }

    #for admin create new coupon

    public function save_coupon() {
        $invite_friend_obj = new Tyr_invite_friend_entity();
        $invite_friend_obj->user_id = $this->session->userdata("user_id");
        $invite_friend_obj->friend_name = $this->input->post("friend_name");
        $invite_friend_obj->invite_message = $this->input->post("invite_message");
        $invite_friend_obj->email = $this->input->post("friend_email");
        $invite_friend_obj->code = '';
        $invite_friend_obj->status_sl = 1;
        $invite_friend_obj->save_invite_friend();
        $return = true;
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_EDUCATION_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ADD_EDUCATION_SUCCESS));
        }
    }

    public function delete_account() {
        $user_id = $this->session->userdata("user_id");

        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();

        $users_obj->email = 'Remove';
        $users_obj->profile_url = 'Remove';
        $users_obj->username = 'Remove';
        $users_obj->status_sl = -1;
        $return = $users_obj->update_users();
 
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function delete_account_suggestion() {
        $user_id = $this->session->userdata("user_id");
        $suggestion = $this->input->post("sugg");

        $deleted_account_obj = new Tyr_deleted_account_entity();
        $deleted_account_obj->user_id = $user_id;
        $deleted_account_obj->suggestions = $suggestion;
        $deleted_account_obj->status_sl = 1;
        $deleted_account_obj->save_deleted_account();
        $return = true;

        if ($return) {
            $subject = "Account Delete";
            $message = "<u>Annonymous message:</u> <b>" . $suggestion . "</b>";
            $this->email_sender(EMAIL_FROM, $subject, $message);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function update_linked_status() {
        $linked_type = $this->input->post('linked_type');
        $user_id = $this->session->userdata("user_id");

        $fb_user_obj = new Tyr_fb_users_entity();
        $fb_user_obj->status_sl = 0;
        $fb_user_obj->access_date = date("Y-m-d H:i:s");
        $fb_user_obj->user_id = $user_id;
        $fb_user_obj->socialmedia_type = $linked_type;
        $fb_user_obj->update_link_status();
    }
    
    public function delete_studio_account() {
            $user_id = $this->session->userdata("user_id");
           
            $usersObj = new Tyr_users_entity();
            $usersObj->user_id = $user_id;
            $usersObj->get_user();
            
            if($usersObj->role_id == 3){
                $delete_id = $usersObj->parent_id;
                if($usersObj->parent_id == 0){
                    $delete_id = $user_id;
                }
                    
                
                /* Check Main Parent */
                $company_details_obj = new Tyr_company_detail_entity();
                $company_details_obj->studio_id = $delete_id;
                $company_details_obj->get_company_detail_by_studio();

                $joins_obj  = new Tyr_joins_entity();
                $query_studio = $joins_obj->get_studio_locations_list($company_details_obj->company_id);

                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $delete_id;
                $users_obj->get_user();
                $users_obj->email = 'Remove';
                $users_obj->profile_url = 'Remove';
                $users_obj->username = 'Remove';
                if($company_details_obj->parent_id != 0){
                    $users_obj->status_sl = -1;
                }

                if(count($query_studio) == 1){
                    $users_obj->status_sl = -1;
                }

                $result = $users_obj->update_users();

                /* Remove All Team Member*/
                $team_member_obj = new Tyr_users_entity();
                $team_member_obj->parent_id = $delete_id;
                $team_member = $team_member_obj->get_all_team_member_of_studio();
                if($team_member != ''){
                    
                foreach($team_member as $member_id){
                    $remove_team_obj = new Tyr_users_entity();
                    $remove_team_obj->user_id = $member_id;
                    $remove_team_obj->get_user();
                    $remove_team_obj->email = 'Remove';
                    $remove_team_obj->profile_url = 'Remove';
                    $remove_team_obj->username = 'Remove';
                    $remove_team_obj->status_sl = -1;
                    $remove_team_obj->update_users();
                    }
                }


                /* Add Jobs To Archive*/
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->user_id = $delete_id;
                $jobs_obj->archive_job_by_user();
                if($team_member != ''){
                    foreach($team_member as $member_id){
                        $jobs_obj = new Tyr_jobs_entity();
                        $jobs_obj->user_id = $member_id;
                        $jobs_obj->archive_job_by_user();
                    }
                }

                if ($result) {
                    echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
                } else {
                    echo json_encode(array('success' => false));
                }
                
            }else if($usersObj->role_id == 4 || $usersObj->role_id == 2){
                
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $user_id;
                $users_obj->get_user();

                $users_obj->email = 'Remove';
                $users_obj->profile_url = 'Remove';
                $users_obj->username = 'Remove';
                $users_obj->status_sl = -1;
                $return = $users_obj->update_users();

                if ($return) {
                    echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
                } else {
                    echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
                }
            }
            
        }

}
