<?php

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'libraries/stripe_lib/Stripe' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_payment_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_cards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_transaction_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_job_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_stripe_coupons_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);

class Payment extends Tyroe_Controller {

    public function __construct() {
        parent::__construct();
        $this->data['page_title'] = 'Payment';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_SEARCH . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_SEARCH . ') ');
        }
        Stripe::setApiKey(STRIPE_PRIVATE_KEY);
        Stripe::setApiVersion("2014-11-20");
    }

    public function index() {

        $parent_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $parent_id = $this->session->userdata("parent_id");
        }

        $this->_add_css('payment.css');

        $payment_transactions_obj = new Tyr_user_transaction_entity();
        $payment_transactions_obj->user_id = $parent_id;

        $all_transactions = $payment_transactions_obj->get_all_transactions();

        foreach ($all_transactions as $index => $transaction) {
            if ($transaction['package'] == 1) {
                $all_transactions[$index]['package_name'] = "Lite";
            } else {
                $all_transactions[$index]['package_name'] = "Awesome";
            }

            if ($transaction['type'] == 1) {
                $all_transactions[$index]['payment_for'] = "Talent Search";
            } else {

                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $transaction['job_id'];
                $jobs_obj->get_jobs();
                $job_title = $jobs_obj->job_title;

                if ($transaction['package'] == 2) {
                    $all_transactions[$index]['payment_for'] = "Job Opening - " . $job_title;
                } else {
                    $all_transactions[$index]['payment_for'] = "Job Opening - " . $job_title;
                }
            }
        }

        $this->data['transactions'] = $all_transactions;
        $this->template_arr = array('header', 'contents/payment/payment_history', 'footer');
        $this->load_template();
    }

    public function profile_access() {
        $amt = $this->input->get_post('amt');
        $tax = $this->input->get_post('tax');
        $discount = $this->input->get_post('discount');
        $discount_type = $this->input->get_post('discount_type');
        $coupon_id = $this->input->get_post('coupon_id');

        $coupon_obj = new Tyr_stripe_coupons_entity();
        $coupon_obj->coupon_id = $coupon_id;
        $coupon_obj->get_by_coupon_id();

        if ($coupon_obj->max_use > 0) {
            $coupon_obj->max_use = $coupon_obj->max_use - 1;
            $coupon_obj->update();
        }

        $discount_amt = 0;
        if (intval($discount_type) == 1) {
            $amt = floatval($amt) - floatval($discount);
            $discount_amt = intval($discount);
        } else if (intval($discount_type) == 2) {
            $discount_amt = ((floatval($amt) * floatval($discount)) / 100);
            $amt = floatval($amt) - ((floatval($amt) * floatval($discount)) / 100);
        }

        $tax_amt = 0;
        if (floatval($tax) != 0) {
            $tax_amt = ((floatval($amt) * floatval($tax)) / 100);
            $amt = floatval($amt) + ((floatval($amt) * floatval($tax)) / 100);
        }

        $total_amt = intval(floatval($amt) * 100);

        $cc = md5($this->input->get_post('number_c'));

        $user_obj = new Tyr_users_entity();
        $parent_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $parent_id = $this->session->userdata("parent_id");
        }
        $user_obj->user_id = $parent_id;
        $user_obj->get_user();
        if ($total_amt > 0) {
            $compnay_details_obj = new Tyr_company_detail_entity();
            $compnay_details_obj->studio_id = $parent_id;
            $compnay_details_obj->get_company_detail_by_studio();

            $city = '';
            $country = '';

            if (intval($compnay_details_obj->company_city) != 0) {
                $city_obj = new Tyr_cities_entity();
                $city_obj->city_id = intval($compnay_details_obj->company_city);
                $city_obj->get_cities();
                $city = $city_obj->city;
            }

            if (intval($compnay_details_obj->company_country) != 0) {
                $country_obj = new Tyr_countries_entity();
                $country_obj->country_id = intval($compnay_details_obj->company_country);
                $country_obj->get_countries();
                $country = $country_obj->country;
            }

            $description = $compnay_details_obj->company_name;
            if ($country != '') {
                $description .= ', ' . $country;
            }

            if ($city != '') {
                $description .= ', ' . $city;
            }

            $stripe_customer_obj = new Tyr_payment_entity();
            $stripe_customer_obj->user_id = $parent_id;
            $stripe_customer_obj->get_customer();


            if ($stripe_customer_obj->stripe_user_id == '') {
                $customer = Stripe_Customer::create(array(
                            "description" => $description,
                            "email" => $user_obj->email,
                            "metadata" => array(
                                "country" => $country,
                                "city" => $city,
                                "studio_id" => $parent_id
                            )
                                )
                );
                $stripe_customer_obj->stripe_user_id = $customer->id;
                $stripe_customer_obj->save_customer();
            }
            $stripe_customer_id = $stripe_customer_obj->stripe_user_id;

            $card_obj = new Tyr_user_cards_entity();
            $card_obj->user_id = $parent_id;
            $card_obj->card_no = $cc;
            $card_obj->get();

            if ($card_obj->card_id == '') {
                $cu = Stripe_Customer::retrieve($stripe_customer_id);
                $card_response = $cu->cards->create(array("card" => $this->input->get_post('stripeToken')));
                $card_obj->card_id = $card_response->id;
                $card_obj->save();
            }

            $card_id = $card_obj->card_id;
            try {
                $charge_reponse = $charge = Stripe_Charge::create(array(
                            "amount" => $total_amt, // amount in cents, again,
                            "currency" => "aud",
                            "customer" => $stripe_customer_id,
                            "card" => $card_id,
                            "description" => "Charge for Talent Search - Awesome Package",
                            "metadata" => array(
                                "address" => $this->input->get_post('address'),
                                "country" => $this->input->get_post('select_country'),
                                "city" => $this->input->get_post('a_city'),
                                "state" => $this->input->get_post('a_state'),
                                "zip" => $this->input->get_post('a_postcode'),
                                "job_id" => 0,
                                "package_type" => 2,
                                "payment_for" => 1,
                                "coupon_id" => $coupon_id,
                                "orig_amt" => $this->input->get_post('amt'),
                                "tax_amt" => strval(number_format($tax_amt, 2)),
                                "discount_amt" => strval(number_format($discount_amt, 2))
                            )
                                )
                );
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe
                var_dump($e);
            }
        }

        if ($charge_reponse->paid || intval($total_amt) == 0) {
            if (intval($total_amt) > 0) {
                $transaction_obj = new Tyr_user_transaction_entity();
                $transaction_obj->user_id = $parent_id;
                $transaction_obj->transaction_id = $charge_reponse->id;
                $transaction_obj->job_id = 0;
                $transaction_obj->type = 1;
                $transaction_obj->package = 2;
                $transaction_obj->amt = $total_amt / 100;
                $transaction_obj->coupon_id = $coupon_id;
                $transaction_obj->transaction_time = $charge_reponse->created;
                $transaction_obj->save_transaction();
            }

            $profile_access_obj = new Tyr_studio_profile_access_entity();
            $profile_access_obj->studio_id = $parent_id;
            $profile_access_obj->created_date = ($total_amt > 0) ? $charge_reponse->created : time();
            $profile_access_obj->expiry_date = time() + (30 * 24 * 60 * 60);
            $profile_access_obj->save_studio_profile_access();
            $this->session->set_flashdata('payment_success_message', 'Payment Successfull! Full access to Tyroe Profiles now available.');
            redirect(LIVE_SITE_URL . 'search');
        } else {
            $this->session->set_flashdata('payment_success_message', 'Your payment could not be process this time, please try after some time.');
            redirect(LIVE_SITE_URL . 'search');
        }
    }

    public function job_access() {

        $amt = $this->input->get_post('amt');
        $tax = $this->input->get_post('tax');
        $discount = $this->input->get_post('discount');
        $discount_type = $this->input->get_post('discount_type');
        $coupon_id = $this->input->get_post('coupon_id');
        $package_type = $this->input->get_post('package_type');
        $job_id = $this->input->get_post('job_id');

        $coupon_obj = new Tyr_stripe_coupons_entity();
        $coupon_obj->coupon_id = $coupon_id;
        $coupon_obj->get_by_coupon_id();

        if ($coupon_obj->max_use > 0) {
            $coupon_obj->max_use = $coupon_obj->max_use - 1;
            $coupon_obj->update();
        }

        $discount_amt = 0;
        if (intval($discount_type) == 1) {
            $amt = floatval($amt) - floatval($discount);
            $discount_amt = intval($discount);
        } else if (intval($discount_type) == 2) {
            $discount_amt = ((floatval($amt) * floatval($discount)) / 100);
            $amt = floatval($amt) - ((floatval($amt) * floatval($discount)) / 100);
        }

        $tax_amt = 0;
        if (floatval($tax) != 0) {
            $tax_amt = ((floatval($amt) * floatval($tax)) / 100);
            $amt = floatval($amt) + ((floatval($amt) * floatval($tax)) / 100);
        }

        $total_amt = intval(floatval($amt) * 100);


        $cc = md5($this->input->get_post('number_c'));

        $user_obj = new Tyr_users_entity();
        $parent_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $parent_id = $this->session->userdata("parent_id");
        }
        $user_obj->user_id = $parent_id;
        $user_obj->get_user();

        if ($total_amt > 0) {
            $compnay_details_obj = new Tyr_company_detail_entity();
            $compnay_details_obj->studio_id = $parent_id;
            $compnay_details_obj->get_company_detail_by_studio();

            $city = '';
            $country = '';

            if (intval($compnay_details_obj->company_city) != 0) {
                $city_obj = new Tyr_cities_entity();
                $city_obj->city_id = intval($compnay_details_obj->company_city);
                $city_obj->get_cities();
                $city = $city_obj->city;
            }

            if (intval($compnay_details_obj->company_country) != 0) {
                $country_obj = new Tyr_countries_entity();
                $country_obj->country_id = intval($compnay_details_obj->company_country);
                $country_obj->get_countries();
                $country = $country_obj->country;
            }

            $description = $compnay_details_obj->company_name;
            if ($country != '') {
                $description .= ', ' . $country;
            }

            if ($city != '') {
                $description .= ', ' . $city;
            }

            $stripe_customer_obj = new Tyr_payment_entity();
            $stripe_customer_obj->user_id = $parent_id;
            $stripe_customer_obj->get_customer();


            if ($stripe_customer_obj->stripe_user_id == '') {
                $customer = Stripe_Customer::create(array(
                            "description" => $description,
                            "email" => $user_obj->email,
                            "metadata" => array(
                                "country" => $country,
                                "city" => $city,
                                "studio_id" => $parent_id
                            )
                                )
                );
                $stripe_customer_obj->stripe_user_id = $customer->id;
                $stripe_customer_obj->save_customer();
            }
            $stripe_customer_id = $stripe_customer_obj->stripe_user_id;

            $card_obj = new Tyr_user_cards_entity();
            $card_obj->user_id = $parent_id;
            $card_obj->card_no = $cc;
            $card_obj->get();

            if ($card_obj->card_id == '') {
                $cu = Stripe_Customer::retrieve($stripe_customer_id);
                $card_response = $cu->cards->create(array("card" => $this->input->get_post('stripeToken')));
                $card_obj->card_id = $card_response->id;
                $card_obj->save();
            }

            $card_id = $card_obj->card_id;
            $charge_reponse = $charge = Stripe_Charge::create(array(
                        "amount" => $total_amt, // amount in cents, again,
                        "currency" => "aud",
                        "customer" => $stripe_customer_id,
                        "card" => $card_id,
                        "description" => "Charge for Job Opening " . ($package_type == 1 ? "- Lite Package" : "- Awesome Package"),
                        "metadata" => array(
                            "address" => $this->input->get_post('address'),
                            "country" => $this->input->get_post('select_country'),
                            "city" => $this->input->get_post('a_city'),
                            "state" => $this->input->get_post('a_state'),
                            "zip" => $this->input->get_post('a_postcode'),
                            "job_id" => $job_id,
                            "package_type" => $package_type,
                            "payment_for" => 2,
                            "coupon_id" => $coupon_id,
                            "orig_amt" => $this->input->get_post('amt'),
                            "tax_amt" => strval(number_format($tax_amt, 2)),
                            "discount_amt" => strval(number_format($discount_amt, 2))
                        )
                            )
            );
        }

        if ($charge_reponse->paid || intval($total_amt) == 0) {
            if ($total_amt > 0) {
                $transaction_obj = new Tyr_user_transaction_entity();
                $transaction_obj->user_id = $parent_id;
                $transaction_obj->transaction_id = $charge_reponse->id;
                $transaction_obj->job_id = $job_id;
                $transaction_obj->type = 2;
                $transaction_obj->package = $package_type;
                $transaction_obj->amt = $total_amt / 100;
                $transaction_obj->coupon_id = $coupon_id;
                $transaction_obj->transaction_time = $charge_reponse->created;
                $transaction_obj->save_transaction();
            }

            $job_access_obj = new Tyr_studio_job_access_entity();
            $job_access_obj->studio_id = $parent_id;
            $job_access_obj->created_date = ($total_amt > 0) ? $charge_reponse->created : time();
            $job_access_obj->expiry_date = time() + (30 * 24 * 60 * 60);
            $job_access_obj->save_studio_job_access();
            $payment_text = 'job';
            if ($package_type == 2) {
                $profile_access_obj = new Tyr_studio_profile_access_entity();
                $profile_access_obj->studio_id = $parent_id;
                $profile_access_obj->created_date = ($total_amt > 0) ? $charge_reponse->created : time();
                $profile_access_obj->expiry_date = time() + (30 * 24 * 60 * 60);
                $profile_access_obj->save_studio_profile_access();
                $payment_text = ' profiles and job ';
            }

            $jobs_obj = new tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->get_jobs();
            $jobs_obj->archived_status = 0;
            $jobs_obj->update_job_archived_status_by_id();

            $job_title = $this->cleanString($jobs_obj->job_title) . '-' . $this->generate_job_number($job_id);
            $this->session->set_flashdata('payment_success_message', 'Payment Successfull! Full access to' . $payment_text . 'now available.');
            $this->session->set_flashdata('job_payment_done', '1');
            redirect(LIVE_SITE_URL . 'openings/' . $job_title);
        } else {
            $this->session->set_flashdata('payment_success_message', 'Your payment could not be process this time, please try after some time.');
            redirect(LIVE_SITE_URL . 'openings');
        }
    }

    public function get_coupons_details() {
        $coupon_code = $this->input->get_post('coupon_code');
        try {
            $coupon_response = Stripe_Coupon::retrieve($coupon_code);

            $coupon_obj = new Tyr_stripe_coupons_entity();
            $coupon_obj->coupon_id = $coupon_response->id;
            $coupon_obj->coupon_created_at = $coupon_response->created;
            $coupon_obj->get();
            if ($coupon_obj->id == 0) {
                //save coupon
                $coupon_obj->delete();
                $coupon_obj->cash_discount = is_null($coupon_response->amount_off) ? 0 : $coupon_response->amount_off;
                $coupon_obj->per_discount = is_null($coupon_response->percent_off) ? 0 : $coupon_response->percent_off;
                $coupon_obj->expire_by = is_null($coupon_response->redeem_by) ? 0 : $coupon_response->redeem_by;
                $coupon_obj->max_use = is_null($coupon_response->max_redemptions) ? 0 : $coupon_response->max_redemptions;
                $coupon_obj->save();
            }
            $valid_coupon = true;
            $discount = $coupon_obj->cash_discount;
            $type = 1;
            $cur_time = time();
            if ($coupon_obj->expire_by != 0 && $cur_time > $coupon_obj->expire_by) {
                $valid_coupon = false;
            }

            if (!(is_null($coupon_response->max_redemptions)) && ($coupon_obj->max_use - 1) < 0) {
                $valid_coupon = false;
            }

            if ($coupon_obj->cash_discount == 0 && $coupon_obj->per_discount != 0) {
                $discount = $coupon_obj->per_discount;
                $type = 2;
            }

            if ($valid_coupon) {
                echo json_encode(array("status" => "SUCCESS", "data" => array('type' => $type, "discount" => $discount, 'coupon_id' => $coupon_obj->coupon_id)));
            } else {
                echo json_encode(array("status" => "ERROR", "data" => 'Invalid Coupon'));
            }
        } catch (Exception $e) {
            echo json_encode(array("status" => "ERROR", "data" => 'Invalid Coupon'));
        }
    }

    public function get_transaction_details() {
        $parent_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $parent_id = $this->session->userdata("parent_id");
        }
        $transaction_id = $this->input->get_post('transaction_id');

        $transaction_obj = new Tyr_user_transaction_entity();
        $transaction_obj->user_id = $parent_id;
        $transaction_obj->transaction_id = $transaction_id;
        $transaction_obj->get();

        if ($transaction_obj->id == 0) {
            echo json_encode(array('status' => 'ERROR', 'data' => "You are not authorized to view this transaction."));
        } else {

            $data = Stripe_Charge::retrieve($transaction_id);

            $country = '';
            if (intval($data->metadata->country) != 0) {
                $country_obj = new Tyr_countries_entity();
                $country_obj->country_id = intval($data->metadata->country);
                $country_obj->get_countries();
                $country = $country_obj->country;
            }

            $gst_applicable = false;
            if ($data->metadata->country == 2) {
                $gst_applicable = true;
            }

            $billing_address = $data->metadata->address . '<br/>' . $data->metadata->city . ", " . $data->metadata->state . '<br/>' . $country . '<br/>' . $data->metadata->zip;
//            $transaction_description = $data->description;
            $epoch = $data->created;
            $dt = new DateTime("@$epoch");
            $date = $dt->format('M d, Y');

            $this->get_company_name();
            $company_name = $this->data['company_name'];
            $coupon_id = $data->metadata->coupon_id;
            $type = $data->metadata->payment_for;
            $discount_amt = isset($data->metadata->discount_amt) ? floatval($data->metadata->discount_amt) : 0.00;
            $tax_amt = isset($data->metadata->tax_amt) ? floatval($data->metadata->tax_amt) : 0.00;
            $package = $data->metadata->package_type;
            $amt = floatval($data->amount) / 100;
            $package_name = '';

            if ($transaction_obj->package == 1) {
                $package_name = "Lite";
            } else {
                $package_name = "Awesome";
            }

            if ($transaction_obj->type == 1) {
                $transaction_description = "Talent Search - " . $package_name . ' Package';
            } else {
                $transaction_description = "Job Creation - " . $package_name . ' Package';
            }

            if ($coupon_id == '')
                $coupon_id = "No Coupon";

            $this->data = array(
                "transaction_id" => $transaction_id,
                "billing_address" => $billing_address,
                "transaction_description" => $transaction_description,
                "date" => $date,
                "company_name" => $company_name,
                "coupon_id" => $coupon_id,
                "type" => $type,
                "pkg" => $package_name,
                "amt" => $amt,
                "cc_last_4" => $data->card->last4,
                "card_type" => $data->card->brand,
                "card_exp" => $data->card->exp_month . '/' . $data->card->exp_year,
                "tax_amt" => $tax_amt,
                "discount_amt" => $discount_amt,
                "orig_amt" => $data->metadata->orig_amt
            );
            $this->template_arr = array('contents/invoice');
            $this->load_template();
        }
    }

}
