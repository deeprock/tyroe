<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);

class Subscription extends Tyroe_Controller
{
    public function Subscription()
    {

        parent::__construct();
        $this->data['page_title'] = 'Subscription';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_SUBSCRIPTION . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
        
//        $get_tyroes = $this->getAll("select user_id,username from " . TABLE_USERS . " where status_sl='1' AND role_id IN(2,5)");
//        $this->data['get_tyroes'] = $get_tyroes;
        $users_obj = new Tyr_users_entity();
        $role_in = implode(',',array(2,5));
        $get_tyroes = $users_obj->get_all_users_by_role_in($role_in);
        $this->data['get_tyroes'] = $get_tyroes;
    }

    public function index()
    {
        $search = $this->input->post('search');
        $firstname = $this->input->post('firstname');
        $lastname = $this->input->post('lastname');
        $where = "";
        if ($firstname && $lastname) {
            $where .= " AND (firstname  LIKE('%" . $search . "%') OR lastname  LIKE('%" . $search . "%') )";
        } else if ($firstname) {
            $where .= " AND firstname  LIKE('%" . $search . "%')";
        } else if ($lastname) {
            $where .= " AND lastname  LIKE('%" . $search . "%')";
        } else {

        }
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = "";
        }
        $pagintaion_limit = $this->pagination_limit();

//        $get_subscription = $this->getAll('SELECT subs.status_sl,subs.subscription_card_num,subs.subscription_card_type,subs.subscription_ccv,
//        subs.subscription_coupon,subs.subscription_expiry_month,subs.subscription_expiry_year,subs.subscription_id,subs.subscription_name,
//        subs.subscription_payment_type,subs.subscription_type,subs.subscription_use_coupon,users.username
//        FROM ' . TABLE_SUBSCRIPTION . ' subs INNER JOIN ' . TABLE_USERS . ' users ON subs.tyroe_id= users.user_id
//        WHERE 1=1 ' . $where . ' ' . $order_by . $pagintaion_limit['limit']);
        
        $joins_obj = new Tyr_joins_entity();
        $get_subscription = $joins_obj->get_all_subscription_paginated($where,$order_by,$pagintaion_limit);

        foreach ($get_subscription as $k => $subscription) {
            if ($subscription['subscription_payment_type'] == "credit_card") {
                $payment_type = "Credit Card";
            } else {
                $payment_type = "Paypal";
            }
            if ($subscription['subscription_type'] == "y") {
                $subscription_type = "Yearly";
            } else if ($subscription['subscription_type'] == "m") {
                $subscription_type = "Monthly";
            } else if ($subscription['subscription_type'] == "q") {
                $subscription_type = "Quarterly";
            }


            if ($subscription['subscription_use_coupon'] == "0") {
                $use_coupon = ASSETS_PATH . "img/yes_check.png";
            } else {
                $use_coupon = "";
            }

            if ($subscription['status_sl'] == "1") {
                $status = "Active";
            } else if ($subscription['status_sl'] == "0") {
                $status = "Suspended";
            } else if ($subscription['status_sl'] == "-1") {
                $status = "Cancelled";
            }

            $all_subscription[] = array(
                "status_sl" => $status,
                "subscription_card_num" => $subscription['subscription_card_num'],
                "subscription_card_type" => $subscription['subscription_card_type'],
                "subscription_ccv" => $subscription['subscription_ccv'],
                "subscription_coupon" => $subscription['subscription_coupon'],
                "subscription_expiry_month" => $subscription['subscription_expiry_month'],
                "subscription_expiry_year" => $subscription['subscription_expiry_year'],
                "subscription_id" => $subscription['subscription_id'],
                "subscription_name" => $subscription['subscription_name'],
                "payment_type" => $payment_type,
                "subscription_type" => $subscription_type,
                "use_coupon" => $use_coupon,
                "username" => $subscription['username'],
                "started_date" => date("d/m/Y"),
                "next_payment" => date("d/m/Y")
            );
        }

        $this->data['data'] = $all_subscription;
        
        //$total_rows = $this->getRow('SELECT COUNT(*) AS cnt FROM ' . TABLE_SUBSCRIPTION . ' subs INNER JOIN ' . TABLE_USERS . ' users ON subs.tyroe_id= users.user_id WHERE 1=1 ' . $where . ' ORDER BY subscription_id ASC');
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_subscription_id_count($where);
        
        $this->data['pagination'] = $this->pagination(array('url' => 'subscription/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('actions', 'header', 'contents/user/subscription_info', 'footer');
        $this->load_template();
    }

    public function getcoupons()
    {
        $this->data['page_title'] = 'Coupons';

        $bool = $this->input->post('bool');
        $id = $this->input->post('id');
        $label = $this->input->post('label');
        $title = $this->input->post('title');
        $status = $this->input->post('status');
        if ($title) {
            $where .= " AND (username  LIKE('%" . $title . "%'))";
        }
        if ($id) {
            $where .= " AND allot_id ='" . $id . "'";
        }
        if ($label) {
            $where .= " AND (coupon_label  LIKE('%" . $label . "%'))";
        }
        if((($status==1) || ($status==0)) && $status!=""){
            $where .= " AND coupon_status ='" . $status . "'";
        }
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY allot_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();

//        $get_coupons = $this->getAll('SELECT coup.*,IF(users.username!="",users.username,"") AS username
//            FROM ' . TABLE_COUPONS_ALLOT . ' coup
//            LEFT JOIN ' . TABLE_USERS . ' users ON users.user_id =coup.user_id
//            WHERE 1=1 AND coup.status_sl = "1" ' . $where . ' ' . $order_by . $pagintaion_limit['limit']);
        $joins_obj = new Tyr_joins_entity();
        $get_coupons = $joins_obj->get_all_studio_coupons_paginated($where,$order_by,$pagintaion_limit);

        foreach ($get_coupons as $k => $coupons) {

            if ($coupons['subscription_use_coupon'] == "0") {
                $use_coupon = ASSETS_PATH . "img/yes_check.png";
            } else {
                $use_coupon = "";
            }

            $all_coupons[] = array(
                "allot_id" => $coupons['allot_id'],
                "coupon_id" => $coupons['coupon_id'],
                "username" => $coupons['username'],
                "coupon_label" => $coupons['coupon_label'],
                "coupon_value" => $coupons['coupon_value'],
                "status_coupon" => $coupons['coupon_status'],
                "coupon_issued" => $coupons['coupon_issued'],
                "coupon_used" => $coupons['coupon_used'],
                "coupon_used_for" => $coupons['coupon_used_for']
            );
        }


        $this->data['data'] = $all_coupons;

//        $total_rows = $this->getRow('SELECT count(*) AS cnt
//                                    FROM ' . TABLE_COUPONS_ALLOT . ' coup
//                                    LEFT JOIN ' . TABLE_USERS . ' users ON users.user_id =coup.user_id
//                                    WHERE 1=1 AND coup.status_sl = 1 ' . $where);
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_for_cupons($where);

        $this->data['pagination'] = $this->pagination(array('url' => 'subscription/getcoupons', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));

        if ($bool) {
            $this->template_arr = array('contents/user/get_coupons_filter');
        } else {
            $this->template_arr = array('actions', 'header', 'contents/user/coupon_info', 'footer');
        }

        $this->load_template();
    }

    public function NewCoupon($data = '', $type = '')
    {

        if ($data != null && $data != "undefined" && $type == "Edit") {
            $this->data['flag'] = "edit";
            $allot_id = $data;
            
            //$get_coupon = $this->getRow("select * from " . TABLE_COUPONS_ALLOT . " where allot_id=" . $allot_id);
            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->allot_id = $allot_id;
            $coupons_allot_users_obj->get_coupons_allot_users();
            $get_coupon = array($coupons_allot_users_obj);
            
            $allot_user_id = $get_coupon['user_id'];
            
            
            //$get_username = $this->getRow("select username from " . TABLE_USERS . " where user_id=" . $allot_user_id);
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $allot_user_id;
            $users_obj->get_user();
            
            $this->data['selected_coupon'] = $get_coupon;
            $this->data['tyroe_name'] = $users_obj->username;
            $this->template_arr = array('contents/user/coupon_form');
        } else if ($type == "New") {
            $this->template_arr = array('contents/user/coupon_form');
        } else {
            echo "Please select Record ";
        }

        $this->load_template();
    }

    public function SaveCoupon()
    {
        $coupon_id = $this->input->post("edit_id");
        $allot_id = $this->input->post("allot_id");
        $tyroe_id = $this->input->post("tyroe_id");
        $coupon_label = $this->input->post("coupon_label");
        $coupon_value = $this->input->post("coupon_value");
        if(!empty($coupon_id)){$flag = 'edit';}

        if($flag == "edit"){
//            $edit_coupon_details = array(
//                "coupon_label" => $coupon_label,
//                "coupon_value" => $coupon_value
//            );
            //$this->update(TABLE_COUPONS_ALLOT, $edit_coupon_details,array("allot_id"=>$allot_id));
            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->allot_id = $allot_id;
            $coupons_allot_users_obj->get_coupons_allot_users();
            $coupons_allot_users_obj->coupon_label = $coupon_label;
            $coupons_allot_users_obj->coupon_value = $coupon_value;
            $coupons_allot_users_obj->update_coupons_allot_users();
            
            $return = $coupon_id;
        } else {
            
//            $coupon_detail = array(
//                "tyroe_id" => $tyroe_id,
//                "coupon_label" => $coupon_label,
//                "coupon_value" => $coupon_value,
//                "coupon_type" => "r",
//                "status_sl" => "1"
//            );
            //$coupon_id = $this->insert(TABLE_COUPONS, $coupon_detail);
            
            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->tyroe_id = $tyroe_id;
            $coupons_allot_users_obj->coupon_label = $coupon_label;
            $coupons_allot_users_obj->coupon_value = $coupon_value;
            $coupons_allot_users_obj->coupon_type = "r";
            $coupons_allot_users_obj->status_sl = 1;
            $coupons_allot_users_obj->save_coupons_allot_users();
            $coupon_id = $coupons_allot_users_obj->coupon_id;
            
            if($coupon_id > 0){
//                $alloted_coupon_detail = array(
//                    "coupon_label" => $coupon_label,
//                    "coupon_value" => $coupon_value,
//                    "coupon_type" => "r",
//                );
                if($tyroe_id == 0){
                    
                    //$all_tyroe = $this->getAll("SELECT user_id FROM ".TABLE_USERS." WHERE role_id = '2' || role_id = '5'");
                    $users_obj = new Tyr_users_entity();
                    $role_in = implode(',',array(2,5));
                    $all_tyroe = $users_obj->get_all_users_by_role_in($role_in);
                    
                    foreach($all_tyroe as $v_all){
//                        $alloted_coupon_detail['coupon_id'] = $coupon_id;
//                        $alloted_coupon_detail['user_id'] = $v_all['user_id'];
//                        $alloted_coupon_detail['coupon_status'] = 1;
//                        $alloted_coupon_detail['coupon_issued'] = strtotime(date("now"));
//                        $return = $this->insert(TABLE_COUPONS_ALLOT, $alloted_coupon_detail);
                        $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
                        $coupons_allot_users_obj->coupon_label = $coupon_label;
                        $coupons_allot_users_obj->coupon_value = $coupon_value;
                        $coupons_allot_users_obj->coupon_type = "r";
                        $coupons_allot_users_obj->coupon_id = $coupon_id;
                        $coupons_allot_users_obj->user_id = $v_all['user_id'];
                        $coupons_allot_users_obj->coupon_status = 1;
                        $coupons_allot_users_obj->coupon_issued = strtotime(date("now"));
                        $return = $coupons_allot_users_obj->save_coupons_allot_users();
                    }
                } else {
//                    $alloted_coupon_detail['coupon_id'] = $coupon_id;
//                    $alloted_coupon_detail['user_id'] = $tyroe_id;
//                    $alloted_coupon_detail['coupon_status'] = 1;
//                    $alloted_coupon_detail['coupon_issued'] = strtotime(date("now"));
//                    $return = $this->insert(TABLE_COUPONS_ALLOT, $alloted_coupon_detail);
                    $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
                    $coupons_allot_users_obj->coupon_label = $coupon_label;
                    $coupons_allot_users_obj->coupon_value = $coupon_value;
                    $coupons_allot_users_obj->coupon_type = "r";
                    $coupons_allot_users_obj->coupon_id = $coupon_id;
                    $coupons_allot_users_obj->user_id = $tyroe_id;
                    $coupons_allot_users_obj->coupon_status = 1;
                    $coupons_allot_users_obj->coupon_issued = strtotime(date("now"));
                    $return = $coupons_allot_users_obj->save_coupons_allot_users();
                }
            }
        }
        if ($return) {
            if($flag == "edit"){$msg = MESSAGE_UPDATE_SUCCESS;}else{$msg = MESSAGE_ADD_SUCCESS;}
            echo json_encode(array('success' => true, 'success_message' => $msg));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }

    }

    public function NewSubscription($data = '', $type = '')
    {

        if ($data != null && $data != "undefined" && $type == "Edit") {
            $this->data['flag'] = "edit";
            $subscription_id = $data;
            
            //$get_subscription = $this->getRow("select * from " . TABLE_SUBSCRIPTION . " where subscription_id=" . $subscription_id);
            $subscription_obj = new Tyr_subscription_entity();
            $subscription_obj->subscription_id = $subscription_id;
            $subscription_obj->get_subscription();
            $get_subscription = (array)$subscription_obj;
            $this->data['selected_subscription'] = $get_subscription;
            
            $this->data['selected_subscription'] = $get_subscription;
        }

        $content = "contents/user/subs_admin_form";
        $this->template_arr = array($content);
        $this->load_template();
    }

    public function SaveSubscription()
    {

//        $data_subscription = array(
//            "tyroe_id" => $this->input->post("tyroe_id"),
//            "subscription_type" => $this->input->post("subsc_type"),
//            "subscription_coupon" => serialize($this->input->post("coupons")),
//            "subscription_use_coupon" => $this->input->post("allow_use_copon"),
//            "subscription_payment_type" => $this->input->post("payment_type"),
//            "subscription_card_type" => $this->input->post("card_type"),
//            "subscription_card_num" => $this->input->post("credit_card_num"),
//            "subscription_name" => $this->input->post("name_card"),
//            "subscription_expiry_month" => $this->input->post("expiry_1"),
//            "subscription_expiry_year" => $this->input->post("expiry_2"),
//            "subscription_ccv" => $this->input->post("ccv")
//        );
        
        $subscription_obj = new Tyr_subscription_entity();
        $subscription_obj->tyroe_id = $this->input->post("tyroe_id");
        $subscription_obj->subscription_type = $this->input->post("subsc_type");
        $subscription_obj->subscription_coupon = $this->input->post("coupons");
        $subscription_obj->subscription_use_coupon = $this->input->post("allow_use_copon");
        $subscription_obj->subscription_payment_type = $this->input->post("payment_type");
        $subscription_obj->subscription_card_type = $this->input->post("card_type");
        $subscription_obj->subscription_card_num = $this->input->post("credit_card_num");
        $subscription_obj->subscription_name = $this->input->post("name_card");
        $subscription_obj->subscription_expiry_month = $this->input->post("expiry_1");
        $subscription_obj->subscription_expiry_year = $this->input->post("expiry_2");
        $subscription_obj->subscription_ccv = $this->input->post("ccv");

        $subs_id = $this->input->post("edit_id");
        if ($subs_id > 0) {
            //$where = "subscription_id ='" . $subs_id . "'";
            //$return = $this->update(TABLE_SUBSCRIPTION, $data_subscription, $where);
            $subscription_obj->subscription_id = $subs_id;
            $return = $subscription_obj->update_subscription();
            
        } else {
            //$return = $this->insert(TABLE_SUBSCRIPTION, $data_subscription);
            $return = $subscription_obj->save_subscription();
        }

        $this->template_arr = array('actions', 'header', 'contents/user/subscription_info', 'footer');
        $this->load_template();
        /* if ($return) {
             echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS));
         } else {
             echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
         }*/
    }

    public function DeleteCoupon($data = '')
    {
        $data_details = array("status_sl"=>-1);
        $where = array("allot_id"=>$data);
        if ($data != 'undefined') {
            //$return = $this->update(TABLE_COUPONS_ALLOT, $data_details, $where);
            
            $coupons_allot_users_obj = new Tyr_coupons_allot_users_entity();
            $coupons_allot_users_obj->allot_id = $data;
            $coupons_allot_users_obj->get_coupons_allot_users();
            $coupons_allot_users_obj->status_sl = -1;
            $return = $coupons_allot_users_obj->update_coupons_allot_users();
            
            if ($return) {
                echo "Delete Success";
            } else {
                echo MESSAGE_ERROR;
            }
        } else {
            echo "Please select coupon to delete";
        }
    }

    public function getpayment()
    {

        $this->template_arr = array('header', 'contents/user/payment_info', 'footer');
        $this->load_template();
    }
}
