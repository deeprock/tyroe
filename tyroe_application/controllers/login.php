<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_roles_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_system_variables_entity' . EXT);
require_once(APPPATH . 'entities/tyr_forgotpw_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);

class Login extends Tyroe_Controller
{
    public function Login()
    {
        parent::__construct('login');
    }

    public function index()
    {
        
        if($this->input->server('REQUEST_METHOD') == 'POST'){
            $user = addslashes($this->input->post("user_email"));
            $remember_me = addslashes($this->input->post("rem"));
            $password = $this->hashPass($this->input->post("pass"));
           
            $users_obj = new Tyr_users_entity();
            $users_obj->email = $user;
            $users_obj->password = $password;
            $users_obj->status_sl = 1;
            $users_obj->user_auth();
            $get_auth = (array)$users_obj;

            if ($users_obj->user_id > 0) {
                $role_id = $users_obj->role_id;
                $login_history_obj = new Tyr_login_history_entity();
                $login_history_obj->user_id = $users_obj->user_id;
                $login_history_obj->ip_address = $this->get_ip_address();
                $login_history_obj->user_agent = $_SERVER['HTTP_USER_AGENT'];
                $login_history_obj->save_login_history();

                $module_data = array();
                $module_data[] = -1;

                if ($role_id == ADMIN_ROLE) {
                    $modules_obj = new Tyr_modules_entity();
                    $modules = $modules_obj->get_all_modules();
                    foreach ($modules as $k => $v) {
                        $module_data[] = $modules[$k]['module_id'];
                    }
                } else {
    //                $modules_obj = new Tyr_joins_entity();
    //                $modules = $modules_obj->get_all_modules_by_user_id($users_obj->user_id);
                    if($role_id == 2){
                        $module_data = explode(',',TYROE_FREE_USER_MODULES);
                    }else if($role_id == 3){
                        $module_data = explode(',',STUDIO_USER_MODULES);
                    }else if($role_id == 4){
                        $module_data = explode(',',REVIEWER_USER_MODULES);
                    }
                }



                $job_activity_obj = new Tyr_job_activity_entity();
                $job_activity_obj->performer_id = $users_obj->user_id;
                $job_activity_obj->activity_type = 12;
                $job_activity_obj->user_seen = 0;
                $get_recommendation_notification = $job_activity_obj->get_job_activity_count_by_user_id();

                /*Message show once*/
                $latest_jobs = array('newjobcnt' => 0,'newjobids' => 0);
                $latest_shortlist = array('shorlistjobcount' => 0,'shortlistjobids' => 0);
                if(($get_auth['role_id'] == '2')){

                    $login_history_obj = new Tyr_login_history_entity();
                    $login_history_obj->user_id = $users_obj->user_id;
                    $login_history_obj->get_last_login_history_by_user_id();
                    $last_login_time = (array)$login_history_obj;

                    $jobs_obj = new Tyr_jobs_entity();
                    $new_jobs = $jobs_obj->get_all_new_jobs($last_login_time['created_timestamp']);

                    $latest_jobs['newjobcnt'] = count($new_jobs);
                    $latest_jobs['newjobids'] = implode(',',$new_jobs);

                    $jobs_detail_obj = new Tyr_job_detail_entity();
                    $jobs_detail_obj->tyroe_id = $users_obj->user_id;
                    $jobs_detail_obj->job_status_id = 3;
                    $jobs_detail_obj->status_sl = 1;
                    $latest_shortlist = $jobs_detail_obj->get_all_new_shortlist_jobs($last_login_time['created_timestamp']);

                    $latest_jobs['newjobcnt'] = 0;//if(empty($latest_jobs)){$latest_jobs['newjobcnt'] = 0;}
                    if(empty($latest_shortlist)){$latest_shortlist['shorlistjobcount'] = 0;}
                }
                /*Message show once*/
                $schedule_notification = new Tyr_schedule_notification_entity();
                $schedule_notification->user_id = $users_obj->user_id;
                $get_email_notifications = $schedule_notification->get_all_schedule_notification_by_user_id();

                $session_data = array(
                    'user_id' => $get_auth['user_id'],
                    'parent_id' => $get_auth['parent_id'],
                    'user_email' => $get_auth['email'],
                    'user_type' => $get_auth['role'],
                    'role_id' => $get_auth['role_id'],
                    'user_name' => $get_auth['username'],
                    'firstname' => $get_auth['firstname'],
                    'lastname' => $get_auth['lastname'],
                    'user_job_title' => $get_auth['job_title'],
                    'parent_id' => $get_auth['parent_id'],
                    'profile_url' => $get_auth['profile_url'],
                    'user_status' => $get_auth['status_sl'],
                    'module_id' => $module_data,
                    'recommendation_notification' => $get_recommendation_notification['cnt'],
                    'latest_jobs' => $latest_jobs['newjobcnt'],
                    'latest_jobs_ids' => $latest_jobs['newjobids'],
                    'latest_shortlist_jobs' => $latest_shortlist['shorlistjobcount'],
                    'latest_shortlist_jobs_ids' => $latest_shortlist['shortlistjobids'],
                    'logged_in' => TRUE,
                    'warm_welcome' => 1 //1=ON
                );

                if($get_auth['role_id'] == '3'){
                    $company_detail_obj = new Tyr_company_detail_entity();
                    $company_detail_obj->studio_id = $get_auth['user_id'];
                    $company_detail_obj->get_company_detail_by_studio();
                    $company_data = (array)$company_detail_obj;
                    $session_data['company_name'] = $company_data["company_name"];
                } 

                if($get_email_notifications != ''){
                    for($e_notify=0; $e_notify<count($get_email_notifications); $e_notify++){
                       //like this ----->  [enotify16] => 1
                       $session_data['enotify'.$get_email_notifications[$e_notify]['option_id']] = $get_email_notifications[$e_notify]['status_sl'];
                    }
                }
                $this->session->set_userdata($session_data);

                /*Theme Start*/
                $custome_theme = new Tyr_custome_theme_entity();
                $custome_theme->user_id = $get_auth['user_id'];
                $custome_theme->status_sl = 1;
                $custome_theme->get_custome_theme_by_user_id();

                if($custome_theme->theme_id > 0){
                    //Theme Section Details
                    $profile_theme_arr = array('text'=>'#323A45', 'background'=>'', 'icons'=>'#D0DBE2', 'icons_rollover'=>'#F97E76');
                    $featured_theme_arr = array('text'=>'#323A45', 'backgroundA'=>'#F97E76', 'backgroundB'=>'#E9F0F4');
                    $gallery_theme_arr = array('text'=>'#323A45', 'background'=>'#FFFFFF', 'overlay'=>'rgba(255, 255 ,255, 0.5)', 'button'=>'#F97E76');
                    $resume_theme_arr = array('text'=>'#323A45', 'background'=>'#E9F0F4');
                    $footer_theme_arr = array('icon'=>'#D0DBE2', 'icon_rollover'=>'#F97E76', 'backgroundA'=>'#323A45');

                    $custome_theme = new Tyr_custome_theme_entity();
                    $custome_theme->user_id = $users_obj->user_id;
                    $custome_theme->profile_theme = serialize($profile_theme_arr);
                    $custome_theme->featured_theme = serialize($featured_theme_arr);
                    $custome_theme->gallery_theme = serialize($gallery_theme_arr);
                    $custome_theme->resume_theme = serialize($resume_theme_arr);
                    $custome_theme->footer_theme_arr = serialize($footer_theme_arr);
                    $custome_theme->status_sl = 1;
                    $custome_theme->save_custome_theme();
                }

                $user_theme = new Tyr_custome_theme_entity();
                $user_theme->user_id = $users_obj->user_id;
                $user_theme->status_sl = 1;
                $user_theme->get_custome_theme_by_user_id();

                $profile_theme = unserialize(stripslashes($user_theme->profile_theme));
                $featured_theme = unserialize(stripslashes($user_theme->featured_theme));
                $gallery_theme = unserialize(stripslashes($user_theme->gallery_theme));
                $resume_theme = unserialize(stripslashes($user_theme->resume_theme));
                $footer_theme = unserialize(stripslashes($user_theme->footer_theme));

                $this->session->set_userdata('profile_theme', $profile_theme);
                $this->session->set_userdata('featured_theme', $featured_theme);
                $this->session->set_userdata('gallery_theme', $gallery_theme);
                $this->session->set_userdata('resume_theme', $resume_theme);
                $this->session->set_userdata('footer_theme', $footer_theme);
                /*Theme End*/

                if ($remember_me == 1) {
                    $year = time() + 31536000;
                    setcookie('remember_email', $get_auth['email'], $year);
                    setcookie('remember_key', $this->input->post("pass"), $year);
                    setcookie('remember_chk', 1, $year);
                } else {
                    setcookie('remember_email');
                    setcookie('remember_key');
                    setcookie('remember_chk', 0);
                }

                //System Variables
                $system_variables_obj = new Tyr_system_variables_entity();
                $system_variables = $system_variables_obj->get_all_system_variables();

                $system_variables_data = array();
                foreach ($system_variables as $sv_k => $sv_v) {
                    $system_variables_data[$sv_v['variable_name']] = $sv_v['variable_value'];
                }
                $this->session->set_userdata($system_variables_data);

                if($this->session->userdata('redirect_to_job_overview') == 'yes' ){
                    redirect(LIVE_SITE_URL.'openings');
                }else if($this->session->userdata('redirect_to_job_opening') == 'yes'){
                    redirect(LIVE_SITE_URL.'openings');
                }else if($this->session->userdata('role_id') == 3 || $this->session->userdata('role_id') == 4 || $this->session->userdata('role_id') == 1){ 
                    redirect(LIVE_SITE_URL.'home');
                }else{
                    redirect(LIVE_SITE_URL.'home');
                }

                //echo json_encode(array('success' => true, 'role' => $session_data['role_id']));
            } else {
                //echo json_encode(array('success' => false));
                $this->data['page_title'] = 'Login';
                $this->data['error_already_invite'] = "Invalid Email/Password";
                $this->template_arr = array('header_login', 'contents/login', 'footer');
                $this->load_template();
            }
            
        }else{
            $this->data['page_title'] = 'Login';
            $this->data['error_already_invite'] = $this->session->userdata('alreadyInvite');
            $this->template_arr = array('header_login', 'contents/login', 'footer');
            $this->load_template();
            $this->session->set_userdata('alreadyInvite','');
        }
        
    }

    public function login_auth($email = "", $pass = "")
    {
        //error_reporting(-1);
//        $user = '';
//        $password = '';
//        if($email == '' && $pass == ''){
//            $user = addslashes($this->input->post("user_email"));
//            $remember_me = addslashes($this->input->post("rem"));
//            $password = $this->hashPass($this->input->post("pass"));
//        }else{
//            if($this->session->userdata('reg_user_email') == '' || $this->session->userdata('reg_user_password') == ''){
//                redirect(LIVE_SITE_URL);
//            }
//                
//            $user = $this->session->userdata('reg_user_email');
//            $remember_me = 0;
//            $password = $this->hashPass($this->session->userdata('reg_user_password'));
//        }
        
        if($this->session->userdata('reg_user_email') == '' || $this->session->userdata('reg_user_password') == ''){
                redirect(LIVE_SITE_URL);
        }
        
        $users_obj = new Tyr_users_entity();
        $users_obj->email = addslashes($this->session->userdata('reg_user_email'));
        $users_obj->password = $this->hashPass($this->session->userdata('reg_user_password'));
        $users_obj->status_sl = 1;
        $users_obj->user_auth();
        $get_auth = (array)$users_obj;
        
        $this->session->set_userdata('reg_user_email', '');
        $this->session->set_userdata('reg_user_password', '');
        
        if ($users_obj->user_id > 0) {
            $role_id = $users_obj->role_id;
            $login_history_obj = new Tyr_login_history_entity();
            $login_history_obj->user_id = $users_obj->user_id;
            $login_history_obj->ip_address = $this->get_ip_address();
            $login_history_obj->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $login_history_obj->save_login_history();
            
            $module_data = array();
            $module_data[] = -1;
            
            if ($role_id == ADMIN_ROLE) {
                $modules_obj = new Tyr_modules_entity();
                $modules = $modules_obj->get_all_modules();
                foreach ($modules as $k => $v) {
                    $module_data[] = $modules[$k]['module_id'];
                }
            } else {
//                $modules_obj = new Tyr_joins_entity();
//                $modules = $modules_obj->get_all_modules_by_user_id($users_obj->user_id);
                if($role_id == 2){
                    $module_data = explode(',',TYROE_FREE_USER_MODULES);
                }else if($role_id == 3){
                    $module_data = explode(',',STUDIO_USER_MODULES);
                }else if($role_id == 4){
                    $module_data = explode(',',REVIEWER_USER_MODULES);
                }
            }

           
            
            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $users_obj->user_id;
            $job_activity_obj->activity_type = 12;
            $job_activity_obj->user_seen = 0;
            $get_recommendation_notification = $job_activity_obj->get_job_activity_count_by_user_id();
 
            /*Message show once*/
            $latest_jobs = array('newjobcnt' => 0,'newjobids' => 0);
            $latest_shortlist = array('shorlistjobcount' => 0,'shortlistjobids' => 0);
            if(($get_auth['role_id'] == '2')){
                
                $login_history_obj = new Tyr_login_history_entity();
                $login_history_obj->user_id = $users_obj->user_id;
                $login_history_obj->get_last_login_history_by_user_id();
                $last_login_time = (array)$login_history_obj;
                
                $jobs_obj = new Tyr_jobs_entity();
                $new_jobs = $jobs_obj->get_all_new_jobs($last_login_time['created_timestamp']);
                
                $latest_jobs['newjobcnt'] = count($new_jobs);
                $latest_jobs['newjobids'] = implode(',',$new_jobs);
                
                $jobs_detail_obj = new Tyr_job_detail_entity();
                $jobs_detail_obj->tyroe_id = $users_obj->user_id;
                $jobs_detail_obj->job_status_id = 3;
                $jobs_detail_obj->status_sl = 1;
                $latest_shortlist = $jobs_detail_obj->get_all_new_shortlist_jobs($last_login_time['created_timestamp']);
                
                $latest_jobs['newjobcnt'] = 0;//if(empty($latest_jobs)){$latest_jobs['newjobcnt'] = 0;}
                if(empty($latest_shortlist)){$latest_shortlist['shorlistjobcount'] = 0;}
            }
            /*Message show once*/
            $schedule_notification = new Tyr_schedule_notification_entity();
            $schedule_notification->user_id = $users_obj->user_id;
            $get_email_notifications = $schedule_notification->get_all_schedule_notification_by_user_id();
          
            $session_data = array(
                'user_id' => $get_auth['user_id'],
                'parent_id' => $get_auth['parent_id'],
                'user_email' => $get_auth['email'],
                'user_type' => $get_auth['role'],
                'role_id' => $get_auth['role_id'],
                'user_name' => $get_auth['username'],
                'firstname' => $get_auth['firstname'],
                'lastname' => $get_auth['lastname'],
                'user_job_title' => $get_auth['job_title'],
                'parent_id' => $get_auth['parent_id'],
                'profile_url' => $get_auth['profile_url'],
                'user_status' => $get_auth['status_sl'],
                'module_id' => $module_data,
                'recommendation_notification' => $get_recommendation_notification['cnt'],
                'latest_jobs' => $latest_jobs['newjobcnt'],
                'latest_jobs_ids' => $latest_jobs['newjobids'],
                'latest_shortlist_jobs' => $latest_shortlist['shorlistjobcount'],
                'latest_shortlist_jobs_ids' => $latest_shortlist['shortlistjobids'],
                'logged_in' => TRUE,
                'warm_welcome' => 1 //1=ON
            );
            
            if($get_auth['role_id'] == '3'){
                $company_detail_obj = new Tyr_company_detail_entity();
                $company_detail_obj->studio_id = $get_auth['user_id'];
                $company_detail_obj->get_company_detail_by_studio();
                $company_data = (array)$company_detail_obj;
                $session_data['company_name'] = $company_data["company_name"];
            } 
            
            if($get_email_notifications != ''){
                for($e_notify=0; $e_notify<count($get_email_notifications); $e_notify++){
                   //like this ----->  [enotify16] => 1
                   $session_data['enotify'.$get_email_notifications[$e_notify]['option_id']] = $get_email_notifications[$e_notify]['status_sl'];
                }
            }
            $this->session->set_userdata($session_data);

            /*Theme Start*/
            $custome_theme = new Tyr_custome_theme_entity();
            $custome_theme->user_id = $get_auth['user_id'];
            $custome_theme->status_sl = 1;
            $custome_theme->get_custome_theme_by_user_id();
           
            if($custome_theme->theme_id > 0){
                //Theme Section Details
                $profile_theme_arr = array('text'=>'#323A45', 'background'=>'', 'icons'=>'#D0DBE2', 'icons_rollover'=>'#F97E76');
                $featured_theme_arr = array('text'=>'#323A45', 'backgroundA'=>'#F97E76', 'backgroundB'=>'#E9F0F4');
                $gallery_theme_arr = array('text'=>'#323A45', 'background'=>'#FFFFFF', 'overlay'=>'rgba(255, 255 ,255, 0.5)', 'button'=>'#F97E76');
                $resume_theme_arr = array('text'=>'#323A45', 'background'=>'#E9F0F4');
                $footer_theme_arr = array('icon'=>'#D0DBE2', 'icon_rollover'=>'#F97E76', 'backgroundA'=>'#323A45');
                
                $custome_theme = new Tyr_custome_theme_entity();
                $custome_theme->user_id = $users_obj->user_id;
                $custome_theme->profile_theme = serialize($profile_theme_arr);
                $custome_theme->featured_theme = serialize($featured_theme_arr);
                $custome_theme->gallery_theme = serialize($gallery_theme_arr);
                $custome_theme->resume_theme = serialize($resume_theme_arr);
                $custome_theme->footer_theme_arr = serialize($footer_theme_arr);
                $custome_theme->status_sl = 1;
                $custome_theme->save_custome_theme();
            }
            
            $user_theme = new Tyr_custome_theme_entity();
            $user_theme->user_id = $users_obj->user_id;
            $user_theme->status_sl = 1;
            $user_theme->get_custome_theme_by_user_id();
            
            $profile_theme = unserialize(stripslashes($user_theme->profile_theme));
            $featured_theme = unserialize(stripslashes($user_theme->featured_theme));
            $gallery_theme = unserialize(stripslashes($user_theme->gallery_theme));
            $resume_theme = unserialize(stripslashes($user_theme->resume_theme));
            $footer_theme = unserialize(stripslashes($user_theme->footer_theme));

            $this->session->set_userdata('profile_theme', $profile_theme);
            $this->session->set_userdata('featured_theme', $featured_theme);
            $this->session->set_userdata('gallery_theme', $gallery_theme);
            $this->session->set_userdata('resume_theme', $resume_theme);
            $this->session->set_userdata('footer_theme', $footer_theme);
            /*Theme End*/

            if ($remember_me == 1) {
                $year = time() + 31536000;
                setcookie('remember_email', $get_auth['email'], $year);
                setcookie('remember_key', $this->input->post("pass"), $year);
                setcookie('remember_chk', 1, $year);
            } else {
                setcookie('remember_email');
                setcookie('remember_key');
                setcookie('remember_chk', 0);
            }

            //System Variables
            $system_variables_obj = new Tyr_system_variables_entity();
            $system_variables = $system_variables_obj->get_all_system_variables();
            
            $system_variables_data = array();
            foreach ($system_variables as $sv_k => $sv_v) {
                $system_variables_data[$sv_v['variable_name']] = $sv_v['variable_value'];
            }
            $this->session->set_userdata($system_variables_data);
            
            if($this->session->userdata('redirect_to_job_overview') == 'yes' ){
                redirect(LIVE_SITE_URL.'openings');
            }else if($this->session->userdata('redirect_to_job_opening') == 'yes'){
                redirect(LIVE_SITE_URL.'openings');
            }else if($this->session->userdata('role_id') == 3 || $this->session->userdata('role_id') == 4){ 
                redirect(LIVE_SITE_URL.'home');
            }else{
                redirect(LIVE_SITE_URL.'profile');
            }
            
            //echo json_encode(array('success' => true, 'role' => $session_data['role_id']));
        } else {
            //echo json_encode(array('success' => false));
            $this->data['page_title'] = 'Login';
            $this->data['error_already_invite'] = "Invalid Email/Password";
            $this->template_arr = array('header_login', 'contents/login', 'footer');
            $this->load_template();
        }
    }

    public function PasswordForgot()
    {
        $this->data['page_title'] = 'Forgot Password';
        $this->template_arr = array('header_login', 'contents/PasswordForgot', 'footer');
        $this->load_template();
    }

    public function PasswordRequest()
    {
        $user_email = addslashes($this->input->post("user_email"));
        $users_obj = new Tyr_users_entity();
        $users_obj->email = $user_email;
        $users_obj->get_user_by_email();
        
        if ($users_obj->user_id > 0) {
            $md_user_id = md5($users_obj->user_id);
            $this->email_sender($users_obj->email, 'Forgot Password Request', "<a href='" . $this->getURL("login/ChangePassword/" . $md_user_id) . "'>Click Here </a>to change your password ");
            $forgot_password_obj = new Tyr_forgotpw_entity();
            $forgot_password_obj->user_id = $users_obj->user_id;
            $forgot_password_obj->email = $md_user_id;
            $forgot_password_obj->expiry_time = strtotime("+1 day");
            $forgot_password_obj->status_pw = 1;
            $forgot_password_obj->save_forgotpw();

            echo json_encode(array('success' => true, 'success_message' => LABEL_FORGOT_EMAIL_REQUEST_MESSAGE));
        } else {
            echo json_encode(array('success' => false, 'success_message_error' => LABEL_FORGOT_EMAIL_NOT_FOUND));
        }
    }

    public function ChangePassword($email)
    {
        $forgot_password_obj = new Tyr_forgotpw_entity();
        $forgot_password_obj->email = $email;
        $forgot_password_obj->status_pw = 1;
        $forgot_password_obj->get_forgot_password_user();
        $get_email = (array)$forgot_password_obj;
        if ($forgot_password_obj->user_id > 0) {
            $user_id = $get_email['user_id'];
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $this->data['email'] = (array)$users_obj;
            $this->data['page_title'] = 'Change Password';
            $this->template_arr = array('header_login', 'contents/change_password', 'footer');
            $this->load_template();
        } else {
            header('location:' . $this->getURL("login/PasswordForgot"));
        }
    }

    public function PasswordChanged()
    {
        $user_id = $this->input->post("user_id");
        
        $password = $this->hashPass($this->input->post("pass"));
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->password = $password;
        $users_obj->password_len = strlen($this->input->post("pass"));
        $change_password = $users_obj->update_users();
        if ($change_password) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_PASSWORD_CHANGED));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_PASSWORD_NOT_CHANGED));
        }


    }

    public function check_login()
    {
        if ($this->session->userdata('logged_in')) {
            echo true;
        } else {
            echo false;
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */