<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'entities/tyr_fb_users_entity' . EXT);
class Fbclient extends Tyroe_Controller
{
    public function Fbclient()
    {
        parent::__construct();
        $this->load->library('Facebook');
    }
    public function index()
    {

        $facebook = new Facebook(array(
            'appId' => '780597198622367',
            'secret' => '5434a9f10d1bcac3ce009f042926d726',
            'cookie' => true
        ));

        //Get the FB UID of the currently logged in user
        $user = $facebook->getUser();

        //if the user has already allowed the application, you'll be able to get his/her FB UID

        if($user) {
            //start the session if needed

            $user_id = $this->session->userdata("user_id");
            if( session_id() ) {

            } else {
                session_start();
            }

            //do stuff when already logged in

            //get the user's access token
            $access_token =  $facebook->getAccessToken();

            //check permissions list
            $permissions_list = $facebook->api(
                '/me/permissions',
                'GET',
                array(
                    'access_token' => $access_token
                )
            );

            //check if the permissions we need have been allowed by the user
            //if not then redirect them again to facebook's permissions page
            $permissions_needed = array('publish_stream', 'read_stream', 'publish_actions', 'manage_pages');
            foreach($permissions_needed as $perm) {
                if( !isset($permissions_list['data'][0][$perm]) || $permissions_list['data'][0][$perm] != 1 ) {
                    $login_url_params = array(
                        'scope' => 'publish_stream,read_stream,publish_actions,manage_pages',
                        'fbconnect' =>  1,
                        'display'   =>  "page",
                        'next' => "http://dev.tyroe.com/home"
                    );
                    $login_url = $facebook->getLoginUrl($login_url_params);
                    header("Location: {$login_url}");
                    exit();
                }
            }

            //if the user has allowed all the permissions we need,
            //get the information about the pages that he or she managers
            $accounts = $facebook->api(
                '/me/accounts',
                'GET',
                array(
                    'access_token' => $access_token
                )
            );

            //save the information inside the session
            $_SESSION['access_token'] = $access_token;
            $_SESSION['accounts'] = $accounts['data'];
            //save the first page as the default active page
            $_SESSION['active'] = $accounts['data'][0];

            //$this->email_sender('faisal@wiztech.pk', 'URGENT', serialize($accounts['data']."-".$user));

            //$qry = "select * from tyr_fb_users where user_id='".$user."'";


            //$result = $this->getRow("select * from tyr_fb_users where socialmedia_id='".$user."' & user_id = ".$user_id." ");
            
            
            $social_media_obj = new Tyr_fb_users_entity();
            $social_media_obj->user_id = $user_id;
            $social_media_obj->socialmedia_type = 'fb';
            $social_media_obj->get_user_social_media_details();
            
            //$result = $this->getRow("select * from tyr_fb_users where socialmedia_type = 'fb' AND user_id = '".$user_id."'");

             $user_ip = $this->input->ip_address();

            if($social_media_obj->id == 0){
                //$insert = "insert into tyr_fb_users(socialmedia_id,user_data,user_id,access_token,ip,access_date,status_sl) values('".$user."','".serialize($accounts['data'])."','".$user_id ."','".$_SESSION['access_token']."','".$_SERVER['REMOTE_ADDR']."',NOW(),'fb','1')";
		$social_media_obj = new Tyr_fb_users_entity();
                $social_media_obj->user_id = $user_id;
                $social_media_obj->socialmedia_type = 'fb';
                $social_media_obj->socialmedia_id = $user;
                $social_media_obj->user_data = serialize($accounts["data"]);
                $social_media_obj->access_token = $_SESSION['access_token'];
                $social_media_obj->ip = $user_ip;
                $social_media_obj->status_sl = 1;
                $social_media_obj->access_date = date("Y-m-d H:i:s");
                $social_media_obj->save_fb_users();


	//This code for post on User Wall/Timeline
	/*$post = $facebook->api("/me/feed","POST",array(

            'message'       =>  "Hurray! This works :)",
            'name'          =>  "This is my title",
            'caption'       =>  "My Caption",
            'description'   =>  "Some Description...",
            'link'          =>  "http://stackoverflow.com",
            'picture'       =>  "http://i.imgur.com/VUBz8.png"
        ));*/


            } else {
		  //check status 
                if($social_media_obj->status_sl == 1){
			$update_status_sl = 0;
		}else{
			$update_status_sl = 1;
                }
                
                $social_media_obj->socialmedia_id = $user;
                $social_media_obj->access_token = $_SESSION["access_token"];
                $social_media_obj->ip = $user_ip;
                $social_media_obj->user_data = serialize($accounts["data"]);
                $social_media_obj->status_sl = $update_status_sl;
                $social_media_obj->access_date = date("Y-m-d H:i:s");
                $social_media_obj->user_id = $user_id;
                $social_media_obj->socialmedia_type = 'fb';
                $social_media_obj->update_user_social_media_details();
                

                /*$update = "



                    UPDATE ".TABLE_FB_USERS." SET
                    status_sl = '$update_status_sl' ,
                    socialmedia_id = ".$user." ,
                    user_data = '".serialize($accounts["data"])."' ,
                    access_token = ".$_SESSION["access_token"]." ,
                    socialmedia_id = ".$_SERVER["REMOTE_ADDR"]." ,

                    access_date = NOW()

                    WHERE user_id = ".$user_id." AND  socialmedia_type="fb"

                ";
                $update; */

                //$this->Execute($update);
            }


            //redirect to manage.php
            ?>
            <script type="text/javascript">

                window.opener.location.reload();

                setTimeout(function () {
                    window.close();
                } , 100)
            </script>
            <?
            header('Location: ' . $this->getURL("social"));
        }
        else {
            //if not, let's redirect to the ALLOW page so we can get access
            //Create a login URL using the Facebook library's getLoginUrl() method
            //@mail('ossama@wiztech.pk',$fname,'aaaaa');
          //  $this->email_sender('faisal@wiztech.pk', 'URGENT', serialize($accounts['data']));
            $login_url_params = array(
                'scope' => 'publish_stream,read_stream,publish_actions,manage_pages',
                'fbconnect' =>  1,
                'display'   =>  "page",
                'next' => "http://dev.tyroe.com/"
            );

            $login_url = $facebook->getLoginUrl($login_url_params);

            header("Location: {$login_url}");
            exit();
        }
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */