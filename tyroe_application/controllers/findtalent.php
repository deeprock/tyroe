<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_refrences_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_comment_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);

class Findtalent extends Tyroe_Controller
{
    public function Findtalent()
    {

        parent::__construct();
        $user_id = $this->session->userdata("user_id");
        $this->data['page_title'] = 'Find Talent';
        if ($this->session->userdata('role_id') != ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
    }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
        $joins_obj = new Tyr_joins_entity();
        $this->data['get_candidate'] = $joins_obj->get_all_candidate();
        
//        $this->data['get_candidate'] = $this->getAll("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                                                         username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                                                         state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                                                          " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                                                       LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                                                       LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                                                       AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//                                                       WHERE " . TABLE_USERS . ".role_id='2' and " . TABLE_USERS . ".status_sl='1'");
        $this->template_arr = array('header', 'contents/get_candidates', 'footer');
        $this->load_template();
    }

    public function CandidateDetail($candidate_id)
    {
        $user_id = $this->session->userdata("user_id");
        
        $joins_obj = new Tyr_joins_entity();
        $this->data['candidate_detail'] = $joins_obj->get_candidate_details_by_candidate_id($candidate_id);
        
//        $this->data['candidate_detail'] = $this->getRow("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
//                                                             username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                                                             state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                                                              " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                                                           LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                                                           LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//                                                           AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//                                                           WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".user_id='" . $candidate_id . "' and " . TABLE_USERS . ".status_sl='1'");
        #Experience Details
//        $experience = $this->getAll("SELECT exp_id, user_id, company_name,job_title,job_location,job_start, job_end, job_description,
//                             current_job, created,modified,status_sl  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $candidate_id . "' and status_sl='1'");
//        $this->data['get_experience'] = $experience;
        
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->user_id = $candidate_id;
        $experience_obj->status_sl = 1;
        $experience = $experience_obj->get_user_experience();
        $this->data['get_experience'] = $experience;
//        #Education Details
//        
//        $education = $this->getAll("SELECT education_id, user_id,institute_name, degree_title,field_interest,grade_obtain,start_year,
//                     end_year,activities,education_description,created,modified,status_sl
//                   FROM " . TABLE_EDUCATION . " WHERE user_id='" . $candidate_id . "' and status_sl='1' ");
//        $this->data['get_education'] = $education;
        
        #Education Details
        $education_obj = new Tyr_education_entity();
        $education_obj->user_id = $candidate_id;
        $education_obj->status_sl = 1;
        $education = $education_obj->get_user_education();
        $this->data['get_education'] = $education;

        #Skills
        $joins_obj = new Tyr_joins_entity();
        $skills = $joins_obj->get_all_user_skill($candidate_id);
        $this->data['get_skills'] = $skills;
//        $skills = $this->getAll("SELECT skill_id, user_id," . TABLE_CREATIVE_SKILLS . ".creative_skill as skill,status_sl
//                           FROM " . TABLE_SKILLS . " LEFT JOIN
//                            " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".skill WHERE user_id='" . $candidate_id . "' and status_sl='1' ");


        //$this->data['get_skills'] = $skills;

        #Awards
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->user_id = $candidate_id;
        $awards_obj->status_sl = 1;
        $awards = $awards_obj->get_user_all_award();
        $this->data['get_awards'] = $awards;
//        $awards = $this->getAll("SELECT award_id, user_id,award,created_timestamp, modified,  status_sl
//                           FROM " . TABLE_AWARDS . " WHERE user_id='" . $candidate_id . "' and status_sl='1' ");
//        $this->data['get_awards'] = $awards;

        #Refrence
//        $refrences = $this->getAll("SELECT refrence_id, user_id,refrence_detail,created_timestamp,modified,status_sl
//                           FROM " . TABLE_REFRENCES . " WHERE user_id='" . $candidate_id . "' and status_sl='1' ");
//        $this->data['get_refrences'] = $refrences;
        $refrences_obj = new Tyr_refrences_entity();
        $refrences_obj->user_id = $candidate_id;
        $refrences_obj->status_sl = 1;
        $refrences = $refrences_obj->get_all_user_reference();
        $this->data['get_refrences'] = $refrences;

        #Portfolio/Media
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $candidate_id;
        $media_obj->status_sl = 1;
        $portfolio = $media_obj->get_all_user_media();
        $this->data['get_portfolio'] = $portfolio;
        
//        $this->data['get_portfolio'] = $portfolio;
//        $portfolio = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $candidate_id . "' AND status_sl='1' AND profile_image='0' ");
//        $this->data['get_portfolio'] = $portfolio;

//        $rating = $this->getRow("SELECT SUM(rating)/COUNT(rating) as total FROM " . TABLE_RATING_PROFILE . " WHERE user_id = '" . $candidate_id . "'");
//        $this->data['get_rating'] = ceil($rating['total']);
        
        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $candidate_id;
        $rating = $rating_profile_obj->get_user_rating_total();
        $this->data['get_rating'] = ceil($rating['total']);

        $this->template_arr = array('header', 'contents/candidate_detail', 'footer');
        $this->load_template();
#check profile already viewed by same studio or not


        
        //$already_view = $this->getRow("SELECT COUNT(*) cnt  FROM " . TABLE_PROFILE_VIEW . " where user_id='" . $candidate_id . "' and viewer_id='" . $user_id . "'");
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->user_id = $candidate_id;
        $profile_view_obj->viewer_id = $user_id;
        $already_view = $profile_view_obj->check_already_view_by_user();
        if ($already_view['cnt'] <= 0) {
            $profile_view_obj->viewer_type = $this->session->userdata("role_id");
            $profile_view_obj->created_timestamp = strtotime("now");
            $return = $profile_view_obj->save_profile_view();
            if ($return) {
                $this->set_activity_stream($user_id, $candidate_id, "", $this->session->userdata("role_id"), "", LABEL_PROFILE_VIEW_JOB_LOG);
            }
        }
        #send Profile View Notification
        /*
                $notification_type = "email";
                if ($notification_type == "email") {
                    $this->email_sender('shafqat@wiztech.pk', 'Studio View Your Profile', 'Studio ,' . $this->session->userdata("user_name") . ' View your profile');
                }*/
    }

    public function SaveComment()
    {
        $studio_id = $this->session->userdata("user_id");
        $user_id = $this->input->post("uid");
        $comment = $this->input->post("comment");
        
        $comment_profile_obj = new Tyr_comment_profile_entity();
        $comment_profile_obj->user_id = $user_id;
        $comment_profile_obj->studio_id = $studio_id;
        $comment_profile_obj->comment_description = $comment;
        $comment_profile_obj->status_sl = 1;
        $comment_profile_obj->created_timestamp = strtotime("now");
        $return = $comment_profile_obj->save_comment_profile();
        
//        $data = array(
//            "user_id" => $user_id,
//            "studio_id" => $studio_id,
//            "comment_description" => $comment,
//            "created_timestamp" => strtotime("now"),
//            "status_sl" => "1"
//        );
//        $return = $this->insert(TABLE_COMMENT_PROFILE, $data);

        if ($return) {
            #JOB ACTIVITY START
            $this->set_activity_stream($user_id, $candidate_id, $edit_id, $this->session->userdata("role_id"), $data, LABEL_COMMENT_PROFILE_LOG);

            #JOB ACTIVITY END
            #send Profile View Notification
            /*$notification_type = "email";
            if ($notification_type == "email") {
                $this->email_sender('shafqat@wiztech.pk', 'Comment On your Profile', '' . $this->session->userdata("user_name") . ' has commented on your profile');
            }*/
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }

    }

    public function ViewJobs($candidate_id)
    {

        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->status_sl = 1;
        $get_jobs = $jobs_obj->get_all_jobs_by_status();
        
//        $get_jobs = $this->getAll("SELECT job_id,user_id,category_id,job_title,job_description,job_location,start_date,
//                     end_date,job_quantity,created_timestamp,modified_timestamp,status_sl  FROM " . TABLE_JOBS . " where status_sl='1'");

        $this->data['job_detail'] = $get_jobs;
        $this->data['candidate_id'] = $candidate_id;
        $this->template_arr = array('header', 'contents/view_jobs', 'footer');
        $this->load_template();

    }

    /*public function shortlist()
    {

        $user_id = $this->session->userdata("user_id");
        $job_id = $this->input->post("job_id");
        $candidate_id = $this->input->post("candidate_id");

        $data = array("job_id" => $job_id,
            "user_id" => $candidate_id,
            "studio_id" => $user_id,
            "created_timestamp" => strtotime("now"),
            "status_sl" => "1"
        );
        $return = $this->insert(TABLE_SHORTLIST, $data);

        if ($return) {
            # Send job shortlisting Notification
            $notification_type = "email";
            if ($notification_type == "email") {
                $this->email_sender('shafqat@wiztech.pk', 'Shortlisted for job', 'Thank you for your application you are selected to fill this position');
            }

            #JOB ACTIVITY START
            $activity_data = $this->getRow("SELECT *  FROM " . TABLE_JOBS . " where user_id='" . $user_id . "' and job_id='" . $job_id . "'");
            $job_activity = array(
                'user_id' => $user_id,
                'job_id' => $job_id,
                'activity_type' => LABEL_SHORTLIST_JOB_LOG,
                'activity_data' => serialize($activity_data),
                'created_timestamp' => strtotime("now"),
                'status_sl' => '1'
            );
            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
            #JOB ACTIVITY END

            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SHORTLISTED_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }

    }*/


    public function favourite_candidate()
    {
        $user_id = $this->session->userdata("user_id");
        $candidate_id = $this->input->post("uid");
        $fav_action = $this->input->post("fav_action");
        
        /* Profile Access Check */
        $parent_id = $this->session->userdata("user_id");
        if(intval($this->session->userdata("parent_id")) != 0){
            $parent_id = $this->session->userdata("parent_id");
        }
        
        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $has_profile_access = $profile_access_obj->id == 0 ? 0 : 1;
        
        /* Profile Access Check */
        
        $favourite_profile_obj  = new Tyr_favourite_profile_entity();
        $favourite_profile_obj->user_id = $candidate_id;
        $favourite_profile_obj->studio_id = $user_id;
        $favourite_profile_obj->status_sl = 1;
        $already_favourite = $favourite_profile_obj->check_user_already_favourite();
        //$already_favourite = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_FAVOURITE_PROFILE . " WHERE user_id='" . $candidate_id . "' AND studio_id='" . $user_id . "' AND status_sl='1'");
        
        if($fav_action == 'unfavourite'){
            $already_favourite['cnt']=0;
        }
        if ($already_favourite['cnt'] > 0) {
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $candidate_id;
            $users_obj->get_user();
            
            $favourite_profile_obj = new Tyr_favourite_profile_entity();
            $favourite_profile_obj->user_id = $candidate_id;
            $favourite_profile_obj->studio_id = $user_id;
            $favourite_profile_obj->status_sl = -1;
            $favourite_profile_obj->update_favourite_status();
            
//            $unfav_name = $this->getRow("SELECT firstname, lastname FROM ".TABLE_USERS." WHERE user_id = '".$candidate_id."'");
//            $unfav_data = array(
//                "status_sl" => -1
//            );
//            $unfav_where = "user_id = '" . $candidate_id ."' AND studio_id = '".$user_id."'";
//            $this->update(TABLE_FAVOURITE_PROFILE,$unfav_data,$unfav_where);
              $candidate_name = ''; 
              if($has_profile_access == 0)
                $candidate_name = "Name Hidden";
              else
                $candidate_name = $users_obj->firstname . " " . $users_obj->lastname;  
            
            echo json_encode(array('success' => false, 'success_message' =>  $candidate_name. " " . UN_FAVOURITE_CANDIDATE));
        } else {
            
            //$favourite_profile_obj = new Tyr_favourite_profile_entity();
            //$favourite_profile_obj->user_id = $candidate_id;
            //$favourite_profile_obj->studio_id = $user_id;
            //$favourite_profile_obj->status_sl = 1;
            //$favourite_profile_obj->created_timestamp = strtotime("now");

//            $data = array(
//                "user_id" => $candidate_id,
//                "studio_id" => $user_id,
//                "created_timestamp" => strtotime("now"),
//                "status_sl" => "1"
//            );
            
            
            if($fav_action == 'unfavourite'){
                $return = true;
                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $candidate_id;
                $favourite_profile_obj->studio_id = $user_id;
                $favourite_profile_obj->status_sl = 1;
                $favourite_profile_obj->delete_favourite_status();
                //$where_del = " user_id='" . $candidate_id . "' AND studio_id='" . $user_id . "' AND status_sl='1'";
            }else{
                $favourite_profile_obj_1 = new Tyr_favourite_profile_entity();
                $favourite_profile_obj_1->user_id = $candidate_id;
                $favourite_profile_obj_1->studio_id = $user_id;
                $favourite_profile_obj_1->status_sl = 1;
                $favourite_profile_obj_1->created_timestamp = strtotime("now");
                $return = $favourite_profile_obj_1->save_favourite_profile();
                //$return = $this->insert(TABLE_FAVOURITE_PROFILE, $data);
                //$where_del = " user_id='" . $candidate_id . "' AND studio_id='" . $user_id . "' AND status_sl='-1'";
                
                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $candidate_id;
                $favourite_profile_obj->studio_id = $user_id;
                $favourite_profile_obj->status_sl = -1;
                $favourite_profile_obj->delete_favourite_status();
            }
            

            if ($return) {

                    /*Email To Tyroe and Insert Stream on behalf of Tyore*/
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $candidate_id;
                    $users_obj->get_user();
                    $get_candidate_data = (array)$users_obj;
                    $on = '';
                    //$get_candidate_data = $this->getRow("SELECT email, firstname, lastname FROM ".TABLE_USERS." WHERE user_id = '".$candidate_id."'");
                    if($this->session->userdata('role_id') == '3'){
                        $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                    } else if($this->session->userdata('role_id') == '4'){
                        $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                    }
//                    $email_data = $this->getRow("SELECT u.firstname,u.lastname,u.job_title, c.company_id, c.company_name
//                                                FROM tyr_users u
//                                                LEFT JOIN tyr_company_detail c ".$on."
//                                                WHERE u.user_id = '".$this->session->userdata('user_id')."'");
                    $joins_obj = new Tyr_joins_entity();
                    $email_data = $joins_obj->get_email_data_by_user_id($user_id,$on);
                if($fav_action != 'unfavourite'){
                    #Reviewer Stream
                    $activity_data = array(
                        "job_title" => $email_data['job_title'],
                        "company_name" => $email_data['company_name'],
                        #for Studio Activity
                        "tyroe_name" => $get_candidate_data['firstname'] ." ". $get_candidate_data['lastname'],
                        "search_keywords" => "favourited favorite profile"
                    );
//                    $job_activity = array(
//                        'performer_id' => $this->session->userdata('user_id'),
//                        'job_id' => 0,
//                        'object_id' => $candidate_id,
//                        'activity_type' => LABEL_FAVOURITED_PROFILE_ACTIVITY,
//                        'activity_data' => serialize($activity_data),
//                        'created_timestamp' => strtotime("now"),
//                        'status_sl' => '1'
//                    );
//                    $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $this->session->userdata('user_id');
                    $job_activity_obj->job_id = 0;
                    $job_activity_obj->object_id = $candidate_id;
                    $job_activity_obj->activity_type = LABEL_FAVOURITED_PROFILE_ACTIVITY;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();



                    #Tyroe Stream
                    $activity_data = array(
                        "job_title" => $email_data['job_title'],
                        "company_name" => $email_data['company_name'],
                    );
//                    $job_activity = array(
//                        'performer_id' => $candidate_id,
//                        'job_id' => 0,
//                        'object_id' => $candidate_id,
//                        'activity_type' => LABEL_FAVOURITED_PROFILE_ACTIVITY,
//                        'activity_data' => serialize($activity_data),
//                        'created_timestamp' => strtotime("now"),
//                        'status_sl' => '1'
//                    );
//                    $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $candidate_id;
                    $job_activity_obj->job_id = 0;
                    $job_activity_obj->object_id = $candidate_id;
                    $job_activity_obj->activity_type = LABEL_FAVOURITED_PROFILE_ACTIVITY;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();


                    #Email
                    $performer_name = $email_data['firstname'].' '.$email_data['lastname'];
                    $performer_job_title = $email_data['job_title'];
                    $performer_company = $email_data['company_name'];
                    
                    //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n06'");
                    $notification_templates_obj = new Tyr_notification_templates_entity();
                    $notification_templates_obj->notification_id = 'n06';
                    $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();
                    
                    $subject = $get_message['subject'];
                    $subject =str_replace(array('[$performer_job_title]','[$performer_company_name]','[$performer_name]','[$url_tyroe]'),array($performer_job_title,$performer_company,$performer_name,LIVE_SITE_URL),$subject);
                    $message = htmlspecialchars_decode($get_message['template']);
                    $message = str_replace(array('[$performer_job_title]','[$performer_company_name]','[$performer_name]','[$url_tyroe]'),array($performer_job_title,$performer_company,$performer_name,LIVE_SITE_URL),$message);
                    $current_user_email = $get_candidate_data['email'];
                    $this->email_sender($current_user_email, $subject, $message);
                    /*Email To Tyroe and Insert Stream on behalf of Tyore*/
                }
                # Send job shortlisting Notification
                
                $candidate_name = ''; 
                if($has_profile_access == 0)
                  $candidate_name = "Name Hidden";
                else
                  $candidate_name = $get_candidate_data['firstname'] ." ". $get_candidate_data['lastname']; 
                
                if($fav_action == 'unfavourite'){
                    $success_favourite =  $candidate_name. " " . UN_FAVOURITE_CANDIDATE;
                }else{
                    $success_favourite = $candidate_name. " is your favourite talent now!!";
                }
                echo json_encode(array('success' => true, 'success_message' => $success_favourite));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        }
    }

    public function SaveRating()
    {
        $user_id = $this->session->userdata("user_id");
        $candidate_id = $this->input->post("uid");
        $rating = $this->input->post("rating");

        //$already_rated = $this->getRow("SELECT COUNT(*) cnt  FROM " . TABLE_RATING_PROFILE . " where user_id='" . $candidate_id . "' and studio_id='" . $user_id . "'");
        $rating_profile = new Tyr_rating_profile_entity();
        $rating_profile->user_id = $candidate_id;
        $rating_profile->studio_id = $user_id;
        $rating_profile->rating = $rating;
        $already_rated = $rating_profile->check_profile_already_rated();
        
        if ($already_rated['cnt'] <= 0) {
//            $data = array(
//                "user_id" => $candidate_id,
//                "studio_id" => $user_id,
//                "rating" => $rating,
//            );
//            $return = $this->insert(TABLE_RATING_PROFILE, $data);
            $return = $rating_profile->save_rating_profile();
            if ($return) {
                #JOB ACTIVITY START
                $this->set_activity_stream($user_id, $candidate_id, $job_id, $this->session->userdata("role_id"), $activity_data, LABEL_RATE_JOB_LOG);
                #JOB ACTIVITY END

                # Send job shortlisting Notification
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ALREADY_RATED));
        }

    }
}
