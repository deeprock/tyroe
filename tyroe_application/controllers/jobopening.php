<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);

class jobopening extends Tyroe_Controller {
    private $job_roles_mapping_obj;
    public function jobopening() {
        parent::__construct();
        if ($this->input->post('website_share_post') != '') {
            $this->session->set_userdata('redirect_to_job_opening', 'yes');
        }
        $this->data['page_title'] = 'Openings';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . OPENINGS_SUB_MENU . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_OPENING . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . OPENINGS_SUB_MENU . ') ');
        }

        $this->load->library('form_validation');
        $user_id = $this->session->userdata("user_id");

        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->user_id = $user_id;
        $jobs_obj->status_sl = 1;
        $jobs_obj->archived_status = 0;
        $this->data['total_jobs'] = $jobs_obj->get_jobs_count_by_user();

        //$this->data['total_jobs'] = $this->getRow("SELECT COUNT(*) AS total FROM " . TABLE_JOBS . "  WHERE user_id='" . $user_id . "' and status_sl='1' and archived_status='0' ");

        $job_activity = array(
            'user_id' => $user_id,
            'created_timestamp' => strtotime("now"),
            'status_sl' => '1'
        );

        $job_type_obj = new Tyr_job_type_entity();
        $get_job_type = $job_type_obj->get_all_job_types();
        //$get_job_type = $this->getAll("SELECT job_type_id, job_type FROM " . TABLE_JOB_TYPE);
        $this->data['get_job_type'] = $get_job_type;
        
        $this->job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
        $this->job_roles_mapping_obj->job_id = $job_id;
        $this->job_roles_mapping_obj->user_id = $user_id;
        $this->job_roles_mapping_obj->get_job_role_mapping();
        $this->data['job_role'] = (array)$this->job_roles_mapping_obj;
    }

    public function index() {
     
        $orderby = $this->input->post("orderby");
        $country_id = $this->input->post("country_id");
        $skill_level_id = $this->input->post("skill_level_id");
        $creative_skill = $this->input->post("creative_skill");
        $job_city = $this->input->post("job_city");
        $keyword = $this->input->post("keyword");
        $start_date = strtotime($this->input->post("start_date"));
        //$end_date = strtotime($this->input->post("end_date"));
        $job_type = $this->input->post("job_type");
        $job_filter = $this->input->post("job_filter");

        $industry_obj = new Tyr_industry_entity();
        $get_job_industries = $industry_obj->get_all_industries();
        //$get_job_industries = $this->getAll("SELECT industry_id,industry_name from ".TABLE_INDUSTRY);
        $this->data['job_industries'] = $get_job_industries;

        $this->data['studio_name'] = $this->session->userdata('user_name');
        $job_id = $this->input->post("id");
        if ($job_id != "") {
            $where_job = " AND job.job_id='" . $job_id . "'";
        }

        $user_id = $this->session->userdata("user_id");
        //total collaborators

        $users_obj = new Tyr_users_entity();
        $users_obj->parent_id = $user_id;
        $users_obj->status_sl = 1;
        $this->data['collaborators'] = $users_obj->get_count_of_collaborators();

        //$this->data['collaborators'] = $this->getRow("SELECT COUNT(user_id) AS collaborators FROM ".TABLE_USERS." WHERE parent_id = '".$user_id."' AND status_sl = '1'");
        //getting job ids of current user(tyroe,studio,reviewer)
        
        if ($this->session->userdata("role_id") == '4' || $this->session->userdata("role_id") == '3') {
//            $users_obj = new Tyr_users_entity();
//            $users_obj->user_id = $user_id;
//            $users_obj->get_user();
//            if($users_obj->parent_id != 0){
//                $parent_id = $users_obj->parent_id;
//                $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
//                $reviewer_jobs_obj->reviewer_id = $user_id;
//                $reviewer_jobs_obj->studio_id = $parent_id;
//                $reviewer_jobs_obj->status_sl = 1;
//                $get_jobs = $reviewer_jobs_obj->get_job_ids_by_reviewer();
//                    if (empty($get_jobs)) {
//                   $get_jobs['job_id'] = 0;
//               }
//               $where = " and job.job_id IN(" . $get_jobs['job_id'] . ")";
//                
//            }else{
//                $where = " and job.user_id='" . $user_id . "'";
//            }
            
            $job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
            $job_roles_mapping_obj->is_admin = 1;
            $job_roles_mapping_obj->is_reviewer = 1;
            $job_roles_mapping_obj->user_id = $user_id;
            $get_jobs = $job_roles_mapping_obj->get_user_job_ids();
            if($get_jobs['job_id'] != ''){
                $where = " and job.job_id IN(" . $get_jobs['job_id'] . ")";
            }else{
                $where = " and job.job_id IN(0)";
            }
            //$get_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id,reviewer_id FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $user_id . "' AND studio_id = '".$this->session->userdata('parent_id')."' AND status_sl = 1 GROUP BY reviewer_id");
        }  else {
            if ($this->input->post('latest_jobs_ids') != "") {
                $where = " and job.job_id IN(" . $this->input->post('latest_jobs_ids') . ")";
            } else if ($this->input->post('latest_shortlist_jobs_ids') != "") {
                $where = " and job.job_id IN(" . $this->input->post('latest_shortlist_jobs_ids') . ")";
            } else {
                $where = "";
                if ($orderby == "") {
                    $orderby = "desc";
                } else {
                    $orderby = $orderby;
                }
                if ($country_id != "") {
                    $where .= "  AND job.country_id = '" . $country_id . "'";
                }
                if ($skill_level_id != "") {
                    $where .= " AND job.job_level_id ='" . $skill_level_id . "'";
                }
                if ($job_city != "" && $job_city != 'undefined' && $job_city != 'Select City') {
                    $where .= "AND job.job_city = '" . $job_city . "'";
                }
                if ($keyword != "") {
                    $where .= " AND ( job.job_title LIKE '%" . $keyword . "%'
                    OR CONCAT(users.firstname,' ',users.lastname) LIKE '%" . $keyword . "%' )
                    ";
                }
                if ($job_type != "") {
                    $where .= "AND job.job_type = '" . $job_type . "'";
                }
                if ($job_filter != "") {
                    if ($job_filter == "invite") {
                        $where .= "AND job_detail.job_status_id = '1'";
                    } else if ($job_filter == "shortlist") {
                        $where .= "AND job_detail.job_status_id = '3'";
                    }
                }
                $this->data['country_id'] = $country_id;
                $this->data['skill_level_id'] = $skill_level_id;
                $this->data['creative_skill'] = $creative_skill;
                $this->data['job_city'] = $job_city;
                $this->data['keyword'] = $keyword;
                $this->data['start_date'] = $start_date;
                $this->data['job_type'] = $job_type;
                $this->data['job_filter'] = $job_filter;
                $this->data['orderby'] = $orderby;
            }
        }

        $bool = $this->input->post('bool');
        $pagintaion_limit = $this->pagination_limit();


//        $total_rows = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . " job
//        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
//        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
//        LEFT JOIN " .TABLE_JOB_DETAIL. " job_detail ON job_detail.job_id=job.job_id AND (job_detail.`tyroe_id` = '' OR job_detail.`tyroe_id` = '".$user_id."')
//        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
//                  WHERE job.status_sl='1'
//                  AND archived_status='0' " . $where. ' ' . $where_job );

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_jobopeining($user_id, $where, $where_job);

        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } elseif ($orderby != "") {
            $order_by = "order by Job_id " . $orderby;
        } else {
            $order_by = " ORDER BY job_id DESC ";
        }


//        $get_jobs = $this->getAll("SELECT job_status_id,invitation_status,job.job_id,job.user_id,users.role_id,category_id,job.job_title,job_description,country.country AS job_location,comp_detail.company_name,
//                job_lev.level_title AS job_skill,start_date,job_type.job_type,users.username,
//                  end_date,job_quantity,job.created_timestamp,job.modified_timestamp,job.status_sl," . TABLE_CITIES . ".city as job_city  FROM " . TABLE_JOBS . " job
//        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
//        LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_CITIES . ".city_id=job.job_city
//        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
//        LEFT JOIN " .TABLE_JOB_DETAIL. " job_detail ON job_detail.job_id=job.job_id AND (job_detail.`tyroe_id` = '' OR job_detail.`tyroe_id` = '".$user_id."')
//        LEFT JOIN " .TABLE_COMPANY. " comp_detail ON job.user_id = comp_detail.studio_id
//        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
//                  WHERE job.status_sl='1'
//                  AND archived_status='0' " . $where. ' ' . $where_job . $order_by.$pagintaion_limit['limit']);

        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_job_details_paginated($user_id, $where, $where_job, $order_by, $pagintaion_limit);
        if ($get_jobs != '') {
            foreach ($get_jobs as $key => $jobs) {
                $st_date = date('M j Y', $jobs['start_date']);
                $end_date = date('M j Y', $jobs['end_date']);
                $duration = get_date_diff($st_date, $end_date);
                
                $job_creator_id = $jobs['user_id'];
                $user_obj = new Tyr_users_entity();
                $user_obj->user_id = $job_creator_id;
                $user_obj->get_user();
                $job_parent_id = $user_obj->parent_id;
                if($job_post_user_obj->parent_id == 0){
                    $job_parent_id = $job_creator_id;
                }
                
                $tyr_company_detail_obj = new Tyr_company_detail_entity();
                $tyr_company_detail_obj->studio_id = $job_parent_id;
                $tyr_company_detail_obj->get_company_detail_by_studio();
                $company_id = $tyr_company_detail_obj->parent_id;
                $compnay_logo = trim($tyr_company_detail_obj->logo_image);
                if($company_id != 0){
                    $tyr_company_detail_obj_1 = new Tyr_company_detail_entity();
                    $tyr_company_detail_obj_1->company_id = $company_id;
                    $tyr_company_detail_obj_1->get_company_detail();
                    $compnay_logo = trim($tyr_company_detail_obj_1->logo_image);
                }
                
                $user_id = $this->session->userdata("user_id");
                if ($this->session->userdata("role_id") == 4) {
                    $user_id = $this->session->userdata("parent_id");
                }
                //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$jobs['job_id']."' GROUP BY studio_id");

                $hidden_obj = new Tyr_hidden_entity();
                $hidden_obj->studio_id = $user_id;
                $hidden_obj->job_id = $jobs['job_id'];
                $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

                $hidden_user = 0;
                if (is_array($hiden_tyroes)) {
                    $hidden_user = $hiden_tyroes['user_id'];
                }

                //$candidate_count = $this->getRow("SELECT COUNT(job_status_id) AS total_candidates FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 1 AND invitation_status != -1 AND  tyroe_id NOT IN (" . $hidden_user . ")");
                //$applicant_count = $this->getRow("SELECT COUNT(job_status_id) AS total_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 2 AND invitation_status = 0 AND  tyroe_id NOT IN (" . $hidden_user . ")");
                //$shortlist_count = $this->getRow("SELECT COUNT(job_status_id) AS total_shortlist FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 3 AND invitation_status = 1 AND  tyroe_id NOT IN (" . $hidden_user . ")");

                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 1;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = -1;
                $candidate_count = $job_details_obj->get_candidate_count_except_hidden($hidden_user);

                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 2;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 0;
                $applicant_count = $job_details_obj->get_applicant_count_except_hidden($hidden_user);


                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 3;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 1;
                $shortlist_count = $job_details_obj->get_shortlist_count_except_hidden($hidden_user);


                //Count number of total newest candidate , applicaint, and shortlist
                //$newest_candidate_count = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_candidates FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 1 AND invitation_status != -1  AND tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");
                //$newest_applicant_count = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 2 AND invitation_status = 0  AND tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");
                //$newest_shortlist_count = $this->getRow("SELECT COUNT(newest_candidate_status) AS total_newest_shortlist FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 3 AND invitation_status = 1  AND tyroe_id NOT IN (" . $hidden_user . ") AND newest_candidate_status = 0");

                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 1;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = -1;
                $job_details_obj->newest_candidate_status = 0;
                $newest_candidate_count = $job_details_obj->get_new_candidate_count_except_hidden($hidden_user);


                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 2;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 0;
                $job_details_obj->newest_candidate_status = 0;
                $newest_applicant_count = $job_details_obj->get_new_applicant_count_except_hidden($hidden_user);


                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 3;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 1;
                $job_details_obj->newest_candidate_status = 0;
                $newest_shortlist_count = $job_details_obj->get_new_shortlist_count_except_hidden($hidden_user);


                $parent_id = $this->session->userdata("parent_id");
                if(intval($parent_id) == 0){
                    $parent_id = $user_id;
                }
                
                $job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
                $job_roles_mapping_obj->job_id = $jobs['job_id'];
                $job_roles_mapping_obj->user_id = $jobs['user_id'];
                $job_roles_mapping_obj->get_job_role_mapping();
                
                //End count number of total newest candidate , applicaint, and shortlist
                //check review still pending or not
                if (intval($job_roles_mapping_obj->is_admin) == 1) {

                    //$total_reviewer = $this->getRow("SELECT COUNT(*) AS total_reviewers,job_id FROM ".TABLE_REVIEWER_JOBS." WHERE studio_id = '".$user_id."' AND job_id = '".$jobs['job_id']."'");
                    //$total_tyroes = $this->getAll("SELECT * FROM ".TABLE_JOB_DETAIL." WHERE reviewer_id = ".$user_id." AND job_id = ".$jobs['job_id']." AND job_status_id = 1");

                    $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                    $reviewer_jobs_obj->studio_id = $parent_id;
                    $reviewer_jobs_obj->job_id = $jobs['job_id'];
                    $total_reviewer = $reviewer_jobs_obj->get_reviewers_count_for_studio_job();
                    
                    $total_reviews_needed = intval($total_reviewer['total_reviewers']) * (intval($candidate_count['total_candidates']) + intval($shortlist_count['total_shortlist']));
                    
                    if($total_reviewer['reviewer_ids'] == ''){
                        $total_reviewer['reviewer_ids'] = '-1';
                    }
                    
                    $total_reviews_obj = new Tyr_reviewer_reviews_entity();
                    $total_reviews_obj->job_id = $jobs['job_id'];
                    $total_reviews_obj->studio_id = $parent_id;
                    $total_reviews_obj->status_sl = 1;
                    $total_reviews_arr = $total_reviews_obj->get_all_reviews_count_by_job_id($total_reviewer['reviewer_ids']);
                    
                    $total_reviews_done = intval($total_reviews_arr['review_count']);
                    $reviews_pending_percentage = 0;
                    if($total_reviews_done != 0){
                        $reviews_pending_percentage = $total_reviews_needed - $total_reviews_done;
                        if($reviews_pending_percentage < 0){
                            $reviews_pending_percentage = 100;
                        }else{
                            $reviews_pending_percentage = 100 - (($reviews_pending_percentage/$total_reviews_needed) * 100);
                        }
                    }
                    $total_review_candidate_percent = $reviews_pending_percentage;
                } else if (intval($job_roles_mapping_obj->is_reviewer) == 1) {
//                    $parent_id = $this->session->userdata('parent_id');
//                    $where_reveiw = " AND studio_id = '" . $parent_id . "' AND reviewer_id = '" . $this->session->userdata('user_id') . "'";

                    //$total_tyroes_by_job = $this->getRow("SELECT COUNT(".TABLE_JOB_DETAIL.".tyroe_id) AS cnt_tyroes, GROUP_CONCAT(".TABLE_JOB_DETAIL.".tyroe_id ORDER BY ".TABLE_JOB_DETAIL.".tyroe_id ASC SEPARATOR ',') as total_tyroes FROM `".TABLE_JOB_DETAIL."` WHERE job_status_id = 1 AND job_id IN(".$jobs['job_id'].")");
//                    $job_details_obj = new Tyr_job_detail_entity();
//                    $job_details_obj->job_id = $jobs['job_id'];
//                    $job_details_obj->job_status_id = 1;
//                    $total_tyroes_by_job = $job_details_obj->get_total_tyroes_for_job();
                    
                    $total_reviews_needed = (intval($candidate_count['total_candidates']) + intval($shortlist_count['total_shortlist']));

                    //$total_reviews = $this->getRow("SELECT count(reviewer_reviews_id) as total_reviews FROM ".TABLE_REVIEWER_REVIEWS." WHERE job_id = ".$jobs['job_id']." AND status_sl = 1 ".$where_reveiw);
                    $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                    $reviewer_reviews_obj->studio_id = $parent_id;
                    $reviewer_reviews_obj->reviewer_id = $this->session->userdata('user_id');
                    $reviewer_reviews_obj->job_id = $jobs['job_id'];
                    $reviewer_reviews_obj->status_sl = 1;
                    $total_reviews = $reviewer_reviews_obj->get_reviews_id_count_for_job();


                    //total REviewed candidate
                    $total_review_candidate = intval($total_reviews['total_reviews']);
                    
                    $reviewed_percentage = (($total_reviews_needed - $total_review_candidate)/$total_reviews_needed) * 100;
                    $total_review_candidate_percent = 100 - $reviewed_percentage;
                }

                if (is_numeric($jobs['job_city'])) {

//                    $getcity = $this->getRow("SELECT city FROM ".TABLE_CITIES." where city_id = '".$jobs['job_city']."'");
//                    $jobs['job_city'] = $getcity['city'];
                    $cities_obj = new Tyr_cities_entity();
                    $cities_obj->city_id = $jobs['job_city'];
                    $cities_obj->get_cities();
                    $jobs['job_city'] = $cities_obj->city;
                }
                $all_jobs[] = array("job_id" => $jobs['job_id'],
                    "user_id" => $jobs['user_id'],
                    "studio_name" => $jobs['username'],
                    "category_id" => $jobs['category_id'],
                    "job_title" => $jobs['job_title'],
                    "duration" => $duration,
                    "company_name" => $jobs['company_name'],
                    "job_description" => $jobs['job_description'],
                    "job_location" => $jobs['job_location'],
                    "job_skill" => $jobs['job_skill'],
                    "start_date" => $jobs['start_date'],
                    "end_date" => $jobs['end_date'],
                    "job_quantity" => $jobs['job_quantity'],
                    "studio_name" => $jobs['username'],
                    "job_city" => $jobs['job_city'],
                    "job_type" => $jobs['job_type'],
                    "job_duration" => "6 months",
                    "job_level" => $jobs['job_level_id'],
                    "total_candidates" => $candidate_count['total_candidates'],
                    "total_applicants" => $applicant_count['total_applicants'],
                    "total_shortlist" => $shortlist_count['total_shortlist'],
                    "newest_total_candidates" => $newest_candidate_count['total_newest_candidates'],
                    "newest_total_applicants" => $newest_applicant_count['total_newest_applicants'],
                    "newest_total_shortlist" => $newest_shortlist_count['total_newest_shortlist'],
                    "total_hidden" => $hidden_count['total_hidden'],
                    "job_status_id" => $jobs['job_status_id'],
                    "invitation_status" => $jobs['invitation_status'],
                    "get_total_reviewed" => $total_review_candidate_percent,
                    "company_logo" => $compnay_logo
                );
            }

            for ($i = 0; $i < count($get_jobs); $i++) {

//               $all_jobs[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
//                                                   FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
//                                                   " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
//                                                   WHERE job_id='" . $get_jobs[$i]['job_id'] . "'");
                $joins_obj = new Tyr_joins_entity();
                $job_id = $get_jobs[$i]['job_id'];
                $all_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($job_id);
            }
        }


        //get team members admin
//        $get_team_admin = $this->getRow('SELECT
//        '. TABLE_USERS .'.user_id,
//        '. TABLE_USERS .'.firstname,
//        '. TABLE_USERS .'.lastname,
//        '. TABLE_USERS .'.user_occupation,
//        '. TABLE_USERS .'.email,
//        '. TABLE_MEDIA .'.media_type,
//        '. TABLE_MEDIA .'.media_name
//        FROM '. TABLE_USERS . '
//        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
//        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
//        WHERE '. TABLE_USERS . '.user_id = '.$user_id.' AND
//        '. TABLE_USERS . '.status_sl = "1"');

        $joins_obj = new Tyr_joins_entity();
        $get_team_admin = $joins_obj->get_team_admin($user_id);
        $this->data['team_admin'] = $get_team_admin;
        //end get team admin
        $this->data['job_detail'] = $all_jobs;
        $this->data['archived'] = 0;
        $this->data['pagination'] = $this->pagination(array('url' => 'jobopening/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        if ($this->session->userdata("role_id") == '2' || $this->session->userdata("role_id") == '5') {
            $this->template_arr = array('header', 'contents/free_job_listing', 'footer');
        } else {
            $this->template_arr = array('header', 'contents/job_openings', 'footer');
        }
        $this->load_template();
    }

    public function openingForm() {
        $get_job = array(
            "category_id" => "",
            "job_title" => "",
            "job_description" => "",
            "job_location" => "",
            "job_skill" => "",
            "start_date" => "",
            "end_date" => "",
            "job_quantity" => "",
            "job_id" => "",
            "created_timestamp" => ""
        );
        $this->data['edit_job'] = $get_job;
        //$predefine_skills = $this->getAll("SELECT * FROM " . TABLE_CREATIVE_SKILLS);
        $creative_skills_obj = new Tyr_creative_skills_entity();
        $predefine_skills = $creative_skills_obj->get_all_creative_skills();

        $this->data["predefine_skills"] = $predefine_skills;
        $this->template_arr = array('header', 'contents/opening_form', 'footer');
        $this->load_template();
    }

    public function EditForm($edit_id) {
        if ($this->session->userdata("role_id") == '4') {
            $user_id = $this->session->userdata("parent_id");
        } else {
            $user_id = $this->session->userdata("user_id");
        }
        if ($edit_id != '') {
//            $get_job = $this->getRow("SELECT job_id,user_id,category_id,country_id," . TABLE_JOBS . ".job_level_id,job_title,job_description,job_location, " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date,
//                          end_date,job_quantity,created_timestamp  FROM " . TABLE_JOBS . "
//                          LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//                          where user_id='" . $user_id . "' and job_id='" . $edit_id . "' and status_sl='1'");
//            $this->data['edit_job'] = $get_job;

            $joins_obj = new Tyr_joins_entity();
            $get_job = $joins_obj->get_edit_job_details($user_id, $edit_id);
            $this->data['edit_job'] = $get_job;



//            $predefine_skills = $this->getAll("SELECT * FROM " . TABLE_CREATIVE_SKILLS);
//            $this->data["predefine_skills"] = $predefine_skills;

            $creative_skills_obj = new Tyr_creative_skills_entity();
            $predefine_skills = $creative_skills_obj->get_all_creative_skills();
            $this->data["predefine_skills"] = $predefine_skills;

//            $skill_data = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
//                                   FROM " . TABLE_JOB_SKILLS . " sk
//                                   LEFT JOIN " . TABLE_CREATIVE_SKILLS . " csk ON (sk.creative_skill_id = csk.creative_skill_id)
//                                   WHERE sk.studio_id='" . $user_id . "'");

            $skill_data = $joins_obj->get_all_skill_data_for_job($user_id);
            $this->data["skill_data"] = $skill_data;
            //$this->data["skill_data"] = $skill_data;
        }

        $this->template_arr = array('header', 'contents/opening_form', 'footer');
        $this->load_template();
    }

    public function DeleteOpening() {
        $user_id = $this->session->userdata("user_id");

        $job_id = $this->input->post("job_id");
//        $where = "job_id ='" . $job_id . "'";
//        $update = array(
//            "status_sl" => "-1",
//            "modified_timestamp" => strtotime("now")
//        );
        //$return = $this->update(TABLE_JOBS, $update, $where);

        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->job_id = $job_id;
        $jobs_obj->status_sl = -1;
        $jobs_obj->modified_timestamp = strtotime("now");
        $return = $jobs_obj->update_job_status_by_id();


        if ($return) {
            #JOB ACTIVITY START
            //$activity_data = $this->getRow("SELECT *  FROM " . TABLE_JOBS . " where user_id='" . $user_id . "' and job_id='" . $job_id . "'");
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->job_id = $job_id;
            $activity_data = $jobs_obj->get_job_details_by_user_job();

//            $job_activity['job_id'] = array(
//                'performer_id' => $user_id,
//                'job_id' => $job_id,
//                'activity_type' => LABEL_DELETE_JOB_LOG,
//                'activity_data' => serialize($activity_data),
//                'created_timestamp' => strtotime("now"),
//                'status_sl' => '1'
//            );
//            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $user_id;
            $job_activity_obj->job_id = $job_id;
            $job_activity_obj->activity_type = LABEL_DELETE_JOB_LOG;
            $job_activity_obj->activity_data = serialize($activity_data);
            $job_activity_obj->status_sl = 1;
            $job_activity_obj->created_timestamp = strtotime("now");
            $job_activity_obj->save_job_activity();
            #JOB ACTIVITY END

            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function JobApply() {
        $job_id = $this->input->post('job_id');
        $studio_reviewer_id = $this->input->post('studio_reviewer_id');
        $action = $this->input->post('action');
        $bool = $this->input->post('bool');
        $user_id = $this->session->userdata("user_id");

        //$already_apply = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $job_id . "' AND tyroe_id='" . $user_id . "' AND status_sl='1'");
        $job_details_1 = new Tyr_job_detail_entity();
        $job_details_1->job_id = $job_id;
        $job_details_1->tyroe_id = $user_id;
        $job_details_1->status_sl = 1;
        $already_apply = $job_details_1->get_alrady_apply_job_count();

        //$today_apply_jobs = $this->getRow("SELECT COUNT(job_detail_id) AS today_job_count FROM " . TABLE_JOB_DETAIL . " WHERE tyroe_id ='" . $user_id . "' AND status_sl='1' AND DATE(FROM_UNIXTIME(created_timestamp)) = CURDATE()");
        $job_details = new Tyr_job_detail_entity();
        $job_details->tyroe_id = $user_id;
        $job_details->status_sl = 1;
        $today_apply_jobs = $job_details->get_today_apply_job_count();


        $perday_hire_job_count = $this->session->userdata("sv_perday_hire");
        if ($today_apply_jobs['today_job_count'] > $perday_hire_job_count) {
            echo json_encode(array('success' => false, 'success_message' => ERROR_JOBAPPLY_EXCEED));
        } else {

            //$is_profile_public = $this->getRow("SELECT COUNT(user_id) cnt FROM " . TABLE_USERS . " WHERE publish_account = 1 AND user_id = '".$user_id."'");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();

            if ($users_obj->publish_account == 0) {
                echo json_encode(array('success' => false, 'success_message' => PUBLISH_PROFILE_BEFORE_APPLY_FOR_JOB));
            } else {
                if ($already_apply['cnt'] > 0 && $bool != "invite") {
                    echo json_encode(array('success' => false, 'success_message' => ALREADY_APPLY_FOR_JOB));
                } else {
                    $job_apply = array(
                        'created_timestamp' => strtotime("now"),
                        'status_sl' => '1'
                    );

                    $job_details_obj = new Tyr_job_detail_entity();
                    $job_details_obj->invitation_status = $action;
                    $job_details_obj->status_sl = 1;
                    $job_details_obj->job_id = $job_id;

                    if ($bool == "invite") {
                        $job_apply['invitation_status'] = $action;
                        //$where = "job_id = '{$job_id}'";
                        //$return = $this->update(TABLE_JOB_DETAIL, $job_apply,$where);die($return);
                        $return = $job_details_obj->update_job_invitation_status();
                    } else {
                        $job_apply['tyroe_id'] = $user_id;
                        $job_apply['job_id'] = $job_id;
                        $job_apply['job_status_id'] = 2;
                        $job_apply['reviewer_id'] = $studio_reviewer_id;
                        //$return = $this->insert(TABLE_JOB_DETAIL, $job_apply);
                        $job_details_obj->tyroe_id = $user_id;
                        $job_details_obj->job_status_id = 2;
                        $job_details_obj->reviewer_id = $studio_reviewer_id;
                        $return = $job_details_obj->save_job_detail();
                    }

                    if ($return) {
                        #JOB ACTIVITY START  -- TYROE
                        //$activity_data = $this->getRow("SELECT *  FROM " . TABLE_JOBS . " where job_id='" . $job_id . "'");
                        $jobs_obj = new Tyr_jobs_entity();
                        $jobs_obj->job_id = $job_id;
                        $jobs_obj->get_jobs();
                        $activity_data = (array) $jobs_obj;

//                        $get_email_data = $this->getRow("SELECT job_id, studio_id, job_title, company_name FROM " . TABLE_JOBS . " j
//                                                      LEFT JOIN ".TABLE_COMPANY." cd ON j.user_id = cd.studio_id
//                                                      WHERE job_id = '".$job_id."'");
                        $joins_obj = new Tyr_joins_entity();
                        $get_email_data = $joins_obj->get_email_data_by_job_id($job_id);


                        $job_name = $get_email_data['job_title'];
                        $company_name = $get_email_data['company_name'];
                        $activity_data['job_name'] = $job_name;
                        $activity_data['company_name'] = $company_name;
                        $activity_data['search_keywords'] = "apply applied accepted declied job";
                        $activity_data['activity_sub_type'] = LABEL_TYROE_APPLIED_FOR_JOB; //This define what actually done.
//                        $job_activity = array(
//                            'performer_id' => $user_id,
//                            'job_id' => $job_id,
//                            'object_id' => $user_id,
//                            'activity_type' => LABEL_APPLY_JOB_LOG,
//                            'activity_data' => serialize($activity_data),
//                            'created_timestamp' => strtotime("now"),
//                            'status_sl' => '1'
//                        );
//                        $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
                        #JOB ACTIVITY END

                        $job_activity_obj = new Tyr_job_activity_entity();
                        $job_activity_obj->performer_id = $user_id;
                        $job_activity_obj->job_id = $job_id;
                        $job_activity_obj->object_id = $user_id;
                        $job_activity_obj->activity_type = LABEL_APPLY_JOB_LOG;
                        $job_activity_obj->activity_data = serialize($activity_data);
                        $job_activity_obj->status_sl = 1;
                        $job_activity_obj->created_timestamp = strtotime("now");
                        $job_activity_obj->save_job_activity();

                        //social media notification

                        /* Email to User */
                        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n01'");

                        $notification_templates_obj = new Tyr_notification_templates_entity();
                        $notification_templates_obj->notification_id = 'n01';
                        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                        $url = $this->getURL("opening/" . strtolower(str_replace(' ', '-', $job_name) . "-" . $this->generate_job_number($job_id)));
                        $subject = $get_message['subject'];
                        $subject = str_replace(array('[$job_title]', '[$company_name]', '[$url_hireme]', '[$url_tyroe]'), array($job_name, $company_name, $url, LIVE_SITE_URL), $subject);
                        $message = htmlspecialchars_decode($get_message['template']);
                        $message = str_replace(array('[$job_title]', '[$company_name]', '[$url_hireme]', '[$url_tyroe]'), array($job_name, $company_name, $url, LIVE_SITE_URL), $message);
                        $applied_candidate_email = $this->session->userdata("user_email");
                        $this->email_sender($applied_candidate_email, $subject, $message);
                        /* Email to User */


                        #JOB ACTIVITY START  -- STUDIO ADMIN
                        $activity_data_studio = array(
                            "tyroe_name" => $this->session->userdata("firstname") . " " . $this->session->userdata("lastname"),
                            "job_id" => $job_id,
                            "job_title" => $activity_data['job_title'],
                            "activity_sub_type" => LABEL_TYROE_APPLIED_FOR_JOB,
                            "search_keywords" => "apply applied accepted declied job"
                        );

//                        $job_activity_studio = array(
//                            'performer_id' => $activity_data['user_id'],
//                            'job_id' => $job_id,
//                            'object_id' => $user_id,
//                            'activity_type' => LABEL_APPLY_JOB_LOG,
//                            'activity_data' => serialize($activity_data_studio),
//                            'created_timestamp' => strtotime("now"),
//                            'status_sl' => '1'
//                        );
//                        $this->insert(TABLE_JOB_ACTIVITY, $job_activity_studio);

                        $job_activity_obj = new Tyr_job_activity_entity();
                        $job_activity_obj->performer_id = $activity_data['user_id'];
                        $job_activity_obj->job_id = $job_id;
                        $job_activity_obj->object_id = $user_id;
                        $job_activity_obj->activity_type = LABEL_APPLY_JOB_LOG;
                        $job_activity_obj->activity_data = serialize($activity_data_studio);
                        $job_activity_obj->status_sl = 1;
                        $job_activity_obj->created_timestamp = strtotime("now");
                        $job_activity_obj->save_job_activity();

                        #JOB ACTIVITY END
                        #JOB ACTIVITY START  -- STUDIO REVIEWER
                        $activity_data_reviewer = array(
                            "tyroe_name" => $this->session->userdata("firstname") . " " . $this->session->userdata("lastname"),
                            "job_id" => $job_id,
                            "job_title" => $activity_data['job_title'],
                            "activity_sub_type" => LABEL_TYROE_APPLIED_FOR_JOB,
                            "search_keywords" => "apply applied accepted declied job"
                        );

                        //$get_reviewers = $this->getAll("SELECT reviewer_id FROM ".TABLE_REVIEWER_JOBS." WHERE job_id = '".$job_id."' AND studio_id = '".$activity_data['user_id']."'");
                        $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                        $reviewer_jobs_obj->studio_id = $activity_data['user_id'];
                        $reviewer_jobs_obj->job_id = $job_id;
                        $get_reviewers = $reviewer_jobs_obj->get_all_reviewer_id_for_job();

                        if (!empty($get_reviewers)) {
                            for ($rev = 0; $rev < count($get_reviewers); $rev++) {
//                                $job_activity_reviewer = array(
//                                    'performer_id' => $get_reviewers[$rev]['reviewer_id'],
//                                    'job_id' => $job_id,
//                                    'object_id' => $user_id,
//                                    'activity_type' => LABEL_APPLY_JOB_LOG,
//                                    'activity_data' => serialize($activity_data_reviewer),
//                                    'created_timestamp' => strtotime("now"),
//                                    'status_sl' => '1'
//                                );
//                                $this->insert(TABLE_JOB_ACTIVITY, $job_activity_reviewer);

                                $job_activity_obj = new Tyr_job_activity_entity();
                                $job_activity_obj->performer_id = $get_reviewers[$rev]['reviewer_id'];
                                $job_activity_obj->job_id = $job_id;
                                $job_activity_obj->object_id = $user_id;
                                $job_activity_obj->activity_type = LABEL_APPLY_JOB_LOG;
                                $job_activity_obj->activity_data = serialize($activity_data_reviewer);
                                $job_activity_obj->status_sl = 1;
                                $job_activity_obj->created_timestamp = strtotime("now");
                                $job_activity_obj->save_job_activity();
                            }
                        }
                        #JOB ACTIVITY END

                        echo json_encode(array('success' => true, 'success_message' => SUCCESS_MESSAGE_APPLY_JOB));
                    } else {
                        echo json_encode(array('success' => false, 'success_message' => ERROR_MESSAGE_APPLY_JOB));
                    }
                }
            }
        }
    }

    public function ArchivedOpenings() {
        $user_id = $this->session->userdata("user_id");
        if ($this->session->userdata("role_id") == '2') {
            $where = "";
        } else if ($this->session->userdata("role_id") == '4') {
            $where = " and user_id='" . $this->session->userdata("parent_id") . "'";
        } else {
            $where = " and user_id='" . $user_id . "'";
        }
        $this->data['archived_pagination'] = true;
        $pagintaion_limit = $this->pagination_limit();

//        $total_rows = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . "
//                       LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//                      where status_sl='1' and archived_status='1'" . $where);

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_archive_job($where);


//        $get_jobs = $this->getAll("SELECT job_id,user_id,category_id,job_title,job_description,start_date,
//             end_date,job_quantity,created_timestamp,modified_timestamp," . TABLE_JOB_TYPE . ".job_type," . TABLE_CITIES . ".city as job_city," . TABLE_COUNTRIES . ".country AS job_location  FROM " . TABLE_JOBS . "
//               LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//               LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_CITIES . ".city_id=" . TABLE_JOBS . ".job_city
//               LEFT JOIN " . TABLE_JOB_TYPE . "  ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
//              where " . TABLE_JOBS . ".status_sl='1' and archived_status='1'" . $where . $pagintaion_limit['limit']);

        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_job_details_archive_job($where, $pagintaion_limit);
       
        foreach ($get_jobs as $jobs) {
            //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$jobs['job_id']."' GROUP BY studio_id");

            $hidden_obj = new Tyr_hidden_entity();
            $hidden_obj->studio_id = $user_id;
            $hidden_obj->job_id = $jobs['job_id'];
            $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

            $hidden_user = 0;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            }


            //$candidate_count = $this->getRow("SELECT COUNT(job_status_id) AS total_candidates FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 1 AND invitation_status != -1 AND status_sl=1 AND tyroe_id NOT IN (" . $hidden_user . ")");
            //$applicant_count = $this->getRow("SELECT COUNT(job_status_id) AS total_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 2 AND invitation_status = 0 AND status_sl=1 AND tyroe_id NOT IN (" . $hidden_user . ")");
            //$shortlist_count = $this->getRow("SELECT COUNT(job_status_id) AS total_shortlist FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 3 AND invitation_status = 1 AND status_sl=1 AND tyroe_id NOT IN (" . $hidden_user . ")");

            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = -1;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 1;
            $candidate_count = $job_details_obj->get_candidate_count_except_hidden($hidden_user);


            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = 2;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 0;
            $applicant_count = $job_details_obj->get_applicant_count_except_hidden($hidden_user);


            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = 3;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 1;
            $shortlist_count = $job_details_obj->get_shortlist_count_except_hidden($hidden_user);


            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = -1;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 1;
            $job_details_obj->newest_candidate_status = 0;
            $newest_candidate_count = $job_details_obj->get_new_candidate_count_except_hidden($hidden_user);


            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = 2;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 0;
            $job_details_obj->newest_candidate_status = 0;
            $newest_applicant_count = $job_details_obj->get_new_applicant_count_except_hidden($hidden_user);


            $job_details_obj = new Tyr_job_detail_entity();
            $job_details_obj->job_id = $jobs['job_id'];
            $job_details_obj->job_status_id = 3;
            $job_details_obj->status_sl = 1;
            $job_details_obj->invitation_status = 1;
            $job_details_obj->newest_candidate_status = 0;
            $newest_shortlist_count = $job_details_obj->get_new_shortlist_count_except_hidden($hidden_user);

            $all_jobs[] = array("job_id" => $jobs['job_id'],
                "user_id" => $jobs['user_id'],
                "studio_name" => $jobs['username'],
                "category_id" => $jobs['category_id'],
                "job_title" => $jobs['job_title'],
                "job_description" => $jobs['job_description'],
                "job_location" => $jobs['job_location'],
                "job_skill" => $jobs['job_skill'],
                "start_date" => $jobs['start_date'],
                "end_date" => $jobs['end_date'],
                "job_quantity" => $jobs['job_quantity'],
                "studio_name" => $jobs['username'],
                "job_city" => $jobs['job_city'],
                "job_type" => $jobs['job_type'],
                "job_duration" => "6 months",
                "job_level" => $jobs['job_level_id'],
                "total_candidates" => $candidate_count['total_candidates'],
                "total_applicants" => $applicant_count['total_applicants'],
                "total_shortlist" => $shortlist_count['total_shortlist'],
                "newest_total_candidates" => $newest_candidate_count['total_newest_candidates'],
                "newest_total_applicants" => $newest_applicant_count['total_newest_applicants'],
                "newest_total_shortlist" => $newest_shortlist_count['total_newest_shortlist'],
                "total_hidden" => $hidden_count['total_hidden'],
                "job_status_id" => $jobs['job_status_id'],
                "invitation_status" => $jobs['invitation_status']
            );
        }
        $this->data['job_detail'] = $all_jobs;
        $this->data['archived'] = 1;
        $this->data['pagination'] = $this->pagination(array('url' => 'jobopening/ArchivedOpenings', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('header', 'contents/job_openings', 'footer');
        $this->load_template();
    }

    public function GetInvitation() {
        $user_id = $this->session->userdata("user_id");

        //$get_invite_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id FROM " . TABLE_INVITE_TYROE . " WHERE tyroe_id = '{$user_id}' ");
        $invite_tyroe_obj = new Tyr_invite_tyroe_entity();
        $invite_tyroe_obj->tyroe_id = $user_id;
        $get_invite_jobs = $invite_tyroe_obj->get_tyroe_invite_job_ids();


        if (empty($get_invite_jobs['job_id'])) {
            $get_invite_jobs['job_id'] = -1;
        }

//        $get_jobs = $this->getAll("SELECT j.*, inv.invitation_status  FROM " . TABLE_JOBS . " j
//                 LEFT JOIN " . TABLE_INVITE_TYROE . " inv ON (j.`job_id` = inv.`job_id`) where j.status_sl='1'
//                 and j.archived_status='0' and j.job_id IN(" . $get_invite_jobs['job_id'] . ")  AND inv.tyroe_id = '{$user_id}'");
        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_invite_jobs($get_invite_jobs, $user_id);

        $this->data['get_invite_jobs'] = $get_jobs;
        $this->template_arr = array('header', 'contents/invite_job_listing', 'footer');
        $this->load_template();
    }

    public function AcceptInvitation() {
        $user_id = $this->session->userdata("user_id");
        $job_id = $this->input->post("job_id");
        $update_data = array('invitation_status' => 1);
        $where = "job_id = '{$job_id}' AND tyroe_id = '{$user_id}' AND job_status_id = 1";
        //$this->update(TABLE_JOB_DETAIL, $update_data, $where);

        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->tyroe_id = $user_id;
        $job_details_obj->job_status_id = 1;
        $job_details_obj->invitation_status = 1;
        $job_details_obj->update_job_invitation_status_by_tyroe();


        /* Activity Stream */
//        $getstudio = $this->getRow("SELECT j.job_title, j.user_id, u.email, c.company_name AS job_creator
//                                    FROM tyr_jobs j
//                                    LEFT JOIN tyr_company_detail c ON j.user_id = c.studio_id
//                                    LEFT JOIN tyr_users u ON j.user_id = u.user_id
//                                    WHERE j.job_id = '".$job_id."'");

        $joins_obj = new Tyr_joins_entity();
        $getstudio = $joins_obj->get_studio_details_by_job_id($job_id);

        $job_creator = $getstudio['job_creator'];
        $job_title = $getstudio['job_title'];

        $activity_data = array(
            "job_title" => $job_title,
            "company_name" => $job_creator,
            "activity_sub_type" => LABEL_TYROE_ACCEPT_JOB_INVITATION,
            "job_id" => $job_id,
            "search_keywords" => "accepted job"
        );
        $reviewer_type = 2;
        $activity_type = LABEL_APPLY_JOB_LOG;
        $tyroe_id = $user_id;

//        $job_activity = array(
//            'performer_id' => $user_id,
//            'job_id' => $job_id,
//            'object_id' => $user_id,
//            'activity_type' => $activity_type,
//            'activity_data' => serialize($activity_data),
//            'created_timestamp' => strtotime("now"),
//            'status_sl' => '1'
//        );
//        $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

        $job_activity_obj = new Tyr_job_activity_entity();
        $job_activity_obj->performer_id = $user_id;
        $job_activity_obj->job_id = $job_id;
        $job_activity_obj->object_id = $user_id;
        $job_activity_obj->activity_type = $activity_type;
        $job_activity_obj->activity_data = serialize($activity_data);
        $job_activity_obj->status_sl = 1;
        $job_activity_obj->created_timestamp = strtotime("now");
        $job_activity_obj->save_job_activity();

        /* Activity Stream */

        /* Email AND Stream for STUDIO */
        #Stream
        $studio_activity_data = array(
            "job_title" => $job_title,
            "company_name" => $job_creator,
            "activity_sub_type" => LABEL_TYROE_ACCEPT_JOB_INVITATION,
            "job_id" => $job_id,
            #Studio Data
            "tyroe_name" => $this->session->userdata("firstname") . " " . $this->session->userdata("lastname"),
            "search_keywords" => "apply applied job"
        );


        $reviewer_type = 3;
        $activity_type = LABEL_APPLY_JOB_LOG;
        $tyroe_id = $user_id;


//        $job_activity = array(
//            'performer_id' => $getstudio['user_id'],
//            'job_id' => $job_id,
//            'object_id' => $user_id,
//            'activity_type' => $activity_type,
//            'activity_data' => serialize($studio_activity_data),
//            'created_timestamp' => strtotime("now"),
//            'status_sl' => '1'
//        );
//        $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

        $job_activity_obj = new Tyr_job_activity_entity();
        $job_activity_obj->performer_id = $getstudio['user_id'];
        $job_activity_obj->job_id = $job_id;
        $job_activity_obj->object_id = $user_id;
        $job_activity_obj->activity_type = $activity_type;
        $job_activity_obj->activity_data = serialize($studio_activity_data);
        $job_activity_obj->status_sl = 1;
        $job_activity_obj->created_timestamp = strtotime("now");
        $job_activity_obj->save_job_activity();

        #Email
        $tyroe_name = $studio_activity_data['tyroe_name'];
        $job_title = $studio_activity_data['job_title'];
        $url = $this->getURL("openings/" . strtolower(str_replace(' ', '-', $studio_activity_data['job_title']) . "-" . $this->generate_job_number($job_id)));

        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n08'");
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->notification_id = 'n08';
        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

        $subject = $get_message['subject'];
        $subject = str_replace(array('[$job_title]', '[$tyroe_name]', '[$url_viewopening]', '[$url_tyroe]'), array($job_title, $tyroe_name, $url, LIVE_SITE_URL), $subject);
        $message = htmlspecialchars_decode($get_message['template']);
        $message = str_replace(array('[$job_title]', '[$tyroe_name]', '[$url_viewopening]', '[$url_tyroe]'), array($job_title, $tyroe_name, $url, LIVE_SITE_URL), $message);
        $current_user_email = $getstudio['email'];
        $this->email_sender($current_user_email, $subject, $message);
        /* Email for STUDIO */




        echo json_encode(array('success' => true));
    }

    public function RejectInvitation() {
        $user_id = $this->session->userdata("user_id");
        $job_id = $this->input->post("job_id");
        $update_data = array('invitation_status' => -1);
        $where = "job_id = '{$job_id}' AND tyroe_id = '{$user_id}'";
        ///$this->update(TABLE_JOB_DETAIL, $update_data, $where);

        $job_details_obj = new Tyr_job_detail_entity();
        $job_details_obj->job_id = $job_id;
        $job_details_obj->tyroe_id = $user_id;
        $job_details_obj->job_status_id = 1;
        $job_details_obj->invitation_status = -1;
        $job_details_obj->update_job_invitation_status_by_tyroe();

        /* Activity Stream */
//        $getstudio = $this->getRow("SELECT j.job_title,u.user_id, u.email, c.company_name AS job_creator
//                                    FROM tyr_jobs j
//                                    LEFT JOIN tyr_company_detail c ON j.user_id = c.studio_id
//                                    LEFT JOIN tyr_users u ON j.user_id = u.user_id
//                                    WHERE j.job_id = '".$job_id."'");

        $joins_obj = new Tyr_joins_entity();
        $getstudio = $joins_obj->get_studio_details_by_job_id($job_id);

        $job_creator = $getstudio['job_creator'];
        $job_title = $getstudio['job_title'];
        $activity_data = array(
            "job_title" => $job_title,
            "company_name" => $job_creator,
            "activity_sub_type" => LABEL_TYROE_REJECT_JOB_INVITATION,
            "job_id" => $job_id,
            "search_keywords" => "reject declied job candidate"
        );
        $reviewer_type = 2;
        $activity_type = LABEL_APPLY_JOB_LOG;
        $tyroe_id = $user_id;

//        $job_activity = array(
//            'performer_id' => $tyroe_id,
//            'job_id' => $job_id,
//            'object_id' => $tyroe_id,
//            'activity_type' => $activity_type,
//            'activity_data' => serialize($activity_data),
//            'created_timestamp' => strtotime("now"),
//            'status_sl' => '1'
//        );
//        $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

        $job_activity_obj = new Tyr_job_activity_entity();
        $job_activity_obj->performer_id = $tyroe_id;
        $job_activity_obj->job_id = $job_id;
        $job_activity_obj->object_id = $tyroe_id;
        $job_activity_obj->activity_type = $activity_type;
        $job_activity_obj->activity_data = serialize($activity_data);
        $job_activity_obj->status_sl = 1;
        $job_activity_obj->created_timestamp = strtotime("now");
        $job_activity_obj->save_job_activity();
        /* Activity Stream */

        /* Email AND Stream for STUDIO */
        #Stream
        $studio_activity_data = array(
            "job_title" => $job_title,
            "company_name" => $job_creator,
            "activity_sub_type" => LABEL_TYROE_REJECT_JOB_INVITATION,
            "job_id" => $job_id,
            #Studio Data
            "tyroe_name" => $this->session->userdata("firstname") . " " . $this->session->userdata("lastname"),
            "search_keywords" => "reject rejected declied job"
        );
        $reviewer_type = 3;
        $activity_type = LABEL_APPLY_JOB_LOG;
        $tyroe_id = $user_id;

//        $job_activity = array(
//            'performer_id' => $getstudio['user_id'],
//            'job_id' => $job_id,
//            'object_id' => $user_id,
//            'activity_type' => $activity_type,
//            'activity_data' => serialize($studio_activity_data),
//            'created_timestamp' => strtotime("now"),
//            'status_sl' => '1'
//        );
//        $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

        $job_activity_obj = new Tyr_job_activity_entity();
        $job_activity_obj->performer_id = $getstudio['user_id'];
        $job_activity_obj->job_id = $job_id;
        $job_activity_obj->object_id = $user_id;
        $job_activity_obj->activity_type = $activity_type;
        $job_activity_obj->activity_data = serialize($studio_activity_data);
        $job_activity_obj->status_sl = 1;
        $job_activity_obj->created_timestamp = strtotime("now");
        $job_activity_obj->save_job_activity();

        #Email
        $tyroe_name = $studio_activity_data['tyroe_name'];
        $job_title = $studio_activity_data['job_title'];
        $url = $this->getURL("search");

        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n09'");
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->notification_id = 'n09';
        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

        $subject = $get_message['subject'];
        $subject = str_replace(array('[$job_title]', '[$tyroe_name]', '[$url_search]', '[$url_tyroe]'), array($job_title, $tyroe_name, $url, LIVE_SITE_URL), $subject);
        $message = htmlspecialchars_decode($get_message['template']);
        $message = str_replace(array('[$job_title]', '[$tyroe_name]', '[$url_search]', '[$url_tyroe]'), array($job_title, $tyroe_name, $url, LIVE_SITE_URL), $message);
        $current_user_email = $getstudio['email'];
        $this->email_sender($current_user_email, $subject, $message);

        echo json_encode(array('success' => true));
    }

    public function FilterJobs($data = "") {
        $user_id = $this->session->userdata("user_id");
        $orderby = $this->input->post("orderby");
        $country_id = $this->input->post("country_id");
        $skill_level_id = $this->input->post("skill_level_id");
        $creative_skill = $this->input->post("creative_skill");
        $job_city = $this->input->post("job_city");
        $keyword = $this->input->post("keyword");
        $start_date = strtotime($this->input->post("start_date"));
        $job_type = $this->input->post("job_type");
        $job_filter = $this->input->post("job_filter");

        /* For Email User */
        $this->data['country_id'] = $country_id;
        $this->data['skill_level_id'] = $skill_level_id;
        $this->data['creative_skill'] = $creative_skill;
        $this->data['job_city'] = $job_city;
        $this->data['keyword'] = $keyword;
        $this->data['start_date'] = $start_date;
        $this->data['job_type'] = $job_type;
        $this->data['job_filter'] = $job_filter;
        $this->data['orderby'] = $orderby;

        $job_id = $data;
        if ($job_id != "") {
            if (!is_numeric($job_id)) {
                $job_id = $this->get_number_from_name($job_id);
            }
            $where_values .= " AND job.job_id = '" . $job_id . "'";
        }
        /* For Email User */
        if ($orderby == "") {
            $orderby = "desc";
        } else {
            $orderby = $orderby;
        }
        if ($country_id != "") {
            $where_values .= "  AND job.country_id = '" . $country_id . "'";
        }
        if ($skill_level_id != "") {
            $where_values .= " AND job.job_level_id ='" . $skill_level_id . "'";
        }
        if ($job_city != "" && $job_city != 'undefined' && $job_city != 'Select City') {
            $where_values .= "AND job.job_city = '" . $job_city . "'";
        }

        if ($job_type != "") {
            $where_values .= "AND job.job_type = '" . $job_type . "'";
        }
        if ($job_filter != "") {
            if ($job_filter == "invite") {
                $where_values .= "AND job_detail.job_status_id = '1'";
            } else if ($job_filter == "shortlist") {
                $where_values .= "AND job_detail.job_status_id = '3'";
            }
        }

        if ($start_date != "") {
            $where_values .= "AND job.start_date >= '" . $start_date . "'";
        }
        if ($creative_skill != "") {

            //$get_cskills_id = $this->getRow("SELECT GROUP_CONCAT(creative_skill_id) AS skills_id FROM tyr_creative_skills WHERE creative_skill_id = '".$creative_skill."'");
            $creative_skills_obj = new Tyr_creative_skills_entity();
            $creative_skills_obj->creative_skill_id = $creative_skill;
            $get_cskills_id = $creative_skills_obj->get_creative_skill_ids();
            if (empty($get_cskills_id['skills_id'])) {
                $get_cskills_id['skills_id'] = "-1";
            }

            //$get_cskills_job = $this->getRow("SELECT GROUP_CONCAT(DISTINCT(job_id)) AS jobs_skill_id FROM tyr_job_skills WHERE creative_skill_id IN (".$get_cskills_id['skills_id'].")");
            $job_skills = new Tyr_job_skills_entity();
            $get_cskills_job = $job_skills->get_creative_skill_job_ids($get_cskills_id);
            if (empty($get_cskills_job['jobs_skill_id'])) {
                $get_cskills_job['jobs_skill_id'] = "-1";
            }
            $where_values .= " AND job.job_id IN (" . $get_cskills_job['jobs_skill_id'] . ")";
            //$where_values .= "AND " . TABLE_JOB_SKILLS . ".creative_skill_id = '" . $creative_skill . "'";
        }

        if ($keyword != "") {

            //$get_skills_id = $this->getRow("SELECT GROUP_CONCAT(creative_skill_id) AS skills_id FROM tyr_creative_skills WHERE creative_skill LIKE '%".$keyword."%'");
            $creative_skills_obj = new Tyr_creative_skills_entity();
            $creative_skills_obj->creative_skill = $creative_skill;
            $get_skills_id = $creative_skills_obj->get_relative_creative_skill_ids($keyword);

            if (empty($get_skills_id['skills_id'])) {
                $get_skills_id['skills_id'] = "-1";
            }

            //$get_skills_job = $this->getRow("SELECT GROUP_CONCAT(DISTINCT(job_id)) AS jobs_skill_id FROM tyr_job_skills WHERE creative_skill_id IN (".$get_skills_id['skills_id'].")");
            $job_skills = new Tyr_job_skills_entity();
            $get_skills_job = $job_skills->get_creative_skill_job_ids($get_skills_id);

            if (empty($get_skills_job['jobs_skill_id'])) {
                $get_skills_job['jobs_skill_id'] = "-1";
            }

            $where_values .= " AND (
             job.job_title LIKE '%" . $keyword . "%'
             OR CONCAT(users.firstname,' ',users.lastname) LIKE '%" . $keyword . "%'
             OR comp_detail.company_name LIKE '%" . $keyword . "%'
             OR job.job_id IN (" . $get_skills_job['jobs_skill_id'] . ")
             )
             ";
        }

        if ($this->session->userdata("role_id") == '4') {
            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
            $reviewer_jobs_obj->reviewer_id = $user_id;
            $get_jobs = $reviewer_jobs_obj->get_job_ids_by_reviewer_id();

            //$get_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id,reviewer_id FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $user_id . "' GROUP BY reviewer_id");

            $where = " and job.job_id IN(" . $get_jobs['job_id'] . ")";
            //$where = " and job_id IN(" . $get_jobs['job_id'] . ")";
        } else if ($this->session->userdata("role_id") == '3') {
            $orderby = "desc";
            $where = " and job.user_id='" . $user_id . "'";
        } else {
            $where = "";
        }

        $pagintaion_limit = $this->pagination_limit();
//        $total_rows = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . " job
//                                    LEFT JOIN " . TABLE_JOB_TYPE . " job_type
//                                      ON job.job_type = job_type.job_type_id
//                                    LEFT JOIN " . TABLE_COUNTRIES . " country
//                                      ON job.country_id = country.country_id
//                                    LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev
//                                      ON job.job_level_id = job_lev.job_level_id
//                                    LEFT JOIN " .TABLE_JOB_DETAIL. " job_detail
//                                      ON job_detail.job_id = job.job_id
//                                      AND ( job_detail.tyroe_id = ''
//                                        OR job_detail.tyroe_id = ".$user_id." )
//                                    LEFT JOIN " .TABLE_COMPANY. " comp_detail
//                                      ON job.user_id = comp_detail.studio_id
//                                    INNER JOIN " . TABLE_USERS . " users
//                                      ON job.user_id = users.user_id
//                                  WHERE job.status_sl='1'
//                                  AND archived_status='0'" . $where . " " . $where_values);

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_filter_job($user_id, $where, $where_values);


//        $get_jobs = $this->getAll("SELECT job_status_id,invitation_status,job.job_id,job.user_id,category_id,job.job_title,job_description,country.country AS job_location,
//                        job_lev.level_title AS job_skill,comp_detail.company_name,start_date,job_type.job_type,users.username,
//                          end_date,job_quantity,job.created_timestamp,job.modified_timestamp,job.status_sl," . TABLE_CITIES . ".city as job_city  FROM " . TABLE_JOBS . " job
//                            LEFT JOIN " . TABLE_JOB_TYPE . " job_type
//                              ON job.job_type = job_type.job_type_id
//                            LEFT JOIN " . TABLE_COUNTRIES . " country
//                              ON job.country_id = country.country_id
//                              LEFT JOIN ".TABLE_CITIES." ON ".TABLE_CITIES.".city_id = job.job_city
//                            LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev
//                              ON job.job_level_id = job_lev.job_level_id
//                            LEFT JOIN " .TABLE_JOB_DETAIL. " job_detail
//                              ON job_detail.job_id = job.job_id
//                              AND ( job_detail.tyroe_id = ".$user_id." )
//                            LEFT JOIN " .TABLE_COMPANY. " comp_detail
//                              ON job.user_id=comp_detail.studio_id
//                            INNER JOIN " . TABLE_USERS . " users
//                              ON job.user_id = users.user_id
//                          WHERE job.status_sl='1'
//                          AND archived_status='0'" . $where . " " . $where_values . " ORDER BY job.job_id " . $orderby.$pagintaion_limit['limit']);
        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_job_details_filter_job($user_id, $where, $where_values, $orderby, $pagintaion_limit);
        if ($get_jobs != '') {
            foreach ($get_jobs as $jobs) {

                //$hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $user_id . "' AND job_id = '".$jobs['job_id']."' GROUP BY studio_id");
                $hidden_obj = new Tyr_hidden_entity();
                $hidden_obj->studio_id = $user_id;
                $hidden_obj->job_id = $jobs['job_id'];
                $hiden_tyroes = $hidden_obj->get_hidden_tyroes();

                $hidden_user = 0;
                if (is_array($hiden_tyroes)) {
                    $hidden_user = $hiden_tyroes['user_id'];
                }
                
                $job_creator_id = $jobs['user_id'];
                $user_obj = new Tyr_users_entity();
                $user_obj->user_id = $job_creator_id;
                $user_obj->get_user();
                $job_parent_id = $user_obj->parent_id;
                if($job_post_user_obj->parent_id == 0){
                    $job_parent_id = $job_creator_id;
                }
                
                $tyr_company_detail_obj = new Tyr_company_detail_entity();
                $tyr_company_detail_obj->studio_id = $job_parent_id;
                $tyr_company_detail_obj->get_company_detail_by_studio();
                $company_id = $tyr_company_detail_obj->parent_id;
                $compnay_logo = trim($tyr_company_detail_obj->logo_image);
                if($company_id != 0){
                    $tyr_company_detail_obj_1 = new Tyr_company_detail_entity();
                    $tyr_company_detail_obj_1->company_id = $company_id;
                    $tyr_company_detail_obj_1->get_company_detail();
                    $compnay_logo = trim($tyr_company_detail_obj_1->logo_image);
                }

                $st_date = date('M j Y', $jobs['start_date']);
                $end_date = date('M j Y', $jobs['end_date']);
                $duration = get_date_diff($st_date, $end_date);

                //$reviewer_count = $this->getRow("SELECT COUNT(*) AS total_reviewer FROM " . TABLE_REVIEWER_JOBS . " WHERE job_id='" . $jobs['job_id'] . "'");
                $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs_obj->job_id = $jobs['job_id'];
                $reviewer_jobs_obj->status_sl = 1;
                $reviewer_count = $reviewer_jobs_obj->get_total_reviewers_count_for_job();

                //$applicant_count = $this->getRow("SELECT COUNT(*) AS total_applicants FROM " . TABLE_JOB_APPLY . " WHERE job_id='" . $jobs['job_id'] . "'");
                //$shortlist_count = $this->getRow("SELECT COUNT(*) AS total_shortlist FROM " . TABLE_SHORTLIST . " WHERE job_id='" . $jobs['job_id'] . "'");
                //$hidden_count = $this->getRow("SELECT COUNT(*) AS total_hidden FROM " . TABLE_HIDDEN . " WHERE job_id='" . $jobs['job_id'] . "'");
                $hidden_obj = new Tyr_hidden_entity();
                $hidden_obj->job_id = $jobs['job_id'];
                $hidden_count = $hidden_obj->get_total_hidden_count_by_job();


                //
                //$applicant_count = $this->getRow("SELECT COUNT(job_status_id) AS total_applicants FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 2 AND invitation_status = 0 AND  tyroe_id NOT IN (" . $hidden_user . ")");
                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 2;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 0;
                $applicant_count = $job_details_obj->get_applicant_count_except_hidden($hidden_user);

                //$shortlist_count = $this->getRow("SELECT COUNT(job_status_id) AS total_shortlist FROM " . TABLE_JOB_DETAIL . " WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id = 3 AND invitation_status = 1 AND  tyroe_id NOT IN (" . $hidden_user . ")");
                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->job_id = $jobs['job_id'];
                $job_details_obj->job_status_id = 3;
                $job_details_obj->status_sl = 1;
                $job_details_obj->invitation_status = 1;
                $shortlist_count = $job_details_obj->get_shortlist_count_except_hidden($hidden_user);



                $all_jobs[] = array("job_id" => $jobs['job_id'],
                    "user_id" => $jobs['user_id'],
                    "category_id" => $jobs['category_id'],
                    "job_title" => $jobs['job_title'],
                    "job_description" => $jobs['job_description'],
                    "job_location" => $jobs['job_location'],
                    "job_skill" => $jobs['job_skill'],
                    "duration" => $duration,
                    "company_name" => $jobs['company_name'],
                    "start_date" => $jobs['start_date'],
                    "end_date" => $jobs['end_date'],
                    "job_quantity" => $jobs['job_quantity'],
                    "studio_name" => $jobs['username'],
                    "job_city" => $jobs['job_city'],
                    "job_type" => $jobs['job_type'],
                    "job_duration" => "6",
                    "job_level" => $jobs['job_level_id'],
                    "total_reviewer" => $reviewer_count['total_reviewer'],
                    "total_applicants" => $applicant_count['total_applicants'],
                    "total_shortlist" => $shortlist_count['total_shortlist'],
                    "total_hidden" => $hidden_count['total_hidden'],
                    "job_status_id" => $jobs['job_status_id'],
                    "invitation_status" => $jobs['invitation_status'],
                    "company_logo" => $compnay_logo
                );
            }

            for ($i = 0; $i < count($get_jobs); $i++) {
                if ($creative_skill != "") {
                    $where_skill = " and " . TABLE_JOB_SKILLS . ".creative_skill_id IN (" . $creative_skill . ")";
                }

                //            $all_jobs[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
                //                                           FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
                //                                           " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
                //                                           WHERE job_id='" . $get_jobs[$i]['job_id'] . "'");
                $joins_obj = new Tyr_joins_entity();
                $all_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($get_jobs[$i]['job_id']);
            }
        }
        $this->data['job_detail'] = $all_jobs;
        $this->data['archived'] = 0;
        $this->data['pagination'] = $this->pagination(array('url' => 'jobopening/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        if ($job_id != "") {
            $this->template_arr = array('header', 'contents/free_job_listing', 'footer');
        } else {
            $this->template_arr = array('contents/search/search_job_studio');
        }
        $this->load_template();
    }

    public function FilterExposure() {
        $user_id = $this->session->userdata("user_id");
        $orderby = $this->input->post("orderby");
        $country_id = $this->input->post("country_id");
        $query_var = $this->input->post("query_var");
        $skill_level_id = $this->input->post("skill_level_id");
        $creative_skill = $this->input->post("creative_skill");
        $job_city = $this->input->post("job_city");
        $keyword = $this->input->post("keyword");
        $start_date = strtotime($this->input->post("start_date"));
        $end_date = strtotime($this->input->post("end_date"));

        if ($orderby == "") {
            $orderby = "desc";
        } else {
            $orderby = $orderby;
        }
        if ($country_id != "") {
            $where_values .= "  AND j.country_id = '" . $country_id . "'";
        }
        if ($skill_level_id != "") {
            $where_values .= " AND j.job_level_id ='" . $skill_level_id . "'";
        }
        if ($job_city != "") {
            $where_values .= "AND j.job_city = '" . $job_city . "'";
        }
        if ($keyword != "") {
            $where_values .= "AND j.job_title LIKE '%" . $keyword . "%'";
        }
        if ($start_date != "") {
            $where_values .= "AND j.start_date LIKE '%" . $start_date . "%'";
        }
        if ($end_date != "") {
            $where_values .= "AND j.end_date LIKE '%" . $end_date . "%'";
        }
        if ($creative_skill != "") {
            $where_values .= "AND " . TABLE_JOB_SKILLS . ".creative_skill_id = '" . $creative_skill . "'";
        }

        if ($this->session->userdata("role_id") == '4') {
            $get_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id,reviewer_id FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $user_id . "' GROUP BY reviewer_id");
            $where = " and job_id IN(" . $get_jobs['job_id'] . ")";
            //$where = " and user_id='" . $this->session->userdata("parent_id") . "'";
        } else if ($this->session->userdata("role_id") == '3') {
            $where = " and user_id='" . $user_id . "'";
        } else {
            $where = "";
        }
        if ($query_var == "candidate_filter") {

            //$get_invite_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id FROM " . TABLE_INVITE_TYROE . " WHERE tyroe_id = '{$user_id}' ");
            $invite_tyroe_obj = new Tyr_invite_tyroe_entity();
            $invite_tyroe_obj->tyroe_id = $user_id;
            $get_invite_jobs = $invite_tyroe_obj->get_tyroe_invite_job_ids();

            if (empty($get_invite_jobs['job_id'])) {
                $get_invite_jobs['job_id'] = -1;
            }


//            $get_jobs = $this->getAll("SELECT j.*, inv.invitation_status ,users.username FROM " . TABLE_JOBS . " j
//                                       LEFT JOIN " . TABLE_INVITE_TYROE . " inv ON (j.`job_id` = inv.`job_id`)
//                                         LEFT JOIN " . TABLE_USERS . " users ON(j.user_id= users.user_id)
//                                          LEFT JOIN " . TABLE_JOB_SKILLS . "  ON(" . TABLE_JOB_SKILLS . ".job_id = j.job_id)
//                                        where j.status_sl='1'
//                                       and j.archived_status='0' and j.job_id IN(" . $get_invite_jobs['job_id'] . ")  AND inv.tyroe_id = '{$user_id}'
//                                       " . $where_values . "
//                                       ORDER BY job_id " . $orderby);
            $joins_obj = new Tyr_joins_entity();
            $get_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($get_jobs[$i]['job_id']);

            for ($i = 0; $i < count($get_jobs); $i++) {

//                $get_jobs[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
//                                                        FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
//                                                        " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
//                                                        WHERE job_id='" . $get_jobs[$i]['job_id'] . "'");
                $joins_obj = new Tyr_joins_entity();
                $get_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($get_jobs[$i]['job_id']);
            }


            $this->data['job_detail'] = $get_jobs;
            $this->data['hidden_field'] = $query_var;
        } else if ($query_var == "shortlist_filter") {

//            $get_jobs = $this->getAll("SELECT j.*,users.username FROM " . TABLE_JOBS . " j
//                           LEFT JOIN " . TABLE_USERS . " users ON(j.user_id= users.user_id)
//                           LEFT JOIN " . TABLE_SHORTLIST . " shortlist ON(shortlist.job_id= j.job_id)
//                           LEFT JOIN " . TABLE_JOB_SKILLS . "  ON(" . TABLE_JOB_SKILLS . ".job_id = j.job_id)
//                           WHERE shortlist.user_id='" . $user_id . "'  AND j.archived_status='0'
//                           " . $where_values . "
//                           ORDER BY job_id " . $orderby);
            $joins_obj = new Tyr_joins_entity();
            $joins_obj->get_all_shortlist_invite_jobs_details($user_id, $where_values, $orderby);

            for ($i = 0; $i < count($get_jobs); $i++) {

//                $get_jobs[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
//                                   FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
//                                   " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
//                                   WHERE job_id='" . $get_jobs[$i]['job_id'] . "'");
                $joins_obj = new Tyr_joins_entity();
                $get_jobs[$i]['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($get_jobs[$i]['job_id']);
            }

            $this->data['job_detail'] = $get_jobs;
            $this->data['hidden_field'] = $query_var;
        } else if ($query_var == "intresting_filter") {
            $this->data['job_detail'] = 0;
        }


        /* $get_jobs = $this->getAll("SELECT " . TABLE_JOBS . ".job_id,user_id,category_id,job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
          " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city,
          end_date,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl  FROM " . TABLE_JOBS . "
          LEFT JOIN " . TABLE_JOB_SKILLS . "  ON(" . TABLE_JOB_SKILLS . ".job_id = " . TABLE_JOBS . ".job_id)
          LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
          LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
          LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
          where " . TABLE_JOBS . ".status_sl='1'
          and archived_status='0'" . $where . " ".$where_values. " GROUP BY " . TABLE_JOBS . ".job_id" ." ORDER BY job_id " . $orderby );
          //$this->data['job_detail'] = $get_jobs;
          foreach ($get_jobs as $jobs) {
          $reviewer_count = $this->getRow("SELECT COUNT(*) AS total_reviewer FROM " . TABLE_REVIEWER_JOBS . " WHERE job_id='" . $jobs['job_id'] . "'");
          $applicant_count = $this->getRow("SELECT COUNT(*) AS total_applicants FROM " . TABLE_JOB_APPLY . " WHERE job_id='" . $jobs['job_id'] . "'");
          $shortlist_count = $this->getRow("SELECT COUNT(*) AS total_shortlist FROM " . TABLE_SHORTLIST . " WHERE job_id='" . $jobs['job_id'] . "'");
          $hidden_count = $this->getRow("SELECT COUNT(*) AS total_hidden FROM " . TABLE_HIDDEN . " WHERE job_id='" . $jobs['job_id'] . "'");

          $all_jobs[] = array("job_id" => $jobs['job_id'],
          "user_id" => $jobs['user_id'],
          "category_id" => $jobs['category_id'],
          "job_title" => $jobs['job_title'],
          "job_description" => $jobs['job_description'],
          "job_location" => $jobs['job_location'],
          "job_skill" => $jobs['job_skill'],
          "start_date" => $jobs['start_date'],
          "end_date" => $jobs['end_date'],
          "job_quantity" => $jobs['job_quantity'],
          "studio_name" => $jobs['username'],
          "job_city" => $jobs['job_city'],
          "job_type" => $jobs['job_type'],
          "job_duration" => "6",
          "job_level" => $jobs['job_level_id'],
          "total_reviewer" => $reviewer_count['total_reviewer'],
          "total_applicants" => $applicant_count['total_applicants'],
          "total_shortlist" => $shortlist_count['total_shortlist'],
          "total_hidden" => $hidden_count['total_hidden']
          );
          }

          for ($i = 0; $i < count($get_jobs); $i++) {
          if ($creative_skill != "") {
          $where_skill = " and " . TABLE_JOB_SKILLS . ".creative_skill_id IN (".$creative_skill.")";
          }
          $all_jobs[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
          FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
          " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
          WHERE job_id='" . $get_jobs[$i]['job_id'] . "'");
          }
          $this->data['job_detail'] = $all_jobs;

          $this->data['archived'] = 0; */
        //if (count($all_jobs[0]['job_id']) > 0) {
        $this->template_arr = array('contents/search/search_job_exposures');
        $this->load_template();

        /* } else {
          echo "No Match Found";
          } */
    }

    public function getcities() {
        $country_id = $this->input->post('country_id');
        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $country_id . "'");
        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $country_id . "' ORDER BY city ASC");

        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_all_cities_by_country_id();

        $cities = $this->dropdown($cities, '', array('name' => 'city', 'placeholder' => 'City', 'id' => 'city', 'class' => 'city'));
        echo json_encode(array('dropdown' => $cities));
    }

    public function SaveOpening() {
        $user_id = $this->session->userdata("user_id");
        $startdate = $this->input->post("job_startdate");
        $startdate = str_replace('-', '/', $startdate);
        $startdate = strtotime($startdate);
        $enddate = $this->input->post("job_enddate");
        $enddate = str_replace('-', '/', $enddate);
        $enddate = strtotime($enddate);
        $is_admin_reviewer = $this->input->post('is_admin_reviewer');
        $job_quantity = $this->input->post("job_quantity");
        
        $job_details = array(
            'user_id' => $user_id,
            'job_title' => addslashes($this->input->post("job_title")),
            'country_id' => addslashes($this->input->post("job_country")),
            'job_level_id' => addslashes($this->input->post("job_level")),
            'start_date' => $startdate,
            'end_date' => $enddate,
            'job_description' => addslashes($this->input->post("job_description")),
            'job_city' => addslashes($this->input->post("job_city")),
            'job_type' => addslashes($this->input->post("job_type")),
            'team_moderation' => addslashes($this->input->post("team_moderation")),
            'created_timestamp' => strtotime("now"),
            'status_sl' => '1',
            'industry_id' => addslashes($this->input->post("job_industry")),
            'job_quantity' => $job_quantity
        );
        $job_details['job_description'] = nl2br($job_details['job_description']);
        
        if(isset($_REQUEST['edit_id'])){
            $edit_id = $this->input->post("edit_id");
        }else{
            $edit_id = 0;
        }
        
       
        if ($edit_id) {
            $where = "job_id ='" . $edit_id . "'";
            $job_details['modified_timestamp'] = strtotime("now");

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $edit_id;
            $jobs_obj->get_jobs();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->team_moderation = $this->input->post("team_moderation");
            $jobs_obj->job_title = $this->input->post("job_title");
            $jobs_obj->country_id = $this->input->post("job_country");
            $jobs_obj->job_level_id = $this->input->post("job_level");
            $jobs_obj->start_date = $startdate;
            $jobs_obj->end_date = $enddate;
            $jobs_obj->job_description = $this->input->post("job_description");
            $jobs_obj->job_city = $this->input->post("job_city");
            $jobs_obj->job_type = $this->input->post("job_type");
            $jobs_obj->status_sl = 1;
            $jobs_obj->industry_id = $this->input->post("job_industry");
            $jobs_obj->modified_timestamp = strtotime("now");
            if($job_quantity != false){
                $jobs_obj->job_quantity = $job_quantity;
            }
            $return_job = $jobs_obj->update_jobs();

            //$return_job = $this->update(TABLE_JOBS, $job_details, $where);
            //$this->delete(TABLE_JOB_SKILLS,$where);
            $job_skills_obj = new Tyr_job_skills_entity();
            $job_skills_obj->job_id = $edit_id;
            $job_skills_obj->delete_all_job_skill();

            $job_skills = $this->input->post('job_tags');
            $skills = explode(",", $job_skills);

            foreach ($skills as $skill) {
                //matchin that the skill is already exist in databse if not then insert new
                //$getskill = $this->getRow("SELECT creative_skill_id,creative_skill FROM " . TABLE_CREATIVE_SKILLS . " WHERE creative_skill='" . $skill . "'");
                $creative_skills = new Tyr_creative_skills_entity();
                $creative_skills->creative_skill = $skill;
                $creative_skills->get_creative_skill();

                if ($creative_skills->creative_skill_id == 0) {

//                    $creative_skill = array('creative_skill' => $skill);
//                    $return_skill = $this->insert(TABLE_CREATIVE_SKILLS, $creative_skill);

                    $creative_skills->creative_skill = $skill;
                    $creative_skills->save_creative_skills();

//                    $job_skill_values = array(
//                        'studio_id' => $user_id,
//                        'creative_skill_id' => $return_skill,
//                        'job_id' => $edit_id
//                    );

                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $user_id;
                    $job_skills_obj->creative_skill_id = $creative_skills->creative_skill_id;
                    $job_skills_obj->job_id = $edit_id;
                    $job_skills_obj->save_job_skills();

                    $return_job_skills[] = $job_skills_obj->job_skill_id;
                } else {

//                    $creative_skill_id = $creative_skills->creative_skill_id;
//                    $job_skill_values = array(
//                        'studio_id' => $user_id,
//                        'creative_skill_id' => $creative_skill_id,
//                        'job_id' => $edit_id
//                    );
                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $user_id;
                    $job_skills_obj->creative_skill_id = $creative_skills->creative_skill_id;
                    $job_skills_obj->job_id = $edit_id;
                    $job_skills_obj->save_job_skills();

                    $return_job_skills[] = $job_skills_obj->job_skill_id;
                }
            }

            //echo $edit_id;
            echo json_encode($edit_id);
        } else {

            //$company_title = $this->getRow("SELECT company_name FROM ".TABLE_COMPANY." WHERE studio_id = '".$user_id."'");
            $company_details_obj = new Tyr_company_detail_entity();
            $company_details_obj->studio_id = $user_id;
            $company_details_obj->get_company_detail();

            $job_details['company_title'] = $company_details_obj->company_name;

            //$return_job = $this->insert(TABLE_JOBS, $job_details);
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->job_title = $this->input->post("job_title");
            $jobs_obj->country_id = $this->input->post("job_country");
            $jobs_obj->job_level_id = $this->input->post("job_level");
            $jobs_obj->start_date = $startdate;
            $jobs_obj->company_title = $company_details_obj->company_name;
            $jobs_obj->end_date = $enddate;
            $jobs_obj->team_moderation = $this->input->post("team_moderation");
            $jobs_obj->job_description = $this->input->post("job_description");
            $jobs_obj->job_city = $this->input->post("job_city");
            $jobs_obj->job_type = $this->input->post("job_type");
            $jobs_obj->status_sl = 1;
            $jobs_obj->industry_id = $this->input->post("job_industry");
            $jobs_obj->job_quantity = $job_quantity;
            $jobs_obj->archived_status = 1;
            $jobs_obj->save_jobs();
         
            $edit_id = $jobs_obj->job_id;
            $return_job = $edit_id;
            
            $job_role_mapping = new Tyr_job_role_mapping_entity();
            $job_role_mapping->job_id = $return_job;
            $job_role_mapping->user_id = $this->session->userdata("user_id");
            $job_role_mapping->is_admin = 1;
            $job_role_mapping->is_reviewer = $is_admin_reviewer;
            $job_role_mapping->save_job_role_mapping();

            //add reviewers
            $all_selected_reviewrs = $this->input->post('job_reviewers');
            foreach ($all_selected_reviewrs as $reviewer) {

                //$getreviewer = $this->getRow("SELECT * FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $reviewer . "' AND job_id ='".$return_job."'");
                $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs_obj->reviewer_id = $reviewer;
                $reviewer_jobs_obj->job_id = $return_job;
                $getreviewer = $reviewer_jobs_obj->get_all_row_by_reviewer_job();

                //$getreviewer_data = $this->getRow("SELECT email FROM " . TABLE_USERS . " WHERE user_id='" . $reviewer ."'");
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $reviewer;
                $users_obj->get_user();
                $getreviewer_data = array('email' => $users_obj->email);
                
                if ($getreviewer == '') {
                    $reviewr_array = array(
                        'reviewer_id' => $reviewer,
                        'studio_id' => $user_id,
                        'job_id' => $return_job,
                        'created_timestamp' => time(),
                        'status_sl' => '1'
                    );
                    $activity_type = LABEL_NEW_JOB_LOG;
                    //$this->insert(TABLE_REVIEWER_JOBS, $reviewr_array);
                    $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
                    $reviewer_jobs_obj->reviewer_id = $reviewer;
                    $reviewer_jobs_obj->job_id = $return_job;
                    $reviewer_jobs_obj->studio_id = $user_id;
                    $reviewer_jobs_obj->status_sl = 1;
                    $reviewer_jobs_obj->save_reviewer_jobs();
                    
                    $job_role_mapping = new Tyr_job_role_mapping_entity();
                    $job_role_mapping->job_id = $return_job;
                    $job_role_mapping->user_id = $reviewer;
                    $job_role_mapping->is_admin = 0;
                    $job_role_mapping->is_reviewer = 1;
                    $job_role_mapping->save_job_role_mapping();

                    $activity_data = array(
                        "job_opening_title" => addslashes($this->input->post("job_title")),
                        "activity_sub_type" => LABEL_ADD_REVIEWER_TO_JOB_LOG,
                        "job_id" => $return_job,
                        "search_keywords" => "create new job newopening opening openings"
                    );

                    $job_activity = array(
                        'performer_id' => $reviewer,
                        'job_id' => $return_job,
                        'object_id' => $reviewer,
                        'activity_type' => $activity_type,
                        'activity_data' => serialize($activity_data),
                        'created_timestamp' => strtotime("now"),
                        'status_sl' => '1'
                    );

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $reviewer;
                    $job_activity_obj->job_id = $return_job;
                    $job_activity_obj->object_id = $reviewer;
                    $job_activity_obj->activity_type = $activity_type;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();


//                    $studio_admin_detail = $this->getRow("SELECT u.firstname, u.lastname, c.company_name
//                                                                        FROM ".TABLE_USERS." u
//                                                                        LEFT JOIN ".TABLE_COMPANY." c ON u.user_id = c.studio_id
//                                                                        WHERE studio_id = '".$this->session->userdata('user_id')."'");
//                    $studio_name = $studio_admin_detail['firstname'] . " " . $studio_admin_detail['lastname'];
                    //$this->insert(TABLE_JOB_ACTIVITY, $job_activity);

                    $joins_obj = new Tyr_joins_entity();
                    $studio_admin_detail = $joins_obj->get_studio_admin_details($user_id);
                    $studio_name = $studio_admin_detail['firstname'] . " " . $studio_admin_detail['lastname'];

                    #Email Reviewer
                    $company_name = $studio_admin_detail['company_name'];
                    $job_title = $this->input->post("job_title");
                    $url = $this->getURL("openings/" . strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($return_job)));

                    //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n11'");
                    $notification_templates_obj = new Tyr_notification_templates_entity();
                    $notification_templates_obj->notification_id = 'n11';
                    $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                    $subject = $get_message['subject'];
                    $subject = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$studio_admin_company_name]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $studio_name, $company_name, $url, LIVE_SITE_URL), $subject);
                    $message = htmlspecialchars_decode($get_message['template']);
                    $message = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$studio_admin_company_name]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $studio_name, $company_name, $url, LIVE_SITE_URL), $message);
                    $reviewer_email = $getreviewer_data['email'];
                    $this->email_sender($reviewer_email, $subject, $message);
                }
            }
            
            $job_skills = $this->input->post('job_tags');
            $skills = explode(",", $job_skills);
            foreach ($skills as $skill) {
                //matchin that the skill is already exist in databse if not then insert new
                //$getskill = $this->getRow("SELECT creative_skill_id,creative_skill FROM " . TABLE_CREATIVE_SKILLS . " WHERE creative_skill='" . $skill . "'");
                $creative_skills = new Tyr_creative_skills_entity();
                $creative_skills->creative_skill = $skill;
                $creative_skills->get_creative_skill();
                $getskill = (array) $creative_skills;

                if ($creative_skills->creative_skill_id == 0) {

//                    $creative_skill = array('creative_skill' => $skill);
//                    $return_skill = $this->insert(TABLE_CREATIVE_SKILLS, $creative_skill);
//                    $job_skill_values = array(
//                        'studio_id' => $user_id,
//                        'creative_skill_id' => $return_skill,
//                        'job_id' => $return_job
//                    );
                    $creative_skills = new Tyr_creative_skills_entity();
                    $creative_skills->creative_skill = $skill;
                    $creative_skills->save_creative_skills();

                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $user_id;
                    $job_skills_obj->creative_skill_id = $creative_skills->creative_skill_id;
                    $job_skills_obj->job_id = $edit_id;
                    $job_skills_obj->save_job_skills();

                    $return_job_skills[] = $creative_skills->creative_skill_id;
                } else {

//                    $creative_skill_id = $getskill['creative_skill_id'];
//                    $job_skill_values = array(
//                        'studio_id' => $user_id,
//                        'creative_skill_id' => $creative_skill_id,
//                        'job_id' => $return_job
//                    );
//                    $return_job_skills[] = $this->insert(TABLE_JOB_SKILLS, $job_skill_values);

                    $creative_skill_id = $getskill['creative_skill_id'];
                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $user_id;
                    $job_skills_obj->creative_skill_id = $creative_skills->creative_skill_id;
                    $job_skills_obj->job_id = $edit_id;
                    $job_skills_obj->save_job_skills();

                    $return_job_skills[] = $creative_skill_id;
                }
            }

            /* Email To Studio and Insert Stream on behalf of Studio */
            //$activity_data = $this->getRow("SELECT job_title FROM ".TABLE_JOBS." WHERE job_id = '".$return_job."'");

            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $return_job;
            $jobs_obj->get_jobs();
            $activity_data = array('job_title' => $jobs_obj->job_title);

            #Stream
            $add_activity_data = array(
                "job_title" => $activity_data['job_title'],
                "activity_sub_type" => LABEL_STUDIO_CREATED_JOB,
                "job_id" => $return_job,
                "search_keywords" => "create new job newopening opening openings"
            );
            $reviewer_type = 3;
//            $job_activity = array(
//                'performer_id' => $user_id,
//                'job_id' => $return_job,
//                'object_id' => 0,
//                'activity_type' => LABEL_NEW_JOB_LOG,
//                'activity_data' => serialize($add_activity_data),
//                'created_timestamp' => strtotime("now"),
//                'status_sl' => '1'
//            );
//            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

            $job_activity_obj = new Tyr_job_activity_entity();
            $job_activity_obj->performer_id = $user_id;
            $job_activity_obj->job_id = $return_job;
            $job_activity_obj->object_id = 0;
            $job_activity_obj->activity_type = LABEL_NEW_JOB_LOG;
            $job_activity_obj->activity_data = serialize($add_activity_data);
            $job_activity_obj->status_sl = 1;
            $job_activity_obj->created_timestamp = strtotime("now");
            $job_activity_obj->save_job_activity();

            #Email - Studio
            $job_title = $activity_data['job_title'];
            $url = $this->getURL("openings/" . strtolower(str_replace(' ', '-', $activity_data['job_title']) . "-" . $this->generate_job_number($return_job)));

            //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n10'");
            $notification_templates_obj = new Tyr_notification_templates_entity();
            $notification_templates_obj->notification_id = 'n10';
            $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

            $subject = $get_message['subject'];
            $subject = str_replace(array('[$job_title]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $url, LIVE_SITE_URL), $subject);
            $message = htmlspecialchars_decode($get_message['template']);
            $message = str_replace(array('[$job_title]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $url, LIVE_SITE_URL), $message);
            $current_user_email = $this->session->userdata("user_email");
            $this->email_sender($current_user_email, $subject, $message);
            /* Email To Studio and Insert Stream on behalf of Studio */
            
            if(isset($_POST['invite_emails'])){
                $invite_emails = $this->input->post('invite_emails');
                if(count($invite_emails) > 0){
                    $studio_admin_name = $this->session->userdata('firstname') . " " . $this->session->userdata('lastname');
                    $job_name = $this->input->post("job_title");

                    $notification_templates_obj = new Tyr_notification_templates_entity();
                    $notification_templates_obj->notification_id = 'n13';
                    $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                    $subject_temp = $get_message['subject'];
                    $message_temp = htmlspecialchars_decode($get_message['template']);

                    foreach ($invite_emails as $email_id){
                        $users_obj = new Tyr_users_entity();
                        $users_obj->email = $email_id;
                        $users_obj->get_user_by_email();
                        if($users_obj->user_id == 0){
                            
                            $users_obj->status_sl = 0;
                            $users_obj->role_id = 4;
                            $users_obj->created_timestamp = strtotime("now");
                            $users_obj->modified_timestamp = strtotime("now");
                            $users_obj->save_user();
                            $profile_id = $users_obj->user_id;
                            
                            $job_role_mapping = new Tyr_job_role_mapping_entity();
                            $job_role_mapping->job_id = $return_job;
                            $job_role_mapping->user_id = $profile_id;
                            $job_role_mapping->is_admin = 0;
                            $job_role_mapping->is_reviewer = 1;
                            $job_role_mapping->save_job_role_mapping();
                            
                            $url = $this->getURL('register') . "/?" . md5('job_invitation') . "=1&" . md5('invite_email') . "=" . base64_encode($email_id) . "&" . md5('job_id') . "=" . base64_encode($return_job) . "&" . md5('studio_id') . "=" . base64_encode($this->session->userdata('user_id'));
                            $email_subject = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$url_invitereviewer]', '[$url_tyroe]'), array($job_name, $studio_admin_name, $url, LIVE_SITE_URL), $subject_temp);
                            $email_message = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$url_invitereviewer]', '[$url_tyroe]'), array($job_name, $studio_admin_name, $url, LIVE_SITE_URL), $message_temp);
                            $email_success = $this->email_sender($email_id, $email_subject, $email_message, '', '', '', '', '', 'Tyroe');
                        }
                    }
                }
            }
            echo json_encode($return_job);
        }
    }

    public function search_existing_reviewer() {
        $user_id = $this->session->userdata("user_id");
        $searching_value = $this->input->post('searching_value');
        $job_id = $this->input->post('job_id');
        
        $join = '';
        $where = '';
        if ($this->input->post('search_type') == 'existing_job') {
            $job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
            $job_roles_mapping_obj->job_id = $job_id;
            $reviewers = $job_roles_mapping_obj->get_all_reviewers_for_job();
            if (!empty($reviewers)) {
//                $join = 'LEFT JOIN ' . TABLE_REVIEWER_JOBS . ' ON ' . TABLE_USERS . '.user_id = ' . TABLE_REVIEWER_JOBS . '.reviewer_id
//                AND ' . TABLE_REVIEWER_JOBS . '.reviewer_id NOT IN (' . $reviewers['user_id'] . ') AND job_id = ' . $job_id;
                $where = " and ".TABLE_USERS.'.user_id NOT IN('.$reviewers['user_id'].') ';
            }
        }
        
        if ($this->input->post('search_type') == 'new_job_opening') {
            
            if (isset($_POST['reviewers']) && $_POST['reviewers'] != '') {
                //$revs = implode(',',$this->input->post('reviewers'));
                $where = " and ".TABLE_USERS.'.user_id NOT IN('.$this->input->post('reviewers').')';
            }
        }
        
//      $query = 'SELECT '. TABLE_USERS .'.user_id,'. TABLE_USERS .'.firstname,'. TABLE_USERS .'.lastname,'. TABLE_USERS .'.email,'. TABLE_USERS .'.job_title,' . TABLE_MEDIA . '.media_type,' . TABLE_MEDIA . '.media_name
//        FROM '. TABLE_USERS . '
//        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
//        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
//         '.$join.'
//        WHERE '. TABLE_USERS . '.role_id = "4" AND
//        '. TABLE_USERS . '.status_sl = "1" AND
//        '. TABLE_USERS . '.parent_id = "'.$this->session->userdata("user_id").'"
//         AND
//        (
//         CONCAT(' . TABLE_USERS . '.firstname, " ", ' . TABLE_USERS . '.lastname) LIKE "%'.$searching_value.'%" OR
//         '. TABLE_USERS . '.firstname LIKE "%'.$searching_value.'%" OR
//         '. TABLE_USERS . '.lastname LIKE "%'.$searching_value.'%" OR
//         '. TABLE_USERS . '.username LIKE "%'.$searching_value.'%"
//        )'.$where;
      //echo $query;
        //$get_rec = $this->getAll($query);
        
//        $users_obj = new Tyr_users_entity();
//        $users_obj->user_id = $user_id;
//        $users_obj->get_user();
//        if($users_obj->parent_id == 0){
//            $parent_id = $user_id;
//        }else{
//            $parent_id = $users_obj->parent_id;
//        }
        
        $parent_id = intval($this->session->userdata("parent_id"));
        if($parent_id == 0){
            $parent_id = intval($user_id);
        }
        
        $joins_obj = new Tyr_joins_entity();
        $get_rec = $joins_obj->search_existing_reviewers_1($where, $user_id, $parent_id, $searching_value);

        $this->data['get_records'] = $get_rec;
        $this->template_arr = array('contents/existing_reviewers_list');
        $this->load_template();
    }
    
    public function change_job_team_member_role(){
        $job_id = $this->input->post('job_id');
        $reviewer_id = $this->input->post('reviewer_id');
        $user_role = $this->input->post('user_role');
        $change_value = $this->input->post('change_value');
        $job_role_mapping = new Tyr_job_role_mapping_entity();
        $job_role_mapping->user_id = $reviewer_id;
        $job_role_mapping->job_id = $job_id;
        $job_role_mapping->get_job_role_mapping();
        if($job_role_mapping->id != 0){
            if($user_role == 1){
                $job_role_mapping->is_admin = $change_value;
            }else{
                $job_role_mapping->is_reviewer = $change_value;
            }
            $job_role_mapping->update_job_role_mapping();
        }else{
            if($user_role == 1){
                $job_role_mapping->is_admin = $change_value;
            }else{
                $job_role_mapping->is_reviewer = $change_value;
            }
            $job_role_mapping->save_job_role_mapping();
        }
        
        echo json_encode(array('success' => true));
        
    }
    
    public function delete_job_team_member(){
      
        $job_id = $this->input->post('job_id');
        $reviewer_id = $this->input->post('reviewer_id');
        //$rev_id = $this->input->post('rev_id');
       
        $job_role_mapping = new Tyr_job_role_mapping_entity();
        $job_role_mapping->user_id = $reviewer_id;
        $job_role_mapping->job_id = $job_id;
        $job_role_mapping->delete_job_role_mapping();
        
        $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
        $reviewer_jobs_obj->reviewer_id = $reviewer_id;
        $reviewer_jobs_obj->job_id = $job_id;
        $reviewer_jobs_obj->delete_reviewer_by_job_reviewer();
        
        echo json_encode(array('success' => true));
        
    }
    
    public function add_reviewers_from_job() {
        /* Studio can add Reviewer in job as Team Member */
        $user_id = $this->session->userdata("user_id");
        $job_id = $this->input->post('job_id');
        $reviewer_id = $this->input->post('reviewer_id');
        if(isset($_REQUEST['add_reviewer']) &&  $_REQUEST['add_reviewer'] == 'new_job_opening_reviewer'){
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $reviewer_id;
            $users_obj->get_user();
            $reviewer_data = (array)$users_obj;
            ?>
            <tr class="table_rows job-team-tr" value="<?= $reviewer_data['user_id'] ?>">
                <td><p class="rt-name"><?= stripslashes($reviewer_data['firstname']) . ' ' . stripslashes($reviewer_data['lastname']) ?></p>
                <span class="rt-title"><?= stripslashes($reviewer_data['job_title']); ?></span></td>
                <td class="role-check-td" style="text-align: center"></td>
                <td class="role-check-td" style="text-align: center"><a class="reviewer_check_option active" value="2"><i class="fa fa-check-circle"></i></a></td>
                <td style="text-align: right;padding-right: 13px"><a href="mailto:<?= $reviewer_data['email'] ?>"><?= $reviewer_data['email'] ?></a></td>
                <td class="status_td">
                    <a href="javascript:void(0)" class="reviewer_status_sl"><label class="label label-success" style="text-align: center">Active</label></a>
                </td>
                <td>
                    <a href="javascript:void(0)" class="delet-job-reviewer" id="delet_job_reviewer_<?=$reviewer_data['job_id']?>_<?=$reviewer_data['user_id']?>_0"><i class="fa fa-times"></i></a>
                </td>
            </tr>
            <?php
        }else{
            
            $job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
            $job_roles_mapping_obj->job_id = $job_id;
            $job_roles_mapping_obj->user_id = $user_id;
            $job_roles_mapping_obj->get_job_role_mapping();

            $parent_id = $user_id;
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            if($users_obj->parent_id != 0){
                $parent_id = $users_obj->parent_id;
            }

            //$getreviewer = $this->getRow("SELECT * FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $reviewer_id . "' AND job_id ='".$job_id."'");
            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
            $reviewer_jobs_obj->reviewer_id = $reviewer_id;
            $reviewer_jobs_obj->job_id = $job_id;
            $getreviewer = $reviewer_jobs_obj->get_row_by_reviewer_job();

            if ($getreviewer == '') {
        //            $reviewr_array = array(
        //                'reviewer_id' => $reviewer_id,
        //                'studio_id' => $user_id,
        //                'job_id' => $job_id,
        //                'created_timestamp' => time(),
        //                'status_sl' => '1'
        //            );
        //            //$this->insert(TABLE_REVIEWER_JOBS, $reviewr_array);
        //            
        //            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
        //            $reviewer_jobs_obj->reviewer_id = $reviewer_id;
        //            $reviewer_jobs_obj->job_id = $job_id;
        //            $reviewer_jobs_obj->studio_id = $parent_id;
        //            $reviewer_jobs_obj->status_sl = 1;
        //            $reviewer_jobs_obj->created_timestamp = strtotime("now");
        //            $reviewer_jobs_obj->save_reviewer_jobs();
        //            
        //            $rev_id = $reviewer_jobs_obj->rev_id;

                    $job_role_mapping = new Tyr_job_role_mapping_entity();
                    $job_role_mapping->job_id = $job_id;
                    $job_role_mapping->user_id = $reviewer_id;
                    $job_role_mapping->is_admin = 0;
                    $job_role_mapping->is_reviewer = 1;
                    $job_role_mapping->save_job_role_mapping();

                    #JOB ACTIVITY START
                    //$basic_details = $this->getRow("SELECT job_title, user_id FROM ".TABLE_JOBS." WHERE job_id = '".$job_id."'");
                    $jobs_obj = new Tyr_jobs_entity();
                    $jobs_obj->job_id = $job_id;
                    $jobs_obj->get_jobs();
                    $basic_details = (array) $jobs_obj;

        //            $studio_admin_detail = $this->getRow("SELECT u.firstname, u.lastname, c.company_name
        //                                                    FROM ".TABLE_USERS." u
        //                                                    LEFT JOIN ".TABLE_COMPANY." c ON u.user_id = c.studio_id
        //                                                    WHERE studio_id = '".$basic_details['user_id']."'");
                    $activity_type = LABEL_NEW_JOB_LOG;

                    //$studio_name = $studio_admin_detail['firstname'] . " " . $studio_admin_detail['lastname'];
                    $joins_obj = new Tyr_joins_entity();
                    $studio_admin_detail = $joins_obj->get_studio_admin_details($parent_id);
                    $studio_name = $studio_admin_detail['firstname'] . " " . $studio_admin_detail['lastname'];

                    $activity_data = array(
                        "job_opening_title" => $basic_details['job_title'],
                        "studio_admin_name" => $studio_name,
                        "studio_admin_company_name" => $studio_admin_detail['company_name'],
                        "activity_sub_type" => LABEL_ADD_REVIEWER_TO_JOB_LOG,
                        "search_keywords" => "reviewer add job"
                    );

        //            $job_activity = array(
        //                'performer_id' => $reviewer_id,
        //                'job_id' => $job_id,
        //                'object_id' => $reviewer_id,
        //                'activity_type' => $activity_type,
        //                'activity_data' => serialize($activity_data),
        //                'created_timestamp' => strtotime("now"),
        //                'status_sl' => '1'
        //            );
        //            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $reviewer_id;
                    $job_activity_obj->job_id = $job_id;
                    $job_activity_obj->object_id = $reviewer_id;
                    $job_activity_obj->activity_type = $activity_type;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();

                    #JOB ACTIVITY END
                    #Email
                    //$rev_email_data = $this->getRow("SELECT email FROM " . TABLE_USERS . " WHERE user_id='" . $reviewer_id . "'");

                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $reviewer_id;
                    $users_obj->get_user();
                    $rev_email_data = (array)$users_obj;

                    $company_name = $studio_admin_detail['company_name'];
                    $job_title = $basic_details['job_title'];
                    $url = $this->getURL("openings/" . strtolower(str_replace(' ', '-', $activity_data['job_opening_title']) . "-" . $this->generate_job_number($return_job)));

                    //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n11'");
                    $notification_templates_obj = new Tyr_notification_templates_entity();
                    $notification_templates_obj->notification_id = 'n11';
                    $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                    $subject = $get_message['subject'];
                    $subject = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$studio_admin_company_name]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $studio_name, $company_name, $url, LIVE_SITE_URL), $subject);
                    $message = htmlspecialchars_decode($get_message['template']);
                    $message = str_replace(array('[$job_title]', '[$studio_admin_name]', '[$studio_admin_company_name]', '[$url_openingdashboard]', '[$url_tyroe]'), array($job_title, $studio_name, $company_name, $url, LIVE_SITE_URL), $message);
                    $reviewer_email = $rev_email_data['email'];
                    $this->email_sender($reviewer_email, $subject, $message);

                    //getting added reviewer
        //            $query = 'SELECT '. TABLE_USERS .'.user_id,'. TABLE_USERS .'.firstname,'. TABLE_USERS .'.lastname,'. TABLE_USERS .'.email,'. TABLE_USERS .'.user_occupation,' . TABLE_MEDIA . '.media_type,' . TABLE_MEDIA . '.media_name
        //                        FROM '. TABLE_USERS . '
        //                        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
        //                        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
        //                        LEFT JOIN '.TABLE_REVIEWER_JOBS.' ON '. TABLE_USERS .'.user_id = '.TABLE_REVIEWER_JOBS.'.reviewer_id
        //                         AND '.TABLE_REVIEWER_JOBS.'.job_id = '.$job_id.'
        //                        WHERE '. TABLE_USERS . '.role_id = "4" AND
        //                        '. TABLE_USERS . '.status_sl = "1" AND
        //                        '. TABLE_USERS . '.parent_id = "'.$this->session->userdata("user_id").'" AND '. TABLE_USERS . '.user_id = "'.$reviewer_id.'" ';
        //            $get_record = $this->getRow($query);

                    $parent_id = $user_id;
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $user_id;
                    $users_obj->get_user();
                    if($users_obj->parent_id != 0){
                        $parent_id = $users_obj->parent_id;
                    }

                    $joins_obj = new Tyr_joins_entity();
                    $get_record = $joins_obj->get_added_reviewer_details($parent_id, $job_id, $reviewer_id);

                    $job_role_mapping = new Tyr_job_role_mapping_entity();
                    $job_role_mapping->job_id = $job_id;
                    $job_role_mapping->user_id = $reviewer_id;
                    $job_role_mapping->get_job_role_mapping();
                    if($job_role_mapping->id > 0){
                        $get_record['is_admin'] = $job_role_mapping->is_admin;
                        $get_record['is_reviewer'] = $job_role_mapping->is_reviewer;
                        $get_record['job_role_id'] = $job_role_mapping->id;
                    }else{
                        $get_record['is_admin'] = 0;
                        $get_record['is_reviewer'] = 0;
                        $get_record['job_role_id'] = 0;
                    }

                    $get_record['job_id'] = $job_id;
                    //$get_record['rev_id'] = $rev_id;

                    $image = ASSETS_PATH_NO_IMAGE;
                    if ($get_record['media_name'] != "") {
                        $image = ASSETS_PATH_PROFILE_THUMB . $get_record['media_name'];
                    }
                    ?>
                    <tr class="table_rows row_reviewer_no<?= $get_record['user_id'] ?> job-team-tr">
                        <td>
                            <p class="rt-name"><?= stripslashes($get_record['firstname']) . ' ' . stripslashes($get_record['lastname']) ?></p>
                            <span class="rt-title"><?= stripslashes($get_record['job_title']); ?></span>
                        </td>
                        <?php
                            $is_admin = '';
                            $is_reviewer = '';
                            if($get_record['is_admin'] == 1){
                                $is_admin = 'active';
                            }

                            if($get_record['is_reviewer'] == 1){
                                $is_reviewer = 'active';
                            }

                            $hide_inactive_tick = false;
                            if(intval($job_roles_mapping_obj->is_admin) == 0 && intval($job_roles_mapping_obj->is_reviewer) == 1){
                                $hide_inactive_tick = true;

                            }
                        ?>

                        <td class="role-check-td" style="text-align: center" id="role_check_td_<?=$get_record['job_id']?>_<?=$get_record['user_id']?>"><a class="reviewer_check_option <?=$is_admin?>" value="1" <?php 
                            if($hide_inactive_tick == true && $is_admin == ''){
                                echo 'style="display:none"';
                            }
                        ?>><i class="fa fa-check-circle"></i></a></td>

                        <td class="role-check-td" style="text-align: center" id="role_check_td_<?=$get_record['job_id']?>_<?=$get_record['user_id']?>"><a class="reviewer_check_option <?=$is_reviewer?>" value="2" <?php 
                            if($hide_inactive_tick == true && $is_reviewer == ''){
                                echo 'style="display:none"';
                            }
                        ?>><i class="fa fa-check-circle"></i></a></td>


                        <td style="text-align: right;padding-right: 8px;"><a href="mailto:<?= $get_record['email'] ?>"><?= $get_record['email'] ?></a></td>
                        <td class="status_td">
                            <?php if ($get_record['status_sl'] == 1) { ?>
                                <a href="javascript:void(0)" class="reviewer_status_sl"><label class="label label-success" style="text-align: center">Active</label></a>
                            <?php } else if ($get_record['status_sl'] == 0) { ?>
                                <a href="javascript:void(0)"><label class="label label-info rt-pending" style="text-align: center">Pending</label></a>
                            <?php } ?>
                        </td>
                        <td><a href="javascript:void(0)" class="delet-job-reviewer" id="delet_job_reviewer_<?=$get_record['job_id']?>_<?= $reviewer_id ?>_0"><i class="fa fa-times"></i></a></td>
                    </tr>
                    <?php
                }

            }
    }
    
    public function load_job_team_members(){
        
        $user_id = $this->session->userdata("user_id");
        $job_id = $this->input->post('job_id');
        
        $joins = new Tyr_joins_entity();
        $get_jobs = $joins->get_job_details_by_job_id($job_id);
        
        $joins_obj = new Tyr_joins_entity();
        $get_reviewers = $joins_obj->get_all_reviewers_for_studio_job($job_id, $user_id);
        
        if($get_reviewers != ''){
            foreach($get_reviewers as $index => $val){
                $reviewer_id = $val['user_id'];
                $job_role_mapping = new Tyr_job_role_mapping_entity();
                $job_role_mapping->job_id = $job_id;
                $job_role_mapping->user_id = $reviewer_id;
                $job_role_mapping->get_job_role_mapping();
                if($job_role_mapping->id > 0){
                    $get_reviewers[$index]['is_admin'] = $job_role_mapping->is_admin;
                    $get_reviewers[$index]['is_reviewer'] = $job_role_mapping->is_reviewer;
                    $get_reviewers[$index]['job_role_id'] = $job_role_mapping->id;
                }else{
                    $get_reviewers[$index]['is_admin'] = 0;
                    $get_reviewers[$index]['is_reviewer'] = 0;
                    $get_reviewers[$index]['job_role_id'] = 0;
                }
                
            }
        }
        
        $this->data['get_reviewers'] = $get_reviewers;
        $this->data['get_jobs'] = $get_jobs;
        
        $this->template_arr = array('contents/job_team_members');
        $this->load_template();
    }
    
    public function create_job(){
       
        $user_id = $this->session->userdata("user_id");
        $parent_id = $this->session->userdata("parent_id");
        if ($parent_id == 0) {
            $parent_id = $user_id;
        }
        
        $users_details_obj = new Tyr_users_entity();
        $users_details_obj->user_id  = $user_id;
        $users_details_obj->get_user();
        
        $this->data['user_detail'] = (array)$users_details_obj;
        
        $company_detail_studio_obj = new Tyr_company_detail_entity();
        $company_detail_studio_obj->studio_id = $parent_id;
        $company_detail_studio_obj->get_company_detail_by_studio();
        
        if($company_detail_studio_obj->parent_id != 0){
            $company_detail_obj = new Tyr_company_detail_entity();
            $company_detail_obj->company_id = $company_detail_studio_obj->parent_id;
            $company_detail_obj->get_company_detail();
            $this->data['company_detail'] = (array)$company_detail_obj;
        }else{
            $this->data['company_detail'] = (array)$company_detail_studio_obj;
        }
        
        $this->data['create_job'] = 1;
        
        $this->_add_css('create_job.css');
        $this->_add_css('createjobcustom.css');
        $this->template_arr = array('header','contents/create_job','footer');
        $this->load_template();
    }
    
    public function check_email_present(){
        $emails = $this->input->post('emails');
        $i = 0;
        $j = 0;
        $err_emails = array();
        $invite_emails = array();
        foreach($emails as $email){
            $users_obj = new Tyr_users_entity();
            $users_obj->email = $email;
            $users_obj->get_user_by_email();
            if($users_obj->user_id > 0){
                 $err_emails[$i] = $email;
                 $i++;
            }else{
                 $invite_emails[$i] = $email;
                 $j++;
            }
        }
        echo json_encode(array('success' => true, 'err_emails' => $err_emails,'invite_emails' => $invite_emails));
    }
    
}
