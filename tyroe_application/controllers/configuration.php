<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_system_variables_entity' . EXT);
require_once(APPPATH . 'entities/tyr_system_variables_entity' . EXT);
class Configuration extends Tyroe_Controller
{
    public function Configuration()
    {
        parent::__construct();
        
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_CONFIGURATION . ') ');
        }
    }

    public function index()
    {

    }

    public function system_variables()
    {
        $this->data['page_title'] = 'System Configuration';
        $this->data['msg'] = "";
        
        $system_variables_obj = new Tyr_system_variables_entity();
        $system_variables_obj->status_sl = 1;
        $get_variable = $system_variables_obj->get_all_system_variables_by_status();
        
        $this->data['get_variable'] = $get_variable;
        $submit = $this->input->post("submit");
        if ($submit != "") {

            foreach ($get_variable as $variable) {

                if($this->input->post($variable['variable_name']) != $variable['variable_value']){
                    $system_variables_obj->variable_value = $this->input->post($variable['variable_name']);
                    $system_variables_obj->variable_name = $variable['variable_name'];
                    $return = $system_variables_obj->update_variable_value_by_name();
                }
            }
            if($return){
                $this->data['msg']="System Variables Updated";
                $this->session->set_userdata("status_msg",MESSAGE_UPDATE_SUCCESS);
                $submit="1";
            }
        }
        $this->template_arr = array('header', 'contents/user/system_variable', 'footer');
        $this->load_template();
    }

    public function notification_templates()
    {
        $this->data['page_title'] = 'Notification Templates';
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->status_sl = 1;
        $get_notifications = $notification_templates_obj->get_all_notification_templates();
        
        foreach ($get_notifications as $k => $notifications) {
            $all_notifications[] = array(
                "title" => stripslashes($notifications['title']),
                "template_id" => stripslashes($notifications['template_id']),
                "description" =>stripslashes($notifications['description']),
                "template" => stripslashes($notifications['template']),
                "subject" =>stripslashes($notifications['subject']),
                "social_template" => stripslashes($notifications['social_template']),
                "notification_id" => $notifications['notification_id']
            );
        }
        $this->data['get_notifications'] = $all_notifications;
        $this->template_arr = array('header', 'contents/user/get_notifications', 'footer');
        $this->load_template();
    }

    public function manage_notification()
    {
        $notification_id = $this->input->post('notification_id');
        switch($notification_id)
        {
            case  '1':
                $this->data['call'] = 'applyforjob';
                break;
            case '2' :
                $this->data['call'] = 'invitedforjob';
                break;
            case '3' :
                $this->data['call'] = 'shortlistforjob';
                break;
            case '4' :
                $this->data['call'] = 'gotreview';
                break;
            case '5' :
                $this->data['call'] = 'profileviewed';
                break;
            case '6' :
                $this->data['call'] = 'gotfavourited';
                break;
            case '7' :
                $this->data['call'] = 'receiverecommend';
                break;
            case '8' :
                $this->data['call'] = 'acceptinvite';
                break;
            case '9' :
                $this->data['call'] = 'rejectinvite';
                break;
            case '10' :
                $this->data['call'] = 'jobcreated';
                break;
            case '11' :
                $this->data['call'] = 'revieweradded';
                break;
            case '12' :
                $this->data['call'] = 'studioshortlist';
                break;
            case '13' :
                $this->data['call'] = 'studioinvitetyroe';
                break;
        }
        $this->data['notification_id'] = $notification_id;
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->template_id = $notification_id;
        $notification_templates_obj->get_notification_templates();
        $get_notifications = (array)$notification_templates_obj;
      
        $this->data['selected_notification'] = $get_notifications;
        $this->template_arr = array('contents/user/notification_form');
        $this->load_template();
    }

    public function SaveNotification()
    {
        $template = $this->input->post("template");
        $search = '../assets/';
        $replace = ASSETS_PATH;
        $template = str_replace($search,$replace,$template);
        $template_id = $this->input->post("edit_id");
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->template_id = $template_id;
        $notification_templates_obj->get_notification_templates();
        $notification_templates_obj->title = $this->input->post("title");
        $notification_templates_obj->description = $this->input->post("description");
        $notification_templates_obj->subject = $this->input->post("subject");
        $notification_templates_obj->template = htmlspecialchars($template);
        $return = $notification_templates_obj->update_notification_templates();
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_UPDATE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }
}
