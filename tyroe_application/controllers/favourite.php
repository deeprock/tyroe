<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);
//require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
//require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
//require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
//require_once(APPPATH . 'entities/tyr_media_entity' . EXT);





class Favourite extends Tyroe_Controller
{
    public function Favourite()
    {
        parent::__construct();
        $this->data['page_title'] = 'Favourite';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');

        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_FAVORITES . ') ');

        }
    }

    public function index()
    {

        $this->data['page_number'] = $this->input->post('page');
        $user_id = $this->session->userdata("user_id");
        
        
        /* Profile Access Check */
        $parent_id = $this->session->userdata("user_id");
        if(intval($this->session->userdata("parent_id")) != 0){
            $parent_id = $this->session->userdata("parent_id");
        }
        
        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $this->data['has_profile_access'] = $profile_access_obj->id == 0 ? 0 : 1;
        
        /* Profile Access Check */
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $this->data['get_user'] = (array)$users_obj;
        
        $hide_user_obj = new Tyr_hide_user_entity();
        $hide_user_obj->studio_id = $user_id;
        $hiden_tyroes = $hide_user_obj->get_hidden_tyroes_by_studio_id_1();
        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['tyroe_id'];
        }

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_favourite_profile($user_id,$hidden_user);
        
        $pagintaion_limit = $this->pagination_limit();
        //$this->data['pagintaion_limit'] = $pagintaion_limit;
        /*$per_page = 5;
        $pages = ceil($total_rows['cnt'] / $per_page);
        $this->data['pages'] = $pages;*/

        //forsingle pagination
        $joins_obj = new Tyr_joins_entity();
        $get_rows_for_pagination = $joins_obj->get_all_favourite_profile_paginated($user_id,$hidden_user);
        $this->data['get_rows_for_pagination'] = $get_rows_for_pagination;
        //end for single page pagination


        //6th of Aug, 2014 Update -- Add Distinct in Query
        $query = "SELECT DISTINCT(" . TABLE_USERS . ".user_id),role_id,parent_id," . TABLE_USERS . ".country_id,".TABLE_USERS.".city,email,
              username,firstname,lastname,address,availability,extra_title,
              state,zip," . TABLE_COUNTRIES . ".country,
              " . TABLE_INDUSTRY . ".industry_name,
              " . TABLE_CITIES . ".city, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
              LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
              RIGHT JOIN " .TABLE_FAVOURITE_PROFILE. " ON " .TABLE_FAVOURITE_PROFILE. ".user_id = " . TABLE_USERS . ".user_id
              AND " .TABLE_FAVOURITE_PROFILE. ".studio_id =  ".$user_id." AND " .TABLE_FAVOURITE_PROFILE. ".status_sl = 1
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image= 1 AND " . TABLE_MEDIA . ".status_sl= 1
              WHERE " . TABLE_USERS . ".role_id= 2 AND " . TABLE_USERS . ".status_sl= 1 AND ".TABLE_USERS.".publish_account = 1 AND " .TABLE_USERS. ".user_id NOT IN (" . $hidden_user . ") ORDER BY " .TABLE_USERS. ".user_id DESC " . $pagintaion_limit['limit'];
        $joins_obj = new Tyr_joins_entity();
        $get_tyroes = $joins_obj->get_all_tyroe_details_with_limit($query);
        
        
//        $get_tyroes = $this->getAll
//            ("SELECT DISTINCT(" . TABLE_USERS . ".user_id),role_id,parent_id," . TABLE_USERS . ".country_id,email,
//              username,firstname,lastname,address,availability,extra_title,
//              state,zip," . TABLE_COUNTRIES . ".country,
//              " . TABLE_INDUSTRY . ".industry_name,
//              " . TABLE_CITIES . ".city, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
//              LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
//              RIGHT JOIN " .TABLE_FAVOURITE_PROFILE. " ON " .TABLE_FAVOURITE_PROFILE. ".user_id = " . TABLE_USERS . ".user_id
//              AND " .TABLE_FAVOURITE_PROFILE. ".studio_id = '".$user_id."' AND " .TABLE_FAVOURITE_PROFILE. ".status_sl = 1
//              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND ".TABLE_USERS.".publish_account = '1' AND " .TABLE_USERS. ".user_id NOT IN (" . $hidden_user . ") ORDER BY " .TABLE_FAVOURITE_PROFILE. ".user_id DESC " . $pagintaion_limit['limit']);

        // foreach ($get_tyroes as $tyroes) {
        if($get_tyroes != ''){
            for ($i = 0; $i < count($get_tyroes); $i++) {

                /*Get City Name*/
//                $cities = new Tyr_cities_entity();
//                var_dump($get_tyroes[$i]['city']);
//                $cities->city_id = $get_tyroes[$i]['city'];
//                $getCity = $cities->get_cities();
//                $get_tyroes[$i]['city'] = $cities->city;
//                $getCity = $this->getRow("SELECT city FROM ".TABLE_CITIES." WHERE country_id = '".$get_tyroes[$i]['country_id']."'");
//                $get_tyroes[$i]['city'] = $getCity['city'];
                
                $joins_obj = new Tyr_joins_entity();    
                $get_tyroes[$i]['skills'] = $joins_obj->get_all_user_skill_with_limit($get_tyroes[$i]['user_id']);
                

//                $get_tyroes[$i]['skills'] = $this->getAll("SELECT skill_id, creative_skill
//                         FROM " . TABLE_SKILLS . " LEFT JOIN
//                         " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".`skill`
//                         WHERE user_id='" . $get_tyroes[$i]['user_id'] . "' LIMIT 3,2");

                #Portfolio/Media
                //$get_tyroes[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_tyroes[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $get_tyroes[$i]['user_id'];
                $get_tyroes[$i]['portfolio'] = $media_obj->get_user_short_portfolio();
                
                
                
                /*FEATURED TYROE - TRIANGLE*/
//                $is_user_featured = $this->getRow("SELECT featured_status FROM tyr_featured_tyroes WHERE featured_tyroe_id = '".$get_tyroes[$i]['user_id']."'");
//                $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                
                $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                $featured_tyroes_obj->featured_tyroe_id = $get_tyroes[$i]['user_id'];
                $featured_tyroes_obj->get_user_featured_status();
                $is_user_featured = array('featured_status' => $featured_tyroes_obj->featured_status);
                $get_tyroes[$i]['is_user_featured'] = $is_user_featured['featured_status'];
                
                /*FEATURED TYROE - TRIANGLE*/
                /*Check is favourite or not*/
//                $is_favourite = $this->getRow("SELECT fav_profile_id FROM ".TABLE_FAVOURITE_PROFILE." WHERE user_id = '".$get_tyroes[$i]['user_id']."' AND studio_id = '".$user_id."' AND status_sl = 1");
//                $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                
                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $get_tyroes[$i]['user_id'];
                $favourite_profile_obj->studio_id = $user_id;
                $favourite_profile_obj->status_sl = 1;
                $is_favourite = $favourite_profile_obj->get_favourite_profile_id();
                $get_tyroes[$i]['is_favourite'] = $is_favourite['fav_profile_id'];
                
                /*End Checking favourite*/
                /**Getting tyroe jobs**/
//                $tyroe_jobs = $this->getAll("SELECt job_id FROM ".TABLE_JOB_DETAIL." WHERE tyroe_id = '".$get_tyroes[$i]['user_id']."'");
//                $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;
                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                $get_tyroes[$i]['tyroe_jobs'] = $tyroe_jobs;
                
                /**End getting tyroe jobs**/
                /**get tyroe total score**/
//                $get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_tyroes[$i]['user_id']."'");
//                if($get_tyroes_total_score['tyroe_score'] == NULL){
//                    $get_tyroes_total_score['tyroe_score'] = 0;
//                }
//                $get_tyroes[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->tyroe_id = $get_tyroes[$i]['user_id'];
                $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();
                if($get_tyroes_total_score['tyroe_score'] == NULL){
                    $get_tyroes_total_score['tyroe_score'] = 0;
                }
                $get_tyroes[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                /**end get tyroe total score**/
            }
        }

        if ($this->session->userdata("role_id") == '4') {
            
            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
            $reviewer_jobs_obj->reviewer_id = $user_id;
            $get_jobs = $reviewer_jobs_obj->get_job_ids_by_reviewer_id();
            //$get_jobs = $this->getRow("SELECT GROUP_CONCAT(job_id ORDER BY job_id ASC SEPARATOR ',')AS job_id,reviewer_id FROM " . TABLE_REVIEWER_JOBS . " WHERE reviewer_id='" . $user_id . "' GROUP BY reviewer_id");
            if($get_jobs['job_id'] == ''){
                $get_jobs['job_id'] = -1;
            }
            $where = " and job_id IN(".$get_jobs['job_id'].")";
            //$where = " and user_id='" . $this->session->userdata("parent_id") . "'";
        } else if ($this->session->userdata("role_id") == '3') {
            $where = " and user_id='" . $user_id . "'";
        } else {
            $where = "";
        }
        
        $joins_obj = new Tyr_joins_entity();
        $jobs = $joins_obj->get_all_jobs_details_1($where);
        
//        $jobs = $this->getAll("SELECT job_id,user_id,category_id,job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
//                         " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date,
//                           end_date,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl  FROM " . TABLE_JOBS . "
//                 LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                 LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//                           where " . TABLE_JOBS . ".status_sl='1'
//                           and archived_status='0'" . $where);

        $this->data['get_countries_dropdown'] = $this->dropdown($this->data['get_countries'], $this->data['get_user']["country_id"], array('name' => 'country', 'placeholder' => 'Country', 'onchange' => 'getCities(this)'));
        
        
        
        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $this->data['get_user']["country_id"] . "'");
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $this->data['get_user']["country_id"];
        $cities = $cities_obj->get_all_cities_by_country_id();
        $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City'));
        
        //$get_industry = $this->getAll("SELECT industry_id AS id,industry_name AS name FROM " . TABLE_INDUSTRY);
        $industry_obj = new Tyr_industry_entity();
        $get_industry = $industry_obj->get_all_industries();
        $this->data['get_industry'] = $get_industry;
        
        
//        $get_levels = $this->getAll("SELECT job_level_id  AS id,level_title AS name FROM " . TABLE_JOB_LEVEL);
//        $this->data['get_levels'] = $get_levels;
        
        $job_level_obj = new Tyr_job_level_entity();
        $get_levels = $job_level_obj->get_all_job_level();
        $this->data['get_levels'] = $get_levels;

        $experience_years_obj = new Tyr_experience_years_entity();
        $get_experience = $experience_years_obj->get_all_experience_years();
        $this->data['get_experience'] = $get_experience;
        
//        $get_experience = $this->getAll("SELECT experienceyear_id  AS id,experienceyear AS expyear FROM " . TABLE_EXPERIENCE_YEARS);
//        $this->data['get_experience'] = $get_experience;

        $this->data['get_jobs'] = $jobs;
        $this->data['get_tyroes'] = $get_tyroes;
        // $this->data['get_portfolio'] = $portfolio;
        // $this->data['get_skills'] = $skills;


        if($this->session->userdata("role_id") == 3){
            //$this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '".$user_id."' and status_sl = 1 AND archived_status = 0");
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->status_sl = 1;
            $jobs_obj->archived_status = 0;
            $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
            
        }else if($this->session->userdata("role_id") == 4){
            //$this->data['get_openings_dropdown'] = $this->getAll("SELECT ".TABLE_JOBS.".job_id, ".TABLE_JOBS.".job_title FROM ".TABLE_REVIEWER_JOBS." LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".`job_id` = ".TABLE_REVIEWER_JOBS.".`job_id` WHERE ".TABLE_REVIEWER_JOBS.".`reviewer_id` = '".$this->session->userdata('user_id')."' AND ".TABLE_REVIEWER_JOBS.".studio_id = '".$this->session->userdata('parent_id')."'");
            $studio_id = $this->session->userdata('parent_id');
            $reviewer_id = $this->session->userdata('user_id');
            $joins_obj = new Tyr_joins_entity();
            $this->data['get_openings_dropdown'] = $joins_obj->get_jobs_openings_listing($studio_id, $reviewer_id);
        }
        /*Add to opening dropdown*/

        /*PAGINATION*/
        $this->data['pagination'] = $this->pagination(array('url' => 'search/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->data['ha']=$total_rows['cnt'];
        /* $this->template_arr = array('contents/search/search_tyr');
         $this->load_template();*/

        $this->template_arr = array('header', 'contents/favourite', 'footer');
        $this->load_template();
    }

}