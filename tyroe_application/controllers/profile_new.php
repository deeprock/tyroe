<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_refrences_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendation_entity' . EXT);
require_once(APPPATH . 'entities/tyr_professional_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_other_site_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_sites_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_social_link_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendemail_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_rel_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);

class Profile_new extends Tyroe_Controller
{
    public function Profile_new()
    {

        parent::__construct();

        $this->data['page_title'] = 'Public Profile';
        if($this->data['access_allowed']){
            //$user_id = '127';
        }else{
            if ($this->session->userdata('role_id') != ADMIN_ROLE) {
                //$this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
                $this->get_system_modules(' AND module_id IN (' . PROFILE_SUB_MENU . ') ');
            } else {
                $this->get_system_modules();
            }
            $this->load->library('form_validation');
            $user_id = $this->session->userdata("user_id");
        }


#Profile Details
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_detail_info($user_id);
        
//        $get_user = $this->getRow("SELECT job_level.level_title,exp_years.experienceyear,industry.industry_name,users.user_id,users.city AS city_id,users.job_level_id,users.extra_title,
//                users.industry_id,users.experienceyear_id,role_id,tyroe_level,parent_id,users.country_id,country.country,profile_url,email,
//                 username,password,password_len,firstname,lastname,phone,fax,cellphone,address,cities.city,user_occupation,user_biography,user_web,user_experience,
//                 state,zip,suburb,display_name,created_timestamp,modified_timestamp, users.status_sl,
//                  country.country, media.media_type,media.media_name,
//                    media.image_id FROM " . TABLE_USERS . " users
//               LEFT JOIN " . TABLE_COUNTRIES . " country ON users.country_id=country.country_id
//               LEFT JOIN " . TABLE_MEDIA . " media ON users.user_id=media.user_id AND media.profile_image = '1' AND media.status_sl='1'
//               LEFT JOIN " . TABLE_CITIES . " cities ON users.city=cities.city_id
//               LEFT JOIN " . TABLE_JOB_LEVEL . " job_level ON users.job_level_id=job_level.job_level_id
//               LEFT JOIN " . TABLE_INDUSTRY . " industry ON users.industry_id=industry.industry_id
//               LEFT JOIN " . TABLE_EXPERIENCE_YEARS . " exp_years ON users.experienceyear_id=exp_years.experienceyear_id
//               WHERE users.user_id='" . $user_id . "'  ORDER BY image_id DESC LIMIT 0 ,1");

//        $get_states = '';
//        if ($get_user['country_id'] != "") {
//            $where_country_c = "AND country_id='" . $get_user['country_id'] . "'";
//            $states_obj = new Tyr_states_entity();
//            $states_obj->country_id = $get_user['country_id'];
//            $get_states = $states_obj->get_all_states_by_country();
//            
//        }
//        //$get_states = $this->getAll("SELECT states_id,states_name FROM " . TABLE_STATES . " Where 1=1 " . $where_country_c . " ORDER BY states_name ASC");
//        $this->data['get_states'] = $get_states;
//
//        $tyroe_levels = $this->getAll("SELECT tyroe_level_id,tyroe_level_title FROM " . TABLE_PROFESSIONAL_LEVEL . "");
//        $this->data['tyroe_levels'] = $tyroe_levels;
//        $other_site = $this->getAll("SELECT other_site_id,other_site FROM " . TABLE_OTHER_SITES . "");
//        $this->data['other_site'] = $other_site;
        $this->data['get_states'] = array();
        if ($get_user['country_id'] != "") {
            $states_obj = new Tyr_states_entity();
            $states_obj->country_id = $get_user['country_id'];
            $get_states = $states_obj->get_all_states_by_country();
            $this->data['get_states'] = $get_states;
        }
        
        $professional_level_obj = new Tyr_professional_level_entity();
        $tyroe_levels = $professional_level_obj->get_all_professional_level();
        $this->data['tyroe_levels'] = $tyroe_levels;
        
        $other_site_obj = new Tyr_other_site_entity();
        $other_site = $other_site_obj->get_all_other_site();
        $this->data['other_site'] = $other_site;

        $image;
        if ($get_user['media_name']) {
            $image = ASSETS_PATH_PROFILE_THUMB . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH_NO_IMAGE;
        }


        /*  $new_pass = "";
          for ($i = 1; $i <= $get_user['password_len']; $i++) {
              $new_pass .= " ";
          }*/
        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "profile_url" => $get_user['profile_url'],
            "user_biography" => $get_user['user_biography'],
            "user_occupation" => $get_user['user_occupation'],
            "user_web" => $get_user['user_web'],
            "user_experience" => $get_user['user_experience'],
            "image" => $image,
            "image_id" => $get_user['image_id'],
            "password_len" => $get_user['password_len'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id'],
            "tyroe_level" => $get_user['tyroe_level'],
            "city_id" => $get_user['city_id'],
            "level_title" => $get_user['level_title'],
            "experienceyear" => $get_user['experienceyear'],
            "industry_name" => $get_user['industry_name'],
            "job_level_id" => $get_user['job_level_id'],
            "extra_title" => $get_user['extra_title'],
            "industry_id" => $get_user['industry_id'],
            "experienceyear_id" => $get_user['experienceyear_id']
        );



        $this->data['get_user'] = $user_data;


        /*mysite data*/
        //$mystite = $this->getAll("SELECT * FROM " . TABLE_USER_SITES . " WHERE user_id = '{$user_id}'");
        //$this->data['get_my_sites'] = $mystite;
        $user_sites_obj = new Tyr_user_sites_entity();
        $user_sites_obj->user_id = $user_id;
        $mystite = $user_sites_obj->get_all_user_sites();
        $this->data['get_my_sites'] = $mystite;
    }
    public function getcities(){
        $country_id=$this->input->post('country_id');
        //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM ".TABLE_CITIES." WHERE country_id='".$country_id."'");
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_all_cities_by_country_id();
        
        $cities = $this->dropdown($cities,'',array('name'=>'city','placeholder'=>'City'));
        echo json_encode(array('dropdown'=>$cities));
        exit;

    }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
        
        //$login_history = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_LOGIN_HISTORY." WHERE user_id='{$user_id}'");
        $login_history_obj = new Tyr_login_history_entity();
        $login_history_obj->user_id = $user_id;
        $login_history = $login_history_obj->get_count_login_history_by_user_id();
        
        $this->data['login_history'] = $login_history;
        
        $this->data['page_title'] = 'Edit Profile';
        $this->data['msg'] = "";
        if ($this->session->userdata('update_profile') == 1) {
            $this->data['profile_updated'] = MESSAGE_PROFILE_UPDATED;
            $this->session->unset_userdata("update_profile");

        }
        
        $contries_obj = new Tyr_countries_entity();
        $this->data['get_countries'] = $contries_obj->get_all_countries_as();
        
        $this->data['industry_name']=$this->data['get_user']["industry_name"];
        $this->data['job_title']=$this->data['get_user']["extra_title"];
        $this->data['country']=$this->data['get_user']["country"];
        $this->data['city']=$this->data['get_user']["city"];
        $this->data['level']=$this->data['get_user']["level_title"];
        $this->data['experienceyear']=$this->data['get_user']["experienceyear"];
        $this->data['get_countries_dropdown'] =$this->dropdown($this->data['get_countries'],$this->data['get_user']["country_id"],array('name'=>'country','placeholder'=>'Country','onchange'=>'getCities(this)'));
        
//        $cities = $this->getAll("SELECT city_id AS id,city AS name FROM ".TABLE_CITIES." WHERE country_id='".$this->data['get_user']["country_id"]."'");
//        $this->data['get_cities_dropdown'] =$this->dropdown($cities,$this->data['get_user']["city_id"],array('name'=>'city','placeholder'=>'City'));
//        $get_industry = $this->getAll("SELECT industry_id AS id,industry_name AS name FROM " . TABLE_INDUSTRY);
//        $this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"],array('name'=>'industry','placeholder'=>'Select Industry'));
//        $get_experience_years = $this->getAll("SELECT experienceyear_id AS id,experienceyear AS name FROM " . TABLE_EXPERIENCE_YEARS);
//        $this->data['get_experience_years'] = $this->dropdown($get_experience_years,$this->data['get_user']["experienceyear_id"],array('name'=>'experience_level','placeholder'=>'Years'));
//        $get_job_level = $this->getAll("SELECT job_level_id AS id,level_title AS name FROM " . TABLE_JOB_LEVEL);
//        $this->data['get_job_level'] = $this->dropdown($get_job_level,$this->data['get_user']["job_level_id"],array('name'=>'year','placeholder'=>'Experience Level'));
        
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $this->data['get_user']["country_id"];
        $cities = $cities_obj->get_all_cities_by_country_id();
        $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City'));
        
        $industry_obj = new Tyr_industry_entity();
        $get_industry = $industry_obj->get_all_industries();
        $this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"], array('name' => 'industry', 'placeholder' => 'Select Industry'));
        
        
        $job_level_obj = new Tyr_job_level_entity();
        $get_job_level = $job_level_obj->get_all_job_level();
        $this->data['get_levels'] = $get_job_level;
        $this->data['get_job_level'] = $this->dropdown($get_job_level, $this->data['get_user']["job_level_id"], array('name' => 'year', 'placeholder' => 'Experience Level'));
        
        $experience_years_obj = new Tyr_experience_years_entity();
        $get_experience_years = $experience_years_obj->get_all_experience_years();
        $this->data['get_experience'] = $get_experience_years;
        $this->data['get_experience_years'] = $this->dropdown($get_experience_years, $this->data['get_user']["experienceyear_id"], array('name' => 'experience_level', 'placeholder' => 'Years'));

        //--------------------------Experience Start-------------------------------------------------------
//        $experience = $this->getAll("SELECT exp_id, user_id, company_name,job_title,job_location,IF(job_start='',YEAR(CURDATE()) ,job_start) AS job_start,
//        IF(job_end='',YEAR(CURDATE()) ,job_end) AS job_end, job_description, job_url, job_country, job_city,
//                  current_job, created,modified,status_sl  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $user_id . "' and status_sl='1'
//                  ");
//        $this->data['get_experience'] = $experience;
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->user_id = $user_id;
        $experience_obj->status_sl = 1;
        $experience = $experience_obj->get_user_all_experience();
        $this->data['get_experience'] = $experience;

        //--------------------------Experience END-------------------------------------------------------

        //--------------------------Education Start-------------------------------------------------------
//        $education = $this->getAll("SELECT education_id, user_id,institute_name,edu_organization,edu_position ,degree_title,field_interest,grade_obtain,IF(start_year='',YEAR(CURDATE()) ,start_year) as start_year,
//         IF(end_year='',YEAR(CURDATE()) ,end_year) as end_year,activities,education_description,created,modified,status_sl
//        FROM " . TABLE_EDUCATION . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//        $this->data['get_education'] = $education;
        
        $education_obj = new Tyr_education_entity();
        $education_obj->user_id = $user_id;
        $education_obj->status_sl = 1;
        $education = $education_obj->get_user_all_education();
        $this->data['get_education'] = $education;

        //--------------------------Education END-------------------------------------------------------


        //--------------------------Awards Start-------------------------------------------------------
//        $awards = $this->getAll("SELECT award_id, user_id,award,award_organization,award_description,award_year,created_timestamp, modified,  status_sl
//                FROM " . TABLE_AWARDS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//        $this->data['get_awards'] = $awards;
//        
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->user_id = $user_id;
        $awards_obj->status_sl = 1;
        $awards = $awards_obj->get_user_all_award();
        $this->data['get_awards'] = $awards;
        //--------------------------Awards END-------------------------------------------------------

        $this->template_arr = array('header', 'contents/edit_profile_new', 'footer');
        $this->load_template();
    }

    /*--------------------Create Thumb-------------------*/
    public function rm_experience(){
        $item_id=$this->input->post("item_id");
        $where = "exp_id ='" . $item_id . "' AND user_id='".$this->session->userdata("user_id")."'";
        $experience_detail = array(
                "status_sl" => -1
            );
        //$return = $this->update(TABLE_EXPERIENCE,$experience_detail,$where);
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->exp_id = $item_id;
        $experience_obj->get_experience();
        $experience_obj->status_sl = -1;
        $return = $experience_obj->update_experience();
        
        if($return){
                    echo json_encode(array('status'=>true,'success'=>true));
                }else{
                    echo json_encode(array('status'=>false,'success'=>true));
                }
    }
    
    public function sv_add_experience(){
        if($this->input->post("endpresent")=="true"){
            $current_job=1;
        }else{
            $current_job=0;
        }
        
        $experience_detail = array(
        "user_id" => $this->session->userdata("user_id"),
        "company_name" => $this->input->post("company"),
        "job_title" => $this->input->post("job_title"),
        "current_job" => $current_job,
        "job_start" => strtotime($this->input->post("startdate")),
        "job_end" => strtotime($this->input->post("enddate")),
        "job_description" => $this->input->post("description"),
        "created" => strtotime("now")
        );
        
        if($current_job>0){
            $experience_obj = new Tyr_experience_entity();
            $experience_obj->current_job = 0;
            $experience_obj->job_end = strtotime('now');
            $experience_obj->user_id = $this->session->userdata("user_id");
            $experience_obj->update_user_all_experience();
            
//            $upd_all_experience=array(
//                "current_job" => 0,
//                "job_end" => strtotime('now'),
//            );
//            $where = "user_id ='" . $this->session->userdata("user_id") . "'";
//            $this->update(TABLE_EXPERIENCE,$upd_all_experience,$where);
        }
        
        if($this->input->post("item_id") > 0){
            $exp_id = $this->input->post("item_id");
            
            $experience_obj = new Tyr_experience_entity();
            $experience_obj->exp_id = $exp_id;
            $experience_obj->get_experience();
            $experience_obj->user_id = $this->session->userdata("user_id");
            $experience_obj->company_name = $this->session->post("company");
            $experience_obj->job_title = $this->session->post("job_title");
            $experience_obj->current_job = $current_job;
            $experience_obj->job_start = strtotime($this->input->post("startdate"));
            $experience_obj->job_end = strtotime($this->input->post("enddate"));
            $experience_obj->job_description = $this->input->post("description");
            $experience_obj->created = strtotime("now");
            $return = $experience_obj->update_experience();
            
//            $where = "exp_id ='" . $exp_id . "'";
//            $return = $this->update(TABLE_EXPERIENCE,$experience_detail,$where);
            $item_id=$this->input->post("item_id");
        } else {
            
            $experience_obj = new Tyr_experience_entity();
            $experience_obj->user_id = $this->session->userdata("user_id");
            $experience_obj->company_name = $this->session->post("company");
            $experience_obj->job_title = $this->session->post("job_title");
            $experience_obj->current_job = $current_job;
            $experience_obj->job_start = strtotime($this->input->post("startdate"));
            $experience_obj->job_end = strtotime($this->input->post("enddate"));
            $experience_obj->job_description = $this->input->post("description");
            $experience_obj->created = strtotime("now");
            $return = $experience_obj->save_experience();
            
            //$return = $this->insert(TABLE_EXPERIENCE, $experience_detail);
            $item_id=$return;
        }
        
        if($return){
            echo json_encode(array('status'=>true,'success'=>true,'item_id'=>$item_id));
        }else{
            echo json_encode(array('status'=>false,'success'=>true));
        }
    }
    
    
    public function sv_profile_top(){
        $user_id = $this->session->userdata("user_id");
//        $user_details = array(
//                        'city' => $this->input->post("city"),
//                        'country_id' => $this->input->post("country"),
//                        'job_level_id' =>$this->input->post("year") ,
//                        'extra_title' => $this->input->post("extra_stuff"),
//                        'industry_id' => $this->input->post("industry"),
//                        'experienceyear_id' => $this->input->post("experience_level"),
//                        'modified_timestamp' => strtotime("now")
//                    );
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();

        $users_obj->job_level_id = (int)$this->input->post("year");
        $users_obj->extra_title = (int)$this->input->post("extra_stuff");
        $users_obj->industry_id = (int)$this->input->post("industry");
        $users_obj->experienceyear_id = (int)$this->input->post("experience_level");
        $users_obj->modified_timestamp = strtotime("now");
        $return_user = $users_obj->update_users();
        
//        $where = array("user_id" => $user_id);
//        $return_user = $this->update(TABLE_USERS, $user_details, $where);
        
        if($return_user){
            echo json_encode(array('status'=>true,'success'=>true));
        }else{
            echo json_encode(array('status'=>false,'success'=>true));
        }
    }
    
    public function save_education()
    {
        
           $education_detail = array(
               "user_id" => $this->session->userdata("user_id"),
               "edu_organization" => $this->input->post("school"),
               //"edu_url" => $this->input->post("edu_url"),
               "edu_position" => $this->input->post("degree"),
               //"edu_country" => $this->input->post("edu_country"),
               //"edu_city" => $this->input->post("edu_city"),
               "start_year" => strtotime($this->input->post("startdate")),
               "end_year" => strtotime($this->input->post("enddate")),
               "education_description" => $this->input->post("description"),
               "created" => strtotime("now")
           );

           if($this->input->post("item_id") > 0){
                $edu_id  = $this->input->post("item_id");
               
                $education_obj = new Tyr_education_entity();
                $education_obj->education_id = $edu_id;
                $education_obj->get_education();
                
                $education_obj->user_id = $this->session->userdata("user_id");
                $education_obj->edu_organization = $this->input->post("school");
                $education_obj->edu_position = $this->input->post("degree");
                $education_obj->start_year = $this->input->post("startdate");
                $education_obj->end_year = $this->input->post("enddate");
                $education_obj->education_description = $this->input->post("description");
                $education_obj->created = strtotime("now");
                $return = $education_obj->update_education();
                
               //$where = "education_id ='" . $edu_id . "'";
               //$return = $this->update(TABLE_EDUCATION,$education_detail,$where);
               $item_id=$edu_id;
           } else {
               
                $education_obj = new Tyr_education_entity();
                $education_obj->user_id = $this->session->userdata("user_id");
                $education_obj->edu_organization = $this->input->post("school");
                $education_obj->edu_position = $this->input->post("degree");
                $education_obj->start_year = $this->input->post("startdate");
                $education_obj->end_year = $this->input->post("enddate");
                $education_obj->education_description = $this->input->post("description");
                $education_obj->created = strtotime("now");
                $return = $education_obj->save_education();
               
               //$return = $this->insert(TABLE_EDUCATION, $education_detail);
               $item_id=$return;
           }
           if($return>0){
               echo json_encode(array('status'=>true,'success'=>true,'item_id'=>$item_id));
           }else{
               echo json_encode(array('status'=>false,'success'=>true));
           }
    }
    
    public function rm_education(){
           $item_id=$this->input->post("item_id");
           
            $education_obj = new Tyr_education_entity();
            $education_obj->education_id = $edu_id;
            $education_obj->get_education();
            $education_obj->status_sl = -1;
            $education_obj->modified = strtotime("now");
            $return = $education_obj->update_education();
            
//           $where = "education_id ='" . $item_id . "' AND user_id='".$this->session->userdata("user_id")."'";
//        $update = array(
//                   "status_sl" => -1,
//            "modified" => strtotime("now")
//               );
//        $return = $this->update(TABLE_EDUCATION, $update, $where);
           if($return){
                echo json_encode(array('status'=>true,'success'=>true));
            }else{
                echo json_encode(array('status'=>false,'success'=>true));
            }
    }
    
    public function save_awards(){
        $details = array(
           "user_id" => $this->session->userdata("user_id"),
           "award" => $this->input->post("awards"),
         //  "award_website" => $this->input->post("award_website"),
           "award_organization" => $this->input->post("organisation"),
           "award_year" => $this->input->post("startdate"),
           "award_description" => $this->input->post("description"),
           "created_timestamp" => strtotime("now")
       );
        if($this->input->post("item_id") > 0){
            $award_obj = new Tyr_awards_entity();
            $award_obj->award_id = $this->input->post("item_id");
            $award_obj->get_awards();
            $award_obj->award = $this->input->post("awards");
            $award_obj->award_organization = $this->input->post("organisation");
            $award_obj->award_year = $this->input->post("startdate");
            $award_obj->award_description = $this->input->post("startdate");
            $award_obj->award_year = $this->input->post("description");
            $return = $award_obj->update_awards();
            
            $award_id  = $this->input->post("item_id");
//            $where = "award_id ='" . $award_id . "'";
//            $return = $this->update(TABLE_AWARDS,$details,$where);
            $item_id=$award_id;
            
            } else {
                
                $award_obj = new Tyr_awards_entity();
                $award_obj->created_timestamp = $this->input->post("item_id");
                $award_obj->award = $this->input->post("awards");
                $award_obj->award_organization = $this->input->post("organisation");
                $award_obj->award_year = $this->input->post("startdate");
                $award_obj->award_description = $this->input->post("startdate");
                $award_obj->created_timestamp = strtotime("now");
                $return = $award_obj->save_awards();
//                $return = $this->insert(TABLE_AWARDS, $details);
                $item_id = $award_obj->award_id;
            }
            if($return > 0){
                echo json_encode(array('status'=>true,'success'=>true,'item_id'=>$item_id));
            }else{
                echo json_encode(array('status'=>false,'success'=>true));
            }
    }
    
    public function rm_awards(){
        $item_id = $this->input->post("item_id");
//        $where = "award_id ='" . $item_id . "' AND user_id='".$this->session->userdata("user_id")."'";
//        $update = array(
//                   "status_sl" => -1,
//            "modified" => strtotime("now")
//               );
//        $return = $this->update(TABLE_AWARDS, $update, $where);
        
        $award_obj = new Tyr_awards_entity();
        $award_obj->award_id = $item_id;
        $award_obj->get_awards();
        $award_obj->status_sl = -1;
        $award_obj->modified = strtotime("now");
        $return = $award_obj->update_awards();
        
        if($return){
            echo json_encode(array('status'=>true,'success'=>true));
        }else{
            echo json_encode(array('status'=>false,'success'=>true));
        }
    }
    
    public function save_profile()
    {
        $user_id = $this->session->userdata("user_id");
        $callfrom= $this->input->post("callfrom");
        switch($callfrom){
            case 'profile_top':
                    $this->sv_profile_top();
                exit;
            break;
            case 'experience':
                    $this->sv_add_experience();
                exit;
            break;
            case 'removeexperience':
                $this->rm_experience();
                exit;
            break;
            case 'awards':
                   $this->save_awards();
               exit;
           break;
           case 'removeawards':
               $this->rm_awards();
               exit;
           break;
            case 'education':
                   $this->save_education();
               exit;
           break;
           case 'removeeducation':
               $this->rm_education();
               exit;
           break;
        }
        #Validation
        $config_validation = array(
            array(
                'field' => 'user_name',
                'label' => LABEL_USER_NAME,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_country',
                'label' => LABEL_COUNTRY,
                'rules' => 'trim|required'
            ),

            /* array(
                 'field' => 'user_cellphone',
                 'label' => LABEL_CELLPHONE,
                 'rules' => 'trim|required|numeric'
             )*/
        );
        $this->form_validation->set_rules($config_validation);
#Not Validate
        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = "";
            $this->data['icon'] = "icon-remove-sign";
            $this->data['alert'] = "alert alert-danger";
            $this->template_arr = array('header', 'contents/edit_profile_new', 'footer');
            $this->load_template();
        } #Validate
        else {
            $this->data['alert'] = "alert alert-success";
            $this->data['icon'] = "icon-ok-sign";
            /* $duplicate_username = $this->getRow("SELECT count(*) as cnt from ".TABLE_USERS." where username='".$this->input->post("user_name")."'");
             if($duplicate_username['cnt']> 0){
                 $this->data['msg'] = LABEL_USERNAME_ALREADY_REGISTERED;
                 $this->template_arr = array('header', 'contents/edit_profile_new', 'footer');
                             $this->load_template();
             }else{*/
            $user_id = $this->session->userdata("user_id");
            $where = array("user_id" => $user_id);

            $profile_url = "tyroe." . $user_id;
            if ($this->session->userdata("role_id") == TYROE_PRO_ROLE) {
                $profile_url = $this->input->post("profile_url");
            }
            $state = $this->input->post("user_state1");
            if ($state == "") {
                $state = $this->input->post("user_state");
            }

            $user_details = array(
                'username' => $this->input->post("user_name"),
                'firstname' => $this->input->post("firstname"),
                'lastname' => $this->input->post("lastname"),
                'profile_url' => $profile_url,
                'country_id' => $this->input->post("user_country"),
                'tyroe_level' => $this->input->post("tyroe_level"),
                'user_biography' => $this->input->post("user_biography"),
                'user_occupation' => $this->input->post("user_occupation"),
                'user_web' => $this->input->post("user_web"),
                'user_experience' => $this->input->post("user_experience"),
                'state' => $state,
                'city' => $this->input->post("user_city"),
                'cellphone' => $this->input->post("user_cellphone_code") . "-" . $this->input->post("user_cellphone1"),
                'modified_timestamp' => strtotime("now")
            );

            /* $user_confirmpass = trim($this->input->post("user_confirmpass"));
             #password edit
             if ($user_confirmpass != "") {
                 $user_details['password'] = $this->hashPass($user_confirmpass);
                 $user_details['password_len'] = strlen($user_confirmpass);
             }*/

            //$return_user = $this->update(TABLE_USERS, $user_details, $where);
            
            $user_obj = new Tyr_users_entity();
            $user_obj->user_id = $user_id;
            $user_obj->get_user();
            $user_obj->username = $this->input->post("user_name");
            $user_obj->firstname = $this->input->post("firstname");
            $user_obj->lastname = $this->input->post("lastname");
            $user_obj->profile_url = $profile_url;
            $user_obj->country_id = $this->input->post("user_country");
            $user_obj->tyroe_level = $this->input->post("tyroe_level");
            $user_obj->user_biography = $this->input->post("user_biography");
            $user_obj->user_occupation = $this->input->post("user_occupation");
            $user_obj->user_web = $this->input->post("user_web");
            $user_obj->user_experience = $this->input->post("user_experience");
            $user_obj->state = $state;
            $user_obj->city =  $this->input->post("user_city");
            $user_obj->cellphone = $this->input->post("user_cellphone_code") . "-" . $this->input->post("user_cellphone1");
            $user_obj->modified_timestamp = strtotime("now");
            $return_user = $user_obj->update_users();

            #Profile Image Upload

            $media_name = $this->input->post('imgname');
            if (!empty($media_name)) {
//                $old_profile_pic = $this->getRow("Select image_id from " . TABLE_MEDIA . " where user_id='" . $user_id . "' and profile_image='1' and status_sl='1'");
//                $where_media = "image_id = '" . $old_profile_pic['image_id'] . "'";
//                $data = array(
//                    'modified' => strtotime("now"),
//                    'status_sl' => "-1"
//                );
//                $this->update(TABLE_MEDIA, $data, $where_media);
//
//                $image_detail = array(
//                    "user_id" => $user_id,
//                    "profile_image" => '1',
//                    "media_type" => 'image',
//                    "media_name" => $media_name,
//                    'created' => strtotime("now"),
//                    'modified' => strtotime("now")
//                );
//                $this->insert(TABLE_MEDIA, $image_detail);
                
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $user_id;
                $media_obj->profile_image = 1;
                $media_obj->status_sl = 1;
                $old_profile_pic = $media_obj->get_user_profile_image();
                
                $media_obj = new Tyr_media_entity();
                $media_obj->image_id = $old_profile_pic['image_id'];
                $media_obj->get_media();
                $media_obj->status_sl = -1;
                $media_obj->modified = strtotime("now");
                $media_obj->update_media();
                
                
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = user_id;
                $media_obj->profile_image = 1;
                $media_obj->media_type = 'image';
                $media_obj->media_name = $media_name;
                $media_obj->status_sl = 1;
                $media_obj->created = strtotime("now");
                $media_obj->modified = strtotime("now");
                $media_obj->save_media();

            }

            $s_arr = array();
            $s_id = $this->input->post("site_id");
            $s_name = $this->input->post("site_name");
            if (!empty($s_id[0])) {
                $user_sites_obj = new Tyr_user_sites_entity();
                $user_sites_obj->user_id = $user_id;
                $user_obj->delete_user_sites();
                //$this->delete(TABLE_USER_SITES, "user_id = '{$user_id}'");
            }
            for ($sid = 0; $sid < count($s_id); $sid++) {
                if (!empty($s_id[$sid])) {
                    $s_arr[] = array('other_site_id' => $s_id[$sid], 'site_name' => $s_name[$sid]);
                }
            }
            foreach ($s_arr as $s_k => $s_v) {
//                $site_details = array(
//                    "user_id" => $user_id,
//                    "other_site_id" => $s_v['other_site_id'],
//                    "site_name" => $s_v['site_name'],
//                    'status_sl' => 1
//                );
//                $return = $this->insert(TABLE_USER_SITES, $site_details);
                $user_sites_obj = new Tyr_user_sites_entity();
                $user_sites_obj->user_id = $user_id;
                $user_sites_obj->other_site_id = $s_v['other_site_id'];
                $user_sites_obj->site_name = $s_v['site_name'];
                $user_sites_obj->status_sl = 1;
                $user_sites_obj->save_user_sites();
            }
            if ($return_user > 0) {
                $this->session->set_userdata("update_profile", 1);
                header('location:' . $this->getURL("profile"));
                /*$this->template_arr = array('header', 'contents/edit_profile_new', 'footer');
                $this->load_template();*/
            }
        }
    }

    public function check_username()
    {
        $user_id = $this->session->userdata("user_id");
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->username = $this->input->post("username");
        $users_obj->profile_url = $this->input->post("username");
        $duplicate_username = $users_obj->check_duplicate_username();
//        
//        
//        $duplicate_username = $this->getRow("SELECT count(*) as result from " . TABLE_USERS . " where username='" . $this->input->post("username") . "' AND user_id NOT IN('" . $user_id . "')");
        echo json_encode($duplicate_username);
    }

    public function public_profile($data = "")
    {
        if (!empty($data)) {
            $this->data['param'] = $data;
            $this->data['recommend_id'] = $data;
        }

        $this->data['page_title'] = 'Public Profile';
        $user_id = $this->session->userdata("user_id");

//        $get_user = $this->getRow("SELECT " . TABLE_USERS . ".user_id,tyroe_level,role_id,parent_id," . TABLE_USERS . ".country_id,profile_url,email,
//                       username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                       state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                        " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                     LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                     LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id AND " . TABLE_MEDIA . ".profile_image = '1'
//                     WHERE " . TABLE_USERS . ".user_id='" . $user_id . "'  ORDER BY image_id DESC LIMIT 0 ,1");
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_profile($user_id);

        $image;
        if ($get_user['media_name']) {
            $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH . "img/no-image.jpg";
        }


        /*  $new_pass = "";
          for ($i = 1; $i <= $get_user['password_len']; $i++) {
              $new_pass .= " ";
          }*/
        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "password" => $new_pass,
            "image" => $image,
            "password_len" => $get_user['password_len'],
            "tyroe_level" => $get_user['tyroe_level'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id']
        );
        $this->data['get_user'] = $user_data;

        $this->template_arr = array('header', 'contents/public_profile', 'footer');

        $this->load_template();
    }

    public function public_recommendation($data)
    {
        $this->data['param'] = $data;
        $this->data['recommend_id'] = $data;
        $this->data['page_title'] = 'Public Profile';
//        $link_check = $this->getRow("SELECT recommend_id,user_id,recommend_status FROM ".TABLE_RECOMMENDATION."
//                                        WHERE recommend_id = '{$data}'");
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $data;
        $recommendation_obj->get_recommendation();
        $link_check = (array)$recommendation_obj;
        
        if($link_check){
            $user_id = $link_check['user_id'];
            $this->data['recommend_status'] = $link_check['recommend_status'];
        } else {
            $this->data['recommend_status'] = $link_check['recommend_status'];
        }

//        $get_user = $this->getRow("SELECT " . TABLE_USERS . ".user_id,tyroe_level,role_id,parent_id," . TABLE_USERS . ".country_id,profile_url,email,
//                       username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
//                       state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
//                        " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//                     LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//                     LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id AND " . TABLE_MEDIA . ".profile_image = '1'
//                     WHERE " . TABLE_USERS . ".user_id='" . $user_id . "'  ORDER BY image_id DESC LIMIT 0 ,1");

        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_detail_info($user_id);
        
        $image;
        if ($get_user['media_name']) {
            $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH . "img/no-image.jpg";
        }

        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "password" => $new_pass,
            "image" => $image,
            "password_len" => $get_user['password_len'],
            "tyroe_level" => $get_user['tyroe_level'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id']
        );
        $this->data['get_user'] = $user_data;

        $this->template_arr = array('header', 'contents/public_recommendation', 'footer');

        $this->load_template();

    }

    public function save_recommendation()
    {
        $recommend_id = $this->input->post("recommend_id");
        $recommend_name = $this->input->post("recom_name");
        $recommendation = $this->input->post("recom");
        $data_recommendation = array(
            "recommendation" => $recommendation,
            "recommend_name" => $recommend_name,
            "recommend_status" => -1
        );
        //$this->update(TABLE_RECOMMENDATION, $data_recommendation, "recommend_id = '{$recommend_id}'");
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $recommend_id;
        $recommendation_obj->get_recommendation();
        $recommendation_obj->recommendation = $recommendation;
        $recommendation_obj->recommend_name = $recommend_name;
        $recommendation_obj->recommend_status = -1;
        $recommendation_obj->update_recommendation();
        
        header("location:" . $this->getURL("profile/public_recommendation/".$recommend_id));
        /*$this->template_arr = array('header', 'contents/public_recommendation', 'footer');
        $this->load_template();*/
    }

    public function get_state_by_country($country = "")
    {
        $bool = 0;
        $country = $this->input->post("country");
        $ddname = $this->input->post("ddname");
//        if ($country != "") {
//            $where_country = "AND country_id='" . $country . "'";
//            $bool = 1;
//        }
//        $get_states = $this->getAll("SELECT states_id,states_name FROM " . TABLE_STATES . " where 1=1 " . $where_country . " ORDER BY states_name ASC");
        
        $get_states = array();
        if ($country != "") {
            $bool = 1;
            $states_obj = new Tyr_states_entity();
            $states_obj->country_id = $country;
            $get_states = $states_obj->get_all_states_by_country();
        }
        
        $this->data['get_states'] = $get_states;
        $this->data['ddname'] = $ddname;
        if ($bool == 1) {
            $this->template_arr = array('contents/get_states');
            $this->load_template();
        }
        //return $get_states;
    }

    public function save_image()
    {
        #Profile Image Upload
        $user_id = $this->session->userdata("user_id");
        $media_name = $this->input->post('imgname');
        if (!empty($media_name)) {
            //$old_profile_pic = $this->getRow("Select image_id from " . TABLE_MEDIA . " where user_id='" . $user_id . "' and profile_image='1' and status_sl='1'");
            //$where_media = "image_id = '" . $old_profile_pic['image_id'] . "'";
//            $data = array(
//                'modified' => strtotime("now"),
//                'status_sl' => "-1"
//            );
//            $return = $this->update(TABLE_MEDIA, $data, $where_media);
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->profile_image = 1;
            $media_obj->status_sl = 1;
            $old_profile_pic = $media_obj->get_user_profile_image();

            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $old_profile_pic['image_id'];
            $media_obj->get_media();
            $media_obj->status_sl = -1;
            $media_obj->modified = strtotime("now");
            $return = $media_obj->update_media();
            
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = user_id;
            $media_obj->profile_image = 1;
            $media_obj->media_type = 'image';
            $media_obj->media_name = $media_name;
            $media_obj->status_sl = 1;
            $media_obj->created = strtotime("now");
            $media_obj->modified = strtotime("now");
            $media_obj->save_media();

//            $image_detail = array(
//                "user_id" => $user_id,
//                "profile_image" => '1',
//                "media_type" => 'image',
//                "media_name" => $media_name,
//                'created' => strtotime("now"),
//                'modified' => strtotime("now")
//            );
//            $return = $this->insert(TABLE_MEDIA, $image_detail);
            $image_id_id = $media_obj->image_id;
            //$this->data['image_id']=$image_id_id;
            if ($return) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_PROFILE_IMAGE_UPDATE,'image_id'=>$image_id_id));
            } else {
                echo json_encode(array('success' => false));
            }
        }
    }

    public function delete_photo()
    {
        $user_id = $this->session->userdata("user_id");
//        $where_media = "image_id = '" . $this->input->post('image_id') . "'";
//        $data = array(
//            'modified' => strtotime("now"),
//            'status_sl' => "-1"
//        );
//        $return = $this->update(TABLE_MEDIA, $data, $where_media);
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $this->input->post('image_id');
        $media_obj->get_media();
        $media_obj->status_sl = -1;
        $media_obj->modified = strtotime("now");
        $return = $media_obj->update_media();
        
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_PROFILE_IMAGE_DELETED));
        } else {
            echo json_encode(array('success' => false));
        }
    }
}