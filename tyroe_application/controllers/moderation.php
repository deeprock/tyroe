<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);

class Moderation extends Tyroe_Controller {

    //put your code here
    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->data['page_title'] = 'Review Moderation';
        $user_id = $this->session->userdata("user_id");
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_HOME . ') ');
        } else if ($this->session->userdata('role_id') == TYROE_FREE_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . TYRO_FREE_MODULE_HOME . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_HOME . ') ');
        }

        $this->_add_css('moderation.css');

        $pending_moderation_review_obj = new Tyr_rate_review_entity();
        $this->data['pending_reviews'] = $pending_moderation_review_obj->get_pending_moderation_review($user_id);

        $this->template_arr = array('header', 'contents/moderation', 'footer');
        $this->load_template();
    }

    public function update_review($review_id, $status) {
        $review_update_obj = new Tyr_rate_review_entity();
        $review_update_obj->review_id = $review_id;
        $review_update_obj->moderated = $status;
        $review_update_obj->update_pending_moderation();
        $message_return = 'Review Updated Successfully';

        $review_update_obj->get_rate_review();

        //reviewer user details and company details
        $reviewer_user_obj = new Tyr_users_entity();
        $reviewer_user_obj->user_id = $review_update_obj->user_id;
        $reviewer_user_obj->get_user();

        $parent_id = intval($reviewer_user_obj->parent_id);
        if ($parent_id == 0) {
            $parent_id = $reviewer_user_obj->user_id;
        }

        $company_details_obj = new Tyr_company_detail_entity();
        $company_details_obj->studio_id = $parent_id;
        $company_details_obj->get_company_detail_by_studio();

        //tyroe user details
        $tyroe_user_obj = new Tyr_users_entity();
        $tyroe_user_obj->user_id = $review_update_obj->tyroe_id;
        $tyroe_user_obj->get_user();

        //job details
        $job_details_obj = new Tyr_jobs_entity();
        $job_details_obj->job_id = $review_update_obj->job_id;
        $job_details_obj->get_jobs();

        $activity_type = 3; // Feedback
        $activity_data_tyroe = array(
            "reviewer_job_title" => $reviewer_user_obj->firstname . " " . $reviewer_user_obj->lastname,
            "reviewer_company_name" => $company_details_obj->company_name,
            "rev_id" => $review_id,
            "rev_name" => $reviewer_user_obj->firstname . " " . $reviewer_user_obj->lastname,
            "search_keywords" => "feedback comment reviewed"
        );

        $job_activity_obj1 = new Tyr_job_activity_entity();
        $job_activity_obj1->performer_id = $tyroe_user_obj->user_id;
        $job_activity_obj1->job_id = $review_update_obj->job_id;
        $job_activity_obj1->object_id = $tyroe_user_obj->user_id;
        $job_activity_obj1->activity_type = $activity_type;
        $job_activity_obj1->activity_data = serialize($activity_data_tyroe);
        $job_activity_obj1->status_sl = 1;
        $job_activity_obj1->created_timestamp = strtotime("now");
        $job_activity_obj1->save_job_activity();

        if (intval($status) == 1) {

            $schedule_notification_obj = new Tyr_schedule_notification_entity();
            $schedule_notification_obj->user_id = $tyroe_user_obj->user_id;
            $schedule_notification_obj->option_id = 16;
            $check_notification_feedback_status = $schedule_notification_obj->get_schedule_notification_by_user_option();

            if ($check_notification_feedback_status['status_sl'] == 1) {
                #Email
                $job_title = $job_details_obj->job_title;
                $reviewer_job_title = $reviewer_user_obj->job_title;
                $reviewer_full_name = $reviewer_user_obj->firstname . " " . $reviewer_user_obj->lastname;
                $company_name = $company_details_obj->company_name;
                $url = $this->getURL("feedback/" . strtolower(str_replace(' ', '-', $reviewer_full_name . "-" . $this->generate_job_number($review_id))));

                //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n04'");
                $notification_templates_obj = new Tyr_notification_templates_entity();
                $notification_templates_obj->notification_id = 'n04';
                $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                $subject = $get_message['subject'];
                $subject = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $subject);
                $message = htmlspecialchars_decode($get_message['template']);
                $message = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $message);
                $current_user_email = $tyroe_user_obj->email;
                $this->email_sender($current_user_email, $subject, $message);
            }
        }else if(intval($status) == 2){
            #Email
            $job_title = $job_details_obj->job_title;
            $reviewer_job_title = $reviewer_user_obj->job_title;
            $reviewer_full_name = $reviewer_user_obj->firstname . " " . $reviewer_user_obj->lastname;
            $company_name = $company_details_obj->company_name;
            $url = $this->getURL("feedback/" . strtolower(str_replace(' ', '-', $reviewer_full_name . "-" . $this->generate_job_number($review_id))));

            //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n04'");
            $notification_templates_obj = new Tyr_notification_templates_entity();
            $notification_templates_obj->notification_id = 'n21';
            $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

            $subject = $get_message['subject'];
            $subject = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $subject);
            $message = htmlspecialchars_decode($get_message['template']);
            $message = str_replace(array('[$job_title]', '[$reviewer_company_name]', '[$reviewer_job_title]', '[$reviewer_name]', '[$url_viewfeedback]', '[$url_tyroe]'), array($job_title, $company_name, $reviewer_job_title, $reviewer_full_name, $url, LIVE_SITE_URL), $message);
            $current_user_email = $tyroe_user_obj->email;
            $this->email_sender($current_user_email, $subject, $message);
        }
        echo json_encode(array(
            'status' => 'SUCCESS',
            'message' => $message_return
        ));
    }

}
