<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);

class Jobopenings extends Tyroe_Controller
{
    public function Jobopenings()
    {
        parent::__construct();
        $this->data['page_title'] = 'Openings';

        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_OPENING . ') ');
            //$this->get_system_modules(' AND module_id IN (' . OPENINGS_SUB_MENU . ') ');
        }  else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_OPENING . ') ');
        }else {
            $this->get_system_modules(' AND module_id IN (' . OPENINGS_SUB_MENU . ') ');
        }

        $this->load->library('form_validation');
    }

    public function index()
    {
        $bool = $this->input->post('bool');
        $job_title = $this->input->post('job_title');
        $filter_value=$this->input->post('filter_value');
        $filter_column = $this->input->post('filter_column');
        $filter_condition = $this->input->post('filter_condition');
        $where ='';
        if ($job_title != '') {
            $search_by = $this->input->post("search_by");
            if($search_by =="company_name")
            {
                $where .=  "AND ".TABLE_COMPANY.".company_name LIKE('%".$job_title."%')";
            }
            else{
                $where .= " AND " . TABLE_JOBS . ".job_title  LIKE('%" . $job_title . "%')";
            }
            $this->data['search_by']=$search_by;
            $this->data['job_title'] = $job_title;
        }
        $sorting = $this->input->post('sorting');
        if($sorting == true){
            echo 'sorting';
            $sort_column = $this->input->post('sort_column');
            $sort_type = $this->input->post('sort_type');
            $order_by = "ORDER BY ".$sort_column." ".$sort_type;
            $this->data['sorting'] = $sorting;
            $this->data['sort_column'] = $sort_column;
            $this->data['sort_type'] = $sort_type;
        }else{
            $order_by=' ORDER BY job_id DESC ';
        }
        if($filter_value!="")
        {
            $where .= " AND ".$filter_column.$filter_condition."'".$filter_value."' ";
            $this->data['filter_value']=$filter_value;
            $this->data['filter_column']=$filter_column;
            $this->data['filter_condition']=$filter_condition;
        }
        
        $hidden_user ="SELECT array_to_string(array_agg(tyroe_id ORDER BY user_id ASC), ',')AS user_id FROM ".TABLE_HIDDEN." WHERE studio_id=tyr_jobs.user_id AND job_id = tyr_jobs.job_id GROUP BY studio_id";
        //$hidden_user ="SELECT GROUP_CONCAT(tyroe_id ORDER BY user_id ASC SEPARATOR ',')AS user_id FROM ".TABLE_HIDDEN." WHERE studio_id=tyr_jobs.user_id AND job_id = tyr_jobs.job_id GROUP BY studio_id";
        
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_job_row_count_1($where);
        
//        $total_rows = $this->getRow("SELECT COUNT(*) as cnt
//                  FROM " . TABLE_JOBS . "
//        LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//        LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//        LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
//        LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
//        INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
//                  WHERE " . TABLE_JOBS . ".status_sl='1'
//                  and archived_status='0' ". $where .'  ORDER BY job_id DESC');
        
        
               $this->set_post_for_view();
               $pagintaion_limit = $this->pagination_limit();
            //$user_id = $this->session->userdata("user_id");
               
//        $get_jobs = $this->getAll("SELECT job_id," . TABLE_JOBS . ".user_id,category_id," . TABLE_JOBS . ".job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
//        " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city," . TABLE_USERS . ".parent_id," . TABLE_USERS . ".username,".TABLE_COMPANY.".company_name,
//          end_date,".TABLE_USERS.".email,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl,"
//        .TABLE_INDUSTRY.".industry_name,
//         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='3' AND invitation_status = 1 AND tyroe_id NOT IN(".$hidden_user.")) AS shortlisted,
//         (SELECT COUNT(*) FROM ".TABLE_USERS." WHERE parent_id=".TABLE_JOBS.".user_id AND status_sl=1) AS team,
//         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='1' AND invitation_status != -1  AND  tyroe_id NOT IN (" . $hidden_user . ")) AS candidate,
//         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='2' AND invitation_status = 0 AND  tyroe_id NOT IN (" . $hidden_user . ")) AS applicant
//         FROM " . TABLE_JOBS . "
//        LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//        LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//        LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
//        LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
//        INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
//          where " . TABLE_JOBS . ".status_sl='1'
//          and archived_status='0'" . $where. ' ' . $order_by.$pagintaion_limit['limit']);
        
        $joins_obj = new Tyr_joins_entity();
        $get_jobs = $joins_obj->get_all_jobs_details_not_for_hidden($hidden_user,$where,$order_by,$pagintaion_limit);
        
        foreach ($get_jobs as $jobs) {
           /* $hiden_tyroes = $this->getRow("SELECT GROUP_CONCAT(user_id ORDER BY user_id ASC SEPARATOR ',')AS user_id,studio_id FROM " . TABLE_HIDDEN . " WHERE studio_id='" . $jobs['user_id'] . "' AND job_id = '".$jobs['job_id']."' GROUP BY studio_id");
            echo $this->db->last_query();
            $hidden_user;
            if (is_array($hiden_tyroes)) {
                $hidden_user = $hiden_tyroes['user_id'];
            } else {
                $hidden_user = 0;
            }
            $sl = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id='3' AND invitation_status = 1  AND  tyroe_id NOT IN (" . $hidden_user . ")");
            $team = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_USERS." WHERE parent_id='" . $jobs['user_id'] . "' AND status_sl='1'");
            $cands = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id='1' AND invitation_status != -1  AND  tyroe_id NOT IN (" . $hidden_user . ")");
            $app = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $jobs['job_id'] . "' AND job_status_id='2' AND invitation_status = 0 AND  tyroe_id NOT IN (" . $hidden_user . ")");*/

            $all_jobs[] = array(
                "job_id" => $jobs['job_id'],
                "user_id" => $jobs['user_id'],
                "studio_name" => $jobs['company_name'],
                "industry_name" => $jobs['industry_name'],
                "job_title" => $jobs['job_title'],
                "email" => $jobs['email'],
                "team" => $jobs['team'],
                "ap" => $jobs['applicant'],
                "cands" => $jobs['candidate'],
                "sl" => $jobs['shortlisted'],
                "created_timestamp" =>$jobs['created_timestamp'],
                "offline" => $offline
            );
        }

        $get_all_studios = $this->getAll("SELECT user_id, username FROM ".TABLE_USERS." WHERE role_id = '3'");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 3;
        $get_all_studios = $users_obj->get_all_studio();
        
        
        foreach ($get_all_studios as $all_studios) {
            $get_studios[$all_studios['user_id']] = empty($all_studios['username'])?"No name":$all_studios['username'];
        }
        $this->data['get_all_studios'] = form_dropdown('all_studios',$get_studios,'','id="all_studios"');

        $this->data['job_detail'] = $all_jobs;
        $this->data['pagination'] = $this->pagination(array('url' => 'opening/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        if($bool){
            $this->template_arr = array('contents/user/get_openings_filter');
        }else{
            $this->template_arr = array('header', 'contents/user/get_openings', 'footer');
        }
        $this->load_template();
    }

    public function form()
    {

        $item_id = $this->input->post("item_id");
            if(!empty($item_id)){
                
                    $jobs_obj = new Tyr_jobs_entity();
                    $jobs_obj->job_id = $item_id;
                    $jobs_obj->get_jobs();
                    $opening_details = (array)$jobs_obj;
                    
                    //$opening_details = $this->getRow("SELECT * FROM ".TABLE_JOBS." WHERE job_id='".$item_id."'");
                    
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $opening_details['user_id'];
                    $users_obj->get_user();
                    $studio_name = (array)$users_obj;
                    
                    //$studio_name=$this->getRow("SELECT username FROM ".TABLE_USERS." WHERE user_id='".$opening_details['user_id']."'");
                    
                    $this->data['studio_name']=$studio_name['username'];
                    $this->data['opening_details']=$opening_details;
                    
                    $this->data['flag']="edit";
                    $joins_obj = new Tyr_joins_entity();
                    $skills = $joins_obj->get_all_skills_for_job($opening_details['job_id']);
                    //$skills=$this->getAll("SELECT a.creative_skill_id,b.creative_skill FROM ".TABLE_JOB_SKILLS." AS a LEFT JOIN ".TABLE_CREATIVE_SKILLS." AS b ON a.creative_skill_id=b.creative_skill_id WHERE a.job_id='".$opening_details['job_id']."'");
                    $this->data['skills']=$skills;
                    
                    $cities_obj = new Tyr_cities_entity();
                    $cities_obj->country_id = $opening_details['country_id'];
                    $cities = $cities_obj->get_all_cities_by_country_id();
                    //$cities=$this->getAll("SELECT city_id,city FROM ".TABLE_CITIES." WHERE country_id='".$opening_details['country_id']."'");
                    $this->data['cities']=$cities;
                    $this->data['flag']="edit";
            }
            
            //$all_studios = $this->getAll("SELECT user_id,username FROM ".TABLE_USERS." WHERE role_id='".STUDIO_ROLE."' and status_sl=1");
            $users_obj = new Tyr_users_entity();
            $users_obj->role_id = 3;
            $all_studios = $users_obj->get_all_studio();
            $this->data['studios'] = $all_studios;
            
            
            //$get_job_industries = $this->getAll("SELECT industry_id,industry_name from ".TABLE_INDUSTRY);
            $industry_obj = new Tyr_industry_entity();
            $get_job_industries = $industry_obj->get_all_industries();
            $this->data['industries'] = $get_job_industries;
            
            //$get_job_type = $this->getAll("SELECT job_type_id,job_type FROM " . TABLE_JOB_TYPE);
            $job_type_obj = new Tyr_job_type_entity();
            $get_job_type = $job_type_obj->get_all_job_types();
       
            $this->data['get_job_type'] = $get_job_type;
            $content = "contents/admin_opening_form";
            $this->template_arr = array($content);
            $this->load_template();
    }

    public function saveopening(){
        $studio_id=$this->input->post('studio_id');
        $job_id = $this->input->post('job_id');
        $job_title = $this->input->post('job_title');
        $job_desc = $this->input->post('job_desc');
        $job_desc = nl2br($job_desc);
        $job_industry = $this->input->post('job_industry');
        $job_country = $this->input->post('job_country');
        $job_city = $this->input->post('job_city');
        $job_startdate = $this->input->post('job_startdate');
        $job_startdate = str_replace('-', '/', $job_startdate);
        $job_startdate = strtotime($job_startdate);
        $job_enddate = $this->input->post('job_enddate');
        $job_enddate = str_replace('-', '/', $job_enddate);
        $job_enddate = strtotime($job_enddate);
        $job_type = $this->input->post('job_type');
        $job_level = $this->input->post('job_level');
        $job_skills = $this->input->post('job_tags');
        $flag = $this->input->post('flag');
        $job_details = array(
            'job_title' => addslashes($job_title),
            'country_id' => addslashes($this->input->post("job_country")),
            'job_level_id' => addslashes($job_level),
            'start_date' => $job_startdate,
            'end_date' => $job_enddate,
            'job_description' => addslashes($job_desc),
            'job_city' => addslashes($job_city),
            'job_type' => addslashes($job_type),
            'status_sl' => '1',
            'industry_id' => addslashes($job_industry)
        );
        if($flag=="edit")
        {
            //echo 'in edit';
//            $where = "job_id ='" . $job_id . "'";
//            $job_details['user_id'] = $studio_id;
//            $job_details['modified_timestamp'] = strtotime("now");
//            $return_job = $this->update(TABLE_JOBS, $job_details, $where);
            
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $job_id;
            $jobs_obj->user_id = $studio_id;
            $jobs_obj->job_title = $this->input->post("job_title");
            $jobs_obj->country_id = $this->input->post("job_country");
            $jobs_obj->job_level_id = $this->input->post("job_level");
            $jobs_obj->start_date = $job_startdate;
            $jobs_obj->end_date = $job_enddate;
            $jobs_obj->job_description = $this->input->post("job_desc");
            $jobs_obj->job_city = $this->input->post("job_city");
            $jobs_obj->job_type = $job_type;
            $jobs_obj->status_sl = 1;
            $jobs_obj->industry_id = $this->input->post("job_industry");
            $jobs_obj->modified_timestamp = $this->input->post("modified_timestamp");
            $jobs_obj->update_jobs();
            
            $skills = explode(",", $job_skills);
            
            foreach ($skills as $skill) {
                //matchin that the skill is already exist in databse if not then insert new
                $creative_skills_obj = new Tyr_creative_skills_entity();
                $creative_skills_obj->creative_skill = $skill;
                $creative_skills_obj->get_creative_skill();
                $getskill = (array)$creative_skills_obj;
                
                //$getskill = $this->getRow("SELECT creative_skill_id,creative_skill FROM " . TABLE_CREATIVE_SKILLS . " WHERE creative_skill='" . $skill . "'");
                if ($getskill['creative_skill_id'] == 0) {
                    $creative_skills_obj->creative_skill = $skill;
                    $creative_skills_obj->save_creative_skills();
                    $return_skill = $creative_skills_obj->creative_skill_id;
                    
                    //$return_skill = $this->insert(TABLE_CREATIVE_SKILLS, $creative_skill);
                    
//                    $job_skill_values = array(
//                        'studio_id' => $studio_id,
//                        'creative_skill_id' => $return_skill,
//                        'job_id' => $job_id
//                    );
//                    $return_job_skills[] = $this->insert(TABLE_JOB_SKILLS, $job_skill_values);
                    
                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $studio_id;
                    $job_skills_obj->creative_skill_id = $return_skill;
                    $job_skills_obj->job_id = $job_id;
                    $job_skills_obj->save_job_skills();
                    $return_job_skills[] = $job_skills_obj->creative_skill_id;
                    
                } else {

                    $creative_skill_id = $getskill['creative_skill_id'];
//                    $job_skill_values = array(
//                        'studio_id' => $studio_id,
//                        'creative_skill_id' => $creative_skill_id,
//                        'job_id' => $job_id
//                    );
//                    $return_job_skills[] = $this->insert(TABLE_JOB_SKILLS, $job_skill_values);
                    $job_skills_obj = new Tyr_job_skills_entity();
                    $job_skills_obj->studio_id = $studio_id;
                    $job_skills_obj->creative_skill_id = $creative_skill_id;
                    $job_skills_obj->job_id = $job_id;
                    $job_skills_obj->save_job_skills();
                    $return_job_skills[] = $job_skills_obj->creative_skill_id;
                }

            }
            //preparing the html for returning with json
//            $record = $this->getRow("SELECT job_id," . TABLE_JOBS . ".user_id,category_id," . TABLE_JOBS . ".job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
//                    " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city," . TABLE_USERS . ".parent_id," . TABLE_USERS . ".username,".TABLE_COMPANY.".company_name,
//                      end_date,".TABLE_USERS.".email,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl,"
//                    .TABLE_INDUSTRY.".industry_name FROM " . TABLE_JOBS . "
//            LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
//            LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//            LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//            LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
//            LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
//            INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
//                      where job_id='".$job_id."'");
            
            $joins_obj = new Tyr_joins_entity();
            $record = $joins_obj->get_job_details_by_job_id_1($job_id);
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 3;
            $sl = $job_detail_obj->get_count_for_job_id_status();
            
            //$sl = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $job_id . "' AND job_status_id='3'");
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $studio_id;
            $users_obj->status_sl = 1;
            $team = $users_obj->get_studio_team_count();
            
            //$team = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_USERS." WHERE parent_id='" . $job_id . "' AND status_sl='1'");
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 1;
            $cands = $job_detail_obj->get_count_for_job_id_status();
            
            //$cands = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $job_id . "' AND job_status_id='1'");
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $job_id;
            $job_detail_obj->job_status_id = 2;
            $app = $job_detail_obj->get_count_for_job_id_status();
            
            //$app = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $job_id . "' AND job_status_id='2'");
            $html ='<td><input type="checkbox" name="bulk_check[]" id="opening_check" class="opening_check"
                               value="'.$record['job_id'] .'"/>'.$record['job_id'].'</td>
                    <td>'.$record['username'].'<br>'.$record['industry_name'].'</td>
                    <td>'.$record['job_title'].'</td>
                    <td><a HREF="#">'.$record['email'].'</a> </td>
                    <td>'.date('M j,Y',$record['created_timestamp']).'</td>
                    <td>'.$team['cnt'].'</td>
                    <td>'.$app['cnt'].'</td>
                    <td>'.$cands['cnt'].'</td>
                    <td>'.$sl['cnt'].'</td>
                    <td class="table-center-text"><a href="javascript:void(0)" class="btn_pop" id="'.$record['job_id'] .'">Edit</a> <a href="javascript:void(0)" class="delete_opening" id="'.$record['job_id'] .'">Delete</a></td>';
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_UPDATE_SUCCESS,'flag'=>'edit' , 'html' =>$html ,'id' =>$job_id));
        } else {
            //echo 'in new';
            $job_details['created_timestamp'] = strtotime("now");
            $job_details['user_id'] = $studio_id;
            //$return_job = $this->insert(TABLE_JOBS, $job_details);
            
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $studio_id;
            $jobs_obj->job_title = $this->input->post("job_title");
            $jobs_obj->country_id = $this->input->post("job_country");
            $jobs_obj->job_level_id = $this->input->post("job_level");
            $jobs_obj->start_date = $job_startdate;
            $jobs_obj->end_date = $job_enddate;
            $jobs_obj->job_description = $this->input->post("job_desc");
            $jobs_obj->job_city = $this->input->post("job_city");
            $jobs_obj->job_type = $job_type;
            $jobs_obj->status_sl = 1;
            $jobs_obj->industry_id = $this->input->post("job_industry");
            $jobs_obj->modified_timestamp = $this->input->post("modified_timestamp");
            $jobs_obj->modified_timestamp = $this->input->post("modified_timestamp");
            $jobs_obj->save_jobs();
            
            $return_job = $jobs_obj->job_id;
            $job_id = $jobs_obj->job_id;
            $skills = explode(",", $job_skills);
                foreach ($skills as $skill) {
                    //matchin that the skill is already exist in databse if not then insert new
                    //$getskill = $this->getRow("SELECT creative_skill_id,creative_skill FROM " . TABLE_CREATIVE_SKILLS . " WHERE creative_skill='" . $skill . "'");
                    
                    $creative_skills_obj = new Tyr_creative_skills_entity();
                    $creative_skills_obj->creative_skill = $skill;
                    $creative_skills_obj->get_creative_skill();
                    $getskill = (array)$creative_skills_obj;
                    
                    if ($getskill['creative_skill_id'] == 0) {
                        
                        $creative_skills_obj->creative_skill = $skill;
                        $creative_skills_obj->save_creative_skills();
                        $return_skill = $creative_skills_obj->creative_skill_id;

                        $job_skills_obj = new Tyr_job_skills_entity();
                        $job_skills_obj->studio_id = $studio_id;
                        $job_skills_obj->creative_skill_id = $return_skill;
                        $job_skills_obj->job_id = $job_id;
                        $job_skills_obj->save_job_skills();
                        $return_job_skills[] = $job_skills_obj->creative_skill_id;
                        
                        //$creative_skill = array('creative_skill' => $skill);
                        //$return_skill = $this->insert(TABLE_CREATIVE_SKILLS, $creative_skill);
//                        $job_skill_values = array(
//                            'studio_id' => $studio_id,
//                            'creative_skill_id' => $return_skill,
//                            'job_id' => $return_job
//                        );
//                        $return_job_skills[] = $this->insert(TABLE_JOB_SKILLS, $job_skill_values);
                    } else {
                        
                        $creative_skill_id = $getskill['creative_skill_id'];
                        $job_skills_obj = new Tyr_job_skills_entity();
                        $job_skills_obj->studio_id = $studio_id;
                        $job_skills_obj->creative_skill_id = $creative_skill_id;
                        $job_skills_obj->job_id = $job_id;
                        $job_skills_obj->save_job_skills();
                        $return_job_skills[] = $job_skills_obj->creative_skill_id;
//                        $creative_skill_id = $getskill['creative_skill_id'];
//                        $job_skill_values = array(
//                            'studio_id' => $studio_id,
//                            'creative_skill_id' => $creative_skill_id,
//                            'job_id' => $return_job
//                        );
//                        $return_job_skills[] = $this->insert(TABLE_JOB_SKILLS, $job_skill_values);
                    }
                }
            //preparing the html for returning with json
//            $record = $this->getRow("SELECT job_id," . TABLE_JOBS . ".user_id,category_id," . TABLE_JOBS . ".job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
//                    " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city," . TABLE_USERS . ".parent_id," . TABLE_USERS . ".username,".TABLE_COMPANY.".company_name,
//                      end_date,".TABLE_USERS.".email,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl,"
//                    .TABLE_INDUSTRY.".industry_name FROM " . TABLE_JOBS . "
//            LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
//            LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//            LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
//            LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
//            LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
//            INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
//                      where job_id='".$return_job."'");
//            $sl = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $return_job . "' AND job_status_id='3'");
//            $team = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_USERS." WHERE parent_id='" . $return_job . "' AND status_sl='1'");
//            $cands = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $return_job . "' AND job_status_id='1'");
//            $app = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $return_job . "' AND job_status_id='2'");
            
            $joins_obj = new Tyr_joins_entity();
            $record = $joins_obj->get_job_details_by_job_id_1($return_job);
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $return_job;
            $job_detail_obj->job_status_id = 3;
            $sl = $job_detail_obj->get_count_for_job_id_status();
            
            //$sl = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $job_id . "' AND job_status_id='3'");
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $studio_id;
            $users_obj->status_sl = 1;
            $team = $users_obj->get_studio_team_count();
            
            //$team = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_USERS." WHERE parent_id='" . $job_id . "' AND status_sl='1'");
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $return_job;
            $job_detail_obj->job_status_id = 1;
            $cands = $job_detail_obj->get_count_for_job_id_status();
            
            //$cands = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_JOB_DETAIL." WHERE job_id='" . $job_id . "' AND job_status_id='1'");
            
            $job_detail_obj = new Tyr_job_detail_entity();
            $job_detail_obj->job_id = $return_job;
            $job_detail_obj->job_status_id = 2;
            $app = $job_detail_obj->get_count_for_job_id_status();
            
            $html ='<tr id="job_id_'.$record['job_id'].'">
                    <td><input type="checkbox" name="bulk_check[]" id="opening_check" class="opening_check"
                               value="'.$record['job_id'] .'"/>'.$record['job_id'] .'</td>
                    <td>'.$record['username'].'<br>'.$record['industry_name'].'</td>
                    <td>'.$record['job_title'].'</td>
                    <td><a HREF="#">'.$record['email'].'</a> </td>
                    <td>'.date('M j,Y',$record['created_timestamp']).'</td>
                    <td>'.$team['cnt'].'</td>
                    <td>'.$app['cnt'].'</td>
                    <td>'.$cands['cnt'].'</td>
                    <td>'.$sl['cnt'].'</td>
                    <td class="table-center-text"><a href="javascript:void(0)" class="btn_pop" id="'.$record['job_id'] .'">Edit</a> <a href="javascript:void(0)" class="delete_opening" id="'.$record['job_id'] .'">Delete</a></td>
                    </tr>';
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS , 'flag' => 'new' , 'html' => $html));
        }
    }

    public function delete_opening()
    {
        $delete_id = $this->input->post('delete_id');
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->job_id = $delete_id;
        $jobs_obj->status_sl = -1;
        $result = $jobs_obj->update_job_status_by_id();
        
        
//        $status =array(
//            'status_sl' => -1
//        );
//        $result =$this->update(TABLE_JOBS,$status,array('job_id' => $delete_id));
        if($result)
        {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        }
        else
        {
            echo json_encode(array('success' => false));
        }
    }

    public function bulkaction(){
        $delete_ids = $this->input->post('bulk_check');
//        $status =array(
//                    'status_sl' => -1
//        );
       foreach($delete_ids as $id)
        {
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->job_id = $id;
            $jobs_obj->status_sl = -1;
            $result = $jobs_obj->update_job_status_by_id();
           
         //$result = $this->update(TABLE_JOBS,$status,array('user_id' => $id));
        }
        if($result)
        {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS, 'json_idz'=>$delete_ids));
        }
        else
        {
            echo json_encode(array('success' => false));
        }
    }


    public function filter_openings()
    {

    }

}
