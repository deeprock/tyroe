<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_pageviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_role_mapping_entity' . EXT);

class Home extends Tyroe_Controller
{
    public function Home()
    {
        parent::__construct();
        //error_reporting(-1);
    }

    public function index()
    {
        if($this->session->userdata('submitType')){
            if($this->session->userdata('role_id') != 2){
                $this->data['unWantedApplier'] = MESSAGE_UNWANTEDAPPLIER;
            }
        }
        $this->_add_css('user_dashboard.css');
        //redirect to job from invitation just for reviewer
        if($this->session->userdata('redirect_to_job') == 'yes' && $this->session->userdata('role_id') == 4 ){
            $invite_job_id = $this->session->userdata('invite_job_id');
            $invite_studio_id = $this->session->userdata('invite_studio_id');
            $invite_user_id   = $this->session->userdata('user_id');
            //check already exists
            $reviewer_jobs = new Tyr_reviewer_jobs_entity();
            $reviewer_jobs->reviewer_id = $invite_user_id;
            $reviewer_jobs->job_id = $invite_job_id;
            $reviewer_jobs->status_sl = 1;
            $reviewer_jobs->get_reviewer_exist();
            
            if($reviewer_jobs->rev_id == 0){
                $reviewer_jobs = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs->reviewer_id = $invite_user_id;
                $reviewer_jobs->job_id = $invite_job_id;
                $reviewer_jobs->studio_id = $invite_studio_id;
                $reviewer_jobs->status_sl = 1;
                $reviewer_jobs->get_reviewer_exist();
                
                //gettting job_name
                $jobs_obj = new Tyr_jobs_entity();
                $jobs_obj->job_id = $invite_job_id;
                $jobs_obj->get_jobs();
                $this->session->set_userdata('redirect_to_job','');
                redirect($this->getURL('openings').'/'.str_replace(' ','-',$jobs_obj->job_title).'-'.$this->generate_job_number($invite_job_id));
            }

        }

        //redirect tyroe to a job
        if($this->session->userdata('submitType')){
            if($this->session->userdata('role_id') == 2){
                $job_title = $this->cleanString($this->session->userdata('apply_job_title'));
                $job_id = $this->generate_job_number($this->session->userdata('apply_job_id'));
                redirect($this->getURL('opening').'/'.$job_title.'-'.$job_id);
            }
            $this->session->set_userdata('submitType','');
        }


        if($this->session->userdata('role_id') ==1){
            $week_start  = strtolower(date('D')) =='mon'?time():strtotime('last monday');
            $week_start_unix   = strtotime(date('F j Y 00:00:00',$week_start));
            $week_end  = strtolower(date('D')) =='sun'?time():strtotime('next sunday');
            $week_end_unix = strtotime(date('F j Y 23:59:59',$week_end ));
            $current_year_start = strtotime(date('Y-01-01 00:00:00'));
            $current_year_end = strtotime(date('Y-12-31 23:59:59'));
            $current_day_start = strtotime(date('Y-M-d 00:00:00'));
            $current_day_end = strtotime(date('Y-M-d 23:59:59'));
            $prev_ten_years = strtotime(date('Y-12-31 23:59:59',strtotime('-9 years')));
            $this->data['prev_ten_years']=$prev_ten_years;
            $this->data['week_start_unix']=$week_start_unix;
            $this->data['week_end_unix']=$week_end_unix;
            $this->data['current_year_start']=$current_year_start;
            $this->data['current_year_end']=$current_year_end;
            $this->data['current_day_start']=$current_day_start;
            $this->data['current_day_end']=$current_day_end;
            $where_in = array(2,3,4);
            $users_obj = new Tyr_users_entity();
            
            $this->data['admin_graph_data'] = $users_obj->get_all_admin_graph_data($where_in,$prev_ten_years,$current_year_end);
            
            //$user_graph_where=array('created_timestamp >='=>$prev_ten_years, 'created_timestamp <='=>$current_year_end,'status_sl'=>'1');
            //$this->data['admin_graph_data'] = $this->db->select('*')->from(TABLE_USERS)->where($user_graph_where)->where_in('role_id',$where_in)->get()->result_array();
            
            //$user_graph_where=array('created_timestamp >='=>$current_year_start, 'created_timestamp <='=>$current_year_end,'status_sl'=>'1');
            //$user_graph_where=array('created_timestamp >='=>$last_five_years, 'created_timestamp <='=>$next_five_years,'status_sl'=>'1');
            //echo $this->db->last_query(); die;
        }

        //error_reporting(-1);
        /*$this->session->userdata['profiletheme_data']['text'] = "#800080";
        print_r($this->session->userdata['profiletheme_data']['text']);*/
        $this->data['page_title'] = 'Dashboard';
        $user_id = $this->session->userdata("user_id");
        $session_modiule_id = $this->session->userdata('module_id');
        $module_data = array();
        $modules_obj = new Tyr_modules_entity();
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_HOME . ') ');
        } else if ($this->session->userdata('role_id') == TYROE_FREE_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . TYRO_FREE_MODULE_HOME . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_HOME . ') ');
        }
        $whos_roled = '';
        $this->data['profile_score'] = $this->get_profile_score($user_id,"tyroe_id");
        $this->data['profile_review'] = $this->get_profile_review($user_id);
        
        
        $rate_review_obj = new Tyr_rate_review_entity();
        $rate_review_obj->tyroe_id = $user_id;
        $result = $rate_review_obj->get_tyroe_avg_review_score();
        $this->data['tyroe_review_score'] = $result['tyroe_score'];
        #activity drop down
        $activity_type_obj = new Tyr_activity_type_entity();
        $activity_type_obj->status_sl = 1;
        $activity_type_obj->show_in_menu = 1;
        $activity_type = $activity_type_obj->get_all_activity_type_by_role($whos_roled);
        $this->data['get_activity_type'] = $activity_type;
        $this->data['profile_completness'] =$this->get_profile_completeness($user_id);

#Dashboard Top section
        #TYROE TOP SECTION
//        $this->data['profile_viewed'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_PROFILE_VIEW . " WHERE FROM_UNIXTIME(created_timestamp, '%Y-%m-%d') = CURRENT_DATE() AND user_id='" . $user_id . "'");
        
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->user_id = $user_id;
        $type_in = "'3','4'";
        $this->data['reviewer_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type_IN($type_in);
        
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->user_id = $user_id;
        $profile_view_obj->viewer_type = 3;
        $this->data['studio_viewed'] = $profile_view_obj->get_viewer_id_count_by_viewer_type();
        
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->status_sl = 1;
        $jobs_obj->archived_status = 0;
        $this->data['invited_openings'] = $jobs_obj->get_job_id_count_by_status();
       
        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $user_id;
        $this->data['reviews'] = $rating_profile_obj->get_profile_rating_count();
        

        #STUDIO/REVIEWER TOP SECTION
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 1;
        $this->data['total_members'] = $users_obj->get_total_members_count();
        
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 1;
        $this->data['week_members'] = $users_obj->get_total_members_count();
        
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->status_sl = 1;
        $jobs_obj->archived_status = 0;
        $jobs_obj->user_id = $user_id;
        $this->data['total_openings'] = $jobs_obj->get_jobs_count_by_user();
        
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 1;
        $this->data['total_reviewers'] = $users_obj->get_total_reviewers_count_where_role_not(); 
        

        #ADMIN TOP SECTION
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 1;
        $users_obj->status_sl = 1;
        $roles = implode(',',array(2,3,4,5));
        $this->data['total_users'] = $users_obj->get_total_users_count_where_role_in($roles);
        /*$this->data['this_week_users'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_USERS . "
                                            WHERE role_id IN (2,3,4,5)
                                            AND status_sl='1'
                                            AND (FROM_UNIXTIME(created_timestamp) >= DATE_SUB(CURDATE(), INTERVAL 6 DAY))");*/

        //jobs_in_progress
        $current_ex_time = strtotime(date('Y-m-d, h:i:s A'));
        
        $jobs_obj_in_prg = new Tyr_jobs_entity();
        $jobs_obj_in_prg->status_sl = 1;
        $jobs_obj_in_prg->archived_status = 1;
        $jobs_obj_in_prg->end_date = $current_ex_time;
        $this->data['jobs_in_progress'] = $jobs_obj_in_prg->get_jobs_in_progress();
        
        //echo $this->db->last_query();
        $users_obj_role = new Tyr_users_entity();
        $role_ids = array(4);
        $this->data['admin_companies'] = $users_obj_role->get_count_admin_companies($role_ids);
        
        $jobs_obj_cont_admin = new Tyr_jobs_entity();
        $jobs_obj_cont_admin->status_sl = 1;
        $this->data['admin_reviews'] = $jobs_obj_cont_admin->get_cont_admin_reviews();

        $this->data['studio_name'] = $this->session->userdata('user_name');
#Dashboard Top section End

        #Dashboard job listing for studio and reviewer
        //
        
        $job_roles_mapping_obj = new Tyr_job_role_mapping_entity();
        $job_roles_mapping_obj->user_id = $user_id;
        $job_roles_mapping_obj->is_admin = 1;
        $job_roles_mapping_obj->is_reviewer = 1;
        $my_jobs_array = $job_roles_mapping_obj->get_user_job_ids();
        $my_jobs = $my_jobs_array;
        if(is_null($my_jobs['job_id'])){
            $my_jobs['job_id'] = '-1';
        }
        if ($this->session->userdata("role_id") == '4'|| $this->session->userdata("role_id") == '3') {
            $where = " and job_id IN(" .$my_jobs['job_id'] . ")";
            //$where = " and user_id='" . $this->session->userdata("parent_id") . "'";
        }  else {
            $where = "";
        }
        
        $joins_obj_all_jobs = new Tyr_joins_entity();
        $get_jobs = $joins_obj_all_jobs->get_all_jobs_details($where);
        
        //$this->data['job_detail'] = $get_jobs;
        //getting donute chart pending and reviewed
        if($this->session->userdata("role_id") == '3'){
            //total collaborators
            
            $parent_id = $this->session->userdata('parent_id');
            if(intval($parent_id) == 0){
                $parent_id = $user_id;
            }
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $parent_id;
            $users_obj->status_sl = 1;
            $this->data['collaborators'] = $users_obj->get_count_of_collaborators();
          
//            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
//            $reviewer_jobs_obj->studio_id = $parent_id;
//            $total_reviewer = $reviewer_jobs_obj->get_total_reviewers_count_for_studio();

            $jobs_details = new Tyr_job_detail_entity();
            $jobs_details->reviewer_id = $parent_id;
            $jobs_details->job_status_id = 1;
            $total_tyroes = $jobs_details->get_total_tyroes_from_job_details();
            
            $pending_tyroes  = 0;
            $reviewed_tyroes = 0;
            $total_candidate = 0;
            
            foreach($total_tyroes as $total_tyroe){
                
                $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                $reviewer_reviews_obj->studio_id = $parent_id;
                $reviewer_reviews_obj->tyroe_id = $total_tyroe['tyroe_id'];
                $reviewer_reviews_obj->job_id = $total_tyroe['job_id'];
                $reviewer_reviews_obj->status_sl = 1;
                $get_riview = $reviewer_reviews_obj->get_tyroe_reviews_count();

                $reviewer_jobs = new Tyr_reviewer_jobs_entity();
                $reviewer_jobs->job_id = $total_tyroe['job_id'];
                $reviewer_jobs->status_sl = 1;
                $get_total_job_reviewer = $reviewer_jobs->get_total_reviewers_count_for_job();

                if($get_riview['tyr_review'] == $get_total_job_reviewer['total_reviewers'] ){
                    if($get_riview['tyroe_id'] != ''){
                        $reviewed_tyroes++;
                    }
                }else{
                    if($get_riview['tyroe_id'] != ''){
                        $pending_tyroes++;
                    }
                }
                $total_candidate++;

            }
            $this->data['get_total_reviewed'] = $reviewed_tyroes;
            $this->data['get_total_candidate']  = $total_candidate - $reviewed_tyroes;
        } else if($this->session->userdata("role_id") == '4'){
//            
//            $parent_id = $this->session->userdata('parent_id');
//            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
//            $reviewer_jobs_obj->studio_id = $parent_id;
//            $reviewer_jobs_obj->reviewer_id = $user_id;
//            $total_job_ids = $reviewer_jobs_obj->get_total_job_ids();
//            
//            
//            if($total_job_ids['total_job_ids'] == null){ $total_job_ids['total_job_ids'] = -1;}
            
            $parent_id = $this->session->userdata('parent_id');
            if(intval($parent_id) == 0){
                $parent_id = $user_id;
            }
            
            $jobs_details = new Tyr_job_detail_entity();
            $jobs_details->reviewer_id = $parent_id;
            $jobs_details->job_status_id = 1;
            $total_tyroes = $jobs_details->get_total_tyroes_by_job_id($my_jobs['job_id']);

            $pending_tyroes  = 0;
            $reviewed_tyroes = 0;
            $total_candidate = 0;
            foreach($total_tyroes as $total_tyroe){
                
                $reviewer_reviews_obj = new Tyr_reviewer_reviews_entity();
                $reviewer_reviews_obj->reviewer_id = $user_id;
                $reviewer_reviews_obj->tyroe_id = $total_tyroe['tyroe_id'];
                $reviewer_reviews_obj->job_id = $total_tyroe['job_id'];
                $reviewer_reviews_obj->studio_id = $parent_id;
                $reviewer_reviews_obj->status_sl = 1;
                $get_riview = $reviewer_reviews_obj->get_tyroe_reviews_count();
                
                if($get_riview['tyr_review'] == 1){
                    $reviewed_tyroes++;
                }else{
                    $pending_tyroes++;
                }
                $total_candidate++;

            }

            $this->data['get_total_reviewed']   = $reviewed_tyroes;
            $this->data['get_total_candidate']  = $pending_tyroes;
        }
        //End getting donute chart pending and reviewed
        //getting studio admin dashboard top counters
        if($this->session->userdata("role_id") == '3'){
            //get team members admin
            $joins_obj = new Tyr_joins_entity();
            $get_team_admin = $joins_obj->get_team_admin($user_id);
            $this->data['team_admin'] = $get_team_admin;
             //end get team admin

//            $jobs_obj = new Tyr_jobs_entity();
//            $jobs_obj->user_id = $user_id;
//            $jobs_obj->status_sl = 1;
//            $jobs_obj->archived_status = 0;
//            $total_active_job = $jobs_obj->get_active_job_count();
            $this->data['get_active_jobs'] = $my_jobs['job_id'] == '-1' ? 0 : count(@explode(",",$my_jobs['job_id']));
            
        }else if($this->session->userdata("role_id") == '4'){
//            $reviewer_jobs_obj = new Tyr_reviewer_jobs_entity();
//            $reviewer_jobs_obj->status_sl = 1;
//            $reviewer_jobs_obj->studio_id = $this->session->userdata("parent_id");
//            $reviewer_jobs_obj->reviewer_id = $user_id;
//            $total_active_job = $reviewer_jobs_obj->get_total_active_job_count();
            $this->data['get_active_jobs'] = $my_jobs['job_id'] == '-1' ? 0 : count(@explode(",",$my_jobs['job_id']));
        }

        $users_obj_all_tyroe = new Tyr_users_entity();
        $get_all_tyroes = $users_obj_all_tyroe->get_all_tyroe_order_by_created_time();
  
        $total_new_tyroe_in_this_month = 0;
        if(is_array($get_all_tyroes)){
            foreach($get_all_tyroes as $get_all_tyroe){
                if(date('m',$get_all_tyroe['created_timestamp']) == date('m',strtotime('now'))){
                    $total_new_tyroe_in_this_month++;
                }
            }
        }
        $this->data['total_new_tyroe_in_this_month'] = $total_new_tyroe_in_this_month;
        //end getting studio admin dashboard top counters
        //GETTING TOTAL JOB OF STUDIO
//        $jobs_obj_count = new Tyr_jobs_entity();
//        $jobs_obj_count->user_id = $user_id;
//        $jobs_obj_count->status_sl = 1;
//        $jobs_obj_count->archived_status = 0;
//        $this->data['total_jobs'] = $jobs_obj_count->get_jobs_count_by_user();
        
        //Getting total tyroes
        $users_obj_count_by_role = new Tyr_users_entity();
        $users_obj_count_by_role->role_id = 2;
        $users_obj_count_by_role->status_sl = 1;
        $this->data['total_tyroes'] = $users_obj_count_by_role->get_total_tyroe_count_by_role();

        if($this->session->userdata("role_id") == '3'){
            //getting_pending_reviewer
            
            $parent_id = $this->session->userdata('parent_id');
            if(intval($parent_id) == 0){
                $parent_id = $user_id;
            }
            
            $users_obj = new Tyr_users_entity();
            $users_obj->parent_id = $parent_id;
            $users_obj->status_sl = 1;
            $this->data['pending_reviewer'] = $users_obj->get_pending_reviewer_count();
            $this->session->set_userdata('pending_reviewer',$this->data['pending_reviewer']['pending_reviewer']);
        }
        $all_jobs = array();
        if(is_array($get_jobs)){
            foreach ($get_jobs as $jobs) {
                $reviewer_studio = $this->session->userdata("parent_id");
                $hidden_obj_1 = new Tyr_hidden_entity();
                $hidden_obj_1->studio_id = $reviewer_studio;
                $hidden_obj_1->job_id = $jobs['job_id'];
                $hiden_tyroes = $hidden_obj_1->get_hidden_tyroes();

                $hidden_user = 0;
                if (is_array($hiden_tyroes)) {
                    $hidden_user = $hiden_tyroes['user_id'];
                }

                $job_details_obj_1 = new Tyr_job_detail_entity();
                $job_details_obj_1->job_id = $jobs['job_id'];
                $job_details_obj_1->job_status_id = -1;
                $job_details_obj_1->status_sl = 1;
                $job_details_obj_1->invitation_status = 1;
                $candidate_count = $job_details_obj_1->get_candidate_count_except_hidden($hidden_user);

                $job_details_obj_2 = new Tyr_job_detail_entity();
                $job_details_obj_2->job_id = $jobs['job_id'];
                $job_details_obj_2->job_status_id = 2;
                $job_details_obj_2->status_sl = 1;
                $job_details_obj_2->invitation_status = 0;
                $applicant_count = $job_details_obj_2->get_applicant_count_except_hidden($hidden_user);

                $job_details_obj_3 = new Tyr_job_detail_entity();
                $job_details_obj_3->job_id = $jobs['job_id'];
                $job_details_obj_3->job_status_id = 3;
                $job_details_obj_3->status_sl = 1;
                $job_details_obj_3->invitation_status = 1;
                $shortlist_count = $job_details_obj_3->get_shortlist_count_except_hidden($hidden_user);

                //Count number of total newest candidate , applicaint, and shortlist
                $job_details_obj_4 = new Tyr_job_detail_entity();
                $job_details_obj_4->job_id = $jobs['job_id'];
                $job_details_obj_4->job_status_id = -1;
                $job_details_obj_4->status_sl = 1;
                $job_details_obj_4->invitation_status = 1;
                $job_details_obj_4->newest_candidate_status = 0;
                $newest_candidate_count = $job_details_obj_4->get_new_candidate_count_except_hidden($hidden_user);


                $job_details_obj_5 = new Tyr_job_detail_entity();
                $job_details_obj_5->job_id = $jobs['job_id'];
                $job_details_obj_5->job_status_id = 2;
                $job_details_obj_5->status_sl = 1;
                $job_details_obj_5->invitation_status = 0;
                $job_details_obj_5->newest_candidate_status = 0;
                $newest_applicant_count = $job_details_obj_5->get_new_applicant_count_except_hidden($hidden_user);


                $job_details_obj_6 = new Tyr_job_detail_entity();
                $job_details_obj_6->job_id = $jobs['job_id'];
                $job_details_obj_6->job_status_id = 3;
                $job_details_obj_6->status_sl = 1;
                $job_details_obj_6->invitation_status = 1;
                $job_details_obj_6->newest_candidate_status = 0;
                $newest_shortlist_count = $job_details_obj_6->get_new_shortlist_count_except_hidden($hidden_user);

                $hidden_obj = new Tyr_hidden_entity();
                $hidden_obj->job_id = $jobs['job_id'];
                $hidden_count = $hidden_obj->get_total_hidden_count_by_job();

                $dd_now = $this->dateDifference($jobs['start_date'],$jobs['end_date']);

                $all_jobs[] = array("job_id" => $jobs['job_id'],
                    "user_id" => $jobs['user_id'],
                    "category_id" => $jobs['category_id'],
                    "job_title" => $jobs['job_title'],
                    "job_description" => $jobs['job_description'],
                    "job_location" => $jobs['job_location'],
                    "job_skill" => $jobs['job_skill'],
                    "start_date" => $jobs['start_date'],
                    "end_date" => $jobs['end_date'],
                    "job_quantity" => $jobs['job_quantity'],
                    "job_type" => $jobs['job_type'],
                    "total_candidates"=>$candidate_count['total_candidates'],
                    "total_reviewer" => $reviewer_count['total_reviewer'],
                    "total_applicants" => $applicant_count['total_applicants'],
                    "total_newest_candidates" =>$newest_candidate_count['total_newest_candidates'],
                    "total_newest_applicants" =>$newest_applicant_count['total_newest_applicants'],
                    "total_newest_shortlist" =>$newest_shortlist_count['total_newest_shortlist'],
                    "total_shortlist" => $shortlist_count['total_shortlist'],
                    "total_hidden" => $hidden_count['total_hidden'],
                    "dd_now" => $dd_now
                );
            }
        }
        $this->data['job_detail'] = $all_jobs;


        #Dashboard job listing end
        #dashboard New tyroe Listing FOR STUDIO AND REVIEWER
        $parent_id = $this->session->userdata('parent_id');
        if(intval($parent_id) == 0){
            $parent_id = $user_id;
        }
        
        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $this->data['has_profile_access'] = $profile_access_obj->id == 0 ? 0 : 1;
            
        $hidden_obj = new Tyr_hidden_entity();
        $hidden_obj->studio_id = $parent_id;
        $hidden_obj->status_sl = 1;
        $hiden_tyroes = $hidden_obj->get_hidden_tyroes_by_studio_id();
        $hidden_user = 0;
        if (is_array($hiden_tyroes)) {
            $hidden_user = $hiden_tyroes['tyroe_id'];
        }
        $joins_obj = new Tyr_joins_entity();
        $get_candidate = $joins_obj->get_top_candidate_details($hidden_user);
        if($get_candidate != ''){
          for ($i = 0; $i < count($get_candidate); $i++) {
                $joins_obj = new Tyr_joins_entity();
                $get_candidate[$i]['skills'] = $joins_obj->get_all_user_skill($get_candidate[$i]['user_id']);

                #Portfolio/Media
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $get_candidate[$i]['user_id'];
                $get_candidate[$i]['portfolio'] = $media_obj->get_user_short_portfolio();
                /**get tyroe total score**/

                /*FEATURED TYROE - TRIANGLE*/

                $tyr_featured_tyroes = new Tyr_featured_tyroes_entity();
                $tyr_featured_tyroes->featured_tyroe_id = $get_candidate[$i]['user_id'];
                $tyr_featured_tyroes->get_user_featured_status();
                $get_candidate[$i]['is_user_featured'] = $tyr_featured_tyroes->featured_status;
                /*FEATURED TYROE - TRIANGLE*/

                /*Check is favourite or not*/

                $favourite_profile = new Tyr_favourite_profile_entity();
                $favourite_profile->user_id = $get_candidate[$i]['user_id'];
                $favourite_profile->studio_id = $parent_id;
                $favourite_profile->check_user_favourite();
                $get_candidate[$i]['is_favourite'] = $favourite_profile->fav_profile_id;
                /*End Checking favourite*/

                /**Getting tyroe jobs**/
                $job_details_obj = new Tyr_job_detail_entity();
                $job_details_obj->tyroe_id = $get_candidate[$i]['user_id'];
                $tyroe_jobs = $job_details_obj->get_tyroe_job_ids();
                $get_candidate[$i]['tyroe_jobs'] = $tyroe_jobs;
                /**End getting tyroe jobs**/

                $rate_review_obj = new Tyr_rate_review_entity();
                $rate_review_obj->tyroe_id = $get_candidate[$i]['user_id'];
                $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();

                if($get_tyroes_total_score['tyroe_score'] == NULL){
                    $get_tyroes_total_score['tyroe_score'] = 0;
                }
                $get_candidate[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                /**end get tyroe total score**/
            }  
        }
        
        $this->data['get_candidate']=$get_candidate;

        #dashboard new tyroe listing end
        $where_activity = '';
        if ($this->session->userdata("role_id") == '2') {
            $where_activity = "performer_id='" . $user_id . "'";
        } else {
            $where_activity = "performer_id='" . $user_id . "'";
        }
        ///VISIT QUERY START
        
        $start_day_unixtime = strtotime(date('Y-m-d 00:00:00'));
        $end_day_unixtime = strtotime(date('Y-m-d 23:59:00'));
        $pageviews_obj = new Tyr_pageviews_entity();
        $this->data['visits_today'] = $pageviews_obj->get_todays_pageview_count($start_day_unixtime,$end_day_unixtime);
        
        
        //echo $this->db->last_query();
        ///VISIT QUERY END
        //All TYROE USERS QUERY START
        //$this->data['members_this_week'] =$this->db->select('count(*) AS members_this_week')->from(TABLE_USERS)->where(array('created_timestamp >='=>$last_monday))->get()->row_array();
        //echo $this->db->last_query();
        //All TYROE USERS QUERY END

        $this->data['job_activity'] = $this->get_activity_stream($where_activity,"ORDER BY job_activity_id DESC ","LIMIT 10 OFFSET 0");

        $this->data['acc'] = $this->progress_bar('acc');
        $this->data['profile'] = $this->progress_bar('profile');
        $this->data['resume'] = $this->progress_bar('resume');
        $this->data['portfolio'] = $this->progress_bar('portfolio');
        $this->data['subscription'] = $this->progress_bar('subscription');
        $this->data['notification'] = $this->progress_bar('notification');
        $this->data['socialmedia'] = $this->progress_bar('socialmedia');


        /*Add to opening dropdown*/
        if($this->session->userdata("role_id") == 3 || $this->session->userdata("role_id") == 4){
//            $parent_id = $this->session->userdata('parent_id');
//            if(intval($parent_id) == 0){
//                $parent_id = $user_id;
//            }
            $jobs_obj = new Tyr_jobs_entity();
            $jobs_obj->user_id = $user_id;
            $jobs_obj->archived_status = 0;
            $jobs_obj->status_sl = 1;
            $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
        }
        /*Add to opening dropdown*/

        /*$this->data['job_activity'] = $this->getAll("SELECT job_activity_id," . TABLE_JOB_ACTIVITY . ".user_id,job_id,activity_data,
        activity_type," . TABLE_JOB_ACTIVITY . ".created_timestamp," . TABLE_USERS . ".username
        FROM " . TABLE_JOB_ACTIVITY . "
        INNER JOIN  " . TABLE_USERS . " ON " . TABLE_JOB_ACTIVITY . ".user_id=" . TABLE_USERS . ".user_id
        WHERE " . TABLE_JOB_ACTIVITY . ".user_id='" . $user_id . "' order by job_activity_id DESC");*/
        
        $users_obj_1 = new Tyr_users_entity();
        $users_obj_1->role_id = 2;
        $users_obj_1->user_id = $user_id;
        $this->data['count_all_tyroes'] = $users_obj_1->count_all_tyroes();
        
        $profile_view_obj_1 = new Tyr_profile_view_entity();
        $profile_view_obj_1->user_id = $user_id;
        $this->data['month_chart'] = $profile_view_obj_1->get_profile_view_month_chart();

        $this->data['month_chart_others'] = $profile_view_obj_1->get_profile_view_month_chart_other();

        /*echo "SELECT
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='01',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '1',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='02',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '2',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='03',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '3',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='04',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '4',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='05',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '5',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='06',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '6',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='07',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '7',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='08',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '8',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='09',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '9',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='10',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '10',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='11',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '11',
        IF(MONTH(FROM_UNIXTIME(p.created_timestamp))='12',ROUND( COUNT(MONTH(FROM_UNIXTIME(p.created_timestamp)))/COUNT(p.user_id) ),0) AS '12'
        FROM `tyr_profile_view` p WHERE p.user_id!='" . $user_id . "' AND YEAR(FROM_UNIXTIME(p.created_timestamp)) = YEAR(CURDATE())
        ";die;*/

        $this->data['week_chart'] = $profile_view_obj->get_profile_view_week_chart();
        
            //echo $this->db->last_query();
        $this->data['week_chart_others'] = $profile_view_obj->get_profile_view_week_chart_other();

        if ($this->session->userdata('role_id') == '1') {
            
            $joins_obj = new Tyr_joins_entity();
            $tyroe_featured_approval = $joins_obj->get_all_tyroe_featured_approval();
            $this->data['tyroe_featured_approval'] = $tyroe_featured_approval;
            
            //print_array($this->db->last_query(),false,'$this-db->last_query()');
            //print_array($tyroe_featured,false,'$tyroe_featured;');

            $studios_feature_waiting_where = array('tu.status_sl'=>0, 'tu.role_id'=>3);
            /*$studios_featured = $this->db->select('*, tu.user_id AS tu_user_id')
                    ->from(TABLE_USERS.' AS tu')
                    ->join(TABLE_PAGEVIEWS.' as tpv ','tpv.user_id = tu.user_id','LEFT OUTER')
                    ->join(TABLE_COUNTRIES.' as trc ','trc.country_id = tu.country_id ','LEFT OUTER')
                    ->where($studios_feature_waiting_where)
                    ->group_by('tu.user_id')
                    ->order_by('tu.user_id','desc')
                    ->limit(5)
                    ->get()->result_array();*/
            $where = '';
            $order_by = " ORDER BY st.user_id DESC ";
            $limit = " LIMIT 5 ";
            $studios_featured = $joins_obj->get_all_studios_featured_approval($where,$order_by,$limit,STUDIO_ROLE);
            $this->data['studios_featured_approval'] = $studios_featured;
            
            //print_array($this->db->last_query(),false,'$this-db->last_query()');
            //print_array($tyroe_featured,false,'$tyroe_featured;');
            
            $users_obj = new Tyr_users_entity();
            $this->data['accumulate_tyroes'] = $users_obj->get_accumulate_tyroes_count();
            $this->data['accumulate_studios_reviewers'] = $users_obj->get_accumulate_studios_reviewers_count();
            $this->data['accumulate_jobs'] = $users_obj->get_accumulate_jobs_count();
            $this->template_arr = array('header', 'contents/admin_dashboard', 'footer');
        } else {
            $this->template_arr = array('header', 'contents/dashboard', 'footer');
        }
        $this->load_template();
    }

    public function progress_bar($data)
    {
        $user_id = $this->session->userdata("user_id");
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        switch ($data) {
            case 'acc':
                $acc_count = $users_obj->get_progress_bar_account_per();
                return $acc_count['count_acc'];
                break;
            case 'subscription':
                return 80;
                break;
            case 'notification':
                return 59;
                break;
            case 'profile':
                $profile_count = $users_obj->get_progress_bar_profile_per();
                return $profile_count['count_acc'];
                break;
            case 'socialmedia':
                return 35;
                break;
            case 'resume':
                $resume_count = $users_obj->get_progress_bar_resume_per();
                return $resume_count['total_count'];
                break;
            case 'portfolio':
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $user_id;
                $potfolio_count = $media_obj->get_progressbar_portfolio_per();
                return $potfolio_count['profile'];
                break;
        }
    }

    public function getInfo()
    {
        $this->data['page_title'] = 'Personal Information';
        $this->data['profile_updated'] = "";
        $user_id = $this->session->userdata("user_id");
        #Profile Details
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_personal_info($user_id);
        
        $image = '';
        if ($get_user['media_name']) {
            $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH . "img/no-image.jpg";
        }

        $user_data = array(
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "password" => $get_user['password'],
            "image" => $image,
            "password_len" => $get_user['password_len'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "phone" => $get_user['phone'],
            "cellphone" => $get_user['cellphone'],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id']
        );
        $this->data['get_user'] = $user_data;
        $this->template_arr = array('header', 'contents/personal_info', 'footer');
        $this->load_template();
    }
}
