<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_refrences_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendation_entity' . EXT);
require_once(APPPATH . 'entities/tyr_professional_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_other_site_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_sites_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_social_link_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendemail_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_rel_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_favourite_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);

class Searchpublicprofile extends Tyroe_Controller {

    public function Searchpublicprofile() {
        parent::__construct('searchpublicprofile');
        $this->load->library('user_agent');

        if ($this->data['access_allowed']) {
            //$user_id = '127';
        } else {
            if ($this->session->userdata('role_id') != ADMIN_ROLE) {
                $this->get_system_modules(' AND module_id IN (' . PROFILE_SUB_MENU . ') ');
            } else {
                $this->get_system_modules();
            }
            $this->load->library('form_validation');
            $this->addslashesToPost();
        }
    }

    public function index() {
        
        $parent_id = $this->session->userdata("user_id");
        if (intval($this->session->userdata("parent_id")) != 0) {
            $parent_id = $this->session->userdata("parent_id");
        }

        $profile_access_obj = new Tyr_studio_profile_access_entity();
        $profile_access_obj->studio_id = $parent_id;
        $profile_access_obj->get_studio_profile_access();
        
        $job_candidate_profile = 0;
        if(isset($_POST['job_candidate_profile']) && $_POST['job_candidate_profile'] == 1)
            $job_candidate_profile = 1;
        
        if ($profile_access_obj->id != 0 || $this->session->userdata('role_id') == 1 || $job_candidate_profile == 1) {
            $profile_view = $this->input->post("profile");
            if ($this->uri->segment(3)) {
                $current_userid = $this->uri->segment(3);
            } else {
                $current_userid = $this->input->post("current_userid");
            }
            $this->data['current_userid'] = $current_userid;
            //this code for job overview and request from opening overview
            if ($this->session->userdata("role_id") == 4) {

                $page_position_text = $this->input->post('page_position_text');
                $active_pages_tab = $this->input->post('active_pages_tab');
                $this->data['show_add_review_controls'] = '';
                if ($active_pages_tab != '' && $page_position_text == 'candidate') {

                    if ($active_pages_tab == 'Pending') {
                        $this->data['show_add_review_controls'] = 'yes';
                    } else if ($active_pages_tab == 'Review') {
                        //get review of tyroe by job
                        $job_id = $this->input->post('job_id') . 'job_id';
                        $tyroe_id = $current_userid;
                        $review_id = $this->session->userdata('user_id');

                        $rate_review_obj = new Tyr_rate_review_entity();
                        $rate_review_obj->user_id = $review_id;
                        $rate_review_obj->job_id = $job_id;
                        $rate_review_obj->tyroe_id = $tyroe_id;
                        $rate_review_obj->status_sl = 1;
                        $tyroe_review = $rate_review_obj->get_tyroe_score_total_1();
                        $this->data['tyroe_review'] = $tyroe_review;

                        //$tyroe_review = $this->getRow("SELECT * , ((SUM(`technical`)+SUM(`creative`)+SUM(`impression`))/3) AS review_score  FROM " . TABLE_RATE_REVIEW . " WHERE user_id = '".$review_id."' AND job_id = '".$job_id."' AND tyroe_id = '".$tyroe_id."' AND status_sl = 1");
                        //$this->data['tyroe_review'] = $tyroe_review;
                    }
                }
            }
            //End code for job overview and request from opening overview

            $user_id = $current_userid;
            $publish_account = "users.user_id='" . $user_id . "'";

            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();

            //$get_account = $this->getRow("SELECT COUNT(*) AS cnt FROM " . TABLE_USERS . " users  WHERE ".$publish_account);


            if ($users_obj->user_id > 0) {
                #Profile Details

                $joins_obj = new Tyr_joins_entity();
                $get_user = $joins_obj->get_user_detail_info($user_id);


//          $get_user = $this->getRow("SELECT job_level.level_title,exp_years.experienceyear,industry.industry_name,users.user_id,users.city AS city_id,users.job_level_id,users.extra_title,
//                  users.industry_id,users.experienceyear_id,role_id,tyroe_level,parent_id,users.country_id,country.country,profile_url,email,
//                   username,password,password_len,firstname,lastname,phone,fax,cellphone,address,cities.city,user_occupation,user_biography,user_web,user_experience,
//                   state,zip,suburb,display_name,availability,publish_account,created_timestamp,modified_timestamp, users.status_sl,
//                    country.country, media.media_type,media.media_name,
//                      media.image_id FROM " . TABLE_USERS . " users
//                 LEFT JOIN " . TABLE_COUNTRIES . " country ON users.country_id=country.country_id
//                 LEFT JOIN " . TABLE_MEDIA . " media ON users.user_id=media.user_id AND media.profile_image = '1' AND media.status_sl='1'
//                 LEFT JOIN " . TABLE_CITIES . " cities ON users.city=cities.city_id
//                 LEFT JOIN " . TABLE_JOB_LEVEL . " job_level ON users.job_level_id=job_level.job_level_id
//                 LEFT JOIN " . TABLE_INDUSTRY . " industry ON users.industry_id=industry.industry_id
//                 LEFT JOIN " . TABLE_EXPERIENCE_YEARS . " exp_years ON users.experienceyear_id=exp_years.experienceyear_id
//                 WHERE ". $publish_account ."ORDER BY image_id DESC LIMIT 0 ,1");


                $user_id = $get_user['user_id'];

                $this->data['page_title'] = $get_user['username'];
                $this->data['get_states'] = array();
                if ($get_user['country_id'] != "") {
                    $states_obj = new Tyr_states_entity();
                    $states_obj->country_id = $get_user['country_id'];
                    $get_states = $states_obj->get_all_states_by_country();
                    $this->data['get_states'] = $get_states;
                }

                //$get_states = $this->getAll("SELECT states_id,states_name FROM " . TABLE_STATES . " Where 1=1 " . $where_country_c . " ORDER BY states_name ASC");
                //$this->data['get_states'] = $get_states;
//          $tyroe_levels = $this->getAll("SELECT tyroe_level_id,tyroe_level_title FROM " . TABLE_PROFESSIONAL_LEVEL . "");
//          $this->data['tyroe_levels'] = $tyroe_levels;

                $professional_level_obj = new Tyr_professional_level_entity();
                $tyroe_levels = $professional_level_obj->get_all_professional_level();
                $this->data['tyroe_levels'] = $tyroe_levels;

//          $other_site = $this->getAll("SELECT other_site_id,other_site FROM " . TABLE_OTHER_SITES . "");
//          $this->data['other_site'] = $other_site;

                $other_site_obj = new Tyr_other_site_entity();
                $other_site = $other_site_obj->get_all_other_site();
                $this->data['other_site'] = $other_site;

                $image;
                if ($get_user['media_name']) {
                    $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_user['media_name'];
                } else {
                    $image = ASSETS_PATH_NO_IMAGE;
                }

                $phone1 = explode("-", $get_user['cellphone']);
                $user_data = array(
                    "user_id" => $get_user['user_id'],
                    "email" => $get_user['email'],
                    "username" => $get_user['username'],
                    "firstname" => $get_user['firstname'],
                    "extra_title" => $get_user['extra_title'],
                    "lastname" => $get_user['lastname'],
                    "profile_url" => $get_user['profile_url'],
                    "user_biography" => $get_user['user_biography'],
                    "user_occupation" => $get_user['user_occupation'],
                    "user_web" => $get_user['user_web'],
                    "user_experience" => $get_user['user_experience'],
                    "image" => $image,
                    "image_id" => $get_user['image_id'],
                    "password_len" => $get_user['password_len'],
                    "state" => $get_user['state'],
                    "city" => $get_user['city'],
                    "zip" => $get_user['zip'],
                    "address" => $get_user['address'],
                    "cellphone_code" => $phone1[0],
                    "cellphone" => $phone1[1],
                    "country" => $get_user['country'],
                    "country_id" => $get_user['country_id'],
                    "tyroe_level" => $get_user['tyroe_level'],
                    "city_id" => $get_user['city_id'],
                    "level_title" => $get_user['level_title'],
                    "experienceyear" => $get_user['experienceyear'],
                    "industry_name" => $get_user['industry_name'],
                    "job_level_id" => $get_user['job_level_id'],
                    "industry_id" => $get_user['industry_id'],
                    "availability" => $get_user['availability'],
                    "experienceyear_id" => $get_user['experienceyear_id']
                );
                $this->data['get_user'] = $user_data;


                /* mysite data */
                //$mystite = $this->getAll("SELECT * FROM " . TABLE_USER_SITES . " WHERE user_id = '{$user_id}'");
                //$this->data['get_my_sites'] = $mystite;

                $user_sites_obj = new Tyr_user_sites_entity();
                $user_sites_obj->user_id = $user_id;
                $mystite = $user_sites_obj->get_all_user_sites();
                $this->data['get_my_sites'] = $mystite;

                $login_history_obj = new Tyr_login_history_entity();
                $login_history_obj->user_id = $user_id;
                $login_history = $login_history_obj->get_count_login_history_by_user_id();

                //$login_history = $this->getRow("SELECT COUNT(*) as cnt FROM ".TABLE_LOGIN_HISTORY." WHERE user_id='{$user_id}'");
                $this->data['login_history'] = $login_history;



                $this->data['msg'] = "";
                $this->data['user_id'] = $user_id;
                $this->data['firstname'] = $this->data['get_user']["firstname"];
                $this->data['lastname'] = $this->data['get_user']["lastname"];
                $this->data['user_biography'] = $this->data['get_user']["user_biography"];
                $this->data['availability'] = $this->data['get_user']["availability"];
                $this->data['industry_name'] = $this->data['get_user']["industry_name"];
                $this->data['extra_title'] = $this->data['get_user']["extra_title"];
                $this->data['job_title'] = $this->data['get_user']["extra_title"];
                $this->data['country'] = $this->data['get_user']["country"];
                $this->data['city'] = $this->data['get_user']["city"];
                $this->data['level'] = $this->data['get_user']["level_title"];
                $this->data['experienceyear'] = $this->data['get_user']["experienceyear"];
                $this->data['get_countries_dropdown'] = $this->dropdown($this->data['get_countries'], $this->data['get_user']["country_id"], array('name' => 'country', 'placeholder' => 'Country', 'onchange' => 'getCities(this)'));

                //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM ".TABLE_CITIES." WHERE country_id='".$this->data['get_user']["country_id"]."'");
                //$this->data['get_cities_dropdown'] =$this->dropdown($cities,$this->data['get_user']["city_id"],array('name'=>'city','placeholder'=>'City'));
                $cities_obj = new Tyr_cities_entity();
                $cities_obj->country_id = $this->data['get_user']["country_id"];
                $cities = $cities_obj->get_all_cities_by_country_id();
                $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City'));


                //$get_industry = $this->getAll("SELECT industry_id AS id,industry_name AS name FROM " . TABLE_INDUSTRY);
                //$this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"],array('name'=>'industry','placeholder'=>'Select Industry'));
                $industry_obj = new Tyr_industry_entity();
                $get_industry = $industry_obj->get_all_industries();
                $this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"], array('name' => 'industry', 'placeholder' => 'Select Industry'));


                //$get_experience_years = $this->getAll("SELECT experienceyear_id AS id,experienceyear AS name FROM " . TABLE_EXPERIENCE_YEARS);
                //$this->data['get_experience_years'] = $this->dropdown($get_experience_years,$this->data['get_user']["experienceyear_id"],array('name'=>'experience_level','placeholder'=>'Years'));
                //$get_job_level = $this->getAll("SELECT job_level_id AS id,level_title AS name FROM " . TABLE_JOB_LEVEL);
                //$this->data['get_job_level'] = $this->dropdown($get_job_level,$this->data['get_user']["job_level_id"],array('name'=>'year','placeholder'=>'Experience Level'));

                $job_level_obj = new Tyr_job_level_entity();
                $get_job_level = $job_level_obj->get_all_job_level();
                $this->data['get_levels'] = $get_job_level;
                $this->data['get_job_level'] = $this->dropdown($get_job_level, $this->data['get_user']["job_level_id"], array('name' => 'year', 'placeholder' => 'Experience Level'));

                $experience_years_obj = new Tyr_experience_years_entity();
                $get_experience_years = $experience_years_obj->get_all_experience_years();
                $this->data['get_experience'] = $get_experience_years;
                $this->data['get_experience_years'] = $this->dropdown($get_experience_years, $this->data['get_user']["experienceyear_id"], array('name' => 'experience_level', 'placeholder' => 'Years'));



                //--------------------------Social link start-------------------------------------------------------
//          $social = $this->getAll("SELECT social_id, social_type,user_id,social_link,modified_timestamp,created_timestamp,status_sl
//                  FROM " . TABLE_SOCIAL_LINKS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//          $this->data['get_social_link'] = $social;

                $social_link = new Tyr_social_link_entity();
                $social_link->user_id = $user_id;
                $social = $social_link->get_user_all_social_link();
                $this->data['get_social_link'] = $social;

                //--------------------------Social link  END-------------------------------------------------------
                /* Check is favourite or not */
                //$is_favourite = $this->getRow("SELECT fav_profile_id FROM ".TABLE_FAVOURITE_PROFILE." WHERE user_id = '".$user_id."' AND studio_id = '".$this->session->userdata('user_id')."' AND status_sl = 1");
                //$this->data['is_favourite'] = $is_favourite['fav_profile_id'];

                $favourite_profile_obj = new Tyr_favourite_profile_entity();
                $favourite_profile_obj->user_id = $current_userid;
                $favourite_profile_obj->studio_id = $this->session->userdata('user_id');
                $favourite_profile_obj->check_user_favourite();
                $this->data['is_favourite'] = $favourite_profile_obj->fav_profile_id;
                /* End Checking favourite */

                //--------------------------Experience Start-------------------------------------------------------
//          $experience = $this->getAll("SELECT exp_id, user_id, company_name,job_title,job_location,IF(job_start='',YEAR(CURDATE()) ,job_start) AS job_start,
//          IF(job_end='',YEAR(CURDATE()) ,job_end) AS job_end, job_description, job_url, job_country, job_city,
//                    current_job, created,modified,status_sl  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $user_id . "' and status_sl='1'
//                    ");
//          $this->data['get_experience'] = $experience;
                $experience_obj = new Tyr_experience_entity();
                $experience_obj->user_id = $user_id;
                $experience_obj->status_sl = 1;
                $experience = $experience_obj->get_user_all_experience();
                $this->data['get_experience'] = $experience;

                //--------------------------Experience END-------------------------------------------------------
                //--------------------------Education Start-------------------------------------------------------
//          $education = $this->getAll("SELECT education_id, user_id,institute_name,edu_organization,edu_position ,degree_title,field_interest,grade_obtain,IF(start_year='',YEAR(CURDATE()) ,start_year) as start_year,
//           IF(end_year='',YEAR(CURDATE()) ,end_year) as end_year,activities,education_description,created,modified,status_sl
//          FROM " . TABLE_EDUCATION . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//          $this->data['get_education'] = $education;

                $education_obj = new Tyr_education_entity();
                $education_obj->user_id = $user_id;
                $education_obj->status_sl = 1;
                $education = $education_obj->get_user_all_education();
                $this->data['get_education'] = $education;

                //--------------------------Education END-------------------------------------------------------
                //--------------------------Awards Start-------------------------------------------------------
//          $awards = $this->getAll("SELECT award_id, user_id,award,award_organization,award_description,award_year,created_timestamp, modified,  status_sl
//                  FROM " . TABLE_AWARDS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//          $this->data['get_awards'] = $awards;
                $awards_obj = new Tyr_awards_entity();
                $awards_obj->user_id = $user_id;
                $awards_obj->status_sl = 1;
                $awards = $awards_obj->get_user_all_award();
                $this->data['get_awards'] = $awards;

                //--------------------------Awards END-------------------------------------------------------
                //--------------------------Skills Start-------------------------------------------------------
//                  $skills = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
//                      FROM " . TABLE_CREATIVE_SKILLS . " csk
//                      LEFT JOIN " .TABLE_SKILLS  . " sk  ON (sk.`skill` = csk.creative_skill_id)
//                      WHERE sk.user_id='" . $user_id . "' and sk.status_sl='1' ");
//                  $this->data['get_skills'] = $skills;

                $joins_obj = new Tyr_joins_entity();
                $skills = $joins_obj->get_all_skill_by_users($user_id);
                $this->data['get_skills'] = $skills;

                //--------------------------Skills End-------------------------------------------------------




                /*                 * Getting tyroe jobs* */
                $job_detail_obj = new Tyr_job_detail_entity();
                $job_detail_obj->tyroe_id = $user_id;
                $tyroe_jobs = $job_detail_obj->get_tyroe_job_ids();
                //$tyroe_jobs = $this->getAll("SELECt job_id FROM ".TABLE_JOB_DETAIL." WHERE tyroe_id = '".$user_id."'");
                $this->data['tyroe_jobs'] = $tyroe_jobs;
                /*                 * End getting tyroe jobs* */

                //--------------------------Recommendation Approve Start-------------------------------------------------------
//            $recommendation = $this->getAll("SELECT rec.status_sl,rec.recommend_dept,rec.recommendation,rec.recommend_name,rec.recommend_status,rec.recommend_company,rec.recommend_id,rec.recommend_email
//                    FROM ".TABLE_RECOMMENDATION." rec
//                    WHERE rec.status_sl='1' AND rec.user_id='".$user_id ."' AND rec.recommend_status='1'
//                    ");
//            $this->data['get_recommendation_approved_list'] = $recommendation;
//
                $recommendation_obj_2 = new Tyr_recommendation_entity();
                $recommendation_obj_2->user_id = $user_id;
                $recommendation_obj_2->recommend_status = 1;
                $recommendation_approvedlist = $recommendation_obj_2->get_all_recommendation_approvallist();
                $this->data['get_recommendation_approved_list'] = $recommendation_approvedlist;

                //--------------------------Recommendation Approve END-------------------------------------------------------
                //--------------------------Portfolio Video Start-------------------------------------------------------

                /* $video = $this->getRow("SELECT image_id, media_name, media_title, media_description,video_type,media_url,media_type,modified
                  FROM " . TABLE_MEDIA . "
                  WHERE profile_image!= '1' AND status_sl='1' AND media_type = 'video'  AND user_id = '" . $user_id . "'");
                  $this->data['video'] = $video; */

//          $video = $this->getRow("SELECT image_id, media_name, media_title, media_description,media_tag,video_type,media_url,media_type,modified
//                                                      FROM " . TABLE_MEDIA . "
//                                                      WHERE profile_image!= '1' AND status_sl='1' AND (media_type = 'video' OR media_featured='1')  AND user_id = '" . $user_id . "'");
                $media_obj_1 = new Tyr_media_entity();
                $media_obj_1->user_id = $user_id;
                $media_obj_1->media_featured = 1;
                $media_obj_1->profile_image = 1;
                $media_obj_1->status_sl = 1;
                $media_obj_1->media_type = 'video';
                $video = $media_obj_1->get_all_user_media_by_featured_or_type();
                $this->data['video'] = $video;

                //$this->data['video'] = $video;
                //--------------------------Portfolio Video END-------------------------------------------------------
//          $this->data['latestwork_portfolioimages'] = $this->getAll("SELECT *
//                                                              FROM tyr_media WHERE  user_id = '" . $user_id . "'
//                                                              AND status_sl = '1'  AND media_type = 'image' AND profile_image!='1' AND media_featured='0'
//                                                              ORDER BY media_sortorder ASC LIMIT 0,20");
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $user_id;
                $media_obj->media_featured = 0;
                $media_obj->profile_image = 1;
                $media_obj->status_sl = 1;
                $media_obj->media_type = 'image';
                $latestwork_portfolioimages = $media_obj->get_user_latest_portfolio_image();
                $this->data['latestwork_portfolioimages'] = $latestwork_portfolioimages;
                
                $media_obj_12 = new Tyr_media_entity();
                $media_obj_12->user_id = $user_id;
                $media_obj_12->status_sl = 1;
                $media_obj_12->media_type = 'image';
                $media_obj_12->media_featured = 0;
                $media_obj_12->profile_image = 0;
                $this->data['latestwork_portfolioimages_count'] = $media_obj_12->get_all_user_media_count_1();


                //---------------------------------------Latest Work Images End--------------------------------------
                //--------------------------Portfolio Images END-------------------------------------------------------
                //--------------------------Visibility Start-------------------------------------------------------
//          $visibility = $this->getAll("SELECT visiblity_id, user_id,visible_section,visibility, status_sl,created_date,modified_date
//                         FROM " . TABLE_VISIBLITY_OPTIONS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
//                 $this->data['get_visibility'] = $visibility;

                $visibility_options_obj = new Tyr_visibility_options_entity();
                $visibility_options_obj->user_id = $user_id;
                $visibility_options_obj->status_sl = 1;
                $visibility = $visibility_options_obj->get_all_user_visibility_options();
                ;
                $this->data['get_visibility'] = $visibility;

                //--------------------------Visibility END-------------------------------------------------------
                //------------------------Custome Theme Apply Start--------------//
//              $custom_theme = $this->getRow("SELECT profile_theme, featured_theme, gallery_theme, resume_theme, footer_theme
//                      FROM " . TABLE_CUSTOM_THEME . " ct
//                      WHERE ct.user_id='" . $get_user['user_id'] . "' and ct.status_sl='1' ");

                $custome_theme_obj = new Tyr_custome_theme_entity();
                $custome_theme_obj->user_id = $user_id;
                $custome_theme_obj->status_sl = 1;
                $custome_theme_obj->get_custome_theme_by_user_id();
                $custom_theme = (array) $custome_theme_obj;

                $profile_theme = unserialize(stripslashes($custom_theme['profile_theme']));
                $featured_theme = unserialize(stripslashes($custom_theme['featured_theme']));
                $gallery_theme = unserialize(stripslashes($custom_theme['gallery_theme']));
                $resume_theme = unserialize(stripslashes($custom_theme['resume_theme']));
                $footer_theme = unserialize(stripslashes($custom_theme['footer_theme']));

                $this->data['profile_theme'] = $profile_theme;
                $this->data['featured_theme'] = $featured_theme;
                $this->data['gallery_theme'] = $gallery_theme;
                $this->data['resume_theme'] = $resume_theme;
                $this->data['footer_theme'] = $footer_theme;

                if ($this->session->userdata("role_id") == 3) {
                    $jobs_obj = new Tyr_jobs_entity();
                    $jobs_obj->user_id = $this->session->userdata('user_id');
                    $jobs_obj->status_sl = 1;
                    $jobs_obj->archived_status = 0;
                    $this->data['get_openings_dropdown'] = $jobs_obj->get_jobs_openings_by_user();
                    //$this->data['get_openings_dropdown'] = $this->getAll("SELECT job_id, job_title FROM tyr_jobs WHERE user_id = '".$this->session->userdata('user_id')."' and status_sl = 1 AND archived_status = 0");
                } else if ($this->session->userdata("role_id") == 4) {

                    $studio_id = $this->session->userdata('parent_id');
                    $reviewer_id = $this->session->userdata('user_id');
                    $joins_obj = new Tyr_joins_entity();
                    $this->data['get_openings_dropdown'] = $joins_obj->get_jobs_openings_listing($studio_id, $reviewer_id);
                    //$this->data['get_openings_dropdown'] = $this->getAll("SELECT ".TABLE_JOBS.".job_id, ".TABLE_JOBS.".job_title FROM ".TABLE_REVIEWER_JOBS." LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".`job_id` = ".TABLE_REVIEWER_JOBS.".`job_id` WHERE ".TABLE_REVIEWER_JOBS.".`reviewer_id` = '".$this->session->userdata('user_id')."' AND ".TABLE_REVIEWER_JOBS.".studio_id = '".$this->session->userdata('parent_id')."'");
                }             //------------------------Custome Theme Apply End--------------//


                $this->template_arr = array('contents/search/search_view_profile');
                if ($this->session->userdata('role_id') != '1') {
                    //$this->template_arr = array('header','contents/edit_profile3');
                    // view profile by  viewer entry
                    $viewer_data = array(
                        "viewer_id" => $this->session->userdata('user_id'),
                        "viewer_type" => $this->session->userdata('role_id'),
                        "job_id" => "0",
                        "user_id" => $current_userid,
                        "created_timestamp" => time()
                    );
                    //$this->insert(TABLE_PROFILE_VIEW,$viewer_data);

                    $profile_view_obj = new Tyr_profile_view_entity();
                    $profile_view_obj->viewer_id = $this->session->userdata('user_id');
                    $profile_view_obj->viewer_type = $this->session->userdata('role_id');
                    $profile_view_obj->job_id = 0;
                    $profile_view_obj->user_id = $current_userid;
                    $profile_view_obj->created_timestamp = time();
                    $profile_view_obj->save_profile_view();


                    /* Email To Tyroe and Insert Stream on behalf of Tyore */
                    if ($this->session->userdata('role_id') == '3') {
                        $on = "ON u.user_id = c.studio_id";
                    } else if ($this->session->userdata('role_id') == '4') {
                        $on = "ON u.parent_id = c.studio_id";
                    }
                    //print_array($this->db->last_query(),true);
//                  $email_data = $this->getRow("SELECT u.firstname,u.lastname,u.job_title, c.company_id, c.company_name
//                                                FROM tyr_users u
//                                                LEFT JOIN tyr_company_detail c ".$on."
//                                                WHERE u.user_id = '".$this->session->userdata('user_id')."'");
                    $joins_obj = new Tyr_joins_entity();
                    $email_data = $joins_obj->get_email_data_for_tyroe_1($on, $this->session->userdata('user_id'));

                    #Stream

                    $activity_data = array(
                        "job_title" => $email_data['job_title'],
                        "company_name" => $email_data['company_name']
                    );

//                  $job_activity = array(
//                      'performer_id' => $current_userid,
//                      'job_id' => 0,
//                      'object_id' => $current_userid,
//                      'activity_type' => LABEL_PROFILE_VIEW_JOB_LOG,
//                      'activity_data' => serialize($activity_data),
//                      'created_timestamp' => strtotime("now"),
//                      'status_sl' => '1'
//                  );
//                  $this->insert(TABLE_JOB_ACTIVITY, $job_activity);

                    $job_activity_obj = new Tyr_job_activity_entity();
                    $job_activity_obj->performer_id = $current_userid;
                    $job_activity_obj->job_id = 0;
                    $job_activity_obj->object_id = $current_userid;
                    $job_activity_obj->activity_type = LABEL_PROFILE_VIEW_JOB_LOG;
                    $job_activity_obj->activity_data = serialize($activity_data);
                    $job_activity_obj->status_sl = 1;
                    $job_activity_obj->created_timestamp = strtotime("now");
                    $job_activity_obj->save_job_activity();


                    $schedule_notification_obj = new Tyr_schedule_notification_entity();
                    $schedule_notification_obj->user_id = $current_userid;
                    $schedule_notification_obj->option_id = 15;
                    $check_notification_profile_status = $schedule_notification_obj->get_schedule_notification_by_user_option();

                    //$check_notification_profile_status = $this->getRow("SELECT status_sl FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$current_userid."' AND option_id = 17");


                    if ($check_notification_profile_status['status_sl'] == 1) {
                        #Email
                        $viewer_name = $email_data['firstname'] . ' ' . $email_data['lastname'];
                        $viewer_job_title = $email_data['job_title'];
                        $viewer_company_name = $email_data['company_name'];

                        //$get_message = $this->getRow("SELECT subject,template from ".TABLE_NOTIFICATION_TEMPLATE." where notification_id='n05'");
                        $notification_templates_obj = new Tyr_notification_templates_entity();
                        $notification_templates_obj->notification_id = 'n05';
                        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();

                        $subject = $get_message['subject'];
                        $subject = str_replace(array('[$viewer_job_title]', '[$viewer_company_name]', '[$viewer_name]', '[$url_tyroe]'), array($viewer_job_title, $viewer_company_name, $viewer_name, LIVE_SITE_URL), $subject);
                        $message = htmlspecialchars_decode($get_message['template']);
                        $message = str_replace(array('[$viewer_job_title]', '[$viewer_company_name]', '[$viewer_name]', '[$url_tyroe]'), array($viewer_job_title, $viewer_company_name, $viewer_name, LIVE_SITE_URL), $message);
                        $current_user_email = $user_data['email'];
                        $this->email_sender($current_user_email, $subject, $message);
                    }
                }

                /* Email To Tyroe and Insert Stream on behalf of Tyore */
            } else {
                $this->template_arr = array('header_login', 'contents/no_access', 'footer');
            }
        } else {
            $countries_obj = new Tyr_countries_entity();
            $countries = $countries_obj->get_all_countries_as();
            $this->data['countries'] = $countries;
            
            $is_reviewer = 0;
            if ($this->session->userdata('role_id') == '4'){
                $is_reviewer = 1;
            }
            $this->data['is_reviewer'] = $is_reviewer;
            
            $this->template_arr = array('contents/payment/buy_profile_access');
        }

        $this->load_template();
    }

}
