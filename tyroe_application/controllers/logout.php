<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);

class Logout extends Tyroe_Controller
{
    public function Logout()
    {
        parent::__construct();
        //ECHO "D2 ".$this->publish_or_not();DIE;
    }

    public function index()
    {
        if($this->publish_or_not() == '-1'){
            $user_id = $this->session->userdata("user_id");
            $users_obj = new Tyr_users_entity();
            $users_obj->publish_account = 0;
            $users_obj->user_id = $user_id;
            $users_obj->update_publish_account();
        }
        $this->session->sess_destroy();
	 session_destroy();
        header("location:" . $this->getURL("login"));
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */