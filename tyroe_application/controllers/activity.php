<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);

class Activity extends Tyroe_Controller
{
    public function Activity()
    {

        /*****  THIS CONTROLLER IS DEPRECATED  ****/
        parent::__construct();
        $this->data['page_title'] = 'Activity';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STREAM_SUB_MENU . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACTIVITY_SUBMENU . ') ');
        }else{
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
        
        //$activity_type = $this->getAll("SELECT DISTINCT(activity_type) FROM " . TABLE_JOB_ACTIVITY);
        $activity_type_obj = new Tyr_activity_type_entity();
        $activity_type_obj->status_sl = 1;
        $activity_type_obj->show_in_menu = 1;
        $activity_type = $activity_type_obj->get_all_activity_type_by_menu();
        $this->data['get_activity_type'] = $activity_type;

        $user_id = $this->session->userdata("user_id");
        #TYROE TOP SECTION
        //        $this->data['profile_viewed'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_PROFILE_VIEW . " WHERE FROM_UNIXTIME(created_timestamp, '%Y-%m-%d') = CURRENT_DATE() AND user_id='" . $user_id . "'");
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->viewer_type = 4;
        $profile_view_obj->user_id = $user_id;
        $this->data['reviewer_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type();
        
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->viewer_type = 3;
        $profile_view_obj->user_id = $user_id;
        $this->data['studio_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type();
        
        $invite_tyroe = new Tyr_invite_tyroe_entity();
        $invite_tyroe->tyroe_id = $user_id;
        $invite_tyroe->status_sl = 1;
        $this->data['invited_openings'] = $invite_tyroe->get_invite_job_count_by_tyroe();
        
        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $user_id;
        $this->data['reviews'] = $rating_profile_obj->get_rating_count_by_user_id();
        
    }

    public function index()
    {

        /*****  THIS CONTROLLER IS DEPRECATED  ****/

        $user_id = $this->session->userdata("user_id");
        $where_activity;
        if ($this->session->userdata("role_id") == '2') {
            $where_activity = "tyroe_id='" . $user_id . "'";
        } else {
            $where_activity = "user_id='" . $user_id . "'";
        }

        $bool = $this->input->post('bool');
        
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_activity_stream($where_activity);
          
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY review_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();
        $this->data['activity_stream'] = $this->get_activity_stream($where_activity,$order_by,$pagintaion_limit['limit']);
        
        $this->data['pagination'] = $this->pagination(array('url' => 'stream/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('header', 'contents/get_streams', 'footer');
        $this->load_template();
    }

    public function FilterStream()
    {
        /*****  THIS CONTROLLER IS DEPRECATED  ****/

        $user_id = $this->session->userdata("user_id");
        $activity_type = $this->input->post("activity_type");
        $stream_keyword = $this->input->post("stream_keyword");
        $stream_duration = $this->input->post("stream_duration");
        $where_activity;

        if ($this->session->userdata("role_id") == '2') {
            $where_activity = "tyroe_id='" . $user_id . "'";
        } else {
            $where_activity = "user_id='" . $user_id . "'";
        }
        if (!empty($activity_type)) {
            if ($activity_type == "1") {
                $where_activity;
            } else {
                $where_activity .= " AND activity_type='" . $activity_type . "'";
            }
        }
        if (!empty($stream_keyword)) {
            $where_activity .= " AND activity_title LIKE '%" . $stream_keyword . "%'";
        }
        if (!empty($stream_duration) && $stream_duration == "week") {
            $where_activity .= " AND FROM_UNIXTIME(j_act.created_timestamp) >= DATE_SUB(CURDATE(), INTERVAL 6 DAY)";
        } else if (!empty($stream_duration) && $stream_duration == "month") {
            $where_activity .= " AND MONTH(FROM_UNIXTIME(j_act.created_timestamp)) = MONTH(CURDATE())";
        }


        $bool = $this->input->post('bool');

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_activity_stream($where_activity);
        
          
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY job_activity_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();


        $activity_stream = $this->get_activity_stream($where_activity,$order_by,$pagintaion_limit['limit']);

        //$activity_stream =$this->get_activity_stream($where_activity);
        //echo '<pre>';print_r($this->get_activity_stream($where_activity) );die('Call');echo '</pre>';$this->get_activity_stream($where_activity);
        $this->data['activity_stream'] = $activity_stream;
        $this->data['pagination'] = $this->pagination(array('url' => 'stream/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('contents/search/search_streams');
        $this->load_template();
    }


}
