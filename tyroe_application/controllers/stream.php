
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);

class Stream extends Tyroe_Controller
{
    public function Stream()
    {
        parent::__construct();
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->data['page_title'] = 'Activity';
            $this->get_system_modules(' AND module_id IN (' . STREAM_SUB_MENU . ') ');
            $whos_roled = "";
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->data['page_title'] = 'Activity';
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACTIVITY . ') ');
            $whos_roled = "AND (whos_roled = 9 OR whos_roled = 7)";
            //$this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACTIVITY_SUBMENU . ') ');
        } else {
            $this->data['page_title'] = 'Stream';
            $whos_roled = "AND (whos_roled = 9 OR whos_roled = 2)";
            $this->get_system_modules(' AND module_id IN (' . STREAM_SUB_MENU . ') ');
        }
        
        $activity_type_obj = new Tyr_activity_type_entity();
        $activity_type_obj->show_in_menu = 1;
        $activity_type_obj->status_sl = 1;
        $activity_type = $activity_type_obj->get_all_activity_type_by_role($whos_roled);
        $this->data['get_activity_type'] = $activity_type;

        $user_id = $this->session->userdata("user_id");
        #TYROE TOP SECTION
        //        $this->data['profile_viewed'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_PROFILE_VIEW . " WHERE FROM_UNIXTIME(created_timestamp, '%Y-%m-%d') = CURRENT_DATE() AND user_id='" . $user_id . "'");
    
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->viewer_type = 4;
        $profile_view_obj->user_id = $user_id;
        $this->data['reviewer_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type();
        
        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->viewer_type = 3;
        $profile_view_obj->user_id = $user_id;
        $this->data['studio_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type();
        
        $invite_tyroe_obj = new Tyr_invite_tyroe_entity();
        $invite_tyroe_obj->tyroe_id = $user_id;
        $invite_tyroe_obj->status_sl = 1;
        $this->data['invited_openings'] = $invite_tyroe_obj->get_invite_job_count_by_tyroe();
        
        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $user_id;
        $this->data['reviews'] = $rating_profile_obj->get_rating_count_by_user_id();
      }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
        $search_activity_type = $this->input->post("search_activity_type");
        $search_stream_keyword = $this->input->post("search_stream_keyword");
        $search_stream_duration = $this->input->post("search_stream_duration");
        $where_activity='';
        if ($this->session->userdata("role_id") == '2') {
            //$where_activity = "tyroe_id='" . $user_id . "'";
            $where_activity = "performer_id='" . $user_id . "'";
        } else {
            $where_activity = "performer_id='" . $user_id . "' AND activity_type IN (1,2,3,5,6,19,20)";
        }
        if (!empty($search_activity_type) && $search_activity_type !="") {
            $this->data['search_activity_type'] = $search_activity_type;
            if ($search_activity_type == "1") {
                $where_activity.="";
            } else {
                $where_activity .= " AND activity_type='" . $search_activity_type . "'";
                $where_activity;
            }
        }
        if (!empty($search_stream_keyword)) {
            $this->data['search_stream_keyword']=$search_stream_keyword;
            if ($this->session->userdata("role_id") == '2') {
            $where_activity .= " AND act_type.activity_title LIKE '%" . $search_stream_keyword . "%'";
            }else{
            $where_activity .= " AND j_act.activity_data LIKE '%" . $search_stream_keyword . "%'";
            }

        }
        if (!empty($search_stream_duration) && $search_stream_duration == "week") {
            $this->data['search_stream_duration'] =$search_stream_duration;
            $where_activity .= " AND j_act.created_timestamp >= EXTRACT(EPOCH from (CURRENT_TIMESTAMP - interval '6 days'))";
        } else if (!empty($search_stream_duration) && $search_stream_duration == "month") {
            $where_activity .= " AND EXTRACT(MONTH FROM j_act.created_at) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) ";
            $this->data['search_stream_duration'] =$search_stream_duration;
        }

        $bool = $this->input->post('bool');
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_activity_stream($where_activity);
        
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY job_activity_id DESC ";
        }
        $pagination_limit = $this->pagination_limit();
        $joins_obj = new Tyr_joins_entity();

        $activity_stream = $joins_obj->get_activity_stream($where_activity, $order_by, $pagination_limit["limit"]);
        $this->data['activity_stream'] = $activity_stream;
        $this->data['pagination'] = $this->pagination(array('url' => 'stream/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagination_limit['start_page']));
        $this->template_arr = array('header', 'contents/get_streams', 'footer');
        $this->load_template();
    }

    public function FilterStream()
    {
        $user_id = $this->session->userdata("user_id");
        $activity_type = $this->input->post("activity_type");
        $stream_keyword = $this->input->post("stream_keyword");
        $stream_duration = $this->input->post("stream_duration");
        $pagename = $this->input->post("pagename");
        $where_activity = "";
        $job_id = $this->input->post('job_id');
        if ($this->session->userdata("role_id") == '2') {
            $where_activity = "performer_id='" . $user_id . "'";
        } else {
            $where_activity = "performer_id='" . $user_id . "'";
        }

        if($job_id != "")
        {
            $where_activity .= " AND j_act.job_id='".$job_id."'";
        }
        if (!empty($activity_type)) {
            $this->data['search_activity_type'] = $activity_type;
            if ($activity_type == "1") {
                $where_activity .="";
            } else {
                $where_activity .= " AND activity_type='" . $activity_type . "'";
            }
        }
        if (!empty($stream_keyword)) {
            $this->data['search_stream_keyword']= $stream_keyword;
            $where_activity .= " AND j_act.activity_data LIKE '%" . $stream_keyword . "%'";
        }
        
        if (!empty($stream_duration) && $stream_duration == "week") {
            $this->data['search_stream_duration'] =$search_stream_duration;
            $where_activity .= " AND j_act.created_timestamp >= EXTRACT(EPOCH from (CURRENT_TIMESTAMP - interval '6 days'))";
        } else if (!empty($stream_duration) && $stream_duration == "month") {
            $where_activity .= " AND EXTRACT(MONTH FROM j_act.created_at) = EXTRACT(MONTH FROM CURRENT_TIMESTAMP) ";
            $this->data['search_stream_duration'] =$search_stream_duration;
        }

        $bool = $this->input->post('bool');

        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_activity_stream($where_activity);
        
        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY job_activity_id DESC ";
        }
        $pagination_limit = $this->pagination_limit();
        if($pagename == "dashboard"){
            $pagination_limit['limit'] = " LIMIT 0, 10 ";
        }
        
        $joins_obj = new Tyr_joins_entity();
        $activity_stream = $joins_obj->get_activity_stream($where_activity, $order_by, $pagination_limit['limit']);

        $this->data['activity_stream'] = $activity_stream;
        $this->data['pagination'] = $this->pagination(array('url' => 'stream/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagination_limit['start_page']));
        $this->template_arr = array('contents/search/search_streams');
        $this->load_template();
    }


}
