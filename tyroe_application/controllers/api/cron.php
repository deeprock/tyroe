<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_profile_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_studio_job_access_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);

class Cron extends CI_Controller{

    public function Cron() {
        parent::__construct();
        error_reporting(E_ALL);
        if(!$this->input->is_cli_request()){
            //echo "redirect<br/><br/>";
            //return;
            redirect(base_url());
        }
    }
    
    public function index(){
        
    }

    public function studio_profile_access(){
        $today = strtotime("now");
        $studio_profile_access_obj = new Tyr_studio_profile_access_entity();
        $records = $studio_profile_access_obj->get_all_record();
        foreach($records as $rec){
            $created = $rec['created_date'];
            $record_id = $rec['id'];
            $dateDiff = $today - $created;
            $days = floor($dateDiff/(60*60*24));
            if($days > 30){
                $studio_profile_access_obj = new Tyr_studio_profile_access_entity();
                $studio_profile_access_obj->id = $record_id;
                $studio_profile_access_obj->delete_record();
                //echo $days.' :Record Deleted';
            }
        }
    }
    
//    public function insert_profile_access(){
//        $studio_profile_access_obj = new Tyr_studio_profile_access_entity();
//        $studio_profile_access_obj->studio_id = 75;
//        $studio_profile_access_obj->created_date = strtotime('2014-11-03 07:38:16');
//        $studio_profile_access_obj->expiry_date = strtotime('2014-12-01 07:38:16');
//        $studio_profile_access_obj->save_studio_profile_access();
//    }
    
    public function studio_job_access(){
        $today = strtotime("now");
        $studio_job_access_obj = new Tyr_studio_job_access_entity();
        $records = $studio_job_access_obj->get_all_record();
        foreach($records as $rec){
            $created = $rec['created_date'];
            $record_id = $rec['id'];
            $job_id = $rec['job_id'];
            $dateDiff = $today - $created;
            $days = floor($dateDiff/(60*60*24));
            if($days > 30){
                $jobs_obj = new tyr_jobs_entity();
                $jobs_obj->job_id = $job_id;
                $jobs_obj->get_jobs();
                $jobs_obj->archived_status = 1;
                $status = $jobs_obj->update_job_archived_status_by_id();
                if($status){
                    $studio_job_access_obj = new Tyr_studio_job_access_entity();
                    $studio_job_access_obj->id = $record_id;
                    $studio_job_access_obj->delete_record();
                    //echo $status.'   '.$days.' :Record Deleted';
                }
            }
        }
    }
    
//    public function insert_job_access(){
//        $studio_job_access_obj = new Tyr_studio_job_access_entity();
//        $studio_job_access_obj->job_id = 40;
//        $studio_job_access_obj->created_date = strtotime('2014-11-03 07:38:16');
//        $studio_job_access_obj->expiry_date = strtotime('2014-12-01 07:38:16');
//        $studio_job_access_obj->save_studio_job_access();
//    }
}


