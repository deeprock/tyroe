<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rate_review_entity' . EXT);

class Featured extends Tyroe_Controller
{
    function __construct(){
        parent::__construct();
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_FEATURED . ') ');
        } else {
            $this->get_system_modules();
        }
    }

    function index()
        {
            $this->data['page_title'] = 'Featured Approval';
            $this->data['page_number'] = $this->input->post('page');
            
            $joins_obj = new Tyr_joins_entity();
            $total_featured = $joins_obj->get_total_featured_count();
            
//            $total_featured = $this->getRow("SELECT COUNT(u.user_id) as cnt FROM ".TABLE_USERS." u LEFT JOIN ".TABLE_COUNTRIES." c
//            ON c.country_id = u.country_id LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id=f.featured_tyroe_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1' AND u.publish_account = '1' AND
//                f.featured_status='0' ORDER BY user_id DESC ");
            $pagintaion_limit = $this->pagination_limit();

            //echo $current_time;
            //echo $minus_24;
            $joins_get_obj = new Tyr_joins_entity();
            $get_featured_users = $joins_get_obj->get_featured_users_details_with_limit($pagintaion_limit);
            
//            $get_featured_users = $this->getAll("SELECT u.profile_url, u.user_id,u.firstname,u.lastname,u.extra_title,c.country,ci.city,f.featured_status," . TABLE_INDUSTRY . ".industry_name FROM ".TABLE_USERS." u LEFT JOIN ".TABLE_COUNTRIES." c
//            ON c.country_id = u.country_id LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id=f.featured_tyroe_id LEFT JOIN ".TABLE_CITIES." ci ON u.city = ci.city_id LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=u.industry_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1' AND u.publish_account = '1' AND
//            f.featured_status='0' ORDER BY user_id DESC ".$pagintaion_limit['limit']);

            $joins_count_obj = new Tyr_joins_entity();
            $get_featured_users_total_counts =  $joins_count_obj->get_all_featured_users_details();
            
//            $get_featured_users_total_counts = $this->getAll("SELECT u.user_id as tyr_rev_id  FROM ".TABLE_USERS." u LEFT JOIN ".TABLE_COUNTRIES." c
//            ON c.country_id = u.country_id LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id=f.featured_tyroe_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1' AND u.publish_account = '1' AND
//            f.featured_status='0' ORDER BY user_id DESC ");




            //print_array($get_featured_users_total_counts,true,'$get_featured_users_total_counts');
            if ($get_featured_users != NULL) {
                for ($i = 0; $i < count($get_featured_users); $i++) {
                    
                    $joins_obj = new Tyr_joins_entity();    
                    $get_featured_users[$i]['skills'] = $joins_obj->get_all_user_skill($get_featured_users[$i]['user_id']);
                    

//                    $get_featured_users[$i]['skills'] = $this->getAll("SELECT skill_id," . TABLE_CREATIVE_SKILLS . ".creative_skill
//                                                                       FROM " . TABLE_SKILLS . " LEFT JOIN
//                                                                       " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_SKILLS . ".skill
//                                                                       WHERE user_id='" . $get_featured_users[$i]['user_id'] . "' and status_sl=1");
                    #Portfolio/Media
                    $media_obj = new Tyr_media_entity();
                    $media_obj->user_id = $get_featured_users[$i]['user_id'];
                    $get_featured_users[$i]['portfolio'] = $media_obj->get_user_short_portfolio();
                    //$get_featured_users[$i]['portfolio'] = $this->getAll("SELECT * FROM " . TABLE_MEDIA . " WHERE user_id='" . $get_featured_users[$i]['user_id'] . "' AND status_sl='1' AND profile_image='0' AND media_featured!='1' AND media_type='image' ORDER BY image_id DESC LIMIT 3 ");
                    /**get tyroe total score**/
                    
                    $rate_review_obj = new Tyr_rate_review_entity();
                    $rate_review_obj->tyroe_id = $get_featured_users[$i]['user_id'];
                    $get_tyroes_total_score = $rate_review_obj->get_tyroe_total_score();
                    if($get_tyroes_total_score['tyroe_score'] == NULL){
                        $get_tyroes_total_score['tyroe_score'] = 0;
                    }
                    $get_featured_users[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    
                    
//                    $get_tyroes_total_score = $this->getRow("SELECT ((AVG(`technical`))+(AVG(`creative`))+(AVG(`impression`)))/3 AS tyroe_score  FROM ".TABLE_RATE_REVIEW." WHERE tyroe_id = '".$get_featured_users[$i]['user_id']."'");
//                    if($get_tyroes_total_score['tyroe_score'] == NULL){
//                        $get_tyroes_total_score['tyroe_score'] = 0;
//                    }
//                    $get_featured_users[$i]['get_tyroes_total_score'] = round($get_tyroes_total_score['tyroe_score']);
                    /**end get tyroe total score**/
                }


                $this->data['pagination'] = $this->pagination(array('url' => 'featured/index', 'total_rows' => $total_featured['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
                $this->data['get_featured_users_total_counts']=$get_featured_users_total_counts;
                $this->data['ha']=$total_rows['cnt'];
                $this->data['get_featured_users'] = $get_featured_users;
            }
            $this->template_arr = array('header', 'contents/featured_users', 'footer');
            $this->load_template();
        }

    function add_to_featured()
    {
        $tyroe_id = $this->input->post('data_id');
        $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
        $featured_tyroes_obj->featured_tyroe_id = $tyroe_id;
        $featured_tyroes_obj->get_user_featured_status();
        $featured_tyroes_obj->featured_status= 1;
        if($featured_tyroes_obj->id == 0){
            $featured_tyroes_obj->save_featured_tyroes();
        }else{
            $featured_tyroes_obj->update_featured_tyroes_status();
        }
        echo '1';
    }

    function decline_featured(){
        $tyroe_id = $this->input->post('data_id');
        $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
        $featured_tyroes_obj->featured_tyroe_id= $tyroe_id;
        $featured_tyroes_obj->featured_status= -1;
        $featured_tyroes_obj->update_featured_tyroes_status();
        echo '1';
        }

    public function get_userjson(){
        
        $joins_obj = new Tyr_joins_entity();
        $get_featured_users_total_counts =  $joins_obj->get_all_featured_users_details();
//        $get_featured_users_total_counts = $this->getAll("SELECT u.user_id as tyr_rev_id  FROM ".TABLE_USERS." u LEFT JOIN ".TABLE_COUNTRIES." c
//            ON c.country_id = u.country_id LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id=f.featured_tyroe_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1'
//            AND u.publish_account = '1' AND  f.featured_status='0' ORDER BY user_id DESC ");
        echo json_encode($get_featured_users_total_counts);
    }

}