<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_coupons_allot_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_subscription_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);
require_once(APPPATH . 'entities/tyr_activity_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hide_user_entity' . EXT);
require_once(APPPATH . 'entities/tyr_search_messages_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_refrences_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendation_entity' . EXT);
require_once(APPPATH . 'entities/tyr_professional_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_other_site_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_sites_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_social_link_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendemail_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_rel_entity' . EXT);
require_once(APPPATH . 'entities/tyr_gallery_tags_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);

class Profile extends Tyroe_Controller
{
    public function Profile()
    {


        parent::__construct();
//        if($_GET['showdata']=='true'){
//            echo '<pre>';print_r($this->session->all_userdata());echo '</pre>';
//            echo time();
//            die;
//            //error_reporting(-1);
//            $logged_data = $this->db->select()->from('temp_sess_data')->get()->result_array();
//            //echo '<pre>';print_r($logged_data);echo '</pre>';
//            foreach($logged_data as $loggeddata){
//                $loggeddata_col = $loggeddata['session_data'];
//                $trace_data = $loggeddata['trace_data'];
//                echo '<br /> $loggeddata_col>>>>';
//                echo '<pre>';print_r(json_decode($loggeddata_col));echo '</pre>';
//
//                echo '<br /> $trace_data>>>>';
//                echo '<pre>';print_r(json_decode($trace_data));echo '</pre>';
//
//
//                //die;
//            }
//            die;
//        }
        $this->load->library('user_agent');
        $this->data['page_title'] = 'Public Profile';
        $user_id = $this->session->userdata("user_id");
        if (isset($this->data['access_allowed']) && $this->data['access_allowed']) {
            //$user_id = '127';
        } else {
            if ($this->session->userdata('role_id') != ADMIN_ROLE) {
                $menus = PROFILE_SUB_MENU;
                $this->get_system_modules(' AND module_id IN (' . PROFILE_SUB_MENU . ') ');
            } else {
                $menus = 0;
                $this->get_system_modules(' AND module_id IN (' . 0 . ') ');
            }
            $this->load->library('form_validation');
            
            $this->addslashesToPost();
        }
        
        $this->data['image_media_tags'] = $this->get_mediatags();
        #Profile Details
        $this->data['profile_completness'] = $this->get_profile_completeness($user_id);
        
        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_detail_info($user_id);
        $this->data['get_states'] = array();
        if ($get_user['country_id'] != "") {
            $states_obj = new Tyr_states_entity();
            $states_obj->country_id = $get_user['country_id'];
            $get_states = $states_obj->get_all_states_by_country();
            $this->data['get_states'] = $get_states;
        }
        
        $professional_level_obj = new Tyr_professional_level_entity();
        $tyroe_levels = $professional_level_obj->get_all_professional_level();
        $this->data['tyroe_levels'] = $tyroe_levels;
        
        $other_site_obj = new Tyr_other_site_entity();
        $other_site = $other_site_obj->get_all_other_site();
        $this->data['other_site'] = $other_site;

        $image = '';
        if ($get_user['media_name']) {
            $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH_NO_IMAGE;
        }


        /*  $new_pass = "";
          for ($i = 1; $i <= $get_user['password_len']; $i++) {
              $new_pass .= " ";
          }*/
        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "profile_url" => $get_user['profile_url'],
            "user_biography" => $get_user['user_biography'],
            "user_occupation" => $get_user['user_occupation'],
            "user_web" => $get_user['user_web'],
            "user_experience" => $get_user['user_experience'],
            "image" => $image,
            "image_id" => $get_user['image_id'],
            "password_len" => $get_user['password_len'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id'],
            "tyroe_level" => $get_user['tyroe_level'],
            "city_id" => $get_user['city_id'],
            "level_title" => $get_user['level_title'],
            "experienceyear" => $get_user['experienceyear'],
            "industry_name" => $get_user['industry_name'],
            "job_level_id" => $get_user['job_level_id'],
            "extra_title" => $get_user['extra_title'],
            "job_title" => $get_user['job_title'],
            "industry_id" => $get_user['industry_id'],
            "availability" => $get_user['availability'],
            "publish_account" => $get_user['publish_account'],
            "experienceyear_id" => $get_user['experienceyear_id']
        );
       
        $this->data['get_user'] = $user_data;

        $scorer = $this->get_profile_completeness();
        $this->data['portfolio_scorer'] = $scorer['portfolio_scorer'];
        $this->data['profile_scorer'] = $scorer['profile_scorer'];
        $this->data['resume_scorer'] = $scorer['resume_scorer'];
        $this->data['fullstatus'] = $scorer['fullstatus'];
        $this->data['incomplete'] = $scorer['incomplete'];
        /*mysite data*/
        $user_sites_obj = new Tyr_user_sites_entity();
        $user_sites_obj->user_id = $user_id;
        $mystite = $user_sites_obj->get_all_user_sites();
        $this->data['get_my_sites'] = $mystite;
    }

    public function getcities()
    {
        $country_id = $this->input->post('country_id');
        
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $country_id;
        $cities = $cities_obj->get_all_cities_by_country_id();
        $cities = $this->dropdown($cities, '', array('name' => 'city', 'placeholder' => 'City'));
        echo json_encode(array('dropdown' => $cities));
        exit;
    }

    public function index()
    {
        
        $this->_add_css('edit_profile.css');
        $user_id = $this->session->userdata("user_id");
        if($this->session->userdata("warm_welcome") == 1){
            $login_history_obj = new Tyr_login_history_entity();
            $login_history_obj->user_id = $user_id;
            $login_history = $login_history_obj->get_count_login_history_by_user_id();
            if($login_history['cnt'] == 1){
                $this->data['login_history_cnt'] = $login_history['cnt'];
            }
        }
        
        $joins_obj = new Tyr_joins_entity();
//        $get_user = $joins_obj->get_user_detail_info($user_id);
//        $this->data['get_user'] = $get_user;
        
        $contries_obj = new Tyr_countries_entity();
        $this->data['get_countries'] = $contries_obj->get_all_countries_as();
        
        $this->data['page_title'] = 'Edit Profile';
        $this->data['msg'] = "";
        if ($this->session->userdata('update_profile') == 1) {
            $this->data['profile_updated'] = MESSAGE_PROFILE_UPDATED;
            $this->session->unset_userdata("update_profile");
        }
        $this->data['firstname'] = $this->data['get_user']["firstname"];
        $this->data['lastname'] = $this->data['get_user']["lastname"];
        $this->data['username'] = $this->data['get_user']["username"];
        $this->data['user_biography'] = $this->data['get_user']["user_biography"];
        $this->data['availability'] = $this->data['get_user']["availability"];
        $this->data['industry_name'] = $this->data['get_user']["industry_name"];
        $this->data['extra_title'] = $this->data['get_user']["extra_title"];
        $this->data['job_title'] = $this->data['get_user']["job_title"];
        $this->data['country'] = $this->data['get_user']["country"];
        $this->data['city'] = $this->data['get_user']["city"];
        $this->data['level'] = $this->data['get_user']["level_title"];
        $this->data['experienceyear'] = $this->data['get_user']["experienceyear"];
        $this->data['get_countries_dropdown'] = $this->dropdown($this->data['get_countries'], $this->data['get_user']["country_id"], array('name' => 'country', 'placeholder' => 'Country', 'onchange' => 'getCities(this)')); 
        
        $cities_obj = new Tyr_cities_entity();
        $cities_obj->country_id = $this->data['get_user']["country_id"];
        $cities = $cities_obj->get_all_cities_by_country_id();
        $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City'));
        
        $industry_obj = new Tyr_industry_entity();
        $get_industry = $industry_obj->get_all_industries();
        $this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"], array('name' => 'industry', 'placeholder' => 'Select Industry'));
        
        
        $job_level_obj = new Tyr_job_level_entity();
        $get_job_level = $job_level_obj->get_all_job_level();
        $this->data['get_levels'] = $get_job_level;
        $this->data['get_job_level'] = $this->dropdown($get_job_level, $this->data['get_user']["job_level_id"], array('name' => 'year', 'placeholder' => 'Experience Level'));
        
        $experience_years_obj = new Tyr_experience_years_entity();
        $get_experience_years = $experience_years_obj->get_all_experience_years();
        $this->data['get_experience'] = $get_experience_years;
        $this->data['get_experience_years'] = $this->dropdown($get_experience_years, $this->data['get_user']["experienceyear_id"], array('name' => 'experience_level', 'placeholder' => 'Years'));
        
       
        //--------------------------Social link start-------------------------------------------------------
        $social_link = new Tyr_social_link_entity();
        $social_link->user_id = $user_id;
        $social_link->status_sl = 1;
        $social = $social_link->get_all_social_links_by_user_id_and_status();
        $this->data['get_social_link'] = $social;
        
        //--------------------------Social link  END-------------------------------------------------------


        //--------------------------Experience Start-------------------------------------------------------
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->user_id = $user_id;
        $experience_obj->status_sl = 1;
        $experience = $experience_obj->get_user_all_experience();
        $this->data['get_experience'] = $experience;
        

        //--------------------------Experience END-------------------------------------------------------

        //--------------------------Education Start-------------------------------------------------------
        $education_obj = new Tyr_education_entity();
        $education_obj->user_id = $user_id;
        $education_obj->status_sl = 1;
        $education = $education_obj->get_user_all_education();
        $this->data['get_education'] = $education;
        
        //--------------------------Education END-------------------------------------------------------


        //--------------------------Awards Start-------------------------------------------------------
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->user_id = $user_id;
        $awards_obj->status_sl = 1;
        $awards = $awards_obj->get_user_all_award();
        $this->data['get_awards'] = $awards;
        //--------------------------Awards END-------------------------------------------------------

        //--------------------------Skills Start-------------------------------------------------------
        //
        $joins_obj = new Tyr_joins_entity();
        $skills = $joins_obj->get_all_skill_by_users($user_id);
        $this->data['get_skills'] = $skills;
        //--------------------------Skills End-------------------------------------------------------


        //--------------------------Recommendation SentList Start-------------------------------------------------------
        //$joins_obj = new Tyr_joins_entity();
        $recommendation_sentlist = $joins_obj->get_users_recommendation_sentlist($user_id);
        $this->data['get_recommendation_sent_list'] = $recommendation_sentlist;
        
        //--------------------------Recommendation SentList END-------------------------------------------------------

        //--------------------------Recommendation PendingApprovalList Start-------------------------------------------------------
        $recommendation_obj_1 = new Tyr_recommendation_entity();
        $recommendation_obj_1->user_id = $user_id;
        $recommendation_obj_1->recommend_status = -1;
        $recommendation_approvallist_pending = $recommendation_obj_1->get_all_recommendation_approvallist();
        $this->data['get_recommendation_pending_approval_list'] = $recommendation_approvallist_pending;
        

        //--------------------------Recommendation PendingApprovalList END-------------------------------------------------------

        //--------------------------Recommendation ApprovedList Start-------------------------------------------------------
        $recommendation_obj_2 = new Tyr_recommendation_entity();
        $recommendation_obj_2->user_id = $user_id;
        $recommendation_obj_2->recommend_status = 1;
        $recommendation_approvedlist = $recommendation_obj_2->get_all_recommendation_approvallist();
        $this->data['get_recommendation_approved_list'] = $recommendation_approvedlist;
        //--------------------------Recommendation ApprovedList END-------------------------------------------------------

        //--------------------------Visibility Start-------------------------------------------------------
        $visibility_options_obj = new Tyr_visibility_options_entity();
        $visibility_options_obj->user_id = $user_id;
        $visibility_options_obj->status_sl = 1;
        $visibility = $visibility_options_obj->get_all_user_visibility_options();;
        $this->data['get_visibility'] = $visibility;
        
        //--------------------------Visibility END-------------------------------------------------------
        $media_obj_1 = new Tyr_media_entity();
        $media_obj_1->user_id = $user_id;
        $media_obj_1->media_featured = 1;
        $media_obj_1->profile_image = 1;
        $media_obj_1->status_sl = 1;
        $media_obj_1->media_type = 'video';
        $video = $media_obj_1->get_all_user_media_by_featured_or_type();
        $this->data['video'] = $video;
        
        //---------------------------------------Latest Work Images Start------------------------------------
        /*
         * COMMENTED BY FAISAL-RAMEEZ ---START
        $total_limit = 10;
        $next_start_limit = 0;
        $latestWorkImages = $this->getAll("SELECT *
                                        FROM tyr_media WHERE  user_id = '" . $user_id . "'
                                        AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                                        AND media_imagesection='1'
                                        ORDER BY media_sortorder ASC
                                        LIMIT 0,5
                                        ");
        $this->data['latestWorkImagesColumn1'] = $latestWorkImages;
        if (is_array($latestWorkImages) && count($latestWorkImages) > 0) {
            $next_start_limit = count($latestWorkImages);
            $total_limit = $total_limit - count($latestWorkImages);
        }
        $this->data['latestWorkImagesColumn1Start'] = $next_start_limit;
        $latestWorkImages = $this->getAll("SELECT *
                                                FROM tyr_media WHERE  user_id = '" . $user_id . "'
                                                AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                                                AND media_imagesection='2'
                                                ORDER BY media_sortorder ASC

                                                ");





        //Commented by shafqat START
//        $latestWorkImages = $this->getAll("SELECT *
//                                                        FROM tyr_media WHERE  user_id = '".$user_id."'
//                                                        AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
//                                                        AND media_imagesection='2'
//                                                        ORDER BY media_sortorder ASC
//                                                        LIMIT 0,".$total_limit."
//                                                        ");
//Commented by shafqat END

        $next_start_limit = 0;
        if (is_array($latestWorkImages) && count($latestWorkImages) > 0) {
            $next_start_limit = count($latestWorkImages);
        }
        $this->data['latestWorkImagesColumn2Start'] = $next_start_limit;
        $this->data['latestWorkImagesColumn2'] = $latestWorkImages;
        * COMMENTED BY FAISAL-RAMEEZ ---END
         */
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $user_id;
        $media_obj->media_featured = 0;
        $media_obj->profile_image = 1;
        $media_obj->status_sl = 1;
        $media_obj->media_type = 'image';
        $latestwork_portfolioimages = $media_obj->get_user_latest_portfolio_image();
        $this->data['latestwork_portfolioimages'] = $latestwork_portfolioimages;
        
        ///echo $this->db->last_query();

        /*
         *
          $this->getAll("SELECT *
                                                        FROM tyr_media WHERE  user_id = '".$user_id."'
                                                        AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                                                        AND media_imagesection='2'
                                                        ORDER BY media_sortorder ASC
                                                        LIMIT 0,".$total_limit."
                                                        ");
         * */
        //---------------------------------------Latest Work Images End--------------------------------------


        $this->template_arr = array('header', 'contents/edit_profile', 'footer');
        $this->load_template();
    }

    /*--------------------Create Thumb-------------------*/
    public function rm_experience()
    {
        $item_id = $this->input->post("item_id");
        $where = "exp_id ='" . $item_id . "' AND user_id='" . $this->session->userdata("user_id") . "'";
        $experience_detail = array(
            "status_sl" => -1
        );
        //$return = $this->update(TABLE_EXPERIENCE, $experience_detail, $where);
        
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->exp_id = $item_id;
        $experience_obj->get_experience();
        $experience_obj->status_sl = -1;
        $return = $experience_obj->update_experience();
        
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->user_id = $this->session->userdata("user_id");
        $experience_obj->status_sl = 1;
        $check_records = $experience_obj->get_user_experience_cnt();
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'total_rows' => $check_records['cnt'], 'clas' => 'work-exp-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }
    
    //STAR
    public function sv_add_experience()
    {
        if ($this->input->post("endpresent") == "true") {
            $current_job = 1;
        } else {
            $current_job = 0;
        }

        $item_id = $this->input->post("item_id");
        $user_id = $this->session->userdata("user_id");
        $experience_detail = array(
            "user_id" => $this->session->userdata("user_id")
        );
        foreach ($_POST as $k => $v) {
            if ($k == "item_id") {
                if ($v < 1) {
                    $experience_detail['created'] = strtotime("now");
                }
            } else if ($k == "callfrom" || $k == "endpresent") {

            } else {
                if ($k == "job_end" && $v == "present") {
                    $experience_detail['current_job'] = 1;
                } else {
                    switch ($k) {
                        case 'job_start':
                            $v = strtotime($v);
                            break;
                        case 'job_end':
                            $v = strtotime($v);
                            $experience_detail['current_job'] = 0;
                            break;
                    }
                    $experience_detail[$k] = $v;
                }

            }

        }
        
        
        if ($current_job > 0) {
            $upd_all_experience = array(
                "current_job" => 0,
                "job_end" => strtotime('now'),
            );
            $experience_obj = new Tyr_experience_entity();
            $experience_obj->current_job = 0;
            $experience_obj->job_end = strtotime('now');
            $experience_obj->update_user_all_experience();
//            $where = "user_id ='" . $this->session->userdata("user_id") . "'";
//            $this->update(TABLE_EXPERIENCE, $upd_all_experience, $where);
        }
        
        $experience_obj = new Tyr_experience_entity();
        if ($this->input->post("item_id") > 0) {
            $exp_id = $this->input->post("item_id");
            $experience_obj->exp_id = $exp_id;
            $experience_obj->get_experience();
            if(isset($experience_detail['company_name']) && $experience_detail['company_name'] != '')
                $experience_obj->company_name = $experience_detail['company_name'];
            
            if(isset($experience_detail['job_title']) && $experience_detail['job_title'] != '')
                $experience_obj->job_title = $experience_detail['job_title'];
            
            if(isset($experience_detail['job_description']) && $experience_detail['job_description'] != '')
                $experience_obj->job_description = $experience_detail['job_description'];
            
            if(isset($experience_detail['job_start']) && $experience_detail['job_start'] != '')
                $experience_obj->job_start = $experience_detail['job_start'];
            
            if(isset($experience_detail['job_end']) && $experience_detail['job_end'] != '')
                $experience_obj->job_end = $experience_detail['job_end'];
            
            if(isset($experience_detail['current_job']) && $experience_detail['current_job'] != '')
                $experience_obj->current_job = $experience_detail['current_job'];
            
            $experience_obj->modified = strtotime("now");
            
            
            
            $experience_obj->update_experience();
//            $where = "exp_id ='" . $exp_id . "'";
//            $return = $this->update(TABLE_EXPERIENCE, $experience_detail, $where);
            
            $item_id = $this->input->post("item_id");
            $return = $this->input->post("item_id");
        } else {
            //$return = $this->insert(TABLE_EXPERIENCE, $experience_detail);
            $experience_obj->user_id = $this->session->userdata("user_id");
            $experience_obj->created = strtotime("now");
            $experience_obj->modified = strtotime("now");
            $experience_obj->status_sl = 1;
            $experience_obj->save_experience();
                    
            $return = $experience_obj->exp_id;
            $item_id = $experience_obj->exp_id;
        }
        
        //$check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $this->session->userdata("user_id") . "' and status_sl='1'");
        $experience_obj = new Tyr_experience_entity();
        $experience_obj->user_id = $this->session->userdata("user_id");
        $experience_obj->status_sl = 1;
        $check_records = $experience_obj->get_user_experience_cnt();
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
//            $modified_update = array(
//                'modified_timestamp' =>strtotime("now")
//            );
//            $where = array("user_id" => $user_id);
//            $this->update(TABLE_USERS,$modified_update,$where);
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'item_id' => $item_id, 'total_rows' => $check_records['cnt'], 'clas' => 'work-exp-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function sv_profile_top()
    {
        $user_id = $this->session->userdata("user_id");
        $country_id=(int)$this->input->post("country");
        $city_id=(int)$this->input->post("city");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();

        $users_obj->job_level_id = (int)$this->input->post("year");
        $users_obj->job_title = strip_tags($this->input->post("extra_stuff"));
        $users_obj->industry_id = (int)$this->input->post("industry");
        $users_obj->experienceyear_id = (int)$this->input->post("experience_level");
        //$users_obj->availability = (int)$this->input->post("availability_option");
        $users_obj->user_biography = strip_tags($this->input->post("biography"));
        
        if($country_id==0){
            $users_obj->country_id = "";
        }else if ($country_id != "") {
            $users_obj->country_id = $country_id;
        }

        if($city_id==0){
            $users_obj->city = "";
        }else if ($city_id != "") {
            $users_obj->city = $city_id;
        }
        $return_user = $users_obj->update_users();
        
        if ($return_user) {
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function sv_publish_account()
    {
        $user_id = $this->session->userdata("user_id");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->publish_account = $this->input->post("publish_account");
        $users_obj->modified_timestamp = strtotime("now");
        $return_user = $users_obj->update_users();
        
        if ($return_user > 0) {
            
            $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
            $featured_tyroes_obj->featured_tyroe_id = $user_id;
            $featured_tyroes_obj->get_user_featured_status();
            $find_featured = (array)$featured_tyroes_obj;
           
            if(empty($find_featured) || $find_featured['id'] == ""){
                $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
                $featured_tyroes_obj->featured_tyroe_id = $user_id;
                $featured_tyroes_obj->featured_status = 0;
                $featured_tyroes_obj->save_featured_tyroes();
            }
            echo json_encode(array('status' => true, 'success' => true, "d"=>"SELECT id FROM ".TABLE_FEATURED_TYROE." WHERE featured_tyroe_id = '".$return_user."'"));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function sv_user_names()
    {
        $user_id = $this->session->userdata("user_id");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->firstname = $this->input->post("firstname");
        $users_obj->lastname = $this->input->post("lastname");
        $users_obj->modified_timestamp = strtotime("now");
        $return_user = $users_obj->update_users();
        
        if ($return_user) {
            $this->session->set_userdata('firstname',$this->input->post("firstname"));
            $this->session->set_userdata('lastname',$this->input->post("lastname"));
            echo json_encode(array('status' => true, 'success' => true , 'fn' => $this->input->post("firstname"), 'ln' => $this->input->post("lastname")));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function sv_user_biography()
    {
        $user_id = $this->session->userdata("user_id");
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->user_biography = $this->input->post("bio");
        $return_user = $users_obj->update_users();
        
        $scorer = $this->get_profile_completeness();
        if ($return_user > 0) {
            echo json_encode(array('status' => true, 'success' => true, 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function social_links()
    {
        $user_id = $this->session->userdata("user_id");
        
        $social_link_obj = new Tyr_social_link_entity();
        $social_link_obj->social_type = $this->input->post("social_val");
        $social_link_obj->user_id = $user_id;
        $social_link_obj->social_link = $this->input->post("social_url");
        $social_link_obj->status_sl = 1;
        $social_link_obj->save_social_link();
        $return_user_id = $social_link_obj->social_id;
        
        if ($return_user_id > 0) {
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $return_user = $users_obj->update_users();
            
            $social_link_obj = new Tyr_social_link_entity();
            $social_link_obj->user_id = $user_id;
            $social_link_obj->status_sl = 1;
            $check_records = $social_link_obj->get_user_social_link_count();
            
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'social_id' => $return_user_id, 'cnt' => $check_records['cnt'], 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function remove_social_link()
    {
        $user_id = $this->session->userdata("user_id");
        
        $social_link_obj = new Tyr_social_link_entity();
        $social_link_obj->social_id = $this->input->post("item_id");
        $social_link_obj->get_social_link();
        $social_link_obj->status_sl = -1;
        $social_link_obj->modified_timestamp = strtotime("now");
        $return = $social_link_obj->update_social_link();
        
        $social_link_obj = new Tyr_social_link_entity();
        $social_link_obj->user_id = $user_id;
        $social_link_obj->status_sl = 1;
        $check_records = $social_link_obj->get_user_social_link_count();
        
        if ($return) {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS, 'total_rows' => $check_records['cnt'], 'scorer' => $scorer));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function user_availablity()
    {
        $user_id = $this->session->userdata("user_id");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->availability = $this->input->post("availability");
        $users_obj->modified_timestamp = strtotime("now");
        $return_user = $users_obj->update_users();
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->availability = 0;
        $check_records = $users_obj->get_available_user_count();
       
        $scorer = $this->get_profile_completeness();

        if ($return_user) {
            echo json_encode(array('status' => true, 'success' => true, 'total_rows' => $check_records['cnt'], 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }
    
    //STAR
    public function save_education()
    {
        $education_detail = array(
            "user_id" => $this->session->userdata("user_id")
        );
        foreach ($_POST as $k => $v) {
            if ($k == "item_id") {
                if ($v < 1) {
                    $education_detail['created'] = strtotime("now");
                }
            } else if ($k == "callfrom" || $k == "endpresent") {

            } else {
                if ($k == "end_year" && $v == "present") {
                } else {
                    switch ($k) {
                        case 'start_year':
                        case 'end_year':
                            $v = strtotime($v);
                            break;
                    }
                    $education_detail[$k] = $v;
                }

            }

        }
        
        $edu_obj = new Tyr_education_entity();
        
        if ($this->input->post("item_id") > 0) {
            $edu_id = $this->input->post("item_id");
            $edu_obj->education_id = $edu_id;
            $edu_obj->get_education();
            
            if(isset($education_detail['edu_organization']) && $education_detail['edu_organization'] != '')
                $edu_obj->edu_organization = $education_detail['edu_organization'];
            
            if(isset($education_detail['institute_name']) && $education_detail['institute_name'] != '')
                $edu_obj->institute_name = $education_detail['institute_name'];
            
            if(isset($education_detail['degree_title']) && $education_detail['degree_title'] != '')
                $edu_obj->degree_title = $education_detail['degree_title'];
            
            if(isset($education_detail['education_description']) && $education_detail['education_description'] != '')
                $edu_obj->education_description = $education_detail['education_description'];
            
            if(isset($education_detail['edu_position']) && $education_detail['edu_position'] != '')
                $edu_obj->edu_position = $education_detail['edu_position'];
            
            if(isset($education_detail['start_year']) && $education_detail['start_year'] != '')
                $edu_obj->start_year = $education_detail['start_year'];
            
            if(isset($education_detail['end_year']) && $education_detail['end_year'] != '')
                $edu_obj->end_year = $education_detail['end_year'];
            
            
            $edu_obj->modified = strtotime("now");
//            $where = "education_id ='" . $edu_id . "'";
//            $return = $this->update(TABLE_EDUCATION, $education_detail, $where);
            $edu_obj->update_education();
            $item_id = $edu_id;
            $return = $item_id;
        } else {
            //$return = $this->insert(TABLE_EDUCATION, $education_detail);
            $edu_obj->user_id = $this->session->userdata("user_id");
            $edu_obj->modified = strtotime("now");
            $edu_obj->created = strtotime("now");
            $edu_obj->status_sl = 1;
            $edu_obj->save_education();
            $return = $edu_obj->education_id;
            $item_id = $return;
        }
        
        $education_obj = new Tyr_education_entity();
        $education_obj->user_id = $this->session->userdata("user_id");
        $education_obj->status_sl = 1;
        $check_records = $education_obj->get_user_education_count();
        //$check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_EDUCATION . "  WHERE user_id='" . $this->session->userdata("user_id") . "' and status_sl='1'");
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
//            $modified_update = array(
//                'modified_timestamp' =>strtotime("now")
//            );
//            $where = array("user_id" => $user_id);
//            $this->update(TABLE_USERS,$modified_update,$where);
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'item_id' => $item_id, 'total_rows' => $check_records['cnt'], 'clas' => 'edu-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }


    }

    public function rm_education()
    {
        $item_id = $this->input->post("item_id");
        
        $education_obj = new Tyr_education_entity();
        $education_obj->education_id = $item_id;
        $education_obj->get_education();
        $education_obj->status_sl = -1;
        $education_obj->modified = strtotime("now");
        $return = $education_obj->update_education();
        
        $education_obj = new Tyr_education_entity();
        $education_obj->user_id = $this->session->userdata("user_id");
        $education_obj->status_sl = 1;
        $check_records = $education_obj->get_user_education_count();
        
        //$check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_EDUCATION . "  WHERE user_id='" . $this->session->userdata("user_id") . "' and status_sl='1'");
        if ($return) {
            $user_id = $this->session->userdata("user_id");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'total_rows' => $check_records['cnt'], 'clas' => 'edu-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function visibility_option()
    {
        $vis = $this->input->post("vis");
        $val = $this->input->post("val");
        $user_id = $this->session->userdata("user_id");
        $visibility_options_obj = new Tyr_visibility_options_entity();
        $visibility_options_obj->user_id = $user_id;
        $visibility_options_obj->visible_section = $vis;
        $visibility_options_obj->visibility = $val;
        $return = $visibility_options_obj->update_user_visibility_of_section();
        
        //$check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_EDUCATION . "  WHERE user_id='" . $this->session->userdata("user_id") . "' and status_sl='1'");
        if ($return) {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            echo json_encode(array('status' => true, 'success' => true, 'data_val' => $val));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }

    }
    
    //STAR
    public function save_awards()
    {
        $details = array(
            "user_id" => $this->session->userdata("user_id")
        );
        foreach ($_POST as $k => $v) {
            if ($k == "item_id") {
                if ($v < 1) {
                    $details['created_timestamp'] = strtotime("now");
                }
            } else if ($k == "callfrom" || $k == "endpresent") {
            } else {
                if ($k == "end_year" && $v == "present") {
                } else {
                    $details[$k] = $v;
                }
            }
        }
       
        $awards_obj = new Tyr_awards_entity();
        if ($this->input->post("item_id") > 0) {
            $award_id = $this->input->post("item_id");
            $awards_obj->award_id = $award_id;
            $awards_obj->get_awards();
            
            if(isset($details['award']) && $details['award'] != '')
                $awards_obj->award = $details['award'];
            
            if(isset($details['award_organization']) && $details['award_organization'] != '')
                $awards_obj->award_organization = $details['award_organization'];
            
            if(isset($details['award_year']) && $details['award_year'] != '')
                $awards_obj->award_year = $details['award_year'];
            
            if(isset($details['award_description']) && $details['award_description'] != '')
                $awards_obj->award_description = $details['award_description'];
            
            $awards_obj->modified = strtotime("now");
//            $where = "award_id ='" . $award_id . "'";
//            $return = $this->update(TABLE_AWARDS, $details, $where);
            $return = $awards_obj->update_awards();
            $item_id = $awards_obj->award_id;
            $return = $item_id;
        } else {
            $awards_obj->user_id = $this->session->userdata("user_id");
            $awards_obj->status_sl = 1;
            $awards_obj->modified = strtotime("now");
            $awards_obj->created_timestamp = strtotime("now");
            $awards_obj->save_awards();
            //$return = $this->insert(TABLE_AWARDS, $details);
            $item_id = $awards_obj->award_id;
            $return = $item_id;
        }
        
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->user_id = $this->session->userdata("user_id");
        $awards_obj->status_sl = 1;
        $check_records = $awards_obj->get_user_all_award_count();
        //$check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_AWARDS . "  WHERE user_id='" . $this->session->userdata("user_id") . "' and status_sl='1'");
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
//            $modified_update = array(
//                'modified_timestamp' =>strtotime("now")
//            );
//            $where = array("user_id" => $user_id);
//            $this->update(TABLE_USERS,$modified_update,$where);
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'item_id' => $item_id, 'total_rows' => $check_records['cnt'], 'clas' => 'awards-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function rm_awards()
    {
        $item_id = $this->input->post("item_id");
        
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->award_id = $item_id;
        $awards_obj->get_awards();
        $awards_obj->status_sl = -1;
        $awards_obj->modified = strtotime("now");
        $return = $awards_obj->update_awards();
        
        $awards_obj = new Tyr_awards_entity();
        $awards_obj->user_id = $this->session->userdata("user_id");
        $awards_obj->status_sl = 1;
        $check_records = $awards_obj->get_user_all_award_count();
        if ($return > 0) {
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('status' => true, 'success' => true, 'total_rows' => $check_records['cnt'], 'clas' => 'awards-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function save_skills()
    {
        $skill = strtolower(trim($this->input->post('skill')));
        $user_id = $this->session->userdata("user_id");
        $creative_skills_obj = new Tyr_creative_skills_entity();
        $creative_skills_obj->creative_skill = $skill;
        $skill_exist = $creative_skills_obj->check_skill_exist();
       
        $skill_id = 0;
        if (count($skill_exist) > 0 && is_array($skill_exist)) {
            $skill_id = $skill_exist['creative_skill_id'];
        } else {
            $creative_skills_obj->save_creative_skills();
            $skill_id = $creative_skills_obj->creative_skill_id;
        }
        
        $skills_obj = new Tyr_skills_entity();
        $skills_obj->user_id = $this->session->userdata("user_id");
        $skills_obj->skill =  $skill_id;
        $skills_obj->status_sl = 1;
        $skill_exist = $skills_obj->check_user_skill_exist();
        
        
        if (count($skill_exist) > 0 && is_array($skill_exist)) {
            $skill_id = $skill_exist['skill_id'];
        } else {
            $current_timestamp = strtotime('now');
            
            $skills_obj = new Tyr_skills_entity();
            $skills_obj->user_id = $this->session->userdata("user_id");
            $skills_obj->skill =  $skill_id;
            $skills_obj->status_sl = 1;
            $skills_obj->created_timestamp = $current_timestamp;
            $skills_obj->modified = $current_timestamp;
            $skills_obj->save_skills();
            $skill_id = $skills_obj->skill_id;
        }
        $joins_obj = new Tyr_joins_entity();
        $check_records = $joins_obj->get_user_all_skill_count($user_id);
        
        $user_id = $this->session->userdata("user_id");
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->get_user();
        $users_obj->update_users();
        
        $scorer = $this->get_profile_completeness();
        echo json_encode(array('status' => true, 'success' => true, 'item_id' => $skill_id, 'total_rows' => $check_records['cnt'], 'clas' => 'skill-container', 'scorer' => $scorer));
        exit;
    }

    public function remove_skills()
    {
        $user_id = $this->session->userdata("user_id");
        $item_id = $this->input->post("item_id");
        
        $skills_obj = new Tyr_skills_entity();
        $skills_obj->skill_id = $item_id;
        $skills_obj->get_skills();
        $skills_obj->status_sl = -1;
        $skills_obj->modified = strtotime("now");
        $return = $skills_obj->update_skills();
        
       
        $joins_obj = new Tyr_joins_entity();
        $check_records = $joins_obj->get_user_all_skill_count($user_id);

        $scorer = $this->get_profile_completeness();
        $this->data['portfolio_scorer'] = $scorer['portfolio_scorer'];
        $this->data['profile_scorer'] = $scorer['profile_scorer'];
        $this->data['resume_scorer'] = $scorer['resume_scorer'];

        if ($return) {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->get_user();
            $users_obj->update_users();
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS, 'total_rows' => $check_records['cnt'], 'clas' => 'skill-container', 'scorer' => $scorer));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function encoder_base($str)
    {
        //for ($i = 0; $i < 2; $i++) {
        for ($i = 0; $i < 2; $i++) {
            $str = "jahangirazhar" . base64_encode($str) . "kakasunny";
        }
        for ($i = 0; $i < 5; $i++) {
            $str = base64_encode($str);
        }
        return $str;
    }

    public function decoder_base($str)
    {
        for ($i = 0; $i < 5; $i++) {
            $str = base64_decode($str);
        }
        for ($i = 0; $i < 2; $i++) {
            //$str="jahangirazhar".base64_encode($str)."kakasunny";
            $str = str_replace("jahangirazhar", "", $str);
            $str = str_replace("kakasunny", "", $str);
            $str = base64_decode($str);
        }

        return $str;
    }

    /*public function send_recommendation()
    {
        $user_id = $this->session->userdata("user_id");
        $recommend_email = $this->input->post('recommend_email');
        $recommend_exist = $this->getRow("SELECT *  FROM " . TABLE_RECOMMENDATION . "  WHERE LOWER(TRIM(recommend_email))='$recommend_email' AND user_id='$user_id' AND status_sl='1'");
        $check_records = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_RECOMMENDATION . " WHERE user_id='" . $user_id . "' and status_sl='1'");
        if (count($recommend_exist) > 0 && is_array($recommend_exist)) {
            echo json_encode(array('status' => false, 'success' => true, 'success_message' => RECOMMENDATION_EMAIL_EXIST));
        } else {
            $current_timestamp = strtotime('now');
            $details = array(
                "user_id" => $user_id,
                "status_sl" => 1,
                "recommend_email" => $recommend_email,
                "created_timestamp" => $current_timestamp
            );
            $recommend_id = $this->insert(TABLE_RECOMMENDATION, $details);
            $recommend_id_encode = $this->encoder_base($recommend_id);
            $from = $this->data['get_user']['firstname'] . " " . $this->data['get_user']['lastname'];
            $message = "<a href='" . $this->getURL("profile/public_recommendation/" . $recommend_id_encode) . "'>Click here </a>to write Recommendation for " . $from . ".";
            $email_send = $this->email_sender($recommend_email, LABEL_REQUEST_REFERENCES, $message);
            if ($email_send) {
                $details = array(
                    "recommend_id" => $recommend_id,
                    "message" => $message,
                    "created_timestamp" => $current_timestamp
                );
                $current_date = date("d F Y", $current_timestamp) . " AT " . date("h:i A", $current_timestamp);
                $this->insert(TABLE_RECOMMENDATIONEMAIL_HISTORY, $details);
                echo json_encode(array('status' => true, 'success' => true, 'item_id' => $recommend_id, "email" => $recommend_email, "datesent" => $current_date, 'total_rows' => $check_records['cnt'], 'clas' => 'recommend-container'));
            }
        }
    }*/
    public function send_recommendation()
    {
        $user_id = $this->session->userdata("user_id");
        $recommend_email = $this->input->post('recommend_email');
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->user_id = $user_id;
        $recommendation_obj->recommend_email = $recommend_email;
        $recommendation_obj->status_sl = 1;
        $recommend_exist = $recommendation_obj->check_recommendation_exist();
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->user_id = $user_id;
        $recommendation_obj->status_sl = 1;
        $check_records = $recommendation_obj->get_user_all_recommendation_count();
        
        if (count($recommend_exist) > 0 && is_array($recommend_exist)) {
            echo json_encode(array('status' => false, 'success' => true, 'success_message' => RECOMMENDATION_EMAIL_EXIST));
        } else {
            $current_timestamp = strtotime('now');
            
            $recommendation_obj = new Tyr_recommendation_entity();
            $recommendation_obj->user_id = $user_id;
            $recommendation_obj->status_sl = 1;
            $recommendation_obj->recommend_email = $recommend_email;
            $recommendation_obj->created_timestamp = $current_timestamp;
            $recommendation_obj->save_recommendation();
            $recommend_id = $recommendation_obj->recommend_id;
            
            $joins_obj = new Tyr_joins_entity();
            $get_user = $joins_obj->get_user_detail_info($user_id);
            $this->data['get_user'] = $get_user;
            
            $recommend_id_encode = $this->encoder_base($recommend_id);
            $from = $this->data['get_user']['firstname'] . " " . $this->data['get_user']['lastname'];
            $firstname = $this->session->userdata("firstname");
            $lastname = $this->session->userdata("lastname");
            $profile_url = $this->session->userdata("profile_url");
            
            $message = "<link
            href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
            rel='stylesheet' type='text/css'>";
            $message .= "<style>html { font-family: 'Open Sans'}</style>";
            $message .= "<b>".ucfirst($firstname)." ".ucfirst($lastname)."</b> has a great looking online portfolio page at <a href='".LIVE_SITE_URL."' style='color:#67C5DF'>Tyroe.com</a>. All that is needed now is glowing 'Twitter style' recommendation from you to really and some credibility.
            <br><br> The best part is that no registration is required and it only takes a minute of your time to help out.<br><br><br>";
            $message .= '<a href="' . $this->getURL("profile/public_recommendation/" . $recommend_id_encode) . '" class="" style=""><img width="200" src="'.ASSETS_PATH.'img/add_recomm.png"></a>';
            $message .= "<br><br>";
            $message .= "___________________________________________________";
            $message .= "<br><br>";
            $message .= "<i>Thanks for helping!</i>";
            $message .= "<br>";
            $message .= "Andrew & Alwyn, co-founders of Tyroe";
            $message .= "<br>";
            $message .= "<a href='" . LIVE_SITE_URL . "'><img src='".ASSETS_PATH."img/logo_tyroe_new1.png'></a>";
            $subject = ucfirst($firstname)." ".ucfirst($lastname)." needs your help!";
            $email_send = $this->email_sender($recommend_email, $subject, $message);
            if ($email_send) {
                
                $recommendemail_history_obj = new Tyr_recommendemail_history_entity();
                $recommendemail_history_obj->recommend_id = $recommend_id;
                $recommendemail_history_obj->message = $message;
                $recommendemail_history_obj->created_timestamp = $current_timestamp;
                $recommendemail_history_obj->save_recommendemail_history();
                
                $current_date = date("d F Y", $current_timestamp) . " AT " . date("h:i A", $current_timestamp);
                
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $user_id;
                $users_obj->get_user();
                $users_obj->modified_timestamp = strtotime("now");
                $users_obj->update_users();
               
                echo json_encode(array('status' => true, 'success' => true, 'item_id' => $recommend_id, "email" => $recommend_email, "datesent" => $current_date, 'total_rows' => $check_records['cnt'], 'clas' => 'recommend-container'));
            }
        }
    }

    public function resend_recommendation()
    {
        $user_id = $this->session->userdata("user_id");
        $recommend_id = $this->input->post('recommend_id');
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->user_id = $user_id;
        $recommendation_obj->recommend_id = recommend_id;
        $recommendation_obj->status_sl = 1;
        $recommendation_obj->recommend_status = 0;
        $recommend_exist = $recommendation_obj->check_recommendation_exist_1();
        
        if (count($recommend_exist) > 0 && is_array($recommend_exist)) {
            $current_timestamp = strtotime('now');
            $recommend_id_encode = $this->encoder_base($recommend_id);
            $recommend_email = $recommend_exist['recommend_email'];
            $firstname = $this->session->userdata("firstname");
            $lastname = $this->session->userdata("lastname");
            $message = "<link
            href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
            rel='stylesheet' type='text/css'>";
            $message .= "<style>html { font-family: 'Open Sans'}</style>";
            $message .= "<b>".ucfirst($firstname)." ".ucfirst($lastname)."</b> has a great looking online portfolio page at <a href='".LIVE_SITE_URL."/".$profile_url."' style='color:#67C5DF'>Tyroe.com</a>. All that is needed now is glowing 'Twitter style' recommendation from you to really and some credibility.
            <br><br> The best part is that no registration is required and it only takes a minute of your time to help out.<br><br><br>";
            $message .= '<a href="' . $this->getURL("profile/public_recommendation/" . $recommend_id_encode) . '" class="" style=""><img width="200" src="'.ASSETS_PATH.'img/add_recomm.png"></a>';
            $message .= "<br><br>";
            $message .= "___________________________________________________";
            $message .= "<br><br>";
            $message .= "<i>Thanks for helping!</i>";
            $message .= "<br>";
            $message .= "Andrew & Alwyn, co-creators of Tyroe";
            $message .= "<br>";
            $message .= "<img src='".ASSETS_PATH."img/logo_tyroe_new1.png'>";
            $subject = ucfirst($firstname)." ".ucfirst($lastname)." needs your help!";
            $email_send = $this->email_sender($recommend_email, $subject, $message);
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            
            if ($email_send) {
                
                $recommendemail_history_obj = new Tyr_recommendemail_history_entity();
                $recommendemail_history_obj->recommend_id = $recommend_id;
                $recommendemail_history_obj->message = $message;
                $recommendemail_history_obj->created_timestamp = $current_timestamp;
                $recommendemail_history_obj->save_recommendemail_history();
                
                $current_date = date("d F Y", $current_timestamp) . " AT " . date("h:i A", $current_timestamp);
                echo json_encode(array('status' => true, 'success' => true, 'item_id' => $recommend_id, "datesent" => $current_date));
            } else
                echo json_encode(array('status' => false, 'success' => true));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function remove_recommend()
    {
        $user_id = $this->session->userdata("user_id");
        $item_id = $this->input->post("recommend_id");
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $item_id;
        $recommendation_obj->get_recommendation();
        $recommendation_obj->recommend_status = -1;
        $recommendation_obj->status_sl = -1;
        $return = $recommendation_obj->update_all_data();
        
//        $where = "recommend_id ='" . $item_id . "' AND user_id='" . $this->session->userdata("user_id") . "'";
//        $update = array(
//            "recommend_status" => "-1",
//            "status_sl" => "-1"
//        );
//        $return = $this->update(TABLE_RECOMMENDATION, $update, $where);
        
        
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->user_id = $user_id;
        $recommendation_obj->status_sl = 1;
        $check_records = $recommendation_obj->get_user_all_recommendation_count();
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            echo json_encode(array('status' => true, 'success' => true, 'total_rows' => $check_records['cnt'], 'clas' => 'recommend-container'));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function approve_recommendation()
    {
        $item_id = $this->input->post("recommend_id");
        $user_id= $this->session->userdata("user_id");
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $item_id;
        $recommendation_obj->get_recommendation();
        $recommendation_obj->recommend_status = 1;
        $return = $recommendation_obj->update_all_data();
        //$user_id = $recommendation_obj->user_id;
        
//        $where = "recommend_id ='" . $item_id . "' AND user_id='" . $this->session->userdata("user_id") . "'";
//        $update = array(
//            "recommend_status" => 1
//        );
//        $return = $this->update(TABLE_RECOMMENDATION, $update, $where);
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->user_id = $user_id;
        $recommendation_obj->recommend_id = $item_id;
        $recommendation_obj->status_sl = 1;
        $recommendation_obj->recommend_status = 1;
        $recommend_exist = $recommendation_obj->check_recommendation_exist_1();
        
        if ($return > 0) {
            $user_id = $this->session->userdata("user_id");
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            
            echo json_encode(array('status' => true, 'success' => true,
                'recommend_name' => $recommend_exist['recommend_name'],
                'recommendation' => $recommend_exist['recommendation'],
                'recommend_company' => $recommend_exist['recommend_company'],
                'recommend_dept' => $recommend_exist['recommend_dept'],
                'recommend_id' => $recommend_exist['recommend_id']
            ));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function video_url()
    {

        $media_type = $this->input->post("media_type");
        if ($media_type == 'video') {
            $user_id = $this->session->userdata("user_id");
            $video_url = $this->input->post("video_url");
            $vid_desc = $this->input->post("video_desc");
            $video_type = $this->input->post("video_type");
            $media_url = $this->input->post("media_url");
            
            $media_obj = new Tyr_media_entity();
            $media_obj->media_type = 'video';
            $media_obj->status_sl = 1;
            $media_obj->user_id = $user_id;
            $chk_video = $media_obj->check_user_video_media_1();

            if (!empty($chk_video)) {
                
                $media_obj = new Tyr_media_entity();
                $media_obj->status_sl = 1;
                $media_obj->modified = strtotime("now");
                $media_obj->user_id = $user_id;
                $media_obj->media_featured = 1;
                $media_obj->update_media_status_by_user();
                
                
                $media_obj = new Tyr_media_entity();
                $media_obj->image_id = $chk_video['image_id'];
                $media_obj->get_media();
                $media_obj->media_name = $video_url;
                $media_obj->media_url = $media_url;
                $media_obj->media_description = $vid_desc;
                $media_obj->video_type = $video_type;
                $media_obj->modified = strtotime("now");
                $media_obj->update_media();
                
                $result['success'] = MESSAGE_ADD_SUCCESS;
                $result['status'] = true;

                #count
                $joins_obj = new Tyr_joins_entity();
                $check_records = $joins_obj->get_user_all_skill_count($user_id);
                
            } else {
                
                $media_obj = new Tyr_media_entity();
                $media_obj->status_sl = 1;
                $media_obj->modified = strtotime("now");
                $media_obj->user_id = $user_id;
                $media_obj->media_featured = 1;
                $media_obj->update_media_status_by_user();
                
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $user_id;
                $media_obj->media_title = $this->input->post("media_title");
                $media_obj->media_name = $video_url;
                $media_obj->media_url = $media_url;
                $media_obj->media_description = $vid_desc;
                $media_obj->profile_image = 0;
                $media_obj->video_type = $video_type;
                $media_obj->media_type = $media_type;
                $media_obj->modified = strtotime("now");
                $media_obj->created = strtotime("now");
                $media_obj->status_sl = 1;
                $media_obj->save_media();
             
                
                $scorer = $this->get_profile_completeness();
                $result['success'] = MESSAGE_UPDATE_SUCCESS;
                $result['status'] = true;
                $result['scorer'] = $scorer;

            }
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            echo json_encode($result);

        } else {
            $video_ext_array = array('avi', 'mpg', 'mpeg', 'wmv', 'mp4', '3gp', 'flv', 'mp3', 'AVI', 'MPG', 'MPEG', 'WMV', 'MP4', '3GP', 'FLV', 'MP3');
            $pathinfo = pathinfo($_FILES['Filedata']['name']);
            $pathinfo_ext = $pathinfo['extension'];
            $verifyToken = md5('unique_salt' . $this->input->post('timestamp'));

            $image_upload_folder = 'assets/uploads/portfolio1';
            $image_upload_folder_thumb = 'assets/uploads/portfolio1/thumb';

            if (!file_exists($image_upload_folder)) {
                mkdir($image_upload_folder, DIR_WRITE_MODE, true);
            }
            $this->upload_config = array(
                'upload_path' => $image_upload_folder,
                'allowed_types' => '*',
                'max_size' => 0,
                'remove_space' => TRUE,
                'encrypt_name' => TRUE,
            );
            $this->upload->initialize($this->upload_config);

            if (!empty($_FILES) && $this->input->post('token') == $verifyToken) {
                if (!$this->upload->do_upload('Filedata')) {
                    $upload_error = $this->upload->display_errors();
                    echo json_encode($upload_error);
                } else {
                    $file_info = $this->upload->data();
                    $resp[] = $file_info;

                    /*=======  Making Thumbnail of Image  ========*/
                    $config = array(
                        'source_image' => $file_info['full_path'], //path to the uploaded image
                        'new_image' => $image_upload_folder_thumb, //path to
                        'maintain_ratio' => true,
                        'width' => 273,
                        'height' => 206,
                        'create_thumb' => TRUE
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();
                }
                $user_id = $this->session->userdata("user_id");
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $user_id;
                $users_obj->get_user();
                $users_obj->modified_timestamp = strtotime("now");
                $users_obj->update_users();
                echo json_encode($resp);
            }
        }
    }

    public function sv_video_description()
    {
        $user_id = $this->session->userdata("user_id");
        $item_id = $this->input->post("item_id");
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $item_id;
        $media_obj->get_media();
        $media_obj->media_description = $this->input->post("video_description");
        $media_obj->modified = strtotime("now");
        $return_user = $media_obj->update_media();
       
        if ($return_user == true) {
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            echo json_encode(array('status' => true, 'success' => true));
        } else {
            echo json_encode(array('status' => false, 'success' => true));
        }
    }

    public function upload_image_drop($imagecalltype=null)
    {
        //Featured Image

        $user_id = $this->session->userdata("user_id");
        $url_params = $this->uri->uri_to_assoc('3');
        $callfrom = $url_params['callfrom'];
        $this->load->library('upload');
        $file = addslashes($_FILES['userfile']['name']);
        $explodedName = explode('.', $file, 2);
        $randname = rand(1, 678) . "fas" . rand(1589, 9258);
        $fielname = $explodedName[0] . "_" . $randname . "." . $explodedName[1];
        $_FILES['userfile']['name'] = $fielname;
        $config['upload_path'] = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE_UPLOAD;
        $config['allowed_types'] = 'jpg|jpeg|png';
        $config['max_size'] = '1000000';
        $config['overwrite'] = FALSE;
        $this->upload->initialize($config);
        $file_info = $this->upload->data();
        if ($this->upload->do_upload() == true) {
            $data_upload = $this->upload->data();
            $fielname = $data_upload['file_name'];
            $image_name = $fielname;

            $media_detail = array(
                "user_id" => $user_id,
                "media_name" => $image_name,
                "profile_image" => "0",
                "media_type" => "image",
                "modified" => strtotime("now"),
                "created" => strtotime("now")
            );
            if ($callfrom == "fullwidth") {
                
                $media_obj = new Tyr_media_entity();
                $media_obj->status_sl = 1;
                $media_obj->user_id = $user_id;
                $media_obj->media_featured = 1;
                $media_obj->modified = strtotime("now");
                $media_obj->update_media_status_by_user();
                
                $media_obj = new Tyr_media_entity();
                $media_obj->status_sl = 1;
                $media_obj->user_id = $user_id;
                $media_obj->media_type = "video";
                $media_obj->modified = strtotime("now");
                $media_obj->update_media_status_by_user_media_type();
                
                
                $media_detail["media_featured"] = 1;
            } else {
                /*$getlastImage = $this->getRow("SELECT *
                                FROM tyr_media WHERE  user_id = '".$user_id."'
                                AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                                ORDER BY media_sortorder DESC
                                LIMIT 0,1
                                ");
                if(is_array($getlastImage) && count($getlastImage)>0){
                    $media_detail['media_sortorder']=$getlastImage['media_sortorder']+1;
                    if($getlastImage['media_imagesection']=='1')
                       $media_detail['media_imagesection']='2';
                    else
                        $media_detail['media_imagesection']='1';
                }else{
                    $media_detail['media_sortorder']='1';
                    $media_detail['media_imagesection']='1';
                }*/

            }
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->media_name = $image_name;
            $media_obj->profile_image = 0;
            $media_obj->media_type = "image";
            $media_obj->media_featured = 1;
            $media_obj->modified = strtotime("now");
            $media_obj->created = strtotime("now");
            $media_obj->save_media();
            $media_detail = (array)$media_obj;
            
            $return = $media_obj->image_id;
            
            if ($callfrom != "fullwidth") {
                $media_obj = new Tyr_media_entity();
                $media_obj->image_id = $return;
                $media_obj->get_media();
                $getlastImage = (array)$media_obj;
                $media_detail['media_imagesection'] = $getlastImage['media_imagesection'];
                $media_detail['media_sortorder'] = $getlastImage['media_sortorder'];
            }

            $media_detail['image_url'] = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $image_name;
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->status_sl = 1;
            $get_count_media = $media_obj->get_all_user_media_count();
            
            $scorer = $this->get_profile_completeness();
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS, 'media_id' => $return, 'callfrom' => $callfrom, 'media_detail' => $media_detail, 'myvar' => $get_count_media['total_count'], 'scorer' => $scorer));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function upload_image_drop2($imagecalltype = null, $set_image_section_order=null) {
        //$set_image_section_order;
        $user_id = $this->session->userdata("user_id");
        //Gallery Images
        $str = file_get_contents('php://input');
        $get_file_type = $_SERVER['HTTP_X_FILE_TYPE'];
        $ext ="";
        switch ($get_file_type){
            case "image/jpeg":
                $ext = '.jpg';
                break;
            case "image/jpg":
                $ext = '.jpg';
                break;
            case "image/png":
                $ext = '.png';
                break;
            case "image/gif":
                $ext = '.gif';
                break;
        }
        $filename = md5(time() . uniqid()) . $ext;
        $upload_folder = FCPATH . "assets/uploads/portfolio1/";
        ///str_replace('/','\\','C:\wamp\www\tyroe\assets/uploads/portfolio1');

        //$upload_folder;
        //file_put_contents("../../assets/uploads/portfolio1/".$filename,$str);
        $new_filename_path = $upload_folder. $filename;
        file_put_contents($new_filename_path, $str);
        //$new_filename_path

        $resize_img_filename = md5(time() . mt_rand(10,102)) . $ext;


        /*if(!is_writable($upload_folder) && is_dir($upload_folder)){
            chmod($upload_folder,0777);
        } */

        if ($imagecalltype == 'single_feat_img') {
            $ci_height=1500;
            $ci_width=1500;

        } else if ($imagecalltype == 'multi_latestwork') {
            $ci_height=570;
            $ci_width=570;
        }

        if($ci_width!=null && $ci_height!=null ){
            $this->resizeImage($upload_folder.$filename,$ci_width,$ci_height,$upload_folder.$resize_img_filename);
            //creating 300 x 300 image
            $this->resizeImage($upload_folder.$filename,300,260,$upload_folder.'300x300_'.$resize_img_filename);
            $this->resizeImage($upload_folder.$filename,600,520,$upload_folder.'modal_'.$resize_img_filename);
            //$this->resizeImage($upload_folder.$filename,300,260,$upload_folder.'215x215_'.$resize_img_filename);
        }

        if ($imagecalltype == 'single_feat_img') {
            
            
            $media_obj_1 = new Tyr_media_entity();
            $media_obj_1->status_sl = 1;
            $media_obj_1->user_id = $user_id;
            $media_obj_1->media_featured = 1;
            $media_obj_1->modified = strtotime("now");
            $media_obj_1->update_media_status_by_user();

            $media_obj_2 = new Tyr_media_entity();
            $media_obj_2->status_sl = 1;
            $media_obj_2->user_id = $user_id;
            $media_obj_2->media_type = "video";
            $media_obj_2->modified = strtotime("now");
            $media_obj_2->update_media_status_by_user_media_type();
            
            $media_detail["media_featured"] = 1;

            $media_obj_3 = new Tyr_media_entity();
            $media_obj_3->user_id = $user_id;
            $media_obj_3->media_name = $resize_img_filename;
            $media_obj_3->profile_image = 0;
            $media_obj_3->media_original_name = $filename;
            $media_obj_3->media_type = "image";
            $media_obj_3->image_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $resize_img_filename;
            $media_obj_3->media_featured = 1;
            $media_obj_3->modified = strtotime("now");
            $media_obj_3->created = strtotime("now");
            $media_obj_3->status_sl = 1;
            $media_obj_3->save_media();
            $media_detail = (array)$media_obj_3;

            $return = $media_obj_3->image_id;
            
            $media_detail['image_url'] = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $image_name;
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->status_sl = 1;
            $get_count_media = $media_obj->get_all_user_media_count();
            
            $scorer = $this->get_profile_completeness();
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS, 'media_id' => $return, 'callfrom' => $callfrom, 'media_detail' => $media_detail, 'myvar' => $get_count_media['total_count'], 'scorer' => $scorer));


        } else if ($imagecalltype == 'multi_latestwork') {
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->media_type = 'image';
            $media_obj->media_featured = 0;
            $media_obj->status_sl = 1;
            $media_obj->profile_image = 0;
            $media_obj->media_imagesection = $set_image_section_order;
            $media_obj->modified = strtotime("now");
            //$media_obj->update_media_sort_order();

            //
            /*
             * $old_uploaded_images_media_data = $this->session->userdata('uploaded_images_media_data');
            if (!empty($old_uploaded_images_media_data) && is_array($old_uploaded_images_media_data)) {
                $array_mergedata = array_merge($media_detail, array(1 => $old_uploaded_images_media_data));
            } else {
                $array_mergedata = $media_detail;
            }
            $this->session->set_userdata('uploaded_images_media_data', $array_mergedata);
             *
             *
             CODE FOR Z-STYLE IMAGES LAYOUT
            $queue_time = time();
            $queue_data = array("user_id" => $user_id, "queue_time" => $queue_time);
            $queue_id = $this->insert(TABLE_IMAGE_QUEUE, $queue_data);
            $checkqueue = true;


             * while($checkqueue) {

                //$queue_rs= $this->db->query("select * from ".TABLE_IMAGE_QUEUE." WHERE user_id='".$user_id."' ORDER BY queue_id limit 0,1  ");
                $queue_rs = $this->db->select('*')->from(TABLE_IMAGE_QUEUE)->where(array('user_id' => $user_id))->order_by('queue_id')->limit('1')->get()->result_array();

                //print_r($queue_rs);
                //echo $queue_id."<br>";
                if ($queue_rs[0]['queue_id'] == $queue_id) {
                    $checkqueue = false;
                } else {
                    if (time() - $queue_rs[0]['queue_time'] > 10) {
                        $this->db->query("DELETE FROM  " . TABLE_IMAGE_QUEUE . " WHERE queue_id='" . $queue_id . "'  ");
                    }
                    usleep(1000);
                }

            } */

            $url_params = $this->uri->uri_to_assoc('3');
            $callfrom = $url_params['callfrom'];
            
            
            
            //echo $this->db->last_query();
            //ADDED BY FAISAL-RAMEEZ

            //$update_orderdata = array('media_sortorder'=>$media_sortorder);
            //$where_orderdata = array('user_id' => $user_id, 'media_type' => 'image', 'media_featured' => 0, 'status_sl' => 1, 'profile_image' => 0  );
            //$this->update(TABLE_MEDIA,$update_orderdata,$where_orderdata);
            ///echo $this->db->last_query();
            //die;


            /*if ($this->upload->do_upload() == true) {*/


            $data_upload = $this->upload->data();
            $fielname = $data_upload['file_name'];
            $image_name = $fielname;
            $this->custom_crop_image($upload_folder.'300x300_'.$resize_img_filename,188,164,$ext);

            $max_sort_order_obj = new Tyr_media_entity();
            $max_sort_order_obj->user_id = $user_id;
            $max_sort_order = intval($max_sort_order_obj->get_max_sort_order()) + 1;
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->media_name = $resize_img_filename;
            $media_obj->profile_image = 0;
            $media_obj->media_sortorder = 1;
            $media_obj->media_imagesection = $set_image_section_order;
            $media_obj->media_sortorder = $max_sort_order;
            $media_obj->media_original_name = $filename;
            $media_obj->media_type = "image";
            $media_obj->image_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $resize_img_filename;
            $media_obj->media_featured = 0;
            $media_obj->status_sl = 1;
            $media_obj->modified = strtotime("now");
            $media_obj->created = strtotime("now");
            $media_obj->save_media();
            $media_detail = (array)$media_obj;
            $return = $media_obj->image_id;

            //$this->db->query("DELETE FROM  " . TABLE_IMAGE_QUEUE . " WHERE queue_id='" . $queue_id . "'  ");

            $media_detail['image_url'] = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $image_name;
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->status_sl = 1;
            $get_count_media = $media_obj->get_all_user_media_count();
            
            $scorer = $this->get_profile_completeness();
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $user_id;
            $users_obj->get_user();
            $users_obj->modified_timestamp = strtotime("now");
            $users_obj->update_users();
            
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_SUCCESS, 'media_id' => $return, 'callfrom' => $callfrom, 'media_detail' => $media_detail, 'myvar' => $get_count_media['total_count'], 'scorer' => $scorer));
            /* } else {
                 echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
             }*/

        }
    }

    public function latestwork_update()
    {
        $user_id = $this->session->userdata("user_id");
        $sort_order_id = $this->input->post('sort_order_id');
        $sort_order_idz = array_filter(explode(',',$sort_order_id));

        $sort_order_num = $this->input->post('sort_order_num');
        $sort_order_numz = array_filter(explode(',',$sort_order_num));

        $sort_order_section = $this->input->post('sort_order_section');
        $sort_order_sectionz = array_filter(explode(',',$sort_order_section));
        $foreach_inc = 0;
        foreach($sort_order_idz as $sort_orderid_key=>$sort_orderid_key_val){
            $foreach_inc++;
            
            $sort_orderid_key;
            $sort_ordernum = $sort_order_numz[$sort_orderid_key];
            $sort_ordersection = $sort_order_sectionz[$sort_orderid_key];
            
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $sort_orderid_key;
            $media_obj->user_id = $user_id;
            $media_obj->get_media_by_user();
            $media_obj->media_sortorder = $sort_ordernum;
            $media_obj->media_imagesection = $sort_ordersection;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            $update_array[] = array('image_id' =>$sort_orderid_key, 'order_number' =>$sort_ordernum, 'order_section' =>$sort_ordersection);
            /*
             * $sort_ordernum=$sort_order_numz[$sort_orderid_key];
            $sort_ordersection=$sort_order_sectionz[$sort_orderid_key];
            $update_array[] = array('image_id' =>$sort_orderid_key, 'order_number' =>$sort_ordernum, 'order_section' =>$sort_ordersection);
            $this->update(TABLE_MEDIA,array('media_sortorder'=>$sort_ordernum,'media_imagesection'=> $sort_ordersection),array("image_id"=>$sort_orderid_key,"user_id"=>$user_id));

             */
            
           // $update_array[] = array('image_id' =>$sort_orderid_key, 'order_number' =>$sort_ordernum, 'order_section' =>$sort_ordersection);
            //$this->update(TABLE_MEDIA,array('media_sortorder'=>$sort_ordernum,'media_imagesection'=> $sort_ordersection),array("image_id"=>$sort_orderid_key,"user_id"=>$user_id));
            //echo $this->db->last_query();
            //echo '<br /.';
        }

        //echo '<pre>';print_r($update_array);echo '</pre>';


        //$sort_order_num = $this->input->post('sort_order_num');
        //$sort_order_section = $this->input->post('sort_order_section');
        exit;
        //EXIT FOR TEMPERORY
        $user_id = $this->session->userdata("user_id");
        $str_sortorder = $this->input->post('sortorder');
        $str_sortorder = substr($str_sortorder, 0, -1);
        $str_sortorder = str_replace('portfolio-image-column1=', '', $str_sortorder);
        $str_sortorder = str_replace('portfolio-image-column2=', '', $str_sortorder);
        $str_sortorder = str_replace('portfolioimage-', '', $str_sortorder);
        $arr_sortorder = @explode('&', $str_sortorder);
        $arr_column1 = @explode(',', $arr_sortorder[0]);
        $arr_column2 = @explode(',', $arr_sortorder[1]);
        $arr_column1_image_id = array();
        $arr_column1_sort_order = array();
        $arr_column2_image_id = array();
        $arr_column2_sort_order = array();
        foreach ($arr_column1 as $k => $v) {
            $tmp_arr = array();
            $tmp_arr = @explode('-', $v);
            $arr_column1_image_id[] = $tmp_arr[0];
            $arr_column1_sort_order[] = $tmp_arr[1];
        }

        foreach ($arr_column2 as $k => $v) {
            $tmp_arr = array();
            $tmp_arr = @explode('-', $v);
            $arr_column2_image_id[] = $tmp_arr[0];
            $arr_column2_sort_order[] = $tmp_arr[1];
        }
        sort($arr_column1_sort_order);
        sort($arr_column2_sort_order);
        foreach ($arr_column1_image_id as $k => $v) {
            
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $v;
            $media_obj->get_media();
            $media_obj->media_sortorder = $arr_column1_sort_order[$k];
            $media_obj->media_imagesection = 1;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            
        }
        foreach ($arr_column2_image_id as $k => $v) {
            
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $v;
            $media_obj->get_media();
            $media_obj->media_sortorder = $arr_column2_sort_order[$k];
            $media_obj->media_imagesection = 2;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            
        }
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('status' => true, 'success' => true));
    }

    public function latestwork_remove() {

        //Remove Gallery Images
        //update other images order
        $user_id = $this->session->userdata("user_id");
        $image_id = $this->input->post('image_id');

        //$re_order_idz=  array_filter($this->input->post('re_order_idz'));
        //$re_order_num=  array_filter($this->input->post('re_order_num'));
        //$new_index_array = array_combine($re_order_idz, $re_order_num);


//        foreach($new_index_array as $img_id=>$img_order){
//            //$img_id.'__$img_id';
//            //$img_order.'__$img_order';
//            $media_obj = new Tyr_media_entity();
//            $media_obj->image_id = $img_id;
//            $media_obj->get_media();
//            $media_obj->media_sortorder = $img_order;
//            $media_obj->modified = strtotime("now");
//            $media_obj->update_media();
//            //echo '<br />';
//            //echo $this->db->last_query();
//        }
        
//        $media_obj = new Tyr_media_entity();
//        $media_obj->image_id = $image_id;
//        $media_obj->get_media();
//        $media_sortorder = $media_obj->media_sortorder;

        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $user_id;
        $media_obj->status_sl = 1;
        $media_obj->media_featured = 0;
        $media_obj->media_type = 'image';
        $media_obj->profile_image = 0;
        $get_count_media = $media_obj->get_all_user_media_count_1();
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $image_id;
        $media_obj->get_media();
        $media_obj->status_sl = -1;
        $media_obj->modified = strtotime("now");
        $media_obj->update_media();


        //echo $this->db->last_query();
//        if($media_sortorder){
//            $this->db->set('media_sortorder', '`media_sortorder`-1', FALSE);
//            $where_orderdata = array('user_id' => $user_id, 'media_type' => 'image', 'media_featured' => 0, 'status_sl' => 1, 'profile_image' => 0, 'media_sortorder > ' => $media_sortorder);
//            $this->db->where($where_orderdata);
//            $this->db->update(TABLE_MEDIA);
//            //echo $this->db->last_query();
//            //echo '___ <br />';
//        }

        $scorer = $this->get_profile_completeness();
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('status' => true, 'success' => true, 'cnt' => $get_count_media['total_count'], 'scorer' => $scorer));
    }

    public function latestwork_desc()
    {
        $user_id = $this->session->userdata("user_id");
        $image_id = $this->input->post('image_id');
        $description = $this->input->post('description');
        $media_title = $this->input->post('image_title');
        $media_tag = $this->input->post('image_tag');
        //
        $media_tag_for_db = implode(',',$media_tag);
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $image_id;
        $media_obj->get_media();
        $image_exists = (array)$media_obj;

        if ($media_tag == '_undefined!_') {
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $image_id;
            $media_obj->get_media();
            $media_obj->media_description = $description;
            $media_obj->media_title = $media_title;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
        } else {
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $image_id;
            $media_obj->get_media();
            $media_obj->media_description = $description;
            $media_obj->media_title = $media_title;
            $media_obj->media_tag = $media_tag_for_db;
            $media_obj->modified = strtotime("now");
            $media_obj->update_media();
            $media_tag_array = $media_tag;


            $image_exists_filtered = array_filter($image_exists);
            if (is_array($image_exists) && !empty($image_exists_filtered)) {
                //$tagrel_idz_where_in = array();
                $tag_id = null;
                $media_tag_array_filtered = array_filter($media_tag_array);
                if (empty($media_tag_array_filtered)) {
                    $gallery_tags_rel_obj = new Tyr_gallery_tags_rel_entity();
                    $gallery_tags_rel_obj->status = -1;
                    $gallery_tags_rel_obj->media_image_id = $image_id;
                    $gallery_tags_rel_obj->update_media_tag_image_status();
                } else {
                    foreach ($media_tag_array as $mediatag => $tag_val) {
                        $tag_id = null;
                        $gallery_tags_obj = new Tyr_gallery_tags_entity();
                        $gallery_tags_obj->tag_name = $tag_val;
                        $is_exists = $gallery_tags_obj->check_tag_exist();
                        
                        $tag_id = $is_exists[0]['tag_id'];
                        if (!$tag_id) {
                            
                            $gallery_tags_obj = new Tyr_gallery_tags_entity();
                            $gallery_tags_obj->tag_name = $tag_val;
                            $gallery_tags_obj->tag_add_by = $user_id;
                            $gallery_tags_obj->tag_add_time = time();
                            $gallery_tags_obj->tag_status = 1;
                            $gallery_tags_obj->save_gallery_tags();
                            $tag_id = $this->db->insert_id();
                        }
                        $gallery_tags_rel_obj = new Tyr_gallery_tags_rel_entity();
                        $gallery_tags_rel_obj->media_image_id = $image_id;
                        $gallery_tags_rel_obj->gallerytag_name_id = $tag_id;
                        $already_tag_rel = $gallery_tags_rel_obj->get_already_tag_ref();
                        
                        //echo '<pre>';print_r($already_tag_rel);echo '</pre>';

                        $tag_rel_status = $already_tag_rel[0]['status'];
                        $already_tag_filtered = array_filter($already_tag_rel);
                        if (is_array($already_tag_rel) && !empty($already_tag_filtered)) {
                            $tagrel_idz_where_in[] = $already_tag_rel[0]['gallerytag_id'];

                            if ($tag_rel_status != '1') {
                                $gallery_tags_rel_obj = new Tyr_gallery_tags_rel_entity();
                                $gallery_tags_rel_obj->media_image_id = $image_id;
                                $gallery_tags_rel_obj->gallerytag_name_id = $tag_id;
                                $gallery_tags_rel_obj->status = 1;
                                $gallery_tags_rel_obj->update_tag_rel_status();
                            }

                        } else {
                            $gallery_tags_rel_obj = new Tyr_gallery_tags_rel_entity();
                            $gallery_tags_rel_obj->media_image_id = $image_id;
                            $gallery_tags_rel_obj->gallerytag_name_id = $tag_id;
                            $gallery_tags_rel_obj->status = 1;
                            $gallery_tags_rel_obj->add_time = time();
                            $gallery_tags_rel_obj->save_gallery_tags_rel();
                            $tagrel_idz_where_in[] = $gallery_tags_rel_obj->gallerytag_id;
                        }
                    }

                    if (is_array($tagrel_idz_where_in) && !empty($tagrel_idz_where_in)) {
                        $where_not_in_update = implode(',', $tagrel_idz_where_in);
                        $gallery_tags_rel_obj = new Tyr_gallery_tags_rel_entity();
                        $gallery_tags_rel_obj->media_image_id = $image_id;
                        $gallery_tags_rel_obj->status = -1;
                        $gallery_tags_rel_obj->update_status_name_id_not_in($where_not_in_update);
                        //$this->db->last_query();

                        //$this->db->set(array('status1'=>'-1'));
                        //$this->db->where_not_in($where_not_in_update);
                        //$this->db->update(TABLE_GALLERY_TAGS_REL)->where_not_in($where_not_in_update)->where( array('media_image_id'=>$image_id) );
                    }

                }

            }


        }

        //echo $this->db->last_query();


        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('status' => true, 'success' => true));
    }

    public function loadmore_images() {
        //$user_id = $this->session->userdata("user_id");
        $last_id = $this->session->userdata("last_id");
        //THIS FUNTION MOVED ON LOADMORE.PHP CONTROLLER
        die('Forbidden');
        exit;

        $column1_start = $this->input->post('column1');
        $column2_start = $this->input->post('column2');
        $total_limit = 20;
        $next_start_limit = $column1_start;
        $joins_obj = new Tyr_joins_entity();
        $latestWorkImages = $joins_obj->get_all_latest_work_media($user_id,$column1_start);

        $latestWorkImagesColumn1 = $latestWorkImages;
        if (is_array($latestWorkImages) && count($latestWorkImages) > 0) {
            $next_start_limit += count($latestWorkImages);
            $total_limit = $total_limit - count($latestWorkImages);
        }
        $latestWorkImagesColumn1Start = $next_start_limit;
        $next_start_limit = $column2_start;
        
        $joins_obj = new Tyr_joins_entity();
        $latestWorkImages = $joins_obj->get_all_latest_work_media_with_limit($user_id,$column2_start,$total_limit);
        
        $latestWorkImagesColumn2 = $latestWorkImages;
        if (is_array($latestWorkImages) && count($latestWorkImages) > 0) {
            $next_start_limit += count($latestWorkImages);
            $total_limit = $total_limit - count($latestWorkImages);
        }
        $latestWorkImagesColumn2Start = $next_start_limit;
        echo json_encode(array('status' => true, 'success' => true, 'column1_start' => $latestWorkImagesColumn1Start, 'column2_start' => $latestWorkImagesColumn2Start,
            'column1_images' => $latestWorkImagesColumn1,
            'column2_images' => $latestWorkImagesColumn2,
        ));
    }

    public function cropImage($is_user_dp = false)
    {
        $user_id = $this->session->userdata("user_id");
        $cropImageX = $this->input->post('cropImageX');
        $cropImageY = $this->input->post('cropImageY');
        $cropImageH = $this->input->post('cropImageH');
        $cropImageW = $this->input->post('cropImageW');
        $cropImageUrl = $this->input->post('cropImageUrl');
        $image_id = $this->input->post('image_id');
        $extension = trim(substr($cropImageUrl, -3, 3));
        $data = array('cropImageX' => $cropImageX, 'cropImageY' => $cropImageY, 'cropImageH' => $cropImageH, 'cropImageW' => $cropImageW);
        $data = serialize($data);
        $targ_w = $targ_h = 256;
        $jpeg_quality = 90;
        //$src = ASSETS_PATH_PROFILE_IMAGE.$cropImageUrl;
        $src = dirname(__FILE__) . '/../../assets/uploads/profile/' . $cropImageUrl;
        $t_width = 230; // Maximum thumbnail width
        $t_height = 230; // Maximum thumbnail height
        $ratio = ($t_width / $cropImageW);
        $nw = ceil($cropImageW * $ratio);
        $nh = ceil($cropImageH * $ratio);
        $nimg = imagecreatetruecolor($nw, $nh);
        switch ($extension) {
            case 'png':
                $im_src = imagecreatefrompng($src);
                break;
            case 'jpg':
                $im_src = imagecreatefromjpeg($src);
                break;
            case 'gif':
                $im_src = imagecreatefromgif($src);
                break;
        }
        imagecopyresampled($nimg, $im_src, 0, 0, $cropImageX, $cropImageY, $nw, $nh, $cropImageW, $cropImageH);
        imagejpeg($nimg, dirname(__FILE__) . '/../../assets/uploads/profile/crop/' . $cropImageUrl, 90);
        
        $media_obj = new Tyr_media_entity();
        $media_obj->status_sl = -1;
        $media_obj->profile_image = 1;
        $media_obj->user_id = $user_id;
        $media_obj->update_user_profile_image_status();
        
        /*$media_detail = $this->getRow("SELECT * FROM " . TABLE_MEDIA . " WHERE image_id = '".$this->session->userdata('last_uploaded_user_dispalyimage_id')."'");
        if (is_array($media_detail) && count($media_detail) > 0) {
            $this->update(TABLE_MEDIA, array('media_crop_data' => $data, 'status_sl' => 1), array('user_id' => $user_id, 'image_id' => $media_detail['image_id']));
        }*/
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $image_id;
        $media_obj->get_media();
        $media_obj->media_crop_data = $data;
        $media_obj->status_sl = 1;
        $media_obj->modified = strtotime("now");
        $media_obj->update_media();

        if ($is_user_dp === true) {
            $this->session->unset_userdata('last_uploaded_user_dispalyimage_id');
            $this->session->unset_userdata('uploaded_user_img');
            $this->session->unset_userdata('old_user_dispalyimage_id');
        }
        $scorer = $this->get_profile_completeness();
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('status' => true, 'success' => true, 'scorer' => $scorer, 'query' => $this->db->last_query()));
        exit;
    }

    public function removeProfileImage()
    {
        $user_id = $this->session->userdata("user_id");
        
        $media_obj = new Tyr_media_entity();
        $media_obj->status_sl = -1;
        $media_obj->user_id = $user_id;
        $media_obj->media_type = "image";
        $media_obj->profile_image = 1;
        $media_obj->modified = strtotime('now');
        $media_obj->update_user_profile_image_status_1();
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('success' => true, 'status' => true));
    }

    public function remove_custom_theme()
    {

        $user_id = $this->session->userdata("user_id");
        $custome_theme_obj = new Tyr_custome_theme_entity();
        $custome_theme_obj->status_sl = -1;
        $custome_theme_obj->user_id = $user_id;
        $custome_theme_obj->update_user_custome_theme_status();
        
        $this->session->unset_userdata('gallery_theme');
        $this->session->unset_userdata('featured_theme');
        $this->session->unset_userdata('profile_theme');
        $this->session->unset_userdata('resume_theme');
        $this->session->unset_userdata('footer_theme');
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
        echo json_encode(array('success' => true, 'status' => true));
    }

    public function save_profile()
    {
        /*$str = file_get_contents('php://input');
          echo $filename = md5(time().uniqid()).".jpg";
          file_put_contents(dirname(__FILE__)."/../../assets/uploads/portfolio1/thumb/".$filename,$str);
          echo '<pre>';print_r($_REQUEST );echo '</pre>';die('Call');*/

        $user_id = $this->session->userdata("user_id");
        $callfrom = $this->input->get_post("callfrom");

        if ($callfrom == "") {
            $url_params = $this->uri->uri_to_assoc('3');
            if (isset($url_params['callfrom'])) {
                $callfrom = $url_params['callfrom'];
            }
        }
        switch ($callfrom) {

            //get_mediatags

            case 'get_mediatags':
                $search_by_tagname = $this->input->get("q");
                $get_mediatags = $this->get_mediatags($search_by_tagname);
                echo json_encode($get_mediatags);
                exit;
                break;
            case 'basic_detail':
                $this->sv_user_names();
                exit;
                break;
            case 'biograph':
                $this->sv_user_biography();
                exit;
                break;
            case 'profile_top':
                $this->sv_profile_top();
                exit;
                break;
            case 'publish_account':
                $this->sv_publish_account();
                exit;
                break;
            case 'social':
                $this->social_links();
                exit;
                break;
            case 'removesocial':
                $this->remove_social_link();
                exit;
                break;
            case 'remove_custom_theme':
                $this->remove_custom_theme();
                exit;
                break;
            case 'availablity':
                $this->user_availablity();
                exit;
                break;
            case 'experience':
                $this->sv_add_experience();
                exit;
                break;
            case 'removeexperience':
                $this->rm_experience();
                exit;
                break;
            case 'awards':
                $this->save_awards();
                exit;
                break;
            case 'removeawards':
                $this->rm_awards();
                exit;
                break;
            case 'education':
                $this->save_education();
                exit;
                break;
            case 'removeeducation':
                $this->rm_education();
                exit;
                break;
            case 'skill':
                $this->save_skills();
                exit;
                break;
            case 'removeskill':
                $this->remove_skills();
                exit;
                break;
            case 'sendrecommend':
                $this->send_recommendation();
                exit;
                break;
            case 'resendrecommend':
                $this->resend_recommendation();
                exit;
                break;
            case 'removerecommend':
                $this->remove_recommend();
                exit;
                break;
            case 'approverecommend':
                $this->approve_recommendation();
                exit;
                break;

            case 'remove-pending-recommend':
                $this->remove_recommend();
                exit;
                break;

            case 'remove-approved-recommend':
                $this->remove_recommend();
                exit;
                break;
            case 'video_url':
                $this->video_url();
                exit;
                break;
            case 'video_desc':
                $this->sv_video_description();
                exit;
                break;
            case 'latestwork':
                $imagecalltype = $this->uri->segment('5');
                $set_image_order = $this->uri->segment('6');
                $this->upload_image_drop2($imagecalltype, $set_image_order);
                exit;
                break;
            case 'fullwidth':
                $this->upload_image_drop();
                exit;
                break;
            /* case 'fullwidth':
                $this->portfolio-image-column2();
                exit;
            break;*/
            case 'latestwork_update':
                $this->latestwork_update();
                exit;
                break;
            case 'latestwork_description':
                $this->latestwork_desc();
                exit;
                break;
            case 'latestwork_remove':
                $this->latestwork_remove();
                exit;
                break;
            case 'visibility':
                $this->visibility_option();
                exit;
                break;
            case 'loadmore_images':
                $this->loadmore_images();
                exit;
                break;
            case 'cropimage':
                ///UPDATE QUERY HERE!!!
                $this->cropImage(true);
                exit;
                break;
            case 'removeprofileimage':
                $this->removeProfileImage();
                exit;
                break;
            case 'set_publish_message':
                $this->set_publish_message();
                exit;
                break;
            case 'recommendation_message_remove':
                $this->recommendation_message_remove();
                exit;
                break;
            case 'latestjob_message_remove':
                $this->latestjob_message_remove();
                exit;
                break;
            case 'latestshortlistjob_message_remove':
                $this->latestshortlistjob_message_remove();
                exit;
                break;
            case 'mail_to_profiler':
                $this->mail_to_profiler();
                exit;
                break;
            case 'warm_welcome':
                $this->warm_welcome();
                exit;
                break;
            case 'save_portfolio_order':
                $this->save_portfolio_order();
                exit;
                break;
            case 'get_last_upload_media_id':
                $this->get_last_upload_media_id();
                exit;
                break;
            case 'get_user_media_to_sort':
                $this->get_user_media_to_sort();
                exit;
                break;
            case 'save_user_media_desc':
                $this->save_user_media_desc();
                exit;
                break;
        }
        #Validation
        $config_validation = array(
            array(
                'field' => 'user_name',
                'label' => LABEL_USER_NAME,
                'rules' => 'trim|required'
            ),
            array(
                'field' => 'user_country',
                'label' => LABEL_COUNTRY,
                'rules' => 'trim|required'
            ),

            /* array(
                 'field' => 'user_cellphone',
                 'label' => LABEL_CELLPHONE,
                 'rules' => 'trim|required|numeric'
             )*/
        );
        $this->form_validation->set_rules($config_validation);
#Not Validate
        if ($this->form_validation->run() == FALSE) {
            $this->data['msg'] = "";
            $this->data['icon'] = "icon-remove-sign";
            $this->data['alert'] = "alert alert-danger";
            $this->template_arr = array('header', 'contents/edit_profile', 'footer');
            $this->load_template();
        } #Validate
        else {
            $this->data['alert'] = "alert alert-success";
            $this->data['icon'] = "icon-ok-sign";
            /* $duplicate_username = $this->getRow("SELECT count(*) as cnt from ".TABLE_USERS." where username='".$this->input->post("user_name")."'");
             if($duplicate_username['cnt']> 0){
                 $this->data['msg'] = LABEL_USERNAME_ALREADY_REGISTERED;
                 $this->template_arr = array('header', 'contents/edit_profile', 'footer');
                             $this->load_template();
             }else{*/
            $user_id = $this->session->userdata("user_id");
            $where = array("user_id" => $user_id);

            $profile_url = "tyroe." . $user_id;
            if ($this->session->userdata("role_id") == TYROE_PRO_ROLE) {
                $profile_url = $this->input->post("profile_url");
            }
            $state = $this->input->post("user_state1");
            if ($state == "") {
                $state = $this->input->post("user_state");
            }

            
            /* $user_confirmpass = trim($this->input->post("user_confirmpass"));
             #password edit
             if ($user_confirmpass != "") {
                 $user_details['password'] = $this->hashPass($user_confirmpass);
                 $user_details['password_len'] = strlen($user_confirmpass);
             }*/

            $user_obj = new Tyr_users_entity();
            $user_obj->user_id = $user_id;
            $user_obj->get_user();
            $user_obj->username = $this->input->post("user_name");
            $user_obj->firstname = $this->input->post("firstname");
            $user_obj->lastname = $this->input->post("lastname");
            $user_obj->profile_url = $profile_url;
            $user_obj->country_id = $this->input->post("user_country");
            $user_obj->tyroe_level = $this->input->post("tyroe_level");
            $user_obj->user_biography = $this->input->post("user_biography");
            $user_obj->user_occupation = $this->input->post("user_occupation");
            $user_obj->user_web = $this->input->post("user_web");
            $user_obj->user_experience = $this->input->post("user_experience");
            $user_obj->state = $state;
            $user_obj->city =  $this->input->post("user_city");
            $user_obj->cellphone = $this->input->post("user_cellphone_code") . "-" . $this->input->post("user_cellphone1");
            $user_obj->modified_timestamp = strtotime("now");
            $return_user = $user_obj->update_users();

            #Profile Image Upload

            $media_name = $this->input->post('imgname');
            if (!empty($media_name)) {
                
                $media_obj_1 = new Tyr_media_entity();
                $media_obj_1->user_id = $user_id;
                $media_obj_1->profile_image = 1;
                $media_obj_1->status_sl = 1;
                $old_profile_pic = $media_obj_1->get_user_profile_image();
                
                $media_obj_2 = new Tyr_media_entity();
                $media_obj_2->image_id = $old_profile_pic['image_id'];
                $media_obj_2->get_media();
                $media_obj_2->status_sl = -1;
                $media_obj_2->modified = strtotime("now");
                $media_obj_2->update_media();
                
                
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = user_id;
                $media_obj->profile_image = 1;
                $media_obj->media_type = 'image';
                $media_obj->media_name = $media_name;
                $media_obj->status_sl = 1;
                $media_obj->created = strtotime("now");
                $media_obj->modified = strtotime("now");
                $media_obj->save_media();
                
            }

            $s_arr = array();
            $s_id = $this->input->post("site_id");
            $s_name = $this->input->post("site_name");
            if (!empty($s_id[0])) {
                $user_sites_obj = new Tyr_user_sites_entity();
                $user_sites_obj->user_id = $user_id;
                $user_obj->delete_user_sites();
            }
            for ($sid = 0; $sid < count($s_id); $sid++) {
                if (!empty($s_id[$sid])) {
                    $s_arr[] = array('other_site_id' => $s_id[$sid], 'site_name' => $s_name[$sid]);
                }
            }
            foreach ($s_arr as $s_k => $s_v) {
                $user_sites_obj = new Tyr_user_sites_entity();
                $user_sites_obj->user_id = $user_id;
                $user_sites_obj->other_site_id = $s_v['other_site_id'];
                $user_sites_obj->site_name = $s_v['site_name'];
                $user_sites_obj->status_sl = 1;
                $user_sites_obj->save_user_sites();
            };
            if ($return_user > 0) {
                $this->session->set_userdata("update_profile", 1);
                header('location:' . $this->getURL("profile"));
                /*$this->template_arr = array('header', 'contents/edit_profile', 'footer');
                $this->load_template();*/
            }
        }
    }

    public function check_username()
    {
        $user_id = $this->session->userdata("user_id");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->username = $this->input->post("username");
        $users_obj->profile_url = $this->input->post("username");
        $duplicate_username = $users_obj->check_duplicate_username();
       
        if( ($this->session->userdata('role_id') == 3) || ($this->session->userdata('role_id') == 4) ){
            $duplicate_username = array("result"=>0,"role_id"=>$this->session->userdata("role_id"));
        } else {
            $duplicate_username = array("result"=>$duplicate_username['result'],"role_id"=>$this->session->userdata("role_id"));
        }
        echo json_encode($duplicate_username);
    }

    public function public_profile($data = "")
    {
        if (!empty($data)) {
            $this->data['param'] = $data;
            $this->data['recommend_id'] = $data;
        }

        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_profile($user_id);

        $image = '';
        if ($get_user['media_name']) {
            $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH . "img/no-image.jpg";
        }


        /*  $new_pass = "";
          for ($i = 1; $i <= $get_user['password_len']; $i++) {
              $new_pass .= " ";
          }*/
        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "password" => $new_pass,
            "image" => $image,
            "password_len" => $get_user['password_len'],
            "tyroe_level" => $get_user['tyroe_level'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id']
        );
        $this->data['get_user'] = $user_data;

        $this->template_arr = array('header', 'contents/public_profile', 'footer');

        $this->load_template();
    }

    public function public_recommendation($data)
    {
        //$data = "VjFaamVGSXlTWGhpUm1oUFZucFdiMVl3Vm5kaU1YQllUVmM1YUZJd05UQlZiVFZUVlRKS1ZXSkhSbGRpYmtKWVdrWmFjMVl5Umtaa1IyaFRUVlp2ZDFkV1ZtdGlNa1owVW1wYVYyRXdOVmRWYlhoSFRrWnJlV1ZJVGxoU01VcEpXVlZrZDFSck1WZGlNMmhZVm5wQmVGa3llRU5YUjFKSVlVZG9UbUV4YnpKWGExcHFUVmRTV0Zac1NsRldSRUU1";
        $data = $this->decoder_base($data);
        $this->data['param'] = $data;
        $this->data['recommend_id'] = $data;
        $this->data['page_title'] = 'Public Profile';
        
        $this->data['public_recommendation'] = 1;
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $data;
        $recommendation_obj->get_recommendation();
        $link_check = (array)$recommendation_obj;
        
        if ($link_check) {
            $user_id = $link_check['user_id'];
            $this->data['recommend_status'] = $link_check['recommend_status'];
        } else {
            $this->data['recommend_status'] = $link_check['recommend_status'];
        }

        /*$get_user = $this->getRow("SELECT " . TABLE_USERS . ".user_id,tyroe_level,role_id,parent_id," . TABLE_USERS . ".country_id,profile_url,email,
                       username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                       state,zip,suburb,display_name,created_timestamp,modified_timestamp, " . TABLE_USERS . ".status_sl,
                        " . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
                     LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                     LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id AND " . TABLE_MEDIA . ".profile_image = '1'
                     WHERE " . TABLE_USERS . ".user_id='" . $user_id . "'  ORDER BY image_id DESC LIMIT 0 ,1");*/


        $joins_obj = new Tyr_joins_entity();
        $get_user = $joins_obj->get_user_detail_info($user_id);
        
        $image;
        if ($get_user['media_name']) {
            $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
        } else {
            $image = ASSETS_PATH . "img/no-image.jpg";
        }

        $phone1 = explode("-", $get_user['cellphone']);
        $user_data = array(
            "user_id" => $get_user['user_id'],
            "email" => $get_user['email'],
            "username" => $get_user['username'],
            "firstname" => $get_user['firstname'],
            "lastname" => $get_user['lastname'],
            "lastname" => $get_user['lastname'],
            "user_biography" => $get_user['user_biography'],
            "image" => $image,
            "password_len" => $get_user['password_len'],
            "tyroe_level" => $get_user['tyroe_level'],
            "state" => $get_user['state'],
            "city" => $get_user['city'],
            "zip" => $get_user['zip'],
            "address" => $get_user['address'],
            "cellphone_code" => $phone1[0],
            "cellphone" => $phone1[1],
            "country" => $get_user['country'],
            "country_id" => $get_user['country_id'],
            "status_sl" => $get_user['status_sl']
        );
        $this->data['get_user'] = $user_data;

        $this->template_arr = array('header', 'contents/public_recommendation', 'footer');

        $this->load_template();

    }

    public function save_recommendation()
    {
        $recommend_id = $this->input->post("recommend_id");
        $recommend_name = $this->input->post("recom_name");
        $recom_dept = $this->input->post("recom_dept");
        $recom_company = $this->input->post("recom_company");
        $recommendation = $this->input->post("recom");
        
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $recommend_id;
        $recommendation_obj->get_recommendation();
        $recommendation_obj->recommendation = $recommendation;
        $recommendation_obj->recommend_name = $recommend_name;
        $recommendation_obj->recommend_dept = $recom_dept;
        $recommendation_obj->recommend_company = $recom_company;
        $recommendation_obj->recommend_status = -1;
        $recommendation_obj->update_all_data();
       
//        $data_recommendation = array(
//            "recommendation" => $recommendation,
//            "recommend_name" => $recommend_name,
//            "recommend_dept" => $recom_dept,
//            "recommend_company" => $recom_company,
//            "recommend_status" => -1
//        );
//        $this->update(TABLE_RECOMMENDATION, $data_recommendation, "recommend_id = '{$recommend_id}'");
        
               
        $recommendation_obj = new Tyr_recommendation_entity();
        $recommendation_obj->recommend_id = $recommend_id;
        $recommendation_obj->get_recommendation();
        $getdata_recommend = (array)$recommendation_obj;
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $getdata_recommend['user_id'];
        $users_obj->get_user();
        $getuser_data = (array)$users_obj;
        
        /*Email To Tyroe and Insert Stream on behalf of Tyore*/
        #Stream
        
        $activity_data = array(
            "job_title" => $getdata_recommend['recommend_name'],
            "company_name" => $getdata_recommend['recommend_company'],
            "search_keywords" => "received recommendation"
        );
        
        $job_activity_obj = new Tyr_job_activity_entity();
        $job_activity_obj->performer_id = $getdata_recommend['user_id'];
        $job_activity_obj->object_id = $getdata_recommend['user_id'];
        $job_activity_obj->activity_type = LABEL_RECOMMEND_COMPLETE;
        $job_activity_obj->activity_data = serialize($activity_data);
        $job_activity_obj->created_timestamp = strtotime("now");
        $job_activity_obj->status_sl = 1;
        $job_activity_obj->save_job_activity();

        //$check_notification_feedback_status = $this->getRow("SELECT * FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$getdata_recommend['user_id']."' AND option_id = 16");
        //if($check_notification_feedback_status['status_sl'] == 1)
        //{
            #Email
        $referrer_name = $getdata_recommend['recommend_name'];
        $referrer_company = $getdata_recommend['recommend_company'];
        $url = $this->getURL("profile/#acc-5");
        
        $notification_templates_obj = new Tyr_notification_templates_entity();
        $notification_templates_obj->notification_id = 'n07';
        $get_message = $notification_templates_obj->get_notification_templates_by_notification_id();
        
        $subject = $get_message['subject'];
        $subject =str_replace(array('[$referrer_name]','[$referrer_company]','[$url_approve_recommendation]','[$url_tyroe]'),array($referrer_name,$referrer_company,$url,LIVE_SITE_URL),$subject);
        $message = htmlspecialchars_decode($get_message['template']);
        $message = str_replace(array('[$referrer_name]','[$referrer_company]','[$url_approve_recommendation]','[$url_tyroe]'),array($referrer_name,$referrer_company,$url,LIVE_SITE_URL),$message);
        $current_user_email = $getuser_data['email'];
        $this->email_sender($current_user_email, $subject, $message);
        //}
        /*Email To Tyroe and Insert Stream on behalf of Tyore*/
        $recommend_id = $this->encoder_base($recommend_id);
        header("location:" . $this->getURL("profile/public_recommendation/" . $recommend_id));
        /*$this->template_arr = array('header', 'contents/public_recommendation', 'footer');
        $this->load_template();*/
    }

    public function get_state_by_country($country = "")
    {
        $bool = 0;
        $country = $this->input->post("country");
        $ddname = $this->input->post("ddname");
        $get_states = array();
        if ($country != "") {
            $bool = 1;
            $states_obj = new Tyr_states_entity();
            $states_obj->country_id = $country;
            $get_states = $states_obj->get_all_states_by_country();
        }
        
        $this->data['get_states'] = $get_states;
        $this->data['ddname'] = $ddname;
        if ($bool == 1) {
            $this->template_arr = array('contents/get_states');
            $this->load_template();
        }
        //return $get_states;
    }

    public function get_cities_by_country($country = "")
    {
        $bool = 0;
        $country = $this->input->post("country");
        $ddname = $this->input->post("ddname");
        $get_cities = array();
        if ($country != "") {
            $cities = new Tyr_cities_entity();
            $cities->country_id = $country;
            $get_cities = $cities->get_all_cities_by_country_id();
            $bool = 1;
        }
        
        $this->data['get_cities'] = $get_cities;
        $this->data['ddname'] = $ddname;
        if ($bool == 1) {
            $this->template_arr = array('contents/get_cities');
            $this->load_template();
        }
        //return $get_states;
    }

    public function save_image()
    {
        #Profile Image Upload
        $user_id = $this->session->userdata("user_id");
        $media_name = $this->input->post('imgname');
        if (!empty($media_name)) {
            /*$old_profile_pic = $this->getRow("Select image_id from " . TABLE_MEDIA . " where user_id='" . $user_id . "' and profile_image='1' and status_sl='1'");
            $where_media = "image_id = '" . $old_profile_pic['image_id'] . "'";
            $data = array(
                'modified' => strtotime("now"),
                'status_sl' => "-1"
            );
            $return = $this->update(TABLE_MEDIA, $data, $where_media);*/
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $media_obj->profile_image = 1;
            $media_obj->media_type = 'image';
            $media_obj->media_name = $media_name;
            $media_obj->status_sl = 1;
            $media_obj->created = strtotime("now");
            $media_obj->modified = strtotime("now");
            $media_obj->save_media();
            
            $image_id_id = $media_obj->image_id;
            $this->session->set_userdata('last_uploaded_user_dispalyimage_id', $image_id_id);
            $this->session->set_userdata('uploaded_user_img', 1);
            //$this->session->set_userdata('old_user_dispalyimage_id', $old_profile_pic['image_id']);
            //$this->data['image_id']=$image_id_id;
            if ($image_id_id) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_PROFILE_IMAGE_UPDATE, 'image_id' => $image_id_id));
            } else {
                echo json_encode(array('success' => false));
            }
        }
    }

    public function get_mediatags($search_where_title=null){
        
        if($search_where_title){
             $creative_skills_obj = new Tyr_creative_skills_entity();
             $skills = $creative_skills_obj->get_all_search_creative_skills($search_where_title);
        } else {
            $creative_skills_obj = new Tyr_creative_skills_entity();
            $skills = $creative_skills_obj->get_all_creative_skills_as();
        }
        return $skills;

    }
    public function delete_photo()
    {
        $user_id = $this->session->userdata("user_id");
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $this->input->post('image_id');
        $media_obj->get_media();
        $media_obj->status_sl = -1;
        $media_obj->modified = strtotime("now");
        $return = $media_obj->update_media();
        
        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_PROFILE_IMAGE_DELETED));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function theme_set()
    {
        //$this->input->is_ajax_request();
        if(!$this->input->is_ajax_request()){
            echo json_encode(array('success' => false));
            die;
        }
        $user_id = $this->session->userdata("user_id");
        $profile_theme_arr = array('text' => $this->input->post('p1_text'), 'background' => $this->input->post('p2_bg'), 'icons' => $this->input->post('p3_icons'), 'icons_rollover' => $this->input->post('p4_icon_rollover'));
        $featured_theme_arr = array('text' => $this->input->post('f1_text'), 'backgroundA' => $this->input->post('f2_bga'), 'backgroundB' => $this->input->post('f3_bgb'));
        $gallery_theme_arr = array('text' => $this->input->post('g1_text'), 'background' => $this->input->post('g2_bg'), 'overlay' => $this->input->post('g3_overlay'));
        $resume_theme_arr = array('text' => $this->input->post('r1_text'), 'background' => $this->input->post('r2_bg'));
        $footer_theme_arr = array('icons' => $this->input->post('foot1_icons'), 'icons_rollover' => $this->input->post('foot2_icons_rollover'), 'backgroundA' => $this->input->post('foot3_bga'));

        $this->session->set_userdata('profile_theme', $profile_theme_arr);
        $this->session->set_userdata('featured_theme', $featured_theme_arr);
        $this->session->set_userdata('gallery_theme', $gallery_theme_arr);
        $this->session->set_userdata('resume_theme', $resume_theme_arr);
        $this->session->set_userdata('footer_theme', $footer_theme_arr);
        
        $custome_theme_obj = new Tyr_custome_theme_entity();
        $custome_theme_obj->user_id = $user_id;
        $custome_theme_obj->get_custome_theme();
        $custome_theme_obj->profile_theme = serialize($profile_theme_arr);
        $custome_theme_obj->featured_theme = serialize($featured_theme_arr);
        $custome_theme_obj->gallery_theme = serialize($gallery_theme_arr);
        $custome_theme_obj->resume_theme = serialize($resume_theme_arr);
        $custome_theme_obj->footer_theme = serialize($footer_theme_arr);
        $custome_theme_obj->status_sl = 1;
        $return = $custome_theme_obj->update_custome_theme();
        if ($return) {
//            $modified_update = array(
//                'modified_timestamp' =>strtotime("now")
//            );
//            $where = array("user_id" => $user_id);
//            $this->update(TABLE_USERS,$modified_update,$where);
            
            
            echo json_encode(array('success' => true));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function set_publish_message(){
        $user_id = $this->session->userdata("user_id");
        $status = $this->input->post("status");
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        $users_obj->publish_account = $status;
        $users_obj->modified_timestamp = strtotime("now");
        $users_obj->update_users();
    }

    public function recommendation_message_remove(){
        $user_id = $this->session->userdata("user_id");
        $job_activity = new Tyr_job_activity_entity();
        $job_activity->object_id = $user_id;
        $job_activity->user_seen = 1;
        $job_activity->update_job_activity_user_seen();
        $this->session->set_userdata("recommendation_notification",0);
    }

    public function latestjob_message_remove(){
        $this->session->set_userdata("latest_jobs",0);
    }

    public function latestshortlistjob_message_remove(){
        $this->session->set_userdata("latest_shortlist_jobs",0);
    }

    public function warm_welcome(){
        $this->session->set_userdata("warm_welcome",0);
    }

    public function mail_to_profiler()
    {
        $profiler_id = $this->input->post('profiler_id');
        $email_sender_name = $this->input->post('email_sender_name');
        $email_sender_message = $this->input->post('email_sender_message');
        
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $profiler_id;
        $users_obj->get_user();
        $profiler_data = (array)$users_obj;
        
        $this->email_sender($profiler_data['email'],"Message from ".$email_sender_name,$email_sender_message);
        echo json_encode(array("success"=>true,"message"=>"Thank you! Your message has been sent."));
    }
    
    public function save_portfolio_order(){
        $order = $this->input->post('order');
        $remove_deleted_images = $this->input->post('remove_deleted_images');
        $i = 1;
        $media_available = array();
        foreach($order as $media_id){
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $media_id;
            $media_obj->get_media();
            $media_obj->media_sortorder = $i;
            $media_obj->update_media();
            $i++;
            $media_available[] = $media_id;
        }
        if($remove_deleted_images == 'true'){
            $media_ids = count($media_available) > 0 ? implode(",", $media_available) : '';
            $media_user_obj = new Tyr_media_entity();
            $media_user_obj->user_id = $this->session->userdata("user_id");
            $media_user_obj->remove_deleted_images($media_ids);
        }
        
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $this->session->userdata("user_id");
        $media_obj->media_featured = 0;
        $media_obj->profile_image = 1;
        $media_obj->status_sl = 1;
        $media_obj->media_type = 'image';
        $latestwork_portfolioimages = $media_obj->get_user_latest_portfolio_image();
        
        echo json_encode(array("status" => true, 'lastest_image' => $latestwork_portfolioimages));
    }
    
    public function get_last_upload_media_id(){
        $user_id = $this->session->userdata("user_id");
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $user_id;
        $result = $media_obj->get_last_upload_media_id();
        echo json_encode(array("status" => true,'image_id' => $result['image_id']));
    }
    
    public function get_user_media_to_sort(){
        $user_id = $this->session->userdata("user_id");
        
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $user_id;
        $media_obj->media_featured = 0;
        $media_obj->profile_image = 1;
        $media_obj->status_sl = 1;
        $media_obj->media_type = 'image';
        $latestwork_portfolioimages = $media_obj->get_user_latest_portfolio_image();
        
        echo json_encode(array("status" => true,'images' => $latestwork_portfolioimages));
    }
    
    public function save_user_media_desc(){
        $image_id = $this->input->post('image_id');
        $description = $this->input->post('description');
        
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $image_id;
        $media_obj->get_media();
        $media_obj->media_description = $description;
        $media_obj->update_media();
        
        echo json_encode(array("status" => true));
    }
}
