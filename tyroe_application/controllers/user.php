<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_type_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_hidden_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_reviewer_reviews_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_notification_templates_entity' . EXT);
require_once(APPPATH . 'entities/tyr_invite_tyroe_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_featured_tyroes_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_permissions_entity' . EXT);

class User extends Tyroe_Controller
{

    public function User()
    {
        parent::__construct();
        $this->load->library('form_validation');
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_USER_TYROE . ') ');
        } else {
            $this->get_system_modules();
        }
    }

    public function form()
    {
        $this->data['page_title'] = 'User Form';
        $item_id = $this->input->post('item_id');
        if ($item_id > 0) {
            $this->data['flag'] = "edit";
            $this->data['user_id'] = $item_id;
            
//            $get_user = $this->getRow("SELECT u.* FROM " . TABLE_USERS . " u LEFT JOIN " . TABLE_MEDIA . " m
//             ON (u.user_id=m.user_id AND m.profile_image='1')
//             WHERE u.user_id='" . $item_id . "'");
            $joins_obj = new Tyr_joins_entity();
            $get_user = $joins_obj->get_user_with_profile_image($item_id);
            
            if($get_user['role_id']==REVIEWER_ROLE)
            {
                //$studio_name = $this->getRow("SELECT username FROM ".TABLE_USERS." WHERE user_id='".$get_user['parent_id']."'");
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $get_user['parent_id'];
                $users_obj->get_user();
                $studio_name = (array) $users_obj;
                $this->data['studio_name']= $studio_name;
            }

            //$get_resume_data = $this->getAll("SELECT e.* FROM " . TABLE_EXPERIENCE . " e WHERE e.user_id='" . $item_id . "' AND status_sl != -1");

            $image = "";
            if ($get_user['media_name']) {
                $image = ASSETS_PATH . "uploads/profile/" . $get_user['media_name'];
            } else {
                $image = ASSETS_PATH . "img/no-image.jpg";
            }
            $new_pass = "";
            for ($i = 1; $i <= $get_user['password_len']; $i++) {
                $new_pass .= " ";
            }
            foreach ($get_user as $k => $v) {
                $this->data[$k] = stripslashes($v);
                if ($k == 'cellphone') {
                    $val_cell = explode("-", $v);
                    $this->data["code"] = $val_cell[0];
                    $this->data["number"] = $val_cell[1];
                }
            }

            $this->data["get_resume_data"] = $get_resume_data;

            //$get_portfolio = $this->getAll("SELECT p.* FROM " . TABLE_MEDIA . " p WHERE p.user_id='" . $item_id . "' AND p.profile_image='0'");
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $item_id;
            $media_obj->profile_image = 0;
            $get_portfolio = $media_obj->get_all_user_images_details();
            $this->data["get_portfolio"] = $get_portfolio;
            
            $get_states = array();
            $get_cities = array();
            if ($get_user['country_id'] != "") {
                $where_country_c = "AND country_id='" . $get_user['country_id'] . "'";
                
                $states_obj = new Tyr_states_entity();
                $states_obj->country_id = $get_user['country_id'];
                $get_states = $states_obj->get_all_states_by_country();

                $cities_obj = new Tyr_cities_entity();
                $cities_obj->country_id = $get_user['country_id'];
                $get_cities = $cities_obj->get_all_cities_by_country_id();
                
            }
            //$get_states = $this->getAll("SELECT states_id,states_name FROM " . TABLE_STATES . " Where 1=1 " . $where_country_c . " ORDER BY states_name ASC");
            //$get_cities = $this->getAll("SELECT city_id,city FROM " . TABLE_CITIES . " WHERE 1=1 " . $where_country_c . " ORDER BY city ASC");
            
            
            
            $this->data['get_states'] = $get_states;
            $this->data['get_cities'] = $get_cities;

        } else {
            $this->set_post_for_view();
        }

        //$this->template_arr = array('actions', 'header', 'contents/user/form', 'footer');
        /*$this->data['roles']=$this->getAll("Select role_id,role FROM ".TABLE_ROLES." where role_id!='".STUDIO_ROLE."'");
        $this->data['studios']=$this->getAll("Select user_id,username FROM ".TABLE_USERS." WHERE role_id='".STUDIO_ROLE."'");*/
        $content = "contents/user/form";
        $this->template_arr = array($content);
        $this->load_template();
    }

    public function listing()
    {
        $this->data['page_title'] = 'User Listing';
        $bool = $this->input->post('bool');
        $username = strtolower($this->input->post('username'));
        $filter_action = $this->input->post('filter_type');
        $sorting = $this->input->post('sorting');
        if ($username != "") {
            $where .= " AND ( (lower(u.username)  LIKE('%" . $username . "%') OR lower(u.firstname)  LIKE('%" . $username . "%') OR lower(u.lastname)  LIKE('%" . $username . "%')) or "
                    . "lower(concat(u.firstname, ' ', u.lastname)) LIKE('%" . $username . "%') )";
            $this->data['username']=$username;
        }
        $this->set_post_for_view();
        $orderby = "";
        if($sorting == true){
            $sort_column = $this->input->post('sort_column');
            $sort_type = $this->input->post('sort_type');
            $orderby = "ORDER BY ".$sort_column." ".$sort_type;
            $this->data['sorting'] = $sorting;
            $this->data['sort_column'] = $sort_column;
            $this->data['sort_type'] = $sort_type;
        } elseif ($filter_action == "mostrecent") {
            $orderby = " ORDER BY u.user_id DESC ";
            $this->data['filter_type']=$filter_action;
        } elseif ($filter_action == "alphabetical") {
            $orderby = " ORDER BY u.firstname ASC ";
            $this->data['filter_type']=$filter_action;
        } else {
            $orderby = "ORDER BY u.user_id DESC";
        }
        $pagintaion_limit = $this->pagination_limit();
        
        
//        $total_rows = $this->getRow("SELECT COUNT(*) AS cnt FROM ".TABLE_USERS." u WHERE  status_sl=1 AND u.role_id !='" . STUDIO_ROLE . "' ".$where);
//        $this->data['data'] = $this->getAll("SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_timestamp,u.status_sl,u.publish_account,
//                                            f.featured_status,tpv.visit_time FROM ".TABLE_USERS." u
//                                            LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id = f.featured_tyroe_id
//                                            LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv
//                                            ON u.user_id = tpv.user_id
//                                            WHERE  status_sl=1 AND u.role_id !='" . STUDIO_ROLE . "' ".$where."
//                                            GROUP BY u.user_id ". $orderby . $pagintaion_limit['limit']);
        
        
        $users_obj = new Tyr_users_entity();
        $users_obj->role_id = 0;
        $total_rows = $users_obj->get_search_listing_users_count($where);
        $joins_obj = new Tyr_joins_entity();
        $this->data['data'] = $joins_obj->get_search_listing_users_data(0,$where,$orderby,$pagintaion_limit);
        
        //echo '<pre>';print_r($pagintaion_limit );echo '</pre>';
        /*$this->data['data'] = $this->getAll("SELECT u.user_id,u.username,u.firstname,u.lastname,u.publish_account,(CASE WHEN  u.role_id = 1 THEN 'Admin' WHEN u.role_id = 2 THEN 'Tyroe'
                     WHEN u.role_id = 4 THEN 'Reviewer' WHEN u.role_id = 5 THEN 'Pro' END) AS role_type,c.country,u.email,s.status,
                     f.featured_status AS featured,tpv.visit_time,u.created_timestamp AS signed_up FROM " . TABLE_USERS . " u LEFT OUTER JOIN "
                    .TABLE_PAGEVIEWS." tpv ON u.user_id=tpv.user_id  LEFT JOIN " . TABLE_FEATURED_TYROE . "
                      f ON u.user_id = f.featured_tyroe_id, " . TABLE_COUNTRIES . " c, " . TABLE_STATUSES . " s WHERE 1 = 1 AND u.country_id=c.country_id AND s.status_id = u.status_sl
                      AND (u.role_id !='" . STUDIO_ROLE . "') " . $where . " And status_sl=1 GROUP BY u.user_id " . $orderby . $pagintaion_limit['limit']);*/
        //echo $this->db->last_query();

        /*$total_rows = $this->getRow('SELECT COUNT(*) AS cnt FROM ' . TABLE_USERS . ' u, ' . TABLE_COUNTRIES . ' c WHERE 1=1 AND c.country_id = u.country_id AND u.status_sl = 1
                            AND role_id!=' . STUDIO_ROLE . ' ' . $where . ' ORDER BY user_id ASC');*/
        /*Country Drop Down*/
        
        //$country_dropdown = $this->getAll('SELECT country_id, country FROM ' . TABLE_COUNTRIES);
        $countries_obj = new Tyr_countries_entity();
        $country_dropdown = $countries_obj->get_all_countries();
        
        
        $arr_dd = array();
        foreach ($country_dropdown as $dd_val) {
            $arr_dd[$dd_val['country_id']] = $dd_val['country'];
        }
        $this->data['country_dropdown'] = form_dropdown('new_dd', $arr_dd, $country, "id='new_dd'");
        /*Country Drop Down*/
        $this->data['pagination'] = $this->pagination(array('url' => 'user/index/page', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        if ($bool) {
            $this->template_arr = array('contents/user/get_tyroes_filter');
        } else {
            $this->template_arr = array('actions', 'header', 'contents/user/view', 'footer');
        }
        /*$this->template_arr = array('actions', 'header', 'contents/user/view', 'footer');*/
        $this->load_template();
    }

    public function check_email()
    {
//        $duplicate_email = $this->getRow("Select email from " . TABLE_USERS . " where email='" . $this->input->post("email") . "'");
//        if ($duplicate_email) {
//            echo $result['email'] = $duplicate_email['email'];
//        }
        //echo json_encode($duplicate_email);
        
        $users_obj = new Tyr_users_entity();
        $users_obj->email = $this->input->post("email");
        $users_obj->get_user_by_email();
        if ($users_obj->user_id != 0) {
            echo $result['email'] = $users_obj->email;
        }
    }

    public function save()
    {
        $role_id = "2";
        $item_id = $this->input->post("item_id");
        $user_email = $this->input->post('email');
        $user_password = $this->input->post('password');
        $user_confirmpass = $this->input->post('confirmpass');
        $password_length = strlen($this->input->post('confirmpass'));
        $user_firstname = $this->input->post('firstname');
        $user_lastname = $this->input->post('lastname');
        $user_country = $this->input->post('user_country');
        //$user_state = $this->input->post('user_state');
        $user_city = $this->input->post('user_city');
        $user_cellphone = $this->input->post("user_cellphone_code") . "-" . $this->input->post("cellphone");
        $flag = $this->input->post('flag');
        
        $user_handle_obj = new Tyr_users_entity();
        $user_handle_obj->firstname = $user_firstname;
        $user_handle_obj->lastname = $user_lastname;
        $profile_handle = $user_handle_obj->get_user_handle();

        $user_name = $profile_handle;
        
        
        $where = "";
        
        if ($flag == 'edit'){
            $where = " AND user_id!='".$item_id."'";
        }
        
        //$duplicate_email = $this->Execute("SELECT email FROM " . TABLE_USERS . " WHERE email='" . $user_email . "' ".$where);
        $users_obj = new Tyr_users_entity();
        $users_obj->email = $this->input->post("email");
        
        if($users_obj->check_duplicate_user_email($where)){
            echo json_encode(array("duplicate_email"=>true));
            exit;
        }else{
            //$duplicate_username =  $this->Execute("SELECT username FROM " . TABLE_USERS . " WHERE username='" . $user_name . "' ".$where);
            $users_obj = new Tyr_users_entity();
            $users_obj->username = $user_name;
            if($users_obj->check_duplicate_user_username($where)){
                echo json_encode(array("duplicate_username"=>true));
                exit;
            }
        }

        $password = $this->hashPass($user_confirmpass);
        $user_details = array(
            'role_id' => $role_id,
            'email' => $user_email,
            'username' => $user_name,
            'profile_url' => $user_name,
            'firstname' => $user_firstname,
            'lastname' => $user_lastname,
            'country_id' => $user_country,
            //'state' => $user_state,
            'city' => $user_city,
            'cellphone' => $user_cellphone,
            'created_timestamp' => strtotime('now'),
            'modified_timestamp' => strtotime('now'),
            'password' => $password,
            'password_len' => $password_length
        );
        
        if ($flag == "edit") {
            unset($user_details['created_timestamp']);
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $item_id;
            $users_obj->get_user();
            //$current_password = array('password' => $users_obj->password);
            
            //$users_obj->role_id = $role_id;
            $users_obj->email = $user_email;
            $users_obj->username = $user_name;
            //$users_obj->profile_url = $user_name;
            $users_obj->firstname = $user_firstname;
            $users_obj->lastname = $user_lastname;
            $users_obj->country_id = $user_country;
            $users_obj->city = $user_city;
            $users_obj->cellphone = $user_cellphone;
            //$users_obj->created_timestamp = strtotime('now');
            $users_obj->modified_timestamp = strtotime('now');
            
            //$current_password = $this->getRow("SELECT password FROM ".TABLE_USERS." WHERE user_id='".$item_id."'");
            if($user_password != $users_obj->password)
            {
                $users_obj->password = $password;
                $users_obj->password_len = $password_length;
            }
            
            //$this->update(TABLE_USERS, $user_details, array("user_id" => $item_id));
            $users_obj->update_users();
            
            
            
//            $record = $this->getRow("SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_timestamp,u.status_sl,u.publish_account,
//                                            f.featured_status,tpv.visit_time FROM ".TABLE_USERS." u
//                                            LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id = f.featured_tyroe_id
//                                            LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv
//                                            ON u.user_id = tpv.user_id
//                                            WHERE  u.user_id='".$item_id."'");
            $joins_obj = new Tyr_joins_entity();
            $record = $joins_obj->get_user_featured_details($item_id);
            
            if($record['visit_time']=="") $last_visit="Not Available";
            else $last_visit= date('M j,Y',$record['visit_time']);
            $featured ="";
            if($record['role_id']==1 || $record['role_id']==4){
                $featured= 'N/A';
            }elseif($record['publish_account'] == 0){
                $featured = "Not Published";
            }elseif($record['featured_status']=='1'){
                $featured = '<i class="icon-star" style="color: #f20;"></i>';
            }elseif($record['featured_status']=='0'){
                $featured = '<i class="icon-star-empty" style="color: #f20;"></i>';
            }elseif($record['featured_status']=='-1'){
                $featured = '<i class="icon-minus-sign" style="color:#D5D5D5;"></i>';
            }
            $group ="";
            if($record['role_id']==1){
                $group = "Admin";
            }elseif($record['role_id']==2){
                $group = "Tyroe";
            }elseif($record['role_id']==4){
                $group = "Reviewer";
            }elseif($record['role_id']==5){
                $group  = "pro";
            }
            $status ="";
            if($record['status_sl'] == 0){
                $status = '<span class="label label-info rt-pending">Pending</span>';
            }else{
                $status = '<span class="label label-success">Active</span>';
            }
            $html = '<td align="center"><input type="checkbox" name="bulk_check[]" class="tyroe_check"
                                               value="'.$record['user_id'].'"></td>
                     <td>'.$record['user_id'].'</td>
                     <td><p class="rt-name">'.$record['firstname'].' '.$record['lastname'].'</p></td>
                     <td>'.$featured.'</td>
                     <td>'.$group.'</td>
                     <td><a href="mailto:'.$record['email'].'">'.$record['email'].'</a></td>
                     <td>'.date('M j,Y',$record['created_timestamp']).'</td>
                     <td>'.$last_visit.'</td>
                     <td>'.$status.'</td>
                     <td><a href="javascript:void(0)" class="btn_pop" id="'.$record['user_id'].'">Edit</a> <a href="javascript:void(0)" class="user_delete" id="'.$record['user_id'].'">Delete</a> </td>';
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_UPDATE_SUCCESS, 'flag' => '0','html' => $html, 'id' => $item_id));
        } else {
            unset($user_details['modified_timestamp']);
            //$user_id = $this->insert(TABLE_USERS, $user_details);
            
            $users_obj = new Tyr_users_entity();
            $users_obj->role_id = $role_id;
            $users_obj->email = $user_email;
            $users_obj->username = $user_name;
            $users_obj->profile_url = $user_name;
            $users_obj->firstname = $user_firstname;
            $users_obj->lastname = $user_lastname;
            $users_obj->country_id = $user_country;
            $users_obj->city = $user_city;
            $users_obj->cellphone = $user_cellphone;
            $users_obj->password = $password;
            $users_obj->password_len = $password_length;
            $users_obj->created_timestamp = strtotime('now');
            $users_obj->modified_timestamp = strtotime('now');
            $users_obj->status_sl = 1;
            $users_obj->save_user();
            $user_id = $users_obj->user_id;
            $item_id = $users_obj->user_id;
//            $featured_details = array(
//                   'featured_tyroe_id' => $user_id,
//                   'featured_status' => 0
//            );
//           $return_featured = $this->insert(TABLE_FEATURED_TYROE,$featured_details);
            
            $featured_tyroes_obj = new Tyr_featured_tyroes_entity();
            $featured_tyroes_obj->featured_tyroe_id = $user_id;
            $featured_tyroes_obj->featured_status = 0;
            $return_featured = $featured_tyroes_obj->save_featured_tyroes();
            
            $modules = @explode(',', TYROE_FREE_USER_MODULES);
                foreach ($modules as $k => $v) {
                    
                $permissions_obj = new Tyr_permissions_entity();    
                $permissions_obj->user_id =  $user_id;
                $permissions_obj->module_id =  $v;
                $permissions_obj->save_permissions();
                
//                $user_modules = array(
//                    'user_id' => $user_id,
//                    'module_id' => $v);
//                $this->insert(TABLE_PERMISSIONS, $user_modules);
            }
            $profile_theme_arr = array('text'=>'#323A45', 'background'=>'', 'icons'=>'#D0DBE2', 'icons_rollover'=>'#F97E76');
            $featured_theme_arr = array('text'=>'#323A45', 'backgroundA'=>'#F97E76', 'backgroundB'=>'#E9F0F4');
            $gallery_theme_arr = array('text'=>'#323A45', 'background'=>'#FFFFFF', 'overlay'=>'rgba(255, 255 ,255, 0.5)', 'button'=>'#F97E76');
            $resume_theme_arr = array('text'=>'#323A45', 'background'=>'#E9F0F4');
            $footer_theme_arr = array('icon'=>'#D0DBE2', 'icon_rollover'=>'#F97E76', 'backgroundA'=>'#323A45');
            $theme_data = array(
                'user_id' => $user_id,
                'profile_theme' => serialize($profile_theme_arr),
                'featured_theme' => serialize($featured_theme_arr),
                'gallery_theme' => serialize($gallery_theme_arr),
                'resume_theme' => serialize($resume_theme_arr),
                'footer_theme' => serialize($footer_theme_arr),
                'status_sl' => '1',
            );
            //$this->insert(TABLE_CUSTOM_THEME, $theme_data);
            
            $custome_theme_obj = new Tyr_custome_theme_entity();
            $custome_theme_obj->user_id = $user_id;
            $custome_theme_obj->profile_theme = serialize($profile_theme_arr);
            $custome_theme_obj->featured_theme = serialize($featured_theme_arr);
            $custome_theme_obj->gallery_theme = serialize($gallery_theme_arr);
            $custome_theme_obj->resume_theme = serialize($resume_theme_arr);
            $custome_theme_obj->footer_theme = serialize($footer_theme_arr);
            $custome_theme_obj->status_sl = 1;
            $custome_theme_obj->save_custome_theme();
            
//            $record = $this->getRow("SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_timestamp,u.status_sl,u.publish_account,
//                                    f.featured_status,tpv.visit_time FROM ".TABLE_USERS." u
//                                    LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id = f.featured_tyroe_id
//                                    LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv
//                                    ON u.user_id = tpv.user_id
//                                    WHERE  u.user_id='".$item_id."'");

            $joins_obj = new Tyr_joins_entity();
            $record = $joins_obj->get_user_featured_details($item_id);
            
            if($record['visit_time']=="") $last_visit="Not Available";
            else $last_visit= date('M j,Y',$record['visit_time']);
            $featured ="";
            if($record['role_id']==1 || $record['role_id']==4){
                $featured= 'N/A';
            }elseif($record['publish_account'] == 0){
                $featured = "Not Published";
            }elseif($record['featured_status']=='1'){
                $featured = '<i class="icon-star" style="color: #f20;"></i>';
            }elseif($record['featured_status']=='0'){
                $featured = '<i class="icon-star-empty" style="color: #f20;"></i>';
            }elseif($record['featured_status']=='-1'){
                $featured = '<i class="icon-minus-sign" style="color:#D5D5D5;"></i>';
            }
            $group ="";
            if($record['role_id']==1){
                $group = "Admin";
            }elseif($record['role_id']==2){
                $group = "Tyroe";
            }elseif($record['role_id']==4){
                $group = "Reviewer";
            }elseif($record['role_id']==5){
                $group  = "pro";
            }
            $status ="";
            if($record['status_sl'] == 0){
                $status = '<span class="label label-info rt-pending">Pending</span>';
            }else{
                $status = '<span class="label label-success">Active</span>';
            }
            $html = '<tr id="tyroe_id_'.$record['user_id'].'">
                     <td align="center"><input type="checkbox" name="bulk_check[]" class="tyroe_check"
                                               value="'.$record['user_id'].'"></td>
                     <td>'.$record['user_id'].'</td>
                     <td><p class="rt-name">'.$record['firstname'].' '.$record['lastname'].'</p></td>
                     <td>'.$featured.'</td>
                     <td>'.$group.'</td>
                     <td><a href="mailto:'.$record['email'].'">'.$record['email'].'</a></td>
                     <td>'.date('M j,Y',$record['created_timestamp']).'</td>
                     <td>'.$last_visit.'</td>
                     <td>'.$status.'</td>
                     <td><a href="javascript:void(0)" class="btn_pop" id="'.$record['user_id'].'">Edit</a> <a href="javascript:void(0)" class="user_delete" id="'.$record['user_id'].'">Delete</a> </td>
                     </tr>';
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS, 'flag' => '1','html' => $html, 'id' => $user_id));

        }
    }

    //}

//This function terminated because all filter related work have been shifted to listing function of this controller...
   /* public function filter_tyroe()
    {
        $filter_type = $this->input->post('filter_type');
        if ($filter_type == "mostrecent") {
            echo "in most recent";
            $orderby = " ORDER BY u.user_id DESC ";
        } elseif ($filter_type == "alphabetical") {
            $orderby = " ORDER BY u.firstname ASC ";
        } else {

            $orderby = "";
        }
        $pagintaion_limit = $this->pagination_limit();
        $this->data['data'] = $this->getAll("SELECT u.user_id,u.username,u.firstname,u.lastname,(CASE WHEN  u.role_id = 1 THEN 'Admin' WHEN u.role_id = 2 THEN 'Tyroe'
                                 WHEN u.role_id = 4 THEN 'Reviewer' WHEN u.role_id = 5 THEN 'Pro' END) AS role_type,c.isocode3 AS country,u.email,s.status,
                                 f.featured_status AS featured,u.created_timestamp as signed_up,tpv.visit_time FROM " . TABLE_USERS . " u LEFT OUTER JOIN
                                 ".TABLE_PAGEVIEWS." tpv ON u.user_id=tpv.user_id LEFT JOIN " . TABLE_FEATURED_TYROE . " f ON u.user_id = f.featured_tyroe_id, " . TABLE_COUNTRIES . " c, " . TABLE_STATUSES . " s WHERE 1 = 1 AND c.country_id = u.country_id AND s.status_id = u.status_sl
                                  AND (u.role_id !='" . STUDIO_ROLE . "')  And u.status_sl='1' GROUP BY u.user_id " . $orderby . $pagintaion_limit['limit']);
        $this->data['pagination'] = $this->pagination(array('url' => 'user/index/page', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('contents/user/get_tyroes_filter');
        $this->load_template();
    }*/


    public function index()
    {

    }
}
