<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);

class gallery extends Tyroe_Controller {

    public function __construct() {
        parent::__construct();
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . ADMIN_MODULE_GALLERY . ') ');
        }
    }

    public function index() {
        $pagenum = $this->uri->segment('3');
        $perpage = $this->uri->segment('4');
        $this->data['gallery_data'] = $this->get_gallery_data_pagi($pagenum, $perpage);
        if (!IS_AJAX) {
            $this->data['page_title'] = 'Gallery';
            $this->data['load_masonry_file'] = true;
            $this->template_arr = array('header', 'contents/admin_gallery', 'footer');
        } else {
            echo $this->data['gallery_data'];
            die;
            //$this->template_arr = array('contents/admin_gallery');
        }
        $this->load_template();
    }

    protected function get_gallery_data_pagi($pagenum = null, $perpage = null) {
        $pagenum = preg_replace('/[^0-9]/', '', $pagenum);
        $perpage = preg_replace('/[^0-9]/', '', $perpage);
        $limit = preg_replace('/[^0-9]/', '', $this->input->get('limit'));
        $tyr_gallery_limit = preg_replace('/[^0-9]/', '', $this->session->userdata('tyr_gallery_limit'));
        $new_limit = 20;
        if ($tyr_gallery_limit == null && $limit == null) {
            $this->session->set_userdata('tyr_gallery_limit', $new_limit);
        } else if ($tyr_gallery_limit != $limit && $limit != null) {
            $this->session->set_userdata('tyr_gallery_limit', $limit);
            $new_limit = $limit;
        } else if ($limit != null) {
            $this->session->set_userdata('tyr_gallery_limit', $limit);
            $new_limit = $limit;
        } else if ($limit == null && $tyr_gallery_limit != null) {
            $new_limit = $tyr_gallery_limit;
        }

        if (!is_numeric($pagenum)) {
            $pagenum = 1;
        }
        $perpage = $new_limit;
        if (!is_numeric($perpage)) {

        }

        $where = array();
        $where['tm.status_sl'] = 1;

        $join_obj = new Tyr_joins_entity();
        $total_records_data = $join_obj->get_all_total_gallary_records_count();
        //var_dump($total_records_data);
//        $total_records_data = $this->db->select('count(*) as total_gallery_records')
//                ->from(TABLE_MEDIA . ' AS tm')
//                ->join(TABLE_USERS . ' AS tu', 'tu.user_id=tm.user_id', 'LEFT OUTER')
//                ->order_by('tm.image_id', 'DESC')
//                ->where($where)
//                ->get()
//                ->row_array();
        
//        var_dump($total_records_data);

        $total_rows = $total_records_data['total_gallery_records'];
        //193 $offset = $pagenum*floor($total_rows/$perpage);
        $this->load->library('pagination');
        $config['total_rows'] = $total_rows;
        $config['per_page'] = $perpage;
        $config['prev_link'] = '&#x2039;';
        $config['next_link'] = '&#x203A;';
        $config['num_links'] = 5;
        $config['first_tag_open'] = '<li class="hide first_link" style="display:none;">';
        $config['first_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li class="hide" style="display:none;">';
        $config['last_tag_close'] = '</li>';
        $config['base_url'] = base_url('gallery/page');
        $config['page_query_string'] = false;


        if ($pagenum != 1) {
            $offset = $perpage * ($pagenum - 1);
        } else {
            $config['prev_tag_open'] = '<li class="hide" style="display:none;">';
            $config['prev_tag_close'] = '</li>';
            $offset = 0;
        }

        $this->pagination->initialize($config);
        $pagination_link = $this->pagination->create_links();

        $gallery_data = $join_obj->get_gallery_records_pagination($perpage, $offset);
        
//        $sel_cols = '
//        tu.profile_url AS gallery_user_profile,
//        tm.image_id AS gallery_id,
//        tm.user_id AS gallery_user_id,
//        tm.media_type AS gallery_type,
//        tm.video_type AS gallery_video_type,
//        tm.media_name AS gallery_src,
//        tm.media_original_name AS original_src,
//        tm.media_url AS gallery_url,
//        tm.media_title AS gallery_title,
//        tm.created AS gallery_uploaded_time,
//        tm.status_sl AS gallery_status';
//        $gallery_data = $this->db->select($sel_cols)
//                ->from(TABLE_MEDIA . ' AS tm')
//                ->join(TABLE_USERS . ' AS tu', 'tu.user_id=tm.user_id', 'LEFT OUTER')
//                ->order_by('tm.image_id', 'DESC')
//                ->where($where)
//                ->limit($perpage, $offset)
//                ->get()
//                ->result_array();
        //print_array($this->db->last_query(),false,'__lastQUERY');
        $return_html = $sel_html . '<div id="fixed_masonry_layout">';
        foreach ($gallery_data as $glry_data) {
            $gallery_id = $glry_data['gallery_id'];
            $gallery_user_id = $glry_data['gallery_user_id'];
            $gallery_user_profile = $glry_data['gallery_user_profile'];
            $gallery_type = $glry_data['gallery_type'];
            $gallery_video_type = $glry_data['gallery_video_type'];
            $gallery_src = $glry_data['gallery_src'];
            $original_src = $glry_data['original_src'];
            $gallery_url = $glry_data['gallery_url'];
            $gallery_title = $glry_data['gallery_title'];
            $gallery_uploaded_time = $glry_data['gallery_uploaded_time'];
            $gallery_status = $glry_data['gallery_status'];
            $gallery_src_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $gallery_src;
            $gallery_src_original_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $original_src;

            $allow_data_type = array('video', 'image');
            if (!in_array($gallery_type, $allow_data_type)) {
                continue;
            }
            $video_url = null;

            if ($gallery_type == 'video') {
                if (strpos($gallery_src, 'http') === false) {
                    $gallery_src = 'http:' . $gallery_src;
                }
                $data_url = $gallery_src;
                $video_url = $gallery_url;
                if ($gallery_video_type == 'vimeo') {
                    $video_id_exploded = explode('/', $gallery_url);
                    foreach ($video_id_exploded as $video_key => $video_val) {
                        if (is_numeric($video_val)) {
                            $vimeo_vid_id = $video_val;
                            break;
                        }
                    }
                    if(intval($vimeo_vid_id) == 0){
                        continue;
                    }
                    $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$vimeo_vid_id.php"));
                    $gallery_src_url = $hash[0]['thumbnail_large'];
                } else if ($gallery_video_type == 'youtube') {

                    $youtube_vid_id = preg_replace("#[&\?].+$#", "", preg_replace("#http://(?:www\.)?youtu\.?be(?:\.com)?/(embed/|watch\?v=|\?v=|v/|e/|.+/|watch.*v=|)#i", "", $video_url));
                    if(stripos($youtube_vid_id, "youtube")){
                        $youtube_vid_id = explode("/", $youtube_vid_id);
                        $youtube_vid_id = end($youtube_vid_id);
                    }
                    $gallery_src_url = 'http://img.youtube.com/vi/' . $youtube_vid_id . '/0.jpg';
                    $video_url = str_replace('embed/', 'watch?v=', $video_url);
                }
            } else {
                $data_url = $gallery_src_original_url;
            }

            $gallery_img_size = getimagesize($gallery_src_url);
            if (strpos($gallery_src_url, 'youtube') === false) {

                if ($gallery_img_size === true || $gallery_img_size === false) {
                    continue;
                    $gallery_src_url = ASSETS_PATH_NO_IMAGE;
                    $title = 'title="image not available" width="100%"';
                }
            }
            $show_full_gallery_data_in_modal = null;
            if ($video_url) {
                $show_full_gallery_data_in_modal = ' show_full_gallery_data_in_modal';
            }

            $return_html.='<div data-type="' . $gallery_type . '" data-url="' . $data_url . '" class="span3' . $show_full_gallery_data_in_modal . ' gallery_id_' . $gallery_id . '">';
            if ($video_url) {
                //$return_html.='<a title="View Video" target="_blank" href="'.$video_url.'">';
                $return_html.='<a title="View Video">';
            } else {
                $return_html.='<a href="' . base_url($gallery_user_profile) . '" target="_blank">';
            }

            $return_html.='<img ' . $title . ' src="' . $gallery_src_url . '" />';
            if ($video_url) {
                //$return_html.='</a>';
            }
            $return_html.='</a>';
            $return_html.='</div>';
        }

        $return_html.='</div> <div class="span12 pagination" style="margin:0px 0px 0px -16px;"> <ul class="tyr_gallery_pagination pull-right">
            ' . $pagination_link . '
                        </ul></div>';
        if (IS_AJAX) {
            echo $return_html;
        } else {
            return $return_html;
        }

        //IS_AJAX?json_encode($gallery_data):return json_encode($gallery_data);
    }

    protected function fileexists($full_filepath = NULL) {

        if (!pathinfo($full_filepath, PATHINFO_EXTENSION)) {
            return false; //check if THIS IS A DIR
        }

        /* if(is_dir($full_filepath)){
          return false;
          } */
        if ($full_filepath) {

            $file_headers = @get_headers($full_filepath);

            if (strpos(strtolower($file_headers[0]), 'ok') === FALSE) {
                $return = false;
            } else {
                $return = true;
            }
        } else {
            $return = false;
        }
        return $return;
    }

    protected function get_gallery_data($return_html = true) {
        //this method use for infinity scroll gallery data
        ///$last_gallery_id = preg_replace('/[^0-9]/','',$this->uri->segment(2));
        //$loaded_img_count = $this->input->get_post('loaded_img_count');
        //$showhtml = $this->input->get_post('showhtml');
        $loaded_images = preg_replace('/[^\d]/', '', $this->input->get('loaded_images'));
        $loaded_images === null ? "0" : $loaded_images;


        $where = array();
        $where['tm.status_sl'] = 1;
        if (is_numeric($last_gallery_id)) {
            ///$where = array('gallery_id <' => $last_gallery_id);
            $where['tm.image_id'] = $last_gallery_id;
        }
        $gallery_limit = 40;
//        $sel_cols = '
//        tu.profile_url AS gallery_user_profile,
//        tm.image_id AS gallery_id,
//        tm.user_id AS gallery_user_id,
//        tm.media_type AS gallery_type,
//        tm.video_type AS gallery_video_type,
//        tm.media_name AS gallery_src,
//        tm.media_original_name AS original_src,
//        tm.media_url AS gallery_url,
//        tm.media_title AS gallery_title,
//        tm.created AS gallery_uploaded_time,
//        tm.status_sl AS gallery_status';
//        $gallery_data = $this->db->select($sel_cols)
//                ->from(TABLE_MEDIA . ' AS tm')
//                ->join(TABLE_USERS . ' AS tu', 'tu.user_id=tm.user_id', 'LEFT OUTER')
//                ->order_by('tm.image_id', 'DESC')
//                ->where($where)
//                ->limit($gallery_limit, $loaded_images)
//                ->get()
//                ->result_array();
        //print_array($this->db->last_query(),true,'last media QUERY!');
        $join_obj = new Tyr_joins_entity();
        $gallery_data = $join_obj->get_gallery_records_pagination($gallery_limit, $loaded_images);

        if (true == $return_html) {
            $return_html = null;
            foreach ($gallery_data as $glry_data) {
                $gallery_id = $glry_data['gallery_id'];
                $gallery_user_id = $glry_data['gallery_user_id'];
                $gallery_user_profile = $glry_data['gallery_user_profile'];
                $gallery_type = $glry_data['gallery_type'];
                $gallery_video_type = $glry_data['gallery_video_type'];
                $gallery_src = $glry_data['gallery_src'];
                $original_src = $glry_data['original_src'];
                $gallery_url = $glry_data['gallery_url'];
                $gallery_title = $glry_data['gallery_title'];
                $gallery_uploaded_time = $glry_data['gallery_uploaded_time'];
                $gallery_status = $glry_data['gallery_status'];
                $gallery_src_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $gallery_src;
                $gallery_src_original_url = ASSETS_PATH_PROFILE_FULLWIDTH_IMAGE . $original_src;

                $allow_data_type = array('video', 'image');
                if (!in_array($gallery_type, $allow_data_type)) {
                    continue;
                }
                $video_url = null;

                if ($gallery_type == 'video') {
                    if (strpos($gallery_src, 'http') === false) {
                        $gallery_src = 'http:' . $gallery_src;
                    }
                    $data_url = $gallery_src;
                    $video_url = $gallery_url;
                    if ($gallery_video_type == 'vimeo') {
                        $video_id_exploded = explode('/', $gallery_url);
                        foreach ($video_id_exploded as $video_key => $video_val) {
                            if (is_numeric($video_val)) {
                                $vimeo_vid_id = $video_val;
                                break;
                            }
                        }
                        $hash = unserialize(file_get_contents("http://vimeo.com/api/v2/video/$vimeo_vid_id.php"));
                        $gallery_src_url = $hash[0]['thumbnail_large'];
                    } else if ($gallery_video_type == 'youtube') {

                        $youtube_vid_id = preg_replace("#[&\?].+$#", "", preg_replace("#http://(?:www\.)?youtu\.?be(?:\.com)?/(embed/|watch\?v=|\?v=|v/|e/|.+/|watch.*v=|)#i", "", $video_url));

                        $gallery_src_url = 'http://img.youtube.com/vi/' . $youtube_vid_id . '/0.jpg';
                        $video_url = str_replace('embed/', 'watch?v=', $video_url);
                    }
                } else {
                    $data_url = $gallery_src_original_url;
                }

                $gallery_img_size = getimagesize($gallery_src_url);
                if (strpos($gallery_src_url, 'youtube') === false) {

                    if ($gallery_img_size === true || $gallery_img_size === false) {
                        $gallery_src_url = ASSETS_PATH_NO_IMAGE;
                        $title = 'title="image not available" width="100%"';
                    }
                }
                $show_full_gallery_data_in_modal = null;
                if ($video_url) {
                    $show_full_gallery_data_in_modal = 'show_full_gallery_data_in_modal';
                }

                $return_html.='<div data-type="' . $gallery_type . '" data-url="' . $data_url . '" class="span3' . $show_full_gallery_data_in_modal . '  gallery_id_' . $gallery_id . '">';
                if ($video_url) {
                    //$return_html.='<a title="View Video" target="_blank" href="'.$video_url.'">';
                    $return_html.='<a title="View Video">';
                } else {
                    $return_html.='<a href="' . base_url($gallery_user_profile) . '" target="_blank">';
                }

                $return_html.='<img ' . $title . ' src="' . $gallery_src_url . '" />';
                if ($video_url) {
                    //$return_html.='</a>';
                }
                $return_html.='</a>';
                $return_html.='</div>';
            }

            $return_html.='<ul class="pagination pull-left">
                            <li><a href="#">‹</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">›</a></li>
                        </ul>';
            if (IS_AJAX) {
                echo $return_html;
            } else {
                return $return_html;
            }
        } else {

            if (IS_AJAX) {
                echo json_encode($gallery_data);
                die;
            } else {
                return $gallery_data;
            }
        }

        //IS_AJAX?json_encode($gallery_data):return json_encode($gallery_data);
    }

}