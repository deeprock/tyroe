<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/user_compay_location_entity' . EXT);

class Team extends Tyroe_Controller
{
    public function Team()
    {
        parent::__construct();
        $this->data['page_title'] = 'Team';
        if ($this->session->userdata('role_id') == STUDIO_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_TEAM . ') ');
        }
    }

    public function index()
    {


        $user_id         = $this->session->userdata("user_id");
        $bool            = $this->input->post('bool');
        $fillter_text    = $this->input->post('fillter_text');
        $sorting = $this->input->post('sorting');
        $orderby = " ORDER BY " .TABLE_USERS. ".user_id DESC";
        $this->data['td_sort'] = 'ASC';
        if($sorting)
        {
            $sort_column = $this->input->post('sort_column');
            $sort_type = $this->input->post('sort_type');
            $orderby = " ORDER BY ".$sort_column." ".$sort_type;
            $this->data['sorting'] = $sorting;

            $this->data['sort_column'] = $sort_column;
            $this->data['sort_type'] = $sort_type;
            if($sort_type == 'ASC')
            {
                $this->data['td_sort']= 'DESC';
            }
            else
            {
                $this->data['td_sort'] = 'ASC';
            }
        }
        
        $parent_id = $user_id;
        $users_obj = new Tyr_users_entity();
        $users_obj->user_id = $user_id;
        $users_obj->get_user();
        if($users_obj->parent_id != 0){
            $parent_id = $users_obj->parent_id;
        }
        
        $joins_obj = new Tyr_joins_entity();
        $total_rows = $joins_obj->get_total_row_count_filter_team($user_id,$parent_id,$fillter_text);
        
//        $total_rows = $this->getRow
//            ("SELECT COUNT(*)AS cnt FROM " . TABLE_USERS . "
//              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//              WHERE " . TABLE_USERS . ".role_id='4' AND " . TABLE_USERS . ".status_sl!='-1' AND " .TABLE_USERS. ".parent_id = '".$user_id."'
//              AND ( CONCAT(" . TABLE_USERS . ".firstname , ' ', " . TABLE_USERS . ".lastname) LIKE '%".$fillter_text."%'
//                    OR " . TABLE_USERS . ".username LIKE '%".$fillter_text."%'
//                   )
//              ");
        
        $pagintaion_limit = $this->pagination_limit();
        
//        $get_reviewer = $this->getAll
//            ("SELECT " . TABLE_USERS . ".user_id,role_id,parent_id,created_timestamp," . TABLE_USERS . ".status_sl," . TABLE_USERS . ".country_id,email,
//              username,firstname,lastname,address,city,availability,job_title,
//              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
//              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
//              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
//              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
//              WHERE " . TABLE_USERS . ".role_id='4' AND " . TABLE_USERS . ".status_sl!='-1' AND " .TABLE_USERS. ".parent_id = '".$user_id."'
//              AND ( CONCAT(" . TABLE_USERS . ".firstname , ' ', " . TABLE_USERS . ".lastname) LIKE '%".$fillter_text."%'
//                    OR " . TABLE_USERS . ".username LIKE '%".$fillter_text."%'
//                   )".$orderby." ". $pagintaion_limit['limit']);
        
        $joins_obj = new Tyr_joins_entity();
        $get_reviewer = $joins_obj->get_reviewer_filter_team($user_id,$parent_id,$fillter_text,$orderby,$pagintaion_limit);
      

        // foreach ($get_tyroes as $tyroes) {
        if($get_reviewer != ''){
            for ($i = 0; $i < count($get_reviewer); $i++) {

//                $getopenings = "SELECT " . TABLE_JOBS . ".job_title FROM " . TABLE_REVIEWER_JOBS . "
//                                LEFT JOIN " . TABLE_JOBS . "
//                                ON " . TABLE_JOBS . ".`job_id` = " . TABLE_REVIEWER_JOBS . ".job_id
//                                AND " . TABLE_JOBS . ".status_sl = 1 AND " . TABLE_JOBS . ".archived_status = 0
//                                WHERE " . TABLE_JOBS . ".user_id = '".$user_id."' AND " . TABLE_REVIEWER_JOBS . ".`reviewer_id` = '".$reviewer_id."' AND " . TABLE_REVIEWER_JOBS . ".`status_sl` = 1";
                
                $reviewer_id = $get_reviewer[$i]['user_id'];
                
                $joins_obj = new Tyr_joins_entity();
                $get_reviewer[$i]['get_reviewer_openings'] = $joins_obj->get_all_openings_by_user_reviewer($user_id,$reviewer_id);
                
                //$get_reviewer[$i]['get_reviewer_openings'] = $this->getAll($getopenings);
                /**end get tyroe total score**/
            }
        }
        /*Add to opening dropdown*/
        $this->data['get_reviewers'] = $get_reviewer;
        $this->data['fillter_input_val'] = $fillter_text;
        /*PAGINATION*/
        $this->data['pagination'] = $this->pagination(array('url' => 'search/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->data['ha']=$total_rows['cnt'];
        /* $this->template_arr = array('contents/search/search_tyr');
         $this->load_template();*/
        $this->data['user_id'] = $user_id;
        if($bool){
            $this->template_arr = array('contents/studio_team_fillter');
        }else{
            $this->template_arr = array('header', 'contents/team', 'footer');
        }
        $this->load_template();
    }

    public function add_edit_reviewer(){
        $user_id         = $this->session->userdata("user_id");
        $action             =   $this->input->post("action");
        $role_id            =   $this->input->post("default_group");
        $user_parent        =   $this->session->userdata('user_id');
        $user_email         =   $this->input->post("email");
        $user_firstname     =   $this->input->post("firstname");
        $user_lastname      =   $this->input->post("lastname");
        $user_name          =   $this->input->post("username");
        $job_title          =   $this->input->post("job_title");
        $reviewer_id        =   $this->input->post('reviewer_id');
        if($action == 'add'){
            $password           =   $this->hashPass($this->input->post("password"));
            $password_len       =   strlen($this->input->post("password"));
        }
        $create_timestamp   =   time();
        $status_sl          =   $this->input->post("status_sl");
        //$default_group         =   $this->input->post("default_group");
        if($action == 'add'){
            //check email and username exist
            if($reviewer_id != ''){
                
                //$duplicate_email = $this->Execute("Select * from " . TABLE_USERS . " where email='" . $user_email . "' AND user_id !='".$reviewer_id."'  AND status_sl = 1");
                //$duplicate_user_name = $this->Execute("Select * from " . TABLE_USERS . " where username='" . $user_name . "' AND user_id !='".$reviewer_id."'  AND status_sl = 1");
                $users_obj = new Tyr_users_entity();
                $users_obj->email = $user_email;
                $users_obj->user_id = $reviewer_id;
                $users_obj->status_sl = 1;
                $duplicate_email = $users_obj->check_duplicate_user_email_1();
                
                $users_obj = new Tyr_users_entity();
                $users_obj->username = $user_name;
                $users_obj->user_id = $reviewer_id;
                $users_obj->status_sl = 1;
                $duplicate_user_name = $users_obj->check_duplicate_user_name_1();
                
            }else{
                //$duplicate_email = $this->Execute("Select * from " . TABLE_USERS . " where email='" . $user_email . "' AND status_sl = 1");
                //$duplicate_user_name = $this->Execute("Select * from " . TABLE_USERS . " where username='" . $user_name . "' AND status_sl = 1");
                
                $users_obj = new Tyr_users_entity();
                $users_obj->email = $user_email;
                $duplicate_email = $users_obj->check_duplicate_user_email();
                
                $users_obj = new Tyr_users_entity();
                $users_obj->username = $user_name;
                $duplicate_user_name = $users_obj->check_duplicate_user_username();
            }
            
            if ($duplicate_email) {
                echo json_encode(array('success_result' => false, 'success_message' => LABEL_EMAIL_ALREADY_REGISTERED));
            }else if ($duplicate_user_name) {
                echo json_encode(array('success_result' => false, 'success_message' => LABEL_USERNAME_ALREADY_REGISTERED));
            }else{
                if($reviewer_id != ''){
                    $update_group = 0;
                    
                    $users_obj = new Tyr_users_entity();
                    $users_obj->user_id = $reviewer_id;
                    $users_obj->get_user();
                    
                    if($users_obj->role_id != $role_id)
                        $update_group = 1;
                    
                    $users_obj->role_id = $role_id;
                    $users_obj->parent_id = $user_parent;
                    $users_obj->email = $user_email;
                    $users_obj->firstname = $user_firstname;
                    $users_obj->lastname = $user_lastname;
                    $users_obj->job_title = $job_title;
                    $users_obj->username = $user_name;
                    $users_obj->profile_url = $user_name;
                    $users_obj->status_sl = $status_sl;
                    $users_obj->modified_timestamp = $create_timestamp;
                    $users_obj->update_users();
                    
                    
                    $user_details       =   array(
                        'role_id'           =>      $role_id,
                        'parent_id'         =>      $user_parent,
                        'email'             =>      $user_email,
                        'firstname'         =>      $user_firstname,
                        'lastname'          =>      $user_lastname,
                        'job_title'         =>      $job_title,
                        'username'          =>      $user_name,
                        'profile_url'       =>      $user_name,
                        'status_sl'         =>      $status_sl,
                        'modified_timestamp' =>     $create_timestamp
                    );
                    
                    //$inserted_id = $this->update(TABLE_USERS,$user_details,array("user_id"=>$reviewer_id));
                    $inserted_id = $reviewer_id;
                    
                    if($update_group){
                        $permission_obj = new Tyr_permissions_entity();
                        $permission_obj->user_id = $reviewer_id;
                        $permission_obj->delete_user_permissions();
                        $reviewer_modules = explode(',',REVIEWER_USER_MODULES);
                        if($role_id == 3){
                            $reviewer_modules = explode(',',STUDIO_USER_MODULES); 
                        }
                        foreach($reviewer_modules as $reviewer_module){
                            $permission_obj = new Tyr_permissions_entity();
                            $permission_obj->user_id = $inserted_id;
                            $permission_obj->module_id = $reviewer_module;
                            $permission_obj->save_permissions();
                        }
                        
                    }
                    
                    if($this->session->userdata("role_id") == '3'){
                        //getting_pending_reviewer
                        $this->data['pending_reviewer'] =  $this->getRow("SELECT COUNT(*) AS pending_reviewer FROM ".TABLE_USERS." WHERE parent_id = '".$user_id."' AND status_sl = 0");
                        $this->session->set_userdata('pending_reviewer',$this->data['pending_reviewer']['pending_reviewer']);
                    }
                }else{
                    $user_details       =   array(

                        'role_id'           =>      $role_id,
                        'parent_id'         =>      $user_parent,
                        'email'             =>      $user_email,
                        'firstname'         =>      $user_firstname,
                        'lastname'          =>      $user_lastname,
                        'username'          =>      $user_name,
                        'job_title'         =>      $job_title,
                        'profile_url'       =>      $user_name,
                        'password'          =>      $password,
                        'password_len'      =>      $password_len,
                        'status_sl'         =>      $status_sl,
                        'created_timestamp' =>      $create_timestamp

                    );
                    //$inserted_id = $this->insert(TABLE_USERS,$user_details);
                    
                    $users_obj = new Tyr_users_entity();
                    $users_obj->role_id = $role_id;
                    $users_obj->parent_id = $user_parent;
                    $users_obj->email = $user_email;
                    $users_obj->firstname = $user_firstname;
                    $users_obj->lastname = $user_lastname;
                    $users_obj->job_title = $job_title;
                    $users_obj->username = $user_name;
                    $users_obj->profile_url = $user_name;
                    $users_obj->password_len = $password_len;
                    $users_obj->password = $password;
                    $users_obj->status_sl = $status_sl;
                    $users_obj->created_timestamp = $create_timestamp;
                    $users_obj->save_user();
                    $inserted_id = $users_obj->user_id;
                    $reviewer_modules = explode(',',REVIEWER_USER_MODULES);
                    foreach($reviewer_modules as $reviewer_module){
                        $permission_obj = new Tyr_permissions_entity();
                        $permission_obj->user_id = $inserted_id;
                        $permission_obj->module_id = $reviewer_module;
                        $permission_obj->save_permissions();
//                        $rec = array('user_id' => $inserted_id,'module_id' => $reviewer_module);
//                        $this->insert(TABLE_PERMISSIONS,$rec);
                    }
                }

                //gettin openings for user
//                $getopenings = "SELECT " . TABLE_JOBS . ".job_title FROM " . TABLE_REVIEWER_JOBS . "
//                                               LEFT JOIN " . TABLE_JOBS . "
//                                               ON " . TABLE_JOBS . ".`job_id` = " . TABLE_REVIEWER_JOBS . ".job_id
//                                               AND " . TABLE_JOBS . ".status_sl = 1 AND " . TABLE_JOBS . ".archived_status = 0
//                                               WHERE " . TABLE_JOBS . ".user_id = '".$user_parent."' AND " . TABLE_REVIEWER_JOBS . ".`reviewer_id` = '".$reviewer_id."' AND " . TABLE_REVIEWER_JOBS . ".`status_sl` = 1";
//
//                $allOpenings = $this->getAll($getopenings);
                
                $joins_obj = new Tyr_joins_entity();
                $allOpenings = $joins_obj->get_all_openings_by_user_reviewer($user_parent,$reviewer_id);
                
                $openingsText = '';
                foreach($allOpenings as $allOpening){
                    $openingsText .= $allOpening['job_title'] . ',';
                }
                $openingsText = rtrim($openingsText, ',');
                //getting inserted reviewer
                
                
                //$get_reviewer = $this->getRow("SELECT user_id,firstname,lastname,created_timestamp,email,status_sl,job_title FROM ".TABLE_USERS." WHERE user_id = '".$inserted_id."' AND parent_id = '".$user_parent."'");
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $inserted_id;
                $users_obj->parent_id = $user_parent;
                $users_obj->get_user_by_parent();
                $get_reviewer = (array)$users_obj;
                
                $inserted_record_html  = "<tr class='table_rows row_reviewer_no".$get_reviewer['user_id']."'>";
                $inserted_record_html .= "<td> <p class='rt-name'>".$get_reviewer['firstname'].' '.$get_reviewer['lastname']."</p><span class='rt-title'>".$get_reviewer['job_title']."</td>";
                
                if($role_id == 3)
                    $inserted_record_html .= "<td>Administrator</td>";
                else
                    $inserted_record_html .= "<td>Reviewer</td>";
                
                $inserted_record_html .= "<td>".date('M d, Y',$get_reviewer['created_timestamp'])."</td>";
                $inserted_record_html .= "<td style='width: 350px'>".$openingsText."</td>";
                $inserted_record_html .= "<td><a href='mailto:".$get_reviewer['email']."'>".$get_reviewer['email']."</a></td>";
                $inserted_record_html .= "<td class='status_td'>";
                if($get_reviewer['status_sl'] == 1){
                    $inserted_record_html .= "<label class='label label-success'>Active</label>";
                }else if($get_reviewer['status_sl'] == 0){
                    $inserted_record_html .= "<label class='label label-info rt-pending'>Pending</label>";
                }
                $inserted_record_html .= "</td>";
                $inserted_record_html .= "<td><a href='javascript:void(0)' data-toggle='modal' class='edit_reviewer margin-right-2' data-target='#myModal' data-value='".$get_reviewer['user_id']."' >Edit</a> <a href='javascript:void(0)' class='delete_reviewer' data-value='".$get_reviewer['user_id']."' >Delete</a></td>";
                $inserted_record_html .= "</tr>";
                if($reviewer_id != ''){
                    $successmsg = MESSAGE_REVIEWER_UPDATE_SUCCESS;
                }else{
                    $successmsg = MESSAGE_ADD_SUCCESS;
                }
                echo json_encode(array('success_result' => true, 'success_message' => $successmsg, 'html' => $inserted_record_html));
            }
        }else if($action == 'edit'){
                $reviewer_id  = $this->input->post('reviewer_id');
                
                //$get_reviewer = $this->getRow("SELECT password,user_id,firstname,lastname,username,created_timestamp,email,status_sl,job_title FROM ".TABLE_USERS." WHERE user_id = '".$reviewer_id."' AND parent_id = '".$user_parent."'");
                $users_obj = new Tyr_users_entity();
                $users_obj->user_id = $reviewer_id;
                $users_obj->parent_id = $user_parent;
                $users_obj->get_user_by_parent();
                $get_reviewer = (array)$users_obj;
                
                $company_details_obj = new Tyr_company_detail_entity();
                $company_details_obj->studio_id = $user_parent;
                $company_details_obj->get_company_detail_by_studio();
                //var_dump($company_details_obj);
                
                $form_html    = '<div class="alert alert-danger hidden error_modal_div"></div>';
                $form_html   .= '<div class="container-fluid">';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span12 center-block">';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>First & Last Name</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9 rt-first-ct">';
                $form_html   .= '<input class="rt-nd1 span6"  type="text" id="firstname" placeholder="First name" value="'.$get_reviewer['firstname'].'"/>';
                $form_html   .= '<input class="rt-nd1 span6"  type="text" id="lastname" placeholder="Last name" value="'.$get_reviewer['lastname'].'"/>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Username</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<input class="width-100 rt-nd1"  type="text" id="username" placeholder="Username" value="'.$get_reviewer['username'].'"/>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Email</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<input class="width-100 rt-nd1"  type="text" id="email" placeholder="Email" value="'.$get_reviewer['email'].'"/>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Company Name</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<input class="width-100 rt-nd1" type="text" id="" value="'. $company_details_obj->company_name .'" disabled="disabled" placeholder="Company Name"/>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Job Title</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<input class="width-100 rt-nd1" type="text" id="job_title" placeholder="Job Title" value="'.$get_reviewer['job_title'].'"/>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Status</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<input class="rt-nd1"  type="hidden" id="user_id" value="'.$reviewer_id.'"/>';
                $form_html   .= '<select id="status_sl" class="rt-status rt-nd1 width-100">';
                if($get_reviewer['status_sl'] == 1){
                    $form_html   .= '<option value="1" selected="selected">Active</option>';
                    $form_html   .= '<option value="0">Pending</option>';
                }else if($get_reviewer['status_sl'] == 0){
                    $form_html   .= '<option value="1">Active</option>';
                    $form_html   .= '<option value="0" selected="selected">Pending</option>';
                }
                $form_html   .= '</select>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="row-fluid">';
                $form_html   .= '<div class="span3">';
                $form_html   .= '<label>Default Group</label>';
                $form_html   .= '</div>';
                $form_html   .= '<div class="span9">';
                $form_html   .= '<select id="default_group" class="rt-status rt-nd1 width-100">';
                if($get_reviewer['role_id'] == 3){
                    $form_html   .= '<option value="3" selected="selected">Administrator</option>';
                    $form_html   .= '<option value="4">Reviewer</option>';
                }else if($get_reviewer['role_id'] == 4){
                    $form_html   .= '<option value="3">Administrator</option>';
                    $form_html   .= '<option value="4" selected="selected">Reviewer</option>';
                }
                $form_html   .= '</select>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                $form_html   .= '</div>';
                echo  json_encode(array('success_result' => true, 'success_message' => MESSAGE_ADD_SUCCESS, 'html' => $form_html));
        }else if($action == 'delete'){
            $reviewer_id = $this->input->post('reviewer_id');
            $user_details       =   array(
                'status_sl'     =>      -1
            );
            $result = $this->update(TABLE_USERS,$user_details,array('user_id'=>$reviewer_id));
            if($result){
                echo json_encode(array('success_result' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
            }
        }
    }
}