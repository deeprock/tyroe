<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);
require_once(APPPATH . 'entities/tyr_cities_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);

class Jobs extends CI_Controller{
    var $data = array();
    public function Jobs() {
        parent::__construct();
    }
    
    public function index($job_id){
        $this->data['job_url_title'] = $job_id;
        if (!is_numeric($job_id)) {
            $job_id = $this->get_number_from_name($job_id);
        }
        
        $jobs_obj = new tyr_jobs_entity();
        $jobs_obj->job_id = $job_id;
        $jobs_obj->get_jobs();
        $this->data['job_detail'] = (array)$jobs_obj;
        
        $start_date = date('jS M,Y',$jobs_obj->start_date);
        $this->data['start_date'] = $start_date;
        
        $start = date("Y-m-d",$jobs_obj->start_date);
        $end = date("Y-m-d",$jobs_obj->end_date);
        $first = new DateTime($start);
        $second = new DateTime($end);
        $interval = $second->diff($first);
        $time_remaining = '';
        if($interval->y > 0)
            $time_remaining .= $interval->y . " years, ";
        if($interval->m > 0)
            $time_remaining .= $interval->y . " months, ";
        if($interval->d > 0)
            $time_remaining .= $interval->d . " days";
        $this->data['time_remaining'] = $time_remaining;
        
  
        $users_details_obj = new Tyr_users_entity();
        $users_details_obj->user_id  = $jobs_obj->user_id;
        $users_details_obj->get_user();
        $parent_id = $users_details_obj->parent_id;
        if($parent_id == 0){
            $parent_id = $users_details_obj->user_id;
        }
        
        $company_detail_studio_obj = new Tyr_company_detail_entity();
        $company_detail_studio_obj->studio_id = $parent_id;
        $company_detail_studio_obj->get_company_detail_by_studio();
        if($company_detail_studio_obj->parent_id != 0){
            $company_detail_obj = new Tyr_company_detail_entity();
            $company_detail_obj->company_id = $company_detail_studio_obj->parent_id;
            $company_detail_obj->get_company_detail();
            $this->data['company_detail'] = (array)$company_detail_obj;
        }else{
            $this->data['company_detail'] = (array)$company_detail_studio_obj;
        }
        
        $country_obj = new Tyr_countries_entity();
        $country_obj->country_id = $jobs_obj->country_id;
        $country_obj->get_countries();
        
        $city_obj = new Tyr_cities_entity();
        $city_obj->city_id = $jobs_obj->job_city;
        $city_obj->get_cities();
        
        $joins_obj = new Tyr_joins_entity();
        $this->data['skills'] = $joins_obj->get_all_jobs_skill_by_job_id($job_id);
        
        $this->data['site_url'] = LIVE_SITE_URL;
        
        $this->data['job_location'] = $city_obj->city.', '.$country_obj->country;
        $this->load->view('contents/public_job_opening',$this->data);
    }
    
    function get_number_from_name($name){
        $get_break_array = explode('-',$name);
        $id = '';
        for($a=0; $a<count($get_break_array); $a++){
            if(is_numeric($get_break_array[$a])){
                    $id = $get_break_array[$a];
            }
        }
        return round($id);
    }
}


