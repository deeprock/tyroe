<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_modules_entity' . EXT);
require_once(APPPATH . 'entities/tyr_fb_users_entity' . EXT);
class Social extends Tyroe_Controller
{
    public function Social()
    {

        parent::__construct();
        $this->data['page_title'] = 'Social';
        if ($this->session->userdata('role_id') != ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . SOCIAL_SUB_MENU . ') ');
        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
        $user_id = $this->session->userdata("user_id");
        $this->load->library('Google_Client');


    }
    public function index()
    {
        $google_client = new Google_Client();
        $google_client ->setApplicationName("Tyroe");
        $this->data['authUrl'] = $google_client->createAuthUrl();
        $user_id = $this->session->userdata("user_id");
//        $in_user = $this->getRow("SELECT * FROM ".TABLE_FB_USERS." WHERE user_id = '{$user_id}' AND socialmedia_type = 'in' AND status_sl = '1'");
//        $this->data['in_user'] = $in_user;
        $this->data['id'] = $user_id;
//        $gp_user = $this->getRow("SELECT * FROM ".TABLE_FB_USERS." WHERE user_id = '{$user_id}' AND socialmedia_type = 'gp' AND status_sl = '1'");
//        $this->data['gp_user'] = $gp_user;
//        $fb_user = $this->getRow("SELECT * FROM ".TABLE_FB_USERS." WHERE user_id = '{$user_id}' AND socialmedia_type = 'fb' AND status_sl = '1'");
//        $this->data['fb_user'] = $fb_user;
//        $tw_user = $this->getRow("SELECT * FROM ".TABLE_FB_USERS." WHERE user_id = '{$user_id}' AND socialmedia_type = 'tw' AND status_sl = '1'");
//        $this->data['tw_user'] = $tw_user;
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'in';
        $social_media_obj->get_user_social_media_details();
        $this->data['in_user'] = (array)$social_media_obj;
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'gp';
        $social_media_obj->get_user_social_media_details();
        $this->data['gp_user'] = (array)$social_media_obj;
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'fb';
        $social_media_obj->get_user_social_media_details();
        $this->data['fb_user'] = (array)$social_media_obj;
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'tw';
        $social_media_obj->get_user_social_media_details();
        $this->data['tw_user'] = (array)$social_media_obj;
        
        
        $this->template_arr = array('header', 'contents/social_media', 'footer');
        $this->load_template();
    }

    public function media_unlink()
    {
        $user_id = $this->session->userdata("user_id");
        $media_id   = $this->input->post("m_id");
        $media_type = $this->input->post("m_type");
        $allow_links = array('fb','tw','in','gp');

        if($user_id == $media_id && in_array(strtolower($media_type),$allow_links ))
        {
//            $where_media = "user_id = '".$media_id."' AND socialmedia_type = '".$media_type."'";
//            $data_media = array('status_sl' => 0);
//            $this->update(TABLE_FB_USERS,$data_media,$where_media);
            
            $social_media_obj = new Tyr_fb_users_entity();
            $social_media_obj->user_id = $user_id;
            $social_media_obj->socialmedia_type = $media_type;
            $social_media_obj->get_user_social_media_details();
            $social_media_obj->status_sl = 0;
            $social_media_obj->update_user_social_media_details();
            
            echo json_encode(array('ok' => true));
            //header('location:' . $this->getURL("social"));
        }

    }

}