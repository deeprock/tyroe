<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_jobs_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_rating_profile_entity' . EXT);
require_once(APPPATH . 'entities/tyr_company_detail_entity' . EXT);

class Feedback extends Tyroe_Controller {

    public function Feedback() {

        parent::__construct();
        $this->data['page_title'] = 'Feedback and Rating';
        if ($this->session->userdata('role_id') == ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . EXPOSURE_SUB_MENU . ') ');
        } else if ($this->session->userdata('role_id') == STUDIO_ROLE || $this->session->userdata('role_id') == REVIEWER_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . STUDIO_MODULE_ACTIVITY . ') ');
        } else {
            //$this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
            $this->get_system_modules(' AND module_id IN (' . EXPOSURE_SUB_MENU . ') ');
        }
        $user_id = $this->session->userdata("user_id");
    }

    public function index($rev_id = '') {
        $feedback_id = $this->input->post("id");
        if ($feedback_id == "") {
            $feedback_id = $this->get_number_from_name($rev_id);
        }
        
        $this->_add_css('feedback.css');

        #getting values for pagination after filtered by filter_feedback function
        $feedback_type = $this->input->post("feedback_type");
        $feedback_keyword = $this->input->post("feedback_keyword");
        $feedback_duration = $this->input->post("feedback_duration");
        $where_feedback = '';
        $user_id = $this->session->userdata("user_id");
        $jobs_obj = new Tyr_jobs_entity();
        $jobs_obj->status_sl = '1';
        $get_job_openings = $jobs_obj->get_all_jobs_by_status();
        $this->data['get_job_openings'] = $get_job_openings;
        #TYROE TOP SECTION
        //        $this->data['profile_viewed'] = $this->getRow("SELECT COUNT(*) cnt FROM " . TABLE_PROFILE_VIEW . " WHERE FROM_UNIXTIME(created_timestamp, '%Y-%m-%d') = CURRENT_DATE() AND user_id='" . $user_id . "'");

        $profile_view_obj = new Tyr_profile_view_entity();
        $profile_view_obj->user_id = $user_id;
        $viewer_type_in_values = "'3','4'";
        $this->data['reviewer_viewed'] = $profile_view_obj->get_viewer_count_by_viewer_type_IN($viewer_type_in_values);

        $profile_view_obj_1 = new Tyr_profile_view_entity();
        $profile_view_obj_1->user_id = $user_id;
        $profile_view_obj_1->viewer_type = '3';
        $this->data['studio_viewed'] = $profile_view_obj_1->get_viewer_id_count_by_viewer_type();

        $job_obj = new Tyr_jobs_entity();
        $job_obj->status_sl = '1';
        $job_obj->archived_status = '0';
        $this->data['invited_openings'] = $job_obj->get_job_id_count_by_status();

        $rating_profile_obj = new Tyr_rating_profile_entity();
        $rating_profile_obj->user_id = $user_id;
        $this->data['reviews'] = $rating_profile_obj->get_rating_count_by_user_id();

        if ($feedback_id != "") {
            $where_feedback = " AND review_id=" . $feedback_id;
        }
        if (!empty($feedback_type)) {
            $this->data['feedback_type'] = $feedback_type;
            $where_feedback .= " AND review.job_id='" . $feedback_type . "'";
        }
        if (!empty($feedback_keyword)) {
            $this->data['feedback_keyword'] = $feedback_keyword;
            $where_feedback .= " AND users.firstname LIKE '%" . $feedback_keyword . "%' OR users.lastname LIKE '%" . $feedback_keyword . "%'";
            //$where_feedback_filter .= " AND review_comment LIKE '%" . $feedback_keyword . "%'";
        }
        if (!empty($feedback_duration) && $feedback_duration == "week") {
            $this->data['feedback_duration'] = $feedback_duration;
            $where_feedback .= " AND FROM_UNIXTIME(review.created_timestamp) >= DATE_SUB(CURDATE(), INTERVAL 6 DAY)";
        } else if (!empty($feedback_duration) && $feedback_duration == "month") {
            $where_feedback .= " AND MONTH(FROM_UNIXTIME(review.created_timestamp)) = MONTH(CURDATE())";
            $this->data['feedback_duration'] = $feedback_duration;
        }


        $bool = $this->input->post('bool');
        $feedback_obj = new Tyr_joins_entity();
        $total_rows = $feedback_obj->get_count_by_feedback($user_id, $where_feedback);

        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY review_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();
        $get_feebacks = $feedback_obj->get_all_feedback($user_id, $where_feedback, $order_by, $pagintaion_limit);
        $icounter = 0;
        $all_feedbacks = array();
        foreach ($get_feebacks as $feedback) {
            if ($feedback['media_name'] == "") {
                $image = ASSETS_PATH_NO_IMAGE;
            } else {
                $image = ASSETS_PATH_PROFILE_THUMB . $feedback['media_name'];
            }
            if ($feedback['job_title'] == "") {
                $job_title = "N/A";
            } else {
                $job_title = $feedback['job_title'];
            }

            if ($feedback['anonymous_feedback_status'] == 1) {
                $annonymous_msg = $feedback['anonymous_feedback_message'];
            } else {
                $annonymous_msg = '';
            }
            
            $job_user_id = $feedback['user_id'];
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $job_user_id;
            $users_obj->get_user();
            
            $job_user_parent = $users_obj->parent_id;
            if($job_user_parent == 0)
                $job_user_parent = $users_obj->user_id;
            
            $company_details_studio_obj = new Tyr_company_detail_entity();
            $company_details_studio_obj->studio_id = $job_user_parent;
            $company_details_studio_obj->get_company_detail_by_studio();
            $company_name = $company_details_studio_obj->company_name;
            $company_logo_image = $company_details_studio_obj->logo_image;
            if($company_details_studio_obj->parent_id > 0){
                $company_details_obj = new Tyr_company_detail_entity();
                $company_details_obj->company_id = $company_details_studio_obj->parent_id;
                $company_details_obj->get_company_detail();
                $company_name = $company_details_obj->company_name;
                $company_logo_image = $company_details_obj->logo_image;
            }
            
            $job_start_date = date('jS M,Y',$feedback['start_date']);
            
            $level_name = '';
            if($feedback['level_id'] == 1)
                $level_name = 'Entry Level';
            if($feedback['level_id'] == 2)
                $level_name = 'Junior';
            if($feedback['level_id'] == 3)
                $level_name = 'Mid Level';
            
            
            $all_feedbacks[] = array(
                "review_id" => $feedback['review_id'],
                "technical" => $feedback['technical'],
                "creative" => $feedback['creative'],
                "profile_score" => $feedback['profile_score'],
                "impression" => $feedback['impression'],
                "review_comment" => $feedback['review_comment'],
                "created_timestamp" => $feedback['created_timestamp'],
                "username" => $feedback['firstname'] . " " . $feedback['lastname'],
                "job_title" => $job_title,
                "media_name" => $image,
                "country" => $feedback['country'],
                "city" => $feedback['job_city'],
                "annonymous_message" => $annonymous_msg,
                "job_level" => $feedback['level_title'],
                "level_name" => $level_name,
                "company_name" => $company_name,
                "company_logo_image" => $company_logo_image,
                "job_start_date" => $job_start_date,
                'moderated' => $feedback['moderated']
            );

            $icounter++;
        }


        /* for ($i = 0; $i < count($get_feebacks); $i++) {

          $all_feedbacks[$i]['skills'] = $this->getAll("SELECT job_skill_id, creative_skill
          FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
          " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
          WHERE job_id='" . $get_feebacks[$i]['job_id'] . "'");
          } */
        $this->data['get_feedback'] = $all_feedbacks;
        $this->data['pagination'] = $this->pagination(array('url' => 'feedback/index', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        $this->template_arr = array('header', 'contents/feedback_rating', 'footer');
        $this->load_template();
    }

    public function filter_feedback($rev_id = '') {
        $feedback_id = $this->get_number_from_name($rev_id);
        $user_id = $this->session->userdata("user_id");
        $feedback_type = $this->input->post("feedback_type");
        $feedback_keyword = $this->input->post("feedback_keyword");
        $feedback_duration = $this->input->post("feedback_duration");
        $where_feedback_filter;

        /* if ($this->session->userdata("role_id") == '2') {
          $where_feedback_filter = "tyroe_id='" . $user_id . "'";
          } else {
          $where_feedback_filter = "user_id='" . $user_id . "'";
          } */
        if (!empty($feedback_id)) {
            $where_feedback_filter .= " AND review.review_id=" . $feedback_id;
        }

        if (!empty($feedback_type)) {
            $this->data['feedback_type'] = $feedback_type;
            $where_feedback_filter .= " AND review.job_id='" . $feedback_type . "'";
        }
        if (!empty($feedback_keyword)) {
            $this->data['feedback_keyword'] = $feedback_keyword;
            $where_feedback_filter .= " AND users.firstname LIKE '%" . $feedback_keyword . "%' OR users.lastname LIKE '%" . $feedback_keyword . "%'";
            //$where_feedback_filter .= " AND review_comment LIKE '%" . $feedback_keyword . "%'";
        }
        if (!empty($feedback_duration) && $feedback_duration == "week") {
            $this->data['feedback_duration'] = $feedback_duration;
            $where_feedback_filter .= " AND FROM_UNIXTIME(review.created_timestamp) >= DATE_SUB(CURDATE(), INTERVAL 6 DAY)";
        } else if (!empty($feedback_duration) && $feedback_duration == "month") {
            $where_feedback_filter .= " AND MONTH(FROM_UNIXTIME(review.created_timestamp)) = MONTH(CURDATE())";
            $this->data['feedback_duration'] = $feedback_duration;
        }


        // required $order_by variable initialization

        $bool = $this->input->post('bool');
        $filter_feedback_obj = new Tyr_joins_entity();
        $total_rows = $filter_feedback_obj->get_count_for_filter_feedback($user_id, $where_feedback_filter, $order_by, $pagintaion_limit);

        $this->set_post_for_view();
        $order_by = $this->orderby();
        if (isset($order_by['order_by'])) {
            $order_by = $order_by['order_by'];
        } else {
            $order_by = " ORDER BY review_id DESC ";
        }
        $pagintaion_limit = $this->pagination_limit();

        /* Earlier filter by review.comment -- After 19-Aug-2014, Search by users.username */

        $feedback_result = $filter_feedback_obj->get_all_feedback($user_id, $where_feedback_filter, $order_by, $pagintaion_limit);

        //echo '<pre>';print_r($this->get_activity_stream($where_activity) );die('Call');echo '</pre>';$this->get_activity_stream($where_activity);
        $all_feedbacks = array();
        foreach ($feedback_result as $feedback) {
            $profile_score = $this->get_profile_score($feedback['company_id'], "user_id");

            if ($feedback['media_name'] == "") {
                $image = ASSETS_PATH_NO_IMAGE;
            } else {
                $image = ASSETS_PATH_PROFILE_THUMB . $feedback['media_name'];
            }
            if ($feedback['job_title'] == "") {
                $job_title = "N/A";
            } else {
                $job_title = $feedback['job_title'];
            }
            if ($feedback['anonymous_feedback_status'] == 1) {
                $annonymous_msg = $feedback['anonymous_feedback_message'];
            } else {
                $annonymous_msg = '';
            }
            
            $job_user_id = $feedback['user_id'];
            
            $users_obj = new Tyr_users_entity();
            $users_obj->user_id = $job_user_id;
            $users_obj->get_user();
            
            $job_user_parent = $users_obj->parent_id;
            if($job_user_parent == 0)
                $job_user_parent = $users_obj->user_id;
            
            $company_details_studio_obj = new Tyr_company_detail_entity();
            $company_details_studio_obj->studio_id = $job_user_parent;
            $company_details_studio_obj->get_company_detail_by_studio();
            $company_name = $company_details_studio_obj->company_name;
            $company_logo_image = $company_details_studio_obj->logo_image;
            if($company_details_studio_obj->parent_id > 0){
                $company_details_obj = new Tyr_company_detail_entity();
                $company_details_obj->company_id = $company_details_studio_obj->parent_id;
                $company_details_obj->get_company_detail();
                $company_name = $company_details_obj->company_name;
                $company_logo_image = $company_details_obj->logo_image;
            }
            
            $job_start_date = date('jS M,Y',$feedback['start_date']);
            
            $level_name = '';
            if($feedback['level_id'] == 1)
                $level_name = 'Entry Level';
            if($feedback['level_id'] == 2)
                $level_name = 'Junior';
            if($feedback['level_id'] == 3)
                $level_name = 'Mid Level';

            $all_feedbacks[] = array(
                "review_id" => $feedback['review_id'],
                "technical" => $feedback['technical'],
                "creative" => $feedback['creative'],
                "profile_score" => $feedback['profile_score'],
                "impression" => $feedback['impression'],
                "review_comment" => $feedback['review_comment'],
                "created_timestamp" => $feedback['created_timestamp'],
                "username" => $feedback['firstname'] . " " . $feedback['lastname'],
                "job_title" => $job_title,
                "media_name" => $image,
                "country" => $feedback['country'],
                "city" => $feedback['job_city'],
                "annonymous_message" => $annonymous_msg,
                "job_level" => $feedback['level_title'],
                "level_name" => $level_name,
                "company_name" => $company_name,
                "company_logo_image" => $company_logo_image,
                "job_start_date" => $job_start_date
            );
        }

        $this->data['get_feedback'] = $all_feedbacks;
        $this->data['pagination'] = $this->pagination(array('url' => 'exposure/filter_feedback', 'total_rows' => $total_rows['cnt'], 'per_page' => LIMIT_END, 'current_page' => $pagintaion_limit['start_page']));
        if (isset($feedback_id) && $feedback_id != "") {
            $this->template_arr = array('header', 'contents/feedback_rating', 'footer');
        } else {
            $this->template_arr = array('contents/search/search_feedback');
        }
        $this->load_template();
    }

}
