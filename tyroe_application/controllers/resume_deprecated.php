<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Resume extends Tyroe_Controller
{
    public function Resume()
    {

        parent::__construct();
        $this->data['page_title'] = 'Resume';
        if ($this->session->userdata('role_id') != ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . RESUME_SUB_MENU . ') ');
            /*if($this->session->userdata("exp_suc")!=""){
                $this->session->set_userdata("exp_suc","");
            }else{
                $this->session->unset_userdata("exp_suc");
            }*/
        } else {
            $this->get_system_modules();
        }
    }

    public function index()
    {

        $user_id = $this->session->userdata("user_id");
        #Experience Details

        $experience = $this->getAll("SELECT exp_id, user_id, company_name,job_title,job_location,IF(job_start='',YEAR(CURDATE()) ,job_start) AS job_start,
        IF(job_end='',YEAR(CURDATE()) ,job_end) AS job_end, job_description, job_url, job_country, job_city,
                  current_job, created,modified,status_sl  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $user_id . "' and status_sl='1'");
        $cur_job = $this->getRow("SELECT count(*) as cnt  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $user_id . "' and status_sl='1' AND current_job='1'");
        $this->session->set_userdata("current_job",$cur_job['cnt']);
        $this->data['get_experience'] = $experience;

        #Education Details
        $education = $this->getAll("SELECT education_id, user_id,institute_name,edu_organization, degree_title,field_interest,grade_obtain,IF(start_year='',YEAR(CURDATE()) ,start_year) as start_year,
         IF(end_year='',YEAR(CURDATE()) ,end_year) as end_year,activities,education_description,created,modified,status_sl
        FROM " . TABLE_EDUCATION . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
        $this->data['get_education'] = $education;

        #Skills
        $skills = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
            FROM " . TABLE_SKILLS . " sk
            LEFT JOIN " . TABLE_CREATIVE_SKILLS . " csk ON (sk.`skill` = csk.creative_skill_id)
            WHERE sk.user_id='" . $user_id . "' and sk.status_sl='1' ");
        $this->data['get_skills'] = $skills;

        #Speciality
        $speciality = $this->getRow("SELECT sk.*, csk.creative_skill as skill_name
            FROM " . TABLE_SKILLS . " sk
            LEFT JOIN " . TABLE_CREATIVE_SKILLS . " csk ON (sk.`skill` = csk.creative_skill_id)
            WHERE sk.user_id='" . $user_id . "' AND sk.status_sl='1' AND sk.speciality = '1' ");
        $this->data['get_speciality'] = $speciality;

        #Awards
        $awards = $this->getAll("SELECT award_id, user_id,award,award_organization,award_year,created_timestamp, modified,  status_sl
                FROM " . TABLE_AWARDS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
        $this->data['get_awards'] = $awards;

        #Refrence
        $refrences = $this->getAll("SELECT refrence_id, user_id,refrence_detail,created_timestamp,modified,status_sl
                FROM " . TABLE_REFRENCES . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
        $this->data['get_refrences'] = $refrences;

        #Recommendation
        $recommendation = $this->getAll("SELECT recommend_id, recommend_email, recommendation, recommend_name, recommend_status, created_timestamp
                FROM " . TABLE_RECOMMENDATION . " WHERE user_id='" . $user_id . "' ");
        $this->data['get_recommendation'] = $recommendation;
        #Query will be changed

        $this->template_arr = array('header', 'contents/get_resume', 'footer');
        $this->load_template();
    }

    public function save_experience()
    {

        $experience_detail = array(
            "user_id" => $this->session->userdata("user_id"),
            "company_name" => $this->input->post("company_name"),
            "job_title" => $this->input->post("job_title"),
            "current_job" => $this->input->post("current_job"),
            "job_start" => strtotime($this->input->post("job_start")),
            "job_end" => strtotime($this->input->post("job_end")),
            "job_description" => $this->input->post("job_description"),
            "job_url" => $this->input->post("job_url"),
            "job_country" => $this->input->post("job_country"),
            "job_city" => $this->input->post("job_city"),
            "created" => strtotime("now")
        );
        if($this->input->post("exp_id") > 0){
            $exp_id = $this->input->post("exp_id");
            $where = "exp_id ='" . $exp_id . "'";
            $return = $this->update(TABLE_EXPERIENCE,$experience_detail,$where);
        } else {
            $return = $this->insert(TABLE_EXPERIENCE, $experience_detail);
        }
        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_EXPERIENCE_SUCCESS));
            //echo "1";
            //$this->session->set_userdata("status_msg",MESSAGE_ADD_EXPERIENCE_SUCCESS);
        } else {
            //$this->session->set_userdata("exp_suc",-1);
            echo json_encode(array('success' => false));
        }


    }

    public function ExperienceForm($data)
    {
        if ($data > 0) {
            $row_id = $data;
            $exp_data = $this->getRow("SELECT * FROM " . TABLE_EXPERIENCE . " WHERE exp_id = '{$row_id}'");
            $this->data["flag"] = "edit";
            $this->data["exp_data"] = $exp_data;
        }
        $this->template_arr = array('contents/experience_form');
        $this->load_template();
        /*$this->template_arr = array('header', 'contents/experience_form', 'footer');
        $this->load_template();*/
    }

    public function DeleteExperience()
    {
        $where = "exp_id ='" . $this->input->post("job_id") . "'";
        $update = array("status_sl" => "-1",
            "modified" => strtotime("now")
        );
        $return = $this->update(TABLE_EXPERIENCE, $update, $where);

        /* $where="exp_id ='".$this->input->post("job_id")."'";
          $return=$this->Delete(TABLE_EXPERIENCE,$where);*/
        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_EXPERIENCE_DELETE));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function GetForm($form, $data = '')
    {
        if($form == "skill_form"){
            $user_id = $this->session->userdata("user_id");
            $predefine_skills = $this->getAll("SELECT * FROM " . TABLE_CREATIVE_SKILLS);
            $this->data["predefine_skills"] = $predefine_skills;

            $skill_data = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
                FROM " . TABLE_SKILLS . " sk
                LEFT JOIN " . TABLE_CREATIVE_SKILLS . " csk ON (sk.`skill` = csk.creative_skill_id)
                WHERE sk.user_id='" . $user_id . "' and sk.status_sl='1' ");
            $this->data["skill_data"] = $skill_data;

        } elseif($form == "speciality_form"){

            $user_id = $this->session->userdata("user_id");
            $speciality_data = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
                FROM " . TABLE_SKILLS . " sk
                LEFT JOIN " . TABLE_CREATIVE_SKILLS . " csk ON (sk.`skill` = csk.creative_skill_id)
                WHERE sk.user_id='" . $user_id . "' and sk.status_sl='1' ");
            $this->data["speciality_data"] = $speciality_data;
        }

        if (($data != '') && ($data > 0)) {
            $this->data["flag"] = "edit";

            if ($form == 'education_form') {
                $education_data = $this->getRow("SELECT * FROM " . TABLE_EDUCATION . " WHERE education_id = '{$data}'");
                $this->data["education_data"] = $education_data;

            } /*else if ($form == 'skill_form') {
                $skill_data = $this->getRow("SELECT skill FROM " . TABLE_SKILLS . " WHERE skill_id = '{$data}'");
                $this->data["skill_data"] = $skill_data;

            } */else if ($form == 'award_form') {
                $award_data = $this->getRow("SELECT award_id,award,award_website,award_organization,award_year,award_description FROM " . TABLE_AWARDS . " WHERE award_id = '{$data}'");
                $this->data["award_data"] = $award_data;

            } else if ($form == 'refrence_form') {
                $refrence_data = $this->getRow("SELECT refrence_id,refrence_detail FROM " . TABLE_REFRENCES . " WHERE refrence_id = '{$data}'");
                $this->data["refrence_data"] = $refrence_data;
            } else if ($form == 'recommendation_form') {
                $refrence_data = $this->getRow("SELECT refrence_id,refrence_detail FROM " . TABLE_REFRENCES . " WHERE refrence_id = '{$data}'");
                $this->data["refrence_data"] = $refrence_data;
            }
        }
        $content = "contents/" . $form;
        $this->template_arr = array($content);
        $this->load_template();
    }

    public function save_education()
    {
        $education_detail = array(
            "user_id" => $this->session->userdata("user_id"),
            "edu_organization" => $this->input->post("edu_organization"),
            "edu_url" => $this->input->post("edu_url"),
            "edu_position" => $this->input->post("edu_position"),
            "edu_country" => $this->input->post("edu_country"),
            "edu_city" => $this->input->post("edu_city"),
            "start_year" => strtotime($this->input->post("start_year")),
            "end_year" => strtotime($this->input->post("end_year")),
            "education_description" => $this->input->post("education_description"),
            "created" => strtotime("now")
        );

        if($this->input->post("edu_id") > 0){
            $edu_id  = $this->input->post("edu_id");
            $where = "education_id ='" . $edu_id . "'";
            $return = $this->update(TABLE_EDUCATION,$education_detail,$where);
        } else {
            $return = $this->insert(TABLE_EDUCATION, $education_detail);
        }

        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_EDUCATION_SUCCESS));
        } else {
            echo json_encode(array('success' => false));
        }


    }

    public function DeleteEducation()
    {

        $where = "education_id ='" . $this->input->post("education_id") . "'";
        $update = array("status_sl" => "-1",
            "modified" => strtotime("now")
        );
        $return = $this->update(TABLE_EDUCATION, $update, $where);

        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_EDUCATION_DELETE));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function save()
    {
        $condition = $this->input->post("formname");
        if($condition == "skill"){
            $user_id = $this->session->userdata("user_id");
            $skills = $this->input->post("skill");
            $new_s_v = "";
            $where = "user_id = '{$user_id}'";
            $this->Delete(TABLE_SKILLS,$where);
            foreach($skills as $s_k => $s_v){
                $new_s_v .= $s_v.",";
                $data = array(
                    'user_id' => $user_id,
                    'skill' => $s_v,
                    'status_sl' => 1
                );
                $retun_skills = $this->insert(TABLE_SKILLS, $data);
                if ($retun_skills) {
                    $this->session->set_userdata("exp_suc", 1);
                    //echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_EDUCATION_SUCCESS));
                } else {
                    //echo json_encode(array('success' => false));
                }
            }
            header('location:' . $this->getURL("resume"));

        }
        elseif($condition == "speciality"){
            $user_id = $this->session->userdata("user_id");
            $speciality = $this->input->post("speciality");
            $flag = 0;
            $where = "user_id = '{$user_id}'";
            $this->update(TABLE_SKILLS, array('speciality' => $flag), $where);
            $flag = 1;
            $where .= " AND skill_id = '{$speciality}'";
            $return_speciality=$this->update(TABLE_SKILLS, array('speciality' => $flag), $where);
            if ($return_speciality) {
                $this->session->set_userdata("exp_suc", 1);
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_EDUCATION_SUCCESS));
            } else {
                echo json_encode(array('success' => false));
            }

            header('location:' . $this->getURL("resume"));
        }
        else {
            $table_name = $this->input->post("table_name");
            $coloum_name = $this->input->post("coloum_name");
            $id = $this->input->post("id");
            if($table_name==TABLE_AWARDS){
                $details = array(
                                "user_id" => $this->session->userdata("user_id"),
                                "award" => $this->input->post("award"),
                                "award_website" => $this->input->post("award_website"),
                                "award_organization" => $this->input->post("award_organization"),
                                "award_year" => $this->input->post("award_year"),
                                "award_description" => $this->input->post("award_description"),
                                "created_timestamp" => strtotime("now")
                            );
            }
            else{

            $details = array(
                "user_id" => $this->session->userdata("user_id"),
                $coloum_name => $this->input->post("data"),
                "created_timestamp" => strtotime("now")
            );

            }
            if($id > 0){
                if($table_name==TABLE_AWARDS){
                    $where = "award_id ='" . $id . "'";

                } elseif($table_name==TABLE_REFRENCES) {
                    $where = "refrence_id ='" . $id . "'";
                }
                $return = $this->update($table_name, $details,$where);
            } else {
                $return = $this->insert($table_name, $details);
            }
            if($table_name == TABLE_RECOMMENDATION)
            {
                //email
                $user_id = $this->session->userdata("user_id");
                $recommend_id = $return;
                $user_data = $this->getRow("SELECT email, firstname, lastname FROM ".TABLE_USERS." WHERE user_id = '{$user_id}'");
                //$to = "ossama@wiztech.pk";//$this->input->post("data");
                //$to = "shafqat@wiztech.pk";
                $to = $this->input->post("data");
                $from = $user_data['firstname'] . " " . $user_data['lastname'];
                $email = "";
               // $return=$this->send_notification("n07",$to);
                $this->email_sender($to, LABEL_REQUEST_REFERENCES, "<a href='" . $this->getURL("profile/public_profile/" . $recommend_id) . "'>Click here </a>to write Recommendation for ".$from.".");
            }
            if ($return) {
                $this->session->set_userdata("exp_suc",1);
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS));
            } else {
                $this->session->set_userdata("exp_suc",-1);
                echo json_encode(array('success' => false));
            }
        }
    }

    public function DeleteSkill()
    {
        $user_id = $this->session->userdata("user_id");
        $where = "skill_id ='" . $this->input->post("skill_id") . "' AND user_id = '{$user_id}'";
        $update = array("status_sl" => "-1",
            "modified" => strtotime("now")
        );
        $return = $this->update(TABLE_SKILLS, $update, $where);
        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function DeleteAward()
    {

        $where = "award_id ='" . $this->input->post("award_id") . "'";
        $update = array("status_sl" => "-1",
            "modified" => strtotime("now")
        );
        $return = $this->update(TABLE_AWARDS, $update, $where);

        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false));
        }
    }

    public function DeleteRefrence()
    {

        $where = "refrence_id ='" . $this->input->post("refrence_id") . "'";
        $update = array("status_sl" => "-1",
            "modified" => strtotime("now")
        );
        $return = $this->update(TABLE_REFRENCES, $update, $where);

        if ($return) {
            $this->session->set_userdata("exp_suc",1);
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function EditForm()
    {

        $tablename = $this->input->post("table");
        $coloumnname = $this->input->post("coloum");
        $rowid = $this->input->post("row");
        $row = $this->getRow("SELECT * from " . $tablename . "  WHERE $coloumnname = '" . $rowid . "' and status_sl='1'");
        $this->data['get_edit'] = $row;
        $this->template_arr = array('header', 'contents/refrence_form', 'footer');
        $this->load_template();
    }
}
