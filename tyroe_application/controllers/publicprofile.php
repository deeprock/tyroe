<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_users_entity' . EXT);
require_once(APPPATH . 'entities/tyr_profile_view_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_activity_entity' . EXT);
require_once(APPPATH . 'entities/tyr_schedule_notification_entity' . EXT);
require_once(APPPATH . 'entities/tyr_states_entity' . EXT);
require_once(APPPATH . 'entities/tyr_other_site_entity' . EXT);
require_once(APPPATH . 'entities/tyr_user_sites_entity' . EXT);
require_once(APPPATH . 'entities/tyr_login_history_entity' . EXT);
require_once(APPPATH . 'entities/tyr_industry_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_years_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_level_entity' . EXT);
require_once(APPPATH . 'entities/tyr_social_link_entity' . EXT);
require_once(APPPATH . 'entities/tyr_experience_entity' . EXT);
require_once(APPPATH . 'entities/tyr_education_entity' . EXT);
require_once(APPPATH . 'entities/tyr_awards_entity' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_recommendation_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);
require_once(APPPATH . 'entities/tyr_visibility_options_entity' . EXT);
require_once(APPPATH . 'entities/tyr_custome_theme_entity' . EXT);
require_once(APPPATH . 'entities/tyr_job_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_creative_skills_entity' . EXT);
require_once(APPPATH . 'entities/tyr_countries_entity' . EXT);

class Publicprofile extends Tyroe_Controller
{
    public function Publicprofile()
    {
        parent::__construct('publicprofile');
        /*$param_arr = explode("/",$_SERVER['REQUEST_URI']);
        $profile_param = $param_arr[4];
        $this->index($profile_param);*/
        //$this->data['page_title'] = "Profile";
        //$user_name=$this->input->get('');
    }

    public function index()
    {
        //$data = $this->uri->segment(2);
        $user_obj = new Tyr_users_entity();
        $data = urldecode($this->uri->segment(1));
        //var_dump($data);
        if($data == 'tyroeJobs'){
            $this->get_public_job();
            return true;
        }
        //$data = preg_replace('/[^a-zA-Z0-9]/','',$data);
        
        $profile_view = $this->input->post("profile");

        $this->data['is_message_show'] = 0;
        $publish_account = '';
        if ($profile_view && $this->session->userdata("user_id")) {
            $user_id = $this->session->userdata("user_id");
            $publish_account = "users.user_id=".$user_id;
        } else if ($profile_view) {
            $user_id = $this->session->userdata("user_id");
            $publish_account = "users.user_id=".$user_id;
        } else {
            $user_id = $data;
            $publish_account = "users.profile_url='" . $user_id . "' AND publish_account='1'";
            if($this->session->userdata('role_id') == '1'){ // Only admin can see unpublish profiles.
                $is_account_publish = $user_obj->get_user_profile_by_url($publish_account);
                if (count($is_account_publish) == 0) {
                    $this->data['is_message_show'] = 1;
                }
                $publish_account = "users.profile_url='" . $user_id . "'";
            }
        }

        $get_account = $user_obj->get_user_profile_by_url($publish_account);

        if (is_array($get_account) && count($get_account) > 0) {

            #Profile Details
            $user_profile_details_obj = new Tyr_users_entity();
            $get_user = $user_profile_details_obj->get_details_for_public_profile($publish_account);
            $this->data['page_title'] = 'Tyroe Profile - '.$get_user['firstname'].' '.$get_user['lastname'];
            $this->data['page_description'] = $get_user['user_biography'];
            $this->data['page_url'] = str_replace("/index.php",'',current_url());
            $this->data['page_type'] = 'Tyroe Profile';

            $user_id = $get_user['user_id'];
            $this->data['profile_user_id'] = $user_id;
            /*Profile View Counter*/
            if($user_id != $this->session->userdata("user_id")){
                $viewer_id = $this->session->userdata("user_id");
                $viewer_type = $this->session->userdata("role_id");
                //$is_exist_row = "viewer_id='" . $viewer_id . "' AND user_id ='".$user_id."'";
                //$exist_row = $this->getRow("SELECT COUNT(*) AS cnt FROM " . TABLE_PROFILE_VIEW . " WHERE " . $is_exist_row);
                //if( ($exist_row['cnt'] == 0) && ($viewer_id > 0) ){
                if($viewer_id > 0){
                    $profile_entity_view_obj = new Tyr_profile_view_entity();
                    $profile_entity_view_obj->viewer_id = $viewer_id;
                    $profile_entity_view_obj->viewer_type = $viewer_type;
                    $profile_entity_view_obj->job_id = 0;
                    $profile_entity_view_obj->created_updated_by = $user_id;
                    $profile_entity_view_obj->user_id = $user_id;
//                    $profileview_data = array(
//                        "viewer_id" => $viewer_id,
//                        "viewer_type" => $viewer_type,
//                        "job_id" => 0,
//                        "user_id" => $user_id,
//                        "created_timestamp" => strtotime("now")
//                    );
                    //$this->insert(TABLE_PROFILE_VIEW,$profileview_data);
                    $profile_entity_view_obj->save_profile_view();

                    /*Email To Tyroe and Insert Stream on behalf of Tyore*/
                    if($this->session->userdata('role_id') == '3'){
                        $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                    } else if($this->session->userdata('role_id') == '4'){
                        $on = "ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)";
                    }
                    if($this->session->userdata('user_id'))
                    {
                        if($this->session->userdata('role_id') != '1' && $this->session->userdata('role_id') != '2'){
                             //if($_GET['data']=='true'){ print_array($this->session->all_userdata(),false,'___REACHED___');  }
                            
                            $email_data_user_obj = new Tyr_users_entity();
                            $email_data_user_obj->user_id = $this->session->userdata('user_id');
                            $email_data = $email_data_user_obj->get_email_data_for_profile($on);
                            
                            $activity_data = array(
                                "job_title" => $email_data['job_title'],
                                "company_name" => $email_data['company_name']
                            );
                            
                            $job_activity_obj = new Tyr_job_activity_entity();

                            $job_activity_obj->performer_id = $user_id;
                            $job_activity_obj->job_id = 0;
                            $job_activity_obj->object_id = $user_id;
                            $job_activity_obj->activity_type = LABEL_PROFILE_VIEW_JOB_LOG;
                            $job_activity_obj->activity_data = serialize($activity_data);
                            $job_activity_obj->status_sl = '1';
                            $job_activity_obj->created_updated_by = $user_id;
                            $job_activity_obj->save_job_activity();

//                            $job_activity = array(
//                                'performer_id' => $user_id,
//                                'job_id' => 0,
//                                'object_id' => $user_id,
//                                'activity_type' => LABEL_PROFILE_VIEW_JOB_LOG,
//                                'activity_data' => serialize($activity_data),
//                                'created_timestamp' => strtotime("now"),
//                                'status_sl' => '1'
//                            );
//                            $this->insert(TABLE_JOB_ACTIVITY, $job_activity);
                        }
                    }
                       
                    $schedule_entity_obj = new Tyr_schedule_notification_entity();
                    $schedule_entity_obj->user_id = $tyroe_id;
                    $schedule_entity_obj->option_id = 17;
                    $check_notification_profile_status = $schedule_entity_obj->get_schedule_notification_by_user_option();
                    $check_notification_profile_status = $check_notification_profile_status[0];
                    
                    //$check_notification_profile_status = $this->getRow("SELECT status_sl FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$tyroe_id."' AND option_id = 17");
                    if($check_notification_profile_status['status_sl'] == 1)
                    {
                        #Email
                        $subject = LABEL_VIEWED_PROFILE_TEXT . $email_data['job_title'] ." from ". $email_data['company_name'];
                        $message = "Someone clearly likes what you are doing! The company is looking for new people to work at their company, so make sure your profile is completely up-to-date and looking the bomb.";
                        $message .= "<br><br>";
                        $message .= "<b>-Team Tyroe</b>";
                        $current_user_email = $get_user['email'];
                        $this->email_sender($current_user_email, $subject, $message);
                        /*Email To Tyroe and Insert Stream on behalf of Tyore*/
                    }
                }
            }
            /*Profile View Counter*/
            $states_entity_obj = new Tyr_states_entity();
            $get_states = array();
            //$this->data['page_title'] = ucfirst($get_user['username']);
            if ($get_user['country_id'] != "") {
                $where_country_c = "AND country_id='" . $get_user['country_id'] . "'";
            }
            $get_states = $states_entity_obj->get_all_states();
            //$get_states = $this->getAll("SELECT states_id,states_name FROM " . TABLE_STATES . " Where 1=1 " . $where_country_c . " ORDER BY states_name ASC");
            $this->data['get_states'] = $get_states;

            $professional_level_entity = new Tyr_professional_level_entity();
            //$tyroe_levels = $this->getAll("SELECT tyroe_level_id,tyroe_level_title FROM " . TABLE_PROFESSIONAL_LEVEL . "");
            $tyroe_levels = $professional_level_entity->get_all_professional_level();
            $this->data['tyroe_levels'] = $tyroe_levels;
            
            $other_sites_obj = new Tyr_other_site_entity();
            $other_site = $other_sites_obj->get_all_other_site();
            $this->data['other_site'] = $other_site;


            $image;
            if ($get_user['media_name']) {
                $image = ASSETS_PATH_PROFILE_CROP_IMAGE . $get_user['media_name'];
            } else {
                $image = ASSETS_PATH_NO_IMAGE;
            }
            if(strpos($get_user['user_biography'],"Type here to add a small biography about yourself.") !== false)
            {
                $biography = "No Description Found";
            }
            else
            {
                $biography = $get_user['user_biography'];
            }
            /*  $new_pass = "";
              for ($i = 1; $i <= $get_user['password_len']; $i++) {
                  $new_pass .= " ";
              }*/
            $phone1 = explode("-", $get_user['cellphone']);
            $user_data = array(
                "user_id" => $get_user['user_id'],
                "email" => $get_user['email'],
                "username" => $get_user['username'],
                "firstname" => $get_user['firstname'],
                "extra_title" => $get_user['extra_title'],
                "lastname" => $get_user['lastname'],
                "profile_url" => $get_user['profile_url'],
                "user_biography" => $biography,
                "user_occupation" => $get_user['user_occupation'],
                "user_web" => $get_user['user_web'],
                "user_experience" => $get_user['user_experience'],
                "image" => $image,
                "image_id" => $get_user['image_id'],
                "password_len" => $get_user['password_len'],
                "state" => $get_user['state'],
                "city" => $get_user['city'],
                "zip" => $get_user['zip'],
                "address" => $get_user['address'],
                "cellphone_code" => $phone1[0],
                "cellphone" => $phone1[1],
                "country" => $get_user['country'],
                "country_id" => $get_user['country_id'],
                "tyroe_level" => $get_user['tyroe_level'],
                "city_id" => $get_user['city_id'],
                "level_title" => $get_user['level_title'],
                "experienceyear" => $get_user['experienceyear'],
                "industry_name" => $get_user['industry_name'],
                "job_level_id" => $get_user['job_level_id'],
                "industry_id" => $get_user['industry_id'],
                "availability" => $get_user['availability'],
                "experienceyear_id" => $get_user['experienceyear_id']
            );
            $schedule_notification_obj = new Tyr_schedule_notification_entity();
            $schedule_notification_obj->user_id = $user_data['user_id'];
            
            $get_email_notifications = $schedule_notification_obj->get_all_schedule_notification_by_user_id();
            //$get_email_notifications = $this->getAll("SELECT option_id, status_sl FROM ".TABLE_SCHEDULE_NOTIFICATION." WHERE user_id = '".$user_data['user_id']."'");
            
            if($get_email_notifications != ''){
                for($e_notify=0; $e_notify<count($get_email_notifications); $e_notify++){
                    //like this ----->  [enotify16] => 1
                    $user_data['enotify'.$get_email_notifications[$e_notify]['option_id']] = $get_email_notifications[$e_notify]['status_sl'];
                }    
            }
            
            $this->data['get_user'] = $user_data;

            /*mysite data*/
            $user_sites_obj = new Tyr_user_sites_entity();
            $user_sites_obj->user_id = $user_id;
            $mystite = $user_sites_obj->get_user_sites_by_user_id();
            
            //$mystite = $this->getAll("SELECT * FROM " . TABLE_USER_SITES . " WHERE user_id = '{$user_id}'");
            $this->data['get_my_sites'] = $mystite;
            
            $login_history_obj = new Tyr_login_history_entity();
            $login_history_obj->user_id = $user_id;
            $login_history = $login_history_obj->get_count_login_history_by_user_id();
            //$login_history = $this->getRow("SELECT COUNT(*) as cnt FROM " . TABLE_LOGIN_HISTORY . " WHERE user_id='{$user_id}'");
            $this->data['login_history'] = $login_history;

            $this->data['msg'] = "";
            if ($this->session->userdata('update_profile') == 1) {
                $this->data['profile_updated'] = MESSAGE_PROFILE_UPDATED;
                $this->session->unset_userdata("update_profile");

            }
            $this->data['firstname'] = $this->data['get_user']["firstname"];
            $this->data['lastname'] = $this->data['get_user']["lastname"];
            $this->data['user_biography'] = $this->data['get_user']["user_biography"];
            $this->data['availability'] = $this->data['get_user']["availability"];
            $this->data['industry_name'] = $this->data['get_user']["industry_name"];
            $this->data['extra_title'] = $this->data['get_user']["extra_title"];
            $this->data['job_title'] = $this->data['get_user']["extra_title"];
            $this->data['country'] = $this->data['get_user']["country"];
            $this->data['city'] = $this->data['get_user']["city"];
            $this->data['level'] = $this->data['get_user']["level_title"];
            $this->data['experienceyear'] = $this->data['get_user']["experienceyear"];
            
            $this->data['get_countries_dropdown'] = $this->dropdown($this->data['get_countries'], $this->data['get_user']["country_id"], array('name' => 'country', 'placeholder' => 'Country', 'onchange' => 'getCities(this)'));
            
            //$cities = $this->getAll("SELECT city_id AS id,city AS name FROM " . TABLE_CITIES . " WHERE country_id='" . $this->data['get_user']["country_id"] . "'");
            $city_obj = new Tyr_cities_entity();
            $city_obj->country_id = $this->data['get_user']["country_id"];
            $cities = $city_obj->get_all_cities_by_country_id();
            $this->data['get_cities_dropdown'] = $this->dropdown($cities, $this->data['get_user']["city_id"], array('name' => 'city', 'placeholder' => 'City'));
            
            //$get_industry = $this->getAll("SELECT industry_id AS id,industry_name AS name FROM " . TABLE_INDUSTRY);
            $industry_entity_obj = new Tyr_industry_entity();
            $get_industry = $industry_entity_obj->get_all_industries();
            $this->data['get_industry'] = $this->dropdown($get_industry, $this->data['get_user']["industry_id"], array('name' => 'industry', 'placeholder' => 'Select Industry'));
            
            //$get_experience_years = $this->getAll("SELECT experienceyear_id AS id,experienceyear AS name FROM " . TABLE_EXPERIENCE_YEARS);
            $exp_yrs_obj = new Tyr_experience_years_entity();
            $get_experience_years = $exp_yrs_obj->get_all_experience_years();
            $this->data['get_experience_years'] = $this->dropdown($get_experience_years, $this->data['get_user']["experienceyear_id"], array('name' => 'experience_level', 'placeholder' => 'Years'));
            
            //$get_job_level = $this->getAll("SELECT job_level_id AS id,level_title AS name FROM " . TABLE_JOB_LEVEL);
            $job_level_obj = new Tyr_job_level_entity();
            $get_job_level = $job_level_obj->get_all_job_level();
            $this->data['get_job_level'] = $this->dropdown($get_job_level, $this->data['get_user']["job_level_id"], array('name' => 'year', 'placeholder' => 'Experience Level'));
            //--------------------------Social link start-------------------------------------------------------
//            $social = $this->getAll("SELECT social_id, social_type,user_id,social_link,modified_timestamp,created_timestamp,status_sl
//                          FROM " . TABLE_SOCIAL_LINKS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
            $social_link_obj = new Tyr_social_link_entity();
            $social_link_obj->user_id = $user_id;
            $social_link_obj->status_sl = 1;
            $social = $social_link_obj->get_all_social_links_by_user_id_and_status();
            $this->data['get_social_link'] = $social;
            //--------------------------Social link  END-------------------------------------------------------


            //--------------------------Experience Start-------------------------------------------------------
//            $experience = $this->getAll("SELECT exp_id, user_id, company_name,job_title,job_location,IF(job_start='',YEAR(CURDATE()) ,job_start) AS job_start,
//          IF(job_end='',YEAR(CURDATE()) ,job_end) AS job_end, job_description, job_url, job_country, job_city,
//                    current_job, created,modified,status_sl  FROM " . TABLE_EXPERIENCE . "  WHERE user_id='" . $user_id . "' and status_sl='1'
//                    ");
            $exp_entity = new Tyr_experience_entity();
            $exp_entity->user_id = $user_id;
            $exp_entity->status_sl = 1;
            $experience = $exp_entity->get_user_all_experience();
            $this->data['get_experience'] = $experience;

            //--------------------------Experience END-------------------------------------------------------

            //--------------------------Education Start-------------------------------------------------------
//            $education = $this->getAll("SELECT education_id, user_id,institute_name,edu_organization,edu_position ,degree_title,field_interest,grade_obtain,IF(start_year='',YEAR(CURDATE()) ,start_year) as start_year,
//           IF(end_year='',YEAR(CURDATE()) ,end_year) as end_year,activities,education_description,created,modified,status_sl
//          FROM " . TABLE_EDUCATION . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
            
            $edu_entity_obj = new Tyr_education_entity();
            $edu_entity_obj->user_id = $user_id;
            $edu_entity_obj->status_sl = 1;
            $education = $edu_entity_obj->get_all_user_education_for_profile();
            $this->data['get_education'] = $education;

            //--------------------------Education END-------------------------------------------------------


            //--------------------------Awards Start-------------------------------------------------------
//            $awards = $this->getAll("SELECT award_id, user_id,award,award_organization,award_description,award_year,created_timestamp, modified,  status_sl
//                  FROM " . TABLE_AWARDS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
            
            $award_entity_obj = new Tyr_awards_entity();
            $award_entity_obj->user_id = $user_id;
            $award_entity_obj->status_sl = 1;
            $awards = $award_entity_obj->get_user_all_award();
            $this->data['get_awards'] = $awards;
            //--------------------------Awards END-------------------------------------------------------

            //--------------------------Skills Start-------------------------------------------------------
//            $skills = $this->getAll("SELECT sk.*, csk.creative_skill as skill_name
//                      FROM " . TABLE_CREATIVE_SKILLS . " csk
//                      LEFT JOIN " . TABLE_SKILLS . " sk  ON (sk.`skill` = csk.creative_skill_id)
//                      WHERE sk.user_id='" . $user_id . "' and sk.status_sl='1' ");
             $joins_entity = new Tyr_joins_entity();
            $skills = $joins_entity->get_all_user_skills_for_profile($user_id, 1);
            $this->data['get_skills'] = $skills;
            //--------------------------Skills End-------------------------------------------------------


            //--------------------------Recommendation Approve Start-------------------------------------------------------
//            $recommendation = $this->getAll("SELECT rec.status_sl,rec.recommend_dept,rec.recommendation,rec.recommend_name,rec.recommend_status,rec.recommend_company,rec.recommend_id,rec.recommend_email
//                          FROM " . TABLE_RECOMMENDATION . " rec
//                          WHERE rec.status_sl='1' AND rec.user_id='" . $user_id . "' AND rec.recommend_status='1'");
            $recommendation_entity = new Tyr_recommendation_entity();
            $recommendation_entity->user_id = $user_id;
            $recommendation_entity->recommend_status = 1;
            $recommendation = $recommendation_entity->get_user_all_recommendation_by_status();
            $this->data['get_recommendation_approved_list'] = $recommendation;
            //--------------------------Recommendation Approve END-------------------------------------------------------

            //--------------------------Portfolio Video Start-------------------------------------------------------

//            $video = $this->getRow("SELECT image_id, media_name, media_title, media_description,media_tag,video_type,media_url,media_type,modified
//                                                      FROM " . TABLE_MEDIA . "
//                                                      WHERE profile_image!= '1' AND status_sl='1' AND (media_type = 'video' OR media_featured='1')
//                                                      AND user_id = '" . $user_id . "'");
            
            $media_obj = new Tyr_media_entity();
            $media_obj->user_id = $user_id;
            $video = $media_obj->get_user_video_media_for_public_profile();
            $this->data['video'] = $video;
            //--------------------------Portfolio Video END-------------------------------------------------------

            //--------------------------Portfolio Images Start-------------------------------------------------------



            //---------------------------------------Latest Work Images Start------------------------------------
            $this->data['latestwork_portfolioimages'] = $media_obj->get_latest_portfolio_for_public_profile_by_user();
            
            $media_obj_1 = new Tyr_media_entity();
            $media_obj_1->user_id = $user_id;
            $media_obj_1->status_sl = 1;
            $media_obj_1->media_type = 'image';
            $media_obj_1->media_featured = 0;
            $media_obj_1->profile_image = 0;
            $this->data['latestwork_portfolioimages_count'] = $media_obj_1->get_all_user_media_count_1();
//            $this->getAll("SELECT * FROM tyr_media WHERE  user_id = '" . $user_id . "' 
//                AND status_sl = '1'  AND media_type = 'image' AND profile_image!='1' 
//                AND media_featured='0' ORDER BY media_sortorder ASC LIMIT 0,20");


            //---------------------------------------Latest Work Images End--------------------------------------
            //--------------------------Portfolio Images END-------------------------------------------------------
            //--------------------------Visibility Start-------------------------------------------------------
//            $visibility = $this->getAll("SELECT visiblity_id, user_id,visible_section,visibility, status_sl,created_date,modified_date
//                         FROM " . TABLE_VISIBLITY_OPTIONS . " WHERE user_id='" . $user_id . "' and status_sl='1' ");
            $visibility_obj = new Tyr_visibility_options_entity();
            $visibility_obj->user_id = $user_id;
            $visibility_obj->status_sl = 1;
            $visibility = $visibility_obj->get_all_user_visibility_options();
            $this->data['get_visibility'] = $visibility;
            //--------------------------Visibility END-------------------------------------------------------

            //------------------------Custome Theme Apply Start--------------//
//            $custom_theme = $this->getRow("SELECT profile_theme, featured_theme, gallery_theme, resume_theme, footer_theme
//                      FROM " . TABLE_CUSTOM_THEME . " ct
//                      WHERE ct.user_id='" . $get_user['user_id'] . "' and ct.status_sl='1' ");
            
            $custom_theme_obj = new Tyr_custome_theme_entity();
            $custom_theme_obj->user_id = $get_user['user_id'];
            $custom_theme_obj->status_sl = 1;
            $custom_theme_object = $custom_theme_obj->get_custome_theme_by_user_id();
            $custom_theme = (array)$custom_theme_object;
            
            $profile_theme = unserialize(stripslashes($custom_theme['profile_theme']));
            $featured_theme = unserialize(stripslashes($custom_theme['featured_theme']));
            $gallery_theme = unserialize(stripslashes($custom_theme['gallery_theme']));
            $resume_theme = unserialize(stripslashes($custom_theme['resume_theme']));
            $footer_theme = unserialize(stripslashes($custom_theme['footer_theme']));

            /*if ($this->session->userdata("role_id")== 2) {*/
            if($user_id == $this->session->userdata("user_id")){
                $this->data['profile_theme'] = $this->session->userdata['profile_theme'];
                $this->data['featured_theme'] = $this->session->userdata['featured_theme'];
                $this->data['gallery_theme'] = $this->session->userdata['gallery_theme'];
                $this->data['resume_theme'] = $this->session->userdata['resume_theme'];
                $this->data['footer_theme'] = $this->session->userdata['footer_theme'];
            } else {
                $this->data['profile_theme'] = $profile_theme;
                $this->data['featured_theme'] = $featured_theme;
                $this->data['gallery_theme'] = $gallery_theme;
                $this->data['resume_theme'] = $resume_theme;
                $this->data['footer_theme'] = $footer_theme;
            }
            //------------------------Custome Theme Apply End--------------//

            if ($this->session->userdata("user_id")) {
                $profile_view = $this->input->post("profile");
                $this->data['profile'] = $profile_view;
                $to = $user_data['email'];
                $fname = $this->session->userdata("firstname");
                $lname = $this->session->userdata("lastname");
                if($user_data["enotify17"] == 1){
                    if ( ($this->session->userdata("role_id") == 3) || ($this->session->userdata("role_id") == 4) ){
                        $msg = "Your ".LABEL_VIEW_PROFILE_TEXT_LOG." ".$fname." ".$lname.".";
                        //$this->email_sender($to, LABEL_EMAIL_NOTIFY_SUBJECT_PROFILE_VIEW, $msg, '', '', '', '', '', 'Tyroe');
                    }
                }

                /*Set Activity Stream
                $get_company_name = $this->getRow("SELECT company_name FROM ".TABLE_COMPANY." WHERE studio_id = '".$this->session->userdata("user_id")."'");
                $companyname = $get_company_name['company_name'];
                $activity_data = array(
                    "viewer_name" => $fname." ".$lname,
                    "company_name" => $companyname
                );
                $this->set_activity_stream($this->session->userdata("user_id"), $user_data['user_id'], "", $this->session->userdata("role_id"), $activity_data, 7);
                Set Activity Stream*/

                $this->data['profiler_id'] = $user_id;

                if ($profile_view == "1") {
                    $this->template_arr = array('contents/edit_profile3');
                } else {
                    $this->template_arr = array('header', 'contents/edit_profile3', 'footer');
                }
            } else {
                $this->template_arr = array('header', 'contents/edit_profile3', 'footer');
            }


        } else {
            $this->output->set_status_header('404');
            $this->template_arr = array('header_login','errors/404_index','footer');
        }

        $this->load_template();
    }

    public function loadmore_images()
    {
        //DIE THIS FUNCTION MOVED ON LOADMORE.PHP CONTROLLER
        die('forbidden');
        //$user_id = $this->session->userdata("user_id");
        $last_id = $this->input->get("last_id");
        $explode_data = explode('-', $last_id);

        $portfolioimage = $explode_data[1];
        $image_order_id = $explode_data[2];
        if (is_numeric($image_order_id) && is_numeric($portfolioimage)) {
            $media_obj = new Tyr_media_entity();
            $latestWorkImages = $media_obj->get_latest_work_for_user_public_profile($portfolioimage, $image_order_id);
//            $latestWorkImages = $this->getAll("SELECT
//  IFNULL(media_original_name, media_name) AS latest_work_image_src, image_id AS latest_work_id, media_imagesection AS latest_work_imagesection, media_sortorder AS latest_work_sortorder, media_title AS latest_work_title, media_tag AS latest_work_tag, media_description AS latest_work_description 
//FROM
//				" . TABLE_MEDIA . "
//				WHERE media_type = 'image'
//				AND status_sl = 1
//				AND media_featured = 0
//				AND profile_image = 0
//				AND user_id =
//				(SELECT
//				user_id
//				FROM
//				" . TABLE_MEDIA . "
//				WHERE image_id = " . $portfolioimage . ")
//				AND media_sortorder > " . $image_order_id . "
//				ORDER BY media_sortorder ASC
//				LIMIT 6
//                                        ");
            //echo $this->db->last_query();

            echo json_encode($latestWorkImages);


            //
        } else {
            //DIE INVALID DATA SUBMITTED THROWN AN ERROR TO USER!!!;

        }
        exit;
    }

    public function get_public_job(){
         #GEt jobid from url
        $job_name = $this->uri->segment(2);
        if($job_name != ''){
            $job_id = $this->get_number_from_name($job_name);
        }else{
            header('location:' . $this->getURL("login"));
        }
//        $get_jobs = $this->getRow("SELECT job_id,user_id,category_id,job_title,job_description,job_city,team_moderation," . TABLE_CITIES . ".city,".TABLE_JOB_TYPE.".job_type_id,".TABLE_JOB_TYPE.".job_type,".TABLE_COUNTRIES.".country
//                AS job_location,".TABLE_COUNTRIES.".country_id,".TABLE_JOB_LEVEL.".job_level_id,".TABLE_JOB_LEVEL.".level_title AS job_skill,start_date,end_date,".TABLE_INDUSTRY.".industry_name,".TABLE_INDUSTRY.".industry_id,job_quantity," . TABLE_JOBS . ".created_timestamp,"
//                . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl FROM " . TABLE_JOBS . "
//                LEFT JOIN " . TABLE_COUNTRIES . " ON "
//                . TABLE_JOBS . ".country_id = " . TABLE_COUNTRIES . ".country_id
//                LEFT JOIN " . TABLE_CITIES . " ON "
//                                . TABLE_JOBS . ".job_city = " . TABLE_CITIES . ".city_id
//                LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id = "
//                . TABLE_JOB_LEVEL . ".job_level_id
//                LEFT JOIN ".TABLE_JOB_TYPE." ON " . TABLE_JOBS . ".job_type=".TABLE_JOB_TYPE.".job_type_id
//                LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id= ".TABLE_INDUSTRY.".industry_id WHERE job_id = '"
//                . $job_id . "' AND " . TABLE_JOBS . ".status_sl = '1' ");
        
        $join_obj = new Tyr_joins_entity();
        $get_jobs = $join_obj->get_job_details_for_public_page($job_id, 1);
        
        $this->data['datedifference'] = $this->dateDifference($get_jobs['start_date'],$get_jobs['end_date']);
        //$getskills=$this->getAll("SELECT creative_skill_id FROM ".TABLE_JOB_SKILLS." where job_id='".$job_id."'");

        $job_skill_entity = new Tyr_job_skills_entity();
        $job_skill_entity->job_id = $job_id;
        $getskills = $job_skill_entity->get_all_creative_skill_id_by_job();
        
        $job_skills=array();
        foreach($getskills as $skill)
        {
            //$job_skills[]=$this->getRow("SELECT creative_skill FROM ".TABLE_CREATIVE_SKILLS." where creative_skill_id='".$skill['creative_skill_id']."'");
            $creative_entity = new Tyr_creative_skills_entity();
            $creative_entity->creative_skill_id = $skill['creative_skill_id'];
            $creative_entity->get_creative_skills();
            $job_skills[] = array('creative_skill' => $creative_entity->creative_skill);
        }
        $this->data['job_id'] =$job_id;
        $this->data['job_skills'] = $job_skills;
        $this->data['get_jobs'] = $get_jobs;
        $this->data['hide_header'] = 'yes';
        //End getting donute chart pending and reviewed
        $this->template_arr = array('header','contents/publicopenings');
        $this->load_template();
    }

}