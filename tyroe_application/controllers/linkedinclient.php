<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Linkedinclient extends Tyroe_Controller
{
    public function Linkedinclient()
    {
        parent::__construct();
        define('API_KEY',      '75jqknj9opfr7c');
        define('API_SECRET',   'KNA412MwGIQnK0aS');
        //define('REDIRECT_URI', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME']);
        define('REDIRECT_URI', 'http://dev.tyroe.com/linkedinclient/linkedin/');
        define('SCOPE',        'r_fullprofile r_emailaddress rw_nus');
    }
    public function linkedin()
    {
        $ci = CI_Controller::get_instance();
        session_name('linkedin');
        session_start();
        if (isset($_GET['error'])) {
            // LinkedIn returned an error
            print $_GET['error'] . ': ' . $_GET['error_description'];
            exit;
        } elseif (isset($_GET['code'])) {
            // User authorized your application
            if ($_SESSION['state'] == $_GET['state']) {
                // Get token so you can make API calls
                $this->getAccessToken();
            } else {
                // CSRF attack? Or did you mix up your states?
                exit;
            }
        } else {
            if ((empty($_SESSION['expires_at'])) || (time() > $_SESSION['expires_at'])) {
                // Token has expired, clear the state
                $_SESSION = array();
            }
            if (empty($_SESSION['access_token'])) {
                // Start authorization process
                $this->getAuthorizationCode();
            }
        }
        $user = $this->fetch('GET', '/v1/people/~:(firstName,lastName,id)');
        $linked_user_id = $user->id;
        $user_id = $this->session->userdata("user_id");
        $user_ip  = $this->input->ip_address();
        $linkedin_token = $this->session->userdata('access_token');
        if(!$this->input->is_ajax_request()) {
            //   die('not allowed');
        }

        if(!$user_id) {
            die('please login');
        }
        if($linkedin_token==null &&  $linked_user_id==null ) {
            die('connection error');
        }
        
        $social_media_obj = new Tyr_fb_users_entity();
        $social_media_obj->user_id = $user_id;
        $social_media_obj->socialmedia_type = 'in';
        $social_media_obj->get_user_social_media_details();
        
//        $this->db->select('*');
//        $this->db->from(TABLE_FB_USERS);
//        $multi_where = array( 'user_id'=> $user_id, 'socialmedia_type' => 'in');
//        $this->db->where($multi_where);
//        $exists_record = $this->db->get()->result_array();

//         if($this->db->now_rows > 0) {
        
//        if(is_array(array_filter($exists_record)) && !empty($exists_record) ) {
        if($social_media_obj->id > 0 ) {
            
            $social_media_obj->socialmedia_id = $linked_user_id;
            $social_media_obj->access_token = $linkedin_token;
            $social_media_obj->ip = $user_ip;
            $social_media_obj->status_sl = 1;
            $social_media_obj->access_date = date("Y-m-d H:i:s");
            $social_media_obj->update_user_social_media_details();
        
       } else {
            $social_media_obj = new Tyr_fb_users_entity();
            $social_media_obj->user_id = $user_id;
            $social_media_obj->socialmedia_type = 'in';
            $social_media_obj->socialmedia_id = $linked_user_id;
            $social_media_obj->access_token = $linkedin_token;
            $social_media_obj->ip = $user_ip;
            $social_media_obj->status_sl = 1;
            $social_media_obj->access_date = date("Y-m-d H:i:s");
            $social_media_obj->save_fb_users();
        }
        die('<script>
            window.opener.location.reload();

            setTimeout(function () {
                window.close();
            } , 100)

        </script>');
    }

    function getAuthorizationCode() {
        $params = array('response_type' => 'code',
            'client_id' => API_KEY,
            'scope' => SCOPE,
            'state' => uniqid('', true), // unique long string
            'redirect_uri' => REDIRECT_URI,
        );

        // Authentication request
        $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);


        // Needed to identify request when it returns to us
        $_SESSION['state'] = $params['state'];

        // Redirect user to authenticate
        if(header("Location: $url")) {

        } else {
            redirect($url);
        }
    }

    function getAccessToken() {
        ///global $ci;
        $ci = CI_Controller::get_instance();
        $params = array('grant_type' => 'authorization_code',
            'client_id' => API_KEY,
            'client_secret' => API_SECRET,
            'code' => $_GET['code'],
            'redirect_uri' => REDIRECT_URI,
        );

        // Access Token request
        $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);

        // Tell streams to make a POST request
        $context = stream_context_create(
            array('http' =>
                array('method' => 'POST',
                )
            )
        );

        // Retrieve access token information
        $response = file_get_contents($url, false, $context);

        // Native PHP object, please
        $token = json_decode($response);



        // Store access token and expiration time
        $ci->session->set_userdata('access_token',$token->access_token);
        $ci->session->set_userdata('expires_in',$token->expires_in);
        $ci->session->set_userdata('expires_at',time()+ $token->expires);

        //$_SESSION['access_token'] = $token->access_token; // guard this!
        //$_SESSION['expires_in']   = $token->expires_in; // relative time (in seconds)
        //$_SESSION['expires_at']   = time() + $_SESSION['expires_in']; // absolute time
        return true;
    }

    function fetch($method, $resource, $body = '') {
        $ci = CI_Controller::get_instance();

        //$params = array('oauth2_access_token' => $_SESSION['access_token'],

        $params = array('oauth2_access_token' => $ci->session->userdata('access_token') ,
            'format' => 'json',
        );

        // Need to use HTTPS
        $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
        // Tell streams to make a (GET, POST, PUT, or DELETE) request
        $context = stream_context_create(
            array('http' =>
                array('method' => $method,
                )
            )
        );


        // Hocus Pocus
        $response = file_get_contents($url, false, $context);

        // Native PHP object, please
        return json_decode($response);
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */