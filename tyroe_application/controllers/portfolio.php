<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH . 'core/tyroe_Controller' . EXT);
require_once(APPPATH . 'entities/tyr_joins_entity' . EXT);
require_once(APPPATH . 'entities/tyr_media_entity' . EXT);

class Portfolio extends Tyroe_Controller
{
    public function Portfolio()
    {

        parent::__construct();
        $this->data['page_title'] = 'Portfolio';
        if ($this->session->userdata('role_id') != ADMIN_ROLE) {
            $this->get_system_modules(' AND module_id IN (' . PORTFOLIO_SUB_MENU . ') ');

        } else {
            $this->get_system_modules(' AND module_id IN (' . USER_MODULE_IN . ') ');
        }
    }

    public function index()
    {
        $user_id = $this->session->userdata("user_id");
        
        $media_obj = new Tyr_media_entity();
        $media_obj->user_id = $user_id;
        $media_obj->status_sl = 1;
        $media_obj->media_type = 'image';
        $media_obj->profile_image = 1;
        $media = $media_obj->get_all_user_media_by_type();
        $this->data['get_media'] = $media;
        
        
        $media_obj_1 = new Tyr_media_entity();
        $media_obj_1->user_id = $user_id;
        $media_obj_1->status_sl = 1;
        $media_obj_1->media_type = 'video';
        $media_obj_1->profile_image = 1;
        $video = $media_obj_1->get_all_user_media_by_type();
        $this->data['video'] = $video;
        
        
        $media_obj_2 = new Tyr_media_entity();
        $media_obj_2->user_id = $user_id;
        $media_obj_2->status_sl = 1;
        $media_obj_2->media_featured = 1;
        $media_obj_2->profile_image = 1;
        $featured = $media_obj_2->get_all_user_media_by_featured();
        $this->data['featured'] = $featured;
        

//        $media_limit = $this->Execute("SELECT image_id, user_id, profile_image,media_type,media_name,media_title,media_description,created,
//                    modified,status_sl FROM " . TABLE_MEDIA . "
//                    WHERE profile_image != '1' AND " . TABLE_MEDIA . ".status_sl='1' AND media_type='image' AND user_id = '" . $user_id . "'");
        
        $this->data['media_limit'] = count($media);

        //$media_qty =$this->getRow("select variable_value as cnt from " . TABLE_SYSTEM_VARIABLES . " where variable_name='image_qty_portfolio'");

              //  $this->data['qty_image'] = $media_qty['cnt'];
        $this->template_arr = array('header', 'contents/get_portfolio', 'footer');
        $this->load_template();
    }

    public function getform($data = "")
    {
        if (!empty($data)) {
            $user_id = $this->session->userdata("user_id");
            
            $joins_obj = new Tyr_joins_entity();
            $data_img = $joins_obj->get_data_image($data,$user_id);
            $this->data['media_data'] = $data_img;
        }
        $this->template_arr = array('contents/portfolio_form');
        $this->load_template();
    }

    public function save_image()
    {
        //$image_name = explode(',', $this->input->post("image"));
        $image_id = $this->input->post("media_id");
        $user_id = $this->session->userdata("user_id");
        if (!empty($image_id)) {
            $featured = $this->input->post("media_featured");
            if($featured == 1)
            {
                $media_obj = new Tyr_media_entity();
                $media_obj->user_id = $user_id;
                $media_obj->media_featured = 0;
                $media_obj->update_media_featured_by_user();
                
                $media_obj_1 = new Tyr_media_entity();
                $media_obj_1->image_id = $image_id;
                $media_obj_1->get_media();
                $media_obj_1->media_featured = 1;
                $media_obj_1->update_media();
            }
            
            $media_obj_2 = new Tyr_media_entity();
            $media_obj_2->image_id = $image_id;
            $media_obj_2->get_media();
            $media_obj_2->media_featured = $featured;
            $return = $media_obj_2->update_media();
            if ($return) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_UPDATE_SUCCESS));
                $this->session->set_userdata("status_msg", MESSAGE_UPDATE_SUCCESS);
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
            header('location:' . $this->getURL("portfolio"));
        } else {
            $featured = $this->input->post("media_featured");
            $image_name = $this->input->post("image");
            $with_p1 = str_ireplace('</p>', ',', $image_name);
            $with_p2 = str_ireplace('<p>', '', $with_p1);
            $image_name = explode(',', $with_p2);
            $image_name = array_slice($image_name, 0, -1);
            $count = count($image_name);
            //CHECK LOOP AND RETURN OBJ
            for ($i = 0; $i < $count; $i++) {
                if ($featured == 1) {
                    $media_featured_obj = new Tyr_media_entity();
                    $media_featured_obj->user_id = $user_id;
                    $media_featured_obj->media_featured = 0;
                    $media_featured_obj->update_media_featured_by_user();
                }
                
                $media_save_obj = new Tyr_media_entity();
                $media_save_obj->user_id = $user_id;
                $media_save_obj->media_title = $this->input->post("media_title");
                $media_save_obj->media_description = $this->input->post("image_description");
                $media_save_obj->media_featured = $featured;
                $media_save_obj->media_name = $image_name[$i];
                $media_save_obj->profile_image = 0;
                $media_save_obj->media_type = "image";
                $media_save_obj->save_media();
            }
            
            header('location:' . $this->getURL("portfolio"));
            if ($return) {
                echo json_encode(array('success' => true, 'success_message' => MESSAGE_ADD_SUCCESS));
            } else {
                echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
            }
        }

    }

    public function DeleteImage()
    {
        $media_obj = new Tyr_media_entity();
        $media_obj->image_id = $this->input->post("image_id");
        $media_obj->get_media();
        $media_obj->status_sl = -1;
        $return = $media_obj->update_media();

        if ($return) {
            echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
        } else {
            echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
        }
    }

    public function upload_image()
    {
        $media_type = $this->input->post("media_type");
        if ($media_type == 'video') {
            $user_id = $this->session->userdata("user_id");
            $video_url = $this->input->post("video_url");
            $vid_desc = $this->input->post("video_desc");
            
            $joins_obj = new Tyr_joins_entity();
            $chk_video = $joins_obj->check_video_group_img_id($user_id);
            
            if (!empty($chk_video)) {
                $media_obj = new Tyr_media_entity();
                $media_obj->image_id = $chk_video['group_img_id'];
                $media_obj->get_media();
                $media_obj->media_name = $video_url;
                $media_obj->media_description = $vid_desc;
                $media_obj->update_media();
                $result['success'] = MESSAGE_ADD_SUCCESS;
            }else{
                $media_obj_1 = new Tyr_media_entity();
                $media_obj_1->user_id = $user_id;
                $media_obj_1->media_title = $this->input->post("media_title");
                $media_obj_1->media_name = $video_url;
                $media_obj_1->media_description = $vid_desc;
                $media_obj_1->profile_image = 0;
                $media_obj_1->media_type = $media_type;
                $media_obj_1->save_media();
                $result['success'] = MESSAGE_UPDATE_SUCCESS;
            }
            echo json_encode($result['success']);

        } else {
            $video_ext_array = array('avi', 'mpg', 'mpeg', 'wmv', 'mp4', '3gp', 'flv', 'mp3', 'AVI', 'MPG', 'MPEG', 'WMV', 'MP4', '3GP', 'FLV', 'MP3');
            $pathinfo = pathinfo($_FILES['Filedata']['name']);
            $pathinfo_ext = $pathinfo['extension'];
            $verifyToken = md5('unique_salt' . $this->input->post('timestamp'));

            $image_upload_folder = 'assets/uploads/portfolio1';
            $image_upload_folder_thumb = 'assets/uploads/portfolio1/thumb';

            if (!file_exists($image_upload_folder)) {
                mkdir($image_upload_folder, DIR_WRITE_MODE, true);
            }
            $this->upload_config = array(
                'upload_path' => $image_upload_folder,
                'allowed_types' => '*',
                'max_size' => 0,
                'remove_space' => TRUE,
                'encrypt_name' => TRUE,
            );
            $this->upload->initialize($this->upload_config);

            if (!empty($_FILES) && $this->input->post('token') == $verifyToken) {
                if (!$this->upload->do_upload('Filedata')) {
                    $upload_error = $this->upload->display_errors();
                    echo json_encode($upload_error);
                } else {
                    $file_info = $this->upload->data();
                    $resp[] = $file_info;

                    /*=======  Making Thumbnail of Image  ========*/
                    $config = array(
                        'source_image' => $file_info['full_path'], //path to the uploaded image
                        'new_image' => $image_upload_folder_thumb, //path to
                        'maintain_ratio' => true,
                        'width' => 273,
                        'height' => 206,
                        'create_thumb' => TRUE
                    );
                    $this->image_lib->initialize($config);
                    $this->image_lib->resize();

                }

                echo json_encode($resp);
            }
        }
    }
    public function delete_video()
       {
            $media_obj = new Tyr_media_entity();
            $media_obj->image_id = $this->input->post("image_id");
            $media_obj->get_media();
            $media_obj->status_sl = -1;
            $return = $media_obj->update_media();

           if ($return) {
               echo json_encode(array('success' => true, 'success_message' => MESSAGE_DELETE_SUCCESS));
           } else {
               echo json_encode(array('success' => false, 'success_message' => MESSAGE_ERROR));
           }
       }

}
