<?php
$ci = &get_instance();
$ci->load->database();

function get_activity_time($save_date)
{
    $datetime1 = date_create(date('Y-m-d h:i:s A',$save_date));
    $datetime2 = date_create(date('Y-m-d h:i:s A'));
    $interval = date_diff($datetime1, $datetime2);

    $show_time = "";
    if($interval->i > 0){
        $show_time = $interval->i." min(s)";
    }
    if($interval->h > 0){
        $show_time = $interval->h." hour(s)";
    }
    if($interval->d > 0){
        $show_time = $interval->d." day(s)";
    }
    return $show_time;
}
?>