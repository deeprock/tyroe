<?php
$ci = &get_instance();
$ci->load->database();

function get_activity_time($save_date)
{
    $datetime1 = date_create(date('Y-m-d h:i:s A',$save_date));
    $datetime2 = date_create(date('Y-m-d h:i:s A'));
    $interval = date_diff($datetime1, $datetime2);

    $show_time = $interval;
    if($interval->s > 0)
    {
        $sec = ($interval->s ==1)?'sec.':'secs.';
        $show_time = $interval->s." ".$sec;
    }
    if($interval->i > 0){
        $min = ($interval->i == 1)?"min.":"mins.";
        $show_time = $interval->i." ".$min;
    }
    if($interval->h > 0){
        $hour = ($interval->h == 1)?"hour.":"hours.";
        $show_time = $interval->h." ".$hour;
    }
    if($interval->d > 0){
        $day = ($interval->d == 1)?"day.":"days.";
        $show_time = $interval->d." ".$day;
    }
    return $show_time;
}

function print_array($array_data = null, $die_exit = FALSE,$print_heading = null) {
    echo '<pre style="width: 100%; clear: both; background: #EEE; font-family: consolas; padding: 5px 0px; margin: 5px 0px; border-bottom: #C5C5C5 4px solid; font-size:12px;word-wrap: break-word;" class="printed_data">';
    if($print_heading){
        echo '<h2 style="font-weight: bold; font-size: 16px;"> >> '.$print_heading.' << </h2>';
    }
    //print_r(debug_backtrace());
    if(is_array($array_data) || is_object($array_data)) {
        print_r($array_data);
    } else if(is_string($array_data) || is_numeric($array_data)) {
        echo $array_data;
    } else {
        var_dump($array_data);
    }
    echo '</pre>';

    if($die_exit === TRUE) { exit; }
}

function get_date_diff($date1 , $date2){
    $datetime1 = new DateTime($date1);
    $datetime2 = new DateTime($date2);
    $interval  = $datetime1->diff($datetime2);
    $year = "";
    $month = "";
    $day = "";
    if($interval->y > 0)
    {
        $year =($interval->y == 1)?'%y year,':'%y years,';
    }
    if($interval->m > 0)
    {
        $month =($interval->m == 1)?'%m month,':'%m months,';
    }
    if($interval->d > 0)
    {
        $day =($interval->d == 1)?'%d day':'%d days';
    }
    $elapsed   = $interval->format($year . $month . $day);
    if($elapsed =="")
    {
        $elapsed = "Archived";
    }
    return $elapsed;
}