<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Faisal (Rameez)
 * Date: 12/31/13
 * Time: 12:42 PM
 * To change this template use File | Settings | File Templates.
 */

$CI = &getInstance();
$CI->load->database();

function is_ajax() {
   $xml_http_req = $CI->input->server('HTTP_X_REQUESTED_WITH');
   if(!empty($xml_http_req) && strtolower($xml_http_req) == 'xmlhttprequest') {
       return true;
   }
   return false;
}
function user_ip(){
    return $CI->input->ip_address();
}