<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
define('TABLE_PREFIX','tyr_');
define('TABLE_SESSIONS',TABLE_PREFIX."sessions");
define('TABLE_CATEGORY',TABLE_PREFIX."category");
define('TABLE_USERS',TABLE_PREFIX."users");
define('TABLE_MEDIA',TABLE_PREFIX."media");
define('TABLE_IMAGE_QUEUE',TABLE_PREFIX."image_queue");
define('TABLE_INVITE_REVIEWER',TABLE_PREFIX."invite_reviewer"); //create on 23/9/2014 by zain
define('TABLE_SHORTLIST',TABLE_PREFIX."job_shortlist"); //this is old one , not in use
define('TABLE_JOB_SHORTLIST',TABLE_PREFIX."job_shortlist"); //this is new, and currently using
define('TABLE_FAVOURITE_PROFILE',TABLE_PREFIX."favourite_profile");
define('TABLE_FAVOURITE_JOB',TABLE_PREFIX."favourite_job");
define('TABLE_FEATURED_TYROE',TABLE_PREFIX."featured_tyroes");
define('TABLE_COMMENT_JOB',TABLE_PREFIX."comment_job");
define('TABLE_OPTIONS',TABLE_PREFIX."options");
define('TABLE_SCHEDULE_NOTIFICATION',TABLE_PREFIX."schedule_notification");
define('TABLE_PUBLISH_NOTIFICATION',TABLE_PREFIX."publish_notificaion");
define('TABLE_COMMENT_PROFILE',TABLE_PREFIX."comment_profile");
define('TABLE_PROFILE_VIEW',TABLE_PREFIX."profile_view");
define('TABLE_RATING_PROFILE',TABLE_PREFIX."rating_profile");
define('TABLE_RATING_JOB',TABLE_PREFIX."rating_job");
define('TABLE_HIDDEN',TABLE_PREFIX."hidden");
define('TABLE_HIDE_USER',TABLE_PREFIX."hide_user");
define('TABLE_JOBS',TABLE_PREFIX."jobs");
define('TABLE_REVIEWER_REVIEWS',TABLE_PREFIX."reviewer_reviews");
define('TABLE_STUDIO_REVIEWERS',TABLE_PREFIX."studio_reviewers(depricated)");
define('TABLE_REVIEWER_JOBS',TABLE_PREFIX."reviewer_jobs");
define('TABLE_JOB_ACTIVITY',TABLE_PREFIX."job_activity");
define('TABLE_JOB_APPLY',TABLE_PREFIX."job_apply");
define('TABLE_JOB_CANDIDATE',TABLE_PREFIX."job_candidate");
define('TABLE_ACTIVITY_LOG',TABLE_PREFIX."activity_log");
define('TABLE_ROLES',TABLE_PREFIX."roles");
define('TABLE_COUNTRIES',TABLE_PREFIX."countries");
define('TABLE_TYROE_PROFILE',TABLE_PREFIX."tyroe_profile");
define('TABLE_MODULES',TABLE_PREFIX."modules");
define('TABLE_PERMISSIONS',TABLE_PREFIX."permissions");
define('TABLE_EXPERIENCE',TABLE_PREFIX."experience");
define('TABLE_EDUCATION',TABLE_PREFIX."education");
define('TABLE_SKILLS',TABLE_PREFIX."skills");
define('TABLE_AWARDS',TABLE_PREFIX."awards");
define('TABLE_REFRENCES',TABLE_PREFIX."refrences");
define('TABLE_RECOMMENDATION',TABLE_PREFIX."recommendation");
define('TABLE_RECOMMENDATIONEMAIL_HISTORY',TABLE_PREFIX."recommendemail_history");
define('TABLE_LOGIN_HISTORY',TABLE_PREFIX."login_history");
define('TABLE_INVITE_TYROE',TABLE_PREFIX."invite_tyroe");
define('TABLE_CREATIVE_SKILLS',TABLE_PREFIX."creative_skills");
define('TABLE_JOB_LEVEL',TABLE_PREFIX."job_level");
define('TABLE_JOB_SKILLS',TABLE_PREFIX."job_skills");
define('TABLE_FORGOT_PW',TABLE_PREFIX."forgotpw");
define('TABLE_INVITE_FRIEND',TABLE_PREFIX."invite_friend");
define('TABLE_JOB_TYPE',TABLE_PREFIX."job_type");
define('TABLE_EXPRESS_INTEREST_JOB',TABLE_PREFIX."express_interest_job");

define('TABLE_COUPONS',TABLE_PREFIX."coupons");
define('TABLE_COUPONS_ALLOT',TABLE_PREFIX."coupons_allot_users");

define('TABLE_STATES',TABLE_PREFIX."states");
define('TABLE_RATE_REVIEW',TABLE_PREFIX."rate_review");
define('TABLE_SUBSCRIPTION',TABLE_PREFIX."subscription");

define('TABLE_FB_USERS',TABLE_PREFIX."fb_users");
define('TABLE_NOTIFICATION_TEMPLATE',TABLE_PREFIX."notification_templates");
define('TABLE_NOTIFICATION_LOG',TABLE_PREFIX."notification_log");
define('TABLE_STATUSES',TABLE_PREFIX."statuses");

define('TABLE_COMPANY',TABLE_PREFIX."company_detail");
define('TABLE_ACTIVITY_TYPE',TABLE_PREFIX."activity_type");

define('TABLE_SYSTEM_VARIABLES',TABLE_PREFIX."system_variables");
define('TABLE_PROFESSIONAL_LEVEL',TABLE_PREFIX."professional_level");
define('TABLE_OTHER_SITES',TABLE_PREFIX."other_site");
define('TABLE_USER_SITES',TABLE_PREFIX."user_sites");
define('TABLE_JOB_DETAIL',TABLE_PREFIX."job_detail");
define('TABLE_INDUSTRY',TABLE_PREFIX."industry");
define('TABLE_EXPERIENCE_YEARS',TABLE_PREFIX."experience_years");
define('TABLE_CITIES',TABLE_PREFIX."cities");

define('TABLE_SOCIAL_LINKS',TABLE_PREFIX."social_link");
define('TABLE_DELETE_ACCOUNT',TABLE_PREFIX."deleted_account");
define('TABLE_VISIBLITY_OPTIONS',TABLE_PREFIX."visibility_options");
define('TABLE_CUSTOM_THEME',TABLE_PREFIX."custome_theme");


define('TABLE_GALLERY_TAGS',TABLE_PREFIX."gallery_tags");
define('TABLE_GALLERY_TAGS_REL',TABLE_PREFIX."gallery_tags_rel");
define('TABLE_PAGEVIEWS',TABLE_PREFIX."pageviews");
define('TABLE_SEARCH_MESSAGES',TABLE_PREFIX."search_messages");