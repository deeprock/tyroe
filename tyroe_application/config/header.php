<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html class="no-js lt-ie9 lt-ie8" lang="en"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html class="no-js lt-ie9" lang="en"><![endif]-->
<!--[if (IE 9)]><html class="no-js ie9" lang="en"><![endif]-->
<!--[if gt IE 8]><!--> <html lang="en-US"> <!--<![endif]-->
<html >
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

        <title><?= $page_title ?></title>
        <meta name="description" content="<?php echo $this->session->userdata("sv_metatag_desc")?>">
        <meta name="keywords" content="<?php echo $this->session->userdata("sv_metatag_keyword")?>">
        <meta name="author" content="<?php echo $this->session->userdata("sv_metatag_author")?>">
        <?php
        if(true===$load_masonry_file){
        ?>
        <script src="<?= ASSETS_PATH ?>/js/masonry.pkgd.min.js"></script>
        <?php
        }
        ?>
        <link rel="canonical" href="<?php echo current_url(); ?>/" />
        <?php if($get_jobs['job_title']){ ?>
            <meta property="og:type" content="job" />
            <meta property="og:title" content="<?php echo $get_jobs['job_title']; ?>" />

        <?php }else{?>
            <meta property="og:type" content="jobs" />
            <meta property="og:title" content="<?php echo $this->session->userdata('firstname').' '.$this->session->userdata('lastname').' Jobs'; ?>" />

        <?php } ?>

        <?php if($get_jobs['job_description']){ ?>
            <meta property="og:description" content="<?php echo $get_jobs['job_description']; ?>" />
        <?php }else{?>
            <meta property="og:description" content="Apply with <?php echo $this->session->userdata('firstname').' '.$this->session->userdata('lastname').' Jobs'; ?>" />
        <?php } ?>
        <meta property="og:url" content="<?php echo current_url(); ?>/" />
        <meta property="og:image" content="<?= ASSETS_PATH ?>img/logo_tyroe.png" />


        <?php if(strtolower(ENVIRONMENT) =='development') { ?>
             <meta http-equiv="cache-control" content="max-age=0" />
             <meta http-equiv="cache-control" content="no-cache" />
             <meta http-equiv="expires" content="0" />
             <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
             <meta http-equiv="pragma" content="no-cache" />
         <?php } ?>

        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">-->

        <!-- Mobile Specifics -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="HandheldFriendly" content="true"/>
        <meta name="MobileOptimized" content="320"/>

        <!-- Mobile Internet Explorer ClearType Technology -->
        <!--[if IEMobile]>  <meta http-equiv="cleartype" content="on">  <![endif]-->

        <style>
            ::selection {background: #f97e76; color: #fff;}
            ::-moz-selection {background: #f97e76; color: #fff;}
            ::-webkit-selection {background: #f97e76; color: #fff;}
        </style>
        
        <?php
        //$controller_name
        //
if ($controller_name == "profile" || $controller_name == "publicprofile" || $controller_name == "profile2" || $controller_name == "search_publicprofile"){
    ?>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="HandheldFriendly" content="true"/>
    <meta name="MobileOptimized" content="320"/>

    <link href="<?= ASSETS_PATH ?>profile/anibus/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/settings.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/main.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/jquery.fancybox.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/fonts.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/shortcodes.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/responsive.css" type="text/css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/custom.css" rel="stylesheet"/>
    <!--<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>-->
    <!--<link href="<?/*= ASSETS_PATH */?>profile/layout-profile.css" rel="stylesheet"/>-->
    <link href="<?= ASSETS_PATH ?>profile/chechkbox.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>profile/anibus/viewport.css" rel="stylesheet"/>

    <!--<link href="<?/*= ASSETS_PATH */?>profile/anibus/layout-profile-viewport.css" rel="stylesheet"/>-->
   <!-- <link href="<?/*= ASSETS_PATH */?>profile/layout-profile.css" rel="stylesheet"/>-->
    <link href="<?= ASSETS_PATH ?>profile/anibus/js/modernizr.js" rel="stylesheet"/>

    <!--<link href="<?/*= ASSETS_PATH */?>profile/bootstrap.min.css" rel="stylesheet"/>

    <link href="<?/*= ASSETS_PATH */?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/icon-fonts.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/settings.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/main.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/shortcodes.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/responsive.css" type="text/css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/custom.css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/layout-profile.css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/chechkbox.css" rel="stylesheet"/>
    <link href="<?/*= ASSETS_PATH */?>profile/layout-profile-viewport.css" rel="stylesheet"/>-->

   <?php
}
/*else if($controller_name == "profile" ){
    ?>
    <link href="<?= ASSETS_PATH ?>profile/bootstrap.min.css" rel="stylesheet"/>

        <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/icon-fonts.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/settings.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/main.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/shortcodes.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/responsive.css" type="text/css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/custom.css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/layout-profile.css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/chechkbox.css" rel="stylesheet"/>
        <link href="<?= ASSETS_PATH ?>profile/layout-profile-viewport.css" rel="stylesheet"/>

}*/
else{
    ?>
    <!-- bootstrap -->
    <!--<link rel="stylesheet" type="text/css" href="<?/*= ASSETS_PATH */?>css/smart-grid.css">-->
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-responsive.css" rel="stylesheet"/>
    <link href="<?= ASSETS_PATH ?>css/bootstrap/bootstrap-overrides.css" type="text/css" rel="stylesheet"/>

    <!-- custom styles -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/custom.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/layout.css">
<?php
}
?>

        <!-- libraries -->
        <link href="<?= ASSETS_PATH ?>css/lib/jquery-ui-1.10.2.custom.css" rel="stylesheet" type="text/css"/>
        <link href="<?= ASSETS_PATH ?>css/lib/bootstrap-wysihtml5.css" type="text/css" rel="stylesheet">
        <link href="<?= ASSETS_PATH ?>css/lib/uniform.default.css" type="text/css" rel="stylesheet">
        <link href="<?= ASSETS_PATH ?>css/lib/select2.css" type="text/css" rel="stylesheet">
        <link href="<?= ASSETS_PATH ?>css/lib/bootstrap.datepicker.css" type="text/css" rel="stylesheet">
        <link href="<?= ASSETS_PATH ?>css/lib/font-awesome.css" type="text/css" rel="stylesheet"/>

        <!--<link href="<?/*= ASSETS_PATH */?>profile/font/font-awesome.css" type="text/css" rel="stylesheet"/>-->
        <!-- global styles -->

        <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/elements.css">
        <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>css/icons.css">
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>profile/colorpicker.css" type="text/css" />
        <link rel="stylesheet" media="screen" type="text/css" href="<?= ASSETS_PATH ?>profile/layout.css" />
        <!-- this page specific styles -->
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/form-wizard.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/compiled/index.css" type="text/css" media="screen"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/reponsive-touches.css" type="text/css" media="screen"/>
        <!--<link href="<?/*= ASSETS_PATH */?>css/lib/select2.css" type="text/css" rel="stylesheet" />-->
        <!--Social share styles -->
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/social_share/jquery.share.css" type="text/css" media="screen"/>
        <!--/End Social share styles -->

        <!-- open sans font -->
        <link
            href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
            rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>


        <![endif]-->

        <?php /* <script src="http://code.jquery.com/jquery-latest.min.js"></script>  */ ?>

        <script src="<?= ASSETS_PATH ?>js/latest-jquery-min.js"></script>

        <script src="<?= ASSETS_PATH ?>js/date.format.js"></script>

        <script src="<?= ASSETS_PATH ?>profile/modernizr.js"></script>
        <!--Social share script-->
        <script src="<?= ASSETS_PATH ?>js/social_share/jquery.share.js"></script>
        <!--/End Social share script-->


        <script type="text/javascript">

        /*    jQuery(document).ready(function(){
             $(".btn-toggler").click(function(){
           alert("sasa");
        });
                jQuery("#menu-toggler").trigger("click");
    })*/
        jQuery(document).ready(function(){
            $.fn.extend({
                 donetyping: function(callback,timeout){
                     timeout = timeout || 1e5; // 1 second default timeout
                     var timeoutReference,
                         doneTyping = function(el){
                             if (!timeoutReference) return;
                             timeoutReference = null;
                             callback.call(el);
                         };
                     return this.each(function(i,el){
                         var $el = $(el);
                         $el.is(':input') && $el.keydown(function(){
                             if (timeoutReference) clearTimeout(timeoutReference);
                             timeoutReference = setTimeout(function(){
                                 doneTyping(el);
                             }, timeout);
                         }).blur(function(){
                             doneTyping(el);
                         });
                     });
                 }
             });

        });

        </script>
        <style type="text/css">
            .icon-chevron-left{
                color: #96C134 !important;
            }
            .icon-chevron-right{
                           color: #96C134 !important;
                       }
        </style>

        <script>
            /*.team-first div - row or row-fluid change on Window Resize*/
            $(document).ready(function(){
                // Switch slide buttons
                $('.slider-button').click(function() {
                    if ($(this).hasClass("on")) {
                        $(this).removeClass('on').html($(this).data("off-text"));
                    } else {
                        $(this).addClass('on').html($(this).data("on-text"));
                    }
                });
                window.row_fluid_row = "";
                var row_fluid_row_func = function(){
                    var default_window_size = $(window).width();
                    if( ($(window).width() >= 0) && (default_window_size <= 767) ){
                        window.row_fluid_row = "row";
                        $(".team-first").removeClass('row-fluid').addClass('row');
                    } else if(default_window_size > 767) {
                        window.row_fluid_row = "row-fluid";
                        $(".team-first").removeClass('row').addClass('row-fluid');
                    }
                }
                row_fluid_row_func();
                $(window).resize(function(){ row_fluid_row_func(); });


                /*Mobile Menu Toggler - Start*/
                var $menu = $("#sidebar-nav");
                $("body").click(function () {
                    if ($menu.hasClass("display")) {
                        $menu.removeClass("display");
                    }
                });
                $menu.click(function(e) {
                    e.stopPropagation();
                });
                $("#menu-toggler").click(function (e) {
                    e.stopPropagation();
                    $menu.toggleClass("display");
                });
                /*Mobile Menu Toggler - End*/
            });
        </script>
        <script>
            function multiplechecks(master_check,checkboxes)
                  {
                      var is_checked = null;
                      var checked_elez = $(checkboxes);
                      var is_checked = master_check.is(':checked');

                      if(is_checked==true){
                          checked_elez.prop('checked',true);
                      } else {
                          checked_elez.prop('checked',false);
                      }
                      return;
                      if(master_check.checked) { // check select status
                          $(checkboxes).each(function() { //loop through each checkbox
                              var each_ele  = $(this);
                              each_ele.attr('checked','checked');
                              checkboxes.checked = true;  //select all checkboxes with class "param2"
                          });
                      }else{
                          $(checkboxes).each(function() { //loop through each checkbox
                              checkboxes.checked = false; //deselect all checkboxes with class "param2"
                          });
                      }
                  }
        </script>
        <script>
            //function for profile iframes
            var callback = 1;
            function open_dropdown(){
                if( callback == 1){
                    $('#profile_iframe').contents().find('.dropdown-menu').show();
                    callback = 0;
                }else{
                    $('#profile_iframe').contents().find('.dropdown-menu').hide();
                    callback = 1;
                }
            }
            function add_in_opening_jobs(iframe_job_id,iframe_tyroe_id,thisanc){
                /*First Param =  JOB ID*/
                /*Second Param =  Tyroe ID*/
                $('#profile_iframe').contents().find('.dropdown-menu .add_job_anch'+iframe_job_id).addClass('disable_opening');
                $('#profile_iframe').contents().find('.dropdown-menu .add_job_anch'+iframe_job_id).removeClass('add_in_opening_jobs');
                var job_id = iframe_job_id;
                var tyroe_id = iframe_tyroe_id;
                $.ajax({
                    type: "POST",
                    url: '<?=$vObj->getURL("openingoverview/InviteTyroe")?>',
                    dataType: "json",
                    data:{job_id:job_id,tyroe_id:tyroe_id,search_param:'add_by_search'},
                    success: function (data) {
                        if (data.success) {
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                        }
                        else {
                            $(".notification-box-message").css("color", "#b81900");
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                            //$("#error_msg").html(data.success_message);
                        }
                    },
                    failure: function (errMsg) {
                    }
                });
            }
            function favourite_profile(iframe_tyroe_id){
                var uid = iframe_tyroe_id;
                var data = "uid=" + uid;
                $.ajax({
                    type: "POST",
                    data: data,
                    url: '<?=$vObj->getURL("findtalent/favourite_candidate")?>',
                    dataType: "json",
                    success: function (data) {
                        if (data.success) {
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                        }
                        else {
                            $(".notification-box-message").css("color", "#b81900")
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                            //$("#error_msg").html(data.success_message);
                        }
                    },
                    failure: function (errMsg) {
                    }
                });
            }
            function message_tyroe(iframe_tyroe_id){
                $(".sugg-msg").html("");
                msg_to=iframe_tyroe_id;
                $("#hd_email_to"+msg_to).val(msg_to);
                $('#myModal').modal('show');
                rescale();
                $('.modal-body').css('height', 146);
            }
            function hide_search_single(iframe_tyroe_id){
                var tyroe_id = iframe_tyroe_id;
                $.ajax({
                    type: "POST",
                    data: {tyroe_id:tyroe_id},
                    url: '<?=$vObj->getURL("openingoverview/HideUser")?>',
                    dataType: "json",
                    success: function (data) {
                        if (data.success) {
                            $(".row-id"+tyroe_id).hide(500);
                            $(".editor-seperator-row"+tyroe_id).hide(500);
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                        }
                        else {
                            $(".notification-box-message").css("color", "#b81900")
                            $(".notification-box-message").html(data.success_message);
                            $(".notification-box").show(100);
                            setTimeout(function () {
                                $(".notification-box").hide();
                            }, 5000);
                            //$("#error_msg").html(data.success_message);
                        }
                        $('.listview_btn').trigger('click');
                        $('.hiderow'+tyroe_id).remove();
                    }
                });
            }

            //profile view adjustment in search controller , favourite controller , job applicaint , candidate , shortlist
            <?php if ($controller_name == "search" || $controller_name == "openingoverview"  || $controller_name == "favourite"){?>
                $(window).bind('resize',function(){
                    var profile_height = jQuery('#profile_iframe').contents().find('html body .iframe-body').height();
                    jQuery('#profile_iframe').height(profile_height+'px');
                    jQuery('#profile_iframe').css({"border":"0"});
                    jQuery('.loader-save-holder').hide();
                })
            <?php } ?>

        </script>
    </head>
<body>
    <div class="notification-box">
        <div class="notification-box-message"></div>
    </div>
    <!-- navbar -->
<?php
if ($controller_name!="publicprofile") {
    if( ($controller_name!="profile") && ($vObj->publish_or_not() == 0) && $this->session->userdata('role_id')==TYROE_FREE_ROLE) {
    //if($controller_name!="profile") {
    ?>
    <div class="row-fluid" id="publish_notify">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                <?=UNPUBLISH_PROFILE_MESSAGE?>
                <a href="javascript:void(0);" class="remove-publish-notify"><i class="icon-remove icon-remove-profilemessage"></i> </a>
            </div>
        </div>
    </div>
    <?php }
       if( ($controller_name!="profile") && ($this->session->userdata('role_id')==TYROE_FREE_ROLE) && ($this->session->userdata('recommendation_notification')>0) ) {
    ?>
    <div class="row-fluid" id="recommendation_notify">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                <?=RECOMMNENDATION_RECIEVED_MESSAGE?>
                <a href="javascript:void(0);" class="remove-publish-notify"><i class="icon-remove icon-remove-recommendation"></i> </a>
            </div>
        </div>
    </div>
    <?php } if( ($controller_name!="profile") && ($this->session->userdata('role_id')==TYROE_FREE_ROLE) && ($this->session->userdata('latest_jobs')>0) ) { ?>
    <div class="row-fluid" id="latestjobs_notify">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                <?=str_replace(0,$this->session->userdata('latest_jobs'),LATEST_JOB_MESSAGE)?>
                <a href="javascript:void(0);" class="btn-flat btn-remove-newjob-messages">View</a>
                <a href="javascript:void(0);" class="remove-publish-notify"><i class="icon-remove icon-remove-newjob-messages"></i> </a>
            </div>
        </div>
    </div>
    <?php } ?>
    <?php if($this->session->userdata('role_id') == STUDIO_ROLE && $this->session->userdata('pending_reviewer') != 0 && $controller_name!="team"){ ?>
    <div class="row-fluid" id="latestjobs_notify">
        <div class="span12">
            <div class="alert alert-info text-center">
                <i class="icon-exclamation-sign"></i>
                You have <?=$this->session->userdata('pending_reviewer')?> reviewer in pending
                <a href="javascript:void(0);" class="active_reviewer_notification"><i class="icon-remove icon-remove-newjob-messages"></i> </a>
            </div>
        </div>
    </div>
    <?php } ?>
    <div class="navbar navbar-inverse">
        <div class="navbar-inner">
            <button type="button" class="btn btn-navbar visible-phone custom-hide" id="menu-toggler">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="brand" href="<?= LIVE_SITE_URL ?>"><img class="logo-main"
                                                              src="<?= ASSETS_PATH ?>img/logo_tyroe.png"></a>
            <ul class="nav pull-right">
                <?php
                if ($this->session->userdata('logged_in')) {
                    ?>
                    <li class="hidden-phone">
                        <!-- <input class="search" type="text" />-->
                    </li>
                    <!-- <li class="dropdown">
                                         <a href="<?/*=$vObj->getURL('home'); */?>" class="dropdown-toggle hidden-phone" data-toggle="dropdown">Home</a>

                                     </li>-->

                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle hidden-phone" data-toggle="dropdown">
                            <span id="fn_ln">
                            <?php
                            echo $this->session->userdata('firstname')." ".$this->session->userdata('lastname');

                            #display username or email
                            /*if($this->session->userdata('user_name')==""){
                            echo $this->session->userdata('user_email');}
                            else{ echo $this->session->userdata('user_name');}*/ ?>
                            </span>
                            <b class="caret"></b>
                        </a>
                        <?php
                        if($this->session->userdata('role_id')==TYROE_FREE_ROLE){
                        ?>
                        <ul class="dropdown-menu">
                            <li><a href="<?= $vObj->getURL('account') ?>">Account settings</a></li>
                            <li><a href="<?= $vObj->getURL('profile') ?>">Edit Profile</a></li>
                            <li><a href="<?= $vObj->getURL('resume') ?>">Edit Resume</a></li>
                            <li><a href="<?= $vObj->getURL('portfolio') ?>">Edit Portfolio</a></li>
                        </ul>
                            <?php
                        }
                            ?>
                    </li>
                    <?php
                       if (isset($system_modules['top_menu'])) {

                           foreach ($system_modules['top_menu'] as $k => $v) {

                               ?>
                               <li class="settings hidden-phone">
                                   <a href="<?= $vObj->getURL($v['link']) ?>" role="button" target="_blank">
                                       <i class="<?=$v['module_img']?>"></i>

                                   </a>

                               </li>

                           <?php
                           }
                       }
                       ?>
                   <!-- <li class="settings hidden-phone">
                                            <a href="javascript:void(0);" role="button" target="_blank">
                                                <i class="icon-question-sign"></i>
                                            </a>
                                        </li>-->
                   <li class="settings hidden-phone">
                       <a href="<?= $vObj->getURL('logout') ?>" role="button">
                            <?php
                            if ($controller_name == "profile") {
                                ?>
                                <i class="icon-chevron-right"></i>
                            <?php
                            } else {
                                ?>
                                <i class="icon-share-alt"></i>
                            <?php
                            }

                            ?>

                       </a>
                   </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
    <!-- end navbar -->
<?php
}
    ?>
<?php
if ($this->session->userdata('logged_in') && $controller_name!="publicprofile") {
    ?>
    <!-- sidebar -->
    <div id="sidebar-nav">
        <button type="button" class="btn-navbar btn-toggler bth-hide" id="menu-toggler">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
        </button>
        <ul id="dashboard-menu">
           <?php
            if (isset($system_modules['side_menu'])) {
                foreach ($system_modules['side_menu'] as $k => $v) {
                    if ($controller_name == "resume" || $controller_name == "portfolio" || $controller_name == "social") {
                        $controller_name = "profile";
                    } else if($controller_name == "user"){
                        $controller_name = "user/listing";
                    } else if($controller_name == "studios"){
                        $controller_name = "studios/listing";
                    } else if($controller_name == "configuration"){
                        $controller_name = "configuration/system_variables";
                    /*}else if($controller_name == "exposure" || $controller_name == "stream"){
                        $controller_name = "exposure/feedback_rating";*/
                    } else if($controller_name == "stream"){
                        $controller_name = "activity-stream";
                    } else if($controller_name == "openingoverview"){
                        $controller_name = "jobopening";
                    } else if($controller_name == 'activity-stream'){
                        $controller_name = "feedback";
                    }

                    if($v['link'] == "jobopening"){
                        $v['link'] = "openings";
                    }
                    ?>
                    <li  <?php if ($v['link'] == $controller_name) { ?>class="active"<?php }?>>
                        <?php if ($v['link'] == $controller_name) { ?>
                            <div class="pointer">
                                <div class="arrow" id="left-arrow-pointer"></div>
                                <div class="arrow_border" id="left-arrow-pointer-back"></div>
                            </div>
                        <?php
                        }
                        ?>
                        <a href="<?= $vObj->getURL($v['link']) ?>">
                            <i class="<?php echo $v['module_img'] ?>"></i>
                            <span><?php echo $v['module_title']; ?></span>
                        </a>
                    </li>
                <?php
                }
            }
            // }
            ?>
        </ul>
    </div>
    <!-- end sidebar -->
<?php
}

if($controller_name=="publicprofile"){
?>

    <header>
        <div class="container">
            <div class="row">
                <div class="span7">
                    <!--<nav class="icons-example">
                        <ul>
                            <li><a href="#"><span class="font-icon-arrow-simple-left-circle"></span></a></li>
                            <li><a href="#"><span class="font-icon-list-2"></span></a></li>
                            <li><a href="#"><span class="font-icon-star"></span></a></li>
                            <li><a href="#"><span class="font-icon-ban-circle"></span></a></li>
                        </ul>
                    </nav>--></div>

                <div class="span5">
                    <!-- Mobile Menu -->
                    <a href="javascript:void(0);" class="contact-top-ico pull-right"><i class="icon-envelope-alt"></i> </a>
                    <a href="#menu-nav" class="menu-nav" id="mobile-nav"></a>

                    <!-- Standard Menu -->
                    <nav id="menu">
                        <ul id="menu-nav" class="sf-js-enabled sf-arrows">
                            <li class=""><a href="#intro-box">Portfolio</a></li>
                            <li class=""><a href="#latest-work">Resume</a></li>
                            <li class=""><a href="javascript:void(0);" class=""><i class="icon-envelope-alt"></i></a></li>
                            <!--<a href="javascript:void(0);" class="contact-top-ico pull-right"><i class="icon-envelope-alt"></i> </a>-->
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </header>
<?php
}
    ?>

