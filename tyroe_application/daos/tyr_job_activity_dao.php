<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_activity_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_activity(&$tyr_job_activity_obj) {
        $query = 'INSERT into tyr_job_activity(
                   performer_id, job_id, object_id, activity_data, activity_type, user_seen, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :performer_id, :job_id, :object_id, :activity_data, :activity_type, :user_seen, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':performer_id', $tyr_job_activity_obj->performer_id);
        $statement->bindParam(':job_id', $tyr_job_activity_obj->job_id);
        $statement->bindParam(':object_id', $tyr_job_activity_obj->object_id);
        $statement->bindParam(':activity_data', $tyr_job_activity_obj->activity_data);
        $statement->bindParam(':activity_type', $tyr_job_activity_obj->activity_type);
        $statement->bindParam(':user_seen', $tyr_job_activity_obj->user_seen);
        $statement->bindParam(':status_sl', $tyr_job_activity_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_job_activity_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_activity_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_activity_obj->created_updated_by);
        $statement->execute();
        $tyr_job_activity_obj->job_activity_id = $this->db_connection->lastInsertId('tyr_job_activity_job_activity_id_seq');
    }

    public function get_job_activity(&$tyr_job_activity_obj) {
        $query = 'select * from tyr_job_activity where job_activity_id = :job_activity_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_activity_id', $tyr_job_activity_obj->job_activity_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_activity_obj->job_activity_id = $row['job_activity_id'];
           $tyr_job_activity_obj->performer_id = $row['performer_id'];
           $tyr_job_activity_obj->job_id = $row['job_id'];
           $tyr_job_activity_obj->object_id = $row['object_id'];
           $tyr_job_activity_obj->activity_data = $row['activity_data'];
           $tyr_job_activity_obj->activity_type = $row['activity_type'];
           $tyr_job_activity_obj->user_seen = $row['user_seen'];
           $tyr_job_activity_obj->status_sl = $row['status_sl'];
           $tyr_job_activity_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function get_job_activity_count_by_user_id(&$tyr_job_activity_obj){
        $query = "SELECT COUNT(job_activity_id) cnt FROM tyr_job_activity WHERE activity_type = :activity_type AND user_seen = :user_seen AND performer_id = :performer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':performer_id', $tyr_job_activity_obj->performer_id);
        $statement->bindParam(':activity_type', $tyr_job_activity_obj->activity_type);
        $statement->bindParam(':user_seen', $tyr_job_activity_obj->user_seen);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $count = array();
        if (($row = $statement->fetch()) != FALSE) {
           $count = $row;
        }
        return $count;
    }
    
    public function update_job_activity_user_seen(&$tyr_job_activity_obj){
        $query = 'update tyr_job_activity set user_seen = :user_seen where object_id = :object_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_seen', $tyr_job_activity_obj->user_seen);
        $statement->bindParam(':object_id', $tyr_job_activity_obj->object_id);
        $statement->execute();
        return true;
    }
    
}