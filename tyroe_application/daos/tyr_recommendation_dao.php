<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_recommendation_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_recommendation(&$tyr_recommendation_obj) {
        $query = 'INSERT into tyr_recommendation(
                    user_id, studio_id, job_id, recommend_email, recommendation, recommend_name, recommend_company, recommend_dept, status_sl, recommend_status, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :studio_id, :job_id, :recommend_email, :recommendation, :recommend_name, :recommend_company, :recommend_dept, :status_sl, :recommend_status, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_recommendation_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_recommendation_obj->job_id);
        $statement->bindParam(':recommend_email', $tyr_recommendation_obj->recommend_email);
        $statement->bindParam(':recommendation', $tyr_recommendation_obj->recommendation);
        $statement->bindParam(':recommend_name', $tyr_recommendation_obj->recommend_name);
        $statement->bindParam(':recommend_company', $tyr_recommendation_obj->recommend_company);
        $statement->bindParam(':recommend_dept', $tyr_recommendation_obj->recommend_dept);
        $statement->bindParam(':status_sl', $tyr_recommendation_obj->status_sl);
        $statement->bindParam(':recommend_status', $tyr_recommendation_obj->recommend_status);
        $statement->bindParam(':created_timestamp', $tyr_recommendation_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_recommendation_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_recommendation_obj->created_updated_by);
        $statement->execute();
        $tyr_recommendation_obj->recommend_id = $this->db_connection->lastInsertId('tyr_recommendation_recommend_id_seq');
    }

    public function get_recommendation(&$tyr_recommendation_obj) {
        if(intval($tyr_recommendation_obj->recommend_id) == 0)return;
        $query = 'select * from tyr_recommendation where recommend_id = :recommend_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':recommend_id', $tyr_recommendation_obj->recommend_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_recommendation_obj->recommend_id = $row['recommend_id'];
           $tyr_recommendation_obj->user_id = $row['user_id'];
           $tyr_recommendation_obj->studio_id = $row['studio_id'];
           $tyr_recommendation_obj->job_id = $row['job_id'];
           $tyr_recommendation_obj->recommend_email = $row['recommend_email'];
           $tyr_recommendation_obj->recommendation = $row['recommendation'];
           $tyr_recommendation_obj->recommend_name = $row['recommend_name'];
           $tyr_recommendation_obj->recommend_company = $row['recommend_company'];
           $tyr_recommendation_obj->recommend_dept = $row['recommend_dept'];
           $tyr_recommendation_obj->status_sl = $row['status_sl'];
           $tyr_recommendation_obj->recommend_status = $row['recommend_status'];
           $tyr_recommendation_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function get_user_all_recommendation_by_status(&$tyr_recommendations_obj) {
        $query = "SELECT rec.status_sl,rec.recommend_dept,rec.recommendation,rec.recommend_name,rec.recommend_status,rec.recommend_company,rec.recommend_id,rec.recommend_email
                          FROM " . TABLE_RECOMMENDATION . " rec
                          WHERE rec.status_sl='1' AND rec.user_id= :user_id AND rec.recommend_status= :recommend_status";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendations_obj->user_id);
        $statement->bindParam(':recommend_status', $tyr_recommendations_obj->recommend_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
             $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_recommendation_by_id(&$tyr_recommendation_obj) {
        $query = 'update tyr_recommendation set recommendation = :recommendation, recommend_name = :recommend_name, recommend_status = :recommend_status where recommend_id = :recommend_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':recommendation', $tyr_recommendation_obj->recommendation);
        $statement->bindParam(':recommend_name', $tyr_recommendation_obj->recommend_name);
        $statement->bindParam(':recommend_status', $tyr_recommendation_obj->recommend_status);
        $statement->bindParam(':recommend_id', $tyr_recommendation_obj->recommend_id);
        $statement->execute();
        return true;
    }

    public function get_user_all_recommendation(&$tyr_recommendation_obj) {
        $query = 'select * from tyr_recommendation where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_recommendation_approvallist(&$tyr_recommendation_obj) {
        $query = "SELECT rec.status_sl,rec.recommend_dept,rec.recommendation,rec.recommend_name,rec.recommend_company,rec.recommend_id,rec.recommend_email
                        FROM tyr_recommendation rec
                        WHERE rec.status_sl='1' AND rec.user_id= :user_id AND rec.recommend_status = :recommend_status
                        ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->bindParam(':recommend_status', $tyr_recommendation_obj->recommend_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function check_recommendation_exist(&$tyr_recommendation_obj) {
        $query = "SELECT *  FROM tyr_recommendation  WHERE LOWER(TRIM(recommend_email)) = :recommend_email  AND user_id = :user_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->bindParam(':recommend_email', $tyr_recommendation_obj->recommend_email);
        $statement->bindParam(':status_sl', $tyr_recommendation_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_recommendation_exist_1(&$tyr_recommendation_obj) {
        $query = "SELECT *  FROM tyr_recommendation  WHERE recommend_id = :recommend_id AND recommend_status = :recommend_status AND user_id = :user_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->bindParam(':recommend_id', $tyr_recommendation_obj->recommend_id);
        $statement->bindParam(':status_sl', $tyr_recommendation_obj->status_sl);
        $statement->bindParam(':recommend_status', $tyr_recommendation_obj->recommend_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_user_all_recommendation_count(&$tyr_recommendation_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_recommendation where user_id = :user_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_recommendation_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_recommendation_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_all_data(&$tyr_recommendation_obj) {
        $query = "update tyr_recommendation set studio_id = :studio_id, job_id = :job_id, recommend_email = :recommend_email,"
                . " recommendation = :recommendation, recommend_name = :recommend_name, recommend_company = :recommend_company,"
                . " recommend_dept = :recommend_dept, status_sl = :status_sl, recommend_status = :recommend_status,"
                . " updated_at = :updated_at where recommend_id = :recommend_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_recommendation_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_recommendation_obj->job_id);
        $statement->bindParam(':recommend_email', $tyr_recommendation_obj->recommend_email);
        $statement->bindParam(':recommendation', $tyr_recommendation_obj->recommendation);
        $statement->bindParam(':recommend_name', $tyr_recommendation_obj->recommend_name);
        $statement->bindParam(':recommend_company', $tyr_recommendation_obj->recommend_company);
        $statement->bindParam(':recommend_dept', $tyr_recommendation_obj->recommend_dept);
        $statement->bindParam(':status_sl', $tyr_recommendation_obj->status_sl);
        $statement->bindParam(':recommend_status', $tyr_recommendation_obj->recommend_status);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':recommend_id', $tyr_recommendation_obj->recommend_id);
        $statement->execute();
        return true;
    }
    
     
    
}