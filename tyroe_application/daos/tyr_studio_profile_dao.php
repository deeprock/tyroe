<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_studio_profile_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_studio_profile(&$tyr_studio_profile_obj) {
        $query = 'INSERT into tyr_studio_profile(
                    user_id, studio_name, studio_obejctive, studio_description, category_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :studio_name, :studio_obejctive, :studio_description, :category_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_studio_profile_obj->user_id);
        $statement->bindParam(':studio_name', $tyr_studio_profile_obj->studio_name);
        $statement->bindParam(':studio_obejctive', $tyr_studio_profile_obj->studio_obejctive);
        $statement->bindParam(':studio_description', $tyr_studio_profile_obj->studio_description);
        $statement->bindParam(':category_id', $tyr_studio_profile_obj->category_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_studio_profile_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_studio_profile_obj->created_updated_by);
        $statement->execute();
        $tyr_studio_profile_obj->id = $this->db_connection->lastInsertId('tyr_studio_profile_id_seq');
    }

    public function get_studio_profile(&$tyr_studio_profile_obj) {
        $query = 'select * from tyr_studio_profile where studio_profile_id = :studio_profile_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_profile_id', $tyr_studio_profile_obj->studio_profile_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_studio_profile_obj->id = $row['id'];
           $tyr_studio_profile_obj->user_id = $row['user_id'];
           $tyr_studio_profile_obj->studio_name = $row['studio_name'];
           $tyr_studio_profile_obj->studio_obejctive = $row['studio_obejctive'];
           $tyr_studio_profile_obj->studio_description = $row['studio_description'];
           $tyr_studio_profile_obj->category_id = $row['category_id'];
        }
    }
    
}