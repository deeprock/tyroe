<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_user_sites_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_user_sites(&$tyr_user_sites_obj) {
        $query = 'INSERT into tyr_user_sites(
                    user_id, other_site_id, site_name, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :other_site_id, :site_name, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_sites_obj->user_id);
        $statement->bindParam(':other_site_id', $tyr_user_sites_obj->other_site_id);
        $statement->bindParam(':site_name', $tyr_user_sites_obj->site_name);
        $statement->bindParam(':status_sl', $tyr_user_sites_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_user_sites_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_user_sites_obj->created_updated_by);
        $statement->execute();
        $tyr_user_sites_obj->id = $this->db_connection->lastInsertId('tyr_user_sites_id_seq');
    }

    public function get_user_sites(&$tyr_user_sites_obj) {
        $query = 'select * from tyr_user_sites where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_user_sites_obj->id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_sites_obj->id = $row['id'];
           $tyr_user_sites_obj->user_id = $row['user_id'];
           $tyr_user_sites_obj->other_site_id = $row['other_site_id'];
           $tyr_user_sites_obj->site_name = $row['site_name'];
           $tyr_user_sites_obj->status_sl = $row['status_sl'];
        }
    }
    

    public function get_user_sites_by_user_id(&$tyr_user_sites_obj){
        $query = 'select * from tyr_user_sites where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', intval($tyr_user_sites_obj->user_id));
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        while (($row = $statement->fetch()) != FALSE) {
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_user_sites(&$tyr_user_sites_obj) {
        $return_array = array();
        if($tyr_user_sites_obj->user_id == '')
            return $return_array;
        $query = 'select * from tyr_user_sites where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_sites_obj->user_id);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function delete_user_sites(&$tyr_user_sites_obj) {
        $query = 'delete from tyr_user_sites where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_sites_obj->user_id);
        $statement->execute();
    }
}
?>