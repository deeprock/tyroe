<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_recommendemail_history_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_recommendemail_history(&$tyr_recommendemail_history_obj) {
        $query = 'INSERT into tyr_recommendemail_history(
                    recommend_id, message, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                    :recommend_id, :message, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':recommend_id', $tyr_recommendemail_history_obj->recommend_id);
        $statement->bindParam(':message', $tyr_recommendemail_history_obj->message);
        $statement->bindParam(':created_timestamp', $tyr_recommendemail_history_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_recommendemail_history_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_recommendemail_history_obj->created_updated_by);
        $statement->execute();
        $tyr_recommendemail_history_obj->recommend_history_id = $this->db_connection->lastInsertId('tyr_recommendemail_history_recommend_history_id_seq');
    }

    public function get_recommendemail_history(&$tyr_recommendemail_history_obj) {
        $query = 'select * from tyr_recommendemail_history where recommend_history_id = :recommend_history_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':recommend_history_id', $tyr_recommendemail_history_obj->recommend_history_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_recommendemail_history_obj->recommend_history_id = $row['recommend_history_id'];
           $tyr_recommendemail_history_obj->recommend_id = $row['recommend_id'];
           $tyr_recommendemail_history_obj->message = $row['message'];
           $tyr_recommendemail_history_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
}