<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_refrences_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_refrences(&$tyr_refrences_obj) {
        $query = 'INSERT into tyr_refrences(
                    user_id, refrence_detail, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :refrence_detail, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_refrences_obj->user_id);
        $statement->bindParam(':refrence_detail', $tyr_refrences_obj->refrence_detail);
        $statement->bindParam(':status_sl', $tyr_refrences_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_refrences_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_refrences_obj->created_updated_by);
        $statement->execute();
        $tyr_refrences_obj->refrence_id = $this->db_connection->lastInsertId('tyr_recommendemail_history_recommend_history_id_seq');
    }

    public function get_refrences(&$tyr_refrences_obj) {
        $query = 'select * from tyr_refrences where refrence_id = :refrence_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':refrence_id', $tyr_refrences_obj->refrence_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_refrences_obj->refrence_id = $row['refrence_id'];
           $tyr_refrences_obj->user_id = $row['user_id'];
           $tyr_refrences_obj->refrence_detail = $row['refrence_detail'];
           $tyr_refrences_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_user_reference(&$tyr_refrences_obj) {
        $query = 'select * from tyr_refrences where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_refrences_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_refrences_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $temp = array();
           $temp['refrence_id'] = $row['refrence_id'];
           $temp['user_id'] = $row['user_id'];
           $temp['refrence_detail'] = $row['refrence_detail'];
           $temp['status_sl'] = $row['status_sl'];
           $temp['created_at'] = $row['created_at'];
           $temp['updated_at'] = $row['updated_at'];
           $return_array[] = $temp;
        }
        return $return_array;
        
    }
    
    public function update_refrences(&$tyr_refrences_obj) {
        $query = 'update tyr_refrences set 
                    user_id = :user_id, refrence_detail = :refrence_detail, status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by
                  where refrence_id = :refrence_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_refrences_obj->user_id);
        $statement->bindParam(':refrence_detail', $tyr_refrences_obj->refrence_detail);
        $statement->bindParam(':status_sl', $tyr_refrences_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_refrences_obj->created_updated_by);
        $statement->bindParam(':refrence_id', $tyr_refrences_obj->refrence_id);
        $statement->execute();
        return true;
    }
    
}