<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_education_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_education(&$tyr_education_obj) {
        $query = 'INSERT into tyr_education(
                   user_id, edu_organization, edu_url, edu_position, edu_country, edu_city, institute_name, degree_title, field_interest, grade_obtain, start_year, end_year, activities, education_description, status_sl, created, modified, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :edu_organization, :edu_url, :edu_position, :edu_country, :edu_city, :institute_name, :degree_title, :field_interest, :grade_obtain, :start_year, :end_year, :activities, :education_description, :status_sl, :created, :modified, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':edu_organization', $tyr_education_obj->edu_organization);
        $statement->bindParam(':edu_url', $tyr_education_obj->edu_url);
        $statement->bindParam(':edu_position', $tyr_education_obj->edu_position);
        $statement->bindParam(':edu_country', $tyr_education_obj->edu_country);
        $statement->bindParam(':edu_city', $tyr_education_obj->edu_city);
        $statement->bindParam(':institute_name', $tyr_education_obj->institute_name);
        $statement->bindParam(':degree_title', $tyr_education_obj->degree_title);
        $statement->bindParam(':field_interest', $tyr_education_obj->field_interest);
        $statement->bindParam(':grade_obtain', $tyr_education_obj->grade_obtain);
        $statement->bindParam(':start_year', $tyr_education_obj->start_year);
        $statement->bindParam(':end_year', $tyr_education_obj->end_year);
        $statement->bindParam(':activities', $tyr_education_obj->activities);
        $statement->bindParam(':education_description', $tyr_education_obj->education_description);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->bindParam(':created', $tyr_education_obj->created);
        $statement->bindParam(':modified', $tyr_education_obj->modified);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_education_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_education_obj->created_updated_by);
        $statement->execute();
        $tyr_education_obj->education_id = $this->db_connection->lastInsertId('tyr_education_education_id_seq');
    }

    public function get_education(&$tyr_education_obj) {
        $query = 'select * from tyr_education where education_id = :education_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':education_id', $tyr_education_obj->education_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_education_obj->education_id = $row['education_id'];
           $tyr_education_obj->user_id = $row['user_id'];
           $tyr_education_obj->edu_organization = $row['edu_organization'];
           $tyr_education_obj->edu_url = $row['edu_url'];
           $tyr_education_obj->edu_position = $row['edu_position'];
           $tyr_education_obj->edu_country = $row['edu_country'];
           $tyr_education_obj->edu_city = $row['edu_city'];
           $tyr_education_obj->institute_name = $row['institute_name'];
           $tyr_education_obj->degree_title = $row['degree_title'];
           $tyr_education_obj->field_interest = $row['field_interest'];
           $tyr_education_obj->grade_obtain = $row['grade_obtain'];
           $tyr_education_obj->start_year = $row['start_year'];
           $tyr_education_obj->end_year = $row['end_year'];
           $tyr_education_obj->activities = $row['activities'];
           $tyr_education_obj->education_description = $row['education_description'];
           $tyr_education_obj->status_sl = $row['status_sl'];
           $tyr_education_obj->created = $row['created'];
           $tyr_education_obj->modified = $row['modified'];
        }
    }
    
    public function get_user_education(&$tyr_education_obj) {
        $query = 'select * from tyr_education where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    
    public function get_user_all_education(&$tyr_education_obj) {
        $query = "SELECT education_id, user_id,institute_name,edu_organization, edu_position, degree_title,field_interest,grade_obtain,
            (case when start_year = 0 then extract(YEAR from CURRENT_TIMESTAMP) else start_year end) as start_year,
         (case when end_year = 0 then extract(YEAR from CURRENT_TIMESTAMP) else end_year end) as end_year,activities,education_description,created,modified,status_sl
        FROM tyr_education WHERE user_id = :user_id and status_sl= :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    

    public function get_all_user_education_for_profile(&$tyr_education_obj){
        $query = "SELECT education_id, user_id,institute_name,edu_organization,edu_position ,degree_title,field_interest,grade_obtain,case when start_year = 0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else start_year end as start_year,
           case when end_year = 0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else end_year end as end_year,activities,education_description,created,modified,status_sl
          FROM " . TABLE_EDUCATION . " WHERE user_id= :user_id and status_sl= :status_sl ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while (($row = $statement->fetch()) != FALSE) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function update_education(&$tyr_education_obj) {
        $query = 'update tyr_education
                   set user_id = :user_id, edu_organization = :edu_organization, edu_url = :edu_url, edu_position = :edu_position, edu_country = :edu_country, edu_city = :edu_city, institute_name = :institute_name, degree_title = :degree_title, field_interest = :field_interest, grade_obtain = :grade_obtain, start_year = :start_year, end_year = :end_year, activities = :activities, education_description = :education_description, status_sl = :status_sl,  modified = :modified, updated_at = :updated_at, updated_by = :updated_by
                  where education_id = :education_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':edu_organization', $tyr_education_obj->edu_organization);
        $statement->bindParam(':edu_url', $tyr_education_obj->edu_url);
        $statement->bindParam(':edu_position', $tyr_education_obj->edu_position);
        $statement->bindParam(':edu_country', $tyr_education_obj->edu_country);
        $statement->bindParam(':edu_city', $tyr_education_obj->edu_city);
        $statement->bindParam(':institute_name', $tyr_education_obj->institute_name);
        $statement->bindParam(':degree_title', $tyr_education_obj->degree_title);
        $statement->bindParam(':field_interest', $tyr_education_obj->field_interest);
        $statement->bindParam(':grade_obtain', $tyr_education_obj->grade_obtain);
        $statement->bindParam(':start_year', $tyr_education_obj->start_year);
        $statement->bindParam(':end_year', $tyr_education_obj->end_year);
        $statement->bindParam(':activities', $tyr_education_obj->activities);
        $statement->bindParam(':education_description', $tyr_education_obj->education_description);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_education_obj->user_id);
        $statement->bindParam(':modified', $tyr_education_obj->modified);
        $statement->bindParam(':education_id', $tyr_education_obj->education_id);
        $statement->execute();
        return true;
    }
    
    public function get_user_education_count(&$tyr_education_obj) {
        $query = 'select count(*) as cnt from tyr_education where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_education_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_education_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != false) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}