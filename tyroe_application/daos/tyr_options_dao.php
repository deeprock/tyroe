<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_options_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_options(&$tyr_options_obj) {
        $query = 'INSERT into tyr_options(
                    option_type, option_name, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :option_type, option_name, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':option_type', $tyr_options_obj->option_type);
        $statement->bindParam(':option_name', $tyr_options_obj->option_name);
        $statement->bindParam(':status_sl', $tyr_options_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_options_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_options_obj->created_updated_by);
        $statement->execute();
        $tyr_options_obj->option_id = $this->db_connection->lastInsertId('tyr_options_option_id_seq');
    }

    public function get_options(&$tyr_options_obj) {
        $query = 'select * from tyr_options where option_id = :option_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':option_id', $tyr_options_obj->option_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_options_obj->option_id = $row['option_id'];
           $tyr_options_obj->option_type = $row['option_type'];
           $tyr_options_obj->option_name = $row['option_name'];
           $tyr_options_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_options_by_option_type(&$tyr_options_obj) {
        $query = 'select * from tyr_options where option_type = :option_type';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':option_type', $tyr_options_obj->option_type);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $temp_arr = array();
           $temp_arr['option_id'] = $row['option_id'];
           $temp_arr['option_type'] = $row['option_type'];
           $temp_arr['option_name'] = $row['option_name'];
           $temp_arr['status_sl'] = $row['status_sl'];
           $return_array[] = $temp_arr;
        }
        return $return_array;
    }
    
}