<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_other_site_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_other_site(&$tyr_other_site_obj) {
        $query = 'INSERT into tyr_other_site(
                    other_site, created_at, created_by, updated_at, updated_by
                  ) values(
                    :other_site, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':other_site', $tyr_other_site_obj->other_site);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_other_site_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_other_site_obj->created_updated_by);
        $statement->execute();
        $tyr_other_site_obj->other_site_id = $this->db_connection->lastInsertId('tyr_other_site_other_site_id_seq');
    }

    public function get_other_site(&$tyr_other_site_obj) {
        $query = 'select * from tyr_other_site where other_site_id = :other_site_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':other_site_id', $tyr_other_site_obj->other_site_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_other_site_obj->other_site_id = $row['other_site_id'];
           $tyr_other_site_obj->other_site = $row['other_site'];
        }
    }
    
    public function get_all_other_site() {
        $return_array = '';
        $query = 'select * from tyr_other_site';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}