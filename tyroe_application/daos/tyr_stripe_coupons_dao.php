<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

class Tyr_stripe_coupons_dao extends Abstract_DAO{
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
    
    public function save(&$coupon_obj){
        $query = 'insert into tyr_stripe_coupons(coupon_id, cash_discount, per_discount, max_use, expire_by, created_at)'
                . ' values(:coupon_id, :cash_discount, :per_discount, :max_use, :expire_by, :created_at)';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $coupon_obj->coupon_id);
        $statement->bindParam(':cash_discount', $coupon_obj->cash_discount);
        $statement->bindParam(':per_discount', $coupon_obj->per_discount);
        $statement->bindParam(':max_use', $coupon_obj->max_use);
        $statement->bindParam(':expire_by', $coupon_obj->expire_by);
        $statement->bindParam(':created_at', $coupon_obj->coupon_created_at);
        $statement->execute();
        $coupon_obj->id = $this->db_connection->lastInsertId('tyr_stripe_coupons_id_seq');
    }
    
    public function get(&$coupon_obj){
        $query = 'select * from tyr_stripe_coupons where coupon_id = :coupon_id and created_at = :created_at and status = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $coupon_obj->coupon_id);
        $statement->bindParam(':created_at', $coupon_obj->coupon_created_at);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $coupon_obj->id = $row['id'];
            $coupon_obj->cash_discount = $row['cash_discount'];
            $coupon_obj->per_discount = $row['per_discount'];
            $coupon_obj->max_use = $row['max_use'];
            $coupon_obj->expire_by = $row['expire_by'];
        }
    }
    
    public function get_by_coupon_id(&$coupon_obj){
        $query = 'select * from tyr_stripe_coupons where coupon_id = :coupon_id and status = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $coupon_obj->coupon_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $coupon_obj->id = $row['id'];
            $coupon_obj->cash_discount = $row['cash_discount'];
            $coupon_obj->per_discount = $row['per_discount'];
            $coupon_obj->max_use = $row['max_use'];
            $coupon_obj->expire_by = $row['expire_by'];
        }
    }
    
    public function update(&$coupon_obj){
        $query = 'update tyr_stripe_coupons '
                . 'set max_use = :max_use '
                . 'where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $coupon_obj->id);
        $statement->bindParam(':max_use', $coupon_obj->max_use);
        $statement->execute();
    }
    
    public function delete(&$coupon_obj){
        $query = 'update tyr_stripe_coupons '
                . 'set status = :status '
                . 'where coupon_id = :coupon_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $coupon_obj->coupon_id);
        $statement->bindParam(':status', $coupon_obj->status);
        $statement->execute();
    }
}
