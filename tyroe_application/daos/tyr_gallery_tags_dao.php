<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_gallery_tags_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_gallery_tags(&$tyr_gallery_tags_obj) {
        $query = 'INSERT into tyr_gallery_tags(
                   tag_name, tag_add_by, tag_add_time, tag_status, created_at, created_by, updated_at, updated_by
                  ) values(
                   :tag_name, :tag_add_by, :tag_add_time, :tag_status, created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tag_name', $tyr_gallery_tags_obj->tag_name);
        $statement->bindParam(':tag_add_by', $tyr_gallery_tags_obj->tag_add_by);
        $statement->bindParam(':tag_add_time', $tyr_gallery_tags_obj->tag_add_time);
        $statement->bindParam(':tag_status', $tyr_gallery_tags_obj->tag_status);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_gallery_tags_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_gallery_tags_obj->created_updated_by);
        $statement->execute();
        $tyr_gallery_tags_obj->tag_id = $this->db_connection->lastInsertId('tyr_gallery_tags_tag_id_seq');
    }

    public function get_gallery_tags(&$tyr_gallery_tags_obj) {
        $query = 'select * from tyr_gallery_tags where tag_id = :tag_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tag_id', $tyr_gallery_tags_obj->tag_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_gallery_tags_obj->tag_id = $row['tag_id'];
           $tyr_gallery_tags_obj->tag_name = $row['tag_name'];
           $tyr_gallery_tags_obj->tag_add_by = $row['tag_add_by'];
           $tyr_gallery_tags_obj->tag_add_time = $row['tag_add_time'];
           $tyr_gallery_tags_obj->tag_status = $row['tag_status'];
        }
    }
    
    public function check_tag_exist(&$tyr_gallery_tags_obj) {
        $query = 'select * from tyr_gallery_tags where tag_name = :tag_name';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tag_name', $tyr_gallery_tags_obj->tag_name);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array(); 
           $temp_array['tag_id'] = $row['tag_id'];
           $temp_array['tag_name'] = $row['tag_name'];
           $temp_array['tag_add_by'] = $row['tag_add_by'];
           $temp_array['tag_add_time'] = $row['tag_add_time'];
           $temp_array['tag_status'] = $row['tag_status'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
}