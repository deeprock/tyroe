<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_coupons_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_coupons(&$tyr_coupons_obj) {
        $query = 'INSERT into tyr_coupons(
                   tyroe_id, coupon_label, coupon_value, coupon_type, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :tyroe_id, :coupon_label, :coupon_value, :coupon_type, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_coupons_obj->tyroe_id);
        $statement->bindParam(':coupon_label', $tyr_coupons_obj->coupon_label);
        $statement->bindParam(':coupon_value', $tyr_coupons_obj->coupon_value);
        $statement->bindParam(':coupon_type', $tyr_coupons_obj->coupon_type);
        $statement->bindParam(':status_sl', $tyr_coupons_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_coupons_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_coupons_obj->created_updated_by);
        $statement->execute();
        $tyr_coupons_obj->coupon_id = $this->db_connection->lastInsertId('tyr_coupons_coupon_id_seq');
    }

    public function get_coupons(&$tyr_coupons_obj) {
        $query = 'select * from tyr_coupons where coupon_id = :coupon_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $tyr_coupons_obj->coupon_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_coupons_obj->coupon_id = $row['coupon_id'];
           $tyr_coupons_obj->tyroe_id = $row['tyroe_id'];
           $tyr_coupons_obj->coupon_label = $row['coupon_label'];
           $tyr_coupons_obj->coupon_value = $row['coupon_value'];
           $tyr_coupons_obj->coupon_type = $row['coupon_type'];
           $tyr_coupons_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_coupons_by_coupons_type_and_tyroe(&$tyr_coupons_obj) {
        $query = 'select * from tyr_coupons where coupon_type = :coupon_type and tyroe_id = :tyroe_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_type', $tyr_coupons_obj->coupon_type);
        $statement->bindParam(':tyroe_id', $tyr_coupons_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_coupons_obj->coupon_id = $row['coupon_id'];
           $tyr_coupons_obj->tyroe_id = $row['tyroe_id'];
           $tyr_coupons_obj->coupon_label = $row['coupon_label'];
           $tyr_coupons_obj->coupon_value = $row['coupon_value'];
           $tyr_coupons_obj->coupon_type = $row['coupon_type'];
           $tyr_coupons_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_coupons_by_coupons_type_and_tyroe_zero(&$tyr_coupons_obj) {
        $query = 'select * from tyr_coupons where coupon_type = :coupon_type and tyroe_id = :tyroe_id ORDER BY tyroe_id DESC LIMIT 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_type', $tyr_coupons_obj->coupon_type);
        $statement->bindParam(':tyroe_id', $tyr_coupons_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_coupons_obj->coupon_id = $row['coupon_id'];
           $tyr_coupons_obj->tyroe_id = $row['tyroe_id'];
           $tyr_coupons_obj->coupon_label = $row['coupon_label'];
           $tyr_coupons_obj->coupon_value = $row['coupon_value'];
           $tyr_coupons_obj->coupon_type = $row['coupon_type'];
           $tyr_coupons_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_coupons_by_coupons_type(&$tyr_coupons_obj) {
        $query = 'select * from tyr_coupons where coupon_type = :coupon_type';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_type', $tyr_coupons_obj->coupon_type);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_coupons_obj->coupon_id = $row['coupon_id'];
           $tyr_coupons_obj->tyroe_id = $row['tyroe_id'];
           $tyr_coupons_obj->coupon_label = $row['coupon_label'];
           $tyr_coupons_obj->coupon_value = $row['coupon_value'];
           $tyr_coupons_obj->coupon_type = $row['coupon_type'];
           $tyr_coupons_obj->status_sl = $row['status_sl'];
        }
    }
    
}