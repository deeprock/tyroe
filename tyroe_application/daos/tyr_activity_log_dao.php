<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_activity_log_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_activity_log(&$tyr_activity_log_obj) {
        $query = 'INSERT into tyr_activity_log(
                   data, user_id, table_name, ip_address, activity_status, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :data, :user_id, :table_name, :ip_address, :activity_status, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':data', $tyr_activity_log_obj->data);
        $statement->bindParam(':user_id', $tyr_activity_log_obj->user_id);
        $statement->bindParam(':table_name', $tyr_activity_log_obj->table_name);
        $statement->bindParam(':ip_address', $tyr_activity_log_obj->ip_address);
        $statement->bindParam(':activity_status', $tyr_activity_log_obj->activity_status);
        $statement->bindParam(':created_timestamp', $tyr_activity_log_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_activity_log_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_activity_log_obj->created_updated_by);
        $statement->execute();
        $tyr_activity_log_obj->activity_id = $this->db_connection->lastInsertId('tyr_activity_log_activity_id_seq');
    }

    public function get_activity_log(&$tyr_activity_log_obj) {
        $query = 'select * from tyr_activity_log where activity_id = :activity_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':activity_id', $tyr_activity_log_obj->activity_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_activity_log_obj->activity_id = $row['activity_id'];
           $tyr_activity_log_obj->data = $row['data'];
           $tyr_activity_log_obj->user_id = $row['user_id'];
           $tyr_activity_log_obj->table_name = $row['table_name'];
           $tyr_activity_log_obj->ip_address = $row['ip_address'];
           $tyr_activity_log_obj->activity_status = $row['activity_status'];
           $tyr_activity_log_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
}