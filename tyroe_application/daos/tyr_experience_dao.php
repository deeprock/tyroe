<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_experience_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_experience(&$tyr_experience_obj) {
        $query = 'INSERT into tyr_experience(
                   user_id, company_name, job_title, job_location, job_start, job_end, job_description, job_url, job_country, job_city, current_job, status_sl, created, modified, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :company_name, :job_title, :job_location, :job_start, :job_end, :job_description, :job_url, :job_country, :job_city, :current_job, :status_sl, :created, :modified, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':company_name', $tyr_experience_obj->company_name);
        $statement->bindParam(':job_title', $tyr_experience_obj->job_title);
        $statement->bindParam(':job_location', $tyr_experience_obj->job_location);
        $statement->bindParam(':job_start', $tyr_experience_obj->job_start);
        $statement->bindParam(':job_end', $tyr_experience_obj->job_end);
        $statement->bindParam(':job_description', $tyr_experience_obj->job_description);
        $statement->bindParam(':job_url', $tyr_experience_obj->job_url);
        $statement->bindParam(':job_country', $tyr_experience_obj->job_country);
        $statement->bindParam(':job_city', $tyr_experience_obj->job_city);
        $statement->bindParam(':current_job', $tyr_experience_obj->current_job);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->bindParam(':created', $tyr_experience_obj->created);
        $statement->bindParam(':modified', $tyr_experience_obj->modified);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_experience_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_experience_obj->created_updated_by);
        $statement->execute();
        $tyr_experience_obj->exp_id = $this->db_connection->lastInsertId('tyr_experience_exp_id_seq');
    }

    public function get_experience(&$tyr_experience_obj) {
        $query = 'select * from tyr_experience where exp_id = :exp_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':exp_id', $tyr_experience_obj->exp_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_experience_obj->exp_id = $row['exp_id'];
           $tyr_experience_obj->user_id = $row['user_id'];
           $tyr_experience_obj->company_name = $row['company_name'];
           $tyr_experience_obj->job_title = $row['job_title'];
           $tyr_experience_obj->job_location = $row['job_location'];
           $tyr_experience_obj->job_start = $row['job_start'];
           $tyr_experience_obj->job_end = $row['job_end'];
           $tyr_experience_obj->job_description = $row['job_description'];
           $tyr_experience_obj->job_url = $row['job_url'];
           $tyr_experience_obj->job_country = $row['job_country'];
           $tyr_experience_obj->job_city = $row['job_city'];
           $tyr_experience_obj->current_job = $row['current_job'];
           $tyr_experience_obj->status_sl = $row['status_sl'];
           $tyr_experience_obj->created = $row['created'];
        }
    }
    
    public function get_user_experience(&$tyr_experience_obj) {
        $query = 'select * from tyr_experience where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_user_experience_cnt(&$tyr_experience_obj) {
        $query = 'select COUNT(*) as cnt from tyr_experience where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_exp_by_user_id_and_status(&$tyr_experience_entity){
        $query = "SELECT exp_id, user_id, company_name,job_title,job_location,case when job_start=0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else job_start end AS job_start,
        case when job_end=0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else job_end end AS job_end, job_description, job_url, job_country, job_city,
                  current_job, created,modified,status_sl  FROM tyr_experience  WHERE user_id = :user_id and status_sl = :status_sl
                  ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_entity->user_id);
        $statement->bindParam(':status_sl', $tyr_experience_entity->status_sl);
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['exp_id'] = $row['exp_id'];
           $temp_array['user_id'] = $row['user_id'];
           $temp_array['company_name'] = $row['company_name'];
           $temp_array['job_title'] = $row['job_title'];
           $temp_array['job_location'] = $row['job_location'];
           $temp_array['job_start'] = $row['job_start'];
           $temp_array['job_end'] = $row['job_end'];
           $temp_array['job_description'] = $row['job_description'];
           $temp_array['job_url'] = $row['job_url'];
           $temp_array['job_country'] = $row['job_country'];
           $temp_array['job_city'] = $row['job_city'];
           $temp_array['current_job'] = $row['current_job'];
           $temp_array['status_sl'] = $row['status_sl'];
           $temp_array['created_at'] = $row['created_at'];
           $temp_array['updated_at'] = $row['updated_at'];
           $return_array[] = $temp_array;
        }
        var_dump($return_array);
        return $return_array;
    }
    
    public function get_user_all_experience(&$tyr_experience_obj) {
        $query = "SELECT exp_id, user_id, company_name,job_title,job_location,case when job_start=0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else job_start end AS job_start,
        case when job_end=0 then EXTRACT(YEAR FROM CURRENT_TIMESTAMP) else job_end end AS job_end, job_description, job_url, job_country, job_city,
                  current_job, created,modified,status_sl  FROM tyr_experience  WHERE user_id = :user_id and status_sl = :status_sl
                  ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_user_experience_count(&$tyr_experience_obj) {
        $query = "SELECT count(*) as cnt  FROM tyr_experience  WHERE user_id= :user_id and status_sl = :status_sl  AND current_job = :current_job ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->bindParam(':current_job', $tyr_experience_obj->current_job);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_experience(&$tyr_experience_obj) {
        $query = 'update tyr_experience
                  set user_id = :user_id, company_name = :company_name, job_title = :job_title, job_location = :job_location, job_start = :job_start, job_end = :job_end, job_description = :job_description, job_url = :job_url, job_country = :job_country, job_city = :job_city, current_job = :current_job, status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by
                  where exp_id = :exp_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':company_name', $tyr_experience_obj->company_name);
        $statement->bindParam(':job_title', $tyr_experience_obj->job_title);
        $statement->bindParam(':job_location', $tyr_experience_obj->job_location);
        $statement->bindParam(':job_start', $tyr_experience_obj->job_start);
        $statement->bindParam(':job_end', $tyr_experience_obj->job_end);
        $statement->bindParam(':job_description', $tyr_experience_obj->job_description);
        $statement->bindParam(':job_url', $tyr_experience_obj->job_url);
        $statement->bindParam(':job_country', $tyr_experience_obj->job_country);
        $statement->bindParam(':job_city', $tyr_experience_obj->job_city);
        $statement->bindParam(':current_job', $tyr_experience_obj->current_job);
        $statement->bindParam(':status_sl', $tyr_experience_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_experience_obj->user_id);
        $statement->bindParam(':exp_id', $tyr_experience_obj->exp_id);
        $statement->execute();
        return TRUE;
    }
    
    public function update_user_experience_updated_at(&$tyr_experience_obj) {
        $query = 'update tyr_experience
                  set updated_at = :updated_at, updated_by = :updated_by
                  where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_experience_obj->user_id);
        $statement->execute();
        return TRUE;
    }
    
    public function update_user_all_experience(&$tyr_experience_obj) {
        $query = 'update tyr_experience
                  set current_job = :current_job, job_end = :job_end, updated_at = :updated_at, updated_by = :updated_by
                  where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':current_job', $tyr_experience_obj->current_job);
        $statement->bindParam(':job_end', $tyr_experience_obj->job_end);
        $statement->bindParam(':user_id', $tyr_experience_obj->user_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_experience_obj->user_id);
        $statement->execute();
        return TRUE;
    }
    
}