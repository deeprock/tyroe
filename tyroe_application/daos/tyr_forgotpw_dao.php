<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_forgotpw_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_forgotpw(&$tyr_forgotpw_obj) {
        $query = 'INSERT into tyr_forgotpw(
                   user_id, email, expiry_time, status_pw, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :email, :expiry_time, :status_pw, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_forgotpw_obj->user_id);
        $statement->bindParam(':email', $tyr_forgotpw_obj->email);
        $statement->bindParam(':expiry_time', $tyr_forgotpw_obj->expiry_time);
        $statement->bindParam(':status_pw', $tyr_forgotpw_obj->status_pw);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_forgotpw_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_forgotpw_obj->created_updated_by);
        $statement->execute();
        $tyr_forgotpw_obj->id = $this->db_connection->lastInsertId('tyr_forgotpw_id_seq');
    }

    public function get_forgotpw(&$tyr_forgotpw_obj) {
        $query = 'select * from tyr_forgotpw where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_forgotpw_obj->id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_forgotpw_obj->id = $row['id'];
           $tyr_forgotpw_obj->user_id = $row['user_id'];
           $tyr_forgotpw_obj->email = $row['email'];
           $tyr_forgotpw_obj->expiry_time = $row['expiry_time'];
           $tyr_forgotpw_obj->status_pw = $row['status_pw'];
        }
    }
    
    public function get_forgot_password_user(&$tyr_forgotpw_obj) {
        $query = 'select * from tyr_forgotpw where email = :email and status_pw = :status_pw AND expiry_time >= '.time();
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_forgotpw_obj->email);
        $statement->bindParam(':status_pw', $tyr_forgotpw_obj->status_pw);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_forgotpw_obj->id = $row['id'];
           $tyr_forgotpw_obj->user_id = $row['user_id'];
           $tyr_forgotpw_obj->email = $row['email'];
           $tyr_forgotpw_obj->expiry_time = $row['expiry_time'];
           $tyr_forgotpw_obj->status_pw = $row['status_pw'];
        }
    }
    
    public function update_forgot_password_status(&$tyr_forgotpw_obj) {
        $query = 'update tyr_forgotpw set status_pw = :status_pw, updated_at = :updated_at, updated_by = :updated_by where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_pw', $tyr_forgotpw_obj->status_pw);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_forgotpw_obj->created_updated_by);
        $statement->bindParam(':user_id', $tyr_forgotpw_obj->user_id);
        $statement->execute();
        return true;
    }
    
}