<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_creative_skills_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_creative_skills(&$tyr_creative_skills_obj) {
        $query = 'INSERT into tyr_creative_skills(
                   creative_skill, created_at, created_by, updated_at, updated_by
                  ) values(
                   :creative_skill, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill', $tyr_creative_skills_obj->creative_skill);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_creative_skills_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_creative_skills_obj->created_updated_by);
        $statement->execute();
        $tyr_creative_skills_obj->creative_skill_id = $this->db_connection->lastInsertId('tyr_creative_skills_creative_skill_id_seq');
    }

    public function get_creative_skills(&$tyr_creative_skills_obj) {
        $query = "SELECT creative_skill, creative_skill_id FROM ".TABLE_CREATIVE_SKILLS." where creative_skill_id= :creative_skill_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill_id', $tyr_creative_skills_obj->creative_skill_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_creative_skills_obj->creative_skill_id = $row['creative_skill_id'];
           $tyr_creative_skills_obj->creative_skill = $row['creative_skill'];
        }
    }
    
    public function get_creative_skill(&$tyr_creative_skills_obj) {
        $query = 'select * from tyr_creative_skills where creative_skill = :creative_skill';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill', $tyr_creative_skills_obj->creative_skill);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_creative_skills_obj->creative_skill_id = $row['creative_skill_id'];
           $tyr_creative_skills_obj->creative_skill = $row['creative_skill'];
        }
    }
    
    public function get_all_creative_skills(&$tyr_creative_skills_obj) {
        $query = 'select * from tyr_creative_skills';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_creative_skill_ids(&$tyr_creative_skills_obj) {
        $query = "SELECT array_to_string(array_agg(creative_skill_id),',') AS skills_id FROM tyr_creative_skills WHERE creative_skill_id = :creative_skill_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill_id', $tyr_creative_skills_obj->creative_skill_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_relative_creative_skill_ids(&$tyr_creative_skills_obj,$keyword) {
        $query = "SELECT array_to_string(array_agg(creative_skill_id),',') AS skills_id FROM tyr_creative_skills WHERE creative_skill LIKE '%".$keyword."%'";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill', $tyr_creative_skills_obj->creative_skill);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['skills_id'] = $row['skills_id'];
        }
        return $return_array;
    }
    
    public function get_relative_creative_skill_ids_1($search_input) {
        $query = "SELECT array_to_string(array_agg(" . TABLE_CREATIVE_SKILLS . ".creative_skill_id ORDER BY " . TABLE_CREATIVE_SKILLS 
                    . ".creative_skill_id),',')AS creative_skill_id  FROM " . TABLE_CREATIVE_SKILLS . " WHERE lower(creative_skill) LIKE '%" 
                    . $search_input . "%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_skill_exist(&$tyr_creative_skills_obj) {
        $query = "SELECT *  FROM tyr_creative_skills WHERE LOWER(TRIM(creative_skill)) = :creative_skill";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':creative_skill', $tyr_creative_skills_obj->creative_skill);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_search_creative_skills(&$tyr_creative_skills_obj,$search_where_title) {
        $query = "SELECT skills.creative_skill_id AS media_tag_id, skills.creative_skill AS media_tag_name
                    FROM tyr_creative_skills as skills WHERE skills.creative_skill like ('%".$search_where_title."%') ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_creative_skills_as(&$tyr_creative_skills_obj) {
        $query = "SELECT skills.creative_skill_id AS media_tag_id, skills.creative_skill AS media_tag_name
                    FROM tyr_creative_skills as skills" ;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_creative_skills_for_search($skill_name){
        $query = 'select distinct lower(creative_skill) as text from tyr_creative_skills';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_creative_skills_for_profile($skill_name){
        $found = false;
        $query = "select distinct lower(creative_skill) as text from tyr_creative_skills where lower(creative_skill) like '%".strtolower($skill_name)."%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){
            if($row['text'] == strtolower($skill_name) && $found == false)$found = true;
            $return_array[] = $row;
        }
        
        if(!$found){
            $return_array[] = array('text'=>$skill_name);
        }
        return $return_array;
    }
    
    function in_array_r($needle, $haystack) {
    $found = false;
    foreach ($haystack as $item) {
    if ($item === $needle) { 
            $found = true; 
            break; 
        } elseif (is_array($item)) {
            $found = in_array_r($needle, $item); 
            if($found) { 
                break; 
            } 
        }    
    }
    return $found;
}
    
    public function get_user_id_for_skill_search($search_string){
        $search_string_new = '';
        $search_string = explode(',', $search_string);
        foreach($search_string as $skill){
            $search_string_new = $search_string_new . "'".$skill."'".",";
        }
        $search_string_new = rtrim($search_string_new, ",");
        $query = "select array_to_string(array_agg(distinct user_id ORDER BY user_id ASC),',') as user_id from tyr_user_skills_view where lower(creative_skill) in (".$search_string_new.")";
        //echo $query;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){
           $return_array = $row;
        }
        return $return_array;
    }
    
}