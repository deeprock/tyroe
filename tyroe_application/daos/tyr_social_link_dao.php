<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_social_link_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_social_link(&$tyr_social_link_obj) {
        $query = 'INSERT into tyr_social_link(
                    social_type, user_id, social_link, status_sl, created_timestamp, modified_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                    :social_type, :user_id, :social_link, :status_sl, :created_timestamp, :modified_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':social_type', $tyr_social_link_obj->social_type);
        $statement->bindParam(':user_id', $tyr_social_link_obj->user_id);
        $statement->bindParam(':social_link', $tyr_social_link_obj->social_link);
        $statement->bindParam(':status_sl', $tyr_social_link_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_social_link_obj->created_timestamp);
        $statement->bindParam(':modified_timestamp', $tyr_social_link_obj->modified_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_social_link_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_social_link_obj->created_updated_by);
        $statement->execute();
        $tyr_social_link_obj->social_id = $this->db_connection->lastInsertId('tyr_social_link_social_id_seq');
    }

    public function get_social_link(&$tyr_social_link_obj) {
        $query = 'select * from tyr_social_link where social_id = :social_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':social_id', $tyr_social_link_obj->social_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_social_link_obj->social_id = $row['social_id'];
           $tyr_social_link_obj->social_type = $row['social_type'];
           $tyr_social_link_obj->user_id = $row['user_id'];
           $tyr_social_link_obj->social_link = $row['social_link'];
           $tyr_social_link_obj->status_sl = $row['status_sl'];
           $tyr_social_link_obj->created_timestamp = $row['created_timestamp'];
           $tyr_social_link_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }
    
    public function get_all_social_links_by_user_id_and_status(&$tyr_social_link_obj){
        $query = "SELECT * FROM " . TABLE_SOCIAL_LINKS . " WHERE user_id= :user_id and status_sl= :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_social_link_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_social_link_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_user_all_social_link(&$tyr_social_link_obj) {
        $query = 'select * from tyr_social_link where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_social_link_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_user_social_link_count(&$tyr_social_link_obj) {
        $query = 'select count(*) as cnt from tyr_social_link where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_social_link_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_social_link_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_social_link(&$tyr_social_link_obj) {
        $query = 'update tyr_social_link set
                    social_type = :social_type,
                    user_id = :user_id,
                    social_link = :social_link,
                    status_sl = :status_sl,
                    modified_timestamp = :modified_timestamp,
                    updated_at = :updated_at
                  where social_id = :social_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':social_type', $tyr_social_link_obj->social_type);
        $statement->bindParam(':user_id', $tyr_social_link_obj->user_id);
        $statement->bindParam(':social_link', $tyr_social_link_obj->social_link);
        $statement->bindParam(':status_sl', $tyr_social_link_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':social_id', $tyr_social_link_obj->social_id);
        $statement->bindParam(':modified_timestamp', $tyr_social_link_obj->modified_timestamp);
        $statement->execute();
        return TRUE;
    }
}