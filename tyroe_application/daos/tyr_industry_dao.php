<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_industry_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_industry(&$tyr_industry_obj) {
        $query = 'INSERT into tyr_industry(
                   industry_name, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :industry_name, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':industry_name', $tyr_industry_obj->industry_name);
        $statement->bindParam(':status_sl', $tyr_industry_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_industry_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_industry_obj->created_updated_by);
        $statement->execute();
        $tyr_industry_obj->industry_id = $this->db_connection->lastInsertId('tyr_industry_industry_id_seq');
    }

    public function get_industry(&$tyr_industry_obj) {
        $query = 'select * from tyr_industry where industry_id = :industry_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':industry_id', $tyr_industry_obj->industry_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_industry_obj->industry_id = $row['industry_id'];
           $tyr_industry_obj->industry_name = $row['industry_name'];
           $tyr_industry_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_industries() {
        $query = 'select * from tyr_industry';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $temp_array = array();
           $temp_array['id'] = $row['industry_id'];
           $temp_array['industry_id'] = $row['industry_id'];
           $temp_array['industry_name'] = $row['industry_name'];
           $temp_array['name'] = $row['industry_name'];
           $temp_array['status_sl'] = $row['status_sl'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_all_industries_by_keyword($search_input) {
        $query = "SELECT array_to_string(array_agg(" . TABLE_INDUSTRY . ".industry_id ORDER BY " . TABLE_INDUSTRY . ".industry_id"
                    . " ASC), ',') AS industry_id  FROM " . TABLE_INDUSTRY . " WHERE lower(industry_name) LIKE '%" . $search_input . "%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}