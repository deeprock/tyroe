<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_favourite_job_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_favourite_job(&$tyr_favourite_job_obj) {
        $query = 'INSERT into tyr_favourite_job(
                   job_id, studio_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :job_id, :studio_id, :status_sl, created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_favourite_job_obj->job_id);
        $statement->bindParam(':studio_id', $tyr_favourite_job_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_favourite_job_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_favourite_job_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_favourite_job_obj->created_updated_by);
        $statement->execute();
        $tyr_favourite_job_obj->fav_job_id = $this->db_connection->lastInsertId('tyr_favourite_job_fav_job_id_seq');
    }

    public function get_favourite_job(&$tyr_favourite_job_obj) {
        $query = 'select * from tyr_favourite_job where fav_job_id = :fav_job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':fav_job_id', $tyr_favourite_job_obj->fav_job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_favourite_job_obj->fav_job_id = $row['fav_job_id'];
           $tyr_favourite_job_obj->job_id = $row['job_id'];
           $tyr_favourite_job_obj->studio_id = $row['studio_id'];
           $tyr_favourite_job_obj->status_sl = $row['status_sl'];
        }
    }
    
}