<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_user_cards_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save(&$tyr_card_obj) {
        $query = 'INSERT into tyr_user_cards(
                   user_id, card_no, card_id) values(
                   :user_id, :card_no, :card_id)';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_card_obj->user_id);
        $statement->bindParam(':card_no', $tyr_card_obj->card_no);
        $statement->bindParam(':card_id', $tyr_card_obj->card_id);
        $statement->execute();
        $tyr_card_obj->id = $this->db_connection->lastInsertId('tyr_user_cards_id_seq');
    }
    
    public function get(&$tyr_card_obj) {
        $query = 'select * from tyr_user_cards where card_no = :card_no and user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':card_no', $tyr_card_obj->card_no);
        $statement->bindParam(':user_id', $tyr_card_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_card_obj->card_id = $row['card_id'];
        }
    }
}