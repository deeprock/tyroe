<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_tyroe_profile_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_tyroe_profile(&$tyr_tyroe_profile_obj) {
        $query = 'INSERT into tyr_tyroe_profile(
                    user_id, tyr_about, tyr_intrest, tyr_description, tyro_objective, tyro_summary, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :tyr_about, :tyr_intrest, :tyr_description, :tyro_objective, :tyro_summary, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_tyroe_profile_obj->user_id);
        $statement->bindParam(':tyr_about', $tyr_tyroe_profile_obj->tyr_about);
        $statement->bindParam(':tyr_intrest', $tyr_tyroe_profile_obj->tyr_intrest);
        $statement->bindParam(':tyr_description', $tyr_tyroe_profile_obj->tyr_description);
        $statement->bindParam(':tyro_objective', $tyr_tyroe_profile_obj->tyro_objective);
        $statement->bindParam(':tyro_summary', $tyr_tyroe_profile_obj->tyro_summary);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_tyroe_profile_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_tyroe_profile_obj->created_updated_by);
        $statement->execute();
        $tyr_tyroe_profile_obj->id = $this->db_connection->lastInsertId('tyr_tyroe_profile_id_seq');
    }

    public function get_tyroe_profile(&$tyr_tyroe_profile_obj) {
        $query = 'select * from tyr_tyroe_profile where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_tyroe_profile_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_tyroe_profile_obj->id = $row['id'];
           $tyr_tyroe_profile_obj->user_id = $row['user_id'];
           $tyr_tyroe_profile_obj->tyr_about = $row['tyr_about'];
           $tyr_tyroe_profile_obj->tyr_intrest = $row['tyr_intrest'];
           $tyr_tyroe_profile_obj->tyr_description = $row['tyr_description'];
           $tyr_tyroe_profile_obj->tyro_objective = $row['tyro_objective'];
           $tyr_tyroe_profile_obj->tyro_summary = $row['tyro_summary'];
        }
    }
    
}