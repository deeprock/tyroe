<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_publish_notificaion_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_publish_notificaion(&$tyr_publish_notificaion_obj) {
        $query = 'INSERT into tyr_publish_notificaion(
                    user_id, option_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :option_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_publish_notificaion_obj->user_id);
        $statement->bindParam(':option_id', $tyr_publish_notificaion_obj->option_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_publish_notificaion_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_publish_notificaion_obj->created_updated_by);
        $statement->execute();
        $tyr_publish_notificaion_obj->publish_id = $this->db_connection->lastInsertId('tyr_publish_notificaion_publish_id_seq');
    }

    public function get_publish_notificaion(&$tyr_publish_notificaion_obj) {
        $query = 'select * from tyr_publish_notificaion where publish_id = :publish_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':publish_id', $tyr_publish_notificaion_obj->publish_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_publish_notificaion_obj->publish_id = $row['publish_id'];
           $tyr_publish_notificaion_obj->user_id = $row['user_id'];
           $tyr_publish_notificaion_obj->option_id = $row['option_id'];
        }
    }
    
    public function get_selected_systemcomms_by_user_id(&$tyr_publish_notificaion_obj) {
        $query = 'select * from tyr_publish_notificaion where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_publish_notificaion_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['publish_id'] = $row['publish_id'];
           $return_array['user_id'] = $row['user_id'];
           $return_array['option_id'] = $row['option_id'];
        }
        return $return_array;
    }
    
}