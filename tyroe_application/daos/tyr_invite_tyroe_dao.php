<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_invite_tyroe_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_invite_tyroe(&$tyr_invite_tyroe_obj) {
        $query = 'INSERT into tyr_invite_tyroe(
                   reviewer_id, job_id, tyroe_id, invitation_status, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :reviewer_id, :job_id, :tyroe_id, :invitation_status, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_invite_tyroe_obj->reviewer_id);
        $statement->bindParam(':job_id', $tyr_invite_tyroe_obj->job_id);
        $statement->bindParam(':tyroe_id', $tyr_invite_tyroe_obj->tyroe_id);
        $statement->bindParam(':invitation_status', $tyr_invite_tyroe_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_invite_tyroe_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_invite_tyroe_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_invite_tyroe_obj->created_updated_by);
        $statement->execute();
        $tyr_invite_tyroe_obj->invite_tyr_id = $this->db_connection->lastInsertId('tyr_invite_tyroe_invite_tyr_id_seq');
    }

    public function get_invite_tyroe(&$tyr_invite_tyroe_obj) {
        $query = 'select * from tyr_invite_tyroe where invite_tyr_id = :invite_tyr_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_tyr_id', $tyr_invite_tyroe_obj->invite_tyr_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_invite_tyroe_obj->invite_tyr_id = $row['invite_tyr_id'];
           $tyr_invite_tyroe_obj->reviewer_id = $row['reviewer_id'];
           $tyr_invite_tyroe_obj->job_id = $row['job_id'];
           $tyr_invite_tyroe_obj->tyroe_id = $row['tyroe_id'];
           $tyr_invite_tyroe_obj->invitation_status = $row['invitation_status'];
           $tyr_invite_tyroe_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_tyroe_invite_job_ids(&$tyr_invite_tyroe_obj) {
        $query = 'SELECT job_id FROM tyr_invite_tyroe WHERE tyroe_id = :tyroe_id ORDER BY job_id ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_invite_tyroe_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $temp = array();
        $return_array = '';
        $return_array['job_id'] = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp[] = $row['job_id'];
        }
      
        if (empty($temp)) {
            $return_array['job_id'] = -1;
        }else{
            $return_array['job_id'] = implode(',',$temp);
        }
        return $return_array;
    }
    
    public function get_invite_job_count_by_tyroe(&$tyr_invite_tyroe_obj) {
        $query = 'SELECT COUNT(DISTINCT job_id) AS cnt FROM tyr_invite_tyroe WHERE tyroe_id = :tyroe_id AND status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_invite_tyroe_obj->tyroe_id);
        $statement->bindParam(':status_sl', $tyr_invite_tyroe_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}