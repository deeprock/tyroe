<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_login_history_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_login_history(&$tyr_login_history_obj) {
        $query = 'INSERT into tyr_login_history(
                    user_id, ip_address, user_agent, created_at, created_by, updated_at, updated_by, created_timestamp
                  ) values(
                    :user_id, :ip_address, :user_agent, :created_at, :created_by, :updated_at, :updated_by, :created_timestamp
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_login_history_obj->user_id);
        $statement->bindParam(':ip_address', $tyr_login_history_obj->ip_address);
        $statement->bindParam(':user_agent', $tyr_login_history_obj->user_agent);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_login_history_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_login_history_obj->created_updated_by);
        $statement->bindParam(':created_timestamp',strtotime('now'));
        $statement->execute();
        $tyr_login_history_obj->history_id = $this->db_connection->lastInsertId('tyr_login_history_history_id_seq');
    }

    public function get_login_history(&$tyr_login_history_obj) {
        $query = 'select * from tyr_login_history where history_id = :history_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':history_id', $tyr_login_history_obj->history_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_login_history_obj->history_id = $row['history_id'];
           $tyr_login_history_obj->user_id = $row['user_id'];
           $tyr_login_history_obj->ip_address = $row['ip_address'];
           $tyr_login_history_obj->user_agent = $row['user_agent'];
        }
    }
    
    public function get_last_login_history_by_user_id(&$tyr_login_history_obj) {
        $query = 'SELECT * FROM tyr_login_history WHERE user_id = :user_id ORDER BY history_id DESC LIMIT 1 OFFSET 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_login_history_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_login_history_obj->history_id = $row['history_id'];
           $tyr_login_history_obj->user_id = $row['user_id'];
           $tyr_login_history_obj->ip_address = $row['ip_address'];
           $tyr_login_history_obj->user_agent = $row['user_agent'];
           $tyr_login_history_obj->created_at = $row['created_at'];
           $tyr_login_history_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    

    public function get_all_login_history_by_user_id(&$tyr_login_history_obj) {
        $query = 'SELECT created_timestamp FROM tyr_login_history WHERE user_id = :user_id ORDER BY history_id DESC LIMIT 1 OFFSET 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_login_history_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }


    public function get_login_history_count(&$tyr_login_history_obj) {
        $query = 'SELECT * FROM tyr_login_history WHERE user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_login_history_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_count_login_history_by_user_id(&$tyr_login_history_obj) {
        $query = 'SELECT count(*) as cnt FROM tyr_login_history WHERE user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_login_history_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array =  $row;
        }
        return $return_array;
    }
    
}