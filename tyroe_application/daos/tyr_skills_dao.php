<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_skills_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_skills(&$tyr_skills_obj) {
        $query = 'INSERT into tyr_skills(
                    user_id, skill, speciality, status_sl, created_timestamp, modified, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :skill, :speciality, :status_sl, :created_timestamp, :modified, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->bindParam(':skill', $tyr_skills_obj->skill);
        $statement->bindParam(':speciality', $tyr_skills_obj->speciality);
        $statement->bindParam(':status_sl', $tyr_skills_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_skills_obj->created_timestamp);
        $statement->bindParam(':modified', $tyr_skills_obj->modified);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_skills_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_skills_obj->created_updated_by);
        $statement->execute();
        $tyr_skills_obj->skill_id = $this->db_connection->lastInsertId('tyr_skills_skill_id_seq');
    }

    public function get_skills(&$tyr_skills_obj) {
        $query = 'select * from tyr_skills where skill_id = :skill_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':skill_id', $tyr_skills_obj->skill_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_skills_obj->skill_id = $row['skill_id'];
           $tyr_skills_obj->user_id = $row['user_id'];
           $tyr_skills_obj->skill = $row['skill'];
           $tyr_skills_obj->speciality = $row['speciality'];
           $tyr_skills_obj->status_sl = $row['status_sl'];
           $tyr_skills_obj->created_timestamp = $row['created_timestamp'];
           $tyr_skills_obj->modified = $row['modified'];
       }
    }
    
    
    public function get_user_skill(&$tyr_skills_obj) {
        $query = 'select * from tyr_skills where skill_id = :skill_id and user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':skill_id', $tyr_skills_obj->skill_id);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_skills_obj->skill_id = $row['skill_id'];
           $tyr_skills_obj->user_id = $row['user_id'];
           $tyr_skills_obj->skill = $row['skill'];
           $tyr_skills_obj->speciality = $row['speciality'];
           $tyr_skills_obj->status_sl = $row['status_sl'];
            $tyr_skills_obj->created_timestamp = $row['created_timestamp'];
           $tyr_skills_obj->modified = $row['modified'];
        }
    }
    
    //STAR-CHANGES
    public function get_users_for_skill_id_in($skill_ids) {
//        $query = "SELECT array_to_string(array_agg(" . TABLE_SKILLS . ".user_id  ORDER BY " . TABLE_SKILLS . ".user_id ASC),',')"
//                    . " AS tyroe_id_by_skills FROM " . TABLE_SKILLS . " WHERE " . TABLE_SKILLS . ".skill IN(" . $skill_ids['creative_skill_id'] . ")";
        $query = "SELECT array_to_string(array_agg(" . TABLE_SKILLS . ".user_id  ORDER BY " . TABLE_SKILLS . ".user_id ASC),',')"
                    . " AS tyroe_id_by_skills FROM " . TABLE_SKILLS . " WHERE " . TABLE_SKILLS . ".skill IN(" . $skill_ids['creative_skill_id'] . ")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_user_skill(&$tyr_skills_obj) {
        $query = 'update tyr_skills set
                    skill = :skill, speciality = :speciality, status_sl = :status_sl, modified = :modified, updated_at = :updated_at, updated_by = :updated_by
                  where user_id = :user_id and skill_id = :skill_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':skill', $tyr_skills_obj->skill);
        $statement->bindParam(':speciality', $tyr_skills_obj->speciality);
        $statement->bindParam(':status_sl', $tyr_skills_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_skills_obj->user_id);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->bindParam(':skill_id', $tyr_skills_obj->skill_id);
        $statement->bindParam(':modified', $tyr_skills_obj->modified);
        $statement->execute();
        return true;
    }
    
    public function update_user_all_speciality_flag(&$tyr_skills_obj) {
        $query = 'update tyr_skills set
                    speciality = :speciality, :updated_at, :updated_by
                  where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':speciality', $tyr_skills_obj->speciality);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_skills_obj->user_id);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->execute();
        return true;
    }
    
    public function update_user_skill_speciality_flag(&$tyr_skills_obj) {
        $query = 'update tyr_skills set
                    speciality = :speciality, :updated_at, :updated_by
                  where user_id = :user_id and skill_id = :skill_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':speciality', $tyr_skills_obj->speciality);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_skills_obj->user_id);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->bindParam(':skill_id', $tyr_skills_obj->skill_id);
        $statement->execute();
        return true;
    }
    
     public function delete_user_skills(&$tyr_skills_obj) {
        $query = 'delete from tyr_skills where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->execute();
    }
    
    public function check_user_skill_exist(&$tyr_skills_obj) {
        $query = "SELECT *  FROM tyr_skills  WHERE user_id = :user_id AND skill = :skill AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_skills_obj->status_sl);
        $statement->bindParam(':skill', $tyr_skills_obj->skill);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_skills(&$tyr_skills_obj) {
        $query = 'update tyr_skills set
                    user_id = :user_id, skill = :skill, speciality = :speciality, status_sl = :status_sl, modified = :modified, updated_at = :updated_at
                  where skill_id = :skill_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_skills_obj->user_id);
        $statement->bindParam(':skill', $tyr_skills_obj->skill);
        $statement->bindParam(':speciality', $tyr_skills_obj->speciality);
        $statement->bindParam(':status_sl', $tyr_skills_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_skills_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':skill_id', $this->skill_id);
        $statement->bindParam(':modified', $tyr_skills_obj->modified);
        $statement->execute();
        return true;
    }
    
}