<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_activity_type_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_activity_type(&$tyr_activity_type_obj) {
        $query = 'INSERT into tyr_activity_type(
                   activity_type_id, activity_title, sort_order, show_in_menu, whos_roled, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :activity_type_id, :activity_title, :sort_order, :show_in_menu, :whos_roled, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':activity_type_id', $tyr_activity_type_obj->activity_type_id);
        $statement->bindParam(':activity_title', $tyr_activity_type_obj->activity_title);
        $statement->bindParam(':sort_order', $tyr_activity_type_obj->sort_order);
        $statement->bindParam(':show_in_menu', $tyr_activity_type_obj->show_in_menu);
        $statement->bindParam(':whos_roled', $tyr_activity_type_obj->whos_roled);
        $statement->bindParam(':status_sl', $tyr_activity_type_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_activity_type_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_activity_type_obj->created_updated_by);
        $statement->execute();
        $tyr_activity_type_obj->id = $this->db_connection->lastInsertId('tyr_activity_type_id_seq');
    }

    public function get_activity_type(&$tyr_activity_type_obj) {
        $query = 'select * from tyr_activity_type where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_activity_type_obj->id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_activity_type_obj->id = $row['id'];
           $tyr_activity_type_obj->activity_type_id = $row['activity_type_id'];
           $tyr_activity_type_obj->activity_title = $row['activity_title'];
           $tyr_activity_type_obj->sort_order = $row['sort_order'];
           $tyr_activity_type_obj->show_in_menu = $row['show_in_menu'];
           $tyr_activity_type_obj->whos_roled = $row['whos_roled'];
           $tyr_activity_type_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_activity_type_by_role(&$tyr_activity_type_obj,$whos_roled) {
        $query = 'select * from tyr_activity_type where status_sl = :status_sl AND show_in_menu = :show_in_menu '.$whos_roled.' ORDER BY sort_order ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_activity_type_obj->status_sl);
        $statement->bindParam(':show_in_menu', $tyr_activity_type_obj->show_in_menu);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_activity_type_by_menu(&$tyr_activity_type_obj) {
        $query = 'select * from tyr_activity_type where status_sl = :status_sl AND show_in_menu = :show_in_menu ORDER BY sort_order ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_activity_type_obj->status_sl);
        $statement->bindParam(':show_in_menu', $tyr_activity_type_obj->show_in_menu);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}