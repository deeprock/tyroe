<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_notification_templates_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_notification_templates(&$tyr_notification_templates_obj) {
        $query = 'INSERT into tyr_notification_templates(
                    title, description, subject, template, variables, status_sl, social_template, notification_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :title, :description, :subject, :template, :variables, :status_sl, :social_template, :notification_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':title', $tyr_notification_templates_obj->title);
        $statement->bindParam(':description', $tyr_notification_templates_obj->description);
        $statement->bindParam(':subject', $tyr_notification_templates_obj->subject);
        $statement->bindParam(':template', $tyr_notification_templates_obj->template);
        $statement->bindParam(':variables', $tyr_notification_templates_obj->variables);
        $statement->bindParam(':status_sl', $tyr_notification_templates_obj->status_sl);
        $statement->bindParam(':social_template', $tyr_notification_templates_obj->social_template);
        $statement->bindParam(':notification_id', $tyr_notification_templates_obj->notification_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_notification_templates_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_notification_templates_obj->created_updated_by);
        $statement->execute();
        $tyr_notification_templates_obj->template_id = $this->db_connection->lastInsertId('tyr_notification_templates_template_id_seq');
    }

    public function get_notification_templates(&$tyr_notification_templates_obj) {
        $query = 'select * from tyr_notification_templates where template_id = :template_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':template_id', $tyr_notification_templates_obj->template_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_notification_templates_obj->template_id = $row['template_id'];
           $tyr_notification_templates_obj->title = $row['title'];
           $tyr_notification_templates_obj->description = $row['description'];
           $tyr_notification_templates_obj->subject = $row['subject'];
           $tyr_notification_templates_obj->template = $row['template'];
           $tyr_notification_templates_obj->variables = $row['variables'];
           $tyr_notification_templates_obj->status_sl = $row['status_sl'];
           $tyr_notification_templates_obj->social_template = $row['social_template'];
           $tyr_notification_templates_obj->notification_id = $row['notification_id'];
        }
    }
    
    public function get_notification_templates_by_notification_id(&$tyr_notification_templates_obj) {
        $query = "SELECT subject,template from " . TABLE_NOTIFICATION_TEMPLATE . " where notification_id = :notification_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':notification_id', $tyr_notification_templates_obj->notification_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_notification_templates(&$tyr_notification_templates_obj) {
        $query = 'select * from tyr_notification_templates where status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_notification_templates_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_notification_templates(&$tyr_notification_templates_obj) {
        $query = 'update tyr_notification_templates 
                  set title = :title, description = :description, subject = :subject, template = :template, variables = :variables, status_sl = :status_sl, social_template = :social_template, notification_id = :notification_id, updated_at = :updated_at
                  where template_id = :template_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':title', $tyr_notification_templates_obj->title);
        $statement->bindParam(':description', $tyr_notification_templates_obj->description);
        $statement->bindParam(':subject', $tyr_notification_templates_obj->subject);
        $statement->bindParam(':template', $tyr_notification_templates_obj->template);
        $statement->bindParam(':variables', $tyr_notification_templates_obj->variables);
        $statement->bindParam(':status_sl', $tyr_notification_templates_obj->status_sl);
        $statement->bindParam(':social_template', $tyr_notification_templates_obj->social_template);
        $statement->bindParam(':notification_id', $tyr_notification_templates_obj->notification_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':template_id', $tyr_notification_templates_obj->template_id);
        $statement->execute();
        $tyr_notification_templates_obj->template_id = $this->db_connection->lastInsertId();
        return true;
    }
    
}