<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class User_company_location_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
    }
    
    public function get_user_company_location(&$user_company_location_obj) {
        $query = 'select * from user_company_location where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_company_location_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $user_company_location_obj->user_id = $row['user_id'];
           $user_company_location_obj->parent_id = $row['parent_id'];
           $user_company_location_obj->country_id = $row['country_id'];
           $user_company_location_obj->city_id = $row['city_id'];
           $user_company_location_obj->country_city = $row['country_city'];
           $user_company_location_obj->country = $row['country'];
           $user_company_location_obj->city = $row['city'];
        }
    }
}

?>