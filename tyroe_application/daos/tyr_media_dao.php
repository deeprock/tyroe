<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_media_dao extends Abstract_DAO {

    

    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }

    public function save_media(&$tyr_media_obj) {
        $query = 'INSERT into tyr_media(
                    user_id, profile_image, video_type, media_url, media_type, media_name, media_original_name, media_title, media_tag, media_description, media_imagesection, media_crop_data, media_sortorder, status_sl, media_featured, created, modified, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :profile_image, :video_type, :media_url, :media_type, :media_name, :media_original_name, :media_title, :media_tag, :media_description, :media_imagesection, :media_crop_data, :media_sortorder, :status_sl, :media_featured, :created, :modified, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':video_type', $tyr_media_obj->video_type);
        $statement->bindParam(':media_url', $tyr_media_obj->media_url);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_name', $tyr_media_obj->media_name);
        $statement->bindParam(':media_original_name', $tyr_media_obj->media_original_name);
        $statement->bindParam(':media_title', $tyr_media_obj->media_title);
        $statement->bindParam(':media_tag', $tyr_media_obj->media_tag);
        $statement->bindParam(':media_description', $tyr_media_obj->media_description);
        $statement->bindParam(':media_imagesection', $tyr_media_obj->media_imagesection);
        $statement->bindParam(':media_crop_data', $tyr_media_obj->media_crop_data);
        $statement->bindParam(':media_sortorder', $tyr_media_obj->media_sortorder);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':created', $tyr_media_obj->created);
        $statement->bindParam(':modified', $tyr_media_obj->modified);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_media_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_media_obj->created_updated_by);
        $statement->execute();
        $tyr_media_obj->image_id = $this->db_connection->lastInsertId('tyr_media_image_id_seq');
    }

    public function get_media(&$tyr_media_obj) {
        $tyr_media_obj->image_id = intval($tyr_media_obj->image_id);
        $query = 'select * from tyr_media where image_id = :image_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':image_id', $tyr_media_obj->image_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_media_obj->image_id = $row['image_id'];
            $tyr_media_obj->user_id = $row['user_id'];
            $tyr_media_obj->profile_image = $row['profile_image'];
            $tyr_media_obj->video_type = $row['video_type'];
            $tyr_media_obj->media_url = $row['media_url'];
            $tyr_media_obj->media_type = $row['media_type'];
            $tyr_media_obj->media_name = $row['media_name'];
            $tyr_media_obj->media_original_name = $row['media_original_name'];
            $tyr_media_obj->media_title = $row['media_title'];
            $tyr_media_obj->media_tag = $row['media_tag'];
            $tyr_media_obj->media_description = $row['media_description'];
            $tyr_media_obj->media_imagesection = $row['media_imagesection'];
            $tyr_media_obj->media_crop_data = $row['media_crop_data'];
            $tyr_media_obj->media_sortorder = $row['media_sortorder'];
            $tyr_media_obj->status_sl = $row['status_sl'];
            $tyr_media_obj->created = $row['created'];
            $tyr_media_obj->modified = $row['modified'];
        }
    }
    
    public function get_media_by_user(&$tyr_media_obj) {
        $query = 'select * from tyr_media where image_id = :image_id and user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':image_id', $tyr_media_obj->image_id);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_media_obj->image_id = $row['image_id'];
           $tyr_media_obj->user_id = $row['user_id'];
           $tyr_media_obj->profile_image = $row['profile_image'];
           $tyr_media_obj->video_type = $row['video_type'];
           $tyr_media_obj->media_url = $row['media_url'];
           $tyr_media_obj->media_type = $row['media_type'];
           $tyr_media_obj->media_name = $row['media_name'];
           $tyr_media_obj->media_original_name = $row['media_original_name'];
           $tyr_media_obj->media_title = $row['media_title'];
           $tyr_media_obj->media_tag = $row['media_tag'];
           $tyr_media_obj->media_description = $row['media_description'];
           $tyr_media_obj->media_imagesection = $row['media_imagesection'];
           $tyr_media_obj->media_crop_data = $row['media_crop_data'];
           $tyr_media_obj->media_sortorder = $row['media_sortorder'];
           $tyr_media_obj->status_sl = $row['status_sl'];
           $tyr_media_obj->created = $row['created'];
           $tyr_media_obj->modified = $row['modified'];
        }
    }

    public function get_all_user_media(&$tyr_media_obj) {
        $query = 'select * from tyr_media where user_id = :user_id and status_sl = :status_sl and profile_image= 0';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_short_portfolio(&$tyr_media_obj) {
        $query = "SELECT * FROM " . TABLE_MEDIA . " WHERE user_id = :user_id AND status_sl='1'"
                    . " AND profile_image = '0' AND media_featured != '1' AND media_type = 'image' ORDER BY image_id DESC LIMIT 3 ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_progressbar_portfolio_per(&$tyr_media_obj) {
        $query = "SELECT (case when (COUNT(m.profile_image) = 1) then 2 else 0 end) + (case when COUNT(m.profile_image)=0 then 4 else 0 end) AS profile FROM tyr_media m
            WHERE m.user_id = :user_id AND m.status_sl = 1
                ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_user_media_by_type(&$tyr_media_obj) {
        $query = "SELECT * FROM tyr_media
             WHERE profile_image != :profile_image AND tyr_media.status_sl= :status_sl AND media_type = :media_type AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_user_media_by_featured(&$tyr_media_obj) {
        $query = "SELECT * FROM tyr_media
             WHERE profile_image != :profile_image AND tyr_media.status_sl= :status_sl AND media_featured = :media_featured AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function update_media_featured_by_user(&$tyr_media_obj) {
        $query = "update tyr_media set media_featured = :media_featured, updated_at = :updated_at WHERE user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }

    public function update_media(&$tyr_media_obj) {
        $tyr_media_obj->image_id = intval($tyr_media_obj->image_id);
        $query = "update tyr_media set user_id = :user_id, profile_image = :profile_image, video_type = :video_type, media_url = :media_url,"
                . " media_type = :media_type, media_name = :media_name, media_original_name = :media_original_name, media_title = :media_title,"
                . " media_tag = :media_tag, media_description = :media_description, media_imagesection = :media_imagesection,"
                . " media_crop_data = :media_crop_data, media_sortorder = :media_sortorder, status_sl = :status_sl,"
                . " media_featured = :media_featured, modified = :modified, updated_at = :updated_at WHERE image_id = :image_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':video_type', $tyr_media_obj->video_type);
        $statement->bindParam(':media_url', $tyr_media_obj->media_url);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_name', $tyr_media_obj->media_name);
        $statement->bindParam(':media_original_name', $tyr_media_obj->media_original_name);
        $statement->bindParam(':media_title', $tyr_media_obj->media_title);
        $statement->bindParam(':media_tag', $tyr_media_obj->media_tag);
        $statement->bindParam(':media_description', $tyr_media_obj->media_description);
        $statement->bindParam(':media_imagesection', $tyr_media_obj->media_imagesection);
        $statement->bindParam(':media_crop_data', $tyr_media_obj->media_crop_data);
        $statement->bindParam(':media_sortorder', $tyr_media_obj->media_sortorder);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':image_id', $tyr_media_obj->image_id);
        $statement->bindParam(':modified', $tyr_media_obj->modified);
        $statement->execute();
        return true;
    }

    public function get_all_user_images_details(&$tyr_media_obj) {
        $query = "SELECT p.* FROM tyr_media p WHERE p.user_id= :user_id AND p.profile_image = :profile_image";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_video_media_for_public_profile(&$tyr_media_obj) {
        $query = "SELECT image_id, media_name, media_title, media_description,media_tag,video_type,media_url,media_type,modified
                                                      FROM " . TABLE_MEDIA . "
                                                      WHERE profile_image!= '1' AND status_sl='1' AND (media_type = 'video' OR media_featured='1')
                                                      AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_latest_portfolio_for_public_profile_by_user(&$tyr_media_obj) {
        $query = "SELECT * FROM tyr_media WHERE  user_id = :user_id 
                AND status_sl = '1'  AND media_type = 'image' AND profile_image!='1' 
                AND media_featured='0' ORDER BY media_sortorder ASC LIMIT 20 OFFSET 0";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while (($row = $statement->fetch()) != false) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function update_status(&$tyr_media_obj) {
        $query = "update tyr_media set status_sl = :status_sl, updated_at = :updated_at WHERE image_id = :image_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':image_id', $tyr_media_obj->image_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
    }

    public function get_old_profile_pic(&$tyr_media_obj) {
        $query = "Select image_id from tyr_media WHERE user_id= :user_id and profile_image= :profile_image and status_sl=:status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_user_media_by_featured_or_type(&$tyr_media_obj) {
        $query = "SELECT * FROM tyr_media
             WHERE profile_image != :profile_image AND tyr_media.status_sl= :status_sl AND
             (media_type = :media_type OR media_featured = :media_featured) AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if ($tyr_media_obj->media_type != 'video') {
            while ($row = $statement->fetch()) {
                if (!is_array($return_array))
                    $return_array = array();
                $return_array[] = $row;
            }
        }else {
            if (($row = $statement->fetch()) != false) {
                $return_array = $row;
            }
        }
        return $return_array;
    }

    public function get_user_latest_portfolio_image(&$tyr_media_obj) {
        $query = "SELECT *
                    FROM tyr_media WHERE  user_id = :user_id
                    AND status_sl = :status_sl  AND profile_image != :profile_image AND  media_type = :media_type AND media_featured = :media_featured
                    ORDER BY media_sortorder ASC ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function update_media_status_by_user(&$tyr_media_obj) {
        $query = "update tyr_media set status_sl = -1, updated_at = :updated_at WHERE user_id = :user_id and status_sl = :status_sl and media_featured = :media_featured and modified = :modified";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':modified', $this->modified);
        $statement->execute();
        return true;
    }

    public function update_media_status_by_user_media_type(&$tyr_media_obj) {
        $query = "update tyr_media set status_sl = -1, updated_at = :updated_at WHERE user_id = :user_id and status_sl = :status_sl and media_type = :media_type and modified = :modified";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':modified', $this->modified);
        $statement->execute();
        return true;
    }

    public function check_user_video_media(&$tyr_media_obj) {
        $query = "SELECT *
                    FROM tyr_media WHERE  user_id = :user_id
                    AND status_sl = :status_sl  AND media_url != :media_url AND  media_type = :media_type AND media_featured = :media_featured";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_url', $tyr_media_obj->media_url);

        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_user_video_media_1(&$tyr_media_obj) {
        $query = "SELECT * FROM tyr_media WHERE user_id = :user_id AND status_sl = :status_sl AND media_type = :media_type";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_user_media_count(&$tyr_media_obj) {
        $query = "SELECT count(image_id) AS total_count FROM tyr_media WHERE user_id = :user_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_user_media_count_1(&$tyr_media_obj) {
        $query = "SELECT count(image_id) AS total_count FROM tyr_media WHERE user_id = :user_id AND status_sl = :status_sl AND media_type = :media_type AND media_featured = :media_featured AND profile_image = :profile_image";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function update_media_sort_order(&$tyr_media_obj) {
        $query = "update tyr_media set media_sortorder = (:media_sortorder + 1), updated_at = :updated_at, modified = :modified WHERE user_id = :user_id and status_sl = :status_sl and media_type = :media_type and profile_image = :profile_image and media_featured = :media_featured and media_imagesection = :media_imagesection";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':media_sortorder', $tyr_media_obj->media_sortorder);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':media_featured', $tyr_media_obj->media_featured);
        $statement->bindParam(':media_imagesection', $tyr_media_obj->media_imagesection);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':modified', $this->modified);
        $statement->execute();
        return true;
    }

    public function update_user_profile_image_status(&$tyr_media_obj) {
        $query = "update tyr_media set status_sl = :status_sl, updated_at = :updated_at WHERE user_id = :user_id and profile_image = :profile_image ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }

    public function update_user_profile_image_status_1(&$tyr_media_obj) {
        $query = "update tyr_media set status_sl = :status_sl, updated_at = :updated_at WHERE user_id = :user_id and profile_image = :profile_image and media_type = :media_type ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':media_type', $tyr_media_obj->media_type);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }

    public function get_user_profile_image(&$tyr_media_obj) {
        $query = "Select image_id from tyr_media where user_id = :user_id and profile_image = :profile_image and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_media_obj->status_sl);
        $statement->bindParam(':profile_image', $tyr_media_obj->profile_image);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_latest_work_for_user_public_profile($portfolioimage, $image_order_id) {
        $query = "SELECT
  IFNULL(media_original_name, media_name) AS latest_work_image_src, image_id AS latest_work_id, 
  media_imagesection AS latest_work_imagesection, media_sortorder AS latest_work_sortorder,
  media_title AS latest_work_title, media_tag AS latest_work_tag, media_description AS latest_work_description 
FROM " . TABLE_MEDIA . " WHERE media_type = 'image'
				AND status_sl = 1
				AND media_featured = 0
				AND profile_image = 0
				AND user_id =
				(SELECT
				user_id
				FROM
				" . TABLE_MEDIA . "
				WHERE image_id = :image_id)
				AND media_sortorder > :media_sortorder
				ORDER BY media_sortorder ASC
				LIMIT 6
                                        ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':image_id', $portfolioimage);
        $statement->bindParam(':media_sortorder', $image_order_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while (($row = $statement->fetch()) != false) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_media_for_load_more($portfolioimage,$left_or_right_count,$qry_inc) {
        $query = "SELECT image_id FROM ".TABLE_MEDIA." WHERE media_type = 'image'
                AND status_sl = 1
                AND media_featured = 0
                AND profile_image = 0
                AND user_id = (SELECT user_id FROM ".TABLE_MEDIA."
				WHERE image_id = ".$portfolioimage." LIMIT 1)
		AND media_sortorder > ".$left_or_right_count."
                AND media_imagesection = ".$qry_inc."
		ORDER BY media_sortorder ASC LIMIT 10";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_latest_image_for_load_more($portfolioimage,$left_or_right,$qry_inc,$new_limit) {
        $query = "SELECT case when (media_original_name=NULL || media_original_name='') then media_name else
                 media_original_name end AS latest_work_image_src, image_id 
                 AS latest_work_id, media_imagesection AS latest_work_imagesection, media_sortorder AS latest_work_sortorder, media_title 
                 AS latest_work_title, media_tag AS latest_work_tag, media_description AS latest_work_description
                 FROM" . TABLE_MEDIA . " WHERE media_type = 'image'
		 AND status_sl = 1 AND media_featured = 0 AND profile_image = 0
		 AND user_id =(SELECT user_id FROM" . TABLE_MEDIA . "WHERE image_id = " . $portfolioimage . " LIMIT 1)
		 AND media_sortorder > " . $left_or_right . " AND media_imagesection = " . $qry_inc . "
		 ORDER BY media_sortorder ASC LIMIT " . $new_limit;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_latest_image_for_load_more_new($user_id, $start) {
        $query = "SELECT case when (media_original_name='') then media_name else
                 media_original_name end AS latest_work_image_src, image_id 
                 AS latest_work_id, media_imagesection AS latest_work_imagesection, media_sortorder AS latest_work_sortorder, media_title 
                 AS latest_work_title, media_tag AS latest_work_tag, media_description AS latest_work_description
                 FROM " . TABLE_MEDIA . " WHERE media_type = 'image'
		 AND status_sl = 1 AND media_featured = 0 AND profile_image = 0
		 AND user_id = $user_id 		 
		 ORDER BY media_sortorder ASC LIMIT 10 offset ".$start;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_filled_cols_image_for_load_more($last_image_id,$last_image_order,$result_target) {
        $query = "SELECT case when (media_original_name = NULL || media_original_name='') then media_name else media_original_name end
                    AS latest_work_image_src, image_id AS latest_work_id, media_imagesection AS latest_work_imagesection, media_sortorder 
                    AS latest_work_sortorder, media_title AS latest_work_title, media_tag AS latest_work_tag, media_description 
                    AS latest_work_description FROM " . TABLE_MEDIA . " WHERE media_type = 'image' AND status_sl = 1
		    AND media_featured = 0 AND profile_image = 0 
                    AND user_id = (SELECT user_id FROM " . TABLE_MEDIA . " WHERE image_id = " . $last_image_id . " LIMIT 1)
                    AND media_sortorder > " . $last_image_order . " AND media_imagesection = " . $result_target . "
		    ORDER BY media_sortorder ASC LIMIT 10";
        var_dump($query);
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_last_upload_media_id(&$tyr_media_obj) {
        $query = "SELECT image_id FROM ".TABLE_MEDIA." WHERE user_id = :user_id ORDER BY image_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while ($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_max_sort_order(&$media_obj){
        $query = "select max(media_sortorder) as max_sort_order from ".TABLE_MEDIA."  WHERE user_id = :user_id and status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $media_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $max_sort_order = 0;
        if (($row = $statement->fetch()) != false) {
            $max_sort_order = $row['max_sort_order'];
        }
        return $max_sort_order;
    }
    
    public function remove_deleted_images(&$media_obj,$media_to_be_kept){
        $query = "update tyr_media set status_sl = -1, media_sortorder = 0 where user_id = :user_id and video_type = ''  and profile_image != 1 and media_featured != 1";
        if($media_to_be_kept != ''){
            $query = "update tyr_media set status_sl = -1, media_sortorder = 0 where image_id not in (".$media_to_be_kept.") and user_id = :user_id and video_type = ''  and profile_image != 1 and media_featured != 1";
        }
        //var_dump($query);
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $media_obj->user_id);
        $statement->execute();
    }

}
