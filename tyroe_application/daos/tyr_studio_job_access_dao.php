<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_studio_job_access_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_studio_job_access(&$tyr_studio_job_access_obj) {
        $query = 'INSERT into tyr_studio_job_access(
                    job_id, created_date, expiry_date
                  ) values(
                    :job_id, :created_date, :expiry_date
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_studio_job_access_obj->job_id);
        $statement->bindParam(':created_date', $tyr_studio_job_access_obj->created_date);
        $statement->bindParam(':expiry_date', $tyr_studio_job_access_obj->expiry_date);
        $statement->execute();
        $tyr_studio_job_access_obj->id = $this->db_connection->lastInsertId('tyr_studio_job_access_id_seq');
    }

    public function get_studio_job_access(&$tyr_studio_job_access_obj) {
        $query = 'select * from tyr_studio_job_access where job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_studio_job_access_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_studio_job_access_obj->id = $row['id'];
           $tyr_studio_job_access_obj->job_id = $row['job_id'];
           $tyr_studio_job_access_obj->created_date = $row['created_date'];
           $tyr_studio_job_access_obj->expiry_date = $row['expiry_date'];
        }
    }
    
    public function get_all_record(&$tyr_studio_job_access_obj) {
        $query = 'select * from tyr_studio_job_access';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function delete_record(&$tyr_studio_job_access_obj) {
        $query = 'delete from tyr_studio_job_access where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_studio_job_access_obj->id);
        $statement->execute();
    }
    
}