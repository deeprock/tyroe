<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_level_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_level(&$tyr_job_level_obj) {
        $query = 'INSERT into tyr_job_level(
                   level_title, created_at, created_by, updated_at, updated_by
                  ) values(
                   :level_title, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':level_title', $tyr_job_level_obj->level_title);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_level_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_level_obj->created_updated_by);
        $statement->execute();
        $tyr_job_level_obj->job_level_id = $this->db_connection->lastInsertId('tyr_job_level_job_level_id_seq');
    }

    public function get_job_level(&$tyr_job_level_obj) {
        $query = 'select * from tyr_job_level where job_level_id = :job_level_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_level_id', $tyr_job_level_obj->job_level_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_level_obj->job_level_id = $row['job_level_id'];
           $tyr_job_level_obj->level_title = $row['level_title'];
        }
    }
    
    public function get_all_job_level() {
        $query = 'select * from tyr_job_level';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['job_level_id'] = $row['job_level_id'];
           $temp_array['id'] = $row['job_level_id'];
           $temp_array['level_title'] = $row['level_title'];
           $temp_array['name'] = $row['level_title'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
}