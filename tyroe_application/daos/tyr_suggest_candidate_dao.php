<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_suggest_candidate_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_suggest_candidate(&$tyr_suggest_candidate_obj) {
        $query = 'INSERT into tyr_suggest_candidate(
                    user_id, jobs_id, studio_id, reviewer_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :jobs_id, :studio_id, :reviewer_id, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_suggest_candidate_obj->user_id);
        $statement->bindParam(':jobs_id', $tyr_suggest_candidate_obj->jobs_id);
        $statement->bindParam(':studio_id', $tyr_suggest_candidate_obj->studio_id);
        $statement->bindParam(':reviewer_id', $tyr_suggest_candidate_obj->reviewer_id);
        $statement->bindParam(':status_sl', $tyr_suggest_candidate_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_suggest_candidate_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_suggest_candidate_obj->created_updated_by);
        $statement->execute();
        $tyr_suggest_candidate_obj->suggest_id = $this->db_connection->lastInsertId('tyr_suggest_candidate_suggest_id_seq');
    }

    public function get_suggest_candidate(&$tyr_suggest_candidate_obj) {
        $query = 'select * from tyr_suggest_candidate where suggest_id = :suggest_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':suggest_id', $tyr_suggest_candidate_obj->suggest_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_suggest_candidate_obj->suggest_id = $row['suggest_id'];
           $tyr_suggest_candidate_obj->jobs_id = $row['jobs_id'];
           $tyr_suggest_candidate_obj->studio_id = $row['studio_id'];
           $tyr_suggest_candidate_obj->reviewer_id = $row['reviewer_id'];
           $tyr_suggest_candidate_obj->status_sl = $row['status_sl'];
        }
    }
    
}