<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_membership_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_membership(&$tyr_membership_obj) {
        $query = 'INSERT into tyr_membership(
                    user_id, expiration_timestamp, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :expiration_timestamp, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_membership_obj->user_id);
        $statement->bindParam(':expiration_timestamp', $tyr_membership_obj->expiration_timestamp);
        $statement->bindParam(':status_sl', $tyr_membership_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_membership_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_membership_obj->created_updated_by);
        $statement->execute();
        $tyr_membership_obj->membership_id = $this->db_connection->lastInsertId('tyr_membership_membership_id_seq');
    }

    public function get_membership(&$tyr_membership_obj) {
        $query = 'select * from tyr_membership where membership_id = :membership_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':membership_id', $tyr_membership_obj->membership_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_membership_obj->membership_id = $row['membership_id'];
           $tyr_membership_obj->user_id = $row['user_id'];
           $tyr_membership_obj->expiration_timestamp = $row['expiration_timestamp'];
           $tyr_membership_obj->status_sl = $row['status_sl'];
        }
    }
    
}