<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_favourite_profile_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_favourite_profile(&$tyr_favourite_profile_obj) {
        $query = 'INSERT into tyr_favourite_profile(
                   user_id, studio_id, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :studio_id, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_favourite_profile_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_favourite_profile_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_favourite_profile_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_favourite_profile_obj->created_updated_by);
        $statement->execute();
        $tyr_favourite_profile_obj->fav_profile_id = $this->db_connection->lastInsertId('tyr_favourite_profile_fav_profile_id_seq');
    }

    public function get_favourite_profile(&$tyr_favourite_profile_obj) {
        $query = 'select * from tyr_favourite_profile where fav_profile_id = :fav_profile_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':fav_profile_id', $tyr_favourite_profile_obj->fav_profile_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_favourite_profile_obj->fav_profile_id = $row['fav_profile_id'];
           $tyr_favourite_profile_obj->user_id = $row['user_id'];
           $tyr_favourite_profile_obj->studio_id = $row['studio_id'];
           $tyr_favourite_profile_obj->status_sl = $row['status_sl'];
           $tyr_favourite_profile_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function check_user_favourite(&$tyr_favourite_profile_obj) {
        $query = 'SELECT * FROM tyr_favourite_profile WHERE user_id = :user_id AND studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_favourite_profile_obj->fav_profile_id = $row['fav_profile_id'];
           $tyr_favourite_profile_obj->user_id = $row['user_id'];
           $tyr_favourite_profile_obj->studio_id = $row['studio_id'];
           $tyr_favourite_profile_obj->status_sl = $row['status_sl'];
           $tyr_favourite_profile_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function update_favourite_status(&$tyr_favourite_profile_obj) {
        $query = 'update tyr_favourite_profile set status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by where user_id = :user_id AND studio_id= :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_favourite_profile_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->bindParam(':updated_by', $tyr_favourite_profile_obj->studio_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return TRUE;
    }
    
    public function delete_favourite_status(&$tyr_favourite_profile_obj) {
        $query = 'delete from tyr_favourite_profile where user_id = :user_id AND studio_id= :studio_id AND status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_favourite_profile_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->execute();
    }
    
    public function check_user_already_favourite(&$tyr_favourite_profile_obj) {
        $query = 'SELECT COUNT(*) cnt FROM tyr_favourite_profile WHERE user_id = :user_id AND studio_id = :studio_id AND status_sl= :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_favourite_profile_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_favourite_profile_id(&$tyr_favourite_profile_obj) {
        $query = "SELECT fav_profile_id FROM tyr_favourite_profile WHERE user_id = :user_id AND studio_id = :studio_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_favourite_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_favourite_profile_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_favourite_profile_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}