<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_coupons_allot_users_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_coupons_allot_users(&$tyr_coupons_allot_users_obj) {
        $query = 'INSERT into tyr_coupons_allot_users(
                   coupon_id, user_id, coupon_label, coupon_value, coupon_type, coupon_status, coupon_issued, coupon_used, coupon_used_for, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :coupon_id, :user_id, :coupon_label, :coupon_value, :coupon_type, :coupon_status, :coupon_issued, :coupon_used, :coupon_used_for, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $tyr_coupons_allot_users_obj->coupon_id);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->user_id);
        $statement->bindParam(':coupon_label', $tyr_coupons_allot_users_obj->coupon_label);
        $statement->bindParam(':coupon_value', $tyr_coupons_allot_users_obj->coupon_value);
        $statement->bindParam(':coupon_type', $tyr_coupons_allot_users_obj->coupon_type);
        $statement->bindParam(':coupon_status', $tyr_coupons_allot_users_obj->coupon_status);
        $statement->bindParam(':coupon_issued', $tyr_coupons_allot_users_obj->coupon_issued);
        $statement->bindParam(':coupon_used', $tyr_coupons_allot_users_obj->coupon_used);
        $statement->bindParam(':coupon_used_for', $tyr_coupons_allot_users_obj->coupon_used_for);
        $statement->bindParam(':status_sl', $tyr_coupons_allot_users_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_coupons_allot_users_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_coupons_allot_users_obj->created_updated_by);
        $statement->execute();
        $tyr_coupons_allot_users_obj->allot_id = $this->db_connection->lastInsertId('tyr_coupons_allot_users_allot_id_seq');
    }

    public function get_coupons_allot_users(&$tyr_coupons_allot_users_obj) {
        $query = 'select * from tyr_coupons_allot_users where allot_id = :allot_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':allot_id', $tyr_coupons_allot_users_obj->allot_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_coupons_allot_users_obj->allot_id = $row['allot_id'];
           $tyr_coupons_allot_users_obj->coupon_id = $row['coupon_id'];
           $tyr_coupons_allot_users_obj->user_id = $row['user_id'];
           $tyr_coupons_allot_users_obj->coupon_label = $row['coupon_label'];
           $tyr_coupons_allot_users_obj->coupon_value = $row['coupon_value'];
           $tyr_coupons_allot_users_obj->coupon_type = $row['coupon_type'];
           $tyr_coupons_allot_users_obj->coupon_status = $row['coupon_status'];
           $tyr_coupons_allot_users_obj->coupon_issued = $row['coupon_issued'];
           $tyr_coupons_allot_users_obj->coupon_used = $row['coupon_used'];
           $tyr_coupons_allot_users_obj->coupon_used_for = $row['coupon_used_for'];
           $tyr_coupons_allot_users_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_current_coupon_by_user_id(&$tyr_coupons_allot_users_obj) {
        $query = 'select * from tyr_coupons_allot_users where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != false){
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_current_coupons_by_user_id(&$tyr_coupons_allot_users_obj) {
        $query = 'select * from tyr_coupons_allot_users where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_coupons_allot_users(&$tyr_coupons_allot_users_obj) {
        $query = 'update tyr_coupons_allot_users set coupon_id = :coupon_id, user_id = :user_id, coupon_label = :coupon_label, coupon_value = :coupon_value, coupon_type = :coupon_type, coupon_status = :coupon_status, coupon_issued = :coupon_issued, coupon_used = :coupon_used, coupon_used_for = :coupon_used_for, status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by where allot_id = :allot_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $tyr_coupons_allot_users_obj->coupon_id);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->user_id);
        $statement->bindParam(':coupon_label', $tyr_coupons_allot_users_obj->coupon_label);
        $statement->bindParam(':coupon_value', $tyr_coupons_allot_users_obj->coupon_value);
        $statement->bindParam(':coupon_type', $tyr_coupons_allot_users_obj->coupon_type);
        $statement->bindParam(':coupon_status', $tyr_coupons_allot_users_obj->coupon_status);
        $statement->bindParam(':coupon_issued', $tyr_coupons_allot_users_obj->coupon_issued);
        $statement->bindParam(':coupon_used', $tyr_coupons_allot_users_obj->coupon_used);
        $statement->bindParam(':coupon_used_for', $tyr_coupons_allot_users_obj->coupon_used_for);
        $statement->bindParam(':status_sl', $tyr_coupons_allot_users_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_coupons_allot_users_obj->created_updated_by);
        $statement->bindParam(':allot_id', $tyr_coupons_allot_users_obj->allot_id);
        $statement->execute();
        return true;
        
    }
    
    public function get_all_current_coupons_by_user_id_and_status(&$tyr_coupons_allot_users_obj) {
        $query = 'select * from tyr_coupons_allot_users where user_id = :user_id and coupon_status = :coupon_status';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->user_id);
        $statement->bindParam(':user_id', $tyr_coupons_allot_users_obj->coupon_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array = $row;
        }
        return $return_array;
    }
    public function update_status_sl(&$tyr_invite_friend_obj) {
        $query = 'update tyr_coupons_allot_users set status_sl = :status_sl where user_id = :user_id AND allot_id = :allot_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_invite_friend_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_invite_friend_obj->user_id);
        $statement->bindParam(':allot_id', $tyr_invite_friend_obj->allot_id);
        $statement->execute();
        return true;
    }
}