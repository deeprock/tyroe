<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_company_detail_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_company_detail(&$tyr_company_detail_obj) {
        $query = 'INSERT into tyr_company_detail(
                   studio_id, company_name, parent_id, company_country, company_state, company_city, company_job_title, company_description, industry_id, website, logo_image, created_at, created_by, updated_at, updated_by
                  ) values(
                   :studio_id, :company_name, :parent_id, :company_country, :company_state, :company_city, :company_job_title,  :company_description, :industry_id, :website, :logo_image, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_company_detail_obj->studio_id);
        $statement->bindParam(':company_name', $tyr_company_detail_obj->company_name);
        $statement->bindParam(':company_country', $tyr_company_detail_obj->company_country);
        $statement->bindParam(':company_state', $tyr_company_detail_obj->company_state);
        $statement->bindParam(':company_city', $tyr_company_detail_obj->company_city);
        $statement->bindParam(':company_job_title', $tyr_company_detail_obj->company_job_title);
        $statement->bindParam(':company_description', $tyr_company_detail_obj->company_description);
        $statement->bindParam(':industry_id', $tyr_company_detail_obj->industry_id);
        $statement->bindParam(':website', $tyr_company_detail_obj->website);
        $statement->bindParam(':logo_image', $tyr_company_detail_obj->logo_image);
        $statement->bindParam(':parent_id', $tyr_company_detail_obj->parent_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_company_detail_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_company_detail_obj->created_updated_by);
        $statement->execute();
        $tyr_company_detail_obj->company_id = $this->db_connection->lastInsertId('tyr_company_detail_company_id_seq');
    }

    public function get_company_detail(&$tyr_company_detail_obj) {
        $query = 'select * from tyr_company_detail where company_id = :company_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_id', $tyr_company_detail_obj->company_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_company_detail_obj->company_id = $row['company_id'];
           $tyr_company_detail_obj->studio_id = $row['studio_id'];
           $tyr_company_detail_obj->company_name = $row['company_name'];
           $tyr_company_detail_obj->company_country = $row['company_country'];
           $tyr_company_detail_obj->company_state = $row['company_state'];
           $tyr_company_detail_obj->company_city = $row['company_city'];
           $tyr_company_detail_obj->company_job_title = $row['company_job_title'];
           $tyr_company_detail_obj->company_description = $row['company_description'];
           $tyr_company_detail_obj->industry_id = $row['industry_id'];
           $tyr_company_detail_obj->website = $row['website'];
           $tyr_company_detail_obj->logo_image = $row['logo_image'];
           $tyr_company_detail_obj->parent_id = $row['parent_id'];
           $tyr_company_detail_obj->updated_by = $row['updated_by'];
           $tyr_company_detail_obj->updated_at = $row['updated_at'];
        }
    }
    
    public function get_company_detail_by_studio(&$tyr_company_detail_obj) {
        $query = 'select * from tyr_company_detail where studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_company_detail_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_company_detail_obj->company_id = $row['company_id'];
           $tyr_company_detail_obj->studio_id = $row['studio_id'];
           $tyr_company_detail_obj->company_name = $row['company_name'];
           $tyr_company_detail_obj->company_country = $row['company_country'];
           $tyr_company_detail_obj->company_state = $row['company_state'];
           $tyr_company_detail_obj->company_city = $row['company_city'];
           $tyr_company_detail_obj->company_job_title = $row['company_job_title'];
           $tyr_company_detail_obj->company_description = $row['company_description'];
           $tyr_company_detail_obj->industry_id = $row['industry_id'];
           $tyr_company_detail_obj->website = $row['website'];
           $tyr_company_detail_obj->logo_image = $row['logo_image'];
           $tyr_company_detail_obj->parent_id = $row['parent_id'];
           $tyr_company_detail_obj->updated_by = $row['updated_by'];
           $tyr_company_detail_obj->updated_at = $row['updated_at'];
        }
    }
    
    public function get_company_count(&$tyr_company_detail_obj) {
        $query = 'SELECT COUNT(studio_id) AS cnt FROM tyr_company_detail WHERE studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_company_detail_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_company_all_details(&$tyr_company_detail_obj) {
        $query = "update tyr_company_detail set studio_id = :studio_id, company_name = :company_name, company_country = :company_country, company_state = :company_state, company_city = :company_city, company_job_title = :company_job_title, company_description = :company_description, industry_id = :industry_id, website = :website, logo_image = :logo_image, updated_at = :updated_at, updated_by = :updated_by WHERE company_id = :company_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_company_detail_obj->studio_id);
        $statement->bindParam(':company_name', $tyr_company_detail_obj->company_name);
        $statement->bindParam(':company_country', $tyr_company_detail_obj->company_country);
        $statement->bindParam(':company_state', $tyr_company_detail_obj->company_state);
        $statement->bindParam(':company_city', $tyr_company_detail_obj->company_city);
        $statement->bindParam(':company_job_title', $tyr_company_detail_obj->company_job_title);
        $statement->bindParam(':company_description', $tyr_company_detail_obj->company_description);
        $statement->bindParam(':industry_id', $tyr_company_detail_obj->industry_id);
        $statement->bindParam(':website', $tyr_company_detail_obj->website);
        $statement->bindParam(':logo_image', $tyr_company_detail_obj->logo_image);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_company_detail_obj->updated_by);
        $statement->bindParam(':company_id', $tyr_company_detail_obj->company_id);
        $statement->execute();
        return true;
    }
    
    public function update_company_details(&$tyr_company_detail_obj) {
        $query = 'update tyr_company_detail set company_job_title = :company_job_title, updated_at = :updated_at, updated_by = :updated_by WHERE studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        //$statement->bindParam(':company_name', $tyr_company_detail_obj->company_name);
        $statement->bindParam(':company_job_title', $tyr_company_detail_obj->company_job_title);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_company_detail_obj->studio_id);
        $statement->bindParam(':studio_id', $tyr_company_detail_obj->studio_id);
        $statement->execute();
        return true;
    }
    
    public function check_duplicate_company_name(&$tyr_company_detail_obj,$where_company) {
        $query = 'select * from tyr_company_detail where company_name = :company_name '.$where_company;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_name', $tyr_company_detail_obj->company_name);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_company_detail_obj->company_id = $row['company_id'];
           $tyr_company_detail_obj->studio_id = $row['studio_id'];
           $tyr_company_detail_obj->company_name = $row['company_name'];
           $tyr_company_detail_obj->company_country = $row['company_country'];
           $tyr_company_detail_obj->company_state = $row['company_state'];
           $tyr_company_detail_obj->company_city = $row['company_city'];
           $tyr_company_detail_obj->company_job_title = $row['company_job_title'];
           $tyr_company_detail_obj->company_description = $row['company_description'];
           $tyr_company_detail_obj->industry_id = $row['industry_id'];
           $tyr_company_detail_obj->website = $row['website'];
           $tyr_company_detail_obj->logo_image = $row['logo_image'];
           $tyr_company_detail_obj->updated_by = $row['updated_by'];
           $tyr_company_detail_obj->updated_at = $row['updated_at'];
        }
    }
    
    
    public function get_group_concat_company_name($where_searching_value) {
        $search_value = "%".$where_searching_value."%";
        $query = "SELECT array_to_string(array_agg(company_name),',') AS cname FROM tyr_company_detail WHERE company_name LIKE :company_name";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_name',$search_value);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['cname'] = $row['cname'];
        }
        return $return_array;
    }
    
    public function get_company_name_list($where_searching_value) {
        $search_value = $where_searching_value."%";
        $query = "SELECT a.company_name, a.studio_id  FROM tyr_company_detail as a, tyr_users as b WHERE a.studio_id = b.user_id and b.status_sl = 1 and a.company_name LIKE :company_name and a.parent_id = 0";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_name',$search_value);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $temp_array = array();
            $temp_array['label'] = $row['company_name'];
            $temp_array['value'] = $row['studio_id'];
            $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_company_count_by_company_name(&$tyr_company_detail_obj) {
        $query = 'SELECT COUNT(company_id) AS co_cnt, studio_id AS company_owner FROM tyr_company_detail WHERE company_name = :company_name';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_name', $tyr_company_detail_obj->company_name);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_company_count_by_parent_id_and_location(&$tyr_company_detail_obj) {
        $query = 'SELECT COUNT(company_id) AS co_cnt, studio_id AS company_owner FROM tyr_company_detail WHERE (company_id = :company_id or parent_id = :company_id) '
                . 'and company_country = :company_country and company_city = :company_city group by studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':company_id', $tyr_company_detail_obj->parent_id);
        $statement->bindParam(':company_country', $tyr_company_detail_obj->company_country);
        $statement->bindParam(':company_city', $tyr_company_detail_obj->company_city);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
}