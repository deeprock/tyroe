<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_reviewer_jobs_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_reviewer_jobs(&$tyr_reviewer_jobs_obj) {
        $query = 'INSERT into tyr_reviewer_jobs(
                    reviewer_id, studio_id, job_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :reviewer_id, :studio_id, :job_id, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_reviewer_jobs_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_reviewer_jobs_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_reviewer_jobs_obj->created_updated_by);
        $statement->execute();
        $tyr_reviewer_jobs_obj->rev_id = $this->db_connection->lastInsertId('tyr_reviewer_jobs_rev_id_seq'); 
    }

    public function get_reviewer_jobs(&$tyr_reviewer_jobs_obj) {
        $query = 'select * from tyr_reviewer_jobs where rev_id = :rev_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':rev_id', $tyr_reviewer_jobs_obj->rev_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_reviewer_jobs_obj->rev_id = $row['rev_id'];
           $tyr_reviewer_jobs_obj->reviewer_id = $row['reviewer_id'];
           $tyr_reviewer_jobs_obj->studio_id = $row['studio_id'];
           $tyr_reviewer_jobs_obj->job_id = $row['job_id'];
           $tyr_reviewer_jobs_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_reviewer_exist(&$tyr_reviewer_jobs_obj) {
        $query = 'select * from tyr_reviewer_jobs where reviewer_id = :reviewer_id and job_id : job_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_reviewer_jobs_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_reviewer_jobs_obj->rev_id = $row['rev_id'];
           $tyr_reviewer_jobs_obj->reviewer_id = $row['reviewer_id'];
           $tyr_reviewer_jobs_obj->studio_id = $row['studio_id'];
           $tyr_reviewer_jobs_obj->job_id = $row['job_id'];
           $tyr_reviewer_jobs_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_total_reviewers_count_for_studio(&$tyr_reviewer_jobs_obj) {
        $query = 'select COUNT(*) AS total_reviewers,job_id from tyr_reviewer_jobs where studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_reviewers_count_for_job(&$tyr_reviewer_jobs_obj) {
        //$query = 'SELECT COUNT(*) AS total_reviewers,job_id from tyr_reviewer_jobs where job_id = :job_id and status_sl = :status_sl';
        $query = 'select count(*) as total_reviewers from tyr_job_role_mapping where is_reviewer = 1 and job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        //$statement->bindParam(':status_sl', $tyr_reviewer_jobs_obj->status_sl);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_job_ids(&$tyr_reviewer_jobs_obj) {
        $query = 'SELECT array_to_string(array_agg(job_id ORDER BY job_id ASC), ",") As total_job_ids from tyr_reviewer_jobs where reviewer_id = :reviewer_id and studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
          $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_job_ids_by_reviewer(&$tyr_reviewer_jobs_obj) {
        $query = "SELECT array_to_string(array_agg(job_id ORDER BY job_id ASC ),',')AS job_id,reviewer_id FROM " . TABLE_REVIEWER_JOBS . 
                " WHERE reviewer_id= :reviewer_id AND studio_id = :studio_id "
                . "AND status_sl = :status_sl GROUP BY reviewer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_reviewer_jobs_obj->status_sl);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['job_id'] = $row['job_id'];
           $return_array['reviewer_id'] = $row['reviewer_id'];
        }
        return $return_array;
    }
    
    public function get_total_active_job_count(&$tyr_reviewer_jobs_obj) {
        $query = 'SELECT count(tyr_reviewer_jobs.job_id) as total_active_job from tyr_reviewer_jobs where reviewer_id = :reviewer_id and studio_id = :studio_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_reviewer_jobs_obj->status_sl);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_active_job'] = $row['total_active_job'];
        }
        return $return_array;
    }
    
    public function get_reviewers_count_for_studio_job(&$tyr_reviewer_jobs_obj) {
        //$query = 'SELECT COUNT(*) AS total_reviewers,job_id from tyr_reviewer_jobs where job_id = :job_id and studio_id = :studio_id';
        $query = "select COUNT(*) AS total_reviewers,tyr_job_role_mapping.job_id, "
                . "array_to_string(array_agg(tyr_job_role_mapping.user_id ORDER BY tyr_job_role_mapping.user_id ASC),',')AS reviewer_ids"
                . ' from tyr_job_role_mapping inner join tyr_jobs on'
                . ' tyr_job_role_mapping.job_id = tyr_jobs.job_id inner join tyr_users on tyr_jobs.user_id = tyr_users.user_id '
                . 'where tyr_jobs.job_id = :job_id and (tyr_users.user_id = :studio_id or tyr_users.parent_id = :studio_id) and '
                . 'tyr_job_role_mapping.is_reviewer = 1 group by tyr_job_role_mapping.job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_reviewer_id_for_job(&$tyr_reviewer_jobs_obj) {
        $query = 'SELECT reviewer_id from tyr_reviewer_jobs where job_id = :job_id and studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_jobs_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    
    public function get_job_ids_by_reviewer_id(&$tyr_reviewer_jobs_obj) {
        $query = "SELECT array_to_string(array_agg(job_id ORDER BY job_id ASC),',')AS job_id,reviewer_id FROM "
                    . TABLE_REVIEWER_JOBS . " WHERE reviewer_id = :reviewer_id GROUP BY reviewer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->execute();
        $return_array = '';
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_row_by_reviewer_job(&$tyr_reviewer_jobs_obj) {
        $query = 'SELECT * from tyr_reviewer_jobs where job_id = :job_id and reviewer_id = :reviewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['rev_id'] = $row['rev_id'];
           $temp_array['reviewer_id'] = $row['reviewer_id'];
           $temp_array['studio_id'] = $row['studio_id'];
           $temp_array['job_id'] = $row['job_id'];
           $temp_array['status_sl'] = $row['status_sl'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_row_by_reviewer_job(&$tyr_reviewer_jobs_obj) {
        $query = 'SELECT * from tyr_reviewer_jobs where job_id = :job_id and reviewer_id = :reviewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function delete_reviewer_job(&$tyr_reviewer_jobs_obj) {
        $query = 'delete from tyr_reviewer_jobs where rev_id = :rev_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':rev_id', $tyr_reviewer_jobs_obj->rev_id);
        $statement->execute();
        return TRUE;
    }
    
    public function delete_reviewer_by_job_reviewer(&$tyr_reviewer_jobs_obj) {
        $query = 'delete from tyr_reviewer_jobs where job_id = :job_id && reviewer_id = :reviewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_reviewer_jobs_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_jobs_obj->reviewer_id);
        $statement->execute();
        return TRUE;
    }
}