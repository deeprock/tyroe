<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_schedule_notification_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_schedule_notification(&$tyr_schedule_notification_obj) {
        $query = 'INSERT into tyr_schedule_notification(
                    user_id, option_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :option_id, :status_sl,  :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_schedule_notification_obj->user_id);
        $statement->bindParam(':option_id', $tyr_schedule_notification_obj->option_id);
        $statement->bindParam(':status_sl', $tyr_schedule_notification_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_schedule_notification_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_schedule_notification_obj->created_updated_by);
        $statement->execute();
        $tyr_schedule_notification_obj->schedule_id = $this->db_connection->lastInsertId('tyr_schedule_notification_schedule_id_seq');
    }

    public function get_schedule_notification(&$tyr_schedule_notification_obj) {
        $query = 'select * from tyr_schedule_notification where schedule_id = :schedule_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':schedule_id', $tyr_schedule_notification_obj->schedule_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_schedule_notification_obj->schedule_id = $row['schedule_id'];
           $tyr_schedule_notification_obj->user_id = $row['user_id'];
           $tyr_schedule_notification_obj->option_id = $row['option_id'];
           $tyr_schedule_notification_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function update_schedule_notification(&$tyr_schedule_notification_obj) {
        $query = 'update tyr_schedule_notification set user_id = :user_id, option_id = :option_id, status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by where schedule_id = :schedule_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':option_id', $tyr_schedule_notification_obj->option_id);
        $statement->bindParam(':status_sl', $tyr_schedule_notification_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_schedule_notification_obj->user_id);
        $statement->bindParam(':schedule_id', $tyr_schedule_notification_obj->schedule_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_schedule_notification_obj->user_id);
        $statement->execute();
        return true;
    }
    
    public function get_all_schedule_notification_by_user_id(&$tyr_schedule_notification_obj) {
        $query = 'SELECT * FROM tyr_schedule_notification WHERE user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', intval($tyr_schedule_notification_obj->user_id));
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_schedule_notification_by_user_option(&$tyr_schedule_notification_obj) {
        $query = 'SELECT * FROM tyr_schedule_notification WHERE user_id = :user_id AND option_id = :option_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_schedule_notification_obj->user_id);
        $statement->bindParam(':option_id', $tyr_schedule_notification_obj->option_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;;
    }
    
}