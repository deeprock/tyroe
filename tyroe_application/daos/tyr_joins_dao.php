<?php
require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_joins_dao extends Abstract_DAO {

    public function __construct($db_conn) {
        parent::__construct($db_conn);
    }

    public function get_all_modules_by_user_id($user_id) {
        $query = 'SELECT m.module_id FROM tyr_modules m, tyr_permissions p WHERE m.module_id = p.module_id AND p.user_id = ' . $user_id . ' '
                . 'ORDER BY m.sort_order ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_company_by_user_id($user_id) {
        $query = "SELECT users.user_id, role_id, users.parent_id, email, firstname, lastname, users.city, 
                                          password,password_len,username,profile_url,cd.company_name,users.job_title
                                   FROM " . TABLE_USERS . " users
                                   LEFT JOIN " . TABLE_COMPANY . " cd ON cd.studio_id = users.user_id 
                                   WHERE users.user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_profile_viewer_type($user_id) {
        $query = 'SELECT
          pv.viewer_id,
          pv.viewer_type,
          IF(pv.viewer_type = 4, u.username, "") AS reviewer,
          IF(pv.viewer_type = 3, u.username, (SELECT username FROM tyr_users WHERE user_id = u.parent_id ))
          AS studio,
          u.lastname,u.city,u.state,m.media_name,pv.created_timestamp,job.job_title
        FROM
          tyr_profile_view pv
          LEFT JOIN tyr_users  u
            ON pv.viewer_id = u.user_id
          LEFT JOIN tyr_media  m
            ON u.user_id = m.user_id
            AND m.profile_image = 1
            LEFT JOIN tyr_jobs  job
                        ON job.job_id = pv.job_id
        WHERE pv.user_id = ' . $user_id;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_shortlisted_users_jobs($user_id) {
        $query = 'SELECT job.*,users.username FROM try_jobs job
                           LEFT JOIN tyr_users users ON(job.user_id= users.user_id)
                           LEFT JOIN tyr_job_shortlist shortlist ON(shortlist.job_id = job.job_id)
                           WHERE shortlist.user_id = ' . $user_id . ' AND job.archived_status = "0"';

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_jobs_skill_by_job_id($job_id) {
        $query = "SELECT job_skill_id, creative_skill
                    FROM " . TABLE_JOB_SKILLS . " LEFT JOIN
                    " . TABLE_CREATIVE_SKILLS . " ON " . TABLE_CREATIVE_SKILLS . ".creative_skill_id =" . TABLE_JOB_SKILLS . ".creative_skill_id
                    WHERE job_id='" . $job_id . "'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_invite_candidate_users_jobs($user_id, $where_feedback) {
        $query = 'SELECT COUNT(*) as cnt
                                FROM tyr_rate_review review
                                INNER JOIN tyr_users users ON users.user_id=review.user_id
                                LEFT JOIN tyr_jobs jobs ON jobs.job_id = review.job_id
                                LEFT JOIN tyr_media media ON media.user_id=review.user_id AND media.profile_image= 1 AND media.status_sl= 1
                                LEFT JOIN tyr_countries country ON country.country_id=users.country_id
                                WHERE review.tyroe_id = ' . $user_id . $where_feedback . ' ORDER BY review_id DESC';

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_feedback_by_reviews($user_id, $where_feedback, $order_by_pagintaion) {
        $query = 'SELECT review.user_id as company_id,review_id,technical,creative,impression,review_comment,review.created_timestamp,users.username,
              jobs.job_title,media.media_name,users.city,country.country,review.job_id
              FROM tyr_rate_review review
              INNER JOIN tyr_users users ON users.user_id=review.user_id
              LEFT JOIN tyr_jobs jobs ON jobs.job_id=review.job_id
              LEFT JOIN tyr_media media ON media.user_id=review.user_id AND media.profile_image= 1 AND media.status_sl= 1
              LEFT JOIN tyr_countries country ON country.country_id=users.country_id
              WHERE review.status_sl= 1  AND review.tyroe_id = ' . $user_id . $where_feedback . $order_by_pagintaion;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_candidate() {
        $query = "SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                    username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                    state,zip,suburb,display_name,modified_timestamp,created_timestamp, tyr_users.status_sl,
                     tyr_countries.country, tyr_media.media_type, tyr_media.media_name FROM tyr_users
                  LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                  LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id
                  AND tyr_media.profile_image = 1 AND tyr_media.status_sl= 1
                  WHERE tyr_users.role_id = 2 and tyr_users.status_sl = 1 ";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_candidate_details_by_candidate_id($candidate_id) {
        $query = 'SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                                username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                                state,zip,suburb,display_name,modified_timestamp,created_timestamp, tyr_users.status_sl,
                                 tyr_countries.country, tyr_media.media_type, tyr_media.media_name FROM tyr_users
                              LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                              LEFT JOIN tyr_media ON tyr_media.user_id = tyr_media.user_id
                              AND tyr_media.profile_image= 1 AND tyr_media.status_sl = 1
                              WHERE tyr_users.role_id= 2  AND tyr_users.user_id= ' . $candidate_id . ' and tyr_users.status_sl= 1';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_user_skill($candidate_id) {
        $query = 'SELECT skill_id, user_id, tyr_creative_skills.creative_skill as skill,status_sl
                           FROM tyr_skills LEFT JOIN
                            tyr_creative_skills ON tyr_creative_skills.creative_skill_id = tyr_skills.skill
                            WHERE user_id = ' . $candidate_id . ' and status_sl = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_email_data_by_user_id($user_id, $on) {
        $query = 'SELECT u.firstname,u.lastname,u.job_title, c.company_id, c.company_name
                                                FROM tyr_users u
                                                LEFT JOIN tyr_company_detail c ' . $on . '
                                                WHERE u.user_id = ' . $user_id;
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['firstname'] = $row['firstname'];
            $return_array['lastname'] = $row['lastname'];
            $return_array['job_title'] = $row['job_title'];
            $return_array['company_id'] = $row['company_id'];
            $return_array['company_name'] = $row['company_name'];
        }
        return $return_array;
    }

    public function get_all_jobs_details($where) {
        $query = "SELECT job_id,user_id,category_id,job_title,job_type.job_type,job_description,tyr_countries.country as job_location,
        tyr_job_level.level_title as job_skill,start_date,
                 end_date,job_quantity,tyr_jobs.created_timestamp,tyr_jobs.modified_timestamp,tyr_jobs.status_sl  FROM tyr_jobs
                   LEFT JOIN tyr_countries ON tyr_jobs.country_id=tyr_countries.country_id
                   LEFT JOIN tyr_job_level ON tyr_jobs.job_level_id= tyr_job_level.job_level_id
                   LEFT JOIN tyr_job_type job_type ON tyr_jobs.job_type=job_type.job_type_id
                  where tyr_jobs.status_sl='1'
                 and archived_status= 0" . $where . "ORDER BY job_id DESC LIMIT 2";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_jobs_details_1($where) {
        $query = "SELECT job_id,user_id,category_id,job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
                         " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date,
                           end_date,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl  FROM " . TABLE_JOBS . "
                 LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                 LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
                           where " . TABLE_JOBS . ".status_sl='1'
                           and archived_status='0'" . $where;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_team_admin($user_id) {
        $query = 'SELECT
            tyr_users.user_id,
            tyr_users.firstname,
            tyr_users.lastname,
            tyr_users.user_occupation,
            tyr_users.email,
            tyr_media.media_type,
            tyr_media.media_name
            FROM tyr_users
            LEFT JOIN tyr_media ON tyr_users.user_id=tyr_media.user_id
            AND tyr_media.profile_image= 1 AND tyr_media.status_sl= 1
            WHERE tyr_users.user_id = ' . $user_id . ' AND
            tyr_users.status_sl =  1';

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_top_candidate_details($hidden_user) {
        $query = "SELECT tyr_users.user_id, availability, role_id, parent_id, tyr_users.country_id, email,
                    username, password, password_len, firstname, lastname, extra_title, phone, fax, cellphone, address,
                    state, zip, suburb, display_name, created_timestamp, modified_timestamp, tyr_industry.industry_name, tyr_cities.city, tyr_users.status_sl,
                     tyr_countries.country, tyr_media.media_type, tyr_media.media_name FROM tyr_users
                  RIGHT JOIN tyr_featured_tyroes ON tyr_users.user_id = tyr_featured_tyroes.featured_tyroe_id  AND tyr_featured_tyroes.featured_status = 1
                  LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                   LEFT JOIN tyr_cities ON tyr_users.city = tyr_cities.city_id
                  LEFT JOIN tyr_industry ON tyr_industry.industry_id= tyr_users.industry_id
                  LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id
                  AND tyr_media.profile_image = 1 AND tyr_media.status_sl = 1
                  WHERE tyr_users.role_id = 2 and tyr_users.status_sl = 1  AND tyr_users.publish_account = 1 AND tyr_users.user_id NOT IN (" . $hidden_user . ") ORDER BY user_id DESC LIMIT 2";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_activity_stream($where, $order = "", $pagination_limit = "") {
        $query = "SELECT
                                    job_activity_id, j_act.object_id, j_act.job_id, j_act.activity_data, j_act.performer_id,
                                    job.job_title, act_type.activity_type_id,
                                    act_type.activity_title AS activity_type, j_act.created_timestamp, users.username
                    FROM " . TABLE_JOB_ACTIVITY . " j_act
                    INNER JOIN  " . TABLE_USERS . " users ON j_act.performer_id=users.user_id
                    INNER JOIN  " . TABLE_ACTIVITY_TYPE . " act_type ON act_type.activity_type_id = j_act.activity_type
                    LEFT JOIN  " . TABLE_JOBS . " job ON j_act.job_id=job.job_id
                    WHERE j_act." . $where . " AND j_act.status_sl = 1 ORDER BY job_activity_id DESC ".$pagination_limit;
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_openings_listing($studio_id,$reviewer_id) {
        $query = "SELECT " . TABLE_JOBS . ".job_id, " . TABLE_JOBS . ".job_title FROM " 
                    . TABLE_REVIEWER_JOBS . " LEFT JOIN " . TABLE_JOBS . " ON " . TABLE_JOBS . ".job_id = " . TABLE_REVIEWER_JOBS 
                    . ".job_id WHERE " . TABLE_REVIEWER_JOBS . ".reviewer_id = :reviewer_id AND " . TABLE_REVIEWER_JOBS . ".studio_id = :studio_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $reviewer_id);
        $statement->bindParam(':studio_id', $studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_openings_listing_1($studio_id,$reviewer_id,$job_id) {
        $query = 'SELECT tyr_jobs.job_id, tyr_jobs.job_title FROM tyr_reviewer_jobs LEFT JOIN tyr_jobs ON tyr_jobs.job_id = tyr_reviewer_jobs.job_id'
                . ' WHERE tyr_reviewer_jobs.reviewer_id = :reviewer_id AND tyr_reviewer_jobs.studio_id = :studio_id AND tyr_reviewer_jobs.job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $reviewer_id);
        $statement->bindParam(':studio_id', $studio_id);
        $statement->bindParam(':job_id', $job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';

        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_tyroe_featured_approval() {
        $query = "SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_timestamp,u.status_sl,u.publish_account,
                                            f.featured_status,tpv.visit_time FROM " . TABLE_USERS . " u
                                            LEFT JOIN " . TABLE_FEATURED_TYROE . " f ON u.user_id = f.featured_tyroe_id
                                            LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  " . TABLE_PAGEVIEWS . " GROUP BY user_id) tpv
                                            ON u.user_id = tpv.user_id WHERE u.status_sl = 1 AND f.featured_status = 0 AND u.publish_account = 1 AND u.role_id = 2 GROUP BY u.user_id,f.featured_status, tpv.visit_time
                                             ORDER BY u.user_id DESC LIMIT 5";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_studios_featured_approval($where, $order_by, $limit, $studio_role) {
        $query = "SELECT st.user_id AS tu_user_id,co.company_name,stat.status,st.email,c.country AS country, tyr_cities.city, i.industry_name AS industry,st.created_timestamp
                    AS signed_up,tpv.visit_time FROM tyr_users st LEFT JOIN tyr_statuses stat ON st.status_sl = stat.status_id
                    LEFT JOIN tyr_industry i ON st.industry_id = i.industry_id LEFT JOIN tyr_countries c ON st.country_id = c.country_id 
                    LEFT JOIN tyr_cities on st.city = tyr_cities.city_id 
                    LEFT JOIN tyr_company_detail co ON st.user_id = co.studio_id LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id
                    FROM tyr_pageviews GROUP BY user_id) tpv ON st.user_id = tpv.user_id WHERE st.parent_id = 0 AND st.role_id != 1 and st.role_id != 2 and  st.status_sl=1 GROUP BY st.user_id, co.company_name, stat.status, c.country, tyr_cities.city, i.industry_name, tpv.visit_time  " . $order_by . $limit;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();            
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_personal_info($user_id) {
        $query = "SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                  username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                  state,zip,suburb,display_name,created_timestamp,modified_timestamp, tyr_users.status_sl,
                   tyr_countries.country, tyr_media.media_type,tyr_media.media_name FROM tyr_users
                LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id
                WHERE tyr_users.user_id='" . $user_id . "' ORDER BY image_id DESC LIMIT 1";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_user_personal_info_1($user_id) {
        $query = "SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                         username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                         state,zip,suburb,display_name,created_timestamp,modified_timestamp, tyr_users.status_sl,
                          tyr_countries.country, tyr_media.image_id, tyr_media.media_type,tyr_media.media_name FROM tyr_users
                       LEFT JOIN tyr_countries ON tyr_users.country_id=tyr_countries.country_id
                       LEFT JOIN tyr_media ON tyr_users.user_id=tyr_media.user_id
                       WHERE tyr_users.user_id='" . $user_id . "'";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_total_row_count_jobopening($user_id, $where, $where_job) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_jobs job
        LEFT JOIN tyr_job_type job_type ON job.job_type=job_type.job_type_id
        LEFT JOIN tyr_countries country ON job.country_id=country.country_id
        LEFT JOIN tyr_job_level job_lev ON job.job_level_id=job_lev.job_level_id
        LEFT JOIN tyr_job_detail job_detail ON job_detail.job_id=job.job_id AND (job_detail.tyroe_id = '' OR job_detail.tyroe_id = " . $user_id . ")
        INNER JOIN tyr_users users ON job.user_id=users.user_id
                  WHERE job.status_sl= 1
                  AND archived_status= 0  " . $where . " " . $where_job;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_job_details_paginated($user_id, $where, $where_job, $order_by, $pagintaion_limit) {
        $query = "SELECT job_status_id,invitation_status,job.job_id,job.user_id,users.role_id,category_id,job.job_title,job_description,country.country AS job_location,comp_detail.company_name,
                job_lev.level_title AS job_skill,start_date,job_type.job_type,users.username,
                  end_date,job_quantity,job.created_timestamp,job.modified_timestamp,job.status_sl," . TABLE_CITIES . ".city as job_city  FROM " . TABLE_JOBS . " job
        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
        LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_CITIES . ".city_id=job.job_city
        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
        LEFT JOIN " .TABLE_JOB_DETAIL. " job_detail ON job_detail.job_id=job.job_id AND (job_detail.tyroe_id = 0 OR job_detail.tyroe_id = ".$user_id.")
        LEFT JOIN " .TABLE_COMPANY. " comp_detail ON job.user_id = comp_detail.studio_id
        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
                  WHERE job.status_sl='1'
                  AND archived_status='0' " . $where. ' ' . $where_job . $order_by.$pagintaion_limit['limit'];
        
//        $query = "SELECT job.job_level_id, job_status_id,invitation_status,job.job_id,job.user_id,users.role_id,category_id,job.job_title,job_description,country.country AS job_location,comp_detail.company_name,
//                job_lev.level_title AS job_skill,start_date,job_type.job_type,users.username,
//                  end_date,job_quantity,job.created_timestamp,job.modified_timestamp,job.status_sl," . TABLE_CITIES . ".city as job_city  FROM " . TABLE_JOBS . " job
//        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
//        LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_CITIES . ".city_id=job.job_city
//        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
//        LEFT JOIN " . TABLE_JOB_DETAIL . " job_detail ON job_detail.job_id=job.job_id
//        LEFT JOIN " . TABLE_COMPANY . " comp_detail ON job.user_id = comp_detail.studio_id
//        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
//                  WHERE job.status_sl = 1
//                  AND archived_status = 0 " . $where . ' ' . $where_job . $order_by . $pagintaion_limit['limit'];

//        echo $query;
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_skill_data_for_job($user_id) {
        $query = "SELECT sk.*, csk.creative_skill as skill_name
                                   FROM tyr_job_skills sk
                                   LEFT JOIN tyr_creative_skills csk ON (sk.creative_skill_id = csk.creative_skill_id)
                                   WHERE sk.studio_id=" . $user_id . "";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $temp = array();
            $temp['skill_name'] = $row['skill_name'];
            $temp['job_skill_id'] = $row['job_skill_id'];
            $temp['studio_id'] = $row['studio_id'];
            $temp['job_id'] = $row['job_id'];
            $temp['creative_skill_id'] = $row['creative_skill_id'];
            $return_array[] = $temp;
        }
        return $return_array;
    }

    public function get_edit_job_details($user_id, $edit_id) {
        $query = "SELECT job_id,user_id,category_id,country_id,tyr_jobs.job_level_id,job_title,job_description,job_location, tyr_job_level.level_title as job_skill,start_date,
                          end_date,job_quantity,created_at  FROM tyr_jobs
                          LEFT JOIN tyr_job_level ON tyr_jobs.job_level_id = tyr_job_level.job_level_id
                          where user_id = " . $user_id . " and job_id= " . $edit_id . " and status_sl= 1 ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['job_id'] = $row['job_id'];
            $return_array['user_id'] = $row['user_id'];
            $return_array['category_id'] = $row['category_id'];
            $return_array['country_id'] = $row['country_id'];
            $return_array['job_level_id'] = $row['job_level_id'];
            $return_array['job_title'] = $row['job_title'];
            $return_array['job_description'] = $row['job_description'];
            $return_array['job_location'] = $row['job_location'];
            $return_array['job_skill'] = $row['job_skill'];
            $return_array['start_date'] = $row['start_date'];
            $return_array['end_date'] = $row['end_date'];
            $return_array['job_quantity'] = $row['job_quantity'];
            $return_array['created_at'] = $row['created_at'];
        }
        return $return_array;
    }

    public function get_email_data_by_job_id($job_id) {
        $query = "SELECT job_id, studio_id, job_title, company_name FROM tyr_jobs j
                                LEFT JOIN tyr_company_detail cd ON j.user_id = cd.studio_id
                                WHERE job_id = " . $job_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['job_id'] = $row['job_id'];
            $return_array['studio_id'] = $row['studio_id'];
            $return_array['job_title'] = $row['job_title'];
            $return_array['company_name'] = $row['company_name'];
        }
        return $return_array;
    }

    public function get_total_row_count_archive_job($where) {
        $query = "SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . "
                       LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
                      where status_sl='1' and archived_status='1'" . $where;
        ///echo $query;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_job_details_archive_job($where, $pagintaion_limit) {
        $query = "SELECT job_id,user_id,category_id,job_title,job_description,start_date,
             end_date,job_quantity,created_timestamp,modified_timestamp," . TABLE_JOB_TYPE . ".job_type," . TABLE_CITIES . ".city as job_city," . TABLE_COUNTRIES . ".country AS job_location  FROM " . TABLE_JOBS . "
               LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
               LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_CITIES . ".city_id=" . TABLE_JOBS . ".job_city
               LEFT JOIN " . TABLE_JOB_TYPE . "  ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
              where " . TABLE_JOBS . ".status_sl='1' and archived_status='1'" . $where . $pagintaion_limit['limit'];

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_invite_jobs($get_invite_jobs, $user_id) {
        $query = "SELECT j.*, inv.invitation_status  FROM tyr_jobs j
                 LEFT JOIN tyr_invite_tyroe inv ON (j.job_id = inv.job_id) where j.status_sl= 1
                 and j.archived_status = 0 and j.job_id IN(" . $get_invite_jobs['job_id'] . ")  AND inv.tyroe_id = " . $user_id;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_studio_details_by_job_id($job_id) {
        $query = "SELECT j.job_title, j.user_id, u.email, c.company_name AS job_creator
                                    FROM tyr_jobs j
                                    LEFT JOIN tyr_company_detail c ON j.user_id = c.studio_id
                                    LEFT JOIN tyr_users u ON j.user_id = u.user_id
                                    WHERE j.job_id = " . $job_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_total_row_count_filter_job($user_id, $where, $where_values) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_jobs job
                                    LEFT JOIN tyr_job_type job_type
                                      ON job.job_type = job_type.job_type_id
                                    LEFT JOIN tyr_countries country
                                      ON job.country_id = country.country_id
                                    LEFT JOIN tyr_job_level job_lev
                                      ON job.job_level_id = job_lev.job_level_id
                                    LEFT JOIN tyr_job_detail job_detail
                                      ON job_detail.job_id = job.job_id
                                      AND job_detail.tyroe_id = " . $user_id . " 
                                    LEFT JOIN tyr_company_detail comp_detail
                                      ON job.user_id = comp_detail.studio_id
                                    INNER JOIN tyr_users users
                                      ON job.user_id = users.user_id
                                  WHERE job.status_sl= 1
                                  AND archived_status= 0 " . $where . " " . $where_values;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_job_details_filter_job($user_id, $where, $where_values, $orderby, $pagintaion_limit) {
        $query = "SELECT job_status_id,invitation_status,job.job_id,job.user_id,category_id,job.job_title,job_description,country.country AS job_location,
                        job_lev.level_title AS job_skill,comp_detail.company_name,start_date,job_type.job_type,users.username,
                          end_date,job_quantity,job.created_at,job.updated_at,job.status_sl,tyr_cities.city as job_city  FROM " . TABLE_JOBS . " job
                            LEFT JOIN tyr_job_type job_type
                              ON job.job_type = job_type.job_type_id
                            LEFT JOIN tyr_countries country
                              ON job.country_id = country.country_id
                              LEFT JOIN tyr_cities ON tyr_cities.city_id = job.job_city
                            LEFT JOIN tyr_job_level job_lev
                              ON job.job_level_id = job_lev.job_level_id
                            LEFT JOIN tyr_job_detail job_detail
                              ON job_detail.job_id = job.job_id
                              AND ( job_detail.tyroe_id = " . $user_id . " )
                            LEFT JOIN tyr_company_detail comp_detail
                              ON job.user_id=comp_detail.studio_id
                            INNER JOIN tyr_users users
                              ON job.user_id = users.user_id
                          WHERE job.status_sl= 1
                          AND archived_status= 0 " . $where . " " . $where_values . " ORDER BY job.job_id " . $orderby . $pagintaion_limit['limit'];

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_invite_jobs_details($get_invite_jobs, $user_id, $where_values, $orderby) {
        $query = "SELECT j.*, inv.invitation_status ,users.username FROM tyr_jobs j
                                       LEFT JOIN tyr_invite_tyroe inv ON (j.job_id = inv.job_id)
                                         LEFT JOIN tyr_users users ON(j.user_id= users.user_id)
                                          LEFT JOIN tyr_job_skills  ON(tyr_job_skills.job_id = j.job_id)
                                        where j.status_sl= 1
                                       and j.archived_status= 0  and j.job_id IN(" . $get_invite_jobs['job_id'] . ")  AND inv.tyroe_id = '{$user_id}'
                                       " . $where_values . "
                                       ORDER BY job_id " . $orderby;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_shortlist_invite_jobs_details($user_id, $where_values, $orderby) {
        $query = "SELECT j.*,users.username FROM tyr_jobs j
                           LEFT JOIN tyr_users users ON(j.user_id= users.user_id)
                           LEFT JOIN tyr_job_shortlist shortlist ON(shortlist.job_id= j.job_id)
                           LEFT JOIN tyr_job_skills  ON(tyr_job_skills.job_id = j.job_id)
                           WHERE shortlist.user_id= " . $user_id . "  AND j.archived_status= 0
                           " . $where_values . "
                           ORDER BY job_id " . $orderby;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_studio_admin_details($user_id) {
        $query = "SELECT u.firstname, u.lastname, c.company_name
                    FROM tyr_users u
                    LEFT JOIN tyr_company_detail c ON u.user_id = c.studio_id
                    WHERE studio_id = " . $user_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['firstname'] = $row['firstname'];
            $return_array['lastname'] = $row['lastname'];
            $return_array['company_name'] = $row['company_name'];
        }
        return $return_array;
    }

    public function search_existing_reviewers($join, $user_id, $searching_value) {
        $query = 'SELECT tyr_users.user_id,tyr_users.firstname,tyr_users.lastname,tyr_users.email,tyr_users.job_title,tyr_media.media_type,tyr_media.media_name
        FROM tyr_users
        LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id
        AND tyr_media.profile_image="1" AND tyr_media.status_sl= 1
         ' . $join . '
        WHERE tyr_users.role_id = 4 AND
        tyr_users.status_sl = 1 AND
        tyr_users.parent_id = ' . $user_id . '
         AND
        (
         CONCAT(tyr_users.firstname, " ", tyr_users.lastname) LIKE "%' . $searching_value . '%" OR
         tyr_users.firstname LIKE "%' . $searching_value . '%" OR
         tyr_users.lastname LIKE "%' . $searching_value . '%" OR
         tyr_users.username LIKE "%' . $searching_value . '%"
        )';

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function search_existing_reviewers_1($where, $user_id, $parent_id, $searching_value) {
        $query = "SELECT ". TABLE_USERS .".user_id,". TABLE_USERS .".firstname,". TABLE_USERS .".lastname,". TABLE_USERS .".email,". TABLE_USERS .".job_title," . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name
        FROM ". TABLE_USERS . "
        LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
        AND " . TABLE_MEDIA . ".profile_image= 1 AND " . TABLE_MEDIA . ".status_sl = 1 
        WHERE ". TABLE_USERS . ".user_id != ". $user_id . " AND
        ". TABLE_USERS . ".status_sl = 1 AND
        ". TABLE_USERS . ".parent_id = ".$parent_id."
         AND
        (
         CONCAT(" . TABLE_USERS . ".firstname ".","." ". TABLE_USERS . ".lastname) LIKE '%".$searching_value."%' OR
         ". TABLE_USERS . ".firstname LIKE '%".$searching_value."%' OR
         ". TABLE_USERS . ".lastname LIKE '%".$searching_value."%' OR
         ". TABLE_USERS . ".username LIKE '%".$searching_value."%'
        )".$where;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_added_reviewer_details($parent_id, $job_id, $reviewer_id) {
        $query = 'SELECT tyr_users.user_id,tyr_users.firstname,tyr_users.job_title,tyr_users.lastname,tyr_users.email,tyr_users.user_occupation,tyr_media.media_type,
            tyr_media.media_name,tyr_users.status_sl
                        FROM tyr_users
                        LEFT JOIN tyr_media ON tyr_users.user_id=tyr_media.user_id
                        AND tyr_media.profile_image = 1 AND tyr_media.status_sl= 1
                        LEFT JOIN tyr_reviewer_jobs ON tyr_users.user_id = tyr_reviewer_jobs.reviewer_id
                         AND tyr_reviewer_jobs.job_id = ' . $job_id . '
                        WHERE (tyr_users.role_id = 4 OR tyr_users.role_id = 3) AND
                        tyr_users.status_sl = 1 AND
                        tyr_users.parent_id = ' . $parent_id . ' AND tyr_users.user_id = ' . $reviewer_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_job_details_by_job_id($job_id) {
        $query = "SELECT job_id,user_id,archived_status,category_id,job_title,job_description,job_city,team_moderation,tyr_cities.city,tyr_job_type.job_type_id,tyr_job_type.job_type,tyr_countries.country
                AS job_location,tyr_countries.country_id,tyr_job_level.job_level_id,tyr_job_level.level_title AS job_skill,start_date,end_date,tyr_industry.industry_name,tyr_industry.industry_id,job_quantity,tyr_jobs.created_at,
                tyr_jobs.updated_at,tyr_jobs.status_sl FROM tyr_jobs
                LEFT JOIN tyr_countries ON tyr_jobs.country_id = tyr_countries.country_id
                LEFT JOIN tyr_cities ON tyr_jobs.job_city = tyr_cities.city_id
                LEFT JOIN tyr_job_level ON tyr_jobs.job_level_id = tyr_job_level.job_level_id
                LEFT JOIN tyr_job_type ON tyr_jobs.job_type=tyr_job_type.job_type_id
                LEFT JOIN tyr_industry ON tyr_jobs.industry_id= tyr_industry.industry_id WHERE job_id = " . $job_id . " AND tyr_jobs.status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
            
        }
        return $return_array;
    }

    public function get_all_reviewers_for_studio_job($job_id, $user_id,$parent_id) {
        $query = 'SELECT
        '. TABLE_USERS .'.user_id,
        '. TABLE_USERS .'.firstname,
        '. TABLE_USERS .'.lastname,
        '. TABLE_USERS .'.job_title,
        '. TABLE_USERS .'.email,
        '. TABLE_USERS .'.status_sl,
        '. TABLE_MEDIA .'.media_type,
        '. TABLE_MEDIA .'.media_name,
        '. TABLE_REVIEWER_JOBS .'.rev_id,
        '. TABLE_REVIEWER_JOBS .'.job_id
        FROM '. TABLE_USERS . '
        LEFT JOIN ' . TABLE_MEDIA . ' ON ' . TABLE_USERS . '.user_id=' . TABLE_MEDIA . '.user_id
        AND ' . TABLE_MEDIA . '.profile_image="1" AND ' . TABLE_MEDIA . '.status_sl="1"
        RIGHT JOIN '. TABLE_REVIEWER_JOBS .' ON ' . TABLE_USERS . '.user_id = '. TABLE_REVIEWER_JOBS .'.reviewer_id
        AND '. TABLE_REVIEWER_JOBS .'.job_id = "'.$job_id.'" AND '. TABLE_REVIEWER_JOBS .'.studio_id = "'.$parent_id.'"
        WHERE ('. TABLE_USERS . '.role_id = "4" OR '. TABLE_USERS . '.role_id = "3") AND
        '. TABLE_USERS . '.status_sl = "1" AND '. TABLE_USERS . '.user_id != '.$user_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_job_team_member($job_id, $user_id) {
        $query = 'SELECT
        a.user_id,
        a.firstname,
        a.lastname,
        a.job_title,
        a.email,
        a.status_sl,
        b.id,
        b.job_id,
        b.is_admin,
        b.is_reviewer
        FROM tyr_users as a, tyr_job_role_mapping as b
        WHERE b.job_id = :job_id and a.user_id = b.user_id and (a.status_sl = 1 or a.status_sl = 0)';
        $statement = $this->db_connection->prepare($query);
        //$statement->bindParam(':user_id', $user_id);
        $statement->bindParam(':job_id', $job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_anonymous_feedback_by_job($job_id) {
        $query = "SELECT
                    tyr_rate_review.review_id,
                    tyr_rate_review.user_id,
                    tyr_rate_review.job_id,
                    tyr_users.firstname,
                    tyr_users.lastname,
                    tyr_users.user_occupation,
                    tyr_rate_review.anonymous_feedback_message,
                    tyr_rate_review.anonymous_feedback_status,
                    tyr_media.media_type,
                    tyr_media.media_name
                  FROM
                    tyr_rate_review
                    LEFT JOIN tyr_users
                      ON tyr_rate_review.user_id = tyr_users.user_id
                    LEFT JOIN tyr_media
                      ON tyr_users.user_id = tyr_media.user_id
                      AND tyr_media.profile_image = 1
                      AND tyr_media.status_sl = 1
                  WHERE tyr_rate_review.job_id = " . $job_id . " AND tyr_rate_review.status_sl = 1 AND tyr_rate_review.anonymous_feedback_status = 0";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_reviewers($user_id) {
        $query = "SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                    username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                    state,zip,suburb,display_name,created_at,updated_at, tyr_users.status_sl,
                     tyr_countries.country, tyr_media.media_type,tyr_media.media_name FROM tyr_users
                  LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                  LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id
                  WHERE tyr_users.parent_id = " . $user_id . " and tyr_users.status_sl= 1";
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_reviewers_by_job_id($job_id) {
        $query = "select tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                    username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,
                    state,zip,suburb,display_name,created_at,updated_at, tyr_users.status_sl,
                     tyr_countries.country, tyr_media.media_type,tyr_media.media_name FROM tyr_users
                  LEFT JOIN tyr_countries ON tyr_users.country_id = tyr_countries.country_id
                  LEFT JOIN tyr_media ON tyr_users.user_id = tyr_media.user_id inner join tyr_job_role_mapping on 
                  tyr_job_role_mapping.user_id = tyr_users.user_id where tyr_job_role_mapping.job_id = ".$job_id." and tyr_users.status_sl= 1"
                . " and tyr_job_role_mapping.is_reviewer = 1";
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_applicant_for_job($selection, $rate_and_review_join, $job_id, $job_detail_status, $user_id, $hidden_user, $group_by, $order_by) {
        $query = "SELECT  " . $selection . " tyr_users.user_id,firstname,extra_title,lastname,availability,tyr_media.media_type,
                        tyr_media.media_name,tyr_countries.country,tyr_industry.industry_name,
                        tyr_cities.city,tyr_job_detail.job_detail_id from tyr_users
                        LEFT JOIN tyr_countries ON tyr_countries.country_id=tyr_users.country_id
                        LEFT JOIN tyr_industry ON tyr_industry.industry_id=tyr_users.industry_id
                LEFT JOIN tyr_media
                ON tyr_media.user_id = tyr_users.user_id
                AND tyr_media.profile_image = '1'
                AND tyr_media.status_sl = '1'
                LEFT JOIN tyr_cities ON tyr_users.city=tyr_cities.city_id
            LEFT JOIN tyr_job_detail ON tyr_users.user_id=tyr_job_detail.tyroe_id AND tyr_job_detail.job_status_id = 2
            " . $rate_and_review_join . "
            WHERE tyr_job_detail.job_id='" . $job_id . "'  AND tyr_users.publish_account = '1' AND tyr_job_detail.invitation_status = 0  AND tyr_job_detail.status_sl=" . $job_detail_status . " AND tyr_job_detail.reviewer_id = '" . $user_id . "' AND tyr_job_detail.tyroe_id NOT IN (" . $hidden_user . ") " . $group_by . " " . $order_by . " ";
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $temp_array = array();
            if (isset($row['tyroe_score'])) {
                $temp_array['tyroe_score'] = $row['tyroe_score'];
            }

            $temp_array['user_id'] = $row['user_id'];
            $temp_array['firstname'] = $row['firstname'];
            $temp_array['extra_title'] = $row['extra_title'];
            $temp_array['lastname'] = $row['lastname'];
            $temp_array['availability'] = $row['availability'];
            $temp_array['media_type'] = $row['media_type'];
            $temp_array['media_name'] = $row['media_name'];
            $temp_array['country'] = $row['country'];
            $temp_array['industry_name'] = $row['industry_name'];
            $temp_array['city'] = $row['city'];
            $temp_array['job_detail_id'] = $row['job_detail_id'];
            $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_all_candidate_by_data_query($data_query) {
        $query = $data_query;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_review_details($job_id, $candidate_id, $user_id) {
        $query = "SELECT * FROM tyr_rate_review
                             LEFT JOIN tyr_job_level ON tyr_job_level.job_level_id = tyr_rate_review.level_id
                             WHERE tyr_rate_review.job_id = '" . $job_id . "' AND tyr_rate_review.tyroe_id = '" . $candidate_id . "' AND tyr_rate_review.user_id = '" . $user_id . "' AND tyr_rate_review.status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['review_id'] = $row['review_id'];
            $return_array['user_id'] = $row['user_id'];
            $return_array['user_type'] = $row['user_type'];
            $return_array['tyroe_id'] = $row['tyroe_id'];
            $return_array['job_id'] = $row['job_id'];
            $return_array['level_id'] = $row['level_id'];
            $return_array['technical'] = $row['technical'];
            $return_array['creative'] = $row['creative'];
            $return_array['impression'] = $row['impression'];
            $return_array['review_comment'] = $row['review_comment'];
            $return_array['action_shortlist'] = $row['action_shortlist'];
            $return_array['action_interview'] = $row['action_interview'];
            $return_array['anonymous_feedback_message'] = $row['anonymous_feedback_message'];
            $return_array['anonymous_feedback_status'] = $row['anonymous_feedback_status'];
            $return_array['status_sl'] = $row['status_sl'];
        }
        return $return_array;
    }

    public function get_all_shortlisted_users($job_id, $user_id, $hidden_user, $order_by) {
        $query = "SELECT tyr_users.user_id,firstname,extra_title,lastname,availability,email,tyr_countries.country,tyr_cities.city,tyr_job_detail.job_detail_id,tyr_job_detail.invitation_status,tyr_media.media_type,
                        tyr_media.media_name,tyr_industry.industry_name from tyr_users
                        LEFT JOIN tyr_countries
                 ON tyr_countries.country_id=tyr_users.country_id
                LEFT JOIN tyr_cities ON tyr_users.city=tyr_cities.city_id
                LEFT JOIN tyr_industry ON tyr_industry.industry_id=tyr_users.industry_id
                LEFT JOIN tyr_media
                ON tyr_media.user_id = tyr_users.user_id
                AND tyr_media.profile_image = '1'
                AND tyr_media.status_sl = '1'
                LEFT JOIN tyr_job_detail ON tyr_users.user_id=tyr_job_detail.tyroe_id AND tyr_job_detail.job_status_id = 3  WHERE tyr_job_detail.job_id='" . $job_id . "'  AND tyr_users.publish_account = '1' AND tyr_job_detail.status_sl=1 AND tyr_job_detail.reviewer_id = '" . $user_id . "' AND tyr_job_detail.tyroe_id NOT IN (" . $hidden_user . ") " . $order_by . " ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $temp_array = array();
            $temp_array['user_id'] = $row['user_id'];
            $temp_array['firstname'] = $row['firstname'];
            $temp_array['lastname'] = $row['lastname'];
            $temp_array['availability'] = $row['availability'];
            $temp_array['email'] = $row['email'];
            $temp_array['media_name'] = $row['media_name'];
            $temp_array['industry_name'] = $row['industry_name'];
            $temp_array['city'] = $row['city'];
            $temp_array['job_detail_id'] = $row['job_detail_id'];
            $temp_array['country'] = $row['country'];
            $temp_array['invitation_status'] = $row['invitation_status'];
            $temp_array['extra_title'] = $row['extra_title'];
            $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_all_hidden_candidate_for_job($job_id) {
        $query = "SELECT tyr_users.user_id,role_id,parent_id,tyr_users.country_id,email,
                    username,firstname,lastname,phone,fax,cellphone,address,city,
                    state,display_name, tyr_users.status_sl,
                    tyr_countries.country, tyr_media.media_type,tyr_media.media_name FROM tyr_users
                    LEFT JOIN tyr_countries ON tyr_users.country_id=tyr_countries.country_id
                    LEFT JOIN tyr_media ON tyr_users.user_id=tyr_media.user_id
                    AND tyr_media.profile_image='1' AND tyr_media.status_sl= 1
                    INNER JOIN tyr_hidden ON tyr_users.user_id=tyr_hidden.user_id
                    and job_id='" . $job_id . "'
                    WHERE tyr_users.role_id= 2  and tyr_usersstatus_sl= 1 ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_email_data_for_tyroe($on, $job_id, $reviewer_id) {
        $query = "SELECT u.firstname, u.lastname, j.job_title, c.company_id, c.company_name, j.job_title as opening_title
                    FROM tyr_users u
                    LEFT JOIN tyr_company_detail c " . $on . "
                    LEFT JOIN tyr_jobs j ON c.studio_id = j.user_id
                    WHERE u.user_id = '" . $reviewer_id . "' AND j.job_id = '" . $job_id . "'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_email_data_for_user_job($user_id, $job_id) {
        $query = "SELECT u.firstname, u.lastname, j.job_title, c.company_id, c.company_name, j.job_title as opening_title
                                                FROM tyr_users u
                                                LEFT JOIN tyr_company_detail c ON (u.user_id = c.studio_id or u.parent_id = c.studio_id)
                                                LEFT JOIN tyr_jobs j ON c.studio_id = j.user_id
                                                WHERE u.user_id = '".$user_id."' AND j.job_id = '".$job_id."'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_reviewers_by_studio_job($user_id, $job_id) {
//        $query = "SELECT rj.reviewer_id, u.email FROM tyr_reviewer_jobs rj
//                    LEFT JOIN tyr_users u ON rj.reviewer_id = u.user_id
//                    WHERE rj.studio_id = '" . $user_id . "' AND rj.job_id = '" . $job_id . "'";
        
        $query = 'select jrm.user_id as reviewer_id, u.email from tyr_job_role_mapping jrm '
                . 'LEFT JOIN tyr_users u ON jrm.user_id = u.user_id'
                . ' where jrm.job_id = '.$job_id;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $temp_array = array();
            $temp_array['reviewer_id'] = $row['reviewer_id'];
            $temp_array['email'] = $row['email'];
            $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_job_activity_data($job_id, $user_id, $tyroe_id) {
        $query = "SELECT
                    tyr_jobs.job_title,
                    tyr_users.firstname,
                    tyr_users.lastname,
                    tyr_users.username,
                    tyr_users.email
                    FROM tyr_job_detail
                    LEFT JOIN tyr_jobs
                    ON
                    tyr_jobs.job_id = tyr_job_detail.job_id
                    LEFT JOIN tyr_users
                    ON
                    tyr_users.user_id = tyr_job_detail.tyroe_id
                    WHERE tyr_job_detail.job_id=" . $job_id . " AND tyr_job_detail.tyroe_id=" . $tyroe_id . " AND tyr_job_detail.reviewer_id = " . $user_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_tyroe_rate_review_detail($review_id, $job_id, $tyroe_id) {
        $query = "SELECT * , ((SUM(tyr_rate_review.technical)+SUM(tyr_rate_review.creative)+SUM(tyr_rate_review.impression))/3) AS review_score
            FROM tyr_rate_review
            LEFT JOIN tyr_job_level ON tyr_job_level.job_level_id = tyr_rate_review.level_id
            LEFT JOIN tyr_users ON tyr_users.user_id = tyr_rate_review.tyroe_id
            WHERE
            tyr_rate_review.user_id = '" . $review_id . "' AND tyr_rate_review.job_id = '" . $job_id .
                "' AND tyr_rate_review.tyroe_id = '" . $tyroe_id . "' AND tyr_rate_review.status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['review_id'] = $row['review_id'];
            $return_array['user_id'] = $row['user_id'];
            $return_array['user_type'] = $row['user_type'];
            $return_array['job_id'] = $row['job_id'];
            $return_array['level_id'] = $row['level_id'];
            $return_array['creative'] = $row['creative'];
            $return_array['technical'] = $row['technical'];
            $return_array['impression'] = $row['impression'];
            $return_array['review_comment'] = $row['review_comment'];
            $return_array['action_shortlist'] = $row['action_shortlist'];
            $return_array['action_interview'] = $row['action_interview'];
            $return_array['anonymous_feedback_message'] = $row['anonymous_feedback_message'];
            $return_array['anonymous_feedback_status'] = $row['anonymous_feedback_status'];
            $return_array['status_sl'] = $row['status_sl'];
            $return_array['review_score'] = $row['review_score'];
        }
        return $return_array;
    }

    public function get_all_reviewer_detail_by_studio($tyroe_id, $job_id, $user_id) {
        $query = "SELECT
                              tyr_users.user_id,
                              tyr_users.firstname,
                              tyr_users.lastname,
                              tyr_users.parent_id,
                              tyr_users.user_occupation,
                              tyr_users.job_title,
                              tyr_rate_review.job_id,
                              tyr_rate_review.action_shortlist,
                              tyr_rate_review.action_interview,
                              tyr_rate_review.level_id,
                              tyr_rate_review.review_comment,
                              tyr_job_level.level_title,
                              tyr_media.media_type,
                              tyr_media.media_name,
                              AVG(
                                (
                                  tyr_rate_review.technical + tyr_rate_review.impression + tyr_rate_review.creative
                                ) / 3
                              ) AS review_average,
                              tyr_job_role_mapping.is_admin,
                              tyr_job_role_mapping.is_reviewer
                            FROM
                              tyr_job_role_mapping
                              LEFT JOIN tyr_users
                                ON tyr_users.user_id = tyr_job_role_mapping.user_id
                              LEFT JOIN tyr_rate_review
                                ON tyr_rate_review.user_id = tyr_users.user_id
                                AND tyr_rate_review.job_id = tyr_job_role_mapping.job_id
                                AND tyr_rate_review.tyroe_id = " . $tyroe_id . "
                                AND tyr_rate_review.status_sl = 1
                                LEFT JOIN tyr_job_level
                               ON tyr_job_level.job_level_id = tyr_rate_review.level_id
                              LEFT JOIN tyr_media
                                ON tyr_media.user_id = tyr_users.user_id
                                AND tyr_media.profile_image = 1
                                AND tyr_media.status_sl = 1
                               where tyr_job_role_mapping.job_id = " . $job_id . "
                                   and tyr_job_role_mapping.is_reviewer = 1 
                            GROUP BY tyr_job_role_mapping.user_id,tyr_users.user_id,tyr_rate_review.job_id,tyr_rate_review.action_shortlist,tyr_rate_review.action_interview,tyr_rate_review.level_id,tyr_rate_review.review_comment,tyr_job_level.level_title,tyr_media.media_type,tyr_media.media_name,tyr_job_role_mapping.is_admin,tyr_job_role_mapping.is_reviewer";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_shotlist_for_pdf($job_id, $user_id, $hidden_user) {
        $query = "SELECT tyr_users.user_id,firstname,lastname,availability,email,tyr_countries.country,tyr_cities.city,tyr_job_detail.job_detail_id,tyr_job_detail.invitation_status,tyr_media.media_type,
                        tyr_media.media_name from tyr_users
                        LEFT JOIN tyr_countries ON tyr_countries.country_id=tyr_users.country_id
                LEFT JOIN tyr_cities ON tyr_users.city=tyr_cities.city_id
                LEFT JOIN tyr_media
                ON tyr_media.user_id = tyr_users.user_id
                AND tyr_media.profile_image = '1'
                AND tyr_media.status_sl = '1'
                LEFT JOIN tyr_job_detail ON tyr_users.user_id=tyr_job_detail.tyroe_id AND tyr_job_detail.job_status_id = 3  WHERE tyr_job_detail.job_id='" . $job_id . "'  AND tyr_users.publish_account = '1' AND tyr_job_detail.status_sl=1 AND tyr_job_detail.reviewer_id = '" . $user_id . "' AND tyr_job_detail.tyroe_id NOT IN (" . $hidden_user . ")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_csv_info($user_id, $job_id, $hidden_user) {
        $query = "SELECT *
        from ".TABLE_USERS."
            LEFT JOIN ".TABLE_JOBS." ON ".TABLE_JOBS.".user_id = '".$user_id."'
            AND ".TABLE_JOBS.".job_id = '".$job_id."'
            LEFT JOIN ".TABLE_JOB_DETAIL." ON ".TABLE_USERS.".user_id=".TABLE_JOB_DETAIL.
            ".tyroe_id AND ".TABLE_JOB_DETAIL.".job_status_id = 3
            WHERE
            ".TABLE_JOB_DETAIL.".job_id='".$job_id."'
            AND ".TABLE_JOB_DETAIL.".status_sl=1
            AND ".TABLE_JOB_DETAIL.".reviewer_id = '".$user_id."'
            AND ".TABLE_JOB_DETAIL.".tyroe_id NOT IN (" . $hidden_user . ")";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
             $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_csv_reviews($user_id, $job_id, $ccv_user_id) {
        $query = "SELECT
                        ". TABLE_RATE_REVIEW .".review_id,
                        ". TABLE_RATE_REVIEW .".user_id,
                        ". TABLE_RATE_REVIEW .".user_type,
                        ". TABLE_RATE_REVIEW .".tyroe_id,
                        ". TABLE_RATE_REVIEW .".job_id,
                        ". TABLE_RATE_REVIEW .".level_id,
                        ". TABLE_RATE_REVIEW .".technical,
                        ". TABLE_RATE_REVIEW .".creative,
                        ". TABLE_RATE_REVIEW .".impression,
                        ". TABLE_RATE_REVIEW .".review_comment,
                        ". TABLE_RATE_REVIEW .".action_shortlist,
                        ". TABLE_RATE_REVIEW .".action_interview,
                        ". TABLE_RATE_REVIEW .".created_timestamp,
                        ". TABLE_RATE_REVIEW .".status_sl
                        FROM
                        ". TABLE_USERS ."
                        LEFT JOIN ". TABLE_RATE_REVIEW ." ON
                        ". TABLE_RATE_REVIEW .".user_id = ". TABLE_USERS .".user_id
                            AND
                            ". TABLE_RATE_REVIEW .".job_id = ".$job_id."
                            AND
                            ". TABLE_RATE_REVIEW .".user_id = ".$ccv_user_id."
                        WHERE
                          (". TABLE_USERS .".parent_id = ".$user_id." OR ". TABLE_USERS .".user_id = ".$user_id.")
                          AND ". TABLE_USERS .".status_sl = 1  AND ".TABLE_USERS.".publish_account = '1'
                          GROUP BY ". TABLE_USERS .".user_id, tyr_rate_review.review_id";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_email_data_for_tyroe_1($on, $user_id) {
        $query = "SELECT u.job_title, c.company_id, c.company_name
                                            FROM tyr_users u
                                            LEFT JOIN tyr_company_detail c " . $on . "
                                            WHERE u.user_id = '" . $user_id . "'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_total_job_row_count($where) {
        $query = "SELECT COUNT(*) as cnt
                  FROM tyr_jobs jobs
        LEFT JOIN tyr_job_type ON jobs.job_type=tyr_job_type.job_type_id
        LEFT JOIN tyr_countries ON jobs.country_id=tyr_countries.country_id
        LEFT JOIN tyr_job_level ON jobs.job_level_id=tyr_job_level.job_level_id
        INNER JOIN tyr_users ON jobs.user_id=tyr_users.user_id
                  where jobs.status_sl='1'
                  and archived_status='0' " . $where . '  ORDER BY job_id ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_job_row_count_1($where) {
        $query = "SELECT COUNT(*) as cnt
                  FROM tyr_jobs jobs
        LEFT JOIN tyr_job_type ON jobs.job_type=tyr_job_type.job_type_id
        LEFT JOIN tyr_countries ON jobs.country_id=tyr_countries.country_id
        LEFT JOIN tyr_job_level ON jobs.job_level_id=tyr_job_level.job_level_id
        LEFT JOIN tyr_industry ON jobs.industry_id=tyr_industry.industry_id
        LEFT JOIN tyr_company_detail ON jobs.user_id = tyr_company_detail.studio_id
        INNER JOIN tyr_users ON jobs.user_id=tyr_users.user_id
                  where jobs.status_sl='1'
                  and archived_status='0' ". $where .'  group by jobs.job_id ORDER BY job_id ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_job_row_paginated($where,$order_by,$pagintaion_limit) {
        $query = "SELECT job_id,tyr_jobs.user_id,category_id,job_title,job_description,tyr_countries.country as job_location,
        tyr_job_level.level_title as job_skill,start_date,tyr_job_type.job_type,job_city,tyr_users.parent_id,tyr_users.username,
          end_date,tyr_users.email,job_quantity,tyr_jobs.created_at,tyr_jobs.updated_at,tyr_jobs.status_sl
                    FROM tyr_jobs
          LEFT JOIN tyr_job_type ON tyr_jobs.job_type=tyr_job_type.job_type_id
          LEFT JOIN tyr_countries ON tyr_jobs.country_id=tyr_countries.country_id
          LEFT JOIN tyr_job_level ON tyr_jobs.job_level_id=tyr_job_level.job_level_id
          INNER JOIN tyr_users ON tyr_jobs.user_id=tyr_users.user_id
                    where tyr_jobs.status_sl='1'
                    and archived_status='0'" . $where . ' ' . $order_by . $pagintaion_limit['limit'];

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_skills_for_job($job_id) {
        $query = "SELECT a.creative_skill_id,b.creative_skill FROM tyr_job_skills AS a LEFT JOIN tyr_creative_skills AS B ON a.creative_skill_id=b.creative_skill_id WHERE a.job_id='" . $job_id . "'";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_data_image($data, $user_id) {
        $query = "SELECT image_id, media_name, media_title, media_featured, media_description
                                    FROM tyr_media
                                    WHERE profile_image ! = 1
                                    AND status_sl= 1
                                    AND media_type = 'image'
                                    AND image_id = " . $data . "
                                    AND user_id = " . $user_id;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['image_id'] = $row['image_id'];
            $return_array['media_name'] = $row['media_name'];
            $return_array['media_title'] = $row['media_title'];
            $return_array['media_featured'] = $row['media_featured'];
            $return_array['media_description'] = $row['media_description'];
        }
        return $return_array;
    }

    public function get_user_with_profile_image($user_id) {
        $query = "SELECT u.* FROM tyr_users u LEFT JOIN tyr_media m
             ON (u.user_id=m.user_id AND m.profile_image= 1 )
             WHERE u.user_id='" . $user_id . "'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array['user_id'] = $row['user_id'];
            $return_array['role_id'] = $row['role_id'];
            $return_array['parent_id'] = $row['parent_id'];
            $return_array['country_id'] = $row['country_id'];
            $return_array['tyroe_level'] = $row['tyroe_level'];
            $return_array['industry_id'] = $row['industry_id'];
            $return_array['experienceyear_id'] = $row['experienceyear_id'];
            $return_array['job_level_id'] = $row['job_level_id'];
            $return_array['profile_url'] = $row['profile_url'];
            $return_array['email'] = $row['email'];
            $return_array['username'] = $row['username'];
            $return_array['password'] = $row['password'];
            $return_array['password_len'] = $row['password_len'];
            $return_array['firstname'] = $row['firstname'];
            $return_array['lastname'] = $row['lastname'];
            $return_array['phone'] = $row['phone'];
            $return_array['fax'] = $row['fax'];
            $return_array['cellphone'] = $row['cellphone'];
            $return_array['address'] = $row['address'];
            $return_array['state'] = $row['city'];
            $return_array['state'] = $row['state'];
            $return_array['zip'] = $row['zip'];
            $return_array['user_web'] = $row['user_web'];
            $return_array['user_occupation'] = $row['user_occupation'];
            $return_array['user_experience'] = $row['user_experience'];
            $return_array['suburb'] = $row['suburb'];
            $return_array['display_name'] = $row['display_name'];
            $return_array['job_title'] = $row['job_title'];
            $return_array['availability'] = $row['availability'];
            $return_array['publish_account'] = $row['publish_account'];
            $return_array['status_sl'] = $row['status_sl'];
            if (isset($row['media_name'])) {
                $return_array['media_name'] = $row['media_name'];
            }
        }
        return $return_array;
    }

    public function get_search_listing_studio_data($role_id, $where, $order_by, $pagintaion_limit) {
        $query = "SELECT st.user_id,st.email,st.created_timestamp,i.industry_name,stat.status,c.company_name,cont.country,cit.city,
                              tpv.visit_time FROM ".TABLE_USERS." st LEFT JOIN ".TABLE_COMPANY." c ON st.user_id = c.studio_id
                              LEFT JOIN ".TABLE_STATUSES." stat ON st.status_sl = stat.status_id
                              LEFT JOIN ".TABLE_COUNTRIES." cont ON c.company_country = cont.country_id
                              LEFT JOIN " . TABLE_CITIES . " cit ON c.company_city = cit.city_id
                              LEFT JOIN ".TABLE_INDUSTRY." i ON st.industry_id = i.industry_id
                              LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv ON st.user_id = tpv.user_id
                              WHERE st.role_id = :role_id AND st.parent_id = 0 and st.status_sl != -1 and st.email != 'Remove' ".$where." GROUP BY st.user_id,i.industry_name,stat.status,c.company_name,cont.country,tpv.visit_time,cit.city " . $order_by . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
        public function get_search_listing_users_data($role_id, $where, $order_by, $pagintaion_limit) {
            $query = "SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_timestamp,u.status_sl,u.publish_account,
                                            f.featured_status,tpv.visit_time FROM ".TABLE_USERS." u
                                            LEFT JOIN ".TABLE_FEATURED_TYROE." f ON u.user_id = f.featured_tyroe_id
                                            LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv
                                            ON u.user_id = tpv.user_id
                                            WHERE  status_sl=1 AND u.role_id != :role_id ".$where."
                                            GROUP BY u.user_id, f.featured_status, tpv.visit_time ". $order_by . $pagintaion_limit['limit'];
        
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_featured_details($user_id) {
        $query = "SELECT u.user_id,u.firstname,u.lastname,u.role_id,u.email,u.created_at,u.status_sl,u.publish_account,
                                            f.featured_status,tpv.visit_time FROM tyr_users u
                                            LEFT JOIN tyr_featured_tyroes f ON u.user_id = f.featured_tyroe_id
                                            LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  tyr_pageviews GROUP BY user_id) tpv
                                            ON u.user_id = tpv.user_id
                                            WHERE  u.user_id='" . $user_id . "'";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_studio_details_record($user_id) {
        $query = "SELECT st.user_id,st.email,st.created_timestamp,i.industry_name,stat.status,c.company_name,cont.country,
                              tpv.visit_time FROM ".TABLE_USERS." st LEFT JOIN ".TABLE_COMPANY." c ON st.user_id = c.studio_id
                              LEFT JOIN ".TABLE_STATUSES." stat ON st.status_sl = stat.status_id
                              LEFT JOIN ".TABLE_COUNTRIES." cont ON c.company_country = cont.country_id
                              LEFT JOIN ".TABLE_INDUSTRY." i ON st.industry_id = i.industry_id
                              LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv ON st.user_id = tpv.user_id
                              WHERE st.user_id='".$user_id."'";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_subscription_paginated($where, $order_by, $pagintaion_limit) {
        $query = 'SELECT subs.status_sl,subs.subscription_card_num,subs.subscription_card_type,subs.subscription_ccv,
        subs.subscription_coupon,subs.subscription_expiry_month,subs.subscription_expiry_year,subs.subscription_id,subs.subscription_name,
        subs.subscription_payment_type,subs.subscription_type,subs.subscription_use_coupon,users.username
        FROM tyr_subscription subs INNER JOIN tyr_users users ON subs.tyroe_id= users.user_id
        WHERE 1=1 ' . $where . ' ' . $order_by . $pagintaion_limit['limit'];

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_total_subscription_id_count($where) {
        $query = 'SELECT COUNT(*) AS cnt FROM tyr_subscription subs INNER JOIN tyr_users users ON subs.tyroe_id = users.user_id WHERE 1=1 ' . $where . ' ORDER BY subscription_id ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_studio_coupons_paginated($where, $order_by, $pagintaion_limit) {
        $query = 'SELECT coup.*,IF(users.username != "",users.username,"") AS username
            FROM tyr_coupons_allot_users coup
            LEFT JOIN tyr_users users ON users.user_id =coup.user_id
            WHERE 1=1 AND coup.status_sl = 1 ' . $where . ' ' . $order_by . $pagintaion_limit['limit'];

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_total_row_count_for_cupons($where) {
        $query = 'SELECT count(*) AS cnt
                                    FROM tyr_coupons_allot_users coup
                                    LEFT JOIN tyr_users users ON users.user_id =coup.user_id
                                    WHERE 1=1 AND coup.status_sl = 1 ' . $where;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_user_skills_for_profile($user_id, $status) {
        $query = "SELECT sk.*, csk.creative_skill as skill_name
                      FROM " . TABLE_CREATIVE_SKILLS . " csk
                      LEFT JOIN " . TABLE_SKILLS . " sk  ON (sk.skill = csk.creative_skill_id)
                      WHERE sk.user_id = :user_id and sk.status_sl = :status_sl ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->bindParam(':status_sl', $status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_profile($user_id) {
        $query = "SELECT tyr_users.user_id,role_id,tyroe_level,parent_id,tyr_users.country_id,tyr_countries.country,profile_url,email,
                 username,password,password_len,firstname,lastname,phone,fax,cellphone,address,city,user_occupation,user_biography,user_web,user_experience,
                 state,zip,suburb,display_name,created_timestamp,modified_timestamp, tyr_users.status_sl,
                  tyr_countries.country, tyr_media.media_type,tyr_media.media_name,
                    tyr_media.image_id FROM tyr_users
               LEFT JOIN tyr_countries ON tyr_users.country_id=tyr_countries.country_id
               LEFT JOIN tyr_media ON tyr_users.user_id=tyr_media.user_id AND tyr_media.profile_image = '1' AND tyr_media.status_sl='1'
               WHERE tyr_users.user_id = '" . $user_id . "'  ORDER BY image_id DESC LIMIT 1";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $temp = '';
        if (($row = $statement->fetch()) != false) {
            $temp = $row;
        }
        return $temp;
    }

    public function get_total_row_count_activity_stream($where) {
        $query = "SELECT COUNT(j_act.performer_id) as cnt
                    FROM tyr_job_activity j_act
                    INNER JOIN  tyr_users users ON j_act.performer_id=users.user_id
                    INNER JOIN  tyr_activity_type act_type ON act_type.activity_type_id=j_act.activity_type
                     LEFT JOIN  tyr_jobs job ON j_act.job_id=job.job_id
                    WHERE j_act.status_sl = 1 AND j_act." . $where . " group by j_act.performer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_featured_user_row_count($featured_join,$hidden_user) {
        $query = "SELECT COUNT(*)AS cnt FROM " . TABLE_USERS . " " . $featured_join . "
                      LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                      LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
                      AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
                      WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND "
                       . TABLE_USERS . ".publish_account = '1' AND tyr_users.user_id NOT IN (" . $hidden_user . ")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_featured_users_detail_page($featured_join,$hidden_user) {
        $query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
              username,firstname,lastname,address,city,availability,
              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . " " . $featured_join . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' AND tyr_users.user_id NOT IN (" . $hidden_user . ") ORDER BY user_id DESC ";

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_featured_tyroe_detail_paginated($featured_join,$hidden_user,$pagintaion_limit) {
        $query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
              username,firstname,lastname,address,availability,extra_title,
              state,zip," . TABLE_CITIES . ".city," . TABLE_INDUSTRY . ".industry_name," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . " " . $featured_join . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
              LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=" . TABLE_USERS . ".industry_id
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' "
                . "AND tyr_users.user_id NOT IN (" . $hidden_user . ") and (availability != 0::smallint) ORDER BY user_id DESC " . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_tyroe_details_without_limit($query) {

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_tyroe_details_with_limit($query) {
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_all_experience($user_id) {
        $query = "SELECT exp_id, user_id, company_name,job_title,job_location,IF(job_start='',YEAR(CURDATE()) ,job_start) AS job_start,
        IF(job_end='',YEAR(CURDATE()) ,job_end) AS job_end, job_description, job_url, job_country, job_city,
                  current_job, created_at,updated_at,status_sl  FROM tyr_experience  WHERE user_id='" . $user_id . "' and status_sl= 1";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_skill_by_users($user_id) {
        $query = "SELECT sk.*, csk.creative_skill as skill_name
            FROM tyr_creative_skills csk
            LEFT JOIN tyr_skills sk  ON (sk.skill = csk.creative_skill_id)
            WHERE sk.user_id= :user_id and sk.status_sl= 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_users_speciality($user_id) {
        $query = "SELECT sk.*, csk.creative_skill as skill_name
            FROM tyr_skills sk
            LEFT JOIN tyr_creative_skills csk ON (sk.skill = csk.creative_skill_id)
            WHERE sk.user_id='" . $user_id . "' AND sk.status_sl= 1 AND sk.speciality = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_job_details_for_public_page($job_id, $status) {
        $query = "SELECT job_id,user_id,category_id,job_title,job_description,job_city,team_moderation," . TABLE_CITIES . ".city,".TABLE_JOB_TYPE.".job_type_id,".TABLE_JOB_TYPE.".job_type,".TABLE_COUNTRIES.".country
                AS job_location,".TABLE_COUNTRIES.".country_id,".TABLE_JOB_LEVEL.".job_level_id,".TABLE_JOB_LEVEL.".level_title AS job_skill,start_date,end_date,".TABLE_INDUSTRY.".industry_name,".TABLE_INDUSTRY.".industry_id,job_quantity," . TABLE_JOBS . ".created_timestamp,"
                . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl FROM " . TABLE_JOBS . "
                LEFT JOIN " . TABLE_COUNTRIES . " ON "
                . TABLE_JOBS . ".country_id = " . TABLE_COUNTRIES . ".country_id
                LEFT JOIN " . TABLE_CITIES . " ON "
                                . TABLE_JOBS . ".job_city = " . TABLE_CITIES . ".city_id
                LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id = "
                . TABLE_JOB_LEVEL . ".job_level_id
                LEFT JOIN ".TABLE_JOB_TYPE." ON " . TABLE_JOBS . ".job_type=".TABLE_JOB_TYPE.".job_type_id
                LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id= ".TABLE_INDUSTRY.".industry_id WHERE job_id = :job_id"
                . " AND " . TABLE_JOBS . ".status_sl = :status ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $job_id);
        $statement->bindParam(':status', $status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_user_profile_score($user_id) {
        $return_array = array();
        if($user_id == '')
            return $return_array;
        $city = 0;
        $query = "SELECT ( (CASE WHEN tyr_users.status_sl= 1 THEN 4  ELSE 0 END )+
                    (CASE WHEN availability IS NOT NULL AND tyr_users.availability!= 0 THEN 4  ELSE 0 END )  +
                    (CASE WHEN country_id IS NOT NULL AND tyr_users.country_id!= 0 THEN 4  ELSE 0 END )  +
                    (CASE WHEN job_level_id IS NOT NULL AND tyr_users.job_level_id!= 0  THEN 4  ELSE 0 END ) +
                    (CASE WHEN industry_id IS NOT NULL AND tyr_users.industry_id!= 0  THEN 4  ELSE 0 END )  +
                    (CASE WHEN experienceyear_id IS NOT NULL AND tyr_users.experienceyear_id!= 0  THEN 4  ELSE 0 END )  +
                    (CASE WHEN extra_title > ''  THEN 4  ELSE 0 END )+
                    (CASE WHEN user_biography IS NOT NULL AND tyr_users.user_biography!='Type here to add a small biography about yourself. Use this chance to grab the attention of recruitment teams by outlining your skills, career aspirations and anything else of interest'  THEN 4  ELSE 0 END )   +
                    (CASE WHEN city IS NOT NULL AND tyr_users.city != 0 THEN 4  ELSE 0 END ) +
                    (CASE when sum(tyr_social_link.status_sl)>0 then 4 else 0 end )) AS cnt
                    FROM tyr_users
                    LEFT JOIN tyr_social_link ON tyr_social_link.user_id=tyr_users.user_id
                    WHERE tyr_users.user_id = :user_id group by tyr_users.status_sl, tyr_users.availability, tyr_users.country_id, tyr_users.job_level_id, 
                    tyr_users.industry_id, tyr_users.experienceyear_id, extra_title, tyr_users.user_biography, tyr_users.city, tyr_social_link.status_sl";

        $statement = $this->db_connection->prepare($query);
        //$statement->bindParam(':city', $city);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_user_resume_score($user_id) {
        $return_array = array();
        if($user_id == '')
            return $return_array;
        $status_sl = 1;
        $query = "SELECT SUM(
                    EXISTS(SELECT exp.exp_id FROM tyr_experience exp WHERE exp.user_id = :user_id AND exp.status_sl = :status_sl)::integer
                    +
                    EXISTS(SELECT edu.education_id FROM tyr_education edu WHERE edu.user_id = :user_id AND edu.status_sl = :status_sl)::integer
                    +
                    EXISTS(SELECT awards.award_id FROM tyr_awards awards WHERE awards.user_id = :user_id AND awards.status_sl = :status_sl)::integer
                    +
                    EXISTS(SELECT skills.skill_id FROM tyr_skills skills WHERE skills.user_id = :user_id AND skills.status_sl = :status_sl)::integer
                ) AS cnt";

        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->bindParam(':status_sl', $status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_user_portfolio_score($user_id) {
        $return_array = array();
        if($user_id == '')
            return $return_array;
        $status_sl = 1;
        $query = "SELECT SUM(
                    EXISTS(SELECT media.image_id FROM tyr_media media WHERE media.user_id = :user_id AND media.status_sl = :status_sl AND media.profile_image ='0' AND ( media.media_featured='1'))::integer
                    +
                    EXISTS(SELECT media.image_id FROM tyr_media media WHERE media.user_id = :user_id AND media.status_sl = :status_sl AND media.profile_image ='0' AND (media.media_type='video'))::integer
                    +
                    EXISTS(SELECT media.image_id FROM tyr_media media WHERE media.user_id = :user_id AND media.status_sl = :status_sl AND media.profile_image ='0' AND media.media_type='image' AND media.media_featured='0')::integer
                    ) AS cnt";

        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->bindParam(':status_sl', $status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_user_detail_info($user_id) {
        $return_array = array();
        if($user_id == '')
            return $return_array;
        $query = "SELECT job_level.level_title,exp_years.experienceyear,industry.industry_name,users.user_id,users.city AS city_id,users.job_level_id,users.extra_title,users.job_title,
                users.industry_id,users.experienceyear_id,role_id,tyroe_level,parent_id,users.country_id,country.country,profile_url,email,
                 username,password,password_len,firstname,lastname,phone,fax,cellphone,address,cities.city,user_occupation,user_biography,user_web,user_experience,
                 state,zip,suburb,display_name,availability,publish_account,created_timestamp,modified_timestamp, users.status_sl,
                  country.country, media.media_type,media.media_name,
                    media.image_id FROM " . TABLE_USERS . " users
               LEFT JOIN " . TABLE_COUNTRIES . " country ON users.country_id=country.country_id
               LEFT JOIN " . TABLE_MEDIA . " media ON users.user_id=media.user_id AND media.profile_image = '1' AND media.status_sl='1'
               LEFT JOIN " . TABLE_CITIES . " cities ON users.city=cities.city_id
               LEFT JOIN " . TABLE_JOB_LEVEL . " job_level ON users.job_level_id=job_level.job_level_id
               LEFT JOIN " . TABLE_INDUSTRY . " industry ON users.industry_id=industry.industry_id
               LEFT JOIN " . TABLE_EXPERIENCE_YEARS . " exp_years ON users.experienceyear_id=exp_years.experienceyear_id
               WHERE users.user_id= :user_id  ORDER BY image_id DESC LIMIT 1 OFFSET 0";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_users_recommendation_sentlist($user_id) {
        $query = "SELECT rec.recommend_id,rec.recommend_email,array_to_string(array_agg(rechistory.created_timestamp),',') AS created_timestamp
                        FROM tyr_recommendation rec LEFT JOIN tyr_recommendemail_history rechistory
                        ON rec.recommend_id=rechistory.recommend_id
                        WHERE rec.status_sl= 1 AND rec.user_id= :user_id AND rec.recommend_status= 0
                        GROUP BY rec.recommend_id, rechistory.created_at 
                        ORDER BY rechistory.created_at DESC
                        ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_user_all_skill_count($user_id) {
        $query = "SELECT COUNT(*) as cnt
                    FROM tyr_creative_skills csk
                    LEFT JOIN tyr_skills sk  ON (sk.skill = csk.creative_skill_id)
                    WHERE sk.user_id='" . $user_id . "' and sk.status_sl= 1 ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_latest_work_media($user_id, $column1_start) {
        $query = "SELECT *
                    FROM tyr_media WHERE  user_id = '" . $user_id . "'
                    AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                    AND media_imagesection='1'
                    ORDER BY media_sortorder ASC
                    OFFSET  " . $column1_start . " LIMIT 6
                    ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_latest_work_media_with_limit($user_id, $column2_start, $total_limit) {
        $query = "SELECT * FROM tyr_media WHERE  user_id = '" . $user_id . "'
                                                AND status_sl = '1'  AND media_type = 'image' AND media_featured='0'
                                                AND media_imagesection='2'
                                                ORDER BY media_sortorder ASC
                                                OFFSET " . $column2_start . " LIMIT " . $total_limit . "
                                                ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function check_already_invites($invite_email, $job_id, $studio_id) {
        $query = 'SELECT tyr_invite_reviewer.invite_email FROM tyr_invite_reviewer
                RIGHT JOIN tyr_users ON
                tyr_users.email = tyr_invite_reviewer.invite_email
                WHERE tyr_invite_reviewer.invite_email =  :invite_email AND
                tyr_invite_reviewer.invite_job_id =  :invite_job_id AND tyr_users.status_sl = 1 AND
                tyr_invite_reviewer.invite_studio_id = :invite_studio_id';

        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_email', $invite_email);
        $statement->bindParam(':invite_job_id', $job_id);
        $statement->bindParam(':invite_studio_id', $studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function check_already_invites_1($invite_email, $job_id, $studio_id) {
        $query = 'SELECT tyr_invite_reviewer.invite_email FROM tyr_invite_reviewer
                RIGHT JOIN tyr_users ON
                tyr_users.email = tyr_invite_reviewer.invite_email
                WHERE tyr_invite_reviewer.invite_email =  :invite_email AND
                tyr_invite_reviewer.invite_job_id =  :invite_job_id AND
                tyr_invite_reviewer.invite_studio_id = :invite_studio_id';

        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_email', $invite_email);
        $statement->bindParam(':invite_job_id', $job_id);
        $statement->bindParam(':invite_studio_id', $studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_total_row_count_jobopeining($user_id, $where, $where_job) {
//        $query = "SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . " job
//        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
//        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
//        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
//        LEFT JOIN " . TABLE_JOB_DETAIL . " job_detail ON job_detail.job_id=job.job_id AND (job_detail.tyroe_id = ''"
//                . " OR job_detail.tyroe_id = '" . $user_id . "')
//        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
//                  WHERE job.status_sl='1'
//                  AND archived_status='0' " . $where . ' ' . $where_job;
        
        $query = "SELECT COUNT(*) as cnt FROM " . TABLE_JOBS . " job
        LEFT JOIN " . TABLE_JOB_TYPE . " job_type ON job.job_type=job_type.job_type_id
        LEFT JOIN " . TABLE_COUNTRIES . " country ON job.country_id=country.country_id
        LEFT JOIN " . TABLE_JOB_LEVEL . " job_lev ON job.job_level_id=job_lev.job_level_id
        LEFT JOIN " . TABLE_JOB_DETAIL . " job_detail ON job_detail.job_id=job.job_id
        INNER JOIN " . TABLE_USERS . " users ON job.user_id=users.user_id
                  WHERE job.status_sl='1'
                  AND archived_status='0' " . $where . ' ' . $where_job;
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_total_row_count_favourite_profile($user_id, $hidden_user) {
        $query = "SELECT COUNT(*)AS cnt FROM " . TABLE_USERS . "
                      LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
                      RIGHT JOIN " . TABLE_FAVOURITE_PROFILE . " ON " . TABLE_FAVOURITE_PROFILE . ".user_id = " . TABLE_USERS . ".user_id
                      AND " . TABLE_FAVOURITE_PROFILE . ".studio_id = '" . $user_id . "' AND " . TABLE_FAVOURITE_PROFILE . ".status_sl = 1
                      LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
                      AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
                      WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' AND " . TABLE_USERS . ".user_id NOT IN (" . $hidden_user . ")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_favourite_profile($user_id, $hidden_user) {
        $query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
              username,firstname,lastname,address,availability,extra_title,
              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_CITIES . ".city," . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
              RIGHT JOIN " . TABLE_FAVOURITE_PROFILE . " ON " . TABLE_FAVOURITE_PROFILE . ".user_id = " . TABLE_USERS . ".user_id
              AND " . TABLE_FAVOURITE_PROFILE . ".studio_id = '" . $user_id . "' AND " . TABLE_FAVOURITE_PROFILE . ".status_sl = 1
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND " . TABLE_USERS . ".publish_account = '1' AND " . TABLE_USERS . ".user_id NOT IN (" . $hidden_user . ") ORDER BY " . TABLE_FAVOURITE_PROFILE . ".user_id DESC ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    //STAR-CHANGES
    public function get_all_user_skill_with_limit($candidate_id) {
        $query = 'SELECT skill_id, user_id, tyr_creative_skills.creative_skill as skill,status_sl
                           FROM tyr_skills LEFT JOIN
                            tyr_creative_skills ON tyr_creative_skills.creative_skill_id = tyr_skills.skill WHERE user_id = ' . $candidate_id . ''
                . ' and status_sl= 1 OFFSET 3 LIMIT 2';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_total_featured_count() {
        $query = "SELECT COUNT(u.user_id) as cnt FROM " . TABLE_USERS . " u LEFT JOIN " . TABLE_COUNTRIES . " c
            ON c.country_id = u.country_id LEFT JOIN " . TABLE_FEATURED_TYROE . " f ON u.user_id=f.featured_tyroe_id WHERE u.role_id IN (2, 5)
                AND u.status_sl = '1' AND u.publish_account = '1' AND
                f.featured_status= 0 group by u.user_id ORDER BY user_id DESC ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_featured_users_details_with_limit($pagintaion_limit) {
        $query = "SELECT u.profile_url, u.user_id,u.firstname,u.lastname,u.extra_title,c.country,ci.city,f.featured_status," . TABLE_INDUSTRY . ".industry_name FROM " . TABLE_USERS . " u LEFT JOIN " . TABLE_COUNTRIES . " c
            ON c.country_id = u.country_id LEFT JOIN " . TABLE_FEATURED_TYROE . " f ON u.user_id=f.featured_tyroe_id LEFT JOIN " . TABLE_CITIES . " ci ON u.city = ci.city_id LEFT JOIN " . TABLE_INDUSTRY . " ON " . TABLE_INDUSTRY . ".industry_id=u.industry_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1' AND u.publish_account = '1' AND
            f.featured_status= 0 ORDER BY user_id DESC " . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_all_featured_users_details() {
        $query = "SELECT u.user_id as tyr_rev_id  FROM " . TABLE_USERS . " u LEFT JOIN " . TABLE_COUNTRIES . " c
            ON c.country_id = u.country_id LEFT JOIN " . TABLE_FEATURED_TYROE . " f ON u.user_id=f.featured_tyroe_id WHERE u.role_id IN (2, 5) AND u.status_sl = '1' AND u.publish_account = '1' AND
            f.featured_status= 0 ORDER BY user_id DESC ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_count_by_feedback($user_id, $where_feedback) {
        $query = "SELECT COUNT(*) as cnt
            FROM " . TABLE_RATE_REVIEW . " review
             INNER JOIN " . TABLE_USERS . " users ON users.user_id=review.user_id
             LEFT JOIN " . TABLE_JOBS . " jobs ON jobs.job_id=review.job_id
             LEFT JOIN " . TABLE_MEDIA . " media ON media.user_id=review.user_id AND media.profile_image= 1 AND media.status_sl= 1
             LEFT JOIN " . TABLE_COUNTRIES . " country ON country.country_id=jobs.country_id
             LEFT JOIN " . TABLE_JOB_LEVEL . " review_level ON review.level_id = review_level.job_level_id
             WHERE review.status_sl= 1 and review.moderated != 0  AND review.tyroe_id= " . $user_id . " " . $where_feedback . " group by review_id ORDER BY review_id DESC";
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $cnt = 0;
        if (($row = $statement->fetch()) != FALSE) {
            $cnt++;
        }
        $return_array = array('cnt' => $cnt);
        return $return_array;
    }

    public function get_all_feedback($user_id, $where_feedback, $order_by, $pagintaion_limit) {
        $query = "SELECT review.user_id as company_id,review.review_id,review.technical,review.creative,review.impression,review.moderated,
                     ROUND((review.technical+review.creative+review.impression)/3)as profile_score,review.review_comment,review.created_timestamp,
                     users.username,users.firstname,users.lastname,jobs.job_title,jobs.user_id,media.media_name,country.country,review.job_id,
                     review.anonymous_feedback_message,review.anonymous_feedback_status,review_level.level_title,city.city as job_city,review.level_id,jobs.start_date,jobs.end_date
                     FROM " . TABLE_RATE_REVIEW . " review
                     INNER JOIN " . TABLE_USERS . " users ON users.user_id=review.user_id
                     LEFT JOIN " . TABLE_JOBS . " jobs ON jobs.job_id=review.job_id
                     LEFT JOIN " . TABLE_MEDIA . " media ON media.user_id=review.user_id AND media.profile_image= 1 AND media.status_sl= 1
                     LEFT JOIN " . TABLE_COUNTRIES . " country ON country.country_id=jobs.country_id
                     LEFT JOIN " . TABLE_CITIES . " city ON city.city_id=jobs.job_city
                     LEFT JOIN " . TABLE_JOB_LEVEL . " review_level ON review.level_id = review_level.job_level_id
                     WHERE review.status_sl= 1 and review.moderated != 0 and review.moderated != 0 AND review.tyroe_id = " . $user_id . $where_feedback . $order_by . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_count_for_filter_feedback($user_id, $where_feedback, $order_by, $pagintaion_limit) {
        $query = "SELECT COUNT(*) as cnt FROM " . TABLE_RATE_REVIEW . " review
                  INNER JOIN " . TABLE_USERS . " users ON users.user_id=review.user_id
                  LEFT JOIN " . TABLE_JOBS . " jobs ON jobs.job_id=review.job_id
                  LEFT JOIN " . TABLE_MEDIA . " media ON media.user_id=review.user_id AND media.profile_image='1' AND media.status_sl='1'
                  LEFT JOIN " . TABLE_COUNTRIES . " country ON country.country_id=jobs.country_id
                  LEFT JOIN " . TABLE_JOB_LEVEL . " review_level ON review.level_id = review_level.job_level_id
                  WHERE review.status_sl='1' and review.moderated != 0 AND review.tyroe_id=" . $user_id . $where_feedback . $order_by . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_jobs_details_not_for_hidden($hidden_user,$where,$order_by,$pagintaion_limit){
        $query = "SELECT job_id," . TABLE_JOBS . ".user_id,category_id," . TABLE_JOBS . ".job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
        " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city," . TABLE_USERS . ".parent_id," . TABLE_USERS . ".username,".TABLE_COMPANY.".company_name,
          end_date,".TABLE_USERS.".email,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl,"
        .TABLE_INDUSTRY.".industry_name,
         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='3' AND invitation_status = 1 AND tyroe_id::character varying NOT IN(".$hidden_user.")) AS shortlisted,
         (SELECT COUNT(*) FROM ".TABLE_USERS." WHERE parent_id=".TABLE_JOBS.".user_id AND status_sl=1) AS team,
         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='1' AND invitation_status != -1  AND  tyroe_id::character varying NOT IN (" . $hidden_user . ")) AS candidate,
         (SELECT COUNT(*) FROM ".TABLE_JOB_DETAIL." WHERE job_id=".TABLE_JOBS.".job_id AND job_status_id='2' AND invitation_status = 0 AND  tyroe_id::character varying NOT IN (" . $hidden_user . ")) AS applicant
         FROM " . TABLE_JOBS . "
        LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
        LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
        LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
        LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
        LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
        INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
          where " . TABLE_JOBS . ".status_sl='1'
          and archived_status='0'" . $where. ' ' . $order_by.$pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_total_gallary_records_count(){
        $query = "select count(*) as total_gallery_records from ".TABLE_MEDIA . ' tm where tm.status_sl = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_job_details_by_job_id_1($job_id) {
        $query = "SELECT job_id," . TABLE_JOBS . ".user_id,category_id," . TABLE_JOBS . ".job_title,job_description," . TABLE_COUNTRIES . ".country as job_location,
                    " . TABLE_JOB_LEVEL . ".level_title as job_skill,start_date," . TABLE_JOB_TYPE . ".job_type,job_city," . TABLE_USERS . ".parent_id," . TABLE_USERS . ".username,".TABLE_COMPANY.".company_name,
                      end_date,".TABLE_USERS.".email,job_quantity," . TABLE_JOBS . ".created_timestamp," . TABLE_JOBS . ".modified_timestamp," . TABLE_JOBS . ".status_sl,"
                    .TABLE_INDUSTRY.".industry_name FROM " . TABLE_JOBS . "
            LEFT JOIN " . TABLE_JOB_TYPE . " ON " . TABLE_JOBS . ".job_type=" . TABLE_JOB_TYPE . ".job_type_id
            LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_JOBS . ".country_id=" . TABLE_COUNTRIES . ".country_id
            LEFT JOIN " . TABLE_JOB_LEVEL . " ON " . TABLE_JOBS . ".job_level_id=" . TABLE_JOB_LEVEL . ".job_level_id
            LEFT JOIN ".TABLE_INDUSTRY." ON ".TABLE_JOBS.".industry_id=".TABLE_INDUSTRY.".industry_id
            LEFT JOIN ".TABLE_COMPANY." ON ".TABLE_JOBS.".user_id = ".TABLE_COMPANY.".studio_id
            INNER JOIN " . TABLE_USERS . " ON " . TABLE_JOBS . ".user_id=" . TABLE_USERS . ".user_id
                      where job_id='".$job_id."'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != false){
           $return_array = $row;
        }
        return $return_array;
    }
    

    public function get_gallery_records_pagination($limit, $offset){
        $query = 'select tu.profile_url AS gallery_user_profile,
        tm.image_id AS gallery_id,
        tm.user_id AS gallery_user_id,
        tm.media_type AS gallery_type,
        tm.video_type AS gallery_video_type,
        tm.media_name AS gallery_src,
        tm.media_original_name AS original_src,
        tm.media_url AS gallery_url,
        tm.media_title AS gallery_title,
        tm.created AS gallery_uploaded_time,
        tm.status_sl AS gallery_status from '.TABLE_MEDIA . ' AS tm LEFT OUTER join '.TABLE_USERS . ' AS tu on '
                . 'tu.user_id=tm.user_id where tm.status_sl = 1 order by tm.image_id DESC LIMIT '.$limit.' offset '.$offset;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_search_listing_studio_by_studio_role($where, $order_by, $pagintaion_limit) {
        $query = "SELECT st.user_id AS id,co.company_name,st.firstname AS admin,stat.status,st.email,c.country AS country,i.industry_name AS industry,FROM_UNIXTIME(st.created_timestamp,'%m-%d-%Y')
            AS signed_up,(SELECT COUNT(user_id) FROM " . TABLE_USERS . " WHERE parent_id = st.user_id) AS reviewers,(SELECT COUNT(job_id) FROM " . TABLE_JOBS . " WHERE
            user_id = st.user_id) AS openings FROM " . TABLE_USERS . " st LEFT JOIN " . TABLE_STATUSES . " stat ON st.status_sl = stat.status_id LEFT JOIN " . TABLE_INDUSTRY . " i
             ON st.industry_id=i.industry_id LEFT JOIN " . TABLE_COUNTRIES . " c ON st.country_id = c.country_id LEFT JOIN " . TABLE_COMPANY . " co ON st.user_id = co.studio_id
             WHERE role_id ='" . STUDIO_ROLE . "' " . $where . ' AND st.status_sl=1 ' . $order_by . $pagintaion_limit['limit'];
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_total_row_count_filter_team($user_id,$parent_id,$fillter_text) {
        $query = "SELECT COUNT(*)AS cnt FROM " . TABLE_USERS . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".role_id='4' AND " . TABLE_USERS . ".status_sl!='-1' AND (" .TABLE_USERS. ".parent_id = '".$parent_id."' OR " .TABLE_USERS. ".user_id = :user_id) 
              AND ( CONCAT(" . TABLE_USERS . ".firstname , ' ', " . TABLE_USERS . ".lastname) LIKE '%".$fillter_text."%'
                    OR " . TABLE_USERS . ".username LIKE '%".$fillter_text."%'
                   )
              ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_reviewer_filter_team($user_id,$parent_id,$fillter_text,$orderby,$pagintaion_limit) {
        $query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id,created_timestamp," . TABLE_USERS . ".status_sl," . TABLE_USERS . ".country_id,email,
              username,firstname,lastname,address,city,availability,job_title,
              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_MEDIA . ".media_type," . TABLE_MEDIA . ".media_name FROM " . TABLE_USERS . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".status_sl !='-1' AND (" .TABLE_USERS. ".parent_id = '".$parent_id."' OR " .TABLE_USERS. ".user_id = :user_id ) 
              AND ( CONCAT(" . TABLE_USERS . ".firstname , ' ', " . TABLE_USERS . ".lastname) LIKE '%".$fillter_text."%'
                    OR " . TABLE_USERS . ".username LIKE '%".$fillter_text."%'
                   )".$orderby." ". $pagintaion_limit['limit'];
        
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) {
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    
    public function get_tag_user_ids($search_input){
        $query = "SELECT
                  array_to_string(array_agg(
                    " . TABLE_MEDIA . ".user_id ORDER BY " . TABLE_MEDIA . ".user_id ASC), ',') AS tags_user_id
                FROM
                  " . TABLE_GALLERY_TAGS . "
                  LEFT JOIN " . TABLE_GALLERY_TAGS_REL . " ON
                  " . TABLE_GALLERY_TAGS_REL . ".gallerytag_name_id =  " . TABLE_GALLERY_TAGS . ".tag_id
                  LEFT JOIN " . TABLE_MEDIA . " ON
                  " . TABLE_MEDIA . ".image_id = " . TABLE_GALLERY_TAGS_REL . ".media_image_id
                WHERE lower(" . TABLE_GALLERY_TAGS . ".tag_name) LIKE '%" . $search_input . "%'
                AND " . TABLE_GALLERY_TAGS_REL . ".status=1
                ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != false){
           $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_openings_by_user_reviewer($user_id,$reviewer_id) {
        $query = "SELECT " . TABLE_JOBS . ".job_title FROM tyr_job_role_mapping
                                LEFT JOIN " . TABLE_JOBS . "
                                ON " . TABLE_JOBS . ".job_id = tyr_job_role_mapping.job_id
                                AND " . TABLE_JOBS . ".status_sl = 1 AND " . TABLE_JOBS . ".archived_status = 0
                                WHERE " . TABLE_JOBS . ".user_id = ".$user_id." AND tyr_job_role_mapping.user_id = ".$reviewer_id;

        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_favourite_profile_paginated($user_id, $hidden_user){
        $query = "SELECT " . TABLE_USERS . ".user_id,role_id,parent_id," . TABLE_USERS . ".country_id,email,
              username,firstname,lastname,address,availability,extra_title,
              state,zip," . TABLE_COUNTRIES . ".country, " . TABLE_CITIES . ".city," . TABLE_MEDIA . ".media_type," . TABLE_MEDIA
                . ".media_name FROM " . TABLE_USERS . "
              LEFT JOIN " . TABLE_COUNTRIES . " ON " . TABLE_USERS . ".country_id=" . TABLE_COUNTRIES . ".country_id
              LEFT JOIN " . TABLE_CITIES . " ON " . TABLE_USERS . ".city=" . TABLE_CITIES . ".city_id
              RIGHT JOIN " .TABLE_FAVOURITE_PROFILE. " ON " .TABLE_FAVOURITE_PROFILE. ".user_id = " . TABLE_USERS . ".user_id
              AND " .TABLE_FAVOURITE_PROFILE. ".studio_id = '".$user_id."' AND " .TABLE_FAVOURITE_PROFILE. ".status_sl = 1
              LEFT JOIN " . TABLE_MEDIA . " ON " . TABLE_USERS . ".user_id=" . TABLE_MEDIA . ".user_id
              AND " . TABLE_MEDIA . ".profile_image='1' AND " . TABLE_MEDIA . ".status_sl='1'
              WHERE " . TABLE_USERS . ".role_id='2' AND " . TABLE_USERS . ".status_sl='1' AND ".TABLE_USERS.".publish_account = '1' AND"
                . " " .TABLE_USERS. ".user_id NOT IN (" . $hidden_user . ") ORDER BY " .TABLE_FAVOURITE_PROFILE. ".user_id DESC ";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            if (!is_array($return_array))
                $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    

    public function check_video_group_img_id($user_id) {
        $query = "SELECT array_to_string(array_agg(CONCAT(image_id)),',')  AS group_img_id FROM tyr_media
                  GROUP BY user_id = '" . $user_id . "' AND status_sl = '1' AND media_type = 'video' LIMIT 1 OFFSET 1";
                                                
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }

    public function get_studio_locations_list($selected_value) {
        $query = "SELECT company_id, studio_id, tyr_company_detail.parent_id, company_country as country_id,company_city as city_id, tyr_countries.country, tyr_cities.city, tyr_users.status_sl as status_sl FROM tyr_company_detail inner join tyr_countries on"
                . " tyr_company_detail.company_country = tyr_countries.country_id inner join tyr_cities on tyr_cities.city_id = tyr_company_detail.company_city inner join tyr_users on tyr_users.user_id = tyr_company_detail.studio_id WHERE"
                . " (tyr_company_detail.company_id = ".$selected_value." or tyr_company_detail.parent_id = ".$selected_value.") AND tyr_users.status_sl != -1 and tyr_users.email != 'Remove' order by company_id asc";
        
        //echo $query;
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while ($row = $statement->fetch()) {
            $return_array[] = $row;

        }
        return $return_array;
    }
    
    public function get_studio_locations_city_ids($selected_value) {
        $query = "SELECT array_to_string(array_agg(city_id),',') as city_id FROM tyr_company_detail inner join tyr_countries on"
                . " tyr_company_detail.company_country = tyr_countries.country_id inner join tyr_cities on tyr_cities.city_id = tyr_company_detail.company_city WHERE"
                . " (company_id = ".$selected_value." or parent_id = ".$selected_value.")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while ($row = $statement->fetch()) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_search_listing_studio_data_count($role_id, $where) {
        $query = "SELECT count(*) as cnt FROM ".TABLE_USERS." st LEFT JOIN ".TABLE_COMPANY." c ON st.user_id = c.studio_id
                              LEFT JOIN ".TABLE_STATUSES." stat ON st.status_sl = stat.status_id
                              LEFT JOIN ".TABLE_COUNTRIES." cont ON c.company_country = cont.country_id
                              LEFT JOIN ".TABLE_INDUSTRY." i ON st.industry_id = i.industry_id
                              LEFT JOIN (SELECT MAX(visit_time) AS visit_time,user_id FROM  ".TABLE_PAGEVIEWS." GROUP BY user_id) tpv ON st.user_id = tpv.user_id
                              WHERE st.role_id = :role_id AND st.parent_id = 0 AND st.status_sl!= -1 and st.email != 'Remove' ".$where." GROUP BY st.user_id";


        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
}
