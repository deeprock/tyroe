<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_deleted_account_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_deleted_account(&$tyr_deleted_account_obj) {
        $query = 'INSERT into tyr_deleted_account(
                   user_id, suggestions, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :suggestions, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_deleted_account_obj->user_id);
        $statement->bindParam(':suggestions', $tyr_deleted_account_obj->suggestions);
        $statement->bindParam(':status_sl', $tyr_deleted_account_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_deleted_account_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_deleted_account_obj->created_updated_by);
        $statement->execute();
        $tyr_deleted_account_obj->acc_id = $this->db_connection->lastInsertId('tyr_custome_theme_theme_id_seq');
    }

    public function get_deleted_account(&$tyr_deleted_account_obj) {
        $query = 'select * from tyr_deleted_account where acc_id = :acc_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':acc_id', $tyr_deleted_account_obj->acc_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_deleted_account_obj->acc_id = $row['acc_id'];
           $tyr_deleted_account_obj->user_id = $row['user_id'];
           $tyr_deleted_account_obj->suggestions = $row['suggestions'];
           $tyr_deleted_account_obj->status_sl = $row['status_sl'];
        }
    }
    
}