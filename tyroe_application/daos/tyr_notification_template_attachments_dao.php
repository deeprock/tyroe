<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_notification_template_attachments_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_notification_template_attachments(&$tyr_notification_template_attachments_obj) {
        $query = 'INSERT into tyr_notification_template_attachments(
                    template_id, original_filename, filename, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :template_id, original_filename, :filename, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':template_id', $tyr_notification_template_attachments_obj->template_id);
        $statement->bindParam(':original_filename', $tyr_notification_template_attachments_obj->original_filename);
        $statement->bindParam(':filename', $tyr_notification_template_attachments_obj->filename);
        $statement->bindParam(':status_sl', $tyr_notification_template_attachments_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_notification_template_attachments_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_notification_template_attachments_obj->created_updated_by);
        $statement->execute();
        $tyr_notification_template_attachments_obj->attachment_id = $this->db_connection->lastInsertId('tyr_notification_template_attachments_attachment_id_seq');
    }

    public function get_notification_template_attachments(&$tyr_notification_template_attachments_obj) {
        $query = 'select * from tyr_notification_template_attachments where option_id = :attachment_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':attachment_id', $tyr_notification_template_attachments_obj->attachment_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_notification_template_attachments_obj->attachment_id = $row['attachment_id'];
           $tyr_notification_template_attachments_obj->template_id = $row['template_id'];
           $tyr_notification_template_attachments_obj->original_filename = $row['original_filename'];
           $tyr_notification_template_attachments_obj->filename = $row['filename'];
           $tyr_notification_template_attachments_obj->status_sl = $row['status_sl'];
        }
    }
    
}