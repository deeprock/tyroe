<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_apply_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_apply(&$tyr_job_apply_obj) {
        $query = 'INSERT into tyr_job_apply(
                   user_id, job_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :job_id, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_job_apply_obj->user_id);
        $statement->bindParam(':job_id', $tyr_job_apply_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_job_apply_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_apply_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_apply_obj->created_updated_by);
        $statement->execute();
        $tyr_job_apply_obj->apply_id = $this->db_connection->lastInsertId('tyr_job_apply_apply_id_seq');
    }

    public function get_job_apply(&$tyr_job_apply_obj) {
        $query = 'select * from tyr_job_apply where apply_id = :apply_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':apply_id', $tyr_job_apply_obj->apply_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_apply_obj->apply_id = $row['apply_id'];
           $tyr_job_apply_obj->user_id = $row['user_id'];
           $tyr_job_apply_obj->job_id = $row['job_id'];
           $tyr_job_apply_obj->status_sl = $row['status_sl'];
        }
    }
    
}