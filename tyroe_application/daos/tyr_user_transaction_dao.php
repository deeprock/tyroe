<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_user_transaction_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_transaction(&$tyr_payment_obj) {
        $query = 'INSERT into tyr_user_transaction(
                   user_id, transaction_id, type, job_id, created_at, updated_at, created_by, updated_by, transaction_time, amt, package, coupon_id) 
                   values(:user_id, :transaction_id, :type, :job_id, :created_at, :updated_at, :created_by, :updated_by, :transaction_time, :amt, :package, 
                   :coupon_id)';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_payment_obj->user_id);
        $statement->bindParam(':transaction_id', $tyr_payment_obj->transaction_id);
        $statement->bindParam(':type', $tyr_payment_obj->type);
        $statement->bindParam(':job_id', $tyr_payment_obj->job_id);
        $statement->bindParam(':created_at', strtotime("now"));
        $statement->bindParam(':created_by', $tyr_payment_obj->created_by);
        $statement->bindParam(':updated_at', strtotime("now"));
        $statement->bindParam(':updated_by', $tyr_payment_obj->updated_by);
        $statement->bindParam(':transaction_time', $tyr_payment_obj->transaction_time);
        $statement->bindParam(':amt', $tyr_payment_obj->amt);
        $statement->bindParam(':package', $tyr_payment_obj->package);
        $statement->bindParam(':coupon_id', $tyr_payment_obj->coupon_id);
        $statement->execute();
        $tyr_payment_obj->id = $this->db_connection->lastInsertId('tyr_user_transaction_id_seq');
    }
    
    public function get_all_transactions(&$tyr_payment_obj){
        $query = 'select * from tyr_user_transaction where user_id = :user_id order by id DESC';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_payment_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while (($row = $statement->fetch()) != FALSE) {
           $return_array[] = $row;
        }
        
        return $return_array;
    }
    
    public function get(&$tyr_payment_obj){
        $query = 'select * from tyr_user_transaction where user_id = :user_id and transaction_id = :transaction_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_payment_obj->user_id);
        $statement->bindParam(':transaction_id', $tyr_payment_obj->transaction_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);

        if (($row = $statement->fetch()) != FALSE) {
           $tyr_payment_obj->id = $row['id'];
           $tyr_payment_obj->type = $row['type'];
           $tyr_payment_obj->job_id = $row['job_id'];
           $tyr_payment_obj->package = $row['package'];
           $tyr_payment_obj->coupon_id = $row['coupon_id'];
        }
    }
}