<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_fb_users_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_fb_users(&$tyr_fb_users_obj) {
        $query = 'INSERT into tyr_fb_users(
                   socialmedia_id, user_data, user_id, access_token, ip, access_date, socialmedia_type, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :socialmedia_id, :user_data, :user_id, :access_token, :ip, :access_date, :socialmedia_type, :status_sl, created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':socialmedia_id', $tyr_fb_users_obj->socialmedia_id);
        $statement->bindParam(':user_data', $tyr_fb_users_obj->user_data);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->bindParam(':access_token', $tyr_fb_users_obj->access_token);
        $statement->bindParam(':ip', $tyr_fb_users_obj->ip);
        $statement->bindParam(':access_date', $tyr_fb_users_obj->access_date);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->bindParam(':status_sl', $tyr_fb_users_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_fb_users_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_fb_users_obj->created_updated_by);
        $statement->execute();
        $tyr_fb_users_obj->id = $this->db_connection->lastInsertId('tyr_fb_users_id_seq');
    }

    public function get_fb_users(&$tyr_fb_users_obj) {
        $query = 'select * from tyr_fb_users where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_fb_users_obj->id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_fb_users_obj->id = $row['id'];
           $tyr_fb_users_obj->socialmedia_id = $row['socialmedia_id'];
           $tyr_fb_users_obj->user_data = $row['user_data'];
           $tyr_fb_users_obj->user_id = $row['user_id'];
           $tyr_fb_users_obj->access_token = $row['access_token'];
           $tyr_fb_users_obj->ip = $row['ip'];
           $tyr_fb_users_obj->access_date = $row['access_date'];
           $tyr_fb_users_obj->socialmedia_type = $row['socialmedia_type'];
           $tyr_fb_users_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_user_social_media_details(&$tyr_fb_users_obj) {
        $query = 'select * from tyr_fb_users where socialmedia_type = :socialmedia_type AND user_id = :user_id and status_sl = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_user_social_media_details_1(&$tyr_fb_users_obj) {
        $query = 'select * from tyr_fb_users where socialmedia_type = :socialmedia_type AND user_id = :user_id and socialmedia_id = :socialmedia_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->bindParam(':socialmedia_id', $tyr_fb_users_obj->socialmedia_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_link_status(&$tyr_fb_users_obj) {
        $query = 'update tyr_fb_users set status_sl = :status_sl, access_date = :access_date, updated_at = :updated_at, updated_by = :updated_by  where socialmedia_type = :socialmedia_type AND user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_fb_users_obj->status_sl);
        $statement->bindParam(':access_date', $tyr_fb_users_obj->access_date);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_fb_users_obj->user_id);
        $statement->execute();
        return true;
    }
    
    public function update_user_social_media_details(&$tyr_fb_users_obj) {
        $query = 'update tyr_fb_users set socialmedia_id = :socialmedia_id, user_data = :user_data, access_token = :access_token, ip = :ip, access_date = :access_date, status_sl = :status_sl, updated_at = :updated_at, updated_by = :updated_by  where socialmedia_type = :socialmedia_type AND user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':socialmedia_id', $tyr_fb_users_obj->socialmedia_id);
        $statement->bindParam(':user_data', $tyr_fb_users_obj->user_data);
        $statement->bindParam(':access_token', $tyr_fb_users_obj->access_token);
        $statement->bindParam(':ip', $tyr_fb_users_obj->ip);
        $statement->bindParam(':access_date', $tyr_fb_users_obj->access_date);
        $statement->bindParam(':status_sl', $tyr_fb_users_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_fb_users_obj->created_updated_by);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->execute();
        return true;
    }
    
    public function get_social_details_by_user_and_type(&$tyr_fb_users_obj) {
        $query = 'select * from tyr_fb_users where socialmedia_type = :socialmedia_type AND user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':socialmedia_type', $tyr_fb_users_obj->socialmedia_type);
        $statement->bindParam(':user_id', $tyr_fb_users_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
    }
}