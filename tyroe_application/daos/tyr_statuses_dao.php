<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_statuses_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_statuses(&$tyr_statuses_obj) {
        $query = 'INSERT into tyr_statuses(
                    status_id, status
                  ) values(
                    :status_id, :status,
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_id', $tyr_statuses_obj->status_id);
        $statement->bindParam(':status', $tyr_statuses_obj->status);
        $statement->execute();
    }

    public function get_statuses(&$tyr_statuses_obj) {
        $query = 'select * from tyr_statuses where status_id = :status_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_id', $tyr_statuses_obj->status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_statuses_obj->status_id = $row['status_id'];
           $tyr_statuses_obj->status = $row['status'];
        }
    }
    
}