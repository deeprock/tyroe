<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_professional_level_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_professional_level(&$tyr_professional_level_obj) {
        $query = 'INSERT into tyr_professional_level(
                    tyroe_level_title, created_at, created_by, updated_at, updated_by
                  ) values(
                    :tyroe_level_title, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_level_title', $tyr_professional_level_obj->tyroe_level_title);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_professional_level_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_professional_level_obj->created_updated_by);
        $statement->execute();
        $tyr_professional_level_obj->tyroe_level_id = $this->db_connection->lastInsertId('tyr_professional_level_tyroe_level_id_seq');
    }

    public function get_professional_level(&$tyr_professional_level_obj) {
        $query = 'select * from tyr_professional_level where tyroe_level_id = :tyroe_level_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_level_id', $tyr_professional_level_obj->tyroe_level_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_professional_level_obj->tyroe_level_id = $row['tyroe_level_id'];
           $tyr_professional_level_obj->tyroe_level_title = $row['tyroe_level_title'];
        }
    }
    
    public function get_all_professional_level(){
        $return_array = '';
        $query = 'select * from tyr_professional_level';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}