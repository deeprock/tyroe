<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_role_mapping_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_role_mapping(&$tyr_job_role_mapping_obj) {
        $query = 'INSERT into tyr_job_role_mapping(
                   job_id, user_id, is_admin, is_reviewer, created_at, created_by, updated_at, updated_by
                  ) values(
                   :job_id, :user_id, :is_admin, :is_reviewer, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->bindParam(':user_id', $tyr_job_role_mapping_obj->user_id);
        $statement->bindParam(':is_admin', $tyr_job_role_mapping_obj->is_admin);
        $statement->bindParam(':is_reviewer', $tyr_job_role_mapping_obj->is_reviewer);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_role_mapping_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_role_mapping_obj->created_updated_by);
        $statement->execute();
        $tyr_job_role_mapping_obj->id = $this->db_connection->lastInsertId('tyr_job_role_mapping_id_seq');
    }

    public function get_job_role_mapping(&$tyr_job_role_mapping_obj) {
        $query = 'select * from tyr_job_role_mapping where job_id = :job_id AND user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->bindParam(':user_id', $tyr_job_role_mapping_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_role_mapping_obj->id = $row['id'];
           $tyr_job_role_mapping_obj->is_admin = $row['is_admin'];
           $tyr_job_role_mapping_obj->is_reviewer = $row['is_reviewer'];
        }
    }
    
    public function update_job_role_mapping(&$tyr_job_role_mapping_obj) {
        $query = "update tyr_job_role_mapping set is_admin = :is_admin, is_reviewer = :is_reviewer where job_id = :job_id AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':is_admin', $tyr_job_role_mapping_obj->is_admin);
        $statement->bindParam(':is_reviewer', $tyr_job_role_mapping_obj->is_reviewer);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->bindParam(':user_id', $tyr_job_role_mapping_obj->user_id);
        $statement->execute();
        return true;
    }
    
    public function delete_job_role_mapping(&$tyr_job_role_mapping_obj) {
        $query = "delete from tyr_job_role_mapping where job_id = :job_id AND user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->bindParam(':user_id', $tyr_job_role_mapping_obj->user_id);
        $statement->execute();
        return true;
    }
    
    public function get_user_job_ids(&$tyr_job_role_mapping_obj) {
        $query = "select array_to_string(array_agg(tyr_job_role_mapping.job_id),',') as job_id from tyr_job_role_mapping inner join tyr_jobs on tyr_jobs.job_id = tyr_job_role_mapping.job_id where"
                . " (is_admin = :is_admin OR is_reviewer = :is_reviewer) AND tyr_job_role_mapping.user_id = :user_id and tyr_jobs.archived_status = 0";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':is_admin', $tyr_job_role_mapping_obj->is_admin);
        $statement->bindParam(':is_reviewer', $tyr_job_role_mapping_obj->is_reviewer);
        $statement->bindParam(':user_id', $tyr_job_role_mapping_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_reviewers_for_job(&$tyr_job_role_mapping_obj) {
        $query = "select array_to_string(array_agg(user_id),',') as user_id from tyr_job_role_mapping where job_id = :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_admins_for_job(&$tyr_job_role_mapping_obj) {
        $query = "select array_to_string(array_agg(user_id),',') as user_id from tyr_job_role_mapping where job_id = :job_id and is_admin = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_role_mapping_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
}