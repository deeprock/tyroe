<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_membership_log_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_membership_log(&$tyr_membership_log_obj) {
        $query = 'INSERT into tyr_membership_log(
                    membership_id, membership_mode, payment_mode, payment, membership_duration, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :membership_id, :membership_mode, :payment_mode, :payment, :membership_duration, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':membership_id', $tyr_membership_log_obj->membership_id);
        $statement->bindParam(':membership_mode', $tyr_membership_log_obj->membership_mode);
        $statement->bindParam(':payment_mode', $tyr_membership_log_obj->payment_mode);
        $statement->bindParam(':payment', $tyr_membership_log_obj->payment);
        $statement->bindParam(':membership_duration', $tyr_membership_log_obj->membership_duration);
        $statement->bindParam(':status_sl', $tyr_membership_log_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_membership_log_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_membership_log_obj->created_updated_by);
        $statement->execute();
        $tyr_membership_log_obj->log_id = $this->db_connection->lastInsertId('tyr_membership_log_log_id_seq');
    }

    public function get_membership_log(&$tyr_membership_log_obj) {
        $query = 'select * from tyr_membership_log where log_id = :log_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':log_id', $tyr_membership_log_obj->log_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_membership_log_obj->log_id = $row['log_id'];
           $tyr_membership_log_obj->membership_id = $row['membership_id'];
           $tyr_membership_log_obj->membership_mode = $row['membership_mode'];
           $tyr_membership_log_obj->payment_mode = $row['payment_mode'];
           $tyr_membership_log_obj->payment = $row['payment'];
           $tyr_membership_log_obj->membership_duration = $row['membership_duration'];
           $tyr_membership_log_obj->status_sl = $row['status_sl'];
        }
    }
    
}