<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_users_dao extends Abstract_DAO {

    

    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }

    public function save_user(&$tyr_user_obj) {
        $query = 'INSERT into tyr_users(
                    role_id, parent_id, country_id, tyroe_level, industry_id, experienceyear_id, job_level_id, profile_url,
                    email, username, password, password_len, firstname, lastname, phone, fax, cellphone, address, city, state, zip,
                    user_web, user_occupation, user_biography, user_experience, suburb, display_name, job_title, availability,
                    publish_account, status_sl, created_timestamp, modified_timestamp, created_at, created_by, updated_at, updated_by, extra_title
                  ) values(
                    :role_id, :parent_id, :country_id, :tyroe_level, :industry_id, :experienceyear_id, :job_level_id, :profile_url, :email, :username,
                    :password, :password_len, :firstname, :lastname, :phone, :fax, :cellphone, :address, :city, :state, :zip, :user_web,
                    :user_occupation, :user_biography, :user_experience, :suburb, :display_name, :job_title, :availability, :publish_account,
                    :status_sl, :created_timestamp, :modified_timestamp, :created_at, :created_by, :updated_at, :updated_by, :extra_title
                  )';
        if($tyr_user_obj->country_id == ''){
            $tyr_user_obj->country_id = 0;
        }
        
        if($tyr_user_obj->city == ''){
            $tyr_user_obj->city = 0;
        }
        
        if($tyr_user_obj->state == ''){
            $tyr_user_obj->state = 0;
        }
        
        
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':country_id', $tyr_user_obj->country_id);
        $statement->bindParam(':tyroe_level', $tyr_user_obj->tyroe_level);
        $statement->bindParam(':industry_id', $tyr_user_obj->industry_id);
        $statement->bindParam(':experienceyear_id', $tyr_user_obj->experienceyear_id);
        $statement->bindParam(':job_level_id', $tyr_user_obj->job_level_id);
        $statement->bindParam(':profile_url', $tyr_user_obj->profile_url);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->bindParam(':username', $tyr_user_obj->username);
        $statement->bindParam(':password', $tyr_user_obj->password);
        $statement->bindParam(':password_len', $tyr_user_obj->password_len);
        $statement->bindParam(':firstname', $tyr_user_obj->firstname);
        $statement->bindParam(':lastname', $tyr_user_obj->lastname);
        $statement->bindParam(':phone', $tyr_user_obj->phone);
        $statement->bindParam(':fax', $tyr_user_obj->fax);
        $statement->bindParam(':cellphone', $tyr_user_obj->cellphone);
        $statement->bindParam(':address', $tyr_user_obj->address);
        $statement->bindParam(':city', $tyr_user_obj->city);
        $statement->bindParam(':state', $tyr_user_obj->state);
        $statement->bindParam(':zip', $tyr_user_obj->zip);
        $statement->bindParam(':user_web', $tyr_user_obj->user_web);
        $statement->bindParam(':user_occupation', $tyr_user_obj->user_occupation);
        $statement->bindParam(':user_biography', $tyr_user_obj->user_biography);
        $statement->bindParam(':user_experience', $tyr_user_obj->user_experience);
        $statement->bindParam(':suburb', $tyr_user_obj->suburb);
        $statement->bindParam(':display_name', $tyr_user_obj->display_name);
        $statement->bindParam(':job_title', $tyr_user_obj->job_title);
        $statement->bindParam(':availability', $tyr_user_obj->availability);
        $statement->bindParam(':publish_account', $tyr_user_obj->publish_account);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_user_obj->created_timestamp);
        $statement->bindParam(':extra_title', $tyr_user_obj->extra_title);
        $statement->bindParam(':modified_timestamp', $tyr_user_obj->modified_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_user_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_user_obj->created_updated_by);
        $statement->execute();
        $tyr_user_obj->user_id = $this->db_connection->lastInsertId('tyr_users_user_id_seq');
    }

    public function get_user(&$tyr_user_obj) {
        $query = 'select * from tyr_users where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->user_id = $row['user_id'];
           $tyr_user_obj->role_id = $row['role_id'];
           $tyr_user_obj->parent_id = $row['parent_id'];
           $tyr_user_obj->country_id = $row['country_id'];
           $tyr_user_obj->tyroe_level = $row['tyroe_level'];
           $tyr_user_obj->industry_id = $row['industry_id'];
           $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_user_obj->job_level_id = $row['job_level_id'];
           $tyr_user_obj->profile_url = $row['profile_url'];
           $tyr_user_obj->email = $row['email'];
           $tyr_user_obj->username = $row['username'];
           $tyr_user_obj->password = $row['password'];
           $tyr_user_obj->password_len = $row['password_len'];
           $tyr_user_obj->firstname = $row['firstname'];
           $tyr_user_obj->lastname = $row['lastname'];
           $tyr_user_obj->phone = $row['phone'];
           $tyr_user_obj->fax = $row['fax'];
           $tyr_user_obj->cellphone = $row['cellphone'];
           $tyr_user_obj->address = $row['address'];
           $tyr_user_obj->city = $row['city'];
           $tyr_user_obj->state = $row['state'];
           $tyr_user_obj->zip = $row['zip'];
           $tyr_user_obj->user_web = $row['user_web'];
           $tyr_user_obj->user_occupation = $row['user_occupation'];
           $tyr_user_obj->user_biography = $row['user_biography'];
           $tyr_user_obj->user_experience = $row['user_experience'];
           $tyr_user_obj->suburb = $row['suburb'];
           $tyr_user_obj->display_name = $row['display_name'];
           $tyr_user_obj->job_title = $row['job_title'];
           $tyr_user_obj->availability = $row['availability'];
           $tyr_user_obj->publish_account = $row['publish_account'];
           $tyr_user_obj->status_sl = $row['status_sl'];
           $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }

    public function get_user_by_status(&$tyr_user_obj) {
        $query = 'select * from tyr_users where user_id = :user_id and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->user_id = $row['user_id'];
           $tyr_user_obj->role_id = $row['role_id'];
           $tyr_user_obj->parent_id = $row['parent_id'];
           $tyr_user_obj->country_id = $row['country_id'];
           $tyr_user_obj->tyroe_level = $row['tyroe_level'];
           $tyr_user_obj->industry_id = $row['industry_id'];
           $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_user_obj->job_level_id = $row['job_level_id'];
           $tyr_user_obj->profile_url = $row['profile_url'];
           $tyr_user_obj->email = $row['email'];
           $tyr_user_obj->username = $row['username'];
           $tyr_user_obj->password = $row['password'];
           $tyr_user_obj->password_len = $row['password_len'];
           $tyr_user_obj->firstname = $row['firstname'];
           $tyr_user_obj->lastname = $row['lastname'];
           $tyr_user_obj->phone = $row['phone'];
           $tyr_user_obj->fax = $row['fax'];
           $tyr_user_obj->cellphone = $row['cellphone'];
           $tyr_user_obj->address = $row['address'];
           $tyr_user_obj->city = $row['city'];
           $tyr_user_obj->state = $row['state'];
           $tyr_user_obj->zip = $row['zip'];
           $tyr_user_obj->user_web = $row['user_web'];
           $tyr_user_obj->user_occupation = $row['user_occupation'];
           $tyr_user_obj->user_experience = $row['user_experience'];
           $tyr_user_obj->user_biography = $row['user_biography'];
           $tyr_user_obj->suburb = $row['suburb'];
           $tyr_user_obj->display_name = $row['display_name'];
           $tyr_user_obj->job_title = $row['job_title'];
           $tyr_user_obj->availability = $row['availability'];
           $tyr_user_obj->publish_account = $row['publish_account'];
           $tyr_user_obj->status_sl = $row['status_sl'];
           $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }

    public function user_auth(&$tyr_user_obj) {
        $query = 'select * from tyr_users where email = :email and password = :password and status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->bindParam(':password', $tyr_user_obj->password);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_user_obj->user_id = $row['user_id'];
            $tyr_user_obj->role_id = $row['role_id'];
            $tyr_user_obj->parent_id = $row['parent_id'];
            $tyr_user_obj->country_id = $row['country_id'];
            $tyr_user_obj->tyroe_level = $row['tyroe_level'];
            $tyr_user_obj->industry_id = $row['industry_id'];
            $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
            $tyr_user_obj->job_level_id = $row['job_level_id'];
            $tyr_user_obj->profile_url = $row['profile_url'];
            $tyr_user_obj->email = $row['email'];
            $tyr_user_obj->username = $row['username'];
            $tyr_user_obj->password = $row['password'];
            $tyr_user_obj->password_len = $row['password_len'];
            $tyr_user_obj->firstname = $row['firstname'];
            $tyr_user_obj->lastname = $row['lastname'];
            $tyr_user_obj->phone = $row['phone'];
            $tyr_user_obj->fax = $row['fax'];
            $tyr_user_obj->cellphone = $row['cellphone'];
            $tyr_user_obj->address = $row['address'];
            $tyr_user_obj->city = $row['city'];
            $tyr_user_obj->state = $row['state'];
            $tyr_user_obj->zip = $row['zip'];
            $tyr_user_obj->user_web = $row['user_web'];
            $tyr_user_obj->user_occupation = $row['user_occupation'];
            $tyr_user_obj->user_experience = $row['user_experience'];
            $tyr_user_obj->suburb = $row['suburb'];
            $tyr_user_obj->display_name = $row['display_name'];
            $tyr_user_obj->job_title = $row['job_title'];
            $tyr_user_obj->availability = $row['availability'];
            $tyr_user_obj->publish_account = $row['publish_account'];
            $tyr_user_obj->status_sl = $row['status_sl'];
            $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }
    
    public function user_auth_for_login(&$tyr_user_obj) {
        $query = "SELECT user_id,parent_id,status_sl,firstname,lastname,username,job_title,parent_id,profile_url,email,role," . TABLE_USERS . ".role_id FROM " . TABLE_USERS . "," . TABLE_ROLES . "  WHERE email= :email AND password = :password AND " . TABLE_USERS . ".role_id = " . TABLE_ROLES . ".role_id AND " . TABLE_USERS . ".status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->bindParam(':password', $tyr_user_obj->password);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_user_obj->user_id = $row['user_id'];
            $tyr_user_obj->role_id = $row['role_id'];
            $tyr_user_obj->role = $row['role'];
            $tyr_user_obj->parent_id = $row['parent_id'];
            $tyr_user_obj->country_id = $row['country_id'];
            $tyr_user_obj->tyroe_level = $row['tyroe_level'];
            $tyr_user_obj->industry_id = $row['industry_id'];
            $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
            $tyr_user_obj->job_level_id = $row['job_level_id'];
            $tyr_user_obj->profile_url = $row['profile_url'];
            $tyr_user_obj->email = $row['email'];
            $tyr_user_obj->username = $row['username'];
            $tyr_user_obj->password = $row['password'];
            $tyr_user_obj->password_len = $row['password_len'];
            $tyr_user_obj->firstname = $row['firstname'];
            $tyr_user_obj->lastname = $row['lastname'];
            $tyr_user_obj->phone = $row['phone'];
            $tyr_user_obj->fax = $row['fax'];
            $tyr_user_obj->cellphone = $row['cellphone'];
            $tyr_user_obj->address = $row['address'];
            $tyr_user_obj->city = $row['city'];
            $tyr_user_obj->state = $row['state'];
            $tyr_user_obj->zip = $row['zip'];
            $tyr_user_obj->user_web = $row['user_web'];
            $tyr_user_obj->user_occupation = $row['user_occupation'];
            $tyr_user_obj->user_experience = $row['user_experience'];
            $tyr_user_obj->suburb = $row['suburb'];
            $tyr_user_obj->display_name = $row['display_name'];
            $tyr_user_obj->job_title = $row['job_title'];
            $tyr_user_obj->availability = $row['availability'];
            $tyr_user_obj->publish_account = $row['publish_account'];
            $tyr_user_obj->status_sl = $row['status_sl'];
            $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }

    public function get_user_by_email(&$tyr_user_obj) {
        $query = 'select * from tyr_users where email = :email';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->user_id = $row['user_id'];
           $tyr_user_obj->role_id = $row['role_id'];
           $tyr_user_obj->parent_id = $row['parent_id'];
           $tyr_user_obj->country_id = $row['country_id'];
           $tyr_user_obj->tyroe_level = $row['tyroe_level'];
           $tyr_user_obj->industry_id = $row['industry_id'];
           $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_user_obj->job_level_id = $row['job_level_id'];
           $tyr_user_obj->profile_url = $row['profile_url'];
           $tyr_user_obj->email = $row['email'];
           $tyr_user_obj->username = $row['username'];
           $tyr_user_obj->password = $row['password'];
           $tyr_user_obj->password_len = $row['password_len'];
           $tyr_user_obj->firstname = $row['firstname'];
           $tyr_user_obj->lastname = $row['lastname'];
           $tyr_user_obj->phone = $row['phone'];
           $tyr_user_obj->fax = $row['fax'];
           $tyr_user_obj->cellphone = $row['cellphone'];
           $tyr_user_obj->address = $row['address'];
           $tyr_user_obj->city = $row['city'];
           $tyr_user_obj->state = $row['state'];
           $tyr_user_obj->zip = $row['zip'];
           $tyr_user_obj->user_web = $row['user_web'];
           $tyr_user_obj->user_occupation = $row['user_occupation'];
           $tyr_user_obj->user_experience = $row['user_experience'];
           $tyr_user_obj->user_biography = $row['user_biography'];
           $tyr_user_obj->suburb = $row['suburb'];
           $tyr_user_obj->display_name = $row['display_name'];
           $tyr_user_obj->job_title = $row['job_title'];
           $tyr_user_obj->availability = $row['availability'];
           $tyr_user_obj->publish_account = $row['publish_account'];
           $tyr_user_obj->status_sl = $row['status_sl'];
           $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }
    
    public function get_user_by_email_and_status(&$tyr_user_obj) {
        $query = "SELECT email FROM " . TABLE_USERS . "
                      WHERE " . TABLE_USERS . ".user_id = :user_id AND " . TABLE_USERS . ".status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->user_id = $row['user_id'];
           $tyr_user_obj->role_id = $row['role_id'];
           $tyr_user_obj->parent_id = $row['parent_id'];
           $tyr_user_obj->country_id = $row['country_id'];
           $tyr_user_obj->tyroe_level = $row['tyroe_level'];
           $tyr_user_obj->industry_id = $row['industry_id'];
           $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_user_obj->job_level_id = $row['job_level_id'];
           $tyr_user_obj->profile_url = $row['profile_url'];
           $tyr_user_obj->email = $row['email'];
           $tyr_user_obj->username = $row['username'];
           $tyr_user_obj->password = $row['password'];
           $tyr_user_obj->password_len = $row['password_len'];
           $tyr_user_obj->firstname = $row['firstname'];
           $tyr_user_obj->lastname = $row['lastname'];
           $tyr_user_obj->phone = $row['phone'];
           $tyr_user_obj->fax = $row['fax'];
           $tyr_user_obj->cellphone = $row['cellphone'];
           $tyr_user_obj->address = $row['address'];
           $tyr_user_obj->city = $row['city'];
           $tyr_user_obj->state = $row['state'];
           $tyr_user_obj->zip = $row['zip'];
           $tyr_user_obj->user_web = $row['user_web'];
           $tyr_user_obj->user_occupation = $row['user_occupation'];
           $tyr_user_obj->user_experience = $row['user_experience'];
           $tyr_user_obj->user_biography = $row['user_biography'];
           $tyr_user_obj->suburb = $row['suburb'];
           $tyr_user_obj->display_name = $row['display_name'];
           $tyr_user_obj->job_title = $row['job_title'];
           $tyr_user_obj->availability = $row['availability'];
           $tyr_user_obj->publish_account = $row['publish_account'];
           $tyr_user_obj->status_sl = $row['status_sl'];
        }
    }

    public function update_users(&$tyr_user_obj) {
        $query = 'update tyr_users set role_id = :role_id, parent_id = :parent_id, country_id = :country_id, tyroe_level = :tyroe_level, industry_id = :industry_id, experienceyear_id = :experienceyear_id, job_level_id = :job_level_id, profile_url = :profile_url, email = :email, username = :username, password = :password, password_len = :password_len, firstname = :firstname, lastname = :lastname, phone = :phone, fax = :fax, cellphone = :cellphone, address = :address, city = :city, state = :state, zip = :zip, user_web = :user_web, user_occupation = :user_occupation, user_biography = :user_biography, user_experience = :user_experience, suburb = :suburb, display_name = :display_name, job_title = :job_title, availability = :availability, publish_account = :publish_account, status_sl = :status_sl, modified_timestamp = :modified_timestamp, updated_at = :updated_at, updated_by = :updated_by where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':country_id', $tyr_user_obj->country_id);
        $statement->bindParam(':tyroe_level', $tyr_user_obj->tyroe_level);
        $statement->bindParam(':industry_id', $tyr_user_obj->industry_id);
        $statement->bindParam(':experienceyear_id', $tyr_user_obj->experienceyear_id);
        $statement->bindParam(':job_level_id', $tyr_user_obj->job_level_id);
        $statement->bindParam(':profile_url', $tyr_user_obj->profile_url);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->bindParam(':username', $tyr_user_obj->username);
        $statement->bindParam(':password', $tyr_user_obj->password);
        $statement->bindParam(':password_len', $tyr_user_obj->password_len);
        $statement->bindParam(':firstname', $tyr_user_obj->firstname);
        $statement->bindParam(':lastname', $tyr_user_obj->lastname);
        $statement->bindParam(':phone', $tyr_user_obj->phone);
        $statement->bindParam(':cellphone', $tyr_user_obj->cellphone);
        $statement->bindParam(':fax', $tyr_user_obj->fax);
        $statement->bindParam(':cellphone', $tyr_user_obj->cellphone);
        $statement->bindParam(':address', $tyr_user_obj->address);
        $statement->bindParam(':city', $tyr_user_obj->city);
        $statement->bindParam(':state', $tyr_user_obj->state);
        $statement->bindParam(':zip', $tyr_user_obj->zip);
        $statement->bindParam(':user_web', $tyr_user_obj->user_web);
        $statement->bindParam(':user_occupation', $tyr_user_obj->user_occupation);
        $statement->bindParam(':user_experience', $tyr_user_obj->user_experience);
        $statement->bindParam(':user_biography', $tyr_user_obj->user_biography);
        $statement->bindParam(':suburb', $tyr_user_obj->suburb);
        $statement->bindParam(':display_name', $tyr_user_obj->display_name);
        $statement->bindParam(':job_title', $tyr_user_obj->job_title);
        $statement->bindParam(':availability', $tyr_user_obj->availability);
        $statement->bindParam(':publish_account', $tyr_user_obj->publish_account);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_user_obj->created_updated_by);
        $statement->bindParam(':modified_timestamp', $tyr_user_obj->modified_timestamp);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        return true;
    }

    public function update_password(&$tyr_user_obj) {
        $query = 'update tyr_users set password = :password, password_len = :password_len, updated_at = :updated_at, updated_by = :updated_by where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':password', $tyr_user_obj->password);
        $statement->bindParam(':password_len', $tyr_user_obj->password_len);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_user_obj->user_id);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        return true;
    }

    public function update_publish_account(&$tyr_user_obj) {
        $query = 'update tyr_users set publish_account = :publish_account, updated_at = :updated_at, updated_by = :updated_by where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':publish_account', $tyr_user_obj->publish_account);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_user_obj->user_id);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        return true;
    }

    public function get_all_admin_graph_data($where_in, $prev_ten_years, $current_year_end) {
        $query = 'select * from tyr_users where created_timestamp >= ' . $prev_ten_years . ' and created_timestamp <=' . $current_year_end . ' and role_id in(' . implode(',', $where_in) . ') and status_sl = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row; 
        }
        return $return_array;
    }

    public function get_total_members_count(&$tyr_user_obj) {
        $query = 'SELECT COUNT(*) as cnt FROM tyr_users WHERE role_id != :role_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }

    public function get_count_admin_companies(&$tyr_user_obj, $role_ids) {
        $query = 'SELECT COUNT(*) as cnt FROM tyr_users WHERE role_id in (' . implode(',', $role_ids) . ')';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_count_of_collaborators(&$tyr_user_obj) {
        $query = 'SELECT COUNT(user_id) AS collaborators FROM tyr_users WHERE (parent_id = :parent_id or user_id = :parent_id) and user_id != :parent_id AND status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array['collaborators'] = 0;
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array['collaborators'];
    }

    public function get_all_tyroe_order_by_created_time(&$tyr_user_obj) {
        $query = 'SELECT user_id, created_timestamp from tyr_users where role_id = 2 AND status_sl = 1 ORDER BY created_timestamp';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row; 
        }
        return $return_array;
    }

    public function get_total_tyroe_count_by_role(&$tyr_user_obj) {
        $publish_account = 1;
        $query = 'SELECT COUNT(*) AS total_tyroe FROM tyr_users WHERE role_id = :role_id AND status_sl = :status_sl and publish_account = :publish_account';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':publish_account', $publish_account);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_pending_reviewer_count(&$tyr_user_obj) {
        $query = 'SELECT COUNT(*) AS pending_reviewer FROM tyr_users WHERE parent_id = :parent_id AND status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function count_all_tyroes(&$tyr_user_obj) {
        $query = 'SELECT COUNT(user_id) AS cnt FROM tyr_users WHERE role_id = :role_id  AND user_id != :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_accumulate_tyroes_count(&$tyr_user_obj) {
        $query = "SELECT
                COUNT(*) as r_cnt,
                EXTRACT(MONTH FROM created_at) AS r_month
                FROM tyr_users
                WHERE role_id= 2
                GROUP BY EXTRACT(MONTH FROM created_at)";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array(); 
           $temp_array['r_cnt'] = $row['r_cnt'];
           $temp_array['r_month'] = $row['r_month'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_accumulate_studios_reviewers_count(&$tyr_user_obj) {
        $query = "SELECT
                COUNT(*) as r_cnt,
                EXTRACT(MONTH FROM created_at) AS r_month
                FROM tyr_users
                WHERE role_id IN (3,4)
                GROUP BY EXTRACT(MONTH FROM created_at)";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array(); 
           $temp_array['r_cnt'] = $row['r_cnt'];
           $temp_array['r_month'] = $row['r_month'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_accumulate_jobs_count(&$tyr_user_obj) {
        $query = "SELECT
                COUNT(*) as r_cnt,
                EXTRACT(MONTH FROM created_at) AS r_month
                FROM tyr_jobs
                GROUP BY EXTRACT(MONTH FROM created_at)";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array(); 
           $temp_array['r_cnt'] = $row['r_cnt'];
           $temp_array['r_month'] = $row['r_month'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }

    public function get_progress_bar_account_per(&$tyr_user_obj) {
        $query = "
                    SELECT
                        (
                         (CASE WHEN username IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN firstname IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN lastname IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN phone IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN fax IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN cellphone IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN address IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN city IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN state IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN zip IS NOT NULL THEN 1 ELSE 0 END)
                        ) AS count_acc
                        FROM tyr_users
                        WHERE user_id = :user_id
                ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while ($row = $statement->fetch()) {
            $return_array['count_acc'] = $row['count_acc'];
        }
        return $return_array;
    }

    public function get_progress_bar_profile_per(&$tyr_user_obj) {
        $query = "SELECT
                        (
                         (CASE WHEN username IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN firstname IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN lastname IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN phone IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN fax IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN cellphone IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN address IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN city IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN state IS NOT NULL THEN 1 ELSE 0 END)
                         + (CASE WHEN zip IS NOT NULL THEN 1 ELSE 0 END)
                        ) AS count_acc
                        FROM tyr_users
                        WHERE user_id = :user_id
                ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array['count_acc'] = $row['count_acc'];
        }
        return $return_array;
    }

    public function get_progress_bar_resume_per(&$tyr_user_obj) {
        $query = "
                    SELECT r.user_id,
                        (SELECT case when COUNT(*) > 0 then 2 else 0 end FROM tyr_skills s WHERE s.user_id = r.user_id)
                        +
                        (SELECT case when COUNT(*) > 0 then 2 else 0 end FROM tyr_education e WHERE e.user_id = r.user_id)
                        +
                        (SELECT case when COUNT(*) > 0 then 2 else 0 end FROM tyr_experience ex WHERE ex.user_id = r.user_id)
                        +
                        (SELECT case when COUNT(*) > 0 then 2 else 0 end FROM tyr_awards aw WHERE aw.user_id = r.user_id)
                        +
                        (SELECT case when COUNT(*) > 0 then 2 else 0 end FROM tyr_refrences ref WHERE ref.user_id = r.user_id)
                        AS total_count
                    FROM tyr_users r WHERE r.user_id = :user_id
                ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array['total_count'] = $row['total_count'];
        }
        return $return_array;
    }

    public function get_all_reviewer_for_studio(&$tyr_user_obj) {
        $query = "SELECT user_id FROM tyr_users WHERE parent_id = :parent_id and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_reviewer_for_studio_1(&$tyr_user_obj) {
        $query = "SELECT user_id FROM tyr_users WHERE parent_id = :parent_id and user_id != :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_team_count_by_parent(&$tyr_user_obj) {
        $query = "SELECT count(*) as cnt FROM tyr_users WHERE parent_id = :parent_id and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_all_studio(&$tyr_user_obj) {
        $query = "SELECT user_id, username FROM tyr_users WHERE role_id = :role_id AND status_sl = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }

    public function get_search_listing_studio_count(&$tyr_user_obj, $where) {
        $query = "SELECT COUNT(*) AS cnt FROM ".TABLE_USERS." st WHERE st.role_id != :role_id AND st.status_sl!='-1' ".$where;
        if($where != ''){
            $query = "SELECT COUNT(*) AS cnt FROM ".TABLE_USERS." st inner join ".TABLE_COMPANY." c WHERE st.role_id != :role_id AND st.status_sl!='-1' ".$where;
        }
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_search_listing_users_count(&$tyr_user_obj, $where) {
        $query = "SELECT COUNT(*) AS cnt FROM ".TABLE_USERS." u WHERE  status_sl=1 AND u.role_id != :role_id ".$where;
        if($where != ''){
            $query = "SELECT COUNT(*) AS cnt FROM ".TABLE_USERS." u inner join ".TABLE_COMPANY." c WHERE u.role_id != :role_id AND u.status_sl!='-1' ".$where;
        }
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    
    public function check_duplicate_user_email(&$tyr_user_obj,$where = '') {
        $query = "SELECT username FROM tyr_users WHERE email = :email".$where;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->username = $row['username'];
           return true;
        }
        return false;
    }
    
    public function check_duplicate_user_username(&$tyr_user_obj, $where) {
        $query = "SELECT email FROM tyr_users WHERE username = :username" . $where;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':username', $tyr_user_obj->username);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->email = $row['email'];
           return true;
        }
        return false;
    }

    public function get_all_users_by_role_in(&$tyr_user_obj, $role_in) {
        $query = "select * from tyr_users where status_sl= 1 AND role_id IN(" . $role_in . ")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp = array();
           $temp['user_id'] = $row['user_id'];
           $temp['role_id'] = $row['role_id'];
           $temp['parent_id'] = $row['parent_id'];
           $temp['country_id'] = $row['country_id'];
           $temp['tyroe_level'] = $row['tyroe_level'];
           $temp['industry_id'] = $row['industry_id'];
           $temp['experienceyear_id'] = $row['experienceyear_id'];
           $temp['job_level_id'] = $row['job_level_id'];
           $temp['profile_url'] = $row['profile_url'];
           $temp['email'] = $row['email'];
           $temp['username'] = $row['username'];
           $temp['password'] = $row['password'];
           $temp['password_len'] = $row['password_len'];
           $temp['firstname'] = $row['firstname'];
           $temp['lastname'] = $row['lastname'];
           $temp['phone'] = $row['phone'];
           $temp['fax'] = $row['fax'];
           $temp['cellphone'] = $row['cellphone'];
           $temp['address'] = $row['address'];
           $temp['state'] = $row['city'];
           $temp['state'] = $row['state'];
           $temp['zip'] = $row['zip'];
           $temp['user_web'] = $row['user_web'];
           $temp['user_occupation'] = $row['user_occupation'];
           $temp['user_experience'] = $row['user_experience'];
           $temp['user_biography'] = $row['user_biography'];
           $temp['suburb'] = $row['suburb'];
           $temp['display_name'] = $row['display_name'];
           $temp['job_title'] = $row['job_title'];
           $temp['availability'] = $row['availability'];
           $temp['publish_account'] = $row['publish_account'];
           $temp['status_sl'] = $row['status_sl'];
           $return_array[] = $temp; 
        }
        return $return_array;
    }

    public function get_remaining_studio_reviewer_count(&$tyr_user_obj) {
        $query = "SELECT COUNT(*) AS cnt FROM tyr_users  WHERE  status_sl = :status_sl AND role_id = :role_id AND parent_id = :parent_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_studio_details(&$tyr_user_obj) {
        $query = "select * from tyr_users where user_id = :user_id AND role_id = :role_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp = array();
           $temp['user_id'] = $row['user_id'];
           $temp['role_id'] = $row['role_id'];
           $temp['parent_id'] = $row['parent_id'];
           $temp['country_id'] = $row['country_id'];
           $temp['tyroe_level'] = $row['tyroe_level'];
           $temp['industry_id'] = $row['industry_id'];
           $temp['experienceyear_id'] = $row['experienceyear_id'];
           $temp['job_level_id'] = $row['job_level_id'];
           $temp['profile_url'] = $row['profile_url'];
           $temp['email'] = $row['email'];
           $temp['username'] = $row['username'];
           $temp['password'] = $row['password'];
           $temp['password_len'] = $row['password_len'];
           $temp['firstname'] = $row['firstname'];
           $temp['lastname'] = $row['lastname'];
           $temp['phone'] = $row['phone'];
           $temp['fax'] = $row['fax'];
           $temp['cellphone'] = $row['cellphone'];
           $temp['address'] = $row['address'];
           $temp['state'] = $row['city'];
           $temp['state'] = $row['state'];
           $temp['zip'] = $row['zip'];
           $temp['user_web'] = $row['user_web'];
           $temp['user_occupation'] = $row['user_occupation'];
           $temp['user_experience'] = $row['user_experience'];
           $temp['user_biography'] = $row['user_biography'];
           $temp['suburb'] = $row['suburb'];
           $temp['display_name'] = $row['display_name'];
           $temp['job_title'] = $row['job_title'];
           $temp['availability'] = $row['availability'];
           $temp['publish_account'] = $row['publish_account'];
           $temp['status_sl'] = $row['status_sl'];
           $return_array[] = $temp; 
        }
        return $return_array;
    }
    

    public function get_role_id_by_user_id(&$user_obj) {
        $query = "SELECT role_id FROM tyr_users WHERE user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if(($row = $statement->fetch()) != false) {
           $user_obj->role_id = $row['role_id'];
        }
    }

    public function get_total_reviewer_count(&$tyr_user_obj) {
        $query = "SELECT COUNT(*) AS total   FROM tyr_users a, tyr_users b WHERE a.parent_id = b.user_id AND  b.user_id= :user_id and a.status_sl= 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_available_user_count(&$tyr_user_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_users  WHERE user_id = :user_id and availability != :availability";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':availability', $tyr_user_obj->availability);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_duplicate_username(&$tyr_user_obj) {
        $query = "SELECT count(*) as result FROM tyr_users where user_id NOT IN(:user_id) AND  profile_url = :profile_url";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(":profile_url", $tyr_user_obj->profile_url);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['result'] = $row['result'];
        }
        return $return_array;
    }
    

    public function get_duplicate_users_count(&$tyr_user_obj) {
        $query = 'SELECT count(*) as total_tyroe FROM tyr_users WHERE username= :username AND AND user_id NOT IN(:user_id)';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':username', $tyr_user_obj->username);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array['total_tyroe'] = $row['total_tyroe'];
        }
        return $return_array;
    }
    
    public function get_email_data_for_profile(&$tyr_user_obj, $on) {
        $query = "SELECT u.job_title, c.company_id, c.company_name
                                            FROM tyr_users u
                                            LEFT JOIN tyr_company_detail c " . $on . "
                                            WHERE u.user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }

        return $return_array;
    }
    
    public function get_total_reviewers_count_where_role_not(&$tyr_user_obj) {
        $return_array = '';
        $query = "SELECT COUNT(*) as cnt FROM tyr_users WHERE role_id != :role_id ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_user_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }

        return $return_array;
    }
    
    public function get_total_users_count_where_role_in(&$tyr_user_obj,$roles) {
        $return_array = '';
        $query = "SELECT COUNT(*) as cnt FROM tyr_users WHERE role_id in (".$roles.") and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_user_profile_by_url($where){
        if($where != '')$where = 'where '.$where;
        $query = 'select * from tyr_users users '.$where;
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_details_for_public_profile($where) {
        $return_array = '';
        $query = "SELECT job_level.level_title,exp_years.experienceyear,industry.industry_name,users.user_id,users.city AS city_id,users.job_level_id,users.extra_title,
                  users.industry_id,users.experienceyear_id,role_id,tyroe_level,parent_id,users.country_id,country.country,profile_url,email,
                   username,password,password_len,firstname,lastname,phone,fax,cellphone,address,cities.city,user_occupation,user_biography,user_web,user_experience,
                   state,zip,suburb,display_name,availability,publish_account,created_timestamp,modified_timestamp, users.status_sl,
                    country.country, media.media_type,media.media_name,
                      media.image_id FROM tyr_users users
                 LEFT JOIN tyr_countries country ON users.country_id=country.country_id
                 LEFT JOIN tyr_media media ON users.user_id=media.user_id AND media.profile_image = '1' AND media.status_sl='1'
                 LEFT JOIN tyr_cities cities ON users.city::integer=cities.city_id
                 LEFT JOIN tyr_job_level job_level ON users.job_level_id=job_level.job_level_id
                 LEFT JOIN tyr_industry industry ON users.industry_id=industry.industry_id
                 LEFT JOIN tyr_experience_years exp_years ON users.experienceyear_id=exp_years.experienceyear_id
                 WHERE " . $where . " ORDER BY image_id DESC LIMIT 1 OFFSET 0";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }

        return $return_array;
    }
    
    public function get_studio_team_count(&$tyr_user_obj) {
        $return_array = '';
        $query = "SELECT COUNT(*) as cnt FROM tyr_users WHERE parent_id = :parent_id and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != false) {
            $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_duplicate_user_email_1(&$tyr_user_obj) {
        $query = "Select * from " . TABLE_USERS . " where email= :email AND user_id != :user_id  AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':email', $tyr_user_obj->email);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
           return true;
        }
        return false;
    }
    
    public function check_duplicate_user_name_1(&$tyr_user_obj) {
        $query = "Select * from " . TABLE_USERS . " where username= :username AND user_id != :user_id  AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':username', $tyr_user_obj->username);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
           return true;
        }
        return false;
    }
    
    public function get_user_by_parent(&$tyr_user_obj) {
        $query = 'select * from tyr_users where user_id = :user_id and parent_id = :parent_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_user_obj->user_id);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_user_obj->user_id = $row['user_id'];
           $tyr_user_obj->role_id = $row['role_id'];
           $tyr_user_obj->parent_id = $row['parent_id'];
           $tyr_user_obj->country_id = $row['country_id'];
           $tyr_user_obj->tyroe_level = $row['tyroe_level'];
           $tyr_user_obj->industry_id = $row['industry_id'];
           $tyr_user_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_user_obj->job_level_id = $row['job_level_id'];
           $tyr_user_obj->profile_url = $row['profile_url'];
           $tyr_user_obj->email = $row['email'];
           $tyr_user_obj->username = $row['username'];
           $tyr_user_obj->password = $row['password'];
           $tyr_user_obj->password_len = $row['password_len'];
           $tyr_user_obj->firstname = $row['firstname'];
           $tyr_user_obj->lastname = $row['lastname'];
           $tyr_user_obj->phone = $row['phone'];
           $tyr_user_obj->fax = $row['fax'];
           $tyr_user_obj->cellphone = $row['cellphone'];
           $tyr_user_obj->address = $row['address'];
           $tyr_user_obj->city = $row['city'];
           $tyr_user_obj->state = $row['state'];
           $tyr_user_obj->zip = $row['zip'];
           $tyr_user_obj->user_web = $row['user_web'];
           $tyr_user_obj->user_occupation = $row['user_occupation'];
           $tyr_user_obj->user_experience = $row['user_experience'];
           $tyr_user_obj->user_biography = $row['user_biography'];
           $tyr_user_obj->suburb = $row['suburb'];
           $tyr_user_obj->display_name = $row['display_name'];
           $tyr_user_obj->job_title = $row['job_title'];
           $tyr_user_obj->availability = $row['availability'];
           $tyr_user_obj->publish_account = $row['publish_account'];
           $tyr_user_obj->status_sl = $row['status_sl'];
           $tyr_user_obj->created_timestamp = $row['created_timestamp'];
           $tyr_user_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }
    
    public function get_user_handle(&$tyr_user_obj) {
        $firstname = strtolower(str_replace(" ", "", $tyr_user_obj->firstname));
        $lastname = strtolower(str_replace(" ", "", $tyr_user_obj->lastname));
        
        $query = 'select count(user_id) as user_count from '. TABLE_USERS .' where lower(firstname) = :firstname and lower(lastname) = :lastname';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':firstname', $firstname);
        $statement->bindParam(':lastname', $lastname);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $count = '';
        if (($row = $statement->fetch()) != FALSE) {
            $count = $row['user_count'];
        }
        if(intval($count) == 0)$count='';
        return $firstname.$lastname.$count;
    }
    
    public function get_all_team_member_of_studio(&$tyr_user_obj) {
        $query = "SELECT user_id FROM tyr_users WHERE parent_id = :parent_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':parent_id', $tyr_user_obj->parent_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row['user_id'];
        }
        return $return_array;
    }
    
    public function get_users_details($comma_seperated_user_id){
        $query = 'select * from tyr_users where user_id in ('.$comma_seperated_user_id.')';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}
