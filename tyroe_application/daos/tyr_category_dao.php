<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_category_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_category(&$tyr_category_obj) {
        $query = 'INSERT into tyr_category(
                   category_name, created_at, created_by, updated_at, updated_by
                  ) values(
                   :category_name, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':category_name', $tyr_category_obj->category_name);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_category_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_category_obj->created_updated_by);
        $statement->execute();
        $tyr_category_obj->category_id = $this->db_connection->lastInsertId('tyr_category_category_id_seq');
    }

    public function get_category(&$tyr_category_obj) {
        $query = 'select * from tyr_category where category_id = :category_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':category_id', $tyr_category_obj->category_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_category_obj->category_id = $row['category_id'];
           $tyr_category_obj->category_name = $row['category_name'];
        }
    }
    
}