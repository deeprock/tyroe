<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_status_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_status(&$tyr_job_status_obj) {
        $query = 'INSERT into tyr_job_status(
                    status_type, created_at, created_by, updated_at, updated_by
                  ) values(
                    :status_type, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_type', $tyr_job_status_obj->status_type);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_status_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_status_obj->created_updated_by);
        $statement->execute();
        $tyr_job_status_obj->job_status_id = $this->db_connection->lastInsertId('tyr_job_status_job_status_id_seq');
    }

    public function get_job_status(&$tyr_job_status_obj) {
        $query = 'select * from tyr_job_status where job_status_id = :job_status_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_status_id', $tyr_job_status_obj->job_status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_status_obj->job_status_id = $row['job_status_id'];
           $tyr_job_status_obj->status_type = $row['status_type'];
        }
    }
    
}