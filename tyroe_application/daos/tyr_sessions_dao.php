<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_sessions_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_sessions(&$tyr_sessions_obj) {
        $query = 'INSERT into tyr_sessions(
                    session_id, ip_address, user_agent, last_activity, user_data, created_at, created_by, updated_at, updated_by
                  ) values(
                    :session_id, :ip_address, :user_agent, :last_activity, :user_data, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':session_id', $tyr_sessions_obj->session_id);
        $statement->bindParam(':ip_address', $tyr_sessions_obj->ip_address);
        $statement->bindParam(':user_agent', $tyr_sessions_obj->user_agent);
        $statement->bindParam(':last_activity', $tyr_sessions_obj->last_activity);
        $statement->bindParam(':user_data', $tyr_sessions_obj->user_data);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_sessions_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_sessions_obj->created_updated_by);
        $statement->execute();
        $tyr_sessions_obj->id = $this->db_connection->lastInsertId('tyr_sessions_id_seq');
    }

    public function get_sessions(&$tyr_sessions_obj) {
        $query = 'select * from tyr_sessions where sessions_id = :sessions_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':sessions_id', $tyr_sessions_obj->sessions_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_sessions_obj->id = $row['id'];
           $tyr_sessions_obj->session_id = $row['session_id'];
           $tyr_sessions_obj->ip_address = $row['ip_address'];
           $tyr_sessions_obj->user_agent = $row['user_agent'];
           $tyr_sessions_obj->last_activity = $row['last_activity'];
           $tyr_sessions_obj->user_data = $row['user_data'];
        }
    }
    
}