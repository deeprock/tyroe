<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_comment_profile_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_comment_profile(&$tyr_comment_profile_obj) {
        $query = 'INSERT into tyr_comment_profile(
                   user_id, studio_id, comment_description, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :studio_id, :comment_description, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_comment_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_comment_profile_obj->studio_id);
        $statement->bindParam(':comment_description', $tyr_comment_profile_obj->comment_description);
        $statement->bindParam(':status_sl', $tyr_comment_profile_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_comment_profile_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_comment_profile_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_comment_profile_obj->created_updated_by);
        $statement->execute();
        $tyr_comment_profile_obj->comm_id = $this->db_connection->lastInsertId('tyr_comment_profile_comm_id_seq');
    }

    public function get_comment_profile(&$tyr_comment_profile_obj) {
        $query = 'select * from tyr_comment_profile where comm_id = :comm_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':comm_id', $tyr_comment_profile_obj->comm_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_comment_profile_obj->comm_id = $row['comm_id'];
           $tyr_comment_profile_obj->user_id = $row['user_id'];
           $tyr_comment_profile_obj->studio_id = $row['studio_id'];
           $tyr_comment_profile_obj->comment_description = $row['comment_description'];
           $tyr_comment_profile_obj->status_sl = $row['status_sl'];
           $tyr_comment_profile_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
}