<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_gallery_tags_rel_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_gallery_tags_rel(&$tyr_gallery_tags_rel_obj) {
        $query = 'INSERT into tyr_gallery_tags_rel(
                   gallerytag_name_id, media_image_id, add_time, delete_time, status, created_at, created_by, updated_at, updated_by
                  ) values(
                   :gallerytag_name_id, :media_image_id, :add_time, :delete_time, :status, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':gallerytag_name_id', $tyr_gallery_tags_rel_obj->gallerytag_name_id);
        $statement->bindParam(':media_image_id', $tyr_gallery_tags_rel_obj->media_image_id);
        $statement->bindParam(':add_time', $tyr_gallery_tags_rel_obj->add_time);
        $statement->bindParam(':delete_time', $tyr_gallery_tags_rel_obj->delete_time);
        $statement->bindParam(':status', $tyr_gallery_tags_rel_obj->status);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_gallery_tags_rel_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_gallery_tags_rel_obj->created_updated_by);
        $statement->execute();
        $tyr_gallery_tags_rel_obj->gallerytag_id = $this->db_connection->lastInsertId('tyr_gallery_tags_rel_gallerytag_id_seq');
    }

    public function get_gallery_tags_rel(&$tyr_gallery_tags_rel_obj) {
        $query = 'select * from tyr_gallery_tags_rel where gallerytag_id = :gallerytag_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':gallerytag_id', $tyr_gallery_tags_rel_obj->gallerytag_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_gallery_tags_rel_obj->gallerytag_id = $row['gallerytag_id'];
           $tyr_gallery_tags_rel_obj->gallerytag_name_id = $row['gallerytag_name_id'];
           $tyr_gallery_tags_rel_obj->media_image_id = $row['media_image_id'];
           $tyr_gallery_tags_rel_obj->add_time = $row['add_time'];
           $tyr_gallery_tags_rel_obj->delete_time = $row['delete_time'];
           $tyr_gallery_tags_rel_obj->status = $row['status'];
        }
    }
    
    public function get_already_tag_ref(&$tyr_gallery_tags_rel_obj) {
        $query = 'select * from tyr_gallery_tags_rel where media_image_id = :media_image_id and gallerytag_name_id = :gallerytag_name_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':media_image_id', $tyr_gallery_tags_rel_obj->media_image_id);
        $statement->bindParam(':gallerytag_name_id', $tyr_gallery_tags_rel_obj->gallerytag_name_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $temp_array = array(); 
           $temp_array['gallerytag_id'] = $row['gallerytag_id'];
           $temp_array['gallerytag_name_id'] = $row['gallerytag_name_id'];
           $temp_array['media_image_id'] = $row['media_image_id'];
           $temp_array['add_time'] = $row['add_time'];
           $temp_array['delete_time'] = $row['delete_time'];
           $temp_array['status'] = $row['status'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function update_media_tag_image_status(&$tyr_gallery_tags_rel_obj) {
        $query = 'update tyr_gallery_tags_rel set status = :status, updated_at = :updated_at where media_image_id = :media_image_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status', $tyr_gallery_tags_rel_obj->status);
        $statement->bindParam(':media_image_id', $tyr_gallery_tags_rel_obj->media_image_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function update_tag_rel_status(&$tyr_gallery_tags_rel_obj) {
        $query = 'update tyr_gallery_tags_rel set status = :status, updated_at = :updated_at where media_image_id = :media_image_id and gallerytag_name_id = :gallerytag_name_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status', $tyr_gallery_tags_rel_obj->status);
        $statement->bindParam(':media_image_id', $tyr_gallery_tags_rel_obj->media_image_id);
        $statement->bindParam(':gallerytag_name_id', $tyr_gallery_tags_rel_obj->gallerytag_name_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function update_status_name_id_not_in(&$tyr_gallery_tags_rel_obj,$where_not_in_update) {
        $query = 'update tyr_gallery_tags_rel set status = :status, updated_at = :updated_at where media_image_id = :media_image_id and gallerytag_name_id not in('.$where_not_in_update.')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status', $tyr_gallery_tags_rel_obj->status);
        $statement->bindParam(':media_image_id', $tyr_gallery_tags_rel_obj->media_image_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
}