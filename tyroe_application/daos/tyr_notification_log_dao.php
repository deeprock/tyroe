<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_notification_log_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_notification_log(&$tyr_notification_log_obj) {
        $query = 'INSERT into tyr_notification_log(
                    send_to, notification_id, notification_status, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                    :send_to, :notification_id, :notification_status, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':send_to', $tyr_notification_log_obj->send_to);
        $statement->bindParam(':notification_id', $tyr_notification_log_obj->notification_id);
        $statement->bindParam(':notification_status', $tyr_notification_log_obj->notification_status);
        $statement->bindParam(':status_sl', $tyr_notification_log_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_notification_log_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_notification_log_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_notification_log_obj->created_updated_by);
        $statement->execute();
        $tyr_notification_log_obj->notfiy_log_id = $this->db_connection->lastInsertId('tyr_notification_log_notfiy_log_id_seq');
    }

    public function get_notification_log(&$tyr_notification_log_obj) {
        $query = 'select * from tyr_notification_log where notfiy_log_id = :notfiy_log_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':notfiy_log_id', $tyr_notification_log_obj->notfiy_log_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_notification_log_obj->notfiy_log_id = $row['notfiy_log_id'];
           $tyr_notification_log_obj->send_to = $row['send_to'];
           $tyr_notification_log_obj->notification_id = $row['notification_id'];
           $tyr_notification_log_obj->notification_status = $row['notification_status'];
           $tyr_notification_log_obj->status_sl = $row['status_sl'];
           $tyr_notification_log_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
}