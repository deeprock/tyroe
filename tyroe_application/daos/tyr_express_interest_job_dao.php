<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_express_interest_job_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_express_interest_job(&$tyr_express_interest_job_obj) {
        $query = 'INSERT into tyr_express_interest_job(
                   tyroe_id, job_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :tyroe_id, :job_id, :status_sl, created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_express_interest_job_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_express_interest_job_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_express_interest_job_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_express_interest_job_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_express_interest_job_obj->created_updated_by);
        $statement->execute();
        $tyr_express_interest_job_obj->express_id = $this->db_connection->lastInsertId('tyr_express_interest_job_express_id_seq');
    }

    public function get_express_interest_job(&$tyr_express_interest_job_obj) {
        $query = 'select * from tyr_express_interest_job where express_id = :express_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':express_id', $tyr_express_interest_job_obj->express_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_express_interest_job_obj->express_id = $row['express_id'];
           $tyr_express_interest_job_obj->tyroe_id = $row['tyroe_id'];
           $tyr_express_interest_job_obj->job_id = $row['job_id'];
           $tyr_express_interest_job_obj->status_sl = $row['status_sl'];
        }
    }
    
}