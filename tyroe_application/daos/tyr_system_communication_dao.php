<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_system_communication_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_system_communication(&$tyr_system_communication_obj) {
        $query = 'INSERT into tyr_system_communication(
                    user_id, option_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :option_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_system_communication_obj->user_id);
        $statement->bindParam(':option_id', $tyr_system_communication_obj->option_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_system_communication_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_system_communication_obj->created_updated_by);
        $statement->execute();
        $tyr_system_communication_obj->system_com_id = $this->db_connection->lastInsertId('tyr_system_communication_system_com_id_seq');
    }

    public function get_system_communication(&$tyr_system_communication_obj) {
        $query = 'select * from tyr_system_communication where system_com_id = :system_com_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':system_com_id', $tyr_system_communication_obj->system_com_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_system_communication_obj->system_com_id = $row['system_com_id'];
           $tyr_system_communication_obj->user_id = $row['user_id'];
           $tyr_system_communication_obj->option_id = $row['option_id'];
        }
    }
    
}