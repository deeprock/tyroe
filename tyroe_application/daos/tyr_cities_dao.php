<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_cities_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_cities(&$tyr_cities_obj) {
        $query = 'INSERT into tyr_cities(
                   country_id, city, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :country_id, :city, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_cities_obj->country_id);
        $statement->bindParam(':city', $tyr_cities_obj->city);
        $statement->bindParam(':status_sl', $tyr_cities_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_cities_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_cities_obj->created_updated_by);
        $statement->execute();
        $tyr_cities_obj->city_id = $this->db_connection->lastInsertId('tyr_cities_city_id_seq');
    }

    public function get_cities(&$tyr_cities_obj) {
        $query = 'select * from tyr_cities where city_id = :city_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':city_id', $tyr_cities_obj->city_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_cities_obj->city_id = $row['city_id'];
           $tyr_cities_obj->country_id = $row['country_id'];
           $tyr_cities_obj->city = $row['city'];
           $tyr_cities_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_cities_by_country_id(&$tyr_cities_obj) {
        if($tyr_cities_obj->country_id == '')return array();
        $query = "SELECT city_id AS id,city AS name FROM tyr_cities WHERE country_id = :country_id ORDER BY city ASC";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_cities_obj->country_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $temp_array = array(); 
           $temp_array['id'] = $row['id'];
           $temp_array['name'] = $row['name'];
           $temp_array['city_id'] = $row['id'];
           $temp_array['city'] = $row['name'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_all_cities_by_keyword($search_input) {
        $query = "SELECT array_to_string(array_agg(" . TABLE_CITIES . ".city_id ORDER BY " . TABLE_CITIES . ".city_id"
                    . " ASC), ',') AS city_id  FROM " . TABLE_CITIES . " WHERE lower(city) LIKE '%" . $search_input . "%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_cities_by_country_where(&$tyr_cities_obj,$cities_not_in) {
        $query = "SELECT city_id AS id,city AS name FROM tyr_cities WHERE country_id = :country_id and city_id NOT IN(".$cities_not_in.") ORDER BY city ASC";
        //echo $query;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_cities_obj->country_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $temp_array = array(); 
           $temp_array['id'] = $row['id'];
           $temp_array['name'] = $row['name'];
           $temp_array['city_id'] = $row['id'];
           $temp_array['city'] = $row['name'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
}