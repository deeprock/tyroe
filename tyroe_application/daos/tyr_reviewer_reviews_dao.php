<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_reviewer_reviews_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_reviewer_reviews(&$tyr_reviewer_reviews_obj) {
        $query = 'INSERT into tyr_reviewer_reviews(
                    studio_id, job_id, reviewer_id, tyroe_id, reviewed_type, reviewed_time, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :studio_id, :job_id, :reviewer_id, :tyroe_id, :reviewed_type, :reviewed_time, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_reviews_obj->reviewer_id);
        $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
        $statement->bindParam(':reviewed_type', $tyr_reviewer_reviews_obj->reviewed_type);
        $statement->bindParam(':reviewed_time', $tyr_reviewer_reviews_obj->reviewed_time);
        $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_reviewer_reviews_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_reviewer_reviews_obj->created_updated_by);
        $statement->execute();
        $tyr_reviewer_reviews_obj->reviewer_reviews_id = $this->db_connection->lastInsertId('tyr_reviewer_reviews_reviewer_reviews_id_seq');
    }

    public function get_reviewer_reviews(&$tyr_reviewer_reviews_obj) {
        $query = 'select * from tyr_reviewer_reviews where reviewer_reviews_id = :reviewer_reviews_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_reviews_id', $tyr_reviewer_reviews_obj->reviewer_reviews_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_reviewer_reviews_obj->reviewer_reviews_id = $row['reviewer_reviews_id'];
           $tyr_reviewer_reviews_obj->studio_id = $row['studio_id'];
           $tyr_reviewer_reviews_obj->job_id = $row['job_id'];
           $tyr_reviewer_reviews_obj->reviewer_id = $row['reviewer_id'];
           $tyr_reviewer_reviews_obj->tyroe_id = $row['tyroe_id'];
           $tyr_reviewer_reviews_obj->reviewed_type = $row['reviewed_type'];
           $tyr_reviewer_reviews_obj->reviewed_time = $row['reviewed_time'];
           $tyr_reviewer_reviews_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_reviewer_reviews_count(&$tyr_reviewer_reviews_obj) {
        $query = 'SELECT COUNT(*) AS tyr_review, tyroe_id from tyr_reviewer_reviews where tyroe_id = :tyroe_id and studio_id = :studio_id and job_id = :job_id and status_sl = 1';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_tyroe_reviews_count(&$tyr_reviewer_reviews_obj) {
        if(intval($tyr_reviewer_reviews_obj->reviewer_id) != 0){
            $query = 'SELECT COUNT(*) AS tyr_review, tyroe_id from tyr_reviewer_reviews where reviewer_id = :reviewer_id and'
                    . ' tyroe_id = :tyroe_id and studio_id = :studio_id and job_id = :job_id and status_sl = :status_sl group by tyr_reviewer_reviews.tyroe_id';
            $statement = $this->db_connection->prepare($query);
            $statement->bindParam(':reviewer_id', $tyr_reviewer_reviews_obj->reviewer_id);
            $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
            $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
            $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
            $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        }else{
            $query = 'SELECT COUNT(*) AS tyr_review, tyroe_id from tyr_reviewer_reviews where '
                    . ' tyroe_id = :tyroe_id and studio_id = :studio_id and job_id = :job_id and status_sl = :status_sl group by tyr_reviewer_reviews.tyroe_id';
            $statement = $this->db_connection->prepare($query);
            $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
            $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
            $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
            $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        }
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    
    public function get_reviews_id_count_for_job(&$tyr_reviewer_reviews_obj) {
        $query = 'SELECT COUNT(reviewer_reviews_id) AS total_reviews from tyr_reviewer_reviews where '
                . 'studio_id = :studio_id and job_id = :job_id and status_sl = :status_sl and reviewer_id = :reviewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_reviews_obj->reviewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_reviewer_reviews(&$tyr_reviewer_reviews_obj) {
        $query = 'update tyr_reviewer_reviews set studio_id = :studio_id, job_id = :job_id, reviewer_id = :reviewer_id, tyroe_id = :tyroe_id, reviewed_type = :reviewed_type, reviewed_time = :reviewed_time, status_sl = :status_sl, updated_at = :updated_at where reviewer_reviews_id = :reviewer_reviews_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_reviews_obj->reviewer_id);
        $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
        $statement->bindParam(':reviewed_type', $tyr_reviewer_reviews_obj->reviewed_type);
        $statement->bindParam(':reviewed_time', $tyr_reviewer_reviews_obj->reviewed_time);
        $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':reviewer_reviews_id', $tyr_reviewer_reviews_obj->reviewer_reviews_id);
        $statement->execute();
        return true;
    }
    
    public function update_reviewer_reviews_status(&$tyr_reviewer_reviews_obj) {
        $query = 'update tyr_reviewer_reviews set status_sl = :status_sl, updated_at = :updated_at where studio_id = :studio_id AND '
                . 'job_id = :job_id AND reviewer_id = :reviewer_id AND tyroe_id = :tyroe_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->bindParam(':reviewer_id', $tyr_reviewer_reviews_obj->reviewer_id);
        $statement->bindParam(':tyroe_id', $tyr_reviewer_reviews_obj->tyroe_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function get_all_reviews_count_by_job_id(&$tyr_reviewer_reviews_obj, $reviewer_ids){
        $query = 'select count(*) as review_count from tyr_reviewer_reviews where job_id = :job_id and studio_id = :studio_id and status_sl = :status_sl'
                . ' and reviewer_id in ('.$reviewer_ids.')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_reviewer_reviews_obj->status_sl);
        $statement->bindParam(':studio_id', $tyr_reviewer_reviews_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_reviewer_reviews_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}