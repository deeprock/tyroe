<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_invite_reviewer_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_invite_reviewer(&$tyr_invite_reviewer_obj) {
        $query = 'INSERT into tyr_invite_reviewer(
                   invite_email, invite_job_id, invite_studio_id, status_sl, created_time, created_at, created_by, updated_at, updated_by
                  ) values(
                   :invite_email, :invite_job_id, :invite_studio_id, :status_sl, :created_time, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_email', $tyr_invite_reviewer_obj->invite_email);
        $statement->bindParam(':invite_job_id', $tyr_invite_reviewer_obj->invite_job_id);
        $statement->bindParam(':invite_studio_id', $tyr_invite_reviewer_obj->invite_studio_id);
        $statement->bindParam(':status_sl', $tyr_invite_reviewer_obj->status_sl);
        $statement->bindParam(':created_time', $tyr_invite_reviewer_obj->created_time);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_invite_reviewer_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_invite_reviewer_obj->created_updated_by);
        $statement->execute();
        $tyr_invite_reviewer_obj->invite_reviewer_id = $this->db_connection->lastInsertId('tyr_invite_reviewer_invite_reviewer_id_seq');
    }

    public function get_invite_reviewer(&$tyr_invite_reviewer_obj) {
        $query = 'select * from tyr_invite_reviewer where invite_reviewer_id = :invite_reviewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_reviewer_id', $tyr_invite_reviewer_obj->invite_reviewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_invite_reviewer_obj->invite_reviewer_id = $row['invite_reviewer_id'];
           $tyr_invite_reviewer_obj->invite_email = $row['invite_email'];
           $tyr_invite_reviewer_obj->invite_job_id = $row['invite_job_id'];
           $tyr_invite_reviewer_obj->invite_studio_id = $row['invite_studio_id'];
           $tyr_invite_reviewer_obj->status_sl = $row['status_sl'];
           $tyr_invite_reviewer_obj->created_time = $row['created_time'];
        }
    }
    
    public function check_insert_invite_reviewer(&$tyr_invite_reviewer_obj) {
        $query = 'select * from tyr_invite_reviewer where invite_email = :invite_email and invite_job_id = :invite_job_id and invite_studio_id = :invite_studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_email', $tyr_invite_reviewer_obj->invite_email);
        $statement->bindParam(':invite_job_id', $tyr_invite_reviewer_obj->invite_job_id);
        $statement->bindParam(':invite_studio_id', $tyr_invite_reviewer_obj->invite_studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_invite_reviewer_obj->invite_reviewer_id = $row['invite_reviewer_id'];
           $tyr_invite_reviewer_obj->invite_email = $row['invite_email'];
           $tyr_invite_reviewer_obj->invite_job_id = $row['invite_job_id'];
           $tyr_invite_reviewer_obj->invite_studio_id = $row['invite_studio_id'];
           $tyr_invite_reviewer_obj->status_sl = $row['status_sl'];
           $tyr_invite_reviewer_obj->created_time = $row['created_time'];
        }
    }
    
}