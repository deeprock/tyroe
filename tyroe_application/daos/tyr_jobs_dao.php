<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_jobs_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_jobs(&$tyr_jobs_obj) {
        $query = 'INSERT into tyr_jobs(
                   user_id, category_id, country_id, job_level_id, job_city, job_title, job_description, job_location, company_title, start_date, end_date, job_quantity, job_type, job_duration, team_moderation, status_sl, archived_status, industry_id, created_timestamp, modified_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :category_id, :country_id, :job_level_id, :job_city, :job_title, :job_description, :job_location, :company_title, :start_date, :end_date, :job_quantity, :job_type, :job_duration, :team_moderation, :status_sl, :archived_status, :industry_id, :created_timestamp, :modified_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':category_id', $tyr_jobs_obj->category_id);
        $statement->bindParam(':country_id', $tyr_jobs_obj->country_id);
        $statement->bindParam(':job_level_id', $tyr_jobs_obj->job_level_id);
        $statement->bindParam(':job_city', $tyr_jobs_obj->job_city);
        $statement->bindParam(':job_title', $tyr_jobs_obj->job_title);
        $statement->bindParam(':job_description', $tyr_jobs_obj->job_description);
        $statement->bindParam(':job_location', $tyr_jobs_obj->job_location);
        $statement->bindParam(':company_title', $tyr_jobs_obj->company_title);
        $statement->bindParam(':start_date', $tyr_jobs_obj->start_date);
        $statement->bindParam(':end_date', $tyr_jobs_obj->end_date);
        $statement->bindParam(':job_quantity', $tyr_jobs_obj->job_quantity);
        $statement->bindParam(':job_type', $tyr_jobs_obj->job_type);
        $statement->bindParam(':job_duration', $tyr_jobs_obj->job_duration);
        $statement->bindParam(':team_moderation', $tyr_jobs_obj->team_moderation);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->bindParam(':industry_id', $tyr_jobs_obj->industry_id);
        $statement->bindParam(':created_timestamp', $tyr_jobs_obj->created_timestamp);
        $statement->bindParam(':modified_timestamp', $tyr_jobs_obj->modified_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_jobs_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_jobs_obj->created_updated_by);
        $statement->execute();
        $tyr_jobs_obj->job_id = $this->db_connection->lastInsertId('tyr_jobs_job_id_seq');
    }

    public function get_jobs(&$tyr_jobs_obj) {
        $query = 'select * from tyr_jobs where job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_jobs_obj->job_id = $row['job_id'];
           $tyr_jobs_obj->user_id = $row['user_id'];
           $tyr_jobs_obj->category_id = $row['category_id'];
           $tyr_jobs_obj->country_id = $row['country_id'];
           $tyr_jobs_obj->job_level_id = $row['job_level_id'];
           $tyr_jobs_obj->job_city = $row['job_city'];
           $tyr_jobs_obj->job_title = $row['job_title'];
           $tyr_jobs_obj->job_description = $row['job_description'];
           $tyr_jobs_obj->job_location = $row['job_location'];
           $tyr_jobs_obj->company_title = $row['company_title'];
           $tyr_jobs_obj->start_date = $row['start_date'];
           $tyr_jobs_obj->end_date = $row['end_date'];
           $tyr_jobs_obj->job_quantity = $row['job_quantity'];
           $tyr_jobs_obj->job_type = $row['job_type'];
           $tyr_jobs_obj->job_duration = $row['job_duration'];
           $tyr_jobs_obj->team_moderation = $row['team_moderation'];
           $tyr_jobs_obj->status_sl = $row['status_sl'];
           $tyr_jobs_obj->archived_status = $row['archived_status'];
           $tyr_jobs_obj->industry_id = $row['industry_id'];
           $tyr_jobs_obj->created_timestamp = $row['created_timestamp'];
           $tyr_jobs_obj->modified_timestamp = $row['modified_timestamp'];
        }
    }
    
    public function get_all_new_jobs(&$tyr_jobs_obj,$last_login_time) {
        $query = 'SELECT job_id FROM tyr_jobs WHERE created_timestamp > '.$last_login_time.' AND status_sl = 1 AND archived_status = 0';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $data = array();
        while($row = $statement->fetch()){
            $data[] = $row['job_id']; 
        }
        return $data;
    }
    
    public function get_all_jobs_by_status(&$tyr_jobs_obj) {
        $query = 'SELECT * FROM tyr_jobs Where status_sl= :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_job_id_count_by_status(&$tyr_jobs_obj) {
        $query = 'SELECT COUNT(DISTINCT job_id) as cnt FROM tyr_jobs Where status_sl= :status_sl and archived_status = :archived_status';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_count_by_user(&$tyr_jobs_obj) {
        $query = 'SELECT COUNT(*) as cnt FROM tyr_jobs where user_id = :user_id and status_sl = :status_sl and archived_status = :archived_status';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_in_progress(&$tyr_jobs_obj) {
        $query = 'SELECT count(*) as jobs_in_progress FROM tyr_jobs Where archived_status = :archived_status and status_sl= :status_sl and'
                . ' end_date >= :end_date';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':end_date', $tyr_jobs_obj->end_date);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
     public function get_cont_admin_reviews(&$tyr_jobs_obj) {
        $query = 'SELECT count(*) as jobs_in_progress FROM tyr_jobs Where status_sl= :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_by_reviewer_id(&$tyr_jobs_obj) {
        $query = "SELECT array_to_string(array_agg(job_id ORDER BY job_id ASC), ',') As job_id, reviewer_id FROM tyr_jobs WHERE reviewer_id = :reviewer_id GROUP BY reviewer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_jobs_obj->reviewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_active_job_count(&$tyr_jobs_obj) {
        $query = 'SELECT count(job_id) as total_active_job FROM tyr_jobs WHERE user_id = :user_id and status_sl = :status_sl and archived_status = :archived_status';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_openings_by_user(&$tyr_jobs_obj) {
        $query = 'select tyr_jobs.job_id, job_title from tyr_jobs inner join tyr_job_role_mapping on tyr_jobs.job_id = tyr_job_role_mapping.job_id '
                . 'where (tyr_job_role_mapping.is_admin = 1 or tyr_job_role_mapping.is_reviewer = 1) and tyr_job_role_mapping.user_id = :user_id and '
                . 'tyr_jobs.status_sl = :status_sl and tyr_jobs.archived_status = :archived_status';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_jobs_openings_by_user_1(&$tyr_jobs_obj) {
        $query = 'SELECT job_id, job_title FROM tyr_jobs Where user_id = :user_id, status_sl= :status_sl and archived_status = :archived_status and job_id != :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['job_id'] = $row['job_id'];
           $temp_array['job_title'] = $row['job_title'];
           $return_array[] = $temp_array;
        }
        var_dump($return_array);
        return $return_array;
    }
    
    public function update_job_status_by_id(&$tyr_jobs_obj) {
        $query = 'update tyr_jobs set status_sl = :status_sl, modified_timestamp = :modified_timestamp, updated_at = :updated_at where job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':modified_timestamp', $tyr_jobs_obj->modified_timestamp);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->execute();
        return true;
    }
    
    public function update_job_archived_status_by_id(&$tyr_jobs_obj) {
        $query = 'update tyr_jobs set archived_status = :archived_status, updated_at = :updated_at where job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->execute();
        return true;
    }
    
    public function get_job_details_by_user_job(&$tyr_jobs_obj) {
        $query = 'SELECT * FROM tyr_jobs Where user_id= :user_id and job_id= :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['job_id'] = $row['job_id'];
           $return_array['user_id'] = $row['user_id'];
           $return_array['category_id'] = $row['category_id'];
           $return_array['country_id'] = $row['country_id'];
           $return_array['job_level_id'] = $row['job_level_id'];
           $return_array['job_city'] = $row['job_city'];
           $return_array['job_title'] = $row['job_title'];
           $return_array['job_description'] = $row['job_description'];
           $return_array['job_location'] = $row['job_location'];
           $return_array['company_title'] = $row['company_title'];
           $return_array['start_date'] = $row['start_date'];
           $return_array['end_date'] = $row['end_date'];
           $return_array['job_quantity'] = $row['job_quantity'];
           $return_array['job_type'] = $row['job_type'];
           $return_array['job_duration'] = $row['job_duration'];
           $return_array['team_moderation'] = $row['team_moderation'];
           $return_array['status_sl'] = $row['status_sl'];
           $return_array['archived_status'] = $row['archived_status'];
           $return_array['industry_id'] = $row['industry_id'];
           $return_array['created_at'] = $row['created_at'];
           $return_array['updated_at'] = $row['updated_at'];
           $return_array['created_timestamp'] = $row['created_timestamp'];
           $return_array['modified_timestamp'] = $row['modified_timestamp'];
           
        }
        return $return_array;
    }
    
    public function update_jobs(&$tyr_jobs_obj) {
        $query = 'update tyr_jobs set user_id = :user_id,'
                . ' category_id = :category_id,'
                . ' country_id = :country_id,'
                . ' job_level_id = :job_level_id,'
                . ' job_city = :job_city,'
                . ' job_title = :job_title,'
                . ' job_description = :job_description,'
                . ' job_location = :job_location,'
                . ' company_title = :company_title,'
                . ' start_date = :start_date,'
                . ' end_date = :end_date,'
                . ' job_quantity = :job_quantity,'
                . ' job_type = :job_type,'
                . ' job_duration = :job_duration,'
                . ' team_moderation = :team_moderation,'
                . ' status_sl = :status_sl,'
                . ' archived_status = :archived_status,'
                . ' industry_id = :industry_id,'
                . ' updated_at = :updated_at,'
                . ' updated_by = :updated_by,'
                . ' modified_timestamp = :modified_timestamp'
                . ' where job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':category_id', $tyr_jobs_obj->category_id);
        $statement->bindParam(':country_id', intval($tyr_jobs_obj->country_id));
        $statement->bindParam(':job_level_id', $tyr_jobs_obj->job_level_id);
        $statement->bindParam(':job_city', $tyr_jobs_obj->job_city);
        $statement->bindParam(':job_title', $tyr_jobs_obj->job_title);
        $statement->bindParam(':job_description', $tyr_jobs_obj->job_description);
        $statement->bindParam(':job_location', $tyr_jobs_obj->job_location);
        $statement->bindParam(':company_title', $tyr_jobs_obj->company_title);
        $statement->bindParam(':start_date', $tyr_jobs_obj->start_date);
        $statement->bindParam(':end_date', $tyr_jobs_obj->end_date);
        $statement->bindParam(':job_quantity', $tyr_jobs_obj->job_quantity);
        $statement->bindParam(':job_type', $tyr_jobs_obj->job_type);
        $statement->bindParam(':job_duration', $tyr_jobs_obj->job_duration);
        $statement->bindParam(':team_moderation', $tyr_jobs_obj->team_moderation);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':archived_status', $tyr_jobs_obj->archived_status);
        $statement->bindParam(':industry_id', $tyr_jobs_obj->industry_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_jobs_obj->user_id);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->bindParam(':modified_timestamp', $tyr_jobs_obj->modified_timestamp);
        $statement->execute();
        return true;
    }
    
    public function check_moderation_status(&$tyr_jobs_obj) {
        $query = "SELECT team_moderation FROM tyr_jobs WHERE job_id = :job_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_jobs_obj->status_sl);
        $statement->bindParam(':job_id', $tyr_jobs_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_distinct_job_cities(&$tyr_jobs_obj) {
        $query = "SELECT DISTINCT(job_city) FROM tyr_jobs";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function archive_job_by_user(&$tyr_jobs_obj) {
        $archived_status = 1;
        $query = "update tyr_jobs set archived_status = :archived_status where user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_jobs_obj->user_id);
        $statement->bindParam(':archived_status', $archived_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}