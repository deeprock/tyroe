<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_detail_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_detail(&$tyr_job_detail_obj) {
        $query = 'INSERT into tyr_job_detail(
                   job_status_id, job_id, tyroe_id, reviewer_id, invitation_status, invite_notification_status,
                   reviewer_type, job_donute_review_status, newest_candidate_status, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :job_status_id, :job_id, :tyroe_id, :reviewer_id, :invitation_status, :invite_notification_status,
                   :reviewer_type, :job_donute_review_status, :newest_candidate_status, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':invitation_status', intval($tyr_job_detail_obj->invitation_status));
        $statement->bindParam(':invite_notification_status', $tyr_job_detail_obj->invite_notification_status);
        $statement->bindParam(':reviewer_type', $tyr_job_detail_obj->reviewer_type);
        $statement->bindParam(':job_donute_review_status', $tyr_job_detail_obj->job_donute_review_status);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_detail_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_detail_obj->created_updated_by);
        $statement->execute();
        $tyr_job_detail_obj->job_detail_id = $this->db_connection->lastInsertId('tyr_job_detail_job_detail_id_seq');
        return true;
    }

    public function get_job_detail(&$tyr_job_detail_obj) {
        $query = 'select * from tyr_job_detail where job_detail_id = :job_detail_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_detail_id', $tyr_job_detail_obj->job_detail_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_detail_obj->job_detail_id = intval($row['job_detail_id']);
           $tyr_job_detail_obj->job_status_id = intval($row['job_status_id']);
           $tyr_job_detail_obj->job_id = intval($row['job_id']);
           $tyr_job_detail_obj->tyroe_id = intval($row['tyroe_id']);
           $tyr_job_detail_obj->reviewer_id = intval($row['reviewer_id']);
           $tyr_job_detail_obj->invitation_status = intval($row['invitation_status']);
           $tyr_job_detail_obj->invite_notification_status = intval($row['invite_notification_status']);
           $tyr_job_detail_obj->reviewer_type = intval($row['reviewer_type']);
           $tyr_job_detail_obj->job_donute_review_status = intval($row['job_donute_review_status']);
           $tyr_job_detail_obj->newest_candidate_status = intval($row['newest_candidate_status']);
           $tyr_job_detail_obj->status_sl = intval($row['status_sl']);
        }
    }
    
    public function get_all_new_shortlist_jobs(&$tyr_job_detail_obj,$last_login_time) {
        $query = "SELECT COUNT(job_id) AS shorlistjobcount, array_to_string(array_agg(job_id), ',') AS shortlistjobids FROM tyr_job_detail WHERE tyroe_id = :tyroe_id AND job_status_id = :job_status_id AND modified_timestamp > :modified_timestamp AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':modified_timestamp', $last_login_time);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row; 
        }
        return $return_array;
    }
    
    public function get_total_tyroes_from_job_details(&$tyr_job_detail_obj) {
        $query = 'SELECT * FROM tyr_job_detail WHERE reviewer_id = :reviewer_id AND job_status_id = :job_status_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row; 
        }
        return $return_array;
    }
    
    public function get_total_tyroes_by_job_id(&$tyr_job_detail_obj,$total_job_ids) {
        $query = 'SELECT * FROM tyr_job_detail WHERE reviewer_id = :reviewer_id AND job_status_id = :job_status_id AND '
                . 'job_id IN('.$total_job_ids.')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row; 
        }
        return $return_array;
    }
    
    public function get_candidate_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(job_status_id) AS total_candidates FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status != :invitation_status AND status_sl = :status_sl AND tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_applicant_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(job_status_id) AS total_applicants FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status = :invitation_status AND status_sl = :status_sl AND tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_applicants'] = $row['total_applicants'];
        }
        return $return_array;
    }
    
    public function get_shortlist_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(job_status_id) AS total_shortlist FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status = :invitation_status AND status_sl = :status_sl AND tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_shortlist'] = $row['total_shortlist'];
        }
        return $return_array;
    }
    
    public function get_new_candidate_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(newest_candidate_status) AS total_newest_candidates FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status != :invitation_status AND status_sl = :status_sl AND newest_candidate_status = :newest_candidate_status AND  tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_newest_candidates'] = $row['total_newest_candidates'];
        }
        return $return_array;
    }
    
    public function get_new_applicant_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(newest_candidate_status) AS total_newest_applicants FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status = :invitation_status AND status_sl = :status_sl AND newest_candidate_status = :newest_candidate_status AND  tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_newest_applicants'] = $row['total_newest_applicants'];
        }
        return $return_array;
    }
    
    public function get_new_shortlist_count_except_hidden(&$tyr_job_detail_obj,$hidden_user) {
        $query = 'SELECT COUNT(newest_candidate_status) AS total_newest_shortlist FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id = :job_status_id AND invitation_status = :invitation_status AND status_sl = :status_sl AND newest_candidate_status = :newest_candidate_status AND tyroe_id NOT IN ('.$hidden_user .')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['total_newest_shortlist'] = $row['total_newest_shortlist'];
        }
        return $return_array;
    }
    
    
    public function get_tyroe_job_ids(&$tyr_job_detail_obj) {
        $query = "SELECt job_id FROM " . TABLE_JOB_DETAIL ." WHERE tyroe_id = :tyroe_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array))$return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_total_tyroes_by_reviewer_job_id(&$tyr_job_detail_obj) {
        $query = 'SELECT * FROM tyr_job_detail WHERE reviewer_id = :reviewer_id AND job_status_id = :job_status_id AND job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array))$return_array = array();
           $return_array[] = $row; 
        }
        return $return_array;
    }
    
    public function get_total_tyroes_for_job(&$tyr_job_detail_obj) {
        $query = "SELECT COUNT(tyroe_id) AS cnt_tyroes, array_to_string(array_agg(tyr_job_detail.tyroe_id ORDER BY tyr_job_detail.tyroe_id ASC), ',') as total_tyroes FROM tyr_job_detail WHERE job_status_id = :job_status_id AND job_id = :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_alrady_apply_job_count(&$tyr_job_detail_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_job_detail WHERE job_id = :job_id AND tyroe_id = :tyroe_id AND status_sl= :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_today_apply_job_count(&$tyr_job_detail_obj) {
        $query = "SELECT COUNT(job_detail_id) AS today_job_count FROM tyr_job_detail WHERE tyroe_id = :tyroe_id AND status_sl = :status_sl"
                . " AND DATE(created_at) = current_date";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    
    public function update_job_detail(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set "
                . "job_status_id = :job_status_id,"
                . " job_id = :job_id,"
                . " tyroe_id = :tyroe_id,"
                . " reviewer_id = :reviewer_id,"
                . " invitation_status = :invitation_status,"
                . " invite_notification_status = :invite_notification_status,"
                . " reviewer_type = :reviewer_type,"
                . " job_donute_review_status = :job_donute_review_status,"
                . " newest_candidate_status = :newest_candidate_status,"
                . " status_sl = :status_sl,"
                . " updated_at = :updated_at,"
                . " updated_by = :updated_by"
                . " where job_detail_id = :job_detail_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id,  PDO::PARAM_INT);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id,  PDO::PARAM_INT);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id,  PDO::PARAM_INT);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id,  PDO::PARAM_INT);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status,  PDO::PARAM_INT);
        $statement->bindParam(':invite_notification_status', $tyr_job_detail_obj->invite_notification_status,  PDO::PARAM_INT);
        $statement->bindParam(':reviewer_type', $tyr_job_detail_obj->reviewer_type,  PDO::PARAM_INT);
        $statement->bindParam(':job_donute_review_status', $tyr_job_detail_obj->job_donute_review_status,  PDO::PARAM_INT);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status,  PDO::PARAM_INT);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl,  PDO::PARAM_INT);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_detail_obj->created_updated_by);
        $statement->bindParam(':job_detail_id', $tyr_job_detail_obj->job_detail_id,  PDO::PARAM_INT);
        $statement->execute();        
        return true;
    }
    
    public function update_job_invitation_status(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set invitation_status = :invitation_status, status_sl = :status_sl, updated_at = :updated_at where job_id = :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function update_job_invitation_status_by_tyroe(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set invitation_status = :invitation_status, updated_at = :updated_at where tyroe_id = :tyroe_id and job_id = :job_id and job_status_id = :job_status_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invitation_status', $tyr_job_detail_obj->invitation_status);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function update_newest_candidate_status(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set newest_candidate_status = :newest_candidate_status, updated_at = :updated_at where reviewer_id = :reviewer_id and job_id = :job_id and job_status_id = :job_status_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':newest_candidate_status', $tyr_job_detail_obj->newest_candidate_status);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function check_duplicate_entry_count(&$tyr_job_detail_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_job_detail WHERE job_id = :job_id AND tyroe_id = :tyroe_id AND status_sl= :status_sl AND reviewer_id= :reviewer_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':reviewer_id', $tyr_job_detail_obj->reviewer_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_job_detail_status(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set status_sl = :status_sl, updated_at = :updated_at where tyroe_id = :tyroe_id and job_id = :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_job_detail_obj->status_sl);
        $statement->bindParam(':tyroe_id', $tyr_job_detail_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function get_count_for_job_id_status($tyr_job_detail_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_job_detail WHERE job_id = :job_id AND job_status_id= :job_status_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_detail_obj->job_id);
        $statement->bindParam(':job_status_id', $tyr_job_detail_obj->job_status_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function update_invite_notification_status(&$tyr_job_detail_obj) {
        $query = "update tyr_job_detail set invite_notification_status = :invite_notification_status, updated_at = :updated_at where job_detail_id = :job_detail_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_notification_status', $tyr_job_detail_obj->invite_notification_status);
        $statement->bindParam(':job_detail_id', $tyr_job_detail_obj->job_detail_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function delete_job_details(&$tyr_job_detail_obj) {
        $query = "delete from tyr_job_detail where job_detail_id = :job_detail_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_detail_id', $tyr_job_detail_obj->job_detail_id);
        $statement->execute();
        return true;
    }
    
    
}