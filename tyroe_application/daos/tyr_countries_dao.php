<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_countries_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_countries(&$tyr_countries_obj) {
        $query = 'INSERT into tyr_countries(
                   country, created_at, created_by, updated_at, updated_by
                  ) values(
                   :country, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country', $tyr_countries_obj->country);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_countries_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_countries_obj->created_updated_by);
        $statement->execute();
        $tyr_countries_obj->country_id = $this->db_connection->lastInsertId('tyr_countries_country_id_seq');
    }

    public function get_countries(&$tyr_countries_obj) {
        $query = 'select * from tyr_countries where country_id = :country_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_countries_obj->country_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_countries_obj->country_id = $row['country_id'];
           $tyr_countries_obj->country = $row['country'];
        }
    }
    
    
    public function get_all_countries(&$tyr_countries_obj) {
        $query = 'select * from tyr_countries';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_countries_as() {
        $query = 'select country_id AS id,country AS name from tyr_countries';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_country_id_by_keyword($search_input) {
        $query = "SELECT array_to_string(array_agg(" . TABLE_COUNTRIES . ".country_id ORDER BY " . TABLE_COUNTRIES 
                    . ".country_id ASC),',') AS country_id  FROM " . TABLE_COUNTRIES . " WHERE lower(country) LIKE '%" . $search_input . "%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}