<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_search_messages_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_search_messages(&$tyr_search_messages_obj) {
        $query = 'INSERT into tyr_search_messages(
                    studio_id, role_id, tyroe_id, email_subject, email_message, email_status, status_sl, created_at, created_by, updated_at, updated_by, created_timestamp
                  ) values(
                    :studio_id, :role_id, :tyroe_id, :email_subject, :email_message, :email_status, :status_sl,  :created_at, :created_by, :updated_at, :updated_by, :created_timestamp
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_search_messages_obj->studio_id);
        $statement->bindParam(':role_id', $tyr_search_messages_obj->role_id);
        $statement->bindParam(':tyroe_id', $tyr_search_messages_obj->tyroe_id);
        $statement->bindParam(':email_subject', $tyr_search_messages_obj->email_subject);
        $statement->bindParam(':email_message', $tyr_search_messages_obj->email_message);
        $statement->bindParam(':email_status', $tyr_search_messages_obj->email_status);
        $statement->bindParam(':status_sl', $tyr_search_messages_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_search_messages_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_search_messages_obj->created_updated_by);
        $statement->bindParam(':created_timestamp', $tyr_search_messages_obj->created_timestamp);
        $statement->execute();
        $tyr_search_messages_obj->search_msg_id = $this->db_connection->lastInsertId('tyr_search_messages_search_msg_id_seq');
    }

    public function get_search_messages(&$tyr_search_messages_obj) {
        $query = 'select * from tyr_search_messages where search_msg_id = :search_msg_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':search_msg_id', $tyr_search_messages_obj->search_msg_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_search_messages_obj->search_msg_id = $row['search_msg_id'];
           $tyr_search_messages_obj->studio_id = $row['studio_id'];
           $tyr_search_messages_obj->role_id = $row['role_id'];
           $tyr_search_messages_obj->tyroe_id = $row['tyroe_id'];
           $tyr_search_messages_obj->email_subject = $row['email_subject'];
           $tyr_search_messages_obj->email_message = $row['email_message'];
           $tyr_search_messages_obj->email_status = $row['email_status'];
           $tyr_search_messages_obj->status_sl = $row['status_sl'];
        }
    }
    
}