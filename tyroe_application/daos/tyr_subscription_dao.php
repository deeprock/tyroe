<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_subscription_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_subscription(&$tyr_subscription_obj) {
        $query = 'INSERT into tyr_subscription(
                    tyroe_id, subscription_type, subscription_coupon, subscription_use_coupon, subscription_payment_type, subscription_card_type, subscription_card_num, subscription_name, subscription_expiry_month, subscription_expiry_year, subscription_ccv, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :tyroe_id, :subscription_type, :subscription_coupon, :subscription_use_coupon, :subscription_payment_type, :subscription_card_type, :subscription_card_num, :subscription_name, :subscription_expiry_month, :subscription_expiry_year, :subscription_ccv, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_subscription_obj->tyroe_id);
        $statement->bindParam(':subscription_type', $tyr_subscription_obj->subscription_type);
        $statement->bindParam(':subscription_coupon', $tyr_subscription_obj->subscription_coupon);
        $statement->bindParam(':subscription_use_coupon', $tyr_subscription_obj->subscription_use_coupon);
        $statement->bindParam(':subscription_payment_type', $tyr_subscription_obj->subscription_payment_type);
        $statement->bindParam(':subscription_card_type', $tyr_subscription_obj->subscription_card_type);
        $statement->bindParam(':subscription_card_num', $tyr_subscription_obj->subscription_card_num);
        $statement->bindParam(':subscription_name', $tyr_subscription_obj->subscription_name);
        $statement->bindParam(':subscription_expiry_month', $tyr_subscription_obj->subscription_expiry_month);
        $statement->bindParam(':subscription_expiry_year', $tyr_subscription_obj->subscription_expiry_year);
        $statement->bindParam(':subscription_ccv', $tyr_subscription_obj->subscription_ccv);
        $statement->bindParam(':status_sl', $tyr_subscription_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_subscription_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_subscription_obj->created_updated_by);
        $statement->execute();
        $tyr_subscription_obj->subscription_id = $this->db_connection->lastInsertId('tyr_subscription_subscription_id_seq');
    }

    public function get_subscription(&$tyr_subscription_obj) {
        $query = 'select * from tyr_subscription where subscription_id = :subscription_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':subscription_id', $tyr_subscription_obj->subscription_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_subscription_obj->subscription_id = $row['subscription_id'];
           $tyr_subscription_obj->tyroe_id = $row['tyroe_id'];
           $tyr_subscription_obj->subscription_type = $row['subscription_type'];
           $tyr_subscription_obj->subscription_coupon = $row['subscription_coupon'];
           $tyr_subscription_obj->subscription_use_coupon = $row['subscription_use_coupon'];
           $tyr_subscription_obj->subscription_payment_type = $row['subscription_payment_type'];
           $tyr_subscription_obj->subscription_card_type = $row['subscription_card_type'];
           $tyr_subscription_obj->subscription_card_num = $row['subscription_card_num'];
           $tyr_subscription_obj->subscription_name = $row['subscription_name'];
           $tyr_subscription_obj->subscription_expiry_month = $row['subscription_expiry_month'];
           $tyr_subscription_obj->subscription_expiry_year = $row['subscription_expiry_year'];
           $tyr_subscription_obj->subscription_ccv = $row['subscription_ccvs'];
           $tyr_subscription_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function update_subscription(&$tyr_subscription_obj) {
        $query = 'update tyr_subscription set tyroe_id = :tyroe_id, subscription_type = :subscription_type, subscription_coupon = :subscription_coupon, subscription_use_coupon = :subscription_use_coupon, subscription_payment_type = :subscription_payment_type, subscription_card_type = :subscription_card_type, subscription_card_num = :subscription_card_num, subscription_name = :subscription_name, subscription_expiry_month = :subscription_expiry_month, subscription_expiry_year = :subscription_expiry_year, subscription_ccv = :subscription_ccv, status_sl = :status_sl, updated_at = :updated_at where subscription_id = :subscription_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_subscription_obj->tyroe_id);
        $statement->bindParam(':subscription_type', $tyr_subscription_obj->subscription_type);
        $statement->bindParam(':subscription_coupon', $tyr_subscription_obj->subscription_coupon);
        $statement->bindParam(':subscription_use_coupon', $tyr_subscription_obj->subscription_use_coupon);
        $statement->bindParam(':subscription_payment_type', $tyr_subscription_obj->subscription_payment_type);
        $statement->bindParam(':subscription_card_type', $tyr_subscription_obj->subscription_card_type);
        $statement->bindParam(':subscription_card_num', $tyr_subscription_obj->subscription_card_num);
        $statement->bindParam(':subscription_name', $tyr_subscription_obj->subscription_name);
        $statement->bindParam(':subscription_expiry_month', $tyr_subscription_obj->subscription_expiry_month);
        $statement->bindParam(':subscription_expiry_year', $tyr_subscription_obj->subscription_expiry_year);
        $statement->bindParam(':subscription_ccv', $tyr_subscription_obj->subscription_ccv);
        $statement->bindParam(':status_sl', $tyr_subscription_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':subscription_id', $tyr_subscription_obj->subscription_id);
        $statement->execute();
        return true;
        
    }
    
}