<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_system_variables_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_system_variables(&$tyr_system_variables_obj) {
        $query = 'INSERT into tyr_system_variables(
                    variable_title, variable_value, variable_name, variable_field_type, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :variable_title, :variable_value, :variable_name, :variable_field_type, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':variable_title', $tyr_system_variables_obj->variable_title);
        $statement->bindParam(':variable_value', $tyr_system_variables_obj->variable_value);
        $statement->bindParam(':variable_name', $tyr_system_variables_obj->variable_name);
        $statement->bindParam(':variable_field_type', $tyr_system_variables_obj->variable_field_type);
        $statement->bindParam(':status_sl', $tyr_system_variables_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_system_variables_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_system_variables_obj->created_updated_by);
        $statement->execute();
        $tyr_system_variables_obj->variable_id = $this->db_connection->lastInsertId('tyr_system_variables_variable_id_seq');
    }

    public function get_system_variables(&$tyr_system_variables_obj) {
        $query = 'select * from tyr_system_variables where variable_id = :variable_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':variable_id', $tyr_system_variables_obj->variable_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_system_variables_obj->variable_id = $row['variable_id'];
           $tyr_system_variables_obj->variable_title = $row['variable_title'];
           $tyr_system_variables_obj->variable_value = $row['variable_value'];
           $tyr_system_variables_obj->variable_name = $row['variable_name'];
           $tyr_system_variables_obj->variable_field_type = $row['variable_field_type'];
           $tyr_system_variables_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_system_variables(&$tyr_system_variables_obj) {
        $query = 'select * from tyr_system_variables';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_system_variables_by_status(&$tyr_system_variables_obj) {
        $query = 'select * from tyr_system_variables where status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_system_variables_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['variable_id'] = $row['variable_id'];
           $temp_array['variable_title'] = $row['variable_title'];
           $temp_array['variable_value'] = $row['variable_value'];
           $temp_array['variable_name'] = $row['variable_name'];
           $temp_array['variable_field_type'] = $row['variable_field_type'];
           $temp_array['status_sl'] = $row['status_sl'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function update_variable_value_by_name(&$tyr_system_variables_obj) {
        $query = 'update tyr_system_variables set variable_value = :variable_value, updated_at = :updated_at where variable_name = :variable_name';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':variable_value', $tyr_system_variables_obj->variable_value);
        $statement->bindParam(':variable_name', $tyr_system_variables_obj->variable_name);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
}