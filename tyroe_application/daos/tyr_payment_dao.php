<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_payment_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_customer(&$tyr_payment_obj) {
        $query = 'INSERT into tyr_user_stripe(
                   user_id, stripe_user_id) values(
                   :user_id, :stripe_user_id)';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_payment_obj->user_id);
        $statement->bindParam(':stripe_user_id', $tyr_payment_obj->stripe_user_id);
        $statement->execute();
        $tyr_payment_obj->id = $this->db_connection->lastInsertId('tyr_user_stripe_id_seq');
    }
    
    public function get_customer(&$tyr_payment_obj) {
        $query = 'select * from tyr_user_stripe where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_payment_obj->user_id);
        $statement->execute();
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_payment_obj->stripe_user_id = $row['stripe_user_id'];
        }
    }
}