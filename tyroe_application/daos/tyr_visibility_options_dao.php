<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_visibility_options_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_visibility_options(&$tyr_visibility_options_obj) {
        $query = 'INSERT into tyr_visibility_options(
                    user_id, visible_section, visibility, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :visible_section, :visibility, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_visibility_options_obj->user_id);
        $statement->bindParam(':visible_section', $tyr_visibility_options_obj->visible_section);
        $statement->bindParam(':visibility', $tyr_visibility_options_obj->visibility);
        $statement->bindParam(':status_sl', $tyr_visibility_options_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_visibility_options_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_visibility_options_obj->created_updated_by);
        $statement->execute();
        $tyr_visibility_options_obj->visiblity_id = $this->db_connection->lastInsertId('tyr_visibility_options_visiblity_id_seq');
    }

    public function get_visibility_options(&$tyr_visibility_options_obj) {
        $query = 'select * from tyr_visibility_options where visiblity_id = :visiblity_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':visiblity_id', $tyr_visibility_options_obj->visiblity_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_visibility_options_obj->visiblity_id = $row['visiblity_id'];
           $tyr_visibility_options_obj->user_id = $row['user_id'];
           $tyr_visibility_options_obj->visible_section = $row['visible_section'];
           $tyr_visibility_options_obj->visibility = $row['visibility'];
           $tyr_visibility_options_obj->status_sl = $row['status_sl'];
        }
    }
    

    public function get_all_user_visibility_options(&$tyr_visibility_options_obj) {
        $query = "SELECT visiblity_id, user_id,visible_section,visibility, status_sl,created_date,modified_date
                         FROM " . TABLE_VISIBLITY_OPTIONS . " WHERE user_id= :user_id and status_sl= :status_sl ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_visibility_options_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_visibility_options_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_user_visibility_of_section(&$tyr_visibility_options_obj) {
        $query = 'update tyr_visibility_options set
                    visibility = :visibility, updated_at = :updated_at,
                  where visible_section = :visible_section and user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_visibility_options_obj->user_id);
        $statement->bindParam(':visible_section', $tyr_visibility_options_obj->visible_section);
        $statement->bindParam(':visibility', $tyr_visibility_options_obj->visibility);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
        
    }
}