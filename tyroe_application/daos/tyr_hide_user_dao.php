<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_hide_user_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_hide_user(&$tyr_hide_user_obj) {
        $query = 'INSERT into tyr_hide_user(
                   tyroe_id, studio_id, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :tyroe_id, :studio_id, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_hide_user_obj->tyroe_id);
        $statement->bindParam(':studio_id', $tyr_hide_user_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_hide_user_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_hide_user_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_hide_user_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_hide_user_obj->created_updated_by);
        $statement->execute();
        $tyr_hide_user_obj->hide_user_id = $this->db_connection->lastInsertId('tyr_hide_user_hide_user_id_seq');
    }

    public function get_hide_user(&$tyr_hide_user_obj) {
        $query = 'select * from tyr_hide_user where hide_user_id = :hide_user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':hide_user_id', $tyr_hide_user_obj->hide_user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_hide_user_obj->hide_user_id = $row['hide_user_id'];
           $tyr_hide_user_obj->tyroe_id = $row['tyroe_id'];
           $tyr_hide_user_obj->studio_id = $row['studio_id'];
           $tyr_hide_user_obj->status_sl = $row['status_sl'];
           $tyr_hide_user_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function delete_hide_users_by_studio(&$tyr_hide_user_obj) {
        $query = 'delete from tyr_hide_user where tyroe_id = :tyroe_id AND studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_hide_user_obj->tyroe_id);
        $statement->bindParam(':studio_id', $tyr_hide_user_obj->studio_id);
        $statement->execute();
    }
    
    public function get_hidden_tyroes_by_studio_id(&$tyr_hide_user_obj) {
        $query = "SELECT array_to_string(array_agg(tyroe_id ORDER BY tyroe_id ASC),',')AS tyroe_id,studio_id FROM tyr_hide_user WHERE studio_id = :studio_id AND status_sl = :status_sl GROUP BY studio_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_hide_user_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_hide_user_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_hidden_tyroes_by_studio_id_1(&$tyr_hide_user_obj) {
        $query = "SELECT array_to_string(array_agg(tyroe_id ORDER BY tyroe_id ASC),',')AS tyroe_id,studio_id FROM tyr_hide_user WHERE studio_id = :studio_id GROUP BY studio_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_hide_user_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}