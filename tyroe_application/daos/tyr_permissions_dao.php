<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_permissions_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_permissions(&$tyr_permissions_obj) {
        $query = 'INSERT into tyr_permissions(
                    user_id, module_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :module_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_permissions_obj->user_id);
        $statement->bindParam(':module_id', $tyr_permissions_obj->module_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_permissions_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_permissions_obj->created_updated_by);
        $statement->execute();
        $tyr_permissions_obj->permission_id = $this->db_connection->lastInsertId('tyr_permissions_permission_id_seq');
    }

    public function get_permissions(&$tyr_permissions_obj) {
        $query = 'select * from tyr_permissions where permission_id = :permission_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':permission_id', $tyr_permissions_obj->permission_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_permissions_obj->permission_id = $row['permission_id'];
           $tyr_permissions_obj->user_id = $row['user_id'];
           $tyr_permissions_obj->module_id = $row['module_id'];
        }
    }
    
    public function delete_user_permissions(&$tyr_permissions_obj) {
        $query = 'delete from tyr_permissions where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_permissions_obj->user_id);
        $statement->execute();
    }
    
    public function get_all_user_permissions(&$tyr_permissions_obj) {
        $query = 'select * from tyr_permissions where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_permissions_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}