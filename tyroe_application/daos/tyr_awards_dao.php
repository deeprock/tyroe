<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_awards_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_awards(&$tyr_awards_obj) {
        $query = 'INSERT into tyr_awards(
                   user_id, award, award_website, award_organization, award_year, award_description, status_sl, created_timestamp, modified, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :award, :award_website, :award_organization, :award_year, :award_description, :status_sl, :created_timestamp, :modified, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_awards_obj->user_id);
        $statement->bindParam(':award', $tyr_awards_obj->award);
        $statement->bindParam(':award_website', $tyr_awards_obj->award_website);
        $statement->bindParam(':award_organization', $tyr_awards_obj->award_organization);
        $statement->bindParam(':award_year', $tyr_awards_obj->award_year);
        $statement->bindParam(':award_description', $tyr_awards_obj->award_description);
        $statement->bindParam(':modified', $tyr_awards_obj->modified);
        $statement->bindParam(':created_timestamp', $tyr_awards_obj->created_timestamp);
        $statement->bindParam(':status_sl', $tyr_awards_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_awards_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_awards_obj->created_updated_by);
        $statement->execute();
        $tyr_awards_obj->award_id = $this->db_connection->lastInsertId('tyr_awards_award_id_seq');
    }

    public function get_awards(&$tyr_awards_obj) {
        $query = 'select * from tyr_awards where award_id = :award_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':award_id', $tyr_awards_obj->award_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_awards_obj->award_id = $row['award_id'];
           $tyr_awards_obj->user_id = $row['user_id'];
           $tyr_awards_obj->award = $row['award'];
           $tyr_awards_obj->award_website = $row['award_website'];
           $tyr_awards_obj->award_organization = $row['award_organization'];
           $tyr_awards_obj->award_year = $row['award_year'];
           $tyr_awards_obj->award_description = $row['award_description'];
           $tyr_awards_obj->status_sl = $row['status_sl'];
           $tyr_awards_obj->modified = $row['modified'];
           $tyr_awards_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function get_user_all_award(&$tyr_awards_obj) {
        $query = "SELECT award_id, user_id,award,award_organization,award_description,award_year,created_timestamp, modified,  status_sl
                  FROM " . TABLE_AWARDS . " WHERE user_id= :user_id and status_sl= :status_sl ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_awards_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_awards_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){ if(!is_array($return_array)) $return_array = array();
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    
    public function update_awards(&$tyr_awards_obj) {
        $query = 'update tyr_awards set 
                   user_id = :user_id, award = :award, award_website = :award_website, award_organization = :award_organization, award_year = :award_year, award_description = :award_description, status_sl = :status_sl, modified = :modified, updated_at = :updated_at, updated_by = :updated_by
                  where award_id = :award_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_awards_obj->user_id);
        $statement->bindParam(':award', $tyr_awards_obj->award);
        $statement->bindParam(':award_website', $tyr_awards_obj->award_website);
        $statement->bindParam(':award_organization', $tyr_awards_obj->award_organization);
        $statement->bindParam(':award_year', $tyr_awards_obj->award_year);
        $statement->bindParam(':award_description', $tyr_awards_obj->award_description);
        $statement->bindParam(':status_sl', $tyr_awards_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_awards_obj->user_id);
        $statement->bindParam(':award_id', $tyr_awards_obj->award_id);
        $statement->bindParam(':modified', $tyr_awards_obj->modified);
        $statement->execute();
        return true;
    }
    
    public function get_user_all_award_count(&$tyr_awards_obj) {
        $query = "SELECT COUNT(*) as cnt FROM tyr_awards  WHERE user_id = :user_id and status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_awards_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_awards_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }
    
}