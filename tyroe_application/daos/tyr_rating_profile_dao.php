<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_rating_profile_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_rating_profile(&$tyr_rating_profile_obj) {
        $query = 'INSERT into tyr_rating_profile(
                    user_id, rating, studio_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :rating, :studio_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_profile_obj->user_id);
        $statement->bindParam(':rating', $tyr_rating_profile_obj->rating);
        $statement->bindParam(':studio_id', $tyr_rating_profile_obj->studio_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_rating_profile_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_rating_profile_obj->created_updated_by);
        $statement->execute();
        $tyr_rating_profile_obj->rate_id = $this->db_connection->lastInsertId('tyr_rating_profile_rate_id_seq');
    }

    public function get_rating_profile(&$tyr_rating_profile_obj) {
        $query = 'select * from tyr_rating_profile where rate_id = :rate_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':rate_id', $tyr_rating_profile_obj->rate_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_rating_profile_obj->rate_id = $row['rate_id'];
           $tyr_rating_profile_obj->user_id = $row['user_id'];
           $tyr_rating_profile_obj->rating = $row['rating'];
           $tyr_rating_profile_obj->studio_id = $row['studio_id'];
        }
    }
    
    public function get_rating_count_by_user_id(&$tyr_rating_profile_obj) {
        $query = 'SELECT COUNT(*) AS cnt FROM tyr_rating_profile WHERE  user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_profile_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_user_rating_total(&$tyr_rating_profile_obj) {
        $query = 'SELECT SUM(rating)/COUNT(rating) as total FROM tyr_rating_profile WHERE user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_profile_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function check_profile_already_rated(&$tyr_rating_profile_obj) {
        $query = 'SELECT COUNT(*) AS cnt FROM tyr_rating_profile WHERE  user_id = :user_id and studio_id = :studio_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_profile_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_rating_profile_obj->studio_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_profile_rating_count(&$tyr_rating_profile_obj) {
        $query = 'SELECT COUNT(*) AS cnt FROM tyr_rating_profile WHERE  user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_profile_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if(($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    
}