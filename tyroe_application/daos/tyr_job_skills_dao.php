<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_skills_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_skills(&$tyr_job_skills_obj) {
        $query = 'INSERT into tyr_job_skills(
                   studio_id, job_id, creative_skill_id, created_at, created_by, updated_at, updated_by
                  ) values(
                   :studio_id, :job_id, :creative_skill_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_job_skills_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_job_skills_obj->job_id);
        $statement->bindParam(':creative_skill_id', $tyr_job_skills_obj->creative_skill_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_skills_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_skills_obj->created_updated_by);
        $statement->execute();
        $tyr_job_skills_obj->job_skill_id = $this->db_connection->lastInsertId('tyr_job_skills_job_skill_id_seq');
    }

    public function get_job_skills(&$tyr_job_skills_obj) {
        $query = 'select * from tyr_job_skills where job_skill_id = :job_skill_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_skill_id', $tyr_job_skills_obj->job_skill_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_skills_obj->job_skill_id = $row['job_skill_id'];
           $tyr_job_skills_obj->studio_id = $row['studio_id'];
           $tyr_job_skills_obj->job_id = $row['job_id'];
           $tyr_job_skills_obj->creative_skill_id = $row['creative_skill_id'];
        }
    }
    
    public function get_creative_skill_job_ids(&$tyr_job_skills_obj,$cskills_id) {
        $query = "SELECT array_to_string(array_agg(DISTINCT(job_id)),',') AS jobs_skill_id FROM tyr_job_skills WHERE creative_skill_id IN (".$cskills_id['skills_id'].")";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function delete_all_job_skill(&$tyr_job_skills_obj) {
        $query = "delete from tyr_job_skills WHERE job_id = :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_skills_obj->job_id);
        $statement->execute();
    }
    
    public function get_all_creative_skill_id_by_job(&$tyr_job_skills_obj) {
        $query = "SELECT creative_skill_id FROM ".TABLE_JOB_SKILLS." where job_id= :job_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_job_skills_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}