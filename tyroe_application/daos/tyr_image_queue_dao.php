<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_image_queue_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_image_queue(&$tyr_image_queue_obj) {
        $query = 'INSERT into tyr_image_queue(
                   user_id, queue_time, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :queue_time, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_image_queue_obj->user_id);
        $statement->bindParam(':queue_time', $tyr_image_queue_obj->queue_time);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_image_queue_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_image_queue_obj->created_updated_by);
        $statement->execute();
        $tyr_image_queue_obj->queue_id = $this->db_connection->lastInsertId('tyr_image_queue_queue_id_seq');
    }

    public function get_image_queue(&$tyr_image_queue_obj) {
        $query = 'select * from tyr_image_queue where queue_id = :queue_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':queue_id', $tyr_image_queue_obj->queue_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_image_queue_obj->queue_id = $row['queue_id'];
           $tyr_image_queue_obj->user_id = $row['user_id'];
           $tyr_image_queue_obj->queue_time = $row['queue_time'];
        }
    }
    
}