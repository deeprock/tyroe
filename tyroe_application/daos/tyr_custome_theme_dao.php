<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_custome_theme_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_custome_theme(&$tyr_custome_theme_obj) {
        $query = 'INSERT into tyr_custome_theme(
                   user_id, profile_theme, featured_theme, gallery_theme, resume_theme, footer_theme, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :user_id, :profile_theme, :featured_theme, :gallery_theme, :resume_theme, :footer_theme, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_custome_theme_obj->user_id);
        $statement->bindParam(':profile_theme', $tyr_custome_theme_obj->profile_theme);
        $statement->bindParam(':featured_theme', $tyr_custome_theme_obj->featured_theme);
        $statement->bindParam(':gallery_theme', $tyr_custome_theme_obj->gallery_theme);
        $statement->bindParam(':resume_theme', $tyr_custome_theme_obj->resume_theme);
        $statement->bindParam(':footer_theme', $tyr_custome_theme_obj->footer_theme);
        $statement->bindParam(':status_sl', $tyr_custome_theme_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_custome_theme_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_custome_theme_obj->created_updated_by);
        $statement->execute();
        $tyr_custome_theme_obj->theme_id = $this->db_connection->lastInsertId('tyr_custome_theme_theme_id_seq');
    }

    public function get_custome_theme(&$tyr_custome_theme_obj) {
        $query = 'select * from tyr_custome_theme where theme_id = :theme_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':theme_id', $tyr_custome_theme_obj->theme_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_custome_theme_obj->theme_id = $row['theme_id'];
           $tyr_custome_theme_obj->user_id = $row['user_id'];
           $tyr_custome_theme_obj->profile_theme = $row['profile_theme'];
           $tyr_custome_theme_obj->featured_theme = $row['featured_theme'];
           $tyr_custome_theme_obj->gallery_theme = $row['gallery_theme'];
           $tyr_custome_theme_obj->resume_theme = $row['resume_theme'];
           $tyr_custome_theme_obj->footer_theme = $row['footer_theme'];
           $tyr_custome_theme_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_custome_theme_by_user_id(&$tyr_custome_theme_obj) {
        $query = "SELECT profile_theme, featured_theme, gallery_theme, resume_theme, footer_theme
                      FROM " . TABLE_CUSTOM_THEME . " ct
                      WHERE ct.user_id= :user_id and ct.status_sl= :status_sl ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_custome_theme_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_custome_theme_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_custome_theme_obj->theme_id = $row['theme_id'];
           $tyr_custome_theme_obj->user_id = $row['user_id'];
           $tyr_custome_theme_obj->profile_theme = $row['profile_theme'];
           $tyr_custome_theme_obj->featured_theme = $row['featured_theme'];
           $tyr_custome_theme_obj->gallery_theme = $row['gallery_theme'];
           $tyr_custome_theme_obj->resume_theme = $row['resume_theme'];
           $tyr_custome_theme_obj->footer_theme = $row['footer_theme'];
           $tyr_custome_theme_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function update_user_custome_theme_status(&$tyr_custome_theme_obj) {
        $query = 'update tyr_custome_theme set status_sl = :status_sl, updated_at = :updated_at where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_custome_theme_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_custome_theme_obj->user_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
    public function update_custome_theme(&$tyr_custome_theme_obj) {
        $query = 'update tyr_custome_theme set user_id = :user_id, profile_theme = :profile_theme, featured_theme = :featured_theme,'
                . ' gallery_theme = :gallery_theme, resume_theme = :resume_theme, footer_theme = :footer_theme, status_sl = :status_sl, '
                . 'updated_at = :updated_at where user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':profile_theme', $tyr_custome_theme_obj->profile_theme);
        $statement->bindParam(':featured_theme', $tyr_custome_theme_obj->featured_theme);
        $statement->bindParam(':gallery_theme', $tyr_custome_theme_obj->gallery_theme);
        $statement->bindParam(':resume_theme', $tyr_custome_theme_obj->resume_theme);
        $statement->bindParam(':footer_theme', $tyr_custome_theme_obj->footer_theme);
        $statement->bindParam(':status_sl', $tyr_custome_theme_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_custome_theme_obj->user_id);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->execute();
        return true;
    }
    
}