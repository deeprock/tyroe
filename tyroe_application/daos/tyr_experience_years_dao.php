<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_experience_years_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_experience_years(&$tyr_experience_years_obj) {
        $query = 'INSERT into tyr_experience_years(
                   experienceyear, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :experienceyear, :status_sl, created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':experienceyear', $tyr_experience_years_obj->experienceyear);
        $statement->bindParam(':status_sl', $tyr_experience_years_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_experience_years_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_experience_years_obj->created_updated_by);
        $statement->execute();
        $tyr_experience_years_obj->experienceyear_id = $this->db_connection->lastInsertId('tyr_experience_years_experienceyear_id_seq');
    }

    public function get_experience_years(&$tyr_experience_years_obj) {
        $query = 'select * from tyr_experience_years where experienceyear_id = :experienceyear_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':experienceyear_id', $tyr_experience_years_obj->experienceyear_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_experience_years_obj->experienceyear_id = $row['experienceyear_id'];
           $tyr_experience_years_obj->experienceyear = $row['experienceyear'];
           $tyr_experience_years_obj->status_sl = $row['status_sl'];
        }
    }
    

    public function get_all_experience_years(){
        $query = 'select * from tyr_experience_years';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while (($row = $statement->fetch()) != FALSE) {
            $temp_array = array();
            $temp_array['experienceyear_id'] = $row['experienceyear_id'];
            $temp_array['id'] = $row['experienceyear_id'];
            $temp_array['experienceyear'] = $row['experienceyear'];
            $temp_array['expyear'] = $row['experienceyear'];
            $temp_array['name'] = $row['experienceyear'];
            $temp_array['status_sl'] = $row['status_sl'];
            $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
}