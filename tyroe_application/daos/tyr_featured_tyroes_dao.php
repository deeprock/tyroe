<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_featured_tyroes_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_featured_tyroes(&$tyr_featured_tyroes_obj) {
        $query = 'INSERT into tyr_featured_tyroes(
                   featured_tyroe_id, featured_status, created_at, created_by, updated_at, updated_by
                  ) values(
                   :featured_tyroe_id, :featured_status, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':featured_tyroe_id', $tyr_featured_tyroes_obj->featured_tyroe_id);
        $statement->bindParam(':featured_status', $tyr_featured_tyroes_obj->featured_status);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_featured_tyroes_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_featured_tyroes_obj->created_updated_by);
        $statement->execute();
        $tyr_featured_tyroes_obj->id = $this->db_connection->lastInsertId('tyr_featured_tyroes_id_seq');
    }

    public function get_featured_tyroes(&$tyr_featured_tyroes_obj) {
        $query = 'select * from tyr_featured_tyroes where id = :id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':id', $tyr_featured_tyroes_obj->id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_featured_tyroes_obj->id = $row['id'];
           $tyr_featured_tyroes_obj->featured_tyroe_id = $row['featured_tyroe_id'];
           $tyr_featured_tyroes_obj->featured_status = $row['featured_status'];
        }
    }
    
    public function get_user_featured_status(&$tyr_featured_tyroes_obj) {
        $query = 'SELECT * FROM tyr_featured_tyroes WHERE featured_tyroe_id = :featured_tyroe_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':featured_tyroe_id', $tyr_featured_tyroes_obj->featured_tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_featured_tyroes_obj->id = $row['id'];
           $tyr_featured_tyroes_obj->featured_tyroe_id = $row['featured_tyroe_id'];
           $tyr_featured_tyroes_obj->featured_status = $row['featured_status'];
        }
    }
    
    public function update_featured_tyroes_status(&$tyr_featured_tyroes_obj) {
        $query = 'update tyr_featured_tyroes set featured_status = :featured_status where featured_tyroe_id = :featured_tyroe_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':featured_status', $tyr_featured_tyroes_obj->featured_status);
        $statement->bindParam(':featured_tyroe_id', $tyr_featured_tyroes_obj->featured_tyroe_id);
        $statement->execute();
    }
    
}