<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_pageviews_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_pageviews(&$tyr_pageviews_obj) {
        $query = 'INSERT into tyr_pageviews(
                    user_id, session_id, user_type, page_url, user_info_detail, user_ip, visit_time, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :session_id, :user_type, :page_url, :user_info_detail, :user_ip, :visit_time, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        if($tyr_pageviews_obj->user_id == false){
            $tyr_pageviews_obj->user_id = 0;
        }
        $statement->bindParam(':user_id', $tyr_pageviews_obj->user_id);
        $statement->bindParam(':session_id', $tyr_pageviews_obj->session_id);
        $statement->bindParam(':user_type', $tyr_pageviews_obj->user_type);
        $statement->bindParam(':page_url', $tyr_pageviews_obj->page_url);
        $statement->bindParam(':user_info_detail', $tyr_pageviews_obj->user_info_detail);
        $statement->bindParam(':user_ip', $tyr_pageviews_obj->user_ip);
        $statement->bindParam(':visit_time', $tyr_pageviews_obj->visit_time);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_pageviews_obj->user_id);
        $statement->bindParam(':updated_by', $tyr_pageviews_obj->user_id);
        $statement->execute();
        $tyr_pageviews_obj->visit_id = $this->db_connection->lastInsertId('tyr_pageviews_visit_id_seq');
    }

    public function get_pageviews(&$tyr_pageviews_obj) {
        $query = 'select * from tyr_pageviews where visit_id = :visit_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':visit_id', $tyr_pageviews_obj->visit_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_pageviews_obj->visit_id = $row['visit_id'];
           $tyr_pageviews_obj->user_id = $row['user_id'];
           $tyr_pageviews_obj->session_id = $row['session_id'];
           $tyr_pageviews_obj->user_type = $row['user_type'];
           $tyr_pageviews_obj->page_url = $row['page_url'];
           $tyr_pageviews_obj->user_info_detail = $row['user_info_detail'];
           $tyr_pageviews_obj->user_ip = $row['user_ip'];
           $tyr_pageviews_obj->visit_time = $row['visit_time'];
        }
    }
    
    public function get_todays_pageview_count(&$tyr_pageviews_obj,$start_day_unixtime,$end_day_unixtime) {
        $query = 'select COUNT(*) AS pages_count, user_type AS visit_by from tyr_pageviews where visit_time >= '.$start_day_unixtime.' and visit_time <= '.$end_day_unixtime.' group by user_type';
        $statement = $this->db_connection->prepare($query);
        //$statement->bindParam(':visit_id', $tyr_pageviews_obj->visit_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while(($row = $statement->fetch()) != FALSE) {
           $temp_array = array();
           $temp_array['pages_count'] = $row['pages_count'];
           $temp_array['visit_by'] = $row['visit_by'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
}