<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_shortlist_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_shortlist(&$tyr_shortlist_obj) {
        $query = 'INSERT into tyr_shortlist(
                    job_id, user_id, studio_id, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :job_id, :user_id, :studio_id, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_shortlist_obj->job_id);
        $statement->bindParam(':user_id', $tyr_shortlist_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_shortlist_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_shortlist_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_shortlist_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_shortlist_obj->created_updated_by);
        $statement->execute();
        $tyr_shortlist_obj->shortlist_id = $this->db_connection->lastInsertId('tyr_shortlist_shortlist_id_seq');
    }

    public function get_shortlist(&$tyr_shortlist_obj) {
        $query = 'select * from tyr_shortlist where shortlist_id = :shortlist_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':shortlist_id', $tyr_shortlist_obj->shortlist_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_shortlist_obj->shortlist_id = $row['shortlist_id'];
           $tyr_shortlist_obj->job_id = $row['job_id'];
           $tyr_shortlist_obj->user_id = $row['user_id'];
           $tyr_shortlist_obj->studio_id = $row['studio_id'];
           $tyr_shortlist_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function check_shortlist(&$tyr_shortlist_obj) {
        $query = 'select * from tyr_shortlist where job_id = :job_id AND user_id = :user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_shortlist_obj->job_id);
        $statement->bindParam(':user_id', $tyr_shortlist_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_shortlist_obj->shortlist_id = $row['shortlist_id'];
           $tyr_shortlist_obj->job_id = $row['job_id'];
           $tyr_shortlist_obj->user_id = $row['user_id'];
           $tyr_shortlist_obj->studio_id = $row['studio_id'];
           $tyr_shortlist_obj->status_sl = $row['status_sl'];
        }
    }
    
}