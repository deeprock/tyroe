<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_hidden_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_hidden(&$tyr_hidden_obj) {
        $query = 'INSERT into tyr_hidden(
                   job_id, user_id, studio_id, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                   :job_id, :user_id, :studio_id, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_hidden_obj->job_id);
        $statement->bindParam(':user_id', $tyr_hidden_obj->user_id);
        $statement->bindParam(':studio_id', $tyr_hidden_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_hidden_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_hidden_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_hidden_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_hidden_obj->created_updated_by);
        $statement->execute();
        $tyr_hidden_obj->hidden_id = $this->db_connection->lastInsertId('tyr_hidden_hidden_id_seq');
    }

    public function get_hidden(&$tyr_hidden_obj) {
        $query = 'select * from tyr_hidden where hidden_id = :hidden_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':hidden_id', $tyr_hidden_obj->hidden_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_hidden_obj->hidden_id = $row['hidden_id'];
           $tyr_hidden_obj->job_id = $row['job_id'];
           $tyr_hidden_obj->user_id = $row['user_id'];
           $tyr_hidden_obj->studio_id = $row['studio_id'];
           $tyr_hidden_obj->status_sl = $row['status_sl'];
           $tyr_hidden_obj->created_timestamp = $row['created_timestamp'];
        }
    }
    
    public function get_hidden_tyroes(&$tyr_hidden_obj) {
        $query = "SELECT array_to_string(array_agg(user_id ORDER BY user_id ASC),',') AS user_id,studio_id FROM tyr_hidden WHERE studio_id = :studio_id AND job_id = :job_id GROUP BY studio_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_hidden_obj->studio_id);
        $statement->bindParam(':job_id', $tyr_hidden_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_total_hidden_count_by_job(&$tyr_hidden_obj) {
        $query = 'SELECT COUNT(*) AS total_hidden FROM tyr_hidden WHERE job_id = :job_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_hidden_obj->job_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_hidden_tyroes_by_studio_id(&$tyr_hidden_obj) {
        $query = "SELECT array_to_string(array_agg(tyroe_id ORDER BY tyroe_id ASC),',') AS tyroe_id,studio_id FROM " . TABLE_HIDE_USER 
                . " WHERE studio_id = :studio_id  AND status_sl = :status_sl GROUP BY studio_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':studio_id', $tyr_hidden_obj->studio_id);
        $statement->bindParam(':status_sl', $tyr_hidden_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}