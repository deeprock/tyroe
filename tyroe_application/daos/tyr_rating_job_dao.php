<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_rating_job_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_rating_job(&$tyr_rating_job_obj) {
        $query = 'INSERT into tyr_rating_job(
                    job_id, rating, studio_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :job_id, :rating, :studio_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_id', $tyr_rating_job_obj->job_id);
        $statement->bindParam(':rating', $tyr_rating_job_obj->rating);
        $statement->bindParam(':studio_id', $tyr_rating_job_obj->studio_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_rating_job_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_rating_job_obj->created_updated_by);
        $statement->execute();
        $tyr_rating_job_obj->rate_id = $this->db_connection->lastInsertId('tyr_rating_job_rate_id_seq');
    }

    public function get_rating_job(&$tyr_rating_job_obj) {
        $query = 'select * from tyr_rating_job where rate_id = :rate_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':rate_id', $tyr_rating_job_obj->rate_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_rating_job_obj->rate_id = $row['rate_id'];
           $tyr_rating_job_obj->job_id = $row['job_id'];
           $tyr_rating_job_obj->rating = $row['rating'];
           $tyr_rating_job_obj->studio_id = $row['studio_id'];
        }
    }
    
}