<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_invite_friend_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_invite_friend(&$tyr_invite_friend_obj) {
        $query = 'INSERT into tyr_invite_friend(
                   coupon_id, user_id, email, friend_name, invite_message, accpeted_timestamp, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                   :coupon_id, :user_id, :email, :friend_name, :invite_message, :accpeted_timestamp, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':coupon_id', $tyr_invite_friend_obj->coupon_id);
        $statement->bindParam(':user_id', $tyr_invite_friend_obj->user_id);
        $statement->bindParam(':email', $tyr_invite_friend_obj->email);
        $statement->bindParam(':friend_name', $tyr_invite_friend_obj->friend_name);
        $statement->bindParam(':invite_message', $tyr_invite_friend_obj->invite_message);
        $statement->bindParam(':accpeted_timestamp', $tyr_invite_friend_obj->accpeted_timestamp);
        $statement->bindParam(':status_sl', $tyr_invite_friend_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_invite_friend_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_invite_friend_obj->created_updated_by);
        $statement->execute();
        $tyr_invite_friend_obj->invite_id = $this->db_connection->lastInsertId('tyr_invite_friend_invite_id_seq');
    }

    public function get_invite_friend(&$tyr_invite_friend_obj) {
        $query = 'select * from tyr_invite_friend where invite_id = :invite_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':invite_id', $tyr_invite_friend_obj->invite_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_invite_friend_obj->invite_id = $row['invite_id'];
           $tyr_invite_friend_obj->coupon_id = $row['coupon_id'];
           $tyr_invite_friend_obj->user_id = $row['user_id'];
           $tyr_invite_friend_obj->email = $row['email'];
           $tyr_invite_friend_obj->friend_name = $row['friend_name'];
           $tyr_invite_friend_obj->invite_message = $row['invite_message'];
           $tyr_invite_friend_obj->accpeted_timestamp = $row['accpeted_timestamp'];
           $tyr_invite_friend_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function update_status_sl(&$tyr_invite_friend_obj) {
        $query = 'update tyr_invite_friend set status_sl = :status_sl where user_id = :user_id AND coupon_id = :coupon_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':status_sl', $tyr_invite_friend_obj->status_sl);
        $statement->bindParam(':user_id', $tyr_invite_friend_obj->user_id);
        $statement->bindParam(':coupon_id', $tyr_invite_friend_obj->coupon_id);
        $statement->execute();
        return true;
    }
    
}