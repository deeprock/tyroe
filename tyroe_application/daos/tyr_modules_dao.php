<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_modules_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_modules(&$tyr_modules_obj) {
        $query = 'INSERT into tyr_modules(
                    module_title, sub_text, module_file, link, module_type, sort_order, parent, module_img, status_sl, created_at, created_by, updated_at, updated_by
                  ) values(
                    :module_title, :sub_text, :module_file, :link, :module_type, :sort_order, :parent, :module_img, :status_sl, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':module_title', $tyr_modules_obj->module_title);
        $statement->bindParam(':sub_text', $tyr_modules_obj->sub_text);
        $statement->bindParam(':module_file', $tyr_modules_obj->module_file);
        $statement->bindParam(':link', $tyr_modules_obj->link);
        $statement->bindParam(':module_type', $tyr_modules_obj->module_type);
        $statement->bindParam(':sort_order', $tyr_modules_obj->sort_order);
        $statement->bindParam(':parent', $tyr_modules_obj->parent);
        $statement->bindParam(':module_img', $tyr_modules_obj->module_img);
        $statement->bindParam(':status_sl', $tyr_modules_obj->status_sl);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_modules_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_modules_obj->created_updated_by);
        $statement->execute();
        $tyr_modules_obj->module_id = $this->db_connection->lastInsertId('tyr_modules_module_id_seq');
    }

    public function get_modules(&$tyr_modules_obj) {
        $query = 'select * from tyr_modules where module_id = :module_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':module_id', $tyr_modules_obj->module_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_modules_obj->module_id = $row['module_id'];
           $tyr_modules_obj->module_title = $row['module_title'];
           $tyr_modules_obj->sub_text = $row['sub_text'];
           $tyr_modules_obj->module_file = $row['module_file'];
           $tyr_modules_obj->link = $row['link'];
           $tyr_modules_obj->module_type = $row['module_type'];
           $tyr_modules_obj->sort_order = $row['sort_order'];
           $tyr_modules_obj->parent = $row['parent'];
           $tyr_modules_obj->module_img = $row['module_img'];
           $tyr_modules_obj->status_sl = $row['status_sl'];
        }
    }
    
    public function get_all_modules(&$tyr_modules_obj) {
        $query = 'SELECT module_id FROM tyr_modules ORDER BY sort_order ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
            $temp_array = array();
            $temp_array['module_id'] = $row['module_id']; 
            $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    
    public function get_system_modules($where = '',$session_modiule_id = '') {
        
        $query = 'SELECT module_id,module_title,sub_text,module_file,link,module_type,module_img FROM tyr_modules WHERE module_id'
                . ' IN (' . @implode(',', $session_modiule_id) . ') '
                . $where.'  ORDER BY module_type ASC,sort_order ASC';
        
//        echo $query;
//        die();
        
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
            $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_all_system_modules_in(&$tyr_modules_obj,$menus = '') {
        $query = 'SELECT * FROM tyr_modules WHERE module_id IN ('.$menus.')';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_all_system_modules_where(&$tyr_modules_obj,$where = '') {
        $status_sl = 1;
        $query = "SELECT module_id,module_file,link FROM tyr_modules WHERE module_file = :module_file AND status_sl = :status_sl " . $where;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':module_file', $tyr_modules_obj->module_file);
        $statement->bindParam(':status_sl', $status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
}