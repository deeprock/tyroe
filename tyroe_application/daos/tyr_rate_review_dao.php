<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_rate_review_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_rate_review(&$tyr_rate_review_obj) {
        $query = 'INSERT into tyr_rate_review(
                    user_id, user_type, tyroe_id, job_id, level_id, technical, creative, impression, review_comment, action_shortlist, action_interview, anonymous_feedback_message, anonymous_feedback_status, status_sl, created_timestamp, created_at, created_by, updated_at, updated_by, moderated
                  ) values(
                    :user_id, :user_type, :tyroe_id, :job_id, :level_id, :technical, :creative, :impression, :review_comment, :action_shortlist, :action_interview, :anonymous_feedback_message, :anonymous_feedback_status, :status_sl, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by, :moderated
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rate_review_obj->user_id);
        $statement->bindParam(':user_type', $tyr_rate_review_obj->user_type);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_rate_review_obj->job_id);
        $statement->bindParam(':level_id', $tyr_rate_review_obj->level_id);
        $statement->bindParam(':technical', $tyr_rate_review_obj->technical);
        $statement->bindParam(':creative', $tyr_rate_review_obj->creative);
        $statement->bindParam(':impression', $tyr_rate_review_obj->impression);
        $statement->bindParam(':review_comment', $tyr_rate_review_obj->review_comment);
        $statement->bindParam(':action_shortlist', $tyr_rate_review_obj->action_shortlist);
        $statement->bindParam(':action_interview', $tyr_rate_review_obj->action_interview);
        $statement->bindParam(':anonymous_feedback_message', $tyr_rate_review_obj->anonymous_feedback_message);
        $statement->bindParam(':anonymous_feedback_status', $tyr_rate_review_obj->anonymous_feedback_status);
        $statement->bindParam(':status_sl', $tyr_rate_review_obj->status_sl);
        $statement->bindParam(':created_timestamp', $tyr_rate_review_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_rate_review_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_rate_review_obj->created_updated_by);
        $statement->bindParam(':moderated', $tyr_rate_review_obj->moderated);
        $statement->execute();
        $tyr_rate_review_obj->review_id = $this->db_connection->lastInsertId('tyr_rate_review_review_id_seq');
    }

    public function get_rate_review(&$tyr_rate_review_obj) {
        $query = 'select * from tyr_rate_review where review_id = :review_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':review_id', $tyr_rate_review_obj->review_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_rate_review_obj->review_id = $row['review_id'];
           $tyr_rate_review_obj->user_type = $row['user_type'];
           $tyr_rate_review_obj->tyroe_id = $row['tyroe_id'];
           $tyr_rate_review_obj->job_id = $row['job_id'];
           $tyr_rate_review_obj->user_id = $row['user_id'];
           $tyr_rate_review_obj->level_id = $row['level_id'];
           $tyr_rate_review_obj->technical = $row['technical'];
           $tyr_rate_review_obj->creative = $row['creative'];
           $tyr_rate_review_obj->impression = $row['impression'];
           $tyr_rate_review_obj->review_comment = $row['review_comment'];
           $tyr_rate_review_obj->action_shortlist = $row['action_shortlist'];
           $tyr_rate_review_obj->action_interview = $row['action_interview'];
           $tyr_rate_review_obj->anonymous_feedback_message = $row['anonymous_feedback_message'];
           $tyr_rate_review_obj->anonymous_feedback_status = $row['anonymous_feedback_status'];
           $tyr_rate_review_obj->status_sl = $row['status_sl'];
           $tyr_rate_review_obj->created_timestamp = $row['created_timestamp'];
           $tyr_rate_review_obj->moderated = $row['moderated'];
        }
    }
    
    public function get_all_rate_review_by_user_type(&$tyr_rate_review_obj,$user_type,$profile_id) {
        $query = 'SELECT rr.technical,rr.creative,rr.impression FROM tyr_rate_review rr WHERE '.$user_type.' = '. $profile_id ;
        $statement = $this->db_connection->prepare($query);
        //$statement->bindParam(':review_id', $tyr_rate_review_obj->review_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_sum_all_rate_review_by_user(&$tyr_rate_review_obj) {
        $query = 'SELECT (SUM(rr.technical))as technical,(SUM(rr.creative))as creative,(SUM(rr.impression))as impression FROM tyr_rate_review rr WHERE tyroe_id = :tyroe_id' ;
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()){
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function get_tyroe_total_score(&$tyr_rate_review_obj) {
        $query = "SELECT ((AVG(technical))+(AVG(creative))+(AVG(impression)))/3 AS"
                        . " tyroe_score  FROM " . TABLE_RATE_REVIEW . " WHERE tyroe_id = :tyroe_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_tyroe_score_total_1(&$tyr_rate_review_obj) {
        $query = "SELECT ((AVG(technical))+(AVG(creative))+(AVG(impression)))/3 AS tyroe_score  FROM tyr_rate_review WHERE tyroe_id = :tyroe_id AND job_id = :job_id AND user_id = :user_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_rate_review_obj->job_id);
        $statement->bindParam(':user_id', $tyr_rate_review_obj->user_id);
        $statement->bindParam(':status_sl', $tyr_rate_review_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['tyroe_score'] = $row['tyroe_score'];
        }
        return $return_array;
    }
    
    public function get_tyroe_score_total_2(&$tyr_rate_review_obj) {
        $query = "SELECT ((AVG(technical))+(AVG(creative))+(AVG(impression)))/3 AS tyroe_score  FROM tyr_rate_review WHERE tyroe_id = :tyroe_id AND job_id = :job_id AND status_sl = :status_sl";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_rate_review_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_rate_review_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['tyroe_score'] = $row['tyroe_score'];
        }
        return $return_array;
    }
    
    public function update_rate_review(&$tyr_rate_review_obj) {
        
        if($tyr_rate_review_obj->anonymous_feedback_status == ''){
            $tyr_rate_review_obj->anonymous_feedback_status = 0;
        }
        
        if($tyr_rate_review_obj->anonymous_feedback_message == ''){
            $tyr_rate_review_obj->anonymous_feedback_message = '';
        }
        
        $query = 'update tyr_rate_review set user_id = :user_id,'
                . ' user_type = :user_type,'
                . ' tyroe_id = :tyroe_id,'
                . ' job_id = :job_id,'
                . ' level_id = :level_id,'
                . ' technical = :technical,'
                . ' creative = :creative,'
                . ' impression = :impression,'
                . ' review_comment = :review_comment,'
                . ' action_shortlist = :action_shortlist,'
                . ' action_interview = :action_interview,'
                . ' anonymous_feedback_message = :anonymous_feedback_message,'
                . ' anonymous_feedback_status = :anonymous_feedback_status,'
                . ' status_sl = :status_sl,'
                . ' updated_at = :updated_at,'
                . ' moderated = :moderated' 
                . ' where review_id = :review_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rate_review_obj->user_id);
        $statement->bindParam(':user_type', $tyr_rate_review_obj->user_type);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->bindParam(':job_id', $tyr_rate_review_obj->job_id);
        $statement->bindParam(':level_id', $tyr_rate_review_obj->level_id);
        $statement->bindParam(':technical', $tyr_rate_review_obj->technical);
        $statement->bindParam(':creative', $tyr_rate_review_obj->creative);
        $statement->bindParam(':impression', $tyr_rate_review_obj->impression);
        $statement->bindParam(':review_comment', $tyr_rate_review_obj->review_comment);
        $statement->bindParam(':action_shortlist', $tyr_rate_review_obj->action_shortlist);
        $statement->bindParam(':action_interview', $tyr_rate_review_obj->action_interview);
        $statement->bindParam(':anonymous_feedback_message', $tyr_rate_review_obj->anonymous_feedback_message);
        $statement->bindParam(':anonymous_feedback_status', $tyr_rate_review_obj->anonymous_feedback_status);
        $statement->bindParam(':status_sl', $tyr_rate_review_obj->status_sl);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':moderated', $tyr_rate_review_obj->moderated);
        $statement->bindParam(':review_id', $tyr_rate_review_obj->review_id);
        $statement->execute();
        return true;
    }
    
    public function get_anonymous_message(&$tyr_rate_review_obj) {
        $query = "SELECT tyr_rate_review.anonymous_feedback_message FROM tyr_rate_review
                      WHERE tyr_rate_review.review_id= :review_id AND tyr_rate_review.status_sl = 1 AND tyr_rate_review.anonymous_feedback_status = 1";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':review_id', $tyr_rate_review_obj->review_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_tyroe_rate_review(&$tyr_rate_review_obj) {
        $query = 'SELECT * FROM tyr_rate_review WHERE tyroe_id = :tyroe_id AND user_id = :user_id AND job_id = :job_id AND status_sl = :status_sl';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->bindParam(':user_id', $tyr_rate_review_obj->user_id);
        $statement->bindParam(':job_id', $tyr_rate_review_obj->job_id);
        $statement->bindParam(':status_sl', $tyr_rate_review_obj->status_sl);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array['review_id'] = $row['review_id'];
           $return_array['user_type'] = $row['user_type'];
           $return_array['tyroe_id'] = $row['tyroe_id'];
           $return_array['job_id'] = $row['job_id'];
           $return_array['level_id'] = $row['level_id'];
           $return_array['technical'] = $row['technical'];
           $return_array['creative'] = $row['creative'];
           $return_array['impression'] = $row['impression'];
           $return_array['review_comment'] = $row['review_comment'];
           $return_array['action_shortlist'] = $row['action_shortlist'];
           $return_array['action_interview'] = $row['action_interview'];
           $return_array['anonymous_feedback_message'] = $row['anonymous_feedback_message'];
           $return_array['anonymous_feedback_status']  = $row['anonymous_feedback_status'];
           $return_array['status_sl'] = $row['status_sl'];
        }
        return $return_array;
    }
    
    public function get_tyroe_avg_review_score(&$tyr_rate_review_obj) {
        $query = "SELECT ((AVG(technical))+(AVG(creative))+(AVG(impression)))/3 AS tyroe_score  FROM tyr_rate_review WHERE tyroe_id = :tyroe_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':tyroe_id', $tyr_rate_review_obj->tyroe_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
    public function get_pending_moderation_review_count($user_id){
        $query = 'select count(admin_user_id) as count from pending_review_moderation_view where admin_user_id = :admin_user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':admin_user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = 0;
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row['count'];
        }
        return $return_array;
    }
    
    public function get_pending_moderation_review($user_id){
        $query = 'select * from pending_review_moderation_view where admin_user_id = :admin_user_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':admin_user_id', $user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = array();
        while (($row = $statement->fetch()) != FALSE) {
           $return_array[] = $row;
        }
        return $return_array;
    }
    
    public function update_pending_moderation($rate_review_obj){
        $query = 'update tyr_rate_review set moderated = :moderated where review_id = :review_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':moderated', $rate_review_obj->moderated);
        $statement->bindParam(':review_id', $rate_review_obj->review_id);
        $statement->execute();
    }
}