<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_roles_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_roles(&$tyr_roles_obj) {
        $query = 'INSERT into tyr_roles(
                    role
                  ) values(
                    role
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role', $tyr_roles_obj->role);
        $statement->execute();
        $tyr_roles_obj->skill_id = $this->db_connection->lastInsertId('tyr_roles_skill_id_seq');
    }

    public function get_roles(&$tyr_roles_obj) {
        $query = 'select * from tyr_roles where role_id = :role_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':role_id', $tyr_roles_obj->role_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_roles_obj->role_id = $row['role_id'];
           $tyr_roles_obj->role = $row['role'];
        }
    }
    
}