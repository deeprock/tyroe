<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_profile_view_dao extends Abstract_DAO {

    public function __construct($db_conn) {
        parent::__construct($db_conn);
    }

    public function save_profile_view(&$tyr_profile_view_obj) {
        $query = 'INSERT into tyr_profile_view(
                    viewer_id, viewer_type, job_id, user_id, created_timestamp, created_at, created_by, updated_at, updated_by
                  ) values(
                    :viewer_id, :viewer_type, :job_id, :user_id, :created_timestamp, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':viewer_id', $tyr_profile_view_obj->viewer_id);
        $statement->bindParam(':viewer_type', $tyr_profile_view_obj->viewer_type);
        $statement->bindParam(':job_id', $tyr_profile_view_obj->job_id);
        $statement->bindParam(':user_id', intval($tyr_profile_view_obj->user_id));
        $statement->bindParam(':created_timestamp', $tyr_profile_view_obj->created_timestamp);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', intval($tyr_profile_view_obj->created_updated_by));
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', intval($tyr_profile_view_obj->created_updated_by));
        $statement->execute();
        $tyr_profile_view_obj->profile_view_id = $this->db_connection->lastInsertId('tyr_profile_view_profile_view_id_seq');
    }

    public function get_profile_view(&$tyr_profile_view_obj) {
        $query = 'select * from tyr_profile_view where profile_view_id = :profile_view_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':profile_view_id', $tyr_profile_view_obj->profile_view_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
            $tyr_profile_view_obj->profile_view_id = $row['profile_view_id'];
            $tyr_profile_view_obj->viewer_id = $row['viewer_id'];
            $tyr_profile_view_obj->viewer_type = $row['viewer_type'];
            $tyr_profile_view_obj->job_id = $row['job_id'];
            $tyr_profile_view_obj->user_id = $row['user_id'];
            $tyr_profile_view_obj->created_timestamp = $row['created_timestamp'];
        }
    }

    public function get_viewer_count_by_viewer_type(&$tyr_profile_view_obj) {
        $query = 'select COUNT(*) as cnt from tyr_profile_view where user_id = :user_id and viewer_type = :viewer_type';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->bindParam(':viewer_type', $tyr_profile_view_obj->viewer_type);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_viewer_id_count_by_viewer_type(&$tyr_profile_view_obj) {
        $query = 'select COUNT(Distinct viewer_id) as cnt from tyr_profile_view where user_id = :user_id and viewer_type = :viewer_type';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->bindParam(':viewer_type', $tyr_profile_view_obj->viewer_type);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_viewer_count_by_viewer_type_IN(&$tyr_profile_view_obj, $type_in) {
        $query = 'select COUNT(*) as cnt from tyr_profile_view where user_id = :user_id and viewer_type in (' . $type_in . ')';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function check_already_view_by_user(&$tyr_profile_view_obj) {
        $query = 'select COUNT(*) as cnt from tyr_profile_view where user_id = :user_id and viewer_id = :viewer_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->bindParam(':viewer_id', $tyr_profile_view_obj->viewer_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $return_array = $row;
        }
        return $return_array;
    }

    public function get_profile_view_month_chart(&$tyr_profile_view_obj) {
        $query = "SELECT
                COUNT(case when EXTRACT(MONTH FROM p.created_at)::integer = 1 then 1 else NULL end) AS col_1,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 2 then 1 else NULL end) AS col_2,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 3 then 1 else NULL end) AS col_3,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 4 then 1 else NULL end) AS col_4,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 5 then 1 else NULL end) AS col_5,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 6 then 1 else NULL end) AS col_6,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 7 then 1 else NULL end) AS col_7,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 8 then 1 else NULL end) AS col_8,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 9 then 1 else NULL end) AS col_9,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 10 then 1 else NULL end) AS col_10,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 11 then 1 else NULL end) AS col_11,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 12 then 1 else NULL end) AS col_12
            FROM tyr_profile_view p
            WHERE p.user_id = :user_id
                        AND EXTRACT(YEAR FROM p.created_at) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP)";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $temp_row = array();
            $temp_row['1'] = $row['col_1'];
            $temp_row['2'] = $row['col_2'];
            $temp_row['3'] = $row['col_3'];
            $temp_row['4'] = $row['col_4'];
            $temp_row['5'] = $row['col_5'];
            $temp_row['6'] = $row['col_6'];
            $temp_row['7'] = $row['col_7'];
            $temp_row['8'] = $row['col_8'];
            $temp_row['9'] = $row['col_9'];
            $temp_row['10'] = $row['col_10'];
            $temp_row['11'] = $row['col_11'];
            $temp_row['12'] = $row['col_12'];
            $return_array = $temp_row;
        }
        return $return_array;
    }

    public function get_profile_view_month_chart_other(&$tyr_profile_view_obj) {
        $query = "SELECT
                COUNT(case when EXTRACT(MONTH FROM p.created_at)::integer = 1 then 1 else NULL end) AS col_1,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 2 then 1 else NULL end) AS col_2,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 3 then 1 else NULL end) AS col_3,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 4 then 1 else NULL end) AS col_4,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 5 then 1 else NULL end) AS col_5,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 6 then 1 else NULL end) AS col_6,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 7 then 1 else NULL end) AS col_7,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 8 then 1 else NULL end) AS col_8,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 9 then 1 else NULL end) AS col_9,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 10 then 1 else NULL end) AS col_10,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 11 then 1 else NULL end) AS col_11,
                COUNT(case when EXTRACT(MONTH FROM p.created_at) = 12 then 1 else NULL end) AS col_12
            FROM tyr_profile_view p
            WHERE p.user_id != :user_id
                    AND p.user_id != 0
                        AND EXTRACT(YEAR FROM p.created_at) = EXTRACT(YEAR FROM CURRENT_TIMESTAMP)
        ";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $temp_row = array();
            $temp_row['1'] = $row['col_1'];
            $temp_row['2'] = $row['col_2'];
            $temp_row['3'] = $row['col_3'];
            $temp_row['4'] = $row['col_4'];
            $temp_row['5'] = $row['col_5'];
            $temp_row['6'] = $row['col_6'];
            $temp_row['7'] = $row['col_7'];
            $temp_row['8'] = $row['col_8'];
            $temp_row['9'] = $row['col_9'];
            $temp_row['10'] = $row['col_10'];
            $temp_row['11'] = $row['col_11'];
            $temp_row['12'] = $row['col_12'];
            $return_array = $temp_row;
        }
        return $return_array;
    }

    public function get_profile_view_week_chart(&$tyr_profile_view_obj) {
        $query = "SELECT
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 1 then 1 else 0 end),NULL ) AS col_1,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 2 then 1 else 0 end),NULL ) AS col_2,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 3 then 1 else 0 end),NULL ) AS col_3,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 4 then 1 else 0 end),NULL ) AS col_4,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 5 then 1 else 0 end),NULL ) AS col_5,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 6 then 1 else 0 end),NULL ) AS col_6,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 0 then 1 else 0 end),NULL ) AS col_7
                FROM tyr_profile_view p
                WHERE (p.created_at >= (NOW() - INTERVAL '6 DAY'))
                AND p.user_id = :user_id";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $temp_row = array();
            $temp_row['1'] = $row['col_1'];
            $temp_row['2'] = $row['col_2'];
            $temp_row['3'] = $row['col_3'];
            $temp_row['4'] = $row['col_4'];
            $temp_row['5'] = $row['col_5'];
            $temp_row['6'] = $row['col_6'];
            $temp_row['7'] = $row['col_7'];
            $return_array = $temp_row;
        }
        return $return_array;
    }

    public function get_profile_view_week_chart_other(&$tyr_profile_view_obj) {
        $query = "SELECT
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 1 then 1 else 0 end),NULL ) AS col_1,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 2 then 1 else 0 end),NULL ) AS col_2,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 3 then 1 else 0 end),NULL ) AS col_3,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 4 then 1 else 0 end),NULL ) AS col_4,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 5 then 1 else 0 end),NULL ) AS col_5,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 6 then 1 else 0 end),NULL ) AS col_6,
                COALESCE( SUM(case when EXTRACT(DOW FROM p.created_at) = 0 then 1 else 0 end),NULL ) AS col_7
                FROM tyr_profile_view p
                WHERE (p.created_at >= (NOW() - INTERVAL '6 DAY'))
                AND p.user_id != :user_id AND  p.user_id != 0";
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_profile_view_obj->user_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
            $temp_row = array();
            $temp_row['1'] = $row['col_1'];
            $temp_row['2'] = $row['col_2'];
            $temp_row['3'] = $row['col_3'];
            $temp_row['4'] = $row['col_4'];
            $temp_row['5'] = $row['col_5'];
            $temp_row['6'] = $row['col_6'];
            $temp_row['7'] = $row['col_7'];
            $return_array = $temp_row;
        }
        return $return_array;
    }

}
