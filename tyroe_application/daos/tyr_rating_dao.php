<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_rating_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_rating(&$tyr_rating_obj) {
        $query = 'INSERT into tyr_rating(
                    user_id, job_id, rating, studio_id, created_at, created_by, updated_at, updated_by
                  ) values(
                    :user_id, :job_id, :rating, :studio_id, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':user_id', $tyr_rating_obj->user_id);
        $statement->bindParam(':job_id', $tyr_rating_obj->job_id);
        $statement->bindParam(':rating', $tyr_rating_obj->rating);
        $statement->bindParam(':studio_id', $tyr_rating_obj->studio_id);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_rating_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_rating_obj->created_updated_by);
        $statement->execute();
        $tyr_rating_obj->rate_id = $this->db_connection->lastInsertId('tyr_rating_rate_id_seq');
    }

    public function get_rating(&$tyr_rating_obj) {
        $query = 'select * from tyr_rating where rate_id = :rate_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':rate_id', $tyr_rating_obj->rate_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_rating_obj->rate_id = $row['rate_id'];
           $tyr_rating_obj->user_id = $row['user_id'];
           $tyr_rating_obj->job_id = $row['job_id'];
           $tyr_rating_obj->rating = $row['rating'];
           $tyr_rating_obj->studio_id = $row['studio_id'];
        }
    }
    
}