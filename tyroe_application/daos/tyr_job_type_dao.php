<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_job_type_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_job_type(&$tyr_job_type_obj) {
        $query = 'INSERT into tyr_job_type(
                    job_type, created_at, created_by, updated_at, updated_by
                  ) values(
                    :job_type, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_type', $tyr_job_type_obj->job_type);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_job_type_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_job_type_obj->created_updated_by);
        $statement->execute();
        $tyr_job_type_obj->job_type_id = $this->db_connection->lastInsertId('tyr_job_type_job_type_id_seq');
    }

    public function get_job_type(&$tyr_job_type_obj) {
        $query = 'select * from tyr_job_type where job_type_id = :job_type_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':job_type_id', $tyr_job_type_obj->job_type_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_job_type_obj->job_type_id = $row['job_type_id'];
           $tyr_job_type_obj->job_type = $row['job_type'];
        }
    }
    
    public function get_all_job_types(&$tyr_job_type_obj) {
        $query = 'SELECT job_type_id, job_type FROM tyr_job_type';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $return_array[] = $row; 
        }
        return $return_array;
    }
    
}