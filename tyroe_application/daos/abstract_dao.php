<?php

require_once(APPPATH . 'config/tables' . EXT);

class Abstract_DAO {

    public $db_connection;
    public $created_updated_at;

    public function __construct($db_conn) {
        $this->db_connection = $db_conn;
        $format = 'Y-m-d H:i:s';
        $this->created_updated_at = date($format);
    }

    public function generate_today_date_time() {
        $format = 'Y-m-d H:i:s';
        return date($format);
    }

}