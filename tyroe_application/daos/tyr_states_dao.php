<?php

require_once(APPPATH . 'daos/abstract_dao' . EXT);

Class Tyr_states_dao extends Abstract_DAO {
    
    public function __construct($db_conn) {
        parent::__construct($db_conn);
        
    }
   
    public function save_states(&$tyr_states_obj) {
        $query = 'INSERT into tyr_states(
                    country_id, states_name, code, adm1code, created_at, created_by, updated_at, updated_by
                  ) values(
                    :country_id, :states_name, :code, :adm1code, :created_at, :created_by, :updated_at, :updated_by
                  )';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_states_obj->country_id);
        $statement->bindParam(':states_name', $tyr_states_obj->states_name);
        $statement->bindParam(':code', $tyr_states_obj->code);
        $statement->bindParam(':adm1code', $tyr_states_obj->adm1code);
        $statement->bindParam(':created_at', $this->created_updated_at);
        $statement->bindParam(':created_by', $tyr_states_obj->created_updated_by);
        $statement->bindParam(':updated_at', $this->created_updated_at);
        $statement->bindParam(':updated_by', $tyr_states_obj->created_updated_by);
        $statement->execute();
        $tyr_states_obj->states_id = $this->db_connection->lastInsertId('tyr_states_states_id_seq');
    }

    public function get_states(&$tyr_states_obj) {
        $query = 'select * from tyr_states where states_id = :states_id';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':states_id', $tyr_states_obj->states_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        if (($row = $statement->fetch()) != FALSE) {
           $tyr_states_obj->states_id = $row['states_id'];
           $tyr_states_obj->country_id = $row['country_id'];
           $tyr_states_obj->states_name = $row['states_name'];
           $tyr_states_obj->code = $row['code'];
           $tyr_states_obj->adm1code = $row['adm1code'];
        }
    }
    
    
    public function get_all_states_by_country(&$tyr_states_obj) {
        $query = 'select * from tyr_states where country_id = :country_id ORDER BY states_name ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->bindParam(':country_id', $tyr_states_obj->country_id);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['states_id'] = $row['states_id'];
           $temp_array['country_id'] = $row['country_id'];
           $temp_array['states_name'] = $row['states_name'];
           $temp_array['code'] = $row['code'];
           $temp_array['adm1code'] = $row['adm1code'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_all_states() {
        $query = 'select * from tyr_states where 1=1 ORDER BY states_name ASC';
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        while($row = $statement->fetch()) { if(!is_array($return_array)) $return_array = array();
           $temp_array = array();
           $temp_array['states_id'] = $row['states_id'];
           $temp_array['country_id'] = $row['country_id'];
           $temp_array['states_name'] = $row['states_name'];
           $temp_array['code'] = $row['code'];
           $temp_array['adm1code'] = $row['adm1code'];
           $return_array[] = $temp_array;
        }
        return $return_array;
    }
    
    public function get_all_states_by_keyword(&$tyr_states_obj,$search_input) {
        $query = "SELECT array_to_string(array_agg(" . TABLE_STATES . ".states_id ORDER BY " . TABLE_STATES . ".states_id ASC),',') AS states_id  FROM " . TABLE_STATES . " WHERE lower(states_name) LIKE '%" . $search_input . "%'";
        $statement = $this->db_connection->prepare($query);
        $statement->execute();
        $statement->setFetchMode(PDO::FETCH_ASSOC);
        $return_array = '';
        if (($row = $statement->fetch()) != FALSE) {
           $return_array = $row;
        }
        return $return_array;
    }
    
}