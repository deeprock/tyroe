<?php
//include the Facebook PHP SDK
require_once("../../DataAccess/SQLPerformer.php");
require_once("../../BusinessLogic/UserBusinessLogic.php");
include_once 'facebook.php';
$UserName = $_SESSION['CurrentUser']->UserName;
$UserID = $_SESSION['CurrentUser']->UserID;


//start the session if necessary
if( session_id() ) {

} else {
	session_start();
}

//instantiate the Facebook library with the APP ID and APP SECRET
$facebook = new Facebook(array(
	'appId' => '246506782137125',
	'secret' => 'a8bb013d4f6b435016037e1ad8f064e6',
	'cookie' => true
));

    $text = $_POST['tt'];
    $status = "Current Photography Status: ".$_POST['status1'];
    $ten_days_rank = "My rank on the Quarterly contest 10 days before the end: ".$_POST['10daysrank'];
    $two_days_rank = "My rank on the Quarterly contest 2 days before the end: ".$_POST['2daysrank'];

$stmt = "SELECT * FROM fb_users where photo_userid = '$UserID'";
$result = SQLPerformer::QueryPerform($stmt);
$_SESSION['active']['access_token'] = $result[0][4];

$photo = $_POST['photo1'];
$message = $text."\n".$status."\n".$ten_days_rank."\n".$two_days_rank."\n".$_POST['reviews'];

$parameters = array(
	'message' => $message,
	'picture' => $photo
);

//print_r($parameters);

//add the access token to it

$parameters['access_token'] = $_SESSION['active']['access_token'];
//$parameters['access_token']='AAADgMlUvGyUBAIUUzLrZAFrYChnAPWBwSFsc8mEmgrET4sMqfg3x2VSAcspKYjlgLtNMaQmXrmN8tHTgs6zZC1MrtLRMfW2I5lKfAOj4FGGT1Px1vY';

//build and call our Graph API request
$newpost = $facebook->api(
	'/me/feed',
	'POST',
	$parameters
);
//print_r($newpost);

//redirect back to the manage page
header('Location:../'.$_SESSION['CurrentUser']->FirstName."-".$_SESSION['CurrentUser']->LastName.'.html?tab=synchronize');
exit();