<?php
//include the Facebook PHP SDK
include_once 'facebook.php';

//start the session if necessary
if( session_id() ) {

} else {
	session_start();
}

//instantiate the Facebook library with the APP ID and APP SECRET
$facebook = new Facebook(array(
	'appId' => '437231886291233',
	'secret' => '75e917f393dacc47a71b7b473785f2c3',
	'cookie' => true
));

//get the info from the form
$parameters = array(
	'message' => $_POST['message']/*,
	'picture' => $_POST['picture'],
	'link' => $_POST['link'],
	'name' => $_POST['name'],
	'caption' => $_POST['caption'],
	'description' => $_POST['description']*/
);

//add the access token to it
$parameters['access_token'] = $_SESSION['active']['access_token'];
//$parameters['access_token']='AAACEdEose0cBAGAiZBTwdZBzCZCuKRLEGtfL7ZCDzwum6jr8v8cMr1Tf6Jp26VqZCkHOSfRVNEJTPUqsjT7Gzu97rZAkFiSoNodmt0TTzCJr8PgZCQyDgdQ';

//build and call our Graph API request
$newpost = $facebook->api(
	'/me/feed',
	'POST',
	$parameters
);

//redirect back to the manage page
header('Location: manage.php');
exit();