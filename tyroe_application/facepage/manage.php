<?php
//include the Facebook PHP SDK
include_once 'facebook.php';
include_once 'db.php';
//start the session if necessary
if( session_id() ) {

} else {
	session_start();
}

//instantiate the Facebook library with the APP ID and APP SECRET
$facebook = new Facebook(array(
	'appId' => '437231886291233',
	'secret' => '75e917f393dacc47a71b7b473785f2c3',
	'cookie' => true
));

//get the news feed of the active page using the page's access token
$page_feed = $facebook->api(
	'/me/feed',
	'GET',
	array(
		'access_token' => $_SESSION['active']['access_token']
	)
);

//var_dump($page_feed); exit;
?>

<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="css/reset.css" type="text/css" />
	<link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap-dropdown.js"></script>
	<script>
	$('.topbar').dropdown()
	</script>
	
	<style>
	body {
		padding-top: 40px;
		/*background-color: #EEEEEE;*/
	}
	img {
		vertical-align: middle;
	}
	#main {
		text-align: center;
	}
	
	.content {
		background-color: #FFFFFF;
		border-radius: 0 0 6px 6px;
		box-shadow: 0 1px 2px rgba(0, 0, 0, 0.15);
		margin: 0 -20px;
		padding: 20px;
	}
	.content .span6 {
		border-left: 1px solid #EEEEEE;
		margin-left: 0;
		padding-left: 19px;
		text-align: left;
	}
	.page-header {
		background-color: #F5F5F5;
		margin: -20px -20px 20px;
		padding: 20px 20px 10px;
		text-align: left;
	}
	
	#feed_list { list-style: none; !important; }
	#feed_list > li { text-align: left; }
	.clearfix { clear: both; }
	.post_photo { width: 50px; float: left; margin-right: 10px; }
	.post_data { width: 360px; float: left; }
	.post_picture { width: 90px; float: left; margin-right: 10px; }
	.post_data_again { width: 260px; float: left; }
	</style>

</head>
<body>
<? //print_r($_SESSION['accounts']); ?>
	<div id="main" class="container">
		<div class="content">
			
			
			<div class="row">
				
				<div class="span6">
					<form method="POST" action="newpost.php" class="form-stacked">
                        <label>Select Page</label>
                        <select onchange="window.location='switch.php?page_id='+this.value">
                        <option <?=(isset($_SESSION['selected_active']))?'':'selected="selected"';?>>Select</option>
                            <?php
                                $page_data=mysql_query("select * from fb_users");
                                while($row=mysql_fetch_array($page_data)){
                                $pagedata=unserialize($row['user_data']);
                                foreach($pagedata as $page): ?>
        						<? if($page['category']!="Application" ): ?>
                                <option <?=($_SESSION['selected_active']==$page['id'])?'selected="selected"':'';?> value="<?php echo $page['id']; ?>" ><?php echo $page['name']; ?></option>
        					<?php endif; 
                            endforeach;
                            }
                             ?>
                        </select>

						<label for="message">Message:</label>
                        <textarea class="span5" id="message" name="message" placeholder="Message of post" cols="36" rows="8" ></textarea>
						<!--<input class="span5" type="text" id="message" name="message" placeholder="Message of post" />
						<label for="picture">Picture:</label>
						<input class="span5" type="text" id="picture" name="picture" placeholder="Picture of post" />
						<label for="link">Link:</label>
						<input class="span5" type="text" id="link" name="link" placeholder="Link of post" />
						<label for="name">Name:</label>
						<input class="span5" type="text" id="name" name="name" placeholder="Name of post" />
						<label for="caption">Caption:</label>
						<input class="span5" type="text" id="caption" name="caption" placeholder="Caption of post" />
						<label for="description">Description:</label>
						<input class="span5" type="text" id="description" name="description" placeholder="Description of post" />-->
						
						<div class="actions">
							<input type="submit" class="btn primary" value="Post" />
							<input type="reset" class="btn" value="Reset" />
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</div>

</body>
</html>